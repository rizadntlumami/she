import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Text,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input
} from "native-base";

import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton
  } from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewReportDetail from "../../components/ListViewReportDetail";

class AccidentReportDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            dataDetail: [],
            isLoading: true,
            headerAccidentReport: [],
            noPeg:'',
            visibleDialogSubmit:false
        };
    }

    static navigationOptions = {
        header: null
    };

    _renderItem = ({ item }) => (
        <ListItem data={item}></ListItem>
    )

    navigateToScreen(route, id_Accident) {
        // alert(this.state.headerAccidentReport.ID);
        AsyncStorage.setItem('id', JSON.stringify(id_Accident)).then(() => {
            this.props.navigation.navigate(route);
        })
    }

    componentDidMount() {
        
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
            AsyncStorage.getItem('list').then((id_Accident) => {
                this.setState({ headerAccidentReport: JSON.parse(id_Accident) });
                this.setState({ isLoading: false });
                console.log(this.state.headerAccidentReport);
                var arrNoPeg=this.state.headerAccidentReport.BADGE_VICT.split('');
                var noPeg=''
                var zeroStop=false
                for (let i=0;i<arrNoPeg.length;i++){
                    if (arrNoPeg[i]!='0'){
                        zeroStop=true
                    } 
                    if (zeroStop){
                        noPeg+=arrNoPeg[i]
                    } 
                }
                this.setState({
                    noPeg:noPeg
                })
                console.log(this.state.noPeg)
            })
            //this.loadData(id_Unsafe);
        });
    }

    konfirmasideleteHeader() {
        Alert.alert(
            'Apakah Anda Yakin Ingin Menghapus', this.state.headerAccidentReport.ID,
            [
                { text: 'Yes', onPress: () => this.deleteDetailAccident() },
                { text: 'Cancel' },
            ],
            { cancelable: false }
        )
    }

    deleteDetailAccident() {
        this.setState({
            visibleDialogSubmit:true
        })
        AsyncStorage.getItem('token').then((value) => {
            // alert(JSON.stringify(value));
            const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/accident/delete';
            var formData = new FormData();
            formData.append("token", value);
            formData.append("ID", this.state.headerAccidentReport.ID);

            fetch(url, {
                headers: {
                    "Content-Type": "multipart/form-data"
                },
                method: "POST",
                body: formData
            })
                .then(response => response.json())
                .then(response => {
                    if (response.status == 200) {
                        this.setState({
                            visibleDialogSubmit:false
                        })
                        Alert.alert('Success', 'Delete Success', [{
                            text: 'Okay',
                        }])
                        this.props.navigation.navigate("AccidentReport")
                    } else {
                        this.setState({
                            visibleDialogSubmit:false
                        })
                        Alert.alert('Error', 'Delete Failed', [{
                            text: 'Okay'
                        }])
                    }
                })
                .catch((error)=>{
                    this.setState({
                        visibleDialogSubmit:false
                    })
                    Alert.alert('Error', 'Delete Failed', [{
                        text: 'Okay'
                    }])
                    console.log(error)
                })
        })
    }

    render() {
        var listReportK3;
        var listDetailReportK3;
        
        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left style={{flex:1}}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("AccidentReport")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={{color:colors.white}}
                            />
                        </Button>
                    </Left>
                    <Body style={{flex:3,alignItems:'center'}}>
                        <Title style={styles.textbody}>Accident Detail Report</Title>
                    </Body>
                    <Right style={{flex:1}}>
                    {(this.state.headerAccidentReport.STATUS_REPORT!="CLOSE")&&(
                        
                        <Button
                            transparent
                            onPress={() => this.konfirmasideleteHeader()}
                        >
                            <Icon
                                name="ios-trash"
                                size={20}
                                style={{color:colors.white}}
                            />
                        </Button>
                    )}
                    {(this.state.headerAccidentReport.STATUS_REPORT!="CLOSE")&&(
                        <Button
                            transparent
                            
                            onPress={() => this.navigateToScreen('AccidentReportUpdate', this.state.headerAccidentReport)}
                        >
                            <Icon
                                name="ios-create"
                                size={20}
                                style={{color:colors.white}}
                            />
                        </Button>
                        
                    )}
                        
                    </Right>
                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                <Content style={{ marginTop: 10, marginBottom: 20 }}>
                    <ScrollView>
                        <View style={{ justifyContent: "center", alignItems: "center" }}>
                            <Text style={{ fontSize: 12, color: colors.green01 }}>Accident Detail {this.state.noPeg} - {this.state.headerAccidentReport.NAME_VICT}</Text>
                        </View>
                        <CardItem style={{ borderRadius: 0, marginTop: 5, }}>
                            <View style={{ flex: 1, flexDirection: "row" }}>
                                <View style={{ marginLeft: 10, flex: 2, marginRight: 10 }}>
                                {this.state.headerAccidentReport.PICT_BEFORE == null ? (
                                        <Image style={{  marginTop: 10,height: 200, width: "100%", borderColor: colors.gray, borderWidth: 3, borderRadius: 10 }} source={require("../../../assets/images/imgorder.png")} />
                                    ) : (
                                        <Image style={{ marginTop: 10, height: 200, width: "100%", borderColor: colors.gray, borderWidth: 3, borderRadius: 10 }} source={{ uri: GlobalConfig.SERVERHOST + 'api' + this.state.headerAccidentReport.PICT_BEFORE+ '?' + new Date() }} />
                                        )}
                                    
                                </View>
                            </View>
                        </CardItem>
                        <CardItem style={{ borderRadius: 0 }}>
                            <View style={{ flex: 1, flexDirection: "row" }}>
                                <View style={{ marginLeft: 10, flex: 2 }}>
                                    {/* <Text note style={{ fontSize: 10, paddingTop: 10 }}>Status</Text>
                                    {this.state.headerAccidentReport.STATUS_REPORT == 'OPEN' ? (
                                        <Text
                                            note
                                            style={{
                                                fontSize: 10, 
                                                fontWeight: "bold",
                                                color: colors.green01,
                                            }}
                                        >OPEN
                                        </Text>
                                    ) : (
                                            <Text
                                                note
                                                style={{
                                                    fontSize: 10, 
                                                    fontWeight: "bold",
                                                    color: colors.red,
                                                }}
                                            >CLOSE
                                        </Text>
                                        )} */}
                                        <Text note style={{ fontSize: 10, paddingTop: 10 }}>Tanggal</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.DATE_ACD}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Pukul</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.TIME_ACD}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Shift</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>Shift {this.state.headerAccidentReport.SHIFT}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Jenis Karyawan Korban</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.STAT_VICT_TEXT}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Unit Kerja</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.UK_VICT} - {this.state.headerAccidentReport.UK_VICT_TEXT}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Posisi</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.POS_TEXT} (Since {this.state.headerAccidentReport.DATE_IN})</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Umur</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.AGE} Tahun</Text>
                                    <Text style={{ fontSize: 10, paddingTop: 10 }}>Macam Pekerjaan Waktu Terjadi Kecelakaan</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.WORK_TASK}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Pengawas Kerja</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.SPV_NAME}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Lokasi</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.LOCATION} - {this.state.headerAccidentReport.LOCATION_TEXT}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Penyebab Terjadinya Kecelakaan</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.ACD_CAUSE}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Saksi Kecelakaan</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.S_NAME} - {this.state.headerAccidentReport.S_POS_TEXT}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Cidera Yang Dialami</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.INJ_TEXT} ({this.state.headerAccidentReport.INJ_NOTE})</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Pertolongan Pertama</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.FIRST_AID}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Pertolongan Selanjutnya</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.NEXT_AID}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Keadan Syarat Keselamatan Kerja</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.SAFETY_REQ}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Unsafe Action</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.UNSAFE_ACT}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Unsafe Condition</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.UNSAFE_COND}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Penjelasan Tentang Penyebab Terjadinya Kecelakaan</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.ACD_RPT_TEXT}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Saran Agar Kejadian Tidak Terulang</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.NOTE}</Text>
                                    <Text note style={{ fontSize: 10, paddingTop: 10 }}>Petugas Investigasi / PIC</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerAccidentReport.CREATE_BY}</Text>
                                </View>
                            </View>
                        </CardItem>
                    </ScrollView>
                    <View style={{ width: 270, position: "absolute" }}>
                        <Dialog
                            visible={this.state.visibleDialogSubmit}
                            dialogTitle={<DialogTitle title="Deleting Accident Report .." />}
                        >
                            <DialogContent>
                            {<ActivityIndicator size="large" color="#330066" animating />}
                            </DialogContent>
                        </Dialog>
                    </View>
                </Content>
                <CustomFooter navigation={this.props.navigation} menu='Safety' />
            </Container>

        );
    }
}

export default AccidentReportDetail;

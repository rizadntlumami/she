import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../../styles/colors";
import Moment from 'moment';

export default class ListViewHydrant extends Component {
  render() {
    var dateNow = Moment().format('YYYY-MM-DD');
    return (
      <View
        style={{
          height: '100%',
          width: "100%",
          paddingBottom:10,
        }}
      >
        <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{marginLeft:5, marginTop:5, width:'18%'}}>
              <Image
                source={this.props.imageUri}
                style={{ marginTop:5, width: 50, height: 50, resizeMode: "contain" }}
              />
            </View>

            <View style={{marginTop:10, marginRight:5, width:'59%'}}>
                <Text style={{fontSize:12, fontWeight:'bold'}}>{this.props.unit_kerja}</Text>
                <Text style={{fontSize:10, paddingTop: 0}}>{this.props.area}</Text>
                <Text style={{fontSize:11, paddingTop:0}}>Last Inspection : {Moment(this.props.last_inspection).format('DD MMMM YYYY')}</Text>
                <Text style={{fontSize:11, paddingTop:0}}>Expired Inspection : {Moment(this.props.exp_inspection).format('DD MMMM YYYY')}</Text>
            </View>

            <View style={{marginTop:10, paddingRight: 5, marginRight:5, width:'20%'}}>
                {dateNow <= this.props.exp_inspection ? (
                <Text
                  note
                  style={{
                    alignSelf: "flex-end",
                    fontSize: 9,
                    color: "white",
                    backgroundColor: colors.green01,
                    paddingRight: 4,
                    paddingLeft: 4,
                    paddingBottom: 2,
                    paddingTop: 2,
                    borderRadius: 6,
                    fontWeight: 'bold'
                  }}
                >CHECKED
                </Text>
              ):(
                <Text
                  note
                  style={{
                    alignSelf: "flex-end",
                    fontSize: 9,
                    color: "white",
                    backgroundColor: "#FF0101",
                    paddingRight: 4,
                    paddingLeft: 4,
                    paddingBottom: 2,
                    paddingTop: 2,
                    borderRadius: 6,
                    fontWeight: 'bold'
                  }}
                >UNCHECKED
                </Text>)}
            </View>
        </View>
      </View>
    );
  }
}

import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../styles/colors";
import GlobalConfig from "../components/GlobalConfig";

export default class ListViewSIO extends Component {
    render() {
        return (
            <View
                style={{
                    height: 65,
                    width: "100%",
                    paddingBottom: 5,
                }}
            >
                <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginLeft: 5, marginTop: 5, width: '18%'}}>
                    {this.props.imageSIO == GlobalConfig.SERVERHOST + "api" +null ? (
                                        <Image style={{ marginTop:0, height: 50, width: 50, borderRadius:10}} source={require("../../assets/images/imgorder.png")} />
                                    ) : (
                                        <Image style={{marginTop:0, height: 50, width: 50, borderRadius:10}} source={{uri : this.props.imageSIO}}/>
                                        )}
                        
                    </View>
                    <View style={{ marginLeft: 5, marginTop: 5, width: '82%' }}>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.props.pemegang}</Text>
                        <Text style={{ fontSize: 9, paddingTop: 2 }}>{this.props.unitkerja}</Text>
                        <Text style={{ fontSize: 9, paddingTop: 2 }}>{this.props.start} s/d {this.props.end}</Text>
                    </View>
                </View>
            </View>
        );
    }
}

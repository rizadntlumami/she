import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input
} from "native-base";

import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton
  } from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from 'react-native-datepicker';

class ListItem extends React.PureComponent {

    konfirmasideleteInspeksi(ID,NO_REKAP) {
        Alert.alert(
          'Apakah Anda Yakin Ingin Menghapus', this.props.data.ID,
          [
            {text: 'Yes', onPress:()=> this.deleteInspeksi(ID,NO_REKAP)},
            {text: 'Cancel'},
          ],
          { cancelable:false}
        )
      }

    deleteInspeksi(ID,NO_REKAP) {
        console.log(ID+NO_REKAP);
      // alert(route);
      AsyncStorage.getItem('token').then((value) => {
        this.props.setVisibleModal(true,'Deleting Inspeksi ..')
        // alert(JSON.stringify(value));
        const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe/delete_inspeksi';
        var formData = new FormData();
        formData.append("token", value);
        formData.append("ID", ID);
        formData.append("NO_REKAP", NO_REKAP);

        fetch(url, {
            headers: {
                "Content-Type": "multipart/form-data"
            },
            method: "POST",
            body: formData
        })
            .then(response => response.json())
            .then(response => {
                if (response.status == 200) {
                    this.props.setVisibleModal(false,'')
                    Alert.alert('Success', 'Delete Success', [{
                        text: 'Oke',
                    }])
                    this.props.navigation.navigate('Recapitulations');
                } else {
                    this.props.setVisibleModal(false,'')
                    Alert.alert('Error', 'Delete Failed', [{
                        text: 'Okay'
                    }])
                }
            })
            .catch((error)=>{
                console.log(error)
                this.props.setVisibleModal(false,'')
                    Alert.alert('Error', 'Delete Failed', [{
                        text: 'Okay'
                    }])
            })
    })
    }
  
    render() {
      return (
        <View>
          <Card style={{ marginLeft: 10, marginRight: 10 }}>
            <CardItem
            >
              <View style={{ flexDirection: "column", flex: 1 }}>
                <View style={{ flexDirection: "row", flex: 1 }}>
                  
                    <View style={{ flexDirection: "column", flex: 1 }}>
                      <Text style={{fontSize: 12,}}>
                        Nomor 
                      </Text>
                      <Text style={{fontSize: 12,}}>
                        Tanggal 
                      </Text>
                      <Text style={{fontSize: 12,}}>
                        Catatan 
                      </Text>
                    </View>   

                    <View style={{ flexDirection: "column", flex: 3 }}>
                      <Text style={{fontSize: 12,}}>
                        : {this.props.index+1}
                      </Text>
                      <Text style={{fontSize: 12,}}>
                        : {this.props.data.CHECKLIST_DATE}
                      </Text>
                      <Text style={{fontSize: 12,}}>
                        : {this.props.data.NOTE}
                      </Text>
                    </View>           
                  
                  
                  <View style={{flex: 1 }}>
                    <Button
                        style={{ alignSelf: "flex-end",flexWrap: 'wrap', backgroundColor:colors.red}}
                        onPress={() => this.konfirmasideleteInspeksi(this.props.data.ID, this.props.data.NO_REKAP)}
                        >
                            <Icon
                                name="ios-trash"
                                size={20}
                                style={{color: colors.white,
                                    fontSize: 20,
                                    paddingTop:5,
                                    height:30,
                                    }}
                            />
                    </Button>
                </View>
                </View>
              </View>
            </CardItem>
          </Card>
        </View>
      );
    }
  }

export default class RecapitulationsDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isLoading: true,
      itemRekapitulasi:[],
      inspeksi_list:[],
      visibleDialogSubmit:false,
      visibleDialogSubmitText:''
    };
  }

  static navigationOptions = {
      header: null
  };

  componentDidMount() {
      AsyncStorage.getItem('list').then((listRekapitulasi) => {
          this.setState({ itemRekapitulasi: JSON.parse(listRekapitulasi) });
          this.setState({ isLoading: false });
          this.setState({ inspeksi_list: this.state.itemRekapitulasi.INSPEKSI_LIST})
          console.log(this.state.itemRekapitulasi);
          console.log(this.state.inspeksi_list.length)
      })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {

      });
  }

  _renderItem = ({ item,index }) => <ListItem data={item} index={index} navigation={this.props.navigation} setVisibleModal={(status,text)=>this.setVisibleModal(status,text)}/>;

  setVisibleModal(status,text){
      this.setState({
          visibleDialogSubmit:status,
          visibleDialogSubmitText:text
      })
  }

  navigateToScreen(route, itemRekapitulasi) {
      AsyncStorage.setItem('list', JSON.stringify(itemRekapitulasi)).then(() => {
          this.props.navigation.navigate(route);
      })
  }

  konfirmasideleteRekapitulasi() {
    Alert.alert(
      'Apakah Anda Yakin Ingin Menghapus', this.state.itemRekapitulasi.NO_REKAP,
      [
        {text: 'Yes', onPress:()=> this.deleteRekapitulasi()},
        {text: 'Cancel'},
      ],
      { cancelable:false}
    )
  }

  deleteRekapitulasi() {
    this.setState({
        visibleDialogSubmit:true,
        visibleDialogSubmitText:'Deleting Recapitulation ..'
    })
    AsyncStorage.getItem('token').then((value) => {
        // alert(JSON.stringify(value));
        const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe/delete_rekap';
        var formData = new FormData();
        formData.append("token", value);
        formData.append("ID", this.state.itemRekapitulasi.ID);

        fetch(url, {
            headers: {
                "Content-Type": "multipart/form-data"
            },
            method: "POST",
            body: formData
        })
            .then(response => response.json())
            .then(response => {
                if (response.status == 200) {
                    this.setState({
                        visibleDialogSubmit:false
                    })
                    Alert.alert('Success', 'Delete Success', [{
                        text: 'Oke',
                    }])
                    this.props.navigation.navigate("Recapitulations");
                } else {
                    this.setState({
                        visibleDialogSubmit:false
                    })
                    Alert.alert('Error', 'Delete Failed', [{
                        text: 'Okay'
                    }])
                }
            })
            .catch((error)=>{
                console.log(error)
                this.setState({
                    visibleDialogSubmit:false
                })
                Alert.alert('Error', 'Delete Failed', [{
                    text: 'Okay'
                }])
            })
    })
  }

  render() {
    var list;
    if (this.state.isloading) {
        list = (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                <ActivityIndicator size="large" color="#330066" animating />
            </View>
        );
    } else {
        if (this.state.isEmpty) {
            list = (
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <Thumbnail square large source={require("../../../assets/images/empty.png")} />
                    <Text>No Data!</Text>
                </View>
            )
        } else {
            list = (
    <View >
      <CardItem style={{ borderRadius: 0 }}>
          <View  style={{width: '100%'}}>
            <Text style={{ fontSize: 10, paddingTop: 10 }}>Foto Sebelum</Text>
            <Image style={{ marginTop: 10, height: 200, width: '100%', borderColor: colors.gray, borderWidth: 3 }} source={{ uri: GlobalConfig.SERVERHOST + 'api' + this.state.itemRekapitulasi.FOTO_BEF+ '?' + new Date() ,cache: 'reload'}} />
            <Text style={{ fontSize: 10, paddingTop: 10 }}>Foto Sesudah</Text>
            <Image style={{ marginTop: 10, height: 200, width: '100%', borderColor: colors.gray, borderWidth: 3 }} source={{ uri: GlobalConfig.SERVERHOST + 'api' + this.state.itemRekapitulasi.FOTO_AFT+ '?' + new Date() ,cache: 'reload'}} />
            <Text style={{fontSize:10}}>No Rekap</Text>
            <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.itemRekapitulasi.NO_REKAP}</Text>
            <Text style={{fontSize:10, paddingTop:10}}>Status</Text>
            {this.state.itemRekapitulasi.STATUS=='OPEN' ? (
                <Text style={{fontSize:10, fontWeight:"bold", color:colors.green01}}>{this.state.itemRekapitulasi.STATUS}</Text>
            ) : (
                <Text style={{fontSize:10, fontWeight:"bold", color:colors.red}}>{this.state.itemRekapitulasi.STATUS}</Text>
            )}
            <Text style={{fontSize:10,paddingTop:10}}>Tanggal Temuan</Text>
            <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.itemRekapitulasi.CREATE_AT}</Text>
            <Text style={{fontSize:10, paddingTop:10}}>Tanggal Komitmen</Text>
            {/*{this.state.itemRekapitulasi.BATAS_WAKTU == null ? (
              <Text style={{fontSize:10, fontWeight:"bold"}}>-</Text>
            ) : (*/}
            <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.itemRekapitulasi.BATAS_WAKTU}</Text>
            <Text style={{fontSize:10, paddingTop:10}}>Note</Text>
            <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.itemRekapitulasi.NOTE}</Text>
            <Text style={{fontSize:10, paddingTop:10}}>Evaluasi</Text>
            <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.itemRekapitulasi.EVALUASI}</Text>
            <Text style={{fontSize:10, paddingTop:10}}>Dasar Hukum</Text>
            <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.itemRekapitulasi.DASAR_HUKUM_TEXT}</Text>
            <Text style={{fontSize:10, paddingTop:10}}>Create BY</Text>
            <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.itemRekapitulasi.CREATE_BY}</Text>
            
          </View>
      </CardItem>
      <View
      style={{marginBottom:10}}
      >
        <Text style={{fontSize:10, paddingTop:10,marginLeft:20}}>Daftar tanggal pengecekkan:</Text>
        {this.state.inspeksi_list.length==0 ? (
            <Text style={{alignSelf:'center',marginBottom:20}}>
                ----NO DATA----
            </Text>
        ) : (
            <FlatList
                data={this.state.itemRekapitulasi.INSPEKSI_LIST}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
                
                /> 
        )}
      </View>
        
    </View>
  );
}
}

    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("Recapitulations")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3}}>
            <Title style={styles.textbody}>{this.state.itemRekapitulasi.NO_REKAP}</Title>
          </Body>
          <Right style={{flex:1}}>
                <Button
                transparent
                onPress={() => this.navigateToScreen('UpdateRecapitulations', this.state.itemRekapitulasi)}
                >
                    <Icon
                        name="ios-create"
                        size={20}
                        style={styles.facebookButtonIconOrder2}
                    />
                </Button>
                <Button
                transparent
                onPress={() => this.konfirmasideleteRekapitulasi()}
                >
                    <Icon
                        name="ios-trash"
                        size={20}
                        style={styles.facebookButtonIconOrder2}
                    />
                </Button>
            </Right>
        </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={{ flex: 1 }}>
        <Content style={{ marginTop: 0 }}>
          {list}
        </Content>
      </View>
      <View style={{ width: 270, position: "absolute" }}>
        <Dialog
            visible={this.state.visibleDialogSubmit}
            dialogTitle={<DialogTitle title={this.state.visibleDialogSubmitText} />}
        >
            <DialogContent>
            {<ActivityIndicator size="large" color="#330066" animating />}
            </DialogContent>
        </Dialog>
      </View>
      
    </Container>
    );
  }
}

import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback ,Platform, AsyncStorage } from "react-native";
import Ripple from "react-native-material-ripple";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    height: 100,
    width: 150,
    marginLeft: 3,
    marginRight:3,
    paddingTop:3,
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class NewsContentRekapitulasi extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={styles.container}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 5,
            elevation: 1,
            borderWidth:Platform.OS==='ios'?1:0,
    borderColor:(Platform.OS==='ios')?colors.lightGray:null
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          rippleColor={colors.accent}
        >
        <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ marginLeft:0 , paddingTop:3, width: 100 }}>
                <Text style={{fontSize: 10, fontWeight:'bold'}}>{this.props.noRekap}</Text>
            </View>
            <View>
                <Text style={{fontSize: 10, paddingTop:3, fontWeight:'bold', textAlign:'right', color:colors.green01}}>{this.props.status}</Text>
            </View>
        </View>
        <View>
            <Text numberOfLines={1} style={{fontSize: 9, fontWeight:'bold', textAlign:'left'}}>PIC : {this.props.create}</Text>
            <Text style={{fontSize: 9, fontWeight:'bold'}}>{this.props.evaluasi}</Text>
            <Text style={{fontSize: 9}}>{this.props.note}</Text>
        </View>
        <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ marginLeft: 2, paddingTop:2, width: 90 }}>
                <Text style={{fontSize: 8, fontWeight:'bold'}}>Batas Waktu</Text>
                <Text style={{fontSize: 8, fontStyle:'italic'}}>{this.props.dateLimit}</Text>
            </View>
            <View>
                <Text style={{fontSize: 8, fontWeight:'bold', textAlign:'right'}}>Commitment</Text>
                <Text style={{fontSize: 8, paddingTop:2, textAlign:'right', fontStyle:'italic'}}>{this.props.dateCommit}</Text>
            </View>
        </View>
        </Ripple>
      </View>
    );
  }
}

NewsContentRekapitulasi.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};

import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Label,
    Item,
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DateTimePicker from 'react-native-datepicker';
import DatePicker from 'react-native-datepicker';
import ImagePicker from "react-native-image-picker";
import Ripple from "react-native-material-ripple";
import CheckBox from 'react-native-check-box';
import moment from 'moment'
import ListView from "../../components/ListView";

export default class InspectionReportHydrantCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
        active: 'true',
        dataSource: [],
        isLoading: true,
        tabNumber:'tab1',
        foto: '',
        pickedImage: '',
        uri: '',
        fileName: '',
        jumlahPress: '',
        casing: 'V',
        kunci: 'V',
        hose: 'V',
        nozle: 'V',
        coplingkanan: 'V',
        coplingkiri: 'V',
        tutupkanan: 'V',
        tutupkiri: 'V',
        valveatas: 'V',
        valvekanan: 'V',
        valvekiri: 'V',
        body: 'V',
        notebox: '',
        notecopling: '',
        notetutup: '',
        notevalve: '',
        notepilar: '',
        temuan: '',
        visibleDialogSubmit:false
    };
  }

  static navigationOptions = {
      header: null
  };

  reset = () => {
    this.setState({
        pickedImage: null
    });
    }

    pickImageHandler = () => {
        ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
            if (res.didCancel) {
                console.log("User cancelled!");
            } else if (res.error) {
                console.log("Error", res.error);
            } else {
                this.setState({
                    pickedImage: res.uri,
                    uri: res.uri,
                    fileType: res.type
                });
            }
        });
    }

  _renderItem = ({ item }) => (
      <ListItem data={item}></ListItem>
  )

  componentDidMount() {
    AsyncStorage.getItem('idHydrant').then((idHydrant) => {
      this.setState({
        idHydrant: idHydrant,
      });
      this.loadLokasi();
    })

      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {

    });
  }

  loadLokasi() {
    this.setState({ visibleDialog: false, isLoading: true });
      const url = GlobalConfig.SERVERHOST + "getLokasiHydrant";
      var formData = new FormData();
      formData.append("id_hydrant", this.state.idHydrant);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataLokasi: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
  }


  ubahPress(jumlah) {
    let arr = this.state.jumlahPress;
    arr = jumlah;
    this.setState({
      jumlahPress: arr
    });
  }

  tambahJumlah() {
    let arr = this.state.jumlahPress;
    arr++;
    this.setState({
      jumlahPress: arr
    });

    console.log(this.state.jumlahPress);
  }

  kurangJumlah() {
    let arr = this.state.jumlahPress;
    if (arr == 0 || arr == null || arr == undefined) {
    } else {
      arr--;
      this.setState({
        jumlahPress: arr
      });
    }
  }

  validasiFieldTab1(){
    if(this.state.casing==null){
      alert("Masukkan Kondisi Casing")
    } else if (this.state.hose==null){
      alert("Masukkan Kondisi Hose")
    } else if (this.state.nozle==null){
      alert("Masukkan Kondisi Nozle")
    } else if (this.state.kunci==null){
        alert("Masukkan Kondisi Kunci")
    } else {
      this.setState({
        tabNumber:'tab2'
      })
    }
  }

  validasiFieldTab2(){
      if(this.state.coplingkiri==null){
        alert("Masukkan Kondisi Copling Kiri")
      } else if (this.state.coplingkanan==null){
        alert("Masukkan Kondisi Copling Kanan")
      } else if (this.state.tutupkiri==null){
        alert("Masukkan Kondisi Tutup Copling Kiri")
      } else if (this.state.tutupkanan==null){
          alert("Masukkan Kondisi Tutup Copling Kanan")
      } else if (this.state.valvekiri==null){
        alert("Masukkan Kondisi Valve Kiri")
      } else if (this.state.valvekanan==null){
        alert("Masukkan Kondisi Valve Kanan")
      } else if (this.state.valveatas==null){
        alert("Masukkan Kondisi Valve Atas")
      } else {
        this.setState({
          tabNumber:'tab3'
        })
      }
    }

  validasiCreate(){
    if(this.state.body==null) {
        alert("Masukkan Kondisi Body Pilar")
    } else if(this.state.jumlahPress==''){
        alert("Masukkan Jumlah Tekanan")
    } else if(this.state.uri==''){
        alert("Masukkan Foto Temuan")
    } else {
      this.hydrantCreate();
    }
  }

  hydrantCreate(){
    this.setState({
        visibleDialogSubmit:true
    })
      var url = GlobalConfig.SERVERHOST + 'addInspectionHydrant';
      var formData = new FormData();
      formData.append("id_hydrant", this.state.idHydrant);
      formData.append("box_casing", this.state.casing);
      formData.append("box_hose", this.state.hose);
      formData.append("box_nozle", this.state.nozle);
      formData.append("box_kunci", this.state.kunci);
      formData.append("box_note", this.state.notebox);
      formData.append("copling_kiri", this.state.coplingkiri);
      formData.append("copling_kanan", this.state.coplingkanan);
      formData.append("copling_note", this.state.notecopling);
      formData.append("t_copling_kiri", this.state.tutupkiri);
      formData.append("t_copling_kanan", this.state.tutupkanan);
      formData.append("t_copling_note", this.state.notetutup);
      formData.append("valve_kiri", this.state.valvekiri);
      formData.append("valve_kanan", this.state.valvekanan);
      formData.append("valve_atas", this.state.valveatas);
      formData.append("valve_note", this.state.notevalve);
      formData.append("pilar_body", this.state.body);
      formData.append("pilar_note", this.state.notepilar);
      formData.append("temuan", this.state.temuan);
      formData.append("press", this.state.jumlahPress);
      if (this.state.uri!='') {
        formData.append("foto_temuan", {
            uri: this.state.uri,
            name: 'image',
            type: this.state.fileType,
        });
      }
      formData.append("pic_badge", '2103161040');
      formData.append("pic_name", 'Muhamat Zaenal Mahmut');
      formData.append("area", this.state.dataLokasi.area);
      formData.append("unit_kerja", this.state.dataLokasi.unit_kerja);
      formData.append("company", this.state.dataLokasi.company);
      formData.append("plant", this.state.dataLokasi.plant);
      formData.append("qr_code", this.state.dataLokasi.qr_code);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        if (response.status == 200) {
          this.setState({
              visibleDialogSubmit:false
          })
          Alert.alert('Success', 'Create Hydrant Success', [{
            text: 'Okay'
          }])
          this.props.navigation.navigate('InspectionReportHydrant')
        } else {
          this.setState({
              visibleDialogSubmit:false
          })
          Alert.alert('Error', 'Create Hydrant Failed', [{
            text: 'Okay'
          }])
        }
      })
      .catch((error) => {
        this.setState({
            visibleDialogSubmit:false
        })
        Alert.alert('Error', 'Check Your Internet Connection', [{
            text: 'Okay'
        }])
          console.log(error)
      })
  }

  render() {
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            {this.state.tabNumber=='tab1' ? (
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("InspectionReportHydrantDetail")}
              >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          ):this.state.tabNumber=='tab2' ? (
              <Button
                transparent
                onPress={() => this.setState({
                  tabNumber:'tab1'
                })}
              >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            ):(
              <Button
                transparent
                onPress={() => this.setState({
                  tabNumber:'tab2'
                })}
              >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          )}
          </Left>
          <Body style={{ flex:3, alignItems:'center' }}>
            <Title style={styles.textbody}>Inspection Hydrant</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />

      <View style={{ flex: 1 }}>
        <Content style={{ marginTop: 0, backgroundColor:colors.gray }}>
          <View style={{ backgroundColor:colors.gray }}>
              {this.state.tabNumber=='tab1' ? (
              <View>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                  <View style={{ flex: 1, flexDirection: 'row'}}>
                    <View style={{width:'33%', alignItems:'center'}}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{color: colors.green01, fontSize: 20, paddingLeft:8}}
                        />
                        <Text style={{fontSize:10, fontWeight:'bold'}}>Pengecekan Box</Text>
                    </View>
                    <View style={{width:'33%', alignItems:'center'}}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{color:colors.graydar, fontSize: 20, paddingLeft:8}}
                        />
                        <Text style={{fontSize:10, fontWeight:'bold', color:colors.graydar}}>Pengecekan Copling</Text>
                    </View>
                    <View style={{width:'33%', alignItems:'center'}}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{color:colors.graydar, fontSize: 20, paddingLeft:8}}
                        />
                        <Text style={{fontSize:10, fontWeight:'bold', color:colors.graydar}}>Pengecekan Pilar</Text>
                    </View>
                  </View>
                </CardItem>

                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1 }}>
                    <View>
                      <Text style={styles.titleInput}>Casing</Text>
                      <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                              style={{ width: '100%', height:35}}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.casing}
                              onValueChange={(itemValue) => this.setState({casing:itemValue})}>
                              <Picker.Item label="Baik"  value="V" />
                              <Picker.Item label="Kotor"  value="K"/>
                              <Picker.Item label="Tidak Ada"  value="-"/>
                              <Picker.Item label="Rusak"  value="X"/>
                              <Picker.Item label="Seret/Macet/Merembes"  value="I"/>
                          </Picker>
                      </Form>
                    </View>
                  </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1 }}>
                    <View>
                      <Text style={styles.titleInput}>Hose</Text>
                      <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                              style={{ width: '100%', height:35}}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.hose}
                              onValueChange={(itemValue) => this.setState({hose:itemValue})}>
                              <Picker.Item label="Baik"  value="V" />
                              <Picker.Item label="Kotor"  value="K"/>
                              <Picker.Item label="Tidak Ada"  value="-"/>
                              <Picker.Item label="Rusak"  value="X"/>
                              <Picker.Item label="Seret/Macet/Merembes"  value="I"/>
                          </Picker>
                      </Form>
                    </View>
                  </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1 }}>
                    <View>
                      <Text style={styles.titleInput}>Nozle</Text>
                      <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                              style={{ width: '100%', height:35}}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.nozle}
                              onValueChange={(itemValue) => this.setState({nozle:itemValue})}>
                              <Picker.Item label="Baik"  value="V" />
                              <Picker.Item label="Kotor"  value="K"/>
                              <Picker.Item label="Tidak Ada"  value="-"/>
                              <Picker.Item label="Rusak"  value="X"/>
                              <Picker.Item label="Seret/Macet/Merembes"  value="I"/>
                          </Picker>
                      </Form>
                    </View>
                  </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1 }}>
                    <View>
                      <Text style={styles.titleInput}>Kunci</Text>
                      <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                              style={{ width: '100%', height:35}}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.kunci}
                              onValueChange={(itemValue) => this.setState({kunci:itemValue})}>
                              <Picker.Item label="Baik"  value="V" />
                              <Picker.Item label="Kotor"  value="K"/>
                              <Picker.Item label="Tidak Ada"  value="-"/>
                              <Picker.Item label="Rusak"  value="X"/>
                              <Picker.Item label="Seret/Macet/Merembes"  value="I"/>
                          </Picker>
                      </Form>
                    </View>
                  </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                    <View style={{ flex: 1, paddingTop: 5 }}>
                      <View>
                          <Text style={styles.titleInput}>Keterangan</Text>
                      </View>
                      <View>
                          <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={2}
                              bordered value={this.state.notebox}
                              placeholder='Type something here ...'
                              onChangeText={(text) => this.setState({ notebox: text })} />
                      </View>
                    </View>
                </CardItem>

                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                  <View style={{ flex: 1}}>
                  <View style={styles.Contentsave}>
                    <Button
                      block
                      style={{
                        width:'100%',
                        height: 45,
                        marginBottom: 20,
                        borderWidth: 1,
                        backgroundColor: "#00b300",
                        borderColor: "#00b300",
                        borderRadius: 4
                      }}
                      onPress={() => this.validasiFieldTab1()}
                    >
                      <Text style={{color:colors.white}}>Lanjutkan</Text>
                    </Button>
                  </View>
                  </View>
                </CardItem>
              </View>

              ):this.state.tabNumber=='tab2' ? (

              <View>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                <View style={{ flex: 1, flexDirection: 'row'}}>
                  <View style={{width:'33%', alignItems:'center'}}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{color: colors.green01, fontSize: 20, paddingLeft:8}}
                      />
                      <Text style={{fontSize:10, fontWeight:'bold'}}>Pengecekan Box</Text>
                  </View>
                  <View style={{width:'33%', alignItems:'center'}}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{color: colors.green01, fontSize: 20, paddingLeft:8}}
                      />
                      <Text style={{fontSize:10, fontWeight:'bold'}}>Pengecekan Copling</Text>
                  </View>
                  <View style={{width:'33%', alignItems:'center'}}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{color: colors.graydar, fontSize: 20, paddingLeft:8}}
                      />
                      <Text style={{fontSize:10, fontWeight:'bold', color:colors.graydar}}>Pengecekan Pilar</Text>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1, flexDirection: 'column' }}>
                    <View>
                      <Text style={styles.titleInput}>Copling Kiri</Text>
                      <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                              style={{ width: '100%', height:35}}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.coplingkiri}
                              onValueChange={(itemValue) => this.setState({coplingkiri:itemValue})}>
                              <Picker.Item label="Baik"  value="V" />
                              <Picker.Item label="Kotor"  value="K"/>
                              <Picker.Item label="Tidak Ada"  value="-"/>
                              <Picker.Item label="Rusak"  value="X"/>
                              <Picker.Item label="Seret/Macet/Merembes"  value="I"/>
                          </Picker>
                      </Form>
                    </View>
                  </View>
              </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1}}>
                    <View>
                      <Text style={styles.titleInput}>Copling Kanan</Text>
                      <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                              style={{ width: '100%', height:35}}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.coplingkanan}
                              onValueChange={(itemValue) => this.setState({coplingkanan:itemValue})}>
                              <Picker.Item label="Baik"  value="V" />
                              <Picker.Item label="Kotor"  value="K"/>
                              <Picker.Item label="Tidak Ada"  value="-"/>
                              <Picker.Item label="Rusak"  value="X"/>
                              <Picker.Item label="Seret/Macet/Merembes"  value="I"/>
                          </Picker>
                      </Form>
                    </View>
                  </View>
              </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1}}>
                      <View>
                          <Text style={styles.titleInput}>Keterangan Copling</Text>
                      </View>
                      <View>
                          <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={2}
                              bordered value={this.state.notecopling}
                              placeholder='Type something here ...'
                              onChangeText={(text) => this.setState({ notecopling: text })} />
                      </View>
                    </View>
              </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1}}>
                    <View>
                      <Text style={styles.titleInput}>Tutup Copling Kiri</Text>
                      <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                              style={{ width: '100%', height:35}}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.tutupkiri}
                              onValueChange={(itemValue) => this.setState({tutupkiri:itemValue})}>
                              <Picker.Item label="Baik"  value="V" />
                              <Picker.Item label="Kotor"  value="K"/>
                              <Picker.Item label="Tidak Ada"  value="-"/>
                              <Picker.Item label="Rusak"  value="X"/>
                              <Picker.Item label="Seret/Macet/Merembes"  value="I"/>
                          </Picker>
                      </Form>
                    </View>
                  </View>
              </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1}}>
                    <View>
                      <Text style={styles.titleInput}>Tutup Copling Kanan</Text>
                      <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                              style={{ width: '100%', height:35}}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.tutupkanan}
                              onValueChange={(itemValue) => this.setState({tutupkanan:itemValue})}>
                              <Picker.Item label="Baik"  value="V" />
                              <Picker.Item label="Kotor"  value="K"/>
                              <Picker.Item label="Tidak Ada"  value="-"/>
                              <Picker.Item label="Rusak"  value="X"/>
                              <Picker.Item label="Seret/Macet/Merembes"  value="I"/>
                          </Picker>
                      </Form>
                    </View>
                  </View>
              </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1}}>
                      <View>
                          <Text style={styles.titleInput}>Keterangan Tutup Copling</Text>
                      </View>
                      <View>
                          <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={2}
                              bordered value={this.state.notetutup}
                              placeholder='Type something here ...'
                              onChangeText={(text) => this.setState({ notetutup: text })} />
                      </View>
                  </View>
              </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1}}>
                    <View>
                      <Text style={styles.titleInput}>Valve Copling Kiri</Text>
                      <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                              style={{ width: '100%', height:35}}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.valvekiri}
                              onValueChange={(itemValue) => this.setState({valvekiri:itemValue})}>
                              <Picker.Item label="Baik"  value="V" />
                              <Picker.Item label="Kotor"  value="K"/>
                              <Picker.Item label="Tidak Ada"  value="-"/>
                              <Picker.Item label="Rusak"  value="X"/>
                              <Picker.Item label="Seret/Macet/Merembes"  value="I"/>
                          </Picker>
                      </Form>
                    </View>
                  </View>
              </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1}}>
                    <View>
                      <Text style={styles.titleInput}>Valve Copling Kanan</Text>
                      <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                              style={{ width: '100%', height:35}}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.valvekanan}
                              onValueChange={(itemValue) => this.setState({valvekanan:itemValue})}>
                              <Picker.Item label="Baik"  value="V" />
                              <Picker.Item label="Kotor"  value="K"/>
                              <Picker.Item label="Tidak Ada"  value="-"/>
                              <Picker.Item label="Rusak"  value="X"/>
                              <Picker.Item label="Seret/Macet/Merembes"  value="I"/>
                          </Picker>
                      </Form>
                    </View>
                  </View>
              </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1}}>
                    <View>
                      <Text style={styles.titleInput}>Valve Copling Atas</Text>
                      <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                              style={{ width: '100%', height:35}}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.valveatas}
                              onValueChange={(itemValue) => this.setState({valveatas:itemValue})}>
                              <Picker.Item label="Baik"  value="V" />
                              <Picker.Item label="Kotor"  value="K"/>
                              <Picker.Item label="Tidak Ada"  value="-"/>
                              <Picker.Item label="Rusak"  value="X"/>
                              <Picker.Item label="Seret/Macet/Merembes"  value="I"/>
                          </Picker>
                      </Form>
                    </View>
                  </View>
              </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1}}>
                      <View>
                          <Text style={styles.titleInput}>Keterangan Valve</Text>
                      </View>
                      <View>
                          <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={2}
                              bordered value={this.state.notevalve}
                              placeholder='Type something here ...'
                              onChangeText={(text) => this.setState({ notevalve: text })} />
                      </View>
                    </View>
                </CardItem>

              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                <View style={{ flex: 1}}>
                <View style={styles.Contentsave}>
                  <Button
                    block
                    style={{
                      width:'100%',
                      height: 45,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: "#00b300",
                      borderColor: "#00b300",
                      borderRadius: 4
                    }}
                    onPress={() => this.validasiFieldTab2()}
                  >
                    <Text style={{color:colors.white}}>Lanjutkan</Text>
                  </Button>
                </View>
                </View>
              </CardItem>
              </View>


            ):(


              <View>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                <View style={{ flex: 1, flexDirection: 'row'}}>
                  <View style={{width:'33%', alignItems:'center'}}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{color: colors.green01, fontSize: 20, paddingLeft:8}}
                      />
                      <Text style={{fontSize:10, fontWeight:'bold'}}>Pengecekan Box</Text>
                  </View>
                  <View style={{width:'33%', alignItems:'center'}}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{color: colors.green01, fontSize: 20, paddingLeft:8}}
                      />
                      <Text style={{fontSize:10, fontWeight:'bold'}}>Pengecekan Copling</Text>
                  </View>
                  <View style={{width:'33%', alignItems:'center'}}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{color: colors.green01, fontSize: 20, paddingLeft:8}}
                      />
                      <Text style={{fontSize:10, fontWeight:'bold'}}>Pengecekan Pilar</Text>
                  </View>
                </View>
              </CardItem>

              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1, flexDirection: 'column' }}>
                    <View>
                      <Text style={styles.titleInput}>Body</Text>
                      <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                              style={{ width: '100%', height:35}}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.body}
                              onValueChange={(itemValue) => this.setState({body:itemValue})}>
                              <Picker.Item label="Baik"  value="V" />
                              <Picker.Item label="Kotor"  value="K"/>
                              <Picker.Item label="Tidak Ada"  value="-"/>
                              <Picker.Item label="Rusak"  value="X"/>
                              <Picker.Item label="Seret/Macet/Merembes"  value="I"/>
                          </Picker>
                      </Form>
                    </View>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                      <View>
                          <Text style={styles.titleInput}>Keterangan</Text>
                      </View>
                      <View>
                          <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={2}
                              bordered value={this.state.notepilar}
                              placeholder='Type something here ...'
                              onChangeText={(text) => this.setState({ notepilar: text })} />
                      </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                    <View style={{flex: 1, flexDirection: 'column'}}>
                        <View>
                            <Text style={styles.titleInput}>Press</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: "row", paddingTop: 5}}>
                            <Button onPress={() => this.kurangJumlah()} style={styles.btnQTYLeft}>
                                <Icon
                                    name="ios-arrow-back"
                                    style={styles.facebookButtonIconQTY}
                                />
                            </Button>

                            <View style={{width:50, height:30, marginTop: 10, backgroundColor: colors.white}}>
                                <Input
                                    style={{
                                    height:30,
                                    marginTop:0,
                                    fontSize:9,
                                    textAlign:'center'}}
                                    value={this.state.jumlahPress + ""}
                                    keyboardType='numeric'
                                    onChangeText={text => this.ubahPress(text)}
                                />
                            </View>
                            <Button onPress={() => this.tambahJumlah()} style={styles.btnQTYRight}>
                                <Icon
                                    name="ios-arrow-forward"
                                    style={styles.facebookButtonIconQTY}
                                />
                            </Button>
                        </View>
                    </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1}}>
                      <View>
                          <Text style={styles.titleInput}>Temuan</Text>
                      </View>
                      <View>
                          <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={2}
                              bordered value={this.state.temuan}
                              placeholder='Type something here ...'
                              onChangeText={(text) => this.setState({ temuan: text })} />
                      </View>
                    </View>
                </CardItem>

                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                    <View style={{ flex: 1}}>
                        <View style={{paddingBottom:5}}>
                            <Text style={styles.titleInput}>Upload Foto</Text>
                        </View>
                        <View>
                            <Ripple
                            style={{
                                flex: 2,
                                justifyContent: "center",
                                alignItems: "center"
                            }}
                            rippleSize={176}
                            rippleDuration={600}
                            rippleContainerBorderRadius={15}
                            onPress={this.pickImageHandler}
                            rippleColor={colors.accent}
                            >
                            <View style={styles.placeholder}>
                                <Image source={{uri :this.state.pickedImage}} style={styles.previewImage}/>
                            </View>
                            </Ripple>
                        </View>
                    </View>
                </CardItem>

              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                <View style={{ flex: 1}}>
                <View style={styles.Contentsave}>
                  <Button
                    block
                    style={{
                      width:'100%',
                      height: 45,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: "#00b300",
                      borderColor: "#00b300",
                      borderRadius: 4
                    }}
                    onPress={() => this.validasiCreate()}
                  >
                    <Text style={{color:colors.white}}>SUBMIT</Text>
                  </Button>
                </View>
                </View>
              </CardItem>
              </View>
            )}
          </View>
        </Content>
      </View>
      <View style={{ width: 270, position: "absolute" }}>
          <Dialog
              visible={this.state.visibleDialogSubmit}
              dialogTitle={<DialogTitle title="Create Inspection Report Hydrant .." />}
          >
              <DialogContent>
              {<ActivityIndicator size="large" color="#330066" animating />}
              </DialogContent>
          </Dialog>
      </View>
      <CustomFooter navigation={this.props.navigation} menu='FireSystem' />
    </Container>

    );
  }
}

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Moment from "moment";

class InspectionReportHydrantDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      dataDetail: [],
      isLoading: true,
      dataLokasi:[],
      listReportHydrant: [],
      listParameter: [],
      visibleDialogSubmit: false,
      idLokasi: "",
      idHydrant:"",
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  componentDidMount() {
    AsyncStorage.getItem("idLokasi").then(idLokasi => {
        this.setState({
          idHydrant: idLokasi,
          isLoading: false,
        });
        this.loadLokasi();
    });
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
      }
    );
  }

  loadLokasi() {
    this.setState({ visibleDialog: false, isLoading: true });
      const url = GlobalConfig.SERVERHOST + "getLokasiHydrant";
      var formData = new FormData();
      formData.append("id_hydrant", this.state.idHydrant);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataLokasi: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
  }

  loadData() {
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + "detailInspectionHydrant";
      var formData = new FormData();
      formData.append("id_hydrant", this.state.idHydrant);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listReportHydrant: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  navigateToScreen(route, listReportHydrant) {
    AsyncStorage.setItem("listReportHydrant", JSON.stringify(listReportHydrant)).then(() => {
        this.props.navigation.navigate(route);
    });
  }

  navigateToScreenCreate(route, idHydrant) {
    AsyncStorage.setItem("idHydrant", idHydrant).then(() => {
        this.props.navigation.navigate(route);
        // alert(JSON.stringify(idHydrant))
      }
    );
  }

  render() {
    var that = this;
    var dateNow = Moment().format('YYYY-MM-DD');
    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
        <Left style={{ flex: 1 }}>
          <Button
            transparent
            onPress={() =>
              that.props.navigation.navigate("InspectionReportHydrant")
            }
          >
            <Icon
              name="ios-arrow-back"
              size={20}
              style={styles.facebookButtonIconOrder2}
            />
          </Button>
        </Left>
        <Body style={{ flex: 3, alignItems: "center" }}>
          <Title style={styles.textbody}>
            {this.state.dataLokasi.area}
          </Title>
        </Body>
        <Right style={{ flex: 1 }}>
            <View style={{ flex: 0, flexDirection: "row" }}>
              <Button
                transparent
                onPress={() =>
                  this.navigateToScreen(
                    "InspectionReportHydrantUpdate",
                    this.state.listReportHydrant
                  )
                }
              >
                <Icon
                  name="ios-create"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
              <Button
                transparent
                onPress={() =>
                  this.konfirmasideleteApar(
                    listReport.UK_TEXT,
                    listReport.ID
                  )
                }
              >
                <Icon
                  name="ios-history"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </View>
        </Right>
      </Header>
      <StatusBar
        backgroundColor={colors.green03}
        barStyle="light-content"
      />
      <ScrollView>
        <Content>
          <CardItem style={{ borderRadius: 0, marginTop: 5 }}>
            <View>
              <Text style={{ fontSize: 10 }}>Area</Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.dataLokasi.area}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 5 }}>
                Unit Kerja
              </Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.dataLokasi.unit_kerja}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 5 }}>Last Inspection</Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.dataLokasi.last_inspection}
              </Text>
            </View>
          </CardItem>

          <CardItem style={{ borderRadius: 0 }}>
            <View>
              <Text style={{ fontSize: 10 }}>Status</Text>
              {dateNow <= this.state.dataLokasi.exp_inspection ? (
                <Text
                  style={{
                    fontSize: 10,
                    fontWeight: "bold",
                    color: colors.green01
                  }}
                >
                  Sudah Diperiksa
                </Text>
              ) : (
                <Text
                  style={{
                    fontSize: 10,
                    fontWeight: "bold",
                    color: "#FF0101"
                  }}
                >
                  Belum Diperiksa
                </Text>
              )}
            </View>
          </CardItem>
          {dateNow <= this.state.dataLokasi.exp_inspection ? (
            <View style={{}}>
              <CardItem style={{ borderRadius: 0 }}>
                <View>
                  <Text style={{ fontSize: 10 }}>Tanggal Periksa</Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listReportHydrant.inspection_date}
                  </Text>
                  <Text style={{ fontSize: 10, paddingTop: 5 }}>
                    Petugas Periksa
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listReportHydrant.pic_name}
                  </Text>
                </View>
              </CardItem>

              <CardItem style={{ borderRadius: 0 }}>
                <View style={{ flex: 1, flexDirection: "column" }}>
                  <View>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Pengecekan BOX
                    </Text>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      paddingTop: 5
                    }}
                  >
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Casing</Text>
                      {this.state.listReportHydrant.box_casing == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_casing == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_casing == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_casing == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_casing == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Hose</Text>
                      {this.state.listReportHydrant.box_hose == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_hose == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_hose == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_hose == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_hose == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Nozle</Text>
                      {this.state.listReportHydrant.box_nozle == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_nozle == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_nozle == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_nozle == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_nozle == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Kunci</Text>
                      {this.state.listReportHydrant.box_kunci == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_kunci == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_kunci == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_kunci == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.box_kunci == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                  </View>

                  <View style={{ paddingTop: 5 }}>
                    <Text style={{ fontSize: 10 }}>Keterangan</Text>
                    {this.state.listReportHydrant.box_note != null ? (
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {this.state.listReportHydrant.box_note}
                      </Text>
                    ) : (
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {" "}
                        -
                      </Text>
                    )}
                  </View>
                </View>
              </CardItem>

              <CardItem style={{ borderRadius: 0 }}>
                <View style={{ flex: 1, flexDirection: "column" }}>
                  <View>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Pengecekan Copling
                    </Text>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      paddingTop: 5
                    }}
                  >
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Copling Kiri</Text>
                      {this.state.listReportHydrant.copling_kiri == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kiri == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kiri == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kiri == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kiri == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Copling Kanan</Text>
                      {this.state.listReportHydrant.copling_kanan == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kanan == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kanan == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kanan == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.copling_kanan == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Tutup Kiri</Text>
                      {this.state.listReportHydrant.t_copling_kiri == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kiri == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kiri == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kiri == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kiri == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Tutup Kanan</Text>
                      {this.state.listReportHydrant.t_copling_kanan == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kanan == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kanan == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kanan == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.t_copling_kanan == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                  </View>

                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      paddingTop: 5
                    }}
                  >
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Valve Kiri</Text>
                      {this.state.listReportHydrant.valve_kiri == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kiri == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kiri == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kiri == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kiri == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Valve Kanan</Text>
                      {this.state.listReportHydrant.valve_kanan == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kanan == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kanan == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kanan == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_kanan == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                    <View style={{ width: "25%" }}>
                      <Text style={{ fontSize: 10 }}>Valve Atas</Text>
                      {this.state.listReportHydrant.valve_atas == "V" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Baik
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_atas == "K" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Kotor
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_atas == "-" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Tidak Ada
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_atas == "X" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Rusak
                        </Text>
                      )}
                      {this.state.listReportHydrant.valve_atas == "I" && (
                        <Text
                          style={{ fontSize: 10, fontWeight: "bold" }}
                        >
                          Seret/Macet/Merembes
                        </Text>
                      )}
                    </View>
                  </View>

                  <View style={{ paddingTop: 5 }}>
                    <Text style={{ fontSize: 10 }}>Keterangan</Text>
                    {this.state.listReportHydrant.box_note != null ? (
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {this.state.listReportHydrant.box_note}
                      </Text>
                    ) : (
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {" "}
                        -
                      </Text>
                    )}
                  </View>
                </View>
              </CardItem>

              <CardItem style={{ borderRadius: 0 }}>
                <View>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    Pengecekan Pilar
                  </Text>
                  <Text style={{ fontSize: 10, paddingTop: 5 }}>
                    Body
                  </Text>
                  {this.state.listReportHydrant.pilar_body == "V" && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Baik
                    </Text>
                  )}
                  {this.state.listReportHydrant.pilar_body == "K" && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Kotor
                    </Text>
                  )}
                  {this.state.listReportHydrant.pilar_body == "-" && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Tidak Ada
                    </Text>
                  )}
                  {this.state.listReportHydrant.pilar_body == "X" && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Rusak
                    </Text>
                  )}
                  {this.state.listReportHydrant.pilar_body == "I" && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Seret/Macet/Merembes
                    </Text>
                  )}
                  <Text style={{ fontSize: 10, paddingTop: 5 }}>
                    Keterangan
                  </Text>
                  {this.state.listReportHydrant.pilar_note != null ? (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.listReportHydrant.pilar_note}
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {" "}
                      -
                    </Text>
                  )}
                </View>
              </CardItem>

              <CardItem style={{ borderRadius: 0 }}>
                <View>
                  <Text style={{ fontSize: 10 }}>Press</Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.listReportHydrant.press}
                  </Text>
                  <Text style={{ fontSize: 10, paddingTop: 5 }}>
                    Temuan
                  </Text>
                  {this.state.listReportHydrant.temua != null ? (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.listReportHydrant.temua}
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {" "}
                      -
                    </Text>
                  )}
                </View>
              </CardItem>
              <CardItem>
                <View style={{ backgroundColor: '#eee', width: '100%', height: 200, borderRadius: 10, marginBottom: 80 }}>
                  <Image
                    style={{
                      width: "100%",
                      height: "100%",
                      borderRadius: 10,
                      marginBottom: 20
                    }}
                    source={{
                      uri:
                        GlobalConfig.IMAGEHOST + 'InspectionHydrant/' + this.state.listReportHydrant.foto_temuan
                    }}
                  />
                </View>
              </CardItem>
            </View>
          ):(
            <Button
              block
              style={{
                height: 45,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 20,
                borderWidth: 1,
                backgroundColor: "#00b300",
                borderColor: "#00b300",
                borderRadius: 4
              }}
              onPress={() =>
                this.navigateToScreenCreate("InspectionReportHydrantCreate", this.state.idHydrant)
              }
            >
              <Text
                style={{
                  fontSize: 14,
                  fontWeight: "bold",
                  color: colors.white
                }}
              >
                Lakukan Pemeriksaan
              </Text>
            </Button>
          )}
            <View style={{ width: 270, position: "absolute" }}>
              <Dialog
                visible={this.state.visibleDialogSubmit}
                dialogTitle={
                  <DialogTitle title="Delete Inspection Report Alarm .." />
                }
              >
                <DialogContent>
                  {
                    <ActivityIndicator
                      size="large"
                      color="#330066"
                      animating
                    />
                  }
                </DialogContent>
              </Dialog>
            </View>
          </Content>
        </ScrollView>
      </Container>
    );
  }
}

export default InspectionReportHydrantDetail;

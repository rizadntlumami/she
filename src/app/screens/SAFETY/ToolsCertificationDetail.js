import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input,
    Thumbnail
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewHeader from "../../components/ListViewHeader";
import ListViewDetail from "../../components/ListViewDetail";

class ToolsCertificationDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      dataDetail: [],
      isLoading: true,
      detailTools:[],
      visibleDialogSubmit:false
    };
  }

  static navigationOptions = {
      header: null
  };

  _renderItem = ({ item }) => (
      <ListItem data={item}></ListItem>
  )

  navigateToScreen(route, detailTools) {
    AsyncStorage.setItem('listAlat', JSON.stringify(detailTools)).then(() => {
        this.props.navigation.navigate(route);
    })
  }

  componentDidMount() {
      AsyncStorage.getItem('listAlat').then((listDataTools) => {
          this.setState({ detailTools: JSON.parse(listDataTools) });
          this.setState({ isLoading: false });
          console.log(this.state.detailTools);
      })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        //this.loadData(id_Unsafe);
      });

  }

  konfirmasideleteTools() {
    Alert.alert(
      'Apakah Anda Yakin Ingin Menghapus', this.state.detailTools.NO_BUKU,
      [
        {text: 'Yes', onPress:()=> this.deleteTools()},
        {text: 'Cancel'},
      ],
      { cancelable:false}
    )
  }

  deleteTools() {
    this.setState({
      visibleDialogSubmit:true
    })
    AsyncStorage.getItem('token').then((value) => {
      // alert(JSON.stringify(value));
      const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/sertification/delete/alat';
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID", this.state.detailTools.ID);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit:false
            })
            Alert.alert('Success', 'Delete Success', [{
              text: 'Oke',
            }])
            this.props.navigation.navigate("ToolsCertification")
          } else {
            this.setState({
              visibleDialogSubmit:false
            })
            Alert.alert('Error', 'Delete Failed', [{
              text: 'Oke'
            }])
          }
        })
        .catch((error)=>{
          console.log(error)
          this.setState({
            visibleDialogSubmit:false
          })
          Alert.alert('Error', 'Delete Failed', [{
            text: 'Oke'
          }])
        })
    })
  }


  render() {
    var listTools;
    var listDetailTools;
    return (
      <Container style={styles.wrapper}>
          <Header style={styles.header}>
            <Left style={{flex:1}}>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("ToolsCertification")}
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Left>
            <Body style={{flex:3,alignItems:'center'}}>
              <Title style={styles.textbody}>{this.state.detailTools.NO_BUKU}</Title>
            </Body>
            <Right style={{flex:1}}>
              <Button
                transparent
                onPress={() => this.navigateToScreen('ToolsCertificationUpdate', this.state.detailTools)}
              >
                <Icon
                  name="ios-create"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
              <Button
                transparent
                onPress={() => this.konfirmasideleteTools()}
              >
                <Icon
                  name="ios-trash"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Right>
          </Header>
          <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
          <Content style={{marginTop:5}}>

          <View style={{}}>
          <CardItem style={{ borderRadius: 0}}>
              <View style={{ flex: 1}}>
                <Text style={{fontSize:10, paddingBottom:5, fontWeight:'bold'}}>Foto Buku</Text>
                <View style={styles.placeholder}>
                  {this.state.detailTools.FILE_BUKU == null ? (
                      <Image style={styles.previewImage} source={require("../../../assets/images/nopic.png")} />
                  ):(
                      <Image style={styles.previewImage} source={{uri : GlobalConfig.SERVERHOST + 'api' + this.state.detailTools.FILE_BUKU+ '?' + new Date()}}/>
                  )}
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0}}>
              <View style={{ flex: 1}}>
                <Text style={{fontSize:10, paddingBottom:5, fontWeight:'bold'}}>Foto Aktual</Text>
                <View style={styles.placeholder}>
                  {this.state.detailTools.FILE_AKTUAL == null ? (
                      <Image style={styles.previewImage} source={require("../../../assets/images/nopic.png")} />
                  ):(
                  <Image style={styles.previewImage} source={{uri : GlobalConfig.SERVERHOST + 'api' + this.state.detailTools.FILE_AKTUAL + '?' + new Date()}}/>
                  )}
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:5, }}>
              <View style={{ flex: 1}}>
                <Text style={{fontSize:10}}>Nomor Buku</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailTools.NO_BUKU}</Text>
                <Text style={{fontSize:10, paddingTop:10}}>Peralatan</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailTools.EQUIP_NAME}</Text>
                <Text style={{fontSize:10, paddingTop:10}}>Mulai Sertifikasi</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailTools.UJI_AWAL}</Text>
                <Text style={{fontSize:10, paddingTop:10}}>Tanggal Riksa</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailTools.UJI_ULANG}</Text>
                <Text style={{fontSize:10, paddingTop:10}}>Data Teknis</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailTools.DATA_TEKNIS}</Text>
                <Text style={{fontSize:10, paddingTop:10}}>Nomor Pengesahan</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailTools.NOMOR_PENGESAHAN}</Text>
                <Text style={{fontSize:10, paddingTop:10}}>Unit Kerja</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailTools.UK_TEXT}</Text>
                <Text style={{fontSize:10, paddingTop:10}}>Lokasi</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailTools.LOKASI}</Text>
              </View>
            </CardItem>
            
          </View>
          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
              dialogTitle={<DialogTitle title="Deleting Tools Certification .." />}
            >
              <DialogContent>
                {<ActivityIndicator size="large" color="#330066" animating />}
              </DialogContent>
            </Dialog>
          </View>
          </Content>
      </Container>
    );
  }
}

export default ToolsCertificationDetail;

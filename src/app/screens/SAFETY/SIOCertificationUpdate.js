import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TextInput,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Item
} from "native-base";

import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton
  } from "react-native-popup-dialog";
import ImagePicker from "react-native-image-picker";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from 'react-native-datepicker';
import Ripple from "react-native-material-ripple";
import Moment from 'moment'

class ListItemPegawai extends React.PureComponent {
    render() {
        return (
            <View style={{ backgroundColor: "#FEFEFE" }}>
                <Ripple
                    style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() =>
                        this.props.setSelectedPegawaiLabel(this.props.data)
                    }
                    rippleColor={colors.accent}
                >
                    <CardItem
                        style={{
                            borderRadius: 0,
                            marginTop: 4,
                            backgroundColor: colors.gray
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                marginTop: 4,
                                backgroundColor: colors.gray
                            }}
                        >
                            <View >
                                <Text style={{ fontSize: 12 }}>{this.props.data.mk_nopeg} - {this.props.data.mk_nama}</Text>
                            </View>
                        </View>
                    </CardItem>
                </Ripple>
            </View>
        );
    }
}

export default class SIOCertificationUpdate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            dataHeader: [],
            isLoading: true,
            detailSIO:[],
            //listKaryawan:[],
            //listUnitKerja: [],
            listGroup: [],
            detailSIO: '',
            karyawan: '',
            unitKerja: '',
            group: '',
            noLisensi: '',
            kelas: '',
            masaAwal: '',
            time: '',
            masaBerlaku: '',
            foto: '',
            pickedImage: '',
            uri: '',
            fileName: '',

            visibleLoadingPegawai: false,
            visibleSearchListPegawai: false,
            selectedPegawaiLabel: '',
            searchWordPegawai: '',
            pegawaiName: '',
            pegawaiUnitKerja: '',

            visibleDialogSubmit:false
        };
    }

    static navigationOptions = {
        header: null
    };

    reset = () => {
        this.setState({
            pickedImage: null
        });
    }

    pickImageHandler = () => {
        ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
            if (res.didCancel) {
                console.log("User cancelled!");
            } else if (res.error) {
                console.log("Error", res.error);
            } else {
                this.setState({
                    pickedImage: res.uri,
                    uri: res.uri,
                    //fileName: res.fileName,
                    fileType: res.type
                });
                //console.log(res.type)
            }
        });
    }

    resetHandler = () => {
        this.reset();
    }

    _renderItem = ({ item }) => (
        <ListItem data={item}></ListItem>
    )

    componentDidMount() {
      AsyncStorage.getItem('listSIO').then((detailSIO) => {
        var momentObj = Moment(JSON.parse(detailSIO).MASA_AWAL, 'DD-MMM-YYYY');
        var momentString = momentObj.format('YYYY-MM-DD')
        this.setState({
          detailSIO: JSON.parse(detailSIO),
          pegawaiBadge: JSON.parse(detailSIO).BADGE,
          pegawaiName: JSON.parse(detailSIO).NAMA,
          pegawaiUnitKerja: JSON.parse(detailSIO).UNIT_KERJA_TXT,
          group: JSON.parse(detailSIO).GRUP,
          noLisensi: JSON.parse(detailSIO).NO_LISENSI,
          kelas: JSON.parse(detailSIO).KELAS,
          masaAwal: momentString,
          time: JSON.parse(detailSIO).TIME_CERTIFICATION,
          masaBerlaku: JSON.parse(detailSIO).MASA_BERLAKU,
          pickedImage: GlobalConfig.SERVERHOST + 'api' + JSON.parse(detailSIO).FOTO,
        });
      })
        //this.loadKaryawan();
        //this.loadUnitKerja();
        this.loadGroup();
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        this.loadData();
      });
    }

    loadData() {

    }

    onChangePegawai(text) {
        this.setState({
            searchWordPegawai: text
        })
        this.setState({
            visibleLoadingPegawai: true
        })
        AsyncStorage.getItem('token').then((value) => {

            const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/employee';
            var formData = new FormData();
            formData.append("token", value)
            formData.append("search", this.state.searchWordPegawai)
            formData.append("limit", 10)
            formData.append("plant", null)
            fetch(url, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                body: formData
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        visibleLoadingPegawai: false
                    })
                    this.setState({
                        listPegawai: responseJson,
                        listPegawaiMaster: responseJson,
                        isloading: false
                    });
                })
                .catch((error) => {
                    this.setState({
                        visibleLoadingPegawai: false
                    })
                    console.log(error)
                })
        })
        // this.setState({
        //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
        // })
    }

    onClickSearchPegawai() {
        console.log('masuk')
        if (this.state.visibleSearchListPegawai) {
            this.setState({
                visibleSearchListPegawai: false
            })
        } else {
            this.setState({
                visibleSearchListPegawai: true
            })
        }
    }

    setSelectedPegawaiLabel(pegawai) {
        this.setState({
            pegawaiBadge: pegawai.mk_nopeg,
            pegawaiName: pegawai.mk_nama,
            pegawaiUnitKerja: pegawai.muk_nama,
            pegawaiKodeUnit: pegawai.muk_kode,
            visibleSearchListPegawai: false,
        })
    }

    _renderItemPegawai= ({ item }) => <ListItemPegawai data={item} setSelectedPegawaiLabel={text => this.setSelectedPegawaiLabel(text)} />;

      loadGroup(){
        AsyncStorage.getItem('token').then((value) => {
            const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/sertification/get_sio_group';
            var formData = new FormData();
            formData.append("token", value)

            fetch(url, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                body: formData
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        listGroup: responseJson.data,
                        isloading: false
                    });
                })
                .catch((error) => {
                    console.log(error)
                })
        })
      }

      updateSIOSetRiksa(){
        if (this.state.pegawaiBadge == null) {
            alert('Masukkan Pemegang');
        }
        else if (this.state.kelas == null) {
            alert('Masukkan Kelas');
        }
        else if (this.state.group == null) {
            alert('Masukkan Jenis Lisensi');
        }
        else if (this.state.noLisensi == null) {
            alert('Masukkan Nomor Lisensi');
        }
        else if (this.state.masaAwal == null) {
            alert('Masukkan Tanggal Mulai Sertifikasi');
        }
        else if (this.state.time == null) {
            alert('Masukkan Waktu');
        }
        else {
          var masaAwal = this.state.masaAwal;
          var stringMasaAwal = masaAwal.substring(0, 4);
          var stringMasaAkhir = masaAwal.substring(4, 10);
          var date = parseInt(stringMasaAwal)
          var range = parseInt(this.state.time)
          let dateRiksa = date + range
          this.setState({
              masaBerlaku : dateRiksa + stringMasaAkhir
          })
          this.updateSIO()
        }
      }

    updateSIO() {
            this.setState({
                visibleDialogSubmit:true
            })
            AsyncStorage.getItem("token").then(value => {
                var url = GlobalConfig.SERVERHOST + "api/v_mobile/safety/sertification/update/sio";
                var formData = new FormData();
                formData.append("token", value);
                formData.append("ID", this.state.detailSIO.ID);
                formData.append("BADGE", this.state.pegawaiBadge);
                formData.append("NAMA", this.state.pegawaiName);
                formData.append("UNIT_KERJA", this.state.pegawaiKodeUnit);
                formData.append("UNIT_KERJA_TXT", this.state.pegawaiUnitKerja);
                formData.append("GRUP", this.state.group);
                formData.append("NO_LISENSI", this.state.noLisensi);
                formData.append("KELAS", this.state.kelas);
                formData.append("MASA_AWAL", this.state.masaAwal);
                formData.append("TIME_CERTIFICATION", this.state.time);
                formData.append("MASA_BERLAKU", this.state.masaBerlaku);
                if (this.state.uri!='') {
                formData.append("FOTO", {
                    uri: this.state.uri,
                    name: 'image',
                    type: this.state.fileType,
                });
                }
                fetch(url, {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    },
                    method: "POST",
                    body: formData
                })
                    .then(response => response.json())
                    .then(response => {
                        if (response.status == 200) {
                            this.setState({
                                visibleDialogSubmit:false
                            })
                            Alert.alert('Success', 'Update SIO Certification Success', [{
                                text: 'Okay'
                            }])
                            this.props.navigation.navigate('SIOCertification')
                        } else {
                            this.setState({
                                visibleDialogSubmit:false
                            })
                            Alert.alert('Error', 'Update SIO Certification Failed', [{
                                text: 'Okay'
                            }])
                        }
                    })
                    .catch((error)=>{
                        console.log(error)
                        this.setState({
                            visibleDialogSubmit:false
                        })
                        Alert.alert('Error', 'Update SIO Certification Failed', [{
                            text: 'Okay'
                        }])
                    })
            })
    };

    render() {

          let listGroup = this.state.listGroup.map( (s, i) => {
            return <Picker.Item key={i} value={s.NICKNAME} label={s.NAME} />
          });

        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left style={{flex:1}}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("SIOCertificationDetail")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body style={{flex:4,alignItems:'center'}}>
                        <Title style={styles.textbody}>Update {this.state.pegawaiName}</Title>
                    </Body>
                    
                        <Right style={{flex:1}}></Right>
                    

                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                <View style={{ flex: 1 }}>
                    <Content style={{ marginTop: 0 }}>
                        <View style={{ backgroundColor: '#FEFEFE' }}>
                        <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                            <View style={{ flex: 1}}>
                                <View style={{paddingBottom:5}}>
                                  <Text style={styles.titleInput}>Upload Foto</Text>
                                </View>
                                <View>
                                  <Ripple
                                    style={{
                                      flex: 2,
                                      justifyContent: "center",
                                      alignItems: "center"
                                    }}
                                    rippleSize={176}
                                    rippleDuration={600}
                                    rippleContainerBorderRadius={15}
                                    onPress={this.pickImageHandler}
                                    rippleColor={colors.accent}
                                  >
                                  <View style={styles.placeholder}>
                                      <Image source={{uri :this.state.pickedImage + '?' + new Date()}} style={styles.previewImage}/>
                                  </View>
                                  </Ripple>
                                </View>
                            </View>
                        </CardItem>
                        <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                            <View style={{ flex: 1 }}>
                                <View>
                                    <Text style={styles.titleInput}>Pemegang *</Text>
                                </View>
                                <View style={{ flex: 1, flexDirection: 'column' }}>
                                    <Button
                                        block
                                        style={{ justifyContent: "flex-start", flex: 1, borderRadius:5, borderWidth: 0, paddingLeft: 10, marginRight: 5, marginLeft: 5, backgroundColor: colors.gray, height: 35 }}
                                        onPress={() => this.onClickSearchPegawai()}>
                                        <Text style={{ fontSize: 12 }}>{this.state.pegawaiBadge}</Text>
                                    </Button>
                                </View>
                                {this.state.visibleSearchListPegawai &&
                                    <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.gray, margin: 5, borderColor: "#E6E6E6", }}>
                                        <View>
                                            <Textarea value={this.state.searchWordPegawai} style={{ marginLeft: 5, marginRight: 5, fontSize: 11 }} bordered rowSpan={1.5} onChangeText={(text) => this.onChangePegawai(text)} placeholder='Ketik nama karyawan' />
                                        </View>
                                        <FlatList
                                            data={this.state.listPegawai}
                                            renderItem={this._renderItemPegawai}
                                            keyExtractor={(item, index) => index.toString()}
                                        />
                                    </View>
                                }
                            </View>
                        </CardItem>
                        <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                            <View style={{ flex: 1}}>
                                <View>
                                    <Text style={styles.titleInput}>Kelas *</Text>
                                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                                        <Picker
                                            mode="dropdown"
                                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                                            style={{ width: '100%', height: 35}}
                                            placeholder="Kelas"
                                            placeholderStyle={{ color: "#bfc6ea" }}
                                            placeholderIconColor="#007aff"
                                            selectedValue={this.state.kelas}
                                            onValueChange={(itemValue) => this.setState({ kelas: itemValue })}>
                                            <Picker.Item label="I (Satu)" value="1" />
                                            <Picker.Item label="II (Dua)" value="2" />
                                            <Picker.Item label="III (Tiga)" value="3" />
                                        </Picker>
                                    </Form>
                                </View>
                            </View>
                        </CardItem>
                        <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                            <View style={{ flex: 1 }}>
                                <View>
                                    <Text style={styles.titleInput}>Unit Kerja *</Text>
                                </View>
                                <View>
                                    <TextInput
                                        style={{ height:40, borderRadius: 5, marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11,borderWidth:1 ,borderColor:colors.lightGray}}
                                        onChangeText={(text) => this.setState({pegawaiUnitKerja:text})}
                                        value={this.state.pegawaiUnitKerja}
                                        bordered
                                        editable={false}
                                        placeholder='Unit Kerja'
                                    />
                                    {/* <Textarea editable={this.state.selectedStatVict == "1" ? false : true} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.saksiPosText} placeholder='Type Something ...' onChangeText={(text) => this.setState({ saksiPosText: text })} /> */}
                                </View>
                            </View>
                        </CardItem>
                        <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                          <View style={{ flex: 1}}>
                            <View>
                              <Text style={styles.titleInput}>Jenis Lisensi *</Text>
                              <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                                  <Picker
                                      mode="dropdown"
                                      iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                                      style={{ width: '100%', height: 35}}
                                      placeholder="Select Lisensi ..."
                                      placeholderStyle={{ color: "#bfc6ea" }}
                                      placeholderIconColor="#007aff"
                                      selectedValue={this.state.group}
                                      onValueChange={(itemValue) => this.setState({group:itemValue})}>
                                      <Picker.Item label="Choose Lisensi..."  value=""/>
                                        {listGroup}
                                  </Picker>
                              </Form>
                            </View>
                          </View>
                        </CardItem>
                        <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                            <View style={{ flex: 1 }}>
                                <View>
                                    <Text style={styles.titleInput}>Nomor Lisensi</Text>
                                </View>
                                <View>
                                    <Textarea style={{
                                        marginLeft: 5,
                                        marginRight: 5,
                                        marginBottom: 5,
                                        borderRadius: 5,
                                        fontSize: 11 }}
                                        rowSpan={2}
                                        bordered value={this.state.noLisensi}
                                        placeholder='Type No Lisensi ...'
                                        onChangeText={(text) => this.setState({ noLisensi: text })} />
                                </View>
                            </View>
                        </CardItem>
                        <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                          <View style={{ flex: 1, flexDirection: 'row'}}>
                            <View style={{width: '50%'}}>
                              <Text style={styles.titleInput}>Tanggal Mulai Sertifikat *</Text>
                              <DatePicker
                                  style={{ width: '100%', fontSize:10, borderRadius:20}}
                                  date={this.state.masaAwal}
                                  mode="date"
                                  placeholder="Choose Date ..."
                                  format="YYYY-MM-DD"
                                  minDate="2018-01-01"
                                  maxDate="2050-12-31"
                                  confirmBtnText="Confirm"
                                  cancelBtnText="Cancel"
                                  iconSource='{this.timeIcon}'
                                  customStyles={{
                                    dateInput: {
                                      marginLeft: 5, marginRight:5, height: 35, borderRadius:5, fontSize:10, borderWidth:1, borderColor:"#E6E6E6"
                                    },
                                    dateIcon: {
                                      position: 'absolute',
                                      left: 0,
                                      top: 5,
                                    },
                                  }}
                                  onDateChange={(time) => { this.setState({ masaAwal: time }) }}
                              />
                            </View>
                            <View style={{width: '50%'}}>
                              <Text style={{fontSize: 10, paddingBottom:2}}>Waktu *</Text>
                              <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                                  <Picker
                                      mode="dropdown"
                                      iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                                      style={{ width: '100%', height: 35 }}
                                      placeholder="Choose Time ..."
                                      placeholderStyle={{ color: "#bfc6ea" }}
                                      placeholderIconColor="#007aff"
                                      selectedValue={this.state.time}
                                      onValueChange={(itemValue) => this.setState({ time: itemValue })}>
                                      <Picker.Item label="1 Tahun" value="1" />
                                      <Picker.Item label="2 tahun" value="2" />
                                      <Picker.Item label="3 tahun" value="3" />
                                      <Picker.Item label="4 tahun" value="4" />
                                      <Picker.Item label="5 tahun" value="5" />
                                  </Picker>
                              </Form>
                            </View>
                          </View>
                        </CardItem>
                        
                        <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                          <View style={{ flex: 1}}>
                          <View style={styles.Contentsave}>
                            <Button
                              block
                              style={{
                                width:'100%',
                                height: 45,
                                marginBottom: 20,
                                borderWidth: 1,
                                backgroundColor: "#00b300",
                                borderColor: "#00b300",
                                borderRadius: 4
                              }}
                              onPress={() => this.updateSIOSetRiksa()}
                            >
                              <Text style={{color:colors.white}}>SUBMIT</Text>
                            </Button>
                          </View>
                          </View>
                        </CardItem>
                    </View>
                    <View style={{ width: 270, position: "absolute" }}>
                        <Dialog
                            visible={this.state.visibleDialogSubmit}
                            dialogTitle={<DialogTitle title="Updating SIO Certification .." />}
                        >
                            <DialogContent>
                            {<ActivityIndicator size="large" color="#330066" animating />}
                            </DialogContent>
                        </Dialog>
                    </View>
                    </Content>
                </View>
                <CustomFooter navigation={this.props.navigation} menu='Safety' />
            </Container>

        );
    }
}

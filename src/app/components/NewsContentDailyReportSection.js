import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback ,Platform, AsyncStorage } from "react-native";
import Ripple from "react-native-material-ripple";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    height: 100,
    width: 100,
    marginLeft: 3,
    marginRight:3,
    paddingTop:3,
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class NewsContentDailyReportSection extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 5,
            elevation: 1,
            borderWidth:Platform.OS==='ios'?1:0,
            borderColor:(Platform.OS==='ios')?colors.lightGray:null
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          rippleColor={colors.accent}
        >
          <Image
            source={this.props.imageUri}
            style={{ marginTop:10, width: 50, height: 50, resizeMode: "contain" }}
          />
          <View style={{ flex: 1, paddingTop: 3, paddingLeft:10,paddingRight:10, alignItems: "center" }}>
            <Text style={{fontSize:9, paddingTop:0, fontWeight:"bold", color:colors.green01}}>{this.props.aksi}</Text>
          </View>
          <View style={{ flex: 1, paddingTop: 3,paddingLeft:10,paddingRight:10, alignItems: "center" }}>
          {this.props.status=='OPEN' ? (
          <Text
            note
            style={{
              alignSelf: "flex-end",
              fontSize: 9,
              fontWeight:"bold",
              color: colors.green01,
            }}
          >OPEN
          </Text>
        ):(
          <Text
            note
            style={{
              alignSelf: "flex-end",
              fontSize: 9,
              fontWeight:"bold",
              color: "#FF0101",
            }}
          >CLOSE
          </Text>)}
          </View>
        </Ripple>
      </View>
    );
  }
}

NewsContentDailyReportSection.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};

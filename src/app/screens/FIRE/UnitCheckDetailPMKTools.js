import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Text,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Thumbnail,
  Item,
  Input,
  Picker,
  Form,
  Switch,
  ListItem
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import DatePicker from "react-native-datepicker";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";

var that;
// class ListItemm extends React.PureComponent {
//   navigateToScreen(route, listUnitcheck) {
//     AsyncStorage.setItem("list", JSON.stringify(listUnitcheck)).then(() => {
//       that.props.navigation.navigate(route);
//     });
//   }

//   render() {
//     return (
//       <Content>
//         <ListItem icon>
//           <Left>
//             <Button style={{ backgroundColor: "#FF9501" }}>
//               <Icon active name="ios-airplane" />
//             </Button>
//           </Left>
//           <Body>
//             <Text note style={{ fontSize: 10, paddingTop: 10 }}>
//               {this.state.dataVehicleChecklist[0].SUB_ITEM[0].ITEM_NAME}
//             </Text>
//           </Body>
//           {this.state.dataVehicleChecklist[0].SUB_ITEM[0].DATA_CHECKED
//             .CHECK_VAL == "ADA" ? (
//             <Right>
//               <Switch value={true} />
//             </Right>
//           ) : (
//             <Right>
//               <Switch value={false} />
//             </Right>
//           )}
//         </ListItem>
{
  /* <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginLeft: 10, flex: 2 }}>
                            <Text note style={{ fontSize: 10, paddingTop: 10 }}>Vehicle Code</Text>
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>this.state.headerUnitCheck.MERK</Text>
                        </View>
                        <View style={{ marginRight: 20 }}>
                            <Text note style={{ fontSize: 10, paddingTop: 10, alignSelf: 'flex-end' }}>Tanggal</Text>
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>this.state.headerUnitCheck.TIPE</Text>
                        </View>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginLeft: 10, flex: 2 }}>
                            <Text note style={{ fontSize: 10, paddingTop: 10 }}>Shift</Text>
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>this.state.headerUnitCheck.MERK</Text>
                        </View>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginLeft: 10, flex: 2 }}>
                            <Text note style={{ fontSize: 10, paddingTop: 10 }}>Pemanasan (Menit)</Text>
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>this.state.headerUnitCheck.MERK Menit</Text>
                        </View>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginLeft: 10, flex: 2 }}>
                            <Text note style={{ fontSize: 10, paddingTop: 10 }}>Spedometer (Km)</Text>
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>this.state.headerUnitCheck.MERK KM</Text>
                        </View>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginLeft: 10, flex: 2 }}>
                            <Text note style={{ fontSize: 10, paddingTop: 10 }}>Level BBM</Text>
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>this.state.headerUnitCheck.MERK</Text>
                        </View>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop: 5, }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginLeft: 10, flex: 2, marginRight: 10 }}>
                            <Image style={{ height: 200, width: "100%", borderColor: colors.gray, borderWidth: 3, borderRadius: 10 }} source={require("../../../assets/images/truckfire.jpg")} />
                        </View>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginLeft: 10, flex: 2, width: "33%" }}>
                            <Text style={{ fontSize: 10, paddingTop: 10, fontWeight: 'bold' }}>Kondisi Oli : </Text>
                        </View>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                            <Text note style={{ fontSize: 10, paddingTop: 0 }}>Oli Mesin</Text>
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>L</Text>
                        </View>
                        <View style={{ flex: 2, marginRight: 20, marginLeft: 20, width: "34%" }}>
                            <Text note style={{ fontSize: 10, paddingTop: 0, }}>Oli Rem</Text>
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>L</Text>
                        </View>
                        <View style={{ marginRight: 20, width: "30%" }}>
                            <Text note style={{ fontSize: 10, paddingTop: 0 }}>Oli Power Steering</Text>
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>L</Text>
                        </View>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginLeft: 10, flex: 2, width: "33%" }}>
                            <Text style={{ fontSize: 10, paddingTop: 10, fontWeight: 'bold' }}>Kondisi Air : </Text>
                        </View>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginLeft: 10, flex: 2, width: "33%" }}>
                            <Text note style={{ fontSize: 10, paddingTop: 0 }}>Air Radiator</Text>
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>L</Text>
                        </View>
                        <View style={{ flex: 2, marginRight: 20, marginLeft: 20, width: "37%" }}>
                            <Text note style={{ fontSize: 10, paddingTop: 0, }}>Cadangan Air Radiator</Text>
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>L</Text>
                        </View>
                        <View style={{ marginRight: 20, width: "30%" }}>
                            <Text note style={{ fontSize: 10, paddingTop: 0 }}>Air Wiper</Text>
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}>L</Text>
                        </View>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginLeft: 10, flex: 2, width: "33%" }}>
                            <Text style={{ fontSize: 10, paddingTop: 10, fontWeight: 'bold' }}>Kondisi Tekanan Ban (Bar) : </Text>
                        </View>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flex: 1, flexDirection: "column" }}>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                            <View style={{ marginLeft: 10, flex: 2, width: "30%" }}>
                                <Text note style={{ fontSize: 10, paddingTop: 0 }}>Ban Depan Kiri</Text>
                                <Text style={{ fontSize: 10, fontWeight: "bold" }}>100</Text>
                            </View>
                            <View style={{ flex: 2, marginRight: 20, marginLeft: 40, width: "70%" }}>
                                <Text note style={{ fontSize: 10, paddingTop: 0, }}>Ban Depan Kanan</Text>
                                <Text style={{ fontSize: 10, fontWeight: "bold" }}>100</Text>
                            </View>
                        </View>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                            <View style={{ marginLeft: 10, flex: 2, width: "30%" }}>
                                <Text note style={{ fontSize: 10, paddingTop: 10 }}>Ban Belakang Kiri Dalam</Text>
                                <Text style={{ fontSize: 10, fontWeight: "bold" }}>100</Text>
                            </View>
                            <View style={{ flex: 2, marginRight: 20, marginLeft: 40, width: "70%" }}>
                                <Text note style={{ fontSize: 10, paddingTop: 10, }}>Ban Belakang Kiri Luar</Text>
                                <Text style={{ fontSize: 10, fontWeight: "bold" }}>100</Text>
                            </View>
                        </View>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                            <View style={{ marginLeft: 10, flex: 2, width: "30%" }}>
                                <Text note style={{ fontSize: 10, paddingTop: 10 }}>Ban Belakang Kanan Dalam</Text>
                                <Text style={{ fontSize: 10, fontWeight: "bold" }}>100</Text>
                            </View>
                            <View style={{ flex: 2, marginRight: 20, marginLeft: 40, width: "70%" }}>
                                <Text note style={{ fontSize: 10, paddingTop: 10, }}>Ban Belakang Kanan Luar</Text>
                                <Text style={{ fontSize: 10, fontWeight: "bold" }}>100</Text>
                            </View>
                        </View>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginLeft: 10, flex: 2, width: "33%" }}>
                            <Text style={{ fontSize: 10, paddingTop: 10, fontWeight: 'bold' }}>Kondisi Lampu-Lampu : </Text>
                        </View>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0, backgroundColor: colors.gray, marginLeft: 30, marginRight: 30 }}>
                    <View style={{ flex: 1, flexDirection: "column" }}>
                        <ListItem icon>
                            <Left>
                                <Button style={{ backgroundColor: "#FF9501" }}>
                                    <Icon active name="ios-airplane" />
                                </Button>
                            </Left>
                            <Body>
                                <Text note style={{ fontSize: 10, paddingTop: 10, }}>Lampu Cabin</Text>
                            </Body>
                            <Right>
                                <Switch value={true} />
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Button style={{ backgroundColor: "#FF9501" }}>
                                    <Icon active name="ios-airplane" />
                                </Button>
                            </Left>
                            <Body>
                                <Text note style={{ fontSize: 10, paddingTop: 10, }}>Lampu Kota</Text>
                            </Body>
                            <Right>
                                <Switch value={true} />
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Button style={{ backgroundColor: "#FF9501" }}>
                                    <Icon active name="ios-airplane" />
                                </Button>
                            </Left>
                            <Body>
                                <Text note style={{ fontSize: 10, paddingTop: 10, }}>Lampu Jauh</Text>
                            </Body>
                            <Right>
                                <Switch value={false} />
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Button style={{ backgroundColor: "#FF9501" }}>
                                    <Icon active name="ios-airplane" />
                                </Button>
                            </Left>
                            <Body>
                                <Text note style={{ fontSize: 10, paddingTop: 10, }}>Lampu Sein Kiri</Text>
                            </Body>
                            <Right>
                                <Switch value={true} />
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Button style={{ backgroundColor: "#FF9501" }}>
                                    <Icon active name="ios-airplane" />
                                </Button>
                            </Left>
                            <Body>
                                <Text note style={{ fontSize: 10, paddingTop: 10, }}>Lampu Sein Kanan</Text>
                            </Body>
                            <Right>
                                <Switch value={true} />
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Button style={{ backgroundColor: "#FF9501" }}>
                                    <Icon active name="ios-airplane" />
                                </Button>
                            </Left>
                            <Body>
                                <Text note style={{ fontSize: 10, paddingTop: 10, }}>Lampu Rem</Text>
                            </Body>
                            <Right>
                                <Switch value={true} />
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Button style={{ backgroundColor: "#FF9501" }}>
                                    <Icon active name="ios-airplane" />
                                </Button>
                            </Left>
                            <Body>
                                <Text note style={{ fontSize: 10, paddingTop: 10, }}>Lampu Atret</Text>
                            </Body>
                            <Right>
                                <Switch value={false} />
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Button style={{ backgroundColor: "#FF9501" }}>
                                    <Icon active name="ios-airplane" />
                                </Button>
                            </Left>
                            <Body>
                                <Text note style={{ fontSize: 10, paddingTop: 10, }}>Lampu Sorot</Text>
                            </Body>
                            <Right>
                                <Switch value={true} />
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Button style={{ backgroundColor: "#FF9501" }}>
                                    <Icon active name="ios-airplane" />
                                </Button>
                            </Left>
                            <Body>
                                <Text note style={{ fontSize: 10, paddingTop: 10, }}>Panel Dashboard</Text>
                            </Body>
                            <Right>
                                <Switch value={false} />
                            </Right>
                        </ListItem>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginLeft: 10, flex: 2, width: "33%" }}>
                            <Text style={{ fontSize: 10, paddingTop: 10, fontWeight: 'bold' }}>Kondisi Kaca Spion : </Text>
                        </View>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0, backgroundColor: colors.gray, marginLeft: 30, marginRight: 30 }}>
                    <View style={{ flex: 1, flexDirection: "column" }}>
                        <ListItem icon>
                            <Left>
                                <Button style={{ backgroundColor: "#FF9501" }}>
                                    <Icon active name="ios-airplane" />
                                </Button>
                            </Left>
                            <Body>
                                <Text note style={{ fontSize: 10, paddingTop: 10, }}>Kaca Spion Kiri</Text>
                            </Body>
                            <Right>
                                <Switch value={false} />
                            </Right>
                        </ListItem>
                        <ListItem icon>
                            <Left>
                                <Button style={{ backgroundColor: "#FF9501" }}>
                                    <Icon active name="ios-airplane" />
                                </Button>
                            </Left>
                            <Body>
                                <Text note style={{ fontSize: 10, paddingTop: 10, }}>Kaca Spion Kanan</Text>
                            </Body>
                            <Right>
                                <Switch value={true} />
                            </Right>
                        </ListItem>
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0 }}>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginLeft: 10, flex: 2 }}>
                            <Text note style={{ fontSize: 10, paddingTop: 10 }}>Catatan</Text>
                            <Text style={{ fontSize: 10, fontWeight: "bold" }}> - </Text>
                        </View>
                    </View>
                </CardItem> */
}
//       </Content>
//     );
//   }
// }

class UnitCheckDetailPMKTools extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataVehicleChecklist: [],
      isLoading: true,
      visibleFilter: false,
      adatidak: false,
      headerUnitCheck: [],
      noPeg: "",
      visibleDialogSubmit: false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItemm data={item} />;

  navigateToScreen(route, listUnitcheck) {
    // alert(JSON.stringify(listUnitcheck));
    AsyncStorage.setItem("id", JSON.stringify(listUnitcheck)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  componentDidMount() {
    AsyncStorage.getItem("list").then(listUnitcheck => {
      this.setState({ headerUnitCheck: JSON.parse(listUnitcheck) });
      this.setState({
        selectedShift: this.state.headerUnitCheck.SHIFT_CHECKED,
        selectedDate: this.state.headerUnitCheck.DATE_CHECKED
      });
    });
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
        // AsyncStorage.getItem("list").then(listUnitcheck => {
        //   this.setState({ headerUnitCheck: JSON.parse(listUnitcheck) });
        //   this.setState({
        //     selectedShift: this.state.headerUnitCheck.SHIFT_CHECKED,
        //     selectedDate: this.state.headerUnitCheck.DATE_CHECKED
        //   });
        // });
      }
    );
  }

  loadData() {
    this.setState({
      visibleDialog: false,
      visibleFilter: false,
      isLoading: true
    });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/firesystem/unit_check/get_vehicle_checklist";
      var array = "Engine & Acsecoris";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("VEHICLE_CHECK", this.state.headerUnitCheck.VEHICLE_TYPE);
      formData.append("REPORT_DATE", this.state.selectedDate);
      formData.append("ID_VEHICLE", this.state.headerUnitCheck.ID_VEHICLE);
      formData.append("REPORT_SHIFT", this.state.selectedShift);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson.Data.Tools))
          this.setState({
            dataVehicleChecklist: responseJson.Data.Tools,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
          this.setState({
            isLoading: false
          });
        });
    });
  }

  konfirmasideleteHeader() {
    Alert.alert(
      "Apakah Anda Yakin Ingin Menghapus",
      this.state.headerAccidentReport.ID,
      [
        { text: "Yes", onPress: () => this.deleteDetailAccident() },
        { text: "Cancel" }
      ],
      { cancelable: false }
    );
  }

  deleteDetailAccident() {
    this.setState({
      visibleDialogSubmit: true
    });
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/safety/accident/delete";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID", this.state.headerAccidentReport.ID);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Success", "Delete Success", [
              {
                text: "Okay"
              }
            ]);
            this.props.navigation.navigate("AccidentReport");
          } else {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Error", "Delete Failed", [
              {
                text: "Okay"
              }
            ]);
          }
        })
        .catch(error => {
          this.setState({
            visibleDialogSubmit: false
          });
          Alert.alert("Error", "Delete Failed", [
            {
              text: "Okay"
            }
          ]);
          console.log(error);
        });
    });
  }

  render() {
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.headerUnitCheck.DATE_CHECKED == null) {
        list = (
          <ScrollView>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Text style={{ fontSize: 12, color: colors.green01 }}>
                Vehicle Tools Checklist{" "}
                {this.state.headerUnitCheck.VEHICLE_CODE}
              </Text>
              <Text style={{ fontSize: 12, color: colors.green01 }}>
                on Shift {this.state.selectedShift}
              </Text>
            </View>
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center",
                marginTop: 150
              }}
            >
              <Thumbnail
                square
                large
                source={require("../../../assets/images/empty.png")}
              />
              <Text>Not Checked!</Text>
            </View>
          </ScrollView>
        );
      } else {
        list = (
          <ScrollView>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Text style={{ fontSize: 12, color: colors.green01 }}>
                Vehicle Tools Checklist{" "}
                {this.state.headerUnitCheck.VEHICLE_CODE}
              </Text>
              <Text style={{ fontSize: 12, color: colors.green01 }}>
                on Shift {this.state.selectedShift}
              </Text>
            </View>
            {/* <FlatList
                            data={this.state.dataVehicleChecklist}
                            renderItem={this._renderItem}
                            keyExtractor={(item, index) => index.toString()}
                        /> */}
            <CardItem style={{ borderRadius: 0 }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2 }}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Vehicle Code
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnitCheck.VEHICLE_CODE}
                  </Text>
                </View>
                <View style={{ marginRight: 20 }}>
                  <Text
                    note
                    style={{
                      fontSize: 10,
                      paddingTop: 10,
                      alignSelf: "flex-end"
                    }}
                  >
                    Tanggal
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.selectedDate}
                  </Text>
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop: 5 }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2, marginRight: 10 }}>
                  <Image
                    style={{
                      height: 200,
                      width: "100%",
                      borderColor: colors.gray,
                      borderWidth: 3,
                      borderRadius: 10
                    }}
                    source={require("../../../assets/images/truckfire.jpg")}
                  />
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0 }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2, width: "33%" }}>
                  <Text
                    style={{ fontSize: 10, paddingTop: 10, fontWeight: "bold" }}
                  >
                    {this.state.dataVehicleChecklist[0].ITEM_NAME} :{" "}
                  </Text>
                </View>
              </View>
            </CardItem>
            <CardItem
              style={{
                borderRadius: 0,
                backgroundColor: colors.gray,
                marginLeft: 30,
                marginRight: 30
              }}
            >
              <View style={{ flex: 1, flexDirection: "column" }}>
                {this.state.dataVehicleChecklist[0].SUB_ITEM[0].DATA_CHECKED ==
                null ? (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {
                          this.state.dataVehicleChecklist[0].SUB_ITEM[0]
                            .ITEM_NAME
                        }
                      </Text>
                    </Body>
                    <Right>
                      <Text
                        note
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.Orange
                        }}
                      >
                        {" "}
                        BELUM DIPERIKSA
                      </Text>
                    </Right>
                  </ListItem>
                ) : (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {
                          this.state.dataVehicleChecklist[0].SUB_ITEM[0]
                            .ITEM_NAME
                        }
                      </Text>
                    </Body>
                    {this.state.dataVehicleChecklist[0].SUB_ITEM[0].DATA_CHECKED
                      .CHECK_VAL ==
                    this.state.dataVehicleChecklist[0].SUB_ITEM[0].STANDARD ? (
                      <Right>
                        <Switch value={true} />
                      </Right>
                    ) : (
                      <Right>
                        <Switch value={false} />
                      </Right>
                    )}
                  </ListItem>
                )}
                {this.state.dataVehicleChecklist[0].SUB_ITEM[1].DATA_CHECKED ==
                null ? (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {
                          this.state.dataVehicleChecklist[0].SUB_ITEM[1]
                            .ITEM_NAME
                        }
                      </Text>
                    </Body>
                    <Right>
                      <Text
                        note
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.Orange
                        }}
                      >
                        {" "}
                        BELUM DIPERIKSA
                      </Text>
                    </Right>
                  </ListItem>
                ) : (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {
                          this.state.dataVehicleChecklist[0].SUB_ITEM[1]
                            .ITEM_NAME
                        }
                      </Text>
                    </Body>
                    {this.state.dataVehicleChecklist[0].SUB_ITEM[1].DATA_CHECKED
                      .CHECK_VAL ==
                    this.state.dataVehicleChecklist[0].SUB_ITEM[1].STANDARD ? (
                      <Right>
                        <Switch value={true} />
                      </Right>
                    ) : (
                      <Right>
                        <Switch value={false} />
                      </Right>
                    )}
                  </ListItem>
                )}
                {this.state.dataVehicleChecklist[0].SUB_ITEM[2].DATA_CHECKED ==
                null ? (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {
                          this.state.dataVehicleChecklist[0].SUB_ITEM[2]
                            .ITEM_NAME
                        }
                      </Text>
                    </Body>
                    <Right>
                      <Text
                        note
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.Orange
                        }}
                      >
                        {" "}
                        BELUM DIPERIKSA
                      </Text>
                    </Right>
                  </ListItem>
                ) : (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {
                          this.state.dataVehicleChecklist[0].SUB_ITEM[2]
                            .ITEM_NAME
                        }
                      </Text>
                    </Body>
                    {this.state.dataVehicleChecklist[0].SUB_ITEM[2].DATA_CHECKED
                      .CHECK_VAL ==
                    this.state.dataVehicleChecklist[0].SUB_ITEM[2].STANDARD ? (
                      <Right>
                        <Switch value={true} />
                      </Right>
                    ) : (
                      <Right>
                        <Switch value={false} />
                      </Right>
                    )}
                  </ListItem>
                )}
                {this.state.dataVehicleChecklist[0].SUB_ITEM[3].DATA_CHECKED ==
                null ? (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {
                          this.state.dataVehicleChecklist[0].SUB_ITEM[3]
                            .ITEM_NAME
                        }
                      </Text>
                    </Body>
                    <Right>
                      <Text
                        note
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.Orange
                        }}
                      >
                        {" "}
                        BELUM DIPERIKSA
                      </Text>
                    </Right>
                  </ListItem>
                ) : (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {
                          this.state.dataVehicleChecklist[0].SUB_ITEM[3]
                            .ITEM_NAME
                        }
                      </Text>
                    </Body>
                    {this.state.dataVehicleChecklist[0].SUB_ITEM[3].DATA_CHECKED
                      .CHECK_VAL ==
                    this.state.dataVehicleChecklist[0].SUB_ITEM[3].STANDARD ? (
                      <Right>
                        <Switch value={true} />
                      </Right>
                    ) : (
                      <Right>
                        <Switch value={false} />
                      </Right>
                    )}
                  </ListItem>
                )}
                {this.state.dataVehicleChecklist[0].SUB_ITEM[4].DATA_CHECKED ==
                null ? (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {
                          this.state.dataVehicleChecklist[0].SUB_ITEM[4]
                            .ITEM_NAME
                        }
                      </Text>
                    </Body>
                    <Right>
                      <Text
                        note
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.Orange
                        }}
                      >
                        {" "}
                        BELUM DIPERIKSA
                      </Text>
                    </Right>
                  </ListItem>
                ) : (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {
                          this.state.dataVehicleChecklist[0].SUB_ITEM[4]
                            .ITEM_NAME
                        }
                      </Text>
                    </Body>
                    {this.state.dataVehicleChecklist[0].SUB_ITEM[4].DATA_CHECKED
                      .CHECK_VAL ==
                    this.state.dataVehicleChecklist[0].SUB_ITEM[4].STANDARD ? (
                      <Right>
                        <Switch value={true} />
                      </Right>
                    ) : (
                      <Right>
                        <Switch value={false} />
                      </Right>
                    )}
                  </ListItem>
                )}
                {this.state.dataVehicleChecklist[0].SUB_ITEM[5].DATA_CHECKED ==
                null ? (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {
                          this.state.dataVehicleChecklist[0].SUB_ITEM[5]
                            .ITEM_NAME
                        }
                      </Text>
                    </Body>
                    <Right>
                      <Text
                        note
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.Orange
                        }}
                      >
                        {" "}
                        BELUM DIPERIKSA
                      </Text>
                    </Right>
                  </ListItem>
                ) : (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {
                          this.state.dataVehicleChecklist[0].SUB_ITEM[5]
                            .ITEM_NAME
                        }
                      </Text>
                    </Body>
                    {this.state.dataVehicleChecklist[0].SUB_ITEM[5].DATA_CHECKED
                      .CHECK_VAL ==
                    this.state.dataVehicleChecklist[0].SUB_ITEM[5].STANDARD ? (
                      <Right>
                        <Switch value={true} />
                      </Right>
                    ) : (
                      <Right>
                        <Switch value={false} />
                      </Right>
                    )}
                  </ListItem>
                )}
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0 }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2, width: "33%" }}>
                  <Text
                    style={{ fontSize: 10, paddingTop: 10, fontWeight: "bold" }}
                  >
                    Lain - Lain :{" "}
                  </Text>
                </View>
              </View>
            </CardItem>
            <CardItem
              style={{ borderRadius: 0, flex: 1, flexDirection: "column" }}
            >
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[1].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[1].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[1].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[1].SATUAN}
                    </Text>
                  )}
                </View>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[2].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[2].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[2].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[2].SATUAN}
                    </Text>
                  )}
                </View>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[3].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[3].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[3].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[3].SATUAN}
                    </Text>
                  )}
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: "row", marginTop: 15 }}>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[4].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[4].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[4].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[4].SATUAN}
                    </Text>
                  )}
                </View>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[5].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[5].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[5].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[5].SATUAN}
                    </Text>
                  )}
                </View>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[6].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[6].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[6].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[6].SATUAN}
                    </Text>
                  )}
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: "row", marginTop: 15 }}>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[7].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[7].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[7].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[7].SATUAN}
                    </Text>
                  )}
                </View>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[8].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[8].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[8].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[8].SATUAN}
                    </Text>
                  )}
                </View>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[9].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[9].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[9].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[9].SATUAN}
                    </Text>
                  )}
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: "row", marginTop: 15 }}>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[10].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[10].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[10].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[10].SATUAN}
                    </Text>
                  )}
                </View>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[11].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[11].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[11].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[11].SATUAN}
                    </Text>
                  )}
                </View>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[12].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[12].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[12].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[12].SATUAN}
                    </Text>
                  )}
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: "row", marginTop: 15 }}>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[13].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[13].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[13].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[13].SATUAN}
                    </Text>
                  )}
                </View>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[14].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[14].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[14].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[14].SATUAN}
                    </Text>
                  )}
                </View>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[15].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[15].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[15].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[15].SATUAN}
                    </Text>
                  )}
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: "row", marginTop: 15 }}>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[16].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[16].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[16].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[16].SATUAN}
                    </Text>
                  )}
                </View>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[17].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[17].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[17].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[17].SATUAN}
                    </Text>
                  )}
                </View>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[18].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[18].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[18].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[18].SATUAN}
                    </Text>
                  )}
                </View>
              </View>
              <View style={{ flex: 1, flexDirection: "row", marginTop: 15 }}>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[19].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[19].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[19].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[19].SATUAN}
                    </Text>
                  )}
                </View>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[20].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[20].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[20].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[20].SATUAN}
                    </Text>
                  )}
                </View>
                <View style={{ marginLeft: 10, flex: 2, width: "37%" }}>
                  <Text note style={{ fontSize: 10, paddingTop: 0 }}>
                    {this.state.dataVehicleChecklist[21].ITEM_NAME}
                  </Text>
                  {this.state.dataVehicleChecklist[21].DATA_CHECKED == null ? (
                    <Text
                      note
                      style={{
                        fontSize: 10,
                        fontWeight: "bold",
                        color: colors.Orange
                      }}
                    >
                      {" "}
                      BELUM DIPERIKSA
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {
                        this.state.dataVehicleChecklist[21].DATA_CHECKED
                          .CHECK_VAL
                      }{" "}
                      {this.state.dataVehicleChecklist[21].SATUAN}
                    </Text>
                  )}
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0 }}>
              <View style={{ flex: 1, flexDirection: "column" }}>
                {this.state.dataVehicleChecklist[22].DATA_CHECKED == null ? (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {this.state.dataVehicleChecklist[22].ITEM_NAME}
                      </Text>
                    </Body>
                    <Right>
                      <Text
                        note
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.Orange
                        }}
                      >
                        {" "}
                        BELUM DIPERIKSA
                      </Text>
                    </Right>
                  </ListItem>
                ) : (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {this.state.dataVehicleChecklist[22].ITEM_NAME}
                      </Text>
                    </Body>
                    {this.state.dataVehicleChecklist[22].DATA_CHECKED
                      .CHECK_VAL ==
                    this.state.dataVehicleChecklist[22].STANDARD ? (
                      <Right>
                        <Switch value={true} />
                      </Right>
                    ) : (
                      <Right>
                        <Switch value={false} />
                      </Right>
                    )}
                  </ListItem>
                )}
                {this.state.dataVehicleChecklist[23].DATA_CHECKED == null ? (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {this.state.dataVehicleChecklist[23].ITEM_NAME}
                      </Text>
                    </Body>
                    <Right>
                      <Text
                        note
                        style={{
                          fontSize: 10,
                          fontWeight: "bold",
                          color: colors.Orange
                        }}
                      >
                        {" "}
                        BELUM DIPERIKSA
                      </Text>
                    </Right>
                  </ListItem>
                ) : (
                  <ListItem icon>
                    <Left>
                      <Button style={{ backgroundColor: "#FF9501" }}>
                        <Icon active name="ios-settings" />
                      </Button>
                    </Left>
                    <Body>
                      <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                        {this.state.dataVehicleChecklist[23].ITEM_NAME}
                      </Text>
                    </Body>
                    {this.state.dataVehicleChecklist[23].DATA_CHECKED
                      .CHECK_VAL ==
                    this.state.dataVehicleChecklist[23].STANDARD ? (
                      <Right>
                        <Switch value={true} />
                      </Right>
                    ) : (
                      <Right>
                        <Switch value={false} />
                      </Right>
                    )}
                  </ListItem>
                )}
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0 }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2 }}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Catatan
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}> - </Text>
                </View>
              </View>
            </CardItem>
          </ScrollView>
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("UnitCheckPMK")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 1 }}>
            <Title style={styles.textbody}>Vehicle Detail</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.setState({ visibleFilter: true })}
            >
              <Icon
                name="ios-funnel"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            <Button
              transparent
              onPress={() =>
                this.navigateToScreen(
                  "UnitCheckPMKCreate",
                  this.state.headerUnitCheck
                )
              }
              // onPress={() => alert("Edit Pressed")}
            >
              <Icon
                name="ios-create"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Footer>
          <FooterTab style={styles.tabfooter}>
            <Button active style={styles.tabfooter}>
              <View style={{ height: "40%" }} />
              <View style={{ height: "50%" }}>
                <Text style={styles.textbody}> Tools </Text>
              </View>
              <View style={{ height: "20%" }} />
              <View
                style={{
                  borderWidth: 2,
                  marginTop: 2,
                  height: 0.5,
                  width: "100%",
                  borderColor: colors.white
                }}
              />
            </Button>
            <Button
              onPress={() =>
                this.props.navigation.navigate("UnitCheckDetailPMKEngine")
              }
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>
                {" "}
                Engine & Accessories{" "}
              </Text>
            </Button>
          </FooterTab>
        </Footer>

        <View style={{ marginTop: 5, flex: 1, flexDirection: "column" }}>
          {list}
          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleFilter}
              dialogAnimation={
                new SlideAnimation({
                  slideFrom: "bottom"
                })
              }
              dialogStyle={{ position: "absolute", top: this.state.posDialog }}
              onTouchOutside={() => {
                this.setState({ visibleFilter: false });
              }}
              dialogTitle={<DialogTitle title="Select shift here" />}
              actions={[
                <DialogButton
                  style={{
                    fontSize: 11,
                    backgroundColor: colors.white,
                    borderColor: colors.blue01
                  }}
                  text="SUBMIT"
                  onPress={() => this.loadData()}
                  // onPress={() => this.setState({ visibleFilter: false })}
                />
              ]}
            >
              <DialogContent>
                {
                  <View>
                    <Form
                      style={{
                        borderWidth: 1,
                        borderRadius: 5,
                        marginRight: 5,
                        marginLeft: 5,
                        marginTop: 15,
                        marginBottom: 10,
                        borderColor: "#E6E6E6",
                        // height: 50,
                        width: 250
                      }}
                    >
                      <Picker
                        mode="dropdown"
                        iosIcon={
                          <Icon
                            name={
                              Platform.OS
                                ? "ios-arrow-down"
                                : "ios-arrow-down-outline"
                            }
                          />
                        }
                        style={{ width: "100%", height: 35 }}
                        placeholder="Select Shift ..."
                        placeholderStyle={{ color: "#bfc6ea" }}
                        placeholderIconColor="#007aff"
                        selectedValue={this.state.selectedShift}
                        onValueChange={itemValue =>
                          this.setState({ selectedShift: itemValue })
                        }
                      >
                        <Picker.Item label="Shift I (Satu)" value="1" />
                        <Picker.Item label="Shift II (Dua)" value="2" />
                        <Picker.Item label="Shift III (Tiga)" value="3" />
                      </Picker>
                    </Form>
                    <Text style={styles.titleInput}>Tanggal Pemeriksaan *</Text>
                    <DatePicker
                      style={{
                        width: "100%",
                        fontSize: 10,
                        borderRadius: 20
                      }}
                      date={this.state.selectedDate}
                      mode="date"
                      placeholder="Choose Date ..."
                      format="YYYY-MM-DD"
                      minDate="2018-01-01"
                      maxDate="2050-12-31"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      // iconSource='{this.timeIcon}'
                      customStyles={{
                        dateInput: {
                          marginLeft: 5,
                          marginRight: 5,
                          height: 35,
                          borderRadius: 5,
                          fontSize: 10,
                          borderWidth: 1,
                          borderColor: "#E6E6E6"
                        },
                        dateIcon: {
                          position: "absolute",
                          left: 0,
                          top: 5
                        }
                      }}
                      onDateChange={date => {
                        this.setState({ selectedDate: date });
                      }}
                    />
                  </View>
                }
              </DialogContent>
            </Dialog>
          </View>

          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
              dialogTitle={<DialogTitle title="Deleting Accident Report .." />}
            >
              <DialogContent>
                {<ActivityIndicator size="large" color="#330066" animating />}
              </DialogContent>
            </Dialog>
          </View>
        </View>
        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}

export default UnitCheckDetailPMKTools;

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  TextInput,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  ListItem,
  Body,
  Fab,
  Textarea,
  Switch,
  Icon,
  Picker,
  Form,
  Input,
  Label,
  Item
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DateTimePicker from "react-native-datepicker";
import DatePicker from "react-native-datepicker";
import ImagePicker from "react-native-image-picker";
import Ripple from "react-native-material-ripple";
import CheckBox from "react-native-check-box";
import Moment from "moment";
import ListView from "../../components/ListView";

class ListItemPegawai extends React.PureComponent {
  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() => this.props.setSelectedPegawaiLabel(this.props.data)}
          rippleColor={colors.accent}
        >
          <CardItem
            style={{
              borderRadius: 0,
              marginTop: 4,
              backgroundColor: colors.gray
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 4,
                backgroundColor: colors.gray
              }}
            >
              <View>
                <Text style={{ fontSize: 12 }}>
                  {this.props.data.mk_nopeg} - {this.props.data.mk_nama}
                </Text>
              </View>
            </View>
          </CardItem>
        </Ripple>
      </View>
    );
  }
}

export default class UnitCheckPMKCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      tabNumber: "tab1",
      shiftChecking: 1,
      isNegatifPemanasan: 0,
      visiblePemanasan: false,
      isNegatifSpeedometer: 0,
      visibleSpeedometer: false,
      isNegatifBbm: 0,
      visibleBbm: false,
      bbmChecking: "R",
      isNegatifWorkPress: 0,
      visibleWorkPress: false,
      isNegatifHourMeterMesin: 0,
      visibleHourMeterMesin: false,
      isNegatifHourMeterPompa: 0,
      visibleHourMeterPompa: false,
      oliMesinChecking: "L",
      isNegatifOliMesin: 0,
      visibleOliMesin: false,
      oliRemChecking: "L",
      isNegatifOliRem: 0,
      visibleOliRem: false,
      oliPowerSteeringChecking: "L",
      isNegatifOliPowerSteering: 0,
      visibleOliPowerSteering: false,
      airRadiatorChecking: "R",
      isNegatifAirRadiator: 0,
      visibleAirRadiator: false,
      airCadanganRadiatorChecking: "R",
      isNegatifAirCadanganRadiator: 0,
      visibleAirCadanganRadiator: false,
      airWiperChecking: "R",
      isNegatifAirWiper: 0,
      visibleAirWiper: false,
      isNegatifBanDepanKiri: 0,
      visibleBanDepanKiri: false,
      isNegatifBanDepanKanan: 0,
      visibleBanDepanKanan: false,
      isNegatifBanBelakangKananLuar: 0,
      visibleBanBelakangKananLuar: false,
      isNegatifBanBelakangKiriLuar: 0,
      visibleBanBelakangKiriLuar: false,
      isNegatifBanBelakangKananDalam: 0,
      visibleBanBelakangKananDalam: false,
      isNegatifBanBelakangKiriDalam: 0,
      visibleBanBelakangKiriDalam: false,
      lampuCabinChecking: false,
      lampuKotaChecking: false,
      lampuJauhChecking: false,
      lampuSeinKiriChecking: false,
      lampuSeinKananChecking: false,
      lampuRemChecking: false,
      lampuAtretChecking: false,
      lampuSorotChecking: false,
      dashboardChecking: false,
      spionKiriChecking: false,
      spionKananChecking: false,
      dongkrakChecking: false,
      stangKabinChecking: false,
      ganjalBanChecking: false,
      kunciRodaChecking: false,
      hammerChecking: false,
      p3kChecking: false,
      sirineChecking: false,
      accessoriesChecking: false,
      isNegatifHose25: 0,
      visibleHose25: false,
      isNegatifHose15: 0,
      visibleHose15: false,
      isNegatifNozzlejs: 0,
      visibleNozzlejs: false,
      isNegatifNozzleakron: 0,
      visibleNozzleakron: false,
      isNegatifNozzlejet: 0,
      visibleNozzlejet: false,
      isNegatifYvalve: 0,
      visibleYvalve: false,
      isNegatifRed25to15: 0,
      visibleRed25to15: false,
      isNegatifNozzlefoam: 0,
      visibleNozzlefoam: false,
      isNegatifKunciHydrant: 0,
      visibleKunciHydrant: false,
      isNegatifHoseSuction: 0,
      visibleHoseSuction: false,
      isNegatifKunciHoseSuction: 0,
      visibleKunciHoseSuction: false,
      isNegatifApar: 0,
      visibleApar: false,
      isNegatifKapak: 0,
      visibleKapak: false,
      isNegatifLinggis: 0,
      visibleLinggis: false,
      isNegatifTanggaGanda: 0,
      visibleTanggaGanda: false,
      isNegatifSenter: 0,
      visibleSenter: false,
      isNegatifGantol: 0,
      visibleGantol: false,
      isNegatifTimba: 0,
      visibleTimba: false,
      isNegatifKarung: 0,
      visibleKarung: false,
      isNegatifJaketPMK: 0,
      visibleJaketPMK: false,
      isNegatifHelmPMK: 0,
      visibleHelmPMK: false,
      headerUnitCheck: [],
      isCheckedOther: false,
      visibleDialogSubmit: false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.setState({
          visibleDialogSubmit: false
        });
      }
    );
    AsyncStorage.getItem("list").then(listUnitcheck => {
      this.setState({ headerUnitCheck: JSON.parse(listUnitcheck) });
      // this.setState({ isLoading: false });
      // this.loadData();
    });
  }

  validasiFieldTab1() {
    console.log(this.state.dateChecking+" "+this.state.dateChecking)
        if (this.state.dateChecking==undefined){
            alert("Field Tanggal Pemeriksaan Wajib Diisi")
        }else if (this.state.timeChecking==undefined){
            alert("Field Jam Pemeriksaan Wajib Diisi")
        }else{
            this.setState({
            tabNumber: 'tab2'
        })
        }
  }

  validasiFieldTab2() {
    this.setState({
      tabNumber: "tab3"
    });
  }

  validasiFieldTab3() {
    this.setState({
      tabNumber: "tab4"
    });
  }

  submitPemanasanNote() {
    this.setState({
      visiblePemanasan: false,
      pemanasanCheckingNote: this.state.pemanasanCheckingNote
    });
  }

  submitSpeedometerNote() {
    this.setState({
      visibleSpeedometer: false,
      speedometerCheckingNote: this.state.speedometerCheckingNote
    });
  }

  submitBbmNote() {
    this.setState({
      visibleBbm: false,
      bbmCheckingNote: this.state.bbmCheckingNote
    });
  }

  submitWorkPressNote() {
    this.setState({
      visibleWorkPress: false,
      workPressCheckingNote: this.state.workPressCheckingNote
    });
  }

  submitHourMeterMesinNote() {
    this.setState({
      visibleHourMeterMesin: false,
      hourMeterMesinCheckingNote: this.state.hourMeterMesinCheckingNote
    });
  }

  submitHourMeterPompaNote() {
    this.setState({
      visibleHourMeterPompa: false,
      hourMeterPompaCheckingNote: this.state.hourMeterPompaCheckingNote
    });
  }

  submitOliMesinNote() {
    this.setState({
      visibleOliMesin: false,
      oliMesinCheckingNote: this.state.oliMesinCheckingNote
    });
  }

  submitOliRemNote() {
    this.setState({
      visibleOliRem: false,
      oliRemCheckingNote: this.state.oliRemCheckingNote
    });
  }

  submitOliPowerSteeringNote() {
    this.setState({
      visibleOliPowerSteering: false,
      oliPowerSteeringCheckingNote: this.state.oliPowerSteeringCheckingNote
    });
  }

  submitAirRadiatorNote() {
    this.setState({
      visibleAirRadiator: false,
      airRadiatorCheckingNote: this.state.airRadiatorCheckingNote
    });
  }

  submitAirCadanganRadiatorNote() {
    this.setState({
      visibleAirCadanganRadiator: false,
      airCadanganRadiatorCheckingNote: this.state
        .airCadanganRadiatorCheckingNote
    });
  }

  submitAirWiperNote() {
    this.setState({
      visibleAirWiper: false,
      airWiperCheckingNote: this.state.airWiperCheckingNote
    });
  }

  submitBanDepanKiriNote() {
    this.setState({
      visibleBanDepanKiri: false,
      banDepanKiriCheckingNote: this.state.banDepanKiriCheckingNote
    });
  }

  submitBanDepanKananNote() {
    this.setState({
      visibleBanDepanKanan: false,
      banDepanKananCheckingNote: this.state.banDepanKananCheckingNote
    });
  }

  submitBanBelakangKiriDalamNote() {
    this.setState({
      visibleBanBelakangKiriDalam: false,
      banBelakangKiriDalamCheckingNote: this.state
        .banBelakangKiriDalamCheckingNote
    });
  }

  submitBanBelakangKananDalamNote() {
    this.setState({
      visibleBanBelakangKananDalam: false,
      banBelakangKananDalamCheckingNote: this.state
        .banBelakangKananDalamCheckingNote
    });
  }

  submitBanBelakangKiriLuarNote() {
    this.setState({
      visibleBanBelakangKiriLuar: false,
      banBelakangKiriLuarCheckingNote: this.state
        .banBelakangKiriLuarCheckingNote
    });
  }

  submitBanBelakangKananLuarNote() {
    this.setState({
      visibleBanBelakangKananLuar: false,
      banBelakangKananLuarCheckingNote: this.state
        .banBelakangKananLuarCheckingNote
    });
  }

  submitHose25Note() {
    this.setState({
      visibleHose25: false,
      hose25CheckingNote: this.state.hose25CheckingNote
    });
  }

  submitHose15Note() {
    this.setState({
      visibleHose15: false,
      hose15CheckingNote: this.state.hose15CheckingNote
    });
  }

  submitNozzle15jsNote() {
    this.setState({
      visibleNozzlejs: false,
      nozzle15jsCheckingNote: this.state.nozzle15jsCheckingNote
    });
  }

  submitNozzle15akronNote() {
    this.setState({
      visibleNozzleakron: false,
      nozzle15akronCheckingNote: this.state.nozzle15akronCheckingNote
    });
  }

  submitNozzle15jetNote() {
    this.setState({
      visibleNozzlejet: false,
      nozzle15jetCheckingNote: this.state.nozzle15jetCheckingNote
    });
  }

  submitYvalveNote() {
    this.setState({
      visibleYvalve: false,
      yvalveCheckingNote: this.state.yvalveCheckingNote
    });
  }

  submitRed25to15Note() {
    this.setState({
      visibleRed25to15: false,
      red25to15CheckingNote: this.state.red25to15CheckingNote
    });
  }

  submitNozzlefoamNote() {
    this.setState({
      visibleNozzlefoam: false,
      nozzlefoamCheckingNote: this.state.nozzlefoamCheckingNote
    });
  }

  submitKunciHydrantNote() {
    this.setState({
      visibleKunciHydrant: false,
      kunciHydrantCheckingNote: this.state.kunciHydrantCheckingNote
    });
  }

  submitHoseSuctionNote() {
    this.setState({
      visibleHoseSuction: false,
      hoseSuctionCheckingNote: this.state.hoseSuctionCheckingNote
    });
  }

  submitKunciHoseSuctionNote() {
    this.setState({
      visibleKunciHoseSuction: false,
      kunciHoseSuctionCheckingNote: this.state.kunciHoseSuctionCheckingNote
    });
  }

  submitAparNote() {
    this.setState({
      visibleApar: false,
      aparCheckingNote: this.state.aparCheckingNote
    });
  }

  submitKapakNote() {
    this.setState({
      visibleKapak: false,
      kapakCheckingNote: this.state.kapakCheckingNote
    });
  }

  submitLinggisNote() {
    this.setState({
      visibleLinggis: false,
      linggisCheckingNote: this.state.linggisCheckingNote
    });
  }

  submitTanggaGandaNote() {
    this.setState({
      visibleTanggaGanda: false,
      tanggaGandaCheckingNote: this.state.tanggaGandaCheckingNote
    });
  }

  submitSenterNote() {
    this.setState({
      visibleSenter: false,
      senterCheckingNote: this.state.senterCheckingNote
    });
  }

  submitGantolNote() {
    this.setState({
      visibleGantol: false,
      gantolCheckingNote: this.state.gantolCheckingNote
    });
  }

  submitTimbaNote() {
    this.setState({
      visibleTimba: false,
      timbaCheckingNote: this.state.timbaCheckingNote
    });
  }

  submitKarungNote() {
    this.setState({
      visibleKarung: false,
      karungCheckingNote: this.state.karungCheckingNote
    });
  }

  submitJaketPMKNote() {
    this.setState({
      visibleJaketPMK: false,
      jaketPMKCheckingNote: this.state.jaketPMKCheckingNote
    });
  }

  submitHelmPMKNote() {
    this.setState({
      visibleHelmPMK: false,
      helmPMKCheckingNote: this.state.helmPMKCheckingNote
    });
  }

  unitCheckPMKCreate() {
    this.setState({
      visibleDialogSubmit: true
    });
    AsyncStorage.getItem("token").then(value => {
      var url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/firesystem/unit_check/checking_unit/save";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID_VEHICLE", this.state.headerUnitCheck.ID_VEHICLE);
      formData.append("REPORT_DATE", this.state.dateChecking);
      formData.append("REPORT_SHIFT", this.state.shiftChecking);
      formData.append("REPORT_CLOCK", this.state.timeChecking);
      // alert(this.state.pemanasanCheckingNote)
      formData.append("ID_STANDARD[0]", 33);
      formData.append("CHECK_VALUE[0]", this.state.pemanasanChecking);
      formData.append("CHECK_NOTE[0]", this.state.pemanasanCheckingNote);
      formData.append("IS_NEGATIF[0]", this.state.isNegatifPemanasan);
      formData.append("OPT_FILE[0]", null);

      formData.append("ID_STANDARD[1]", 34);
      formData.append("CHECK_VALUE[1]", this.state.speedometerChecking);
      formData.append("CHECK_NOTE[1]", this.state.speedometerCheckingNote);
      formData.append("IS_NEGATIF[1]", this.state.isNegatifSpeedometer);
      formData.append("OPT_FILE[1]", null);

      formData.append("ID_STANDARD[2]", 35);
      formData.append("CHECK_VALUE[2]", this.state.bbmChecking);
      formData.append("CHECK_NOTE[2]", this.state.bbmCheckingNote);
      formData.append("IS_NEGATIF[2]", this.state.isNegatifBbm);
      formData.append("OPT_FILE[2]", null);

      formData.append("ID_STANDARD[3]", 36);
      formData.append("CHECK_VALUE[3]", this.state.workPressChecking);
      formData.append("CHECK_NOTE[3]", this.state.workPressCheckingNote);
      formData.append("IS_NEGATIF[3]", this.state.isNegatifWorkPress);
      formData.append("OPT_FILE[3]", null);

      formData.append("ID_STANDARD[4]", 37);
      formData.append("CHECK_VALUE[4]", this.state.hourMeterMesinChecking);
      formData.append("CHECK_NOTE[4]", this.state.hourMeterMesinCheckingNote);
      formData.append("IS_NEGATIF[4]", this.state.isNegatifHourMeterMesin);
      formData.append("OPT_FILE[4]", null);

      formData.append("ID_STANDARD[5]", 38);
      formData.append("CHECK_VALUE[5]", this.state.hourMeterPompaChecking);
      formData.append("CHECK_NOTE[5]", this.state.hourMeterPompaCheckingNote);
      formData.append("IS_NEGATIF[5]", this.state.isNegatifHourMeterPompa);
      formData.append("OPT_FILE[5]", null);

      formData.append("ID_STANDARD[6]", 40);
      formData.append("CHECK_VALUE[6]", this.state.oliMesinChecking);
      formData.append("CHECK_NOTE[6]", this.state.oliMesinCheckingNote);
      formData.append("IS_NEGATIF[6]", this.state.isNegatifOliMesin);
      formData.append("OPT_FILE[6]", null);

      formData.append("ID_STANDARD[7]", 41);
      formData.append("CHECK_VALUE[7]", this.state.oliRemChecking);
      formData.append("CHECK_NOTE[7]", this.state.oliRemCheckingNote);
      formData.append("IS_NEGATIF[7]", this.state.isNegatifOliRem);
      formData.append("OPT_FILE[7]", null);

      formData.append("ID_STANDARD[8]", 42);
      formData.append("CHECK_VALUE[8]", this.state.oliPowerSteeringChecking);
      formData.append("CHECK_NOTE[8]", this.state.oliPowerSteeringCheckingNote);
      formData.append("IS_NEGATIF[8]", this.state.isNegatifOliPowerSteering);
      formData.append("OPT_FILE[8]", null);

      formData.append("ID_STANDARD[9]", 44);
      formData.append("CHECK_VALUE[9]", this.state.airRadiatorChecking);
      formData.append("CHECK_NOTE[9]", this.state.airRadiatorCheckingNote);
      formData.append("IS_NEGATIF[9]", this.state.isNegatifAirRadiator);
      formData.append("OPT_FILE[9]", null);

      formData.append("ID_STANDARD[10]", 45);
      formData.append(
        "CHECK_VALUE[10]",
        this.state.airCadanganRadiatorChecking
      );
      formData.append(
        "CHECK_NOTE[10]",
        this.state.airCadanganRadiatorCheckingNote
      );
      formData.append(
        "IS_NEGATIF[10]",
        this.state.isNegatifAirCadanganRadiator
      );
      formData.append("OPT_FILE[10]", null);

      formData.append("ID_STANDARD[11]", 46);
      formData.append("CHECK_VALUE[11]", this.state.airWiperChecking);
      formData.append("CHECK_NOTE[11]", this.state.airWiperCheckingNote);
      formData.append("IS_NEGATIF[11]", this.state.isNegatifAirWiper);
      formData.append("OPT_FILE[11]", null);

      formData.append("ID_STANDARD[12]", 58);
      formData.append("CHECK_VALUE[12]", this.state.banDepanKiriChecking);
      formData.append("CHECK_NOTE[12]", this.state.banDepanKiriCheckingNote);
      formData.append("IS_NEGATIF[12]", this.state.isNegatifBanDepanKiri);
      formData.append("OPT_FILE[12]", null);

      formData.append("ID_STANDARD[13]", 59);
      formData.append("CHECK_VALUE[13]", this.state.banDepanKananChecking);
      formData.append("CHECK_NOTE[13]", this.state.banDepanKananCheckingNote);
      formData.append("IS_NEGATIF[13]", this.state.isNegatifBanDepanKanan);
      formData.append("OPT_FILE[13]", null);

      formData.append("ID_STANDARD[14]", 60);
      formData.append(
        "CHECK_VALUE[14]",
        this.state.banBelakangKiriDalamChecking
      );
      formData.append(
        "CHECK_NOTE[14]",
        this.state.banBelakangKiriDalamCheckingNote
      );
      formData.append(
        "IS_NEGATIF[14]",
        this.state.isNegatifBanBelakangKiriDalam
      );
      formData.append("OPT_FILE[14]", null);

      formData.append("ID_STANDARD[15]", 61);
      formData.append(
        "CHECK_VALUE[15]",
        this.state.banBelakangKiriLuarChecking
      );
      formData.append(
        "CHECK_NOTE[15]",
        this.state.banBelakangKiriLuarCheckingNote
      );
      formData.append(
        "IS_NEGATIF[15]",
        this.state.isNegatifBanBelakangKiriLuar
      );
      formData.append("OPT_FILE[15]", null);

      formData.append("ID_STANDARD[16]", 62);
      formData.append(
        "CHECK_VALUE[16]",
        this.state.banBelakangKananDalamChecking
      );
      formData.append(
        "CHECK_NOTE[16]",
        this.state.banBelakangKananDalamCheckingNote
      );
      formData.append(
        "IS_NEGATIF[16]",
        this.state.isNegatifBanBelakangKananDalam
      );
      formData.append("OPT_FILE[16]", null);

      formData.append("ID_STANDARD[17]", 63);
      formData.append(
        "CHECK_VALUE[17]",
        this.state.banBelakangKananLuarChecking
      );
      formData.append(
        "CHECK_NOTE[17]",
        this.state.banBelakangKananLuarCheckingNote
      );
      formData.append(
        "IS_NEGATIF[17]",
        this.state.isNegatifBanBelakangKananLuar
      );
      formData.append("OPT_FILE[17]", null);

      formData.append("ID_STANDARD[18]", 48);
      if (this.state.lampuCabinChecking == true) {
        formData.append("CHECK_VALUE[18]", "BAIK");
      } else {
        formData.append("CHECK_VALUE[18]", "MATI");
      }
      formData.append("CHECK_NOTE[18]", null);
      formData.append("IS_NEGATIF[18]", 0);
      formData.append("OPT_FILE[18]", null);

      formData.append("ID_STANDARD[19]", 49);
      if (this.state.lampuKotaChecking) {
        formData.append("CHECK_VALUE[19]", "BAIK");
      } else {
        formData.append("CHECK_VALUE[19]", "MATI");
      }
      formData.append("CHECK_NOTE[19]", null);
      formData.append("IS_NEGATIF[19]", 0);
      formData.append("OPT_FILE[19]", null);

      formData.append("ID_STANDARD[20]", 50);
      if (this.state.lampuJauhChecking == true) {
        formData.append("CHECK_VALUE[20]", "BAIK");
      } else {
        formData.append("CHECK_VALUE[20]", "MATI");
      }
      formData.append("CHECK_NOTE[20]", null);
      formData.append("IS_NEGATIF[20]", 0);
      formData.append("OPT_FILE[20]", null);

      formData.append("ID_STANDARD[21]", 51);
      if (this.state.lampuSeinKiriChecking == true) {
        formData.append("CHECK_VALUE[21]", "BAIK");
      } else {
        formData.append("CHECK_VALUE[21]", "MATI");
      }
      formData.append("CHECK_NOTE[21]", null);
      formData.append("IS_NEGATIF[21]", 0);
      formData.append("OPT_FILE[21]", null);

      formData.append("ID_STANDARD[22]", 52);
      if (this.state.lampuSeinKananChecking == true) {
        formData.append("CHECK_VALUE[22]", "BAIK");
      } else {
        formData.append("CHECK_VALUE[22]", "MATI");
      }
      formData.append("CHECK_NOTE[22]", null);
      formData.append("IS_NEGATIF[22]", 0);
      formData.append("OPT_FILE[22]", null);

      formData.append("ID_STANDARD[23]", 53);
      if (this.state.lampuRemChecking == true) {
        formData.append("CHECK_VALUE[23]", "BAIK");
      } else {
        formData.append("CHECK_VALUE[23]", "MATI");
      }
      formData.append("CHECK_NOTE[23]", null);
      formData.append("IS_NEGATIF[23]", 0);
      formData.append("OPT_FILE[23]", null);

      formData.append("ID_STANDARD[24]", 54);
      if (this.state.lampuAtretChecking == true) {
        formData.append("CHECK_VALUE[24]", "BAIK");
      } else {
        formData.append("CHECK_VALUE[24]", "MATI");
      }
      formData.append("CHECK_NOTE[24]", null);
      formData.append("IS_NEGATIF[24]", 0);
      formData.append("OPT_FILE[24]", null);

      formData.append("ID_STANDARD[24]", 55);
      if (this.state.lampuSorotChecking == true) {
        formData.append("CHECK_VALUE[24]", "BAIK");
      } else {
        formData.append("CHECK_VALUE[24]", "MATI");
      }
      formData.append("CHECK_NOTE[24]", null);
      formData.append("IS_NEGATIF[24]", 0);
      formData.append("OPT_FILE[24]", null);

      formData.append("ID_STANDARD[25]", 56);
      if (this.state.dashboardChecking == true) {
        formData.append("CHECK_VALUE[25]", "BAIK");
      } else {
        formData.append("CHECK_VALUE[25]", "MATI");
      }
      formData.append("CHECK_NOTE[25]", null);
      formData.append("IS_NEGATIF[25]", 0);
      formData.append("OPT_FILE[25]", null);

      formData.append("ID_STANDARD[26]", 65);
      if (this.state.spionKiriChecking == true) {
        formData.append("CHECK_VALUE[26]", "BAIK");
      } else {
        formData.append("CHECK_VALUE[26]", "PECAH");
      }
      formData.append("CHECK_NOTE[26]", null);
      formData.append("IS_NEGATIF[26]", 0);
      formData.append("OPT_FILE[26]", null);

      formData.append("ID_STANDARD[27]", 66);
      if (this.state.spionKananChecking == true) {
        formData.append("CHECK_VALUE[27]", "BAIK");
      } else {
        formData.append("CHECK_VALUE[27]", "PECAH");
      }
      formData.append("CHECK_NOTE[27]", null);
      formData.append("IS_NEGATIF[27]", 0);
      formData.append("OPT_FILE[27]", null);

      formData.append("ID_STANDARD[28]", 4);
      if (this.state.dongkrakChecking == true) {
        formData.append("CHECK_VALUE[28]", "ADA");
      } else {
        formData.append("CHECK_VALUE[28]", "TIDAK");
      }
      formData.append("CHECK_NOTE[28]", null);
      formData.append("IS_NEGATIF[28]", 0);
      formData.append("OPT_FILE[28]", null);

      formData.append("ID_STANDARD[29]", 5);
      if (this.state.stangKabinChecking == true) {
        formData.append("CHECK_VALUE[29]", "ADA");
      } else {
        formData.append("CHECK_VALUE[29]", "TIDAK");
      }
      formData.append("CHECK_NOTE[29]", null);
      formData.append("IS_NEGATIF[29]", 0);
      formData.append("OPT_FILE[29]", null);

      formData.append("ID_STANDARD[30]", 6);
      if (this.state.ganjalBanChecking == true) {
        formData.append("CHECK_VALUE[30]", "ADA");
      } else {
        formData.append("CHECK_VALUE[30]", "TIDAK");
      }
      formData.append("CHECK_NOTE[30]", null);
      formData.append("IS_NEGATIF[30]", 0);
      formData.append("OPT_FILE[30]", null);

      formData.append("ID_STANDARD[31]", 7);
      if (this.state.kunciRodaChecking == true) {
        formData.append("CHECK_VALUE[31]", "ADA");
      } else {
        formData.append("CHECK_VALUE[31]", "TIDAK");
      }
      formData.append("CHECK_NOTE[31]", null);
      formData.append("IS_NEGATIF[31]", 0);
      formData.append("OPT_FILE[31]", null);

      formData.append("ID_STANDARD[32]", 8);
      if (this.state.hammerChecking == true) {
        formData.append("CHECK_VALUE[32]", "ADA");
      } else {
        formData.append("CHECK_VALUE[32]", "TIDAK");
      }
      formData.append("CHECK_NOTE[32]", null);
      formData.append("IS_NEGATIF[32]", 0);
      formData.append("OPT_FILE[32]", null);

      formData.append("ID_STANDARD[33]", 9);
      if (this.state.p3kChecking == true) {
        formData.append("CHECK_VALUE[33]", "ADA");
      } else {
        formData.append("CHECK_VALUE[33]", "TIDAK");
      }
      formData.append("CHECK_NOTE[33]", null);
      formData.append("IS_NEGATIF[33]", 0);
      formData.append("OPT_FILE[33]", null);

      formData.append("ID_STANDARD[34]", 31);
      if (this.state.sirineChecking == true) {
        formData.append("CHECK_VALUE[34]", "BAIK");
      } else {
        formData.append("CHECK_VALUE[34]", "MATI");
      }
      formData.append("CHECK_NOTE[34]", null);
      formData.append("IS_NEGATIF[34]", 0);
      formData.append("OPT_FILE[34]", null);

      formData.append("ID_STANDARD[35]", 32);
      if (this.state.accessoriesChecking == true) {
        formData.append("CHECK_VALUE[35]", "ADA");
      } else {
        formData.append("CHECK_VALUE[35]", "TIDAK");
      }
      formData.append("CHECK_NOTE[35]", null);
      formData.append("IS_NEGATIF[35]", 0);
      formData.append("OPT_FILE[35]", null);

      formData.append("ID_STANDARD[36]", 10);
      formData.append("CHECK_VALUE[36]", this.state.hose25Checking);
      formData.append("CHECK_NOTE[36]", this.state.hose25CheckingNote);
      formData.append("IS_NEGATIF[36]", this.state.isNegatifHose25);
      formData.append("OPT_FILE[36]", null);

      formData.append("ID_STANDARD[37]", 11);
      formData.append("CHECK_VALUE[37]", this.state.hose15Checking);
      formData.append("CHECK_NOTE[37]", this.state.hose15CheckingNote);
      formData.append("IS_NEGATIF[37]", this.state.isNegatifHose15);
      formData.append("OPT_FILE[37]", null);

      formData.append("ID_STANDARD[38]", 12);
      formData.append("CHECK_VALUE[38]", this.state.nozzle15jsChecking);
      formData.append("CHECK_NOTE[38]", this.state.nozzle15jsChecking);
      formData.append("IS_NEGATIF[38]", this.state.isNegatifNozzlejs);
      formData.append("OPT_FILE[38]", null);

      formData.append("ID_STANDARD[39]", 13);
      formData.append("CHECK_VALUE[39]", this.state.nozzle15akronChecking);
      formData.append("CHECK_NOTE[39]", this.state.nozzle15akronCheckingNote);
      formData.append("IS_NEGATIF[39]", this.state.isNegatifNozzleakron);
      formData.append("OPT_FILE[39]", null);

      formData.append("ID_STANDARD[40]", 14);
      formData.append("CHECK_VALUE[40]", this.state.nozzle15jetChecking);
      formData.append("CHECK_NOTE[40]", this.state.nozzle15jetCheckingNote);
      formData.append("IS_NEGATIF[40]", this.state.isNegatifNozzlejet);
      formData.append("OPT_FILE[40]", null);

      formData.append("ID_STANDARD[41]", 15);
      formData.append("CHECK_VALUE[41]", this.state.yvalveChecking);
      formData.append("CHECK_NOTE[41]", this.state.yvalveCheckingNote);
      formData.append("IS_NEGATIF[41]", this.state.isNegatifYvalve);
      formData.append("OPT_FILE[41]", null);

      formData.append("ID_STANDARD[42]", 16);
      formData.append("CHECK_VALUE[42]", this.state.red25to15Checking);
      formData.append("CHECK_NOTE[42]", this.state.red25to15CheckingNote);
      formData.append("IS_NEGATIF[42]", this.state.isNegatifRed25to15);
      formData.append("OPT_FILE[42]", null);

      formData.append("ID_STANDARD[43]", 17);
      formData.append("CHECK_VALUE[43]", this.state.nozzlefoamChecking);
      formData.append("CHECK_NOTE[43]", this.state.nozzlefoamCheckingNote);
      formData.append("IS_NEGATIF[43]", this.state.isNegatifNozzlefoam);
      formData.append("OPT_FILE[43]", null);

      formData.append("ID_STANDARD[44]", 18);
      formData.append("CHECK_VALUE[44]", this.state.kunciHydrantChecking);
      formData.append("CHECK_NOTE[44]", this.state.kunciHydrantCheckingNote);
      formData.append("IS_NEGATIF[44]", this.state.isNegatifKunciHydrant);
      formData.append("OPT_FILE[44]", null);

      formData.append("ID_STANDARD[45]", 19);
      formData.append("CHECK_VALUE[45]", this.state.hoseSuctionChecking);
      formData.append("CHECK_NOTE[45]", this.state.hoseSuctionCheckingNote);
      formData.append("IS_NEGATIF[45]", this.state.isNegatifHoseSuction);
      formData.append("OPT_FILE[45]", null);

      formData.append("ID_STANDARD[46]", 20);
      formData.append("CHECK_VALUE[46]", this.state.kunciHoseSuctionChecking);
      formData.append("CHECK_NOTE[46]", this.state.kunciHoseSuctionCheckingNote);
      formData.append("IS_NEGATIF[46]", this.state.isNegatifKunciHoseSuction);
      formData.append("OPT_FILE[46]", null);

      formData.append("ID_STANDARD[47]", 21);
      formData.append("CHECK_VALUE[47]", this.state.aparChecking);
      formData.append("CHECK_NOTE[47]", this.state.aparCheckingNote);
      formData.append("IS_NEGATIF[47]", this.state.isNegatifApar);
      formData.append("OPT_FILE[47]", null);

      formData.append("ID_STANDARD[48]", 22);
      formData.append("CHECK_VALUE[48]", this.state.kapakChecking);
      formData.append("CHECK_NOTE[48]", this.state.kapakCheckingNote);
      formData.append("IS_NEGATIF[48]", this.state.isNegatifKapak);
      formData.append("OPT_FILE[48]", null);

      formData.append("ID_STANDARD[49]", 23);
      formData.append("CHECK_VALUE[49]", this.state.linggisChecking);
      formData.append("CHECK_NOTE[49]", this.state.linggisCheckingNote);
      formData.append("IS_NEGATIF[49]", this.state.isNegatifLinggis);
      formData.append("OPT_FILE[49]", null);

      formData.append("ID_STANDARD[50]", 24);
      formData.append("CHECK_VALUE[50]", this.state.tanggaGandaChecking);
      formData.append("CHECK_NOTE[50]", this.state.tanggaGandaCheckingNote);
      formData.append("IS_NEGATIF[50]", this.state.isNegatifTanggaGanda);
      formData.append("OPT_FILE[50]", null);

      formData.append("ID_STANDARD[51]", 25);
      formData.append("CHECK_VALUE[51]", this.state.senterChecking);
      formData.append("CHECK_NOTE[51]", this.state.senterCheckingNote);
      formData.append("IS_NEGATIF[51]", this.state.isNegatifSenter);
      formData.append("OPT_FILE[51]", null);

      formData.append("ID_STANDARD[52]", 26);
      formData.append("CHECK_VALUE[52]", this.state.gantolChecking);
      formData.append("CHECK_NOTE[52]", this.state.gantolCheckingNote);
      formData.append("IS_NEGATIF[52]", this.state.isNegatifGantol);
      formData.append("OPT_FILE[52]", null);

      formData.append("ID_STANDARD[53]", 27);
      formData.append("CHECK_VALUE[53]", this.state.timbaChecking);
      formData.append("CHECK_NOTE[53]", this.state.timbaCheckingNote);
      formData.append("IS_NEGATIF[53]", this.state.isNegatifTimba);
      formData.append("OPT_FILE[53]", null);

      formData.append("ID_STANDARD[54]", 28);
      formData.append("CHECK_VALUE[54]", this.state.karungChecking);
      formData.append("CHECK_NOTE[54]", this.state.karungCheckingNote);
      formData.append("IS_NEGATIF[54]", this.state.isNegatifKarung);
      formData.append("OPT_FILE[54]", null);

      formData.append("ID_STANDARD[55]", 29);
      formData.append("CHECK_VALUE[55]", this.state.jaketPMKChecking);
      formData.append("CHECK_NOTE[55]", this.state.jaketPMKCheckingNote);
      formData.append("IS_NEGATIF[55]", this.state.isNegatifJaketPMK);
      formData.append("OPT_FILE[55]", null);

      formData.append("ID_STANDARD[56]", 30);
      formData.append("CHECK_VALUE[56]", this.state.helmPMKChecking);
      formData.append("CHECK_NOTE[56]", this.state.helmPMKCheckingNote);
      formData.append("IS_NEGATIF[56]", this.state.isNegatifHelmPMK);
      formData.append("OPT_FILE[56]", null);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.Status == 200) {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Success", "Update Unit Check PMK Success", [
              {
                text: "Okay"
              }
            ]);
            this.props.navigation.navigate("UnitCheckPMK");
          } else {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Error", "Update Unit Check PMK Failed", [
              {
                text: "Okay"
              }
            ]);
          }
          // alert(responseJson);
        })
        .catch(error => {
          this.setState({
            visibleDialogSubmit: false
          });
          Alert.alert("Error", "Update Unit Check PMK Failed", [
            {
              text: "Okay"
            }
          ]);
          this.props.navigation.navigate("UnitCheckPMK");
          console.log(error);
        });
    });
  }

  render() {
    that = this;
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left>
            {this.state.tabNumber == "tab1" ? (
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            ) : this.state.tabNumber == "tab2" ? (
              <Button
                transparent
                onPress={() =>
                  this.setState({
                    tabNumber: "tab1"
                  })
                }
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            ) : this.state.tabNumber == "tab3" ? (
              <Button
                transparent
                onPress={() =>
                  this.setState({
                    tabNumber: "tab2"
                  })
                }
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            ) : (
              <Button
                transparent
                onPress={() =>
                  this.setState({
                    tabNumber: "tab3"
                  })
                }
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            )}
          </Left>
          <Body>
            <Title style={styles.textbody}>Unit Check Mobil PMK</Title>
          </Body>
          {/* <Right/> */}
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ flex: 1 }}>
          <Content style={{ marginTop: 0, backgroundColor: colors.gray }}>
            <View style={{ backgroundColor: colors.gray }}>
              {this.state.tabNumber == "tab1" ? (
                <View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Pemeriksaan
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: colors.graydar
                          }}
                        >
                          Kondisi PMK
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: colors.graydar
                          }}
                        >
                          Kondisi Sparepart
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: colors.graydar
                          }}
                        >
                          Kondisi Tools
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={styles.titleInput}>Vehicle Code *</Text>
                      </View>
                      <TextInput
                        style={{
                          height: 40,
                          marginLeft: 5,
                          marginRight: 5,
                          marginBottom: 5,
                          fontSize: 11,
                          borderWidth: 1,
                          borderColor: colors.lightGray
                        }}
                        onChangeText={text =>
                          this.setState({ vehicleCode: text })
                        }
                        value={this.state.headerUnitCheck.VEHICLE_CODE}
                        bordered
                        editable={false}
                        placeholder="Vehicle Code"
                      />
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "50%" }}>
                        <Text style={styles.titleInput}>
                          Tanggal Pemeriksaan *
                        </Text>
                        <DatePicker
                          style={{
                            width: "100%",
                            fontSize: 10,
                            borderRadius: 20
                          }}
                          date={this.state.dateChecking}
                          mode="date"
                          placeholder="Choose Date ..."
                          format="YYYY-MM-DD"
                          minDate="2018-01-01"
                          maxDate="2050-12-31"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          // iconSource='{this.timeIcon}'
                          customStyles={{
                            dateInput: {
                              marginLeft: 5,
                              marginRight: 5,
                              height: 35,
                              borderRadius: 5,
                              fontSize: 10,
                              borderWidth: 1,
                              borderColor: "#E6E6E6"
                            },
                            dateIcon: {
                              position: "absolute",
                              left: 0,
                              top: 5
                            }
                          }}
                          onDateChange={date => {
                            this.setState({ dateChecking: date });
                          }}
                        />
                      </View>
                      <View style={{ width: "50%" }}>
                        <Text style={styles.titleInput}>Jam Pemeriksaan *</Text>
                        <DatePicker
                          style={{
                            width: "100%",
                            fontSize: 10,
                            borderRadius: 20
                          }}
                          date={this.state.timeChecking}
                          mode="time"
                          placeholder="Choose Time ..."
                          format="hh:mm"
                          minTime="00:00"
                          maxTime="23:59"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          // iconSource='{this.timeIcon}'
                          customStyles={{
                            dateInput: {
                              marginLeft: 5,
                              marginRight: 5,
                              height: 35,
                              borderRadius: 5,
                              fontSize: 10,
                              borderWidth: 1,
                              borderColor: "#E6E6E6"
                            },
                            dateIcon: {
                              position: "absolute",
                              left: 0,
                              top: 5
                            }
                          }}
                          onDateChange={time => {
                            this.setState({ timeChecking: time });
                          }}
                        />
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={styles.titleInput}>Shift *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: "100%", height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.shiftChecking}
                            onValueChange={itemValue =>
                              this.setState({ shiftChecking: itemValue })
                            }
                          >
                            <Picker.Item label="Shift I (Satu)" value="1" />
                            <Picker.Item label="Shift II (Dua)" value="2" />
                            <Picker.Item label="Shift III (Tiga)" value="3" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Pemanasan (Menit)
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ pemanasanChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifPemanasan}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifPemanasan: itemValue,
                                  visiblePemanasan: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visiblePemanasan}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visiblePemanasan: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitPemanasanNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan pemanasan (Optional)"
                                onChangeText={text =>
                                  this.setState({ pemanasanCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Speedometer (Km)
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ speedometerChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifSpeedometer}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifSpeedometer: itemValue,
                                  visibleSpeedometer: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleSpeedometer}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleSpeedometer: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitSpeedometerNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan speedometer (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    speedometerCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View>
                        <Text style={styles.titleInput}>Level BBM</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 25,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.bbmChecking}
                            onValueChange={itemValue =>
                              this.setState({ bbmChecking: itemValue })
                            }
                          >
                            <Picker.Item label="R" value="R" />
                            <Picker.Item label="1/4" value="1/4" />
                            <Picker.Item label="1/2" value="1/2" />
                            <Picker.Item label="3/4" value="3/4" />
                            <Picker.Item label="F" value="F" />
                          </Picker>
                        </Form>
                      </View>
                      <View>
                        <Text style={styles.titleInput}>Masuk Negatif *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ height: 35, width: 200 }}
                            placeholder="Select ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.isNegatifBbm}
                            onValueChange={itemValue =>
                              this.setState({
                                isNegatifBbm: itemValue,
                                visibleBbm: true
                              })
                            }
                          >
                            <Picker.Item
                              label="Tidak Masuk Negatif List"
                              value="0"
                              disable={true}
                            />
                            <Picker.Item label="Masuk Negatif List" value="1" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleBbm}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleBbm: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitBbmNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan bbm (Optional)"
                                onChangeText={text =>
                                  this.setState({ bbmCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Working Pressure (Bar)
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ workPressChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifWorkPress}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifWorkPress: itemValue,
                                  visibleWorkPress: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleWorkPress}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleWorkPress: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitWorkPressNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan work pressure (Optional)"
                                onChangeText={text =>
                                  this.setState({ workPressCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Hour Meter Mesin (Jam)
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ hourMeterMesinChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifHourMeterMesin}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifHourMeterMesin: itemValue,
                                  visibleHourMeterMesin: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleHourMeterMesin}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleHourMeterMesin: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitHourMeterMesinNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan hour meter mesin (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    hourMeterMesinCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Hour Meter Pompa (Jam)
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ hourMeterPompaChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifHourMeterPompa}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifHourMeterPompa: itemValue,
                                  visibleHourMeterPompa: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleHourMeterPompa}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleHourMeterPompa: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitHourMeterPompaNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan hour meter pompa (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    hourMeterPompaCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  {/* <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                        <View style={{ flex: 1 }}>
                                            <View style={{ paddingBottom: 5 }}>
                                                <Text style={styles.viewMerkFoto}>Upload Foto (jpg, png) max : 500 kb</Text>
                                            </View>
                                            <View>
                                                <Ripple
                                                    style={{
                                                        flex: 2,
                                                        justifyContent: "center",
                                                        alignItems: "center"
                                                    }}
                                                    rippleSize={176}
                                                    rippleDuration={600}
                                                    rippleContainerBorderRadius={15}
                                                    onPress={this.pickImageHandler}
                                                    rippleColor={colors.accent}
                                                >
                                                    <View style={styles.placeholder}>
                                                        <Image source={{ uri: this.state.pickedImage }} style={styles.previewImage} />
                                                    </View>
                                                </Ripple>
                                            </View>
                                        </View>
                                    </CardItem> */}
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View style={styles.Contentsave}>
                        <Button
                          block
                          style={{
                            width: "100%",
                            height: 45,
                            marginBottom: 20,
                            marginTop: 15,
                            borderWidth: 1,
                            backgroundColor: "#00b300",
                            borderColor: "#00b300",
                            borderRadius: 4
                          }}
                          onPress={() => this.validasiFieldTab1()}
                        >
                          <Text
                            style={{ fontWeight: "bold", color: colors.white }}
                          >
                            Lanjutkan
                          </Text>
                        </Button>
                      </View>
                    </View>
                  </CardItem>
                </View>
              ) : this.state.tabNumber == "tab2" ? (
                <View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Pemeriksaan
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Kondisi PMK
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: colors.graydar
                          }}
                        >
                          Kondisi Sparepart
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: colors.graydar
                          }}
                        >
                          Kondisi Tools
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ marginLeft: 0, flex: 1 }}>
                      <Text
                        style={{
                          fontSize: 12,
                          paddingTop: 10,
                          fontWeight: "bold"
                        }}
                      >
                        Kondisi Oli :{" "}
                      </Text>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View>
                        <Text style={styles.titleInput}>Oli Mesin</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 50,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 75, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.oliMesinChecking}
                            onValueChange={itemValue =>
                              this.setState({ oliMesinChecking: itemValue })
                            }
                          >
                            <Picker.Item label="L" value="L" />
                            <Picker.Item label="M" value="M" />
                            <Picker.Item label="H" value="H" />
                          </Picker>
                        </Form>
                      </View>
                      <View>
                        <Text style={styles.titleInput}>Masuk Negatif *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ height: 35, width: 200 }}
                            placeholder="Select ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.isNegatifOliMesin}
                            onValueChange={itemValue =>
                              this.setState({
                                isNegatifOliMesin: itemValue,
                                visibleOliMesin: true
                              })
                            }
                          >
                            <Picker.Item
                              label="Tidak Masuk Negatif List"
                              value="0"
                            />
                            <Picker.Item label="Masuk Negatif List" value="1" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleOliMesin}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleOliMesin: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitOliMesinNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan oli mesin (Optional)"
                                onChangeText={text =>
                                  this.setState({ oliMesinCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View>
                        <Text style={styles.titleInput}>Oli Rem</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 50,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 75, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.oliRemChecking}
                            onValueChange={itemValue =>
                              this.setState({ oliRemChecking: itemValue })
                            }
                          >
                            <Picker.Item label="L" value="L" />
                            <Picker.Item label="M" value="M" />
                            <Picker.Item label="H" value="H" />
                          </Picker>
                        </Form>
                      </View>
                      <View>
                        <Text style={styles.titleInput}>Masuk Negatif *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ height: 35, width: 200 }}
                            placeholder="Select ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.isNegatifOliRem}
                            onValueChange={itemValue =>
                              this.setState({
                                isNegatifOliRem: itemValue,
                                visibleOliRem: true
                              })
                            }
                          >
                            <Picker.Item
                              label="Tidak Masuk Negatif List"
                              value="0"
                            />
                            <Picker.Item label="Masuk Negatif List" value="1" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleOliRem}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleOliRem: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitOliRemNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan oli rem (Optional)"
                                onChangeText={text =>
                                  this.setState({ oliRemCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View>
                        <Text style={styles.titleInput}>
                          Oli Power Steering
                        </Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 50,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 75, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.oliPowerSteeringChecking}
                            onValueChange={itemValue =>
                              this.setState({
                                oliPowerSteeringChecking: itemValue
                              })
                            }
                          >
                            <Picker.Item label="L" value="L" />
                            <Picker.Item label="M" value="M" />
                            <Picker.Item label="H" value="H" />
                          </Picker>
                        </Form>
                      </View>
                      <View>
                        <Text style={styles.titleInput}>Masuk Negatif *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ height: 35, width: 200 }}
                            placeholder="Select ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.isNegatifOliPowerSteering}
                            onValueChange={itemValue =>
                              this.setState({
                                isNegatifOliPowerSteering: itemValue,
                                visibleOliPowerSteering: true
                              })
                            }
                          >
                            <Picker.Item
                              label="Tidak Masuk Negatif List"
                              value="0"
                            />
                            <Picker.Item label="Masuk Negatif List" value="1" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleOliPowerSteering}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleOliPowerSteering: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitOliPowerSteeringNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan oli power steering (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    oliPowerSteeringCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ marginLeft: 0, flex: 1 }}>
                      <Text
                        style={{
                          fontSize: 12,
                          paddingTop: 10,
                          fontWeight: "bold"
                        }}
                      >
                        Kondisi Air :{" "}
                      </Text>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View>
                        <Text style={styles.titleInput}>Air Radiator</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 25,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.airRadiatorChecking}
                            onValueChange={itemValue =>
                              this.setState({ airRadiatorChecking: itemValue })
                            }
                          >
                            <Picker.Item label="R" value="R" />
                            <Picker.Item label="1/4" value="1/4" />
                            <Picker.Item label="1/2" value="1/2" />
                            <Picker.Item label="3/4" value="3/4" />
                            <Picker.Item label="F" value="F" />
                          </Picker>
                        </Form>
                      </View>
                      <View>
                        <Text style={styles.titleInput}>Masuk Negatif *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ height: 35, width: 200 }}
                            placeholder="Select ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.isNegatifAirRadiator}
                            onValueChange={itemValue =>
                              this.setState({
                                isNegatifAirRadiator: itemValue,
                                visibleAirRadiator: true
                              })
                            }
                          >
                            <Picker.Item
                              label="Tidak Masuk Negatif List"
                              value="0"
                            />
                            <Picker.Item label="Masuk Negatif List" value="1" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleAirRadiator}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleAirRadiator: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitAirRadiatorNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan air radiator (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    airRadiatorCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View>
                        <Text style={styles.titleInput}>
                          Air Cadangan Radiator
                        </Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 25,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={
                              this.state.airCadanganRadiatorChecking
                            }
                            onValueChange={itemValue =>
                              this.setState({
                                airCadanganRadiatorChecking: itemValue
                              })
                            }
                          >
                            <Picker.Item label="R" value="R" />
                            <Picker.Item label="1/4" value="1/4" />
                            <Picker.Item label="1/2" value="1/2" />
                            <Picker.Item label="3/4" value="3/4" />
                            <Picker.Item label="F" value="F" />
                          </Picker>
                        </Form>
                      </View>
                      <View>
                        <Text style={styles.titleInput}>Masuk Negatif *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ height: 35, width: 200 }}
                            placeholder="Select ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={
                              this.state.isNegatifAirCadanganRadiator
                            }
                            onValueChange={itemValue =>
                              this.setState({
                                isNegatifAirCadanganRadiator: itemValue,
                                visibleAirCadanganRadiator: true
                              })
                            }
                          >
                            <Picker.Item
                              label="Tidak Masuk Negatif List"
                              value="0"
                            />
                            <Picker.Item label="Masuk Negatif List" value="1" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleAirCadanganRadiator}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleAirCadanganRadiator: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitAirCadanganRadiatorNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan air cadangan radiator (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    airCadanganRadiatorCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View>
                        <Text style={styles.titleInput}>Air Wiper</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 25,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.airWiperChecking}
                            onValueChange={itemValue =>
                              this.setState({ airWiperChecking: itemValue })
                            }
                          >
                            <Picker.Item label="R" value="R" />
                            <Picker.Item label="1/4" value="1/4" />
                            <Picker.Item label="1/2" value="1/2" />
                            <Picker.Item label="3/4" value="3/4" />
                            <Picker.Item label="F" value="F" />
                          </Picker>
                        </Form>
                      </View>
                      <View>
                        <Text style={styles.titleInput}>Masuk Negatif *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ height: 35, width: 200 }}
                            placeholder="Select ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.isNegatifAirWiper}
                            onValueChange={itemValue =>
                              this.setState({
                                isNegatifAirWiper: itemValue,
                                visibleAirWiper: true
                              })
                            }
                          >
                            <Picker.Item
                              label="Tidak Masuk Negatif List"
                              value="0"
                            />
                            <Picker.Item label="Masuk Negatif List" value="1" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleAirWiper}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleAirWiper: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitAirWiperNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan air wiper (Optional)"
                                onChangeText={text =>
                                  this.setState({ airWiperCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ marginLeft: 0, flex: 1 }}>
                      <Text
                        style={{
                          fontSize: 12,
                          paddingTop: 10,
                          fontWeight: "bold"
                        }}
                      >
                        Kondisi Tekanan Ban (Bar) :{" "}
                      </Text>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>Ban Depan Kiri</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banDepanKiriChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifBanDepanKiri}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifBanDepanKiri: itemValue,
                                  visibleBanDepanKiri: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleBanDepanKiri}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleBanDepanKiri: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitBanDepanKiriNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan ban depan kiri (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    banDepanKiriCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>Ban Depan Kanan</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banDepanKananChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifBanDepanKanan}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifBanDepanKanan: itemValue,
                                  visibleBanDepanKanan: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleBanDepanKanan}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleBanDepanKanan: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitBanDepanKananNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan ban depan kanan (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    banDepanKananCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Ban Belakang Kiri Dalam
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({
                                banBelakangKiriDalamChecking: text
                              })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={
                                this.state.isNegatifBanBelakangKiriDalam
                              }
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifBanBelakangKiriDalam: itemValue,
                                  visibleBanBelakangKiriDalam: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleBanBelakangKiriDalam}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleBanBelakangKiriDalam: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitBanBelakangKiriDalamNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan ban belakang kiri dalam (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    banBelakangKiriDalamCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Ban Belakang Kiri Luar
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({
                                banBelakangKiriLuarChecking: text
                              })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={
                                this.state.isNegatifBanBelakangKiriLuar
                              }
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifBanBelakangKiriLuar: itemValue,
                                  visibleBanBelakangKiriLuar: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleBanBelakangKiriLuar}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleBanBelakangKiriLuar: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitBanBelakangKiriLuarNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan ban belakang kiri luar (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    banBelakangKiriLuarCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Ban Belakang Kanan Dalam
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({
                                banBelakangKananDalamChecking: text
                              })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={
                                this.state.isNegatifBanBelakangKananDalam
                              }
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifBanBelakangKananDalam: itemValue,
                                  visibleBanBelakangKananDalam: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleBanBelakangKananDalam}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleBanBelakangKananDalam: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitBanBelakangKananDalamNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan ban belakang kanan dalam (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    banBelakangKananDalamCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Ban Belakang Kanan Luar
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({
                                banBelakangKananLuarChecking: text
                              })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={
                                this.state.isNegatifBanBelakangKananLuar
                              }
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifBanBelakangKananLuar: itemValue,
                                  visibleBanBelakangKananLuar: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleBanBelakangKananLuar}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleBanBelakangKananLuar: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitBanBelakangKananLuarNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan ban belakang kanan luar (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    banBelakangKananLuarCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View style={styles.Contentsave}>
                        <Button
                          block
                          style={{
                            width: "100%",
                            height: 45,
                            marginBottom: 20,
                            marginTop: 15,
                            borderWidth: 1,
                            backgroundColor: "#00b300",
                            borderColor: "#00b300",
                            borderRadius: 4
                          }}
                          onPress={() => this.validasiFieldTab2()}
                        >
                          <Text
                            style={{ fontWeight: "bold", color: colors.white }}
                          >
                            Lanjutkan
                          </Text>
                        </Button>
                      </View>
                    </View>
                  </CardItem>
                </View>
              ) : this.state.tabNumber == "tab3" ? (
                <View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Pemeriksaan
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Kondisi PMK
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Kondisi Sparepart
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Kondisi Tools
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ marginLeft: 10, flex: 2, width: "33%" }}>
                        <Text
                          style={{
                            fontSize: 10,
                            paddingTop: 10,
                            fontWeight: "bold"
                          }}
                        >
                          Kondisi Lampu Lampu :{" "}
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Cabin
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuCabinChecking}
                            onValueChange={itemValue =>
                              this.setState({ lampuCabinChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Kota
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuKotaChecking}
                            onValueChange={itemValue =>
                              this.setState({ lampuKotaChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Jauh
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuJauhChecking}
                            onValueChange={itemValue =>
                              this.setState({ lampuJauhChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Sein Kiri
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuSeinKiriChecking}
                            onValueChange={itemValue =>
                              this.setState({
                                lampuSeinKiriChecking: itemValue
                              })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Sein Kanan
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuSeinKananChecking}
                            onValueChange={itemValue =>
                              this.setState({
                                lampuSeinKananChecking: itemValue
                              })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Rem
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuRemChecking}
                            onValueChange={itemValue =>
                              this.setState({ lampuRemChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Atret
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuAtretChecking}
                            onValueChange={itemValue =>
                              this.setState({ lampuAtretChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Sorot
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuSorotChecking}
                            onValueChange={itemValue =>
                              this.setState({ lampuSorotChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Panel Dashboard
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.dashboardChecking}
                            onValueChange={itemValue =>
                              this.setState({ dashboardChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ marginLeft: 10, flex: 2, width: "33%" }}>
                        <Text
                          style={{
                            fontSize: 10,
                            paddingTop: 10,
                            fontWeight: "bold"
                          }}
                        >
                          Kondisi Kaca Spion :{" "}
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Kaca Spion Kiri
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.spionKiriChecking}
                            onValueChange={itemValue =>
                              this.setState({ spionKiriChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Kaca Spion Kanan
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.spionKananChecking}
                            onValueChange={itemValue =>
                              this.setState({ spionKananChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                    </View>
                  </CardItem>
                  {/* <CardItem style={{ borderRadius: 0, backgroundColor: colors.gray }}>
                                        <View style={{ flex: 1, flexDirection: "column" }}>
                                            <View style={{ marginLeft: 10, flex: 2 }}>
                                                <Text note style={{ fontSize: 10, paddingTop: 10 }}>Catatan</Text>
                                            </View>
                                            <View>
                                                <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11, }}
                                                    rowSpan={2}
                                                    bordered value={this.state.pekerjaan}
                                                    placeholder='Type something .. '
                                                    onChangeText={(text) => this.setState({ pekerjaan: text })} />
                                            </View>
                                        </View>
                                    </CardItem> */}
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View style={styles.Contentsave}>
                        <Button
                          block
                          style={{
                            width: "100%",
                            height: 45,
                            marginBottom: 20,
                            marginTop: 15,
                            borderWidth: 1,
                            backgroundColor: "#00b300",
                            borderColor: "#00b300",
                            borderRadius: 4
                          }}
                          onPress={() => this.validasiFieldTab3()}
                        >
                          <Text
                            style={{ color: colors.white, fontWeight: "bold" }}
                          >
                            Lanjutkan
                          </Text>
                        </Button>
                      </View>
                    </View>
                  </CardItem>
                </View>
              ) : (
                <View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Pemeriksaan
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Kondisi PMK
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Kondisi Sparepart
                        </Text>
                      </View>
                      <View style={{ width: "25%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Kondisi Tools
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ marginLeft: 10, flex: 2, width: "33%" }}>
                        <Text
                          style={{
                            fontSize: 10,
                            paddingTop: 10,
                            fontWeight: "bold"
                          }}
                        >
                          Tool Set :{" "}
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Dongkrak
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.dongkrakChecking}
                            onValueChange={itemValue =>
                              this.setState({ dongkrakChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Stang Kabin
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.stangKabinChecking}
                            onValueChange={itemValue =>
                              this.setState({ stangKabinChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Ganjal Ban
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.ganjalBanChecking}
                            onValueChange={itemValue =>
                              this.setState({ ganjalBanChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Kunci Roda
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.kunciRodaChecking}
                            onValueChange={itemValue =>
                              this.setState({ kunciRodaChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Hammer
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.hammerChecking}
                            onValueChange={itemValue =>
                              this.setState({ hammerChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Kotak P3K
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.p3kChecking}
                            onValueChange={itemValue =>
                              this.setState({ p3kChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ marginLeft: 10, flex: 2, width: "33%" }}>
                        <Text
                          style={{
                            fontSize: 10,
                            paddingTop: 10,
                            fontWeight: "bold"
                          }}
                        >
                          Sirine dan Accessories :{" "}
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Sirine
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.sirineChecking}
                            onValueChange={itemValue =>
                              this.setState({ sirineChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Accessories
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.accessoriesChecking}
                            onValueChange={itemValue =>
                              this.setState({ accessoriesChecking: itemValue })
                            }
                          />
                        </Right>
                      </ListItem>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Hose 2.5"
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ hose25Checking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifHose25}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifHose25: itemValue,
                                  visibleHose25: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleHose25}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleHose25: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitHose25Note()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan hose 2.5'' (Optional)"
                                onChangeText={text =>
                                  this.setState({ hose25CheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Hose 1.5"
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ hose15Checking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifHose15}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifHose15: itemValue,
                                  visibleHose15: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleHose15}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleHose15: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitHose15Note()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan hose 1.5'' (Optional)"
                                onChangeText={text =>
                                  this.setState({ hose15CheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Nozzle 1.5" J/S
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ nozzle15jsChecking : text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifNozzlejs}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifNozzlejs: itemValue,
                                  visibleNozzlejs: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleNozzlejs}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleNozzlejs: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitNozzle15jsNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan nozzle 1.5'' j/s (Optional)"
                                onChangeText={text =>
                                  this.setState({ nozzle15jsCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Nozzle 1.5" Akron
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ nozzle15akronChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifNozzleakron}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifNozzleakron: itemValue,
                                  visibleNozzleakron: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleNozzleakron}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleNozzleakron: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitNozzle15akronNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan nozzle 1.5'' akron (Optional)"
                                onChangeText={text =>
                                  this.setState({ nozzle15akronCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Nozzle 1.5" Jet
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ nozzle15jetChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifNozzlejet}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifNozzlejet: itemValue,
                                  visibleNozzlejet: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleNozzlejet}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleNozzlejet: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitNozzle15jetNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan nozzle 1.5'' jet (Optional)"
                                onChangeText={text =>
                                  this.setState({ nozzle15jetCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Y Valve
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ yvalveChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifYvalve}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifYvalve: itemValue,
                                  visibleYvalve: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleYvalve}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleYvalve: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitYvalveNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan y valve (Optional)"
                                onChangeText={text =>
                                  this.setState({ yvalveCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Red 2.5" to 1.5"
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ red25to15Checking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifRed25to15}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifRed25to15: itemValue,
                                  visibleRed25to15: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleRed25to15}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleRed25to15: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitRed25to15Note()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan red 25'' to 15'' (Optional)"
                                onChangeText={text =>
                                  this.setState({ red25to15CheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Nozzle Foam
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ nozzlefoamChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifNozzlefoam}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifNozzlefoam: itemValue,
                                  visibleNozzlefoam: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleNozzlefoam}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleNozzlefoam: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitNozzlefoamNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan nozzle foam (Optional)"
                                onChangeText={text =>
                                  this.setState({ nozzlefoamCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Kunci Hydrant
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ kunciHydrantChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifKunciHydrant}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifKunciHydrant: itemValue,
                                  visibleKunciHydrant: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleKunciHydrant}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleKunciHydrant: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitKunciHydrantNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan kunci hydrant (Optional)"
                                onChangeText={text =>
                                  this.setState({ kunciHydrantCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Hose Suction 4"
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ hoseSuctionChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifHoseSuction}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifHoseSuction: itemValue,
                                  visibleHoseSuction: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleHoseSuction}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleHoseSuction: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitHoseSuctionNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan hose suction 4'' (Optional)"
                                onChangeText={text =>
                                  this.setState({ hoseSuctionCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Kunci Hose Suction 4"
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ kunciHoseSuctionChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifKunciHoseSuction}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifKunciHoseSuction: itemValue,
                                  visibleKunciHoseSuction: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleKunciHoseSuction}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleKunciHoseSuction: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitKunciHoseSuctionNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan kunci hose suction 4'' (Optional)"
                                onChangeText={text =>
                                  this.setState({ kunciHoseSuctionCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Apar
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ aparChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifApar}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifApar: itemValue,
                                  visibleApar: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleApar}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleApar: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitAparNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan apar (Optional)"
                                onChangeText={text =>
                                  this.setState({ aparCheckingNote: text})
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Kapak
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ kapakChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifKapak}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifKapak: itemValue,
                                  visibleKapak: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleKapak}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleKapak: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitKapakNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan kapak (Optional)"
                                onChangeText={text =>
                                  this.setState({ kapakCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Linggis
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ linggisChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifLinggis}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifLinggis: itemValue,
                                  visibleLinggis: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleLinggis}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleLinggis: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitLinggisNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan linggis (Optional)"
                                onChangeText={text =>
                                  this.setState({ linggisCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Tangga Ganda
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ tanggaGandaChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifTanggaGanda}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifTanggaGanda: itemValue,
                                  visibleTanggaGanda: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleTanggaGanda}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleTanggaGanda: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitTanggaGandaNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan tangga ganda (Optional)"
                                onChangeText={text =>
                                  this.setState({ tanggaGandaCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Senter
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ senterChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifSenter}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifSenter: itemValue,
                                  visibleSenter: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleSenter}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleHoseSuction: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitSenterNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan senter (Optional)"
                                onChangeText={text =>
                                  this.setState({ senterCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Gantol / Pengait
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ gantolChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifGantol}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifGantol: itemValue,
                                  visibleGantol: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleGantol}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleGantol: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitGantolNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan gantol/pengait (Optional)"
                                onChangeText={text =>
                                  this.setState({ gantolCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Timba
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ timbaChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifTimba}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifTimba: itemValue,
                                  visibleTimba: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleTimba}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleTimba: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitTimbaNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan timba (Optional)"
                                onChangeText={text =>
                                  this.setState({ timbaCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Karung
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ karungChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifKarung}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifKarung: itemValue,
                                  visibleKarung: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleKarung}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleKarung: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitKarungNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan karung (Optional)"
                                onChangeText={text =>
                                  this.setState({ karungCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Jaket PMK
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ jaketPMKChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifJaketPMK}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifJaketPMK: itemValue,
                                  visibleJaketPMK: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleJaketPMK}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleJaketPMK: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitJaketPMKNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan jaket PMK (Optional)"
                                onChangeText={text =>
                                  this.setState({ jaketPMKCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Helm PMK
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType='numeric'
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ helmPMKChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifHelmPMK}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifHelmPMK: itemValue,
                                  visibleHelmPMK: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleHelmPMK}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleHelmPMK: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitHelmPMKNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                placeholder="Catatan helm PMK (Optional)"
                                onChangeText={text =>
                                  this.setState({ helmPMKCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View style={styles.Contentsave}>
                        <Button
                          block
                          style={{
                            width: "100%",
                            height: 45,
                            marginBottom: 20,
                            marginTop: 15,
                            borderWidth: 1,
                            backgroundColor: "#00b300",
                            borderColor: "#00b300",
                            borderRadius: 4
                          }}
                          onPress={() => this.unitCheckPMKCreate()}
                        >
                          <Text
                            style={{ color: colors.white, fontWeight: "bold" }}
                          >
                            SUBMIT
                          </Text>
                        </Button>
                      </View>
                    </View>
                  </CardItem>
                </View>
              )}
            </View>
          </Content>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog visible={this.state.visibleLoadingUnitKerja}>
            <DialogContent>
              {<ActivityIndicator size="large" color="#330066" animating />}
            </DialogContent>
          </Dialog>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialogSubmit}
            dialogTitle={<DialogTitle title="Creating Report.." />}
          >
            <DialogContent>
              {<ActivityIndicator size="large" color="#330066" animating />}
            </DialogContent>
          </Dialog>
        </View>
        <CustomFooter navigation={this.props.navigation} menu="FireSystem" />
      </Container>
    );
  }
}

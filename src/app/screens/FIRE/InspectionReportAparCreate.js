import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TextInput,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Item,
    Switch
} from "native-base";

import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton
  } from "react-native-popup-dialog";
import ImagePicker from "react-native-image-picker";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from 'react-native-datepicker';
import Ripple from "react-native-material-ripple";
import Moment from 'moment'
import SwitchToggle from 'react-native-switch-toggle';

export default class InspectionReportAparCreate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            dataHeader: [],
            isLoading: true,
            listReportApar:[],
            klem: 'K',
            saran: '',
            startDate:'',
            endDate: '',
            kondisi: '',
            jumlahPress:0,
            jumlahWeight:0,
            hose:false,
            seal:false,
            sapot:false,
            kondisiHose:'',
            kondisiSeal:'',
            kondisiSapot:'',
            informasi:'',
            visibleLoadingApar: false,
            visibleSearchListApar: false,
            selectedAparLabel: '',
            searchWordApar: '',
            listApar:[],
            aparText: '',
            subAparText:'',
            rnumApar:'',
            visibleDialogSubmit:false,
            dataLokasi:'',
            save:false,
        };
    }

    static navigationOptions = {
        header: null
    };

    ubahPress(jumlah) {
        let arr = this.state.jumlahPress;
        arr = jumlah;
        this.setState({
          jumlahPress: arr
        });
      }

      tambahPress() {
        let arr = this.state.jumlahPress;
        arr++;
        this.setState({
          jumlahPress: arr
        });

        console.log(this.state.jumlahPress);
      }

      kurangPress() {
        let arr = this.state.jumlahPress;
        if (arr == 0 || arr == null || arr == undefined) {
        } else {
          arr--;
          this.setState({
            jumlahPress: arr
          });
        }
      }

      ubahWeight(jumlah) {
        let arr = this.state.jumlahWeight;
        arr = jumlah;
        this.setState({
          jumlahWeight: arr
        });
      }

      tambahWeight() {
        let arr = this.state.jumlahWeight;
        arr++;
        this.setState({
          jumlahWeight: arr
        });

        console.log(this.state.jumlahWeight);
      }

      kurangWeight() {
        let arr = this.state.jumlahWeight;
        if (arr == 0 || arr == null || arr == undefined) {
        } else {
          arr--;
          this.setState({
            jumlahWeight: arr
          });
        }
      }


    componentDidMount() {
      AsyncStorage.getItem('idApar').then((idApar) => {
        this.setState({
          idApar: idApar,
        });
        this.loadLokasi();
      })

        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {

      });
    }

    loadLokasi() {
      this.setState({ visibleDialog: false, isLoading: true });
        const url = GlobalConfig.SERVERHOST + "getLokasiApar";
        var formData = new FormData();
        formData.append("id_apar", this.state.idApar);

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
          .then(response => response.json())
          .then(responseJson => {
            this.setState({
              dataLokasi: responseJson.data,
              isLoading: false
            });
          })
          .catch(error => {
            console.log(error);
          });
    }

      createReportApar(){
        if(this.state.hose == false){
          this.setState({kondisiHose: 'X'})
        } else {
          this.setState({kondisiHose: 'V'})
        }
        if(this.state.seal == false){
          this.setState({kondisiSeal: 'X'})
        } else {
          this.setState({kondisiSeal: 'V'})
        }
        if(this.state.sapot == false){
          this.setState({kondisiSapot: 'X'})
        } else {
          this.setState({kondisiSapot: 'V'})
        }
        if (this.state.jumlahPress == 0) {
            alert('Input Press');
        }
        else if (this.state.jumlahWeight == 0) {
            alert('Input Weight');
        }
        else if (this.state.kondisi == '') {
            alert('Input Condition Apar');
        }
        else {
          this.setState({
            save: true
          });
        }
    }

    createReportAparFinal(){
      this.setState({
          visibleDialogSubmit:true,
          save:false,
      })
          var url = GlobalConfig.SERVERHOST + "addInspectionApar";
          var formData = new FormData();
          formData.append("id_apar", this.state.idApar);
          formData.append("klem", this.state.klem);
          formData.append("press", this.state.jumlahPress);
          formData.append("hose", this.state.kondisiHose);
          formData.append("seal", this.state.kondisiSeal);
          formData.append("sapot", this.state.kondisiSapot);
          formData.append("weight", this.state.jumlahWeight);
          formData.append("status_cond", this.state.kondisi);
          if (this.state.informasi!='') {
            formData.append("note", this.state.informasi);
          }
          formData.append("pic_badge", '2103161040');
          formData.append("pic_name", 'Muhamat Zaenal Mahmut');
          formData.append("area", this.state.dataLokasi.area);
          formData.append("unit_kerja", this.state.dataLokasi.unit_kerja);
          formData.append("company", this.state.dataLokasi.company);
          formData.append("plant", this.state.dataLokasi.plant);
          formData.append("qr_code", this.state.dataLokasi.qr_code);

          fetch(url, {
              headers: {
                  "Content-Type": "multipart/form-data"
              },
              method: "POST",
              body: formData
          })
              .then(response => response.json())
              .then(response => {
                  if (response.status == 200) {
                      this.setState({
                          visibleDialogSubmit:false
                      })
                      Alert.alert('Success', 'Create Inspection Report Apar Success', [{
                          text: 'Okay'
                      }])
                      this.props.navigation.navigate('InspectionReportApar')
                  } else {
                      this.setState({
                          visibleDialogSubmit:false
                      })
                      Alert.alert('Error', 'Create Inspection Report Apar Failed', [{
                          text: 'Okay'
                      }])
                  }
              })
              .catch((error)=>{
                  console.log(error)
                  this.setState({
                      visibleDialogSubmit:false
                  })
                  Alert.alert('Error', 'Create Inspection Report Apar Failed', [{
                      text: 'Okay'
                  }])
              })
    }

    onPressHose = () => {
      this.setState({ hose: !this.state.hose });
    }
    onPressSeal = () => {
      this.setState({ seal: !this.state.seal });
    }
    onPressSapot = () => {
      this.setState({ sapot: !this.state.sapot });
    }

    render() {
        return (
            <Container style={styles.wrapper2}>
                <Header style={styles.header}>
                    <Left style={{flex:1}}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("InspectionReportAparDetail")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body style={{flex:3, alignItems:'center'}}>
                        <Title style={styles.textbody}>{this.state.dataLokasi.area}</Title>
                    </Body>
                    <Right style={{flex:1}}>
                      <Button
                        transparent
                        onPress={() => this.createReportApar()}
                      >
                        <Icon
                          name="ios-checkmark"
                          style={{fontSize:40, color:colors.white}}
                        />
                      </Button>
                    </Right>
                </Header>
                <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:120, marginBottom:5, borderRadius:10, paddingLeft:10, paddingRight:10, paddingTop:5,
                    shadowColor:'#000',
                    shadowOffset:{
                      width:0,
                      height:1,
                    },
                    shadowOpacity:1,
                    shadowRadius:2,
                    elevation:3,
                  }}>
                    <Text style={{ fontSize: 13, fontWeight: "bold" }}>
                      {this.state.dataLokasi.area}
                    </Text>
                    <Text style={{ fontSize: 10 }}>Area</Text>
                    <Text style={{ fontSize: 13, paddingTop: 5, fontWeight: "bold" }}>
                      {this.state.dataLokasi.unit_kerja}
                    </Text>
                    <Text style={{ fontSize: 10 }}>
                      Unit Kerja
                    </Text>
                    <Text style={{ fontSize: 13, paddingTop:5, fontWeight: "bold" }}>
                        {Moment(this.state.dataLokasi.last_inspection).format('DD MMMM YYYY')}
                    </Text>
                    <Text style={{ fontSize: 10 }}>Last Inspection</Text>
                </View>
                <View style={{ flex: 1 }}>
                    <Content style={{ marginTop: 0}}>
                        <ScrollView>
                        <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.white }}>
                            <View style={{ flex: 1}}>
                                <View>
                                    <Text style={styles.titleInput}>Choose Klem *</Text>
                                    <Form style={{borderWidth:1, marginTop:3, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                                        <Picker
                                            mode="dropdown"
                                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                                            style={{ width: '100%', height: 35}}
                                            placeholder="Kelas"
                                            placeholderStyle={{ color: "#bfc6ea" }}
                                            placeholderIconColor="#007aff"
                                            selectedValue={this.state.klem}
                                            onValueChange={(itemValue) => this.setState({ klem: itemValue })}>
                                            <Picker.Item label="K (Klem)" value="K" />
                                            <Picker.Item label="T (Tanduk)" value="T" />
                                            <Picker.Item label="U (U Klem)" value="U" />
                                            <Picker.Item label="B (Box)" value="B" />
                                            <Picker.Item label="D (Duduk)" value="D" />
                                        </Picker>
                                    </Form>
                                </View>
                            </View>
                        </CardItem>

                        <CardItem style={{ borderRadius: 0, backgroundColor:colors.white }}>
                          <View style={{flex:1}}>
                            <View>
                                <Text style={{fontSize:10}}>Press *</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: "row", marginTop:5, backgroundColor:colors.white }}>
                                <View style={{width:'50%'}}>
                                    <View style={{ flex: 1, flexDirection: "row"}}>
                                      <Button onPress={() => this.kurangPress()} style={styles.btnQTYLeft}>
                                          <Icon
                                            name="ios-arrow-back"
                                            style={styles.facebookButtonIconQTY}
                                          />
                                      </Button>
                                      <View style={{width:40, height:30, marginTop: 0, backgroundColor: colors.white}}>
                                        <Input
                                          style={{
                                            height:30,
                                            marginTop:0,
                                            fontSize:9,
                                            textAlign:'center'}}
                                          value={this.state.jumlahPress + ""}
                                          keyboardType='numeric'
                                          onChangeText={text => this.ubahPress(text)}
                                        />
                                      </View>
                                      <Button onPress={() => this.tambahPress()} style={styles.btnQTYRight}>
                                        <Icon
                                          name="ios-arrow-forward"
                                          style={styles.facebookButtonIconQTY}
                                        />
                                      </Button>
                                    </View>
                                </View>
                            </View>
                          </View>
                        </CardItem>

                        <CardItem style={{ borderRadius: 0, backgroundColor:colors.white }}>
                          <View style={{flex:1}}>
                            <View>
                                <Text style={{fontSize:10}}>Weight</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: "row", marginTop:5, backgroundColor:colors.white }}>
                                <View style={{width:'50%'}}>
                                    <View style={{ flex: 1, flexDirection: "row"}}>
                                      <Button onPress={() => this.kurangWeight()} style={styles.btnQTYLeft}>
                                          <Icon
                                            name="ios-arrow-back"
                                            style={styles.facebookButtonIconQTY}
                                          />
                                      </Button>
                                      <View style={{width:40, height:30, marginTop: 0, backgroundColor: colors.white}}>
                                        <Input
                                          style={{
                                            height:30,
                                            marginTop:0,
                                            fontSize:9,
                                            textAlign:'center'}}
                                          value={this.state.jumlahWeight + ""}
                                          keyboardType='numeric'
                                          onChangeText={text => this.ubahWeight(text)}
                                        />
                                      </View>
                                      <Button onPress={() => this.tambahWeight()} style={styles.btnQTYRight}>
                                        <Icon
                                          name="ios-arrow-forward"
                                          style={styles.facebookButtonIconQTY}
                                        />
                                      </Button>
                                    </View>
                                </View>
                            </View>
                          </View>
                        </CardItem>

                        <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.white }}>
                            <View style={{ flex: 1, flexDirection: "column"}}>
                                <View style={{ flex: 1, flexDirection: "row" }}>
                                    <View style={{width:'75%', paddingTop: 5, justifyContent:'center'}}>
                                        <Text style={{fontSize:12, fontWeight:"bold"}}>Hose</Text>
                                    </View>
                                    <View style={{width: '25%', paddingTop: 5}}>
                                      <SwitchToggle
                                        containerStyle={{
                                          width: 70,
                                          height: 30,
                                          borderRadius: 25,
                                          padding: 5,
                                        }}
                                        backgroundColorOff={colors.whitedar}
                                        backgroundColorOn='#00b300'
                                        circleStyle={{
                                          width: 22,
                                          height: 22,
                                          borderRadius: 19,
                                          backgroundColor: 'white',
                                        }}
                                        switchOn={this.state.hose}
                                        onPress={this.onPressHose}
                                        circleColorOff='white'
                                        circleColorOn='white'
                                        duration={500}
                                      />
                                    </View>
                                </View>
                                <View style={{ flex: 1, flexDirection: "row" }}>
                                    <View style={{width:'75%', paddingTop: 10, justifyContent:'center'}}>
                                        <Text style={{fontSize:12, fontWeight:"bold"}}>Seal</Text>
                                    </View>
                                    <View style={{width: '25%', paddingTop: 10}}>
                                      <SwitchToggle
                                        containerStyle={{
                                          width: 70,
                                          height: 30,
                                          borderRadius: 25,
                                          padding: 5,
                                        }}
                                        backgroundColorOff={colors.whitedar}
                                        backgroundColorOn='#00b300'
                                        circleStyle={{
                                          width: 22,
                                          height: 22,
                                          borderRadius: 19,
                                          backgroundColor: 'white',
                                        }}
                                        switchOn={this.state.seal}
                                        onPress={this.onPressSeal}
                                        circleColorOff='white'
                                        circleColorOn='white'
                                        duration={500}
                                      />
                                    </View>
                                </View>
                                <View style={{ flex: 1, flexDirection: "row" }}>
                                    <View style={{width:'75%', paddingTop: 10, justifyContent:'center'}}>
                                        <Text style={{fontSize:12, fontWeight:"bold"}}>Sapot</Text>
                                    </View>
                                    <View style={{width: '25%', paddingTop: 10}}>
                                      <SwitchToggle
                                        containerStyle={{
                                          width: 70,
                                          height: 30,
                                          borderRadius: 25,
                                          padding: 5,
                                        }}
                                        backgroundColorOff={colors.whitedar}
                                        backgroundColorOn='#00b300'
                                        circleStyle={{
                                          width: 22,
                                          height: 22,
                                          borderRadius: 19,
                                          backgroundColor: 'white',
                                        }}
                                        switchOn={this.state.sapot}
                                        onPress={this.onPressSapot}
                                        circleColorOff='white'
                                        circleColorOn='white'
                                        duration={500}
                                      />
                                    </View>
                                </View>
                            </View>
                        </CardItem>

                        <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.white }}>
                            <View style={{ flex: 1}}>
                                <View>
                                    <Text style={styles.titleInput}>Status Kondisi *</Text>
                                    <Form style={{borderWidth:1, marginTop:3, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                                        <Picker
                                            mode="dropdown"
                                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                                            style={{ width: '100%', height: 35}}
                                            placeholder="Kelas"
                                            placeholderStyle={{ color: "#bfc6ea" }}
                                            placeholderIconColor="#007aff"
                                            selectedValue={this.state.kondisi}
                                            onValueChange={(itemValue) => this.setState({ kondisi: itemValue })}>
                                            <Picker.Item label="Belum Dicek" value="" />
                                            <Picker.Item label="Baik" value="BAIK" />
                                            <Picker.Item label="Ganti" value="GANTI" />
                                            <Picker.Item label="Habis" value="HABIS" />
                                            <Picker.Item label="Refill" value="REFILL" />
                                        </Picker>
                                    </Form>
                                </View>
                            </View>
                        </CardItem>
                       <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.white }}>
                            <View style={{ flex: 1 }}>
                                <View>
                                    <Text style={styles.titleInput}>Informasi</Text>
                                </View>
                                <View>
                                    <Textarea style={{
                                        marginLeft: 5,
                                        marginRight: 5,
                                        marginBottom: 5,
                                        borderRadius: 5,
                                        fontSize: 11 }}
                                        rowSpan={2}
                                        bordered value={this.state.informasi}
                                        placeholder='Informasi ...'
                                        onChangeText={(text) => this.setState({ informasi: text })} />
                                </View>
                            </View>
                        </CardItem>
                      </ScrollView>
                      <View style={{ width: '100%', position: "absolute"}}>
                        <Dialog
                          visible={this.state.save}
                          dialogAnimation={
                            new SlideAnimation({
                              slideFrom: "bottom"
                            })
                          }
                          dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300}}
                          onTouchOutside={() => {
                            this.setState({ login: false });
                          }}
                        >
                        <DialogContent>
                        {
                          <View>
                            <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                              <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Do you want to save this data ?</Text>
                            </View>
                            <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                              <View style={{width:'50%', paddingRight:5}}>
                                  <Button
                                    block
                                    style={{
                                      width:'100%',
                                      marginTop:10,
                                      height: 35,
                                      marginBottom: 5,
                                      borderWidth: 0,
                                      backgroundColor: colors.primer,
                                      borderRadius: 15
                                    }}
                                    onPress={() => this.createReportAparFinal()}
                                  >
                                    <Text style={{color:colors.white}}>Yes</Text>
                                  </Button>
                              </View>
                              <View style={{width:'50%', paddingLeft:5}}>
                                  <Button
                                    block
                                    style={{
                                      width:'100%',
                                      marginTop:10,
                                      height: 35,
                                      marginBottom: 5,
                                      borderWidth: 0,
                                      backgroundColor: colors.primer,
                                      borderRadius: 15
                                    }}
                                    onPress={() => this.setState({
                                      save:false,
                                    })}
                                  >
                                    <Text style={{color:colors.white}}>No</Text>
                                  </Button>
                              </View>
                            </View>
                          </View>
                        }
                        </DialogContent>
                        </Dialog>
                      </View>
                      <View style={{ width: 270, position: "absolute" }}>
                          <Dialog
                              visible={this.state.visibleDialogSubmit}
                          >
                              <DialogContent>
                              {
                                <View>
                                  <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                                    <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Creating Inpection Apar ...</Text>
                                  </View>
                                  <ActivityIndicator size="large" color="#330066" animating />
                                </View>
                              }
                              </DialogContent>
                          </Dialog>
                      </View>
                    </Content>
                </View>
                <CustomFooter navigation={this.props.navigation} menu='Safety' />
            </Container>

        );
    }
}

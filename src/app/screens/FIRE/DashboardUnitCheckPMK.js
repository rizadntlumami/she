import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail,
  Textarea,
  Picker,
  Form
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Ripple from "react-native-material-ripple";
import DatePicker from "react-native-datepicker";
import ListViewUnitCheck from "../../components/FireSystem/ListViewUnitCheck";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, listAktifitas) {
    AsyncStorage.setItem("list", JSON.stringify(listAktifitas)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.navigateToScreen("DashboardUnitCheckDetail", this.props.data)
          }
          // onPress={() => alert("Maintenance")}
          rippleColor={colors.accent}
        >
          <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
            <ListViewUnitCheck
              imageUri={require("../../../assets/images/firetruck.png")}
              header={this.props.data.VEHICLE_CODE}
              body1={this.props.data.MERK}
              body2={this.props.data.TIPE}
              body3={this.props.data.TAHUN}
              body4={this.props.data.CHECKED_BY}
              negatif={this.props.data.COUNT_NEGATIF_LIST}
              date={this.props.data.DATE_CHECKED}
              time={this.props.data.CLOCK_CHECKED}
            />
          </View>
        </Ripple>
      </Card>
    );
  }
}

class DashboardUnitCheckPMK extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      visibleDialog: false,
      filterDate: "",
      filterType: "",
      filterStatus: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  onRefresh() {
    console.log("refreshing");
    this.setState({ isLoading: true }, function() {
      this.loadData();
    });
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
        this.setState({ isLoading: true });
      }
    );
  }

  loadData() {
    this.setState({ visibleDialog: false });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/firesystem/unit_check/get_vehicle_list";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("VEHICLE_TYPE", "PMK");
      // if (this.state.filterPlant != '') {
      //     formData.append("PLANT", this.state.filterPlant)
      // } else {
      //     formData.append("PLANT", '5001')
      // }
      // if (this.state.filterDate != '') {
      //     formData.append("INSP_DATETIME", this.state.filterDate)
      // } else {
      //     formData.append("INSP_DATETIME", '')
      // }
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson))
          this.setState({
            dataSource: responseJson.Data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
          this.setState({
            isLoading: false
          });
        });
    });
  }

  onClickSearch() {
    this.setState({
      visibleDialog: true
    });
  }

  render() {
    that = this;
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.dataSource == null) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <FlatList
            data={this.state.dataSource}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isLoading}
                onRefresh={this.onRefresh.bind(this)}
              />
            }
          />
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("FireMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body>
            <Title style={styles.textbody}>Dashboard Unit Check</Title>
          </Body>
          {/* <Right>
                        <Button
                            transparent
                            onPress={() => this.onClickSearch()}
                        >
                            <Icon
                                name="ios-funnel"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Right> */}
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Footer>
          <FooterTab style={styles.tabfooter}>
            <Button
              onPress={() =>
                this.props.navigation.navigate("DashboardUnitCheckPickup")
              }
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>
                {" "}
                Mobil Pick Up
              </Text>
            </Button>
            <Button active style={styles.tabfooter}>
              <View style={{ height: "40%" }} />
              <View style={{ height: "50%" }}>
                <Text style={styles.textbody}>Mobil PMK</Text>
              </View>
              <View style={{ height: "20%" }} />
              <View
                style={{
                  borderWidth: 2,
                  marginTop: 2,
                  height: 0.5,
                  width: "100%",
                  borderColor: colors.white
                }}
              />
            </Button>
            <Button
              onPress={() =>
                this.props.navigation.navigate("DashboardUnitCheckPerformance")
              }
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>
                Performance
              </Text>
            </Button>
          </FooterTab>
        </Footer>

        <View style={{ marginTop: 10, flex: 1, flexDirection: "column" }}>
          {list}
        </View>
        {/* <Fab
                    active={this.state.active}
                    direction="up"
                    containerStyle={{}}
                    style={{ backgroundColor: colors.green01, marginBottom: 50 }}
                    position="bottomRight"

                    // onPress={() => this.props.navigation.navigate('DashboardUnitCheckCreate')}>
                    onPress={() => alert("Maintenance")}>
                    <Icon name="ios-add" style={{ fontSize: 50, fontWeight: "bold" }} />
                </Fab> */}
        <View style={{ width: 500, position: "absolute" }}>
          <Dialog visible={this.state.visibleDialog}>
            <DialogContent style={{ width: 300, height: 250 }}>
              <Text style={{ paddingTop: 5, fontWeight: "bold", fontSize: 10 }}>
                Atur Filter
              </Text>
              <View style={{ flex: 1 }}>
                <DatePicker
                  style={{ width: "100%", fontSize: 10, borderRadius: 20 }}
                  date={this.state.filterDate}
                  mode="date"
                  placeholder="Choose Date ..."
                  format="YYYY-MM-DD"
                  minDate="2018-01-01"
                  maxDate="5000-12-31"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateInput: {
                      marginLeft: 5,
                      marginRight: 5,
                      height: 35,
                      borderRadius: 5,
                      fontSize: 10,
                      borderWidth: 1,
                      borderColor: "#E6E6E6"
                    },
                    dateIcon: {
                      position: "absolute",
                      left: 0,
                      top: 5
                    }
                  }}
                  onDateChange={date => {
                    this.setState({ filterDate: date });
                  }}
                />
              </View>
              <View>
                <Text style={styles.titleInput}>Status</Text>
                <Form
                  style={{
                    borderWidth: 1,
                    borderRadius: 5,
                    marginRight: 5,
                    marginLeft: 5,
                    borderColor: "#E6E6E6",
                    height: 35
                  }}
                >
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                    style={{ width: "100%", height: 35 }}
                    placeholder="Select Shift ..."
                    placeholderStyle={{ color: "#bfc6ea" }}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.filterStatus}
                    onValueChange={itemValue =>
                      this.setState({ filterStatus: itemValue })
                    }
                  >
                    <Picker.Item label="Semua Status" value="" />
                    <Picker.Item label="Open" value="OPEN" />
                    <Picker.Item label="Close" value="CLOSE" />
                  </Picker>
                </Form>
              </View>
              <View>
                <Text style={styles.titleInput}>Type</Text>
                <Form
                  style={{
                    borderWidth: 1,
                    borderRadius: 5,
                    marginRight: 5,
                    marginLeft: 5,
                    borderColor: "#E6E6E6",
                    height: 35
                  }}
                >
                  <Picker
                    mode="dropdown"
                    iosIcon={<Icon name="ios-arrow-down-outline" />}
                    style={{ width: "100%", height: 35 }}
                    placeholder="Select Shift ..."
                    placeholderStyle={{ color: "#bfc6ea" }}
                    placeholderIconColor="#007aff"
                    selectedValue={this.state.filterType}
                    onValueChange={itemValue =>
                      this.setState({ filterType: itemValue })
                    }
                  >
                    <Picker.Item label="Semua Type" value="" />
                    <Picker.Item label="Siaga" value="SIAGA" />
                    <Picker.Item label="Inspeksi" value="INSPEKSI" />
                    <Picker.Item label="Pemadaman" value="PEMADAMAN" />
                  </Picker>
                </Form>
              </View>
              <DialogButton text="FILTER" onPress={() => this.loadData()} />
            </DialogContent>
          </Dialog>
        </View>
        <CustomFooter navigation={this.props.navigation} menu="FireSystem" />
      </Container>
    );
  }
}

export default DashboardUnitCheckPMK;

import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    StatusBar,
    ScrollView,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage,
    FlatList
} from "react-native";
import {
    Container,
    Text,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Body,
    Input,
    Item,
    Card,
    Form,
    Picker,
    CardItem,
    Icon,
    Thumbnail
} from "native-base";
// import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";

import RoundedButtons from "../../components/RoundedButton";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/ReportAPD";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";

var that;
var thatstock;

class ListItem extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
        };
    }

    componentDidMount() {
        AsyncStorage.getItem('token').then((value) => {
            this.setState({ 'token': value });
        })
    }

    render() {
        return (
            <View>
                <Card>
                    <CardItem>
                        <Left>
                            <Thumbnail source={{ uri: GlobalConfig.SERVERHOST + 'api' + this.props.data.FOTO_BEF }} />
                            <Body>
                                <Text>Kode Material       : {this.props.data.KODE}</Text>
                                <Text>Nama     : {this.props.data.NAMA}</Text>
                                <Text>Merk       : {this.props.data.MERK}</Text>
                                <Text>Size                      : {this.props.data.SIZE}</Text>
                            </Body>
                        </Left>
                    </CardItem>
                    <CardItem>
                        <Left>
                            <Body>
                                <Text>Stock Awal : {this.props.data.AVAILABLE}/{this.props.data.LAST_STOCK_START_DATE}</Text>
                                <Text>In/Out : {this.props.data.IN_STOCK_RANGE_DATE}/{this.props.data.OUT_STOCK_RANGE_DATE}</Text>
                            </Body>
                        </Left>
                        <Right>
                            <Body>
                                <Text>Stock Akhir : {this.props.data.LAST_STOCK_END_DATE}</Text>
                            </Body>
                        </Right>
                    </CardItem>
                </Card>
            </View>
        )
    }
}

export default class ReportAPD extends Component {
    static navigationOptions = {
        header: null
    };

    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            isloading: true,
            dataSource: [],
            dataProfile: [],
            selectedAPD: '100'
        };
        thatstock = this;
    }

    onValueChange2(value) {
        setTimeout(() => {
            this.setState({
                selectedAPD: value
            });
        }, 1000);
    }

    _renderItem = ({ item }) => (
        <ListItem data={item}></ListItem>
    )

    searchAPD = () => {
        var selectedValue = this.state.selectedAPD
        var selectedValuePlant = thatstock.state.dataProfile.plant
        AsyncStorage.getItem('token').then((value) => {
            // alert(JSON.stringify(selectedValuePlant));
            const url = GlobalConfig.SERVERHOST + 'api/v_mobile/apd/stock/reporting/inout';
            var formData = new FormData();

            formData.append("token", value)
            formData.append("APD_GROUP", selectedValue)
            formData.append("CODE_PLANT", selectedValuePlant)

            fetch(url, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                body: formData
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    // alert(JSON.stringify(responseJson));
                    this.setState({
                        dataSource: responseJson.data[selectedValuePlant][selectedValue],
                        isloading: false
                    });
                })
                .catch((error) => {
                    console.log(error)
                })
        })
    }

    componentDidMount() {
        this.loadData();
        //this.searchAPD();
    }

    loadData() {
        AsyncStorage.getItem('token').then((value) => {
            const url = GlobalConfig.SERVERHOST + 'api/Auth/user?token=' + value;

            fetch(url)
                .then((response) => response.json())
                .then((responseJson) => {
                    // alert(JSON.stringify(responseJson));
                    this.setState({
                        dataProfile: responseJson.data.data,
                        isloading: false
                    });
                })
                .catch((error) => {
                    console.log(error)
                })
        })
    }

    render() {
        return (
            this.state.isloading
                ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#330066" animating />
                </View>
                :
                <Container style={styles.wrapper}>
                    <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                    <View style={styles.homeWrapper}>
                        <View>
                                <Text style={{ marginLeft: 10 }}>Pilih Jenis APD</Text>
                                <Form underlineColorAndroid='transparent' style={styles.inputQty}>
                                    <Item picker onPress={this.searchAPD()}>
                                        <Picker
                                            mode="dropdown"
                                            style={{ width: undefined }}
                                            placeholder="Quantity"
                                            placeholderStyle={{ color: "#bfc6ea" }}
                                            placeholderIconColor="#007aff"
                                            selectedValue={this.state.selectedAPD}
                                            onValueChange={(itemValue) => this.setState({ selectedAPD: itemValue })}>
                                            <Picker.Item label="Pelindung Kepala" value="100" Sele />
                                            <Picker.Item label="Pelindung Mata" value="200" />
                                            <Picker.Item label="Pelindung Telinga" value="300" />
                                            <Picker.Item label="Pelindung Hidung" value="400" />
                                            <Picker.Item label="Pelindung Tangan" value="500" />
                                            <Picker.Item label="Pelindung Badan" value="600" />
                                            <Picker.Item label="Pelindung Kaki" value="700" />
                                            <Picker.Item label="Full Body Harness" value="800" />
                                            <Picker.Item label="Paket Obat PPPK" value="900" />
                                            <Picker.Item label="Lain-Lain" value="910" />
                                        </Picker>
                                    </Item>
                                </Form>
                            </View>
                        <ScrollView>
                            <FlatList
                                    data={this.state.dataSource}
                                    renderItem={this._renderItem}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                        </ScrollView>
                    </View>
                </Container>
        );
    }
}

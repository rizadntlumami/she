import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../styles/colors";

export default class ListViewRecapitulations extends Component {
    render() {
        return (
            <View style={{ flexDirection: "column", flex: 1 }}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ flex: 1}}>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>No. Rekap : {this.props.noRekap}</Text>
                        <Text style={{fontSize:10, }}>Jenis Temuan : {this.props.type}</Text>
                        <Text style={{ fontSize: 8,paddingTop:5}}>Tanggal Temuan : {this.props.tglTemuan}</Text>
                        <Text style={{ fontSize: 8, }}>Tanggal Komitmen : {this.props.batasWaktu}</Text>
                    </View>
                    <View style={{ }}>
                    {this.props.status=='OPEN' ? (
                        <Text style={{fontSize:10, fontWeight:"bold", textAlign:'right', backgroundColor: "#1BB394",
                        paddingRight: 4,
                        paddingLeft: 4,
                        paddingBottom: 2,
                        paddingTop: 2,
                        borderRadius: 6,
                        color:colors.white,
                        flexWrap: 'wrap'}}>{this.props.status}</Text>
                    ) : (
                        <Text style={{fontSize:10, fontWeight:"bold", textAlign:'right', backgroundColor: "#FF0101",
                        paddingRight: 4,
                        paddingLeft: 4,
                        paddingBottom: 2,
                        paddingTop: 2,
                        borderRadius: 6,
                        color:colors.white,
                        flexWrap: 'wrap'}}>{this.props.status}</Text>
                    )}
                        
                    </View>
                </View>
            </View>
        );
    }
}

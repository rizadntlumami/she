import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
  Form,
  Picker,
  Icon
} from "native-base";
// import Icon from "react-native-vector-icons/FontAwesome";

import GlobalConfig from "../../components/GlobalConfig";
//import APDItems from '../../components/APDItem'
import MenuItems from "../../components/APDItems";
import styles from "../styles/IndividualReport";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Moment from "moment";

class ListItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      active: "true"
    };
  }

  componentDidMount() {
    AsyncStorage.getItem("token").then(value => {
      this.setState({ token: value });
    });
  }

  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
          <CardItem style={{borderRadius:10}}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={styles.viewLeftHeader2}>
                <Text note style={styles.cardtext}>
                  Kode APD
                </Text>
                <Text note style={styles.cardtext}>
                  Nama APD
                </Text>
                <Text note style={styles.cardtext}>
                  Merk
                </Text>
                <Text note style={styles.cardtext}>
                  Note
                </Text>
                <Text note style={styles.cardtext}>
                  Release
                </Text>
              </View>
              <View>
                <Text style={styles.cardtext}>
                  : {this.props.data.kode_apd}
                </Text>
                <Text style={styles.cardtext}>: {this.props.data.nama_apd}</Text>
                <Text style={styles.cardtext}>
                  : {this.props.data.merk}
                </Text>
                <Text note style={styles.cardtext}>
                  : {this.props.data.keterangan}
                </Text>
                <Text style={styles.cardtext}>
                  : {Moment(this.props.data.release_at).format('DD MMMM YYYY')}
                </Text>
                {/* <Text style={styles.cardtextKode}>
                  : {this.props.data.EXPIRED_DAYS_LEFT}
                </Text> */}
              </View>
            </View>
          </CardItem>
        </Card>
      </View>
    );
  }
}

export default class IndividualReportPersonal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      dataProfile: [],
      isloading: true,
    //   selectedAPD: "100",
      isEmpty: false,
      badge : ''
    //   listClickedKatAPD: [],
    //   clickedKatAPD: 0,
    //   arrKodeApd: [
    //     "100",
    //     "200",
    //     "300",
    //     "400",
    //     "500",
    //     "600",
    //     "700",
    //     "800",
    //     "900",
    //     "910"
    //   ]
    };
  }

  static navigationOptions = {
    header: null
  };

//   onValueChange2(value) {
//     setTimeout(() => {
//       this.setState({
//         selectedAPD: value
//       });
//     }, 1000);
//   }

  _renderItem = ({ item }) => <ListItem data={item} />;

//   filterData(groupAPD, clicked) {
//     this.setClickedKategori(clicked);
//     this.setState({
//       isloading: true
//     });
//     AsyncStorage.getItem("token").then(value => {
//       const url =
//         GlobalConfig.SERVERHOST + "api/v_mobile/apd/order/history/personal";
//       // alert(url)
//       var formData = new FormData();
//       formData.append("token", value);
//       fetch(url, {
//         headers: {
//           "Content-Type": "multipart/form-data"
//         },
//         method: "POST",
//         body: formData
//       })
//         .then(response => response.json())
//         .then(responseJson => {
//           // alert(JSON.stringify(responseJson.data));
//           this.setState({
//             dataSource: responseJson.data[groupAPD],
//             isloading: false
//           });
//           console.log(this.state.dataSource);
//           if (this.state.dataSource==undefined){
//             this.setState({
//               isEmpty: true
//             });
//           }else{
//             this.setState({
//               isEmpty: false
//             });
//           }

//         })
//         .catch(error => {
//           this.setState({
//             isloading: false
//           });
//           console.log(error);
//         });
//     });
//   }

  componentDidMount() {
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + 'validasi';
      var formData = new FormData();
      formData.append("token", value)
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            name: responseJson.user.name,
            badge : responseJson.user.no_badge,
            isloading: false
          });
          this.loadData();
        })
        .catch(error => {
          console.log(error);
        });
    })

    this.loadData();
  }

  loadData() {
    // this.setClickedKategori(0);
    this.setState({
      isloading: true
    });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "releaselist/personal";
      // alert(url)
      var formData = new FormData();
      formData.append("token", value);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
      .then(response => response.json())
      .then(responseJson => {
        // alert(JSON.stringify(responseJson));
        if (responseJson.status == 422) {
          this.setState({
            isloading: false
          });
          this.setState({
            isEmpty: true
          });
        } else {
          this.setState({
            dataSource: responseJson.data.filter(x => x.no_badge == this.state.badge),
            isloading: false,
            isEmpty: false
          });
        }
      })
      .catch(error => {
        console.log(error);
      });
  });
  }

//   setClickedKategori(clickedIndex) {
//     var tempclickAPD = [];
//     for (let i = 0; i < 11; i++) {
//       if (i == clickedIndex) {
//         tempclickAPD.push(true);
//       } else {
//         tempclickAPD.push(false);
//       }
//     }
//     this.setState({
//       listClickedKatAPD: tempclickAPD
//     });
//   }

  onRefresh() {
    console.log('refreshing')
    this.setState({ isloading: true }, function() {
      this.loadData();
    });
  }

  render() {
    that = this;
    var list;
    if (this.state.isloading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.dataSource=='') {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <ScrollView>
            <FlatList
                data={this.state.dataSource}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
                refreshControl={
                <RefreshControl
                  refreshing={this.state.isloading}
                  onRefresh={this.onRefresh.bind(this)}
                />}
              />
          </ScrollView>
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("ApdMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Individual Report</Title>
          </Body>

            <Right style={{flex:1}}/>

        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={styles.homeWrapper}>
          <Footer style={styles.tabHeight}>
            <FooterTab style={styles.tabfooter}>
              <Button active style={styles.tabfooter}>
                <View style={{height:'40%'}}></View>
                <View style={{height:'50%'}}>
                <Text>PERSONAL</Text>
                </View>
                <View style={{height:'20%'}}></View>
                <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.white}}></View>
              </Button>
              <Button
                onPress={() =>
                  this.props.navigation.navigate("IndividualReportUnitKerja")
                }
              >
                <Text style={{color:'white', fontWeight:'bold'}}>UNIT KERJA</Text>
              </Button>
              <Button
                onPress={() =>
                  this.props.navigation.navigate("IndividualReportPeminjaman")
                }
              >
                <Text style={{color:'white', fontWeight:'bold'}}>PEMINJAMAN</Text>
              </Button>
            </FooterTab>
          </Footer>
          
          <View
            style={{
              flex: 1,
              flexDirection: "column",

            }}
          >
            {list}
            </View>
          </View>
        {/* <CustomFooter navigation={this.props.navigation} menu="APD" /> */}
      </Container>
    );
  }
}

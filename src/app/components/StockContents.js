import React, { Component } from "react";
import { View, Text, Image } from "react-native";

export default class APDContent extends Component {
  render() {
    return (
      <View
        style={{
          height: 130,
          width: 130,
        }}
      >
        <View style={{ flex: 2 }}>
          <Image
            source={this.props.imageUri}
            style={{ flex: 1, width: null, height: null, resizeMode: "cover" }}
          />
        </View>
        <View style={{ flex: 1, paddingLeft: 10, paddingTop: 10 }}>
          <Text>{this.props.name}</Text>
          <Text>{this.props.merk}</Text>
          <Text>Available : {this.props.available}</Text>
          <Text>Stock on Hand : {this.props.jumlah}</Text>
        </View>
      </View>
    );
  }
}

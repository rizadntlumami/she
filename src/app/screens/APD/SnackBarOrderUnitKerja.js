import React, { Component } from "react";
import { StyleSheet, Text, View, Animated, AsyncStorage } from "react-native";

import styles from "../styles/SnackBar";
import colors from "../../../styles/colors";


class SnackBarOrderUnitKerja extends Component {
  constructor(props) {
    super(props);

    this.animatedValue = new Animated.Value(50);
    this.ShowSnackBar = false;
    this.HideSnackBar = true;
    this.state = {
      SnackBarInsideMsgHolder: [],
      SnackBarObjectAPD: []
    };
  }

  EmptySnackBarFunction() {
    this.animatedValue = new Animated.Value(50);
    this.ShowSnackBar = false;
    this.HideSnackBar = true;
    this.setState({
      SnackBarInsideMsgHolder: [],
      SnackBarObjectAPD: []
    });
  }

  static navigationOptions = {
    header: null
  };
  ShowSnackBarFunction(SnackBarInsideMsgHolder = "Default SnackBar Message...", SnackBarAPD, duration = 3000) {
    if (this.ShowSnackBar === false) {
      this.setState({
        SnackBarInsideMsgHolder: SnackBarInsideMsgHolder,
        SnackBarObjectAPD: SnackBarAPD
      });
      this.ShowSnackBar = true;

      Animated.timing
        (
          this.animatedValue,
          {
            toValue: 0,
            duration: 400
          }
        ).start();
    }
  }

  hide = (duration) => {
    this.timerID = setTimeout(() => {
      if (this.HideSnackBar === true) {
        this.HideSnackBar = false;

        Animated.timing
          (
            this.animatedValue,
            {
              toValue: 50,
              duration: 400
            }
          ).start(() => {
            this.HideSnackBar = true;
            this.ShowSnackBar = false;
            clearTimeout(this.timerID);
          })
      }
    }, 10000);
  }

  SnackBarCloseFunction = () => {
    if (this.HideSnackBar === true) {
      this.HideSnackBar = false;
      clearTimeout(this.timerID);

      Animated.timing
        (
          this.animatedValue,
          {
            toValue: 50,
            duration: 400
          }
        ).start(() => {
          this.ShowSnackBar = false;
          this.HideSnackBar = true;
        });
    }
  }

  SendAsync = () => {
    AsyncStorage.setItem('listApd', JSON.stringify(this.state.SnackBarObjectAPD)).then(() => {
      that.props.data.navigate('OrderUnitKerjaSubmit');
    })
  }

  render() {
    that = this;

    return (
      <Animated.View style={[{ transform: [{ translateY: this.animatedValue }] }, styles.SnackBarContainter]}>
        <Text style={styles.SnackBarMessage}>{this.state.SnackBarInsideMsgHolder.join(", ")}</Text>
        <Text style={styles.SnackBarUndoText} onPress={this.SendAsync} > OK </Text>
      </Animated.View>

    );
  }
}
export default SnackBarOrderUnitKerja;

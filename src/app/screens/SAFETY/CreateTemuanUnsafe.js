import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Textarea,
  Icon,
  Picker,
  Form,
  Input,
  Item
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import Ripple from "react-native-material-ripple";
import ImagePicker from "react-native-image-picker";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from "react-native-datepicker";
import { TextInput } from "react-native-gesture-handler";

class ListItem extends React.PureComponent {
  navigateToScreen(route, id_rusak) {
    // alert(route);
    AsyncStorage.setItem("id", id_rusak).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() => this.props.setSelectedLocLabel(this.props.data)}
          rippleColor={colors.accent}
        >
          <CardItem
            style={{
              borderRadius: 0,
              marginTop: 4,
              backgroundColor: colors.gray
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 4,
                backgroundColor: colors.gray
              }}
            >
              <View>
                <Text style={{ fontSize: 12 }}>
                  {this.props.data.ID_EQUPMENT} - {this.props.data.EQUPMENTCODE}
                </Text>
              </View>
            </View>
          </CardItem>
        </Ripple>
      </View>
    );
  }
}

class ListItemUnit extends React.PureComponent {
  navigateToScreen(route, id_rusak) {
    // alert(route);
    AsyncStorage.setItem("id", id_rusak).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() => this.props.setSelectedUnit(this.props.data)}
          rippleColor={colors.accent}
        >
          <CardItem
            style={{
              borderRadius: 0,
              marginTop: 4,
              backgroundColor: colors.gray
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 4,
                backgroundColor: colors.gray
              }}
            >
              <View>
                <Text style={{ fontSize: 12 }}>
                  {this.props.data.muk_nama} - {this.props.data.muk_kode}
                </Text>
              </View>
            </View>
          </CardItem>
        </Ripple>
      </View>
    );
  }
}

export default class CreateTemuanUnsafe extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      dataHeader: [],
      listLocation: [],
      isLoading: true,
      selectedShift: "1",
      selectedUnsafeType: "UA",
      selectedPriority: "1",

      pickedImage: "",
      uri: "",
      fileName: "",

      visibleLoadingLocation: false,
      visibleSearchListLoc: false,
      selectedLocLabel: "",
      searchWordLocation: "",
      visibleDialogSubmit: false
    };
  }

  static navigationOptions = {
    header: null
  };

  reset = () => {
    this.setState({
      pickedImage: null
    });
  };

  pickImageHandler = () => {
    ImagePicker.showImagePicker(
      { title: "Pick an Image", maxWidth: 800, maxHeight: 600 },
      res => {
        if (res.didCancel) {
          console.log("User cancelled!");
        } else if (res.error) {
          console.log("Error", res.error);
        } else {
          this.setState({
            pickedImage: res.uri,
            uri: res.uri,
            fileName: res.fileName,
            fileType: res.type
          });
          console.log(res.type);
        }
      }
    );
  };

  resetHandler = () => {
    this.reset();
  };

  // _renderItem = ({ item }) => (
  //     <ListItem data={item}></ListItem>
  // )

  componentDidMount() {
    this.onChangeLocation();
  }

  // loadData() {

  // }

  createUnsafe() {
    if (this.state.deskripsi_temuan == null) {
      alert("Masukkan Deskripsi Temuan");
    }
    // else if (this.state.LOKASI_TEXT == null) {
    //     alert('Masukkan Lokasi Temuan');
    // }
    // else if (this.state.PENYEBAB == null) {
    //     alert('Masukkan Penyebab Temuan');
    // }
    // else if (this.state.REKOMENDASI == null) {
    //     alert('Masukkan Rekomendasi Tindak Lanjut pada Temuan');
    // }
    // else if (this.state.POTENSI_BAHAYA == null) {
    //     alert('Masukkan Potensi Bahaya dari Temuan');
    // }
    else {
      this.setState({
        visibleDialogSubmit: true
      });
      // alert(JSON.stringify(this.state.id));
      AsyncStorage.getItem("token").then(value => {
        const url =
          GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/create/detail";
        var formData = new FormData();
        formData.append("token", value);
        formData.append("SHIFT", this.state.selectedShift);
        formData.append("JENIS_TEMUAN", this.state.selectedUnsafeType);
        formData.append("PRIORITY", this.state.selectedPriority);
        formData.append("TEMUAN", this.state.deskripsi_temuan);
        formData.append("LOKASI_TEXT", this.state.lokasi_temuan);
        formData.append("LOKASI_KODE", this.state.lokasi_kode);
        formData.append("UK_TEXT", this.state.uk_nama);
        formData.append("UK_KODE", this.state.uk_kode);
        formData.append("PENYEBAB", this.state.penyebab_temuan);
        formData.append("REKOMENDASI", this.state.rekomendasi_temuan);
        formData.append("TINDAK_LANJUT", this.state.tindak_lanjut_temuan);
        formData.append("POTENSI_BAHAYA", this.state.potensi_bahaya_temuan);
        formData.append("FOTO_BEF", {
          uri: this.state.pickedImage,
          name: "image",
          type: this.state.fileType
        });

        console.log(formData);

        // alert(JSON.stringify(url));

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
          .then(response => response.json())
          .then(responseJson => {
            // alert(responseJson);
            if (responseJson.status == 200) {
              this.setState({
                visibleDialogSubmit: false
              });
              Alert.alert("Success", "Create Temuan Unsafe Success", [
                {
                  text: "Okay"
                }
              ]);
              this.props.navigation.navigate("DailyReport");
            } else {
              this.setState({
                visibleDialogSubmit: false
              });
              Alert.alert("Error", "Create Temuan Unsafe Failed", [
                {
                  text: "Okay"
                }
              ]);
            }
          })
          .catch(error => {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Error", "Create Temuan Unsafe Failed", [
              {
                text: "Okay"
              }
            ]);
            console.log(error);
          });
      });
    }
  }

  onChangeLocation(text) {
    this.setState({
      searchWordLocation: text
    });
    // this.setState({
    //     visibleLoadingLocation: true
    // })
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/list_master_data/search/funcloc";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("search", this.state.searchWordLocation);
      formData.append("limit", 10);
      formData.append("plant", null);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            visibleLoadingLocation: false
          });
          this.setState({
            listLocation: responseJson,
            // listKaryawanMaster: responseJson,
            isloading: false
          });
        })
        .catch(error => {
          // this.setState({
          //     visibleLoadingLocation: false
          // })
          console.log(error);
        });
    });
  }

  onChangeUnit(text) {
    this.setState({
      searchWordUnit: text
    });
    // this.setState({
    //     visibleLoadingLocation: true
    // })
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/list_master_data/search/unit_kerja";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("search", this.state.searchWordUnit);
      formData.append("limit", 10);
      formData.append("plant", null);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            visibleLoadingLocation: false
          });
          this.setState({
            listUnit: responseJson,
            // listKaryawanMaster: responseJson,
            isloading: false
          });
        })
        .catch(error => {
          // this.setState({
          //     visibleLoadingLocation: false
          // })
          console.log(error);
        });
    });
  }

  onClickSearchLoc() {
    console.log("masuk");
    if (this.state.visibleSearchListLoc) {
      this.setState({
        visibleSearchListLoc: false
      });
    } else {
      this.setState({
        visibleSearchListLoc: true
      });
    }
  }

  onClickSearchUnit() {
    console.log("masuk");
    if (this.state.visibleSearchListUnit) {
      this.setState({
        visibleSearchListUnit: false
      });
    } else {
      this.setState({
        visibleSearchListUnit: true
      });
    }
  }

  setSelectedLocLabel(location) {
    this.setState({
      lokasi_temuan: location.EQUPMENTCODE,
      lokasi_kode: location.FUNCLOC,
      visibleSearchListLoc: false
    });
  }

  setSelectedUnit(unit) {
    this.setState({
      uk_kode: unit.muk_kode,
      uk_nama: unit.muk_nama,
      visibleSearchListUnit: false
    });
  }

  _renderItem = ({ item }) => (
    <ListItem
      data={item}
      setSelectedLocLabel={text => this.setSelectedLocLabel(text)}
    />
  );

  _renderItemUnit = ({ item }) => (
    <ListItemUnit
      data={item}
      setSelectedUnit={text => this.setSelectedUnit(text)}
    />
  );

  render() {
    let listLocation = this.state.listLocation.map((s, i) => {
      return (
        <Picker.Item key={i} value={s.EQUPMENTCODE} label={s.EQUPMENTCODE} />
      );
    });
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DailyReport")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>Create Temuan Unsafe</Title>
          </Body>

          <Right style={{ flex: 1 }} />
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ flex: 1 }}>
          <Content style={{ marginTop: 0 }}>
            <View style={{ backgroundColor: "#FEFEFE" }}>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View style={{ flex: 1 }}>
                    <Text style={styles.viewMerk}>Shift</Text>
                    <Form underlineColorAndroid="transparent">
                      <Picker
                        mode="dropdown"
                        iosIcon={
                          <Icon
                            name={
                              Platform.OS
                                ? "ios-arrow-down"
                                : "ios-arrow-down-outline"
                            }
                          />
                        }
                        style={{ width: "100%" }}
                        placeholder="Quantity"
                        placeholderStyle={{ color: "#bfc6ea" }}
                        placeholderIconColor="#007aff"
                        selectedValue={this.state.selectedShift}
                        onValueChange={itemValue =>
                          this.setState({ selectedShift: itemValue })
                        }
                      >
                        <Picker.Item label="Shift 1" value="1" />
                        <Picker.Item label="Shift 2" value="2" />
                        <Picker.Item label="Shift 3" value="3" />
                      </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View style={{ flex: 1 }}>
                    <Text style={styles.viewMerk}>Jenis Temuan</Text>
                    <Form underlineColorAndroid="transparent">
                      <Picker
                        mode="dropdown"
                        iosIcon={
                          <Icon
                            name={
                              Platform.OS
                                ? "ios-arrow-down"
                                : "ios-arrow-down-outline"
                            }
                          />
                        }
                        style={{ width: "100%" }}
                        placeholder="Quantity"
                        placeholderStyle={{ color: "#bfc6ea" }}
                        placeholderIconColor="#007aff"
                        selectedValue={this.state.selectedUnsafeType}
                        onValueChange={itemValue =>
                          this.setState({ selectedUnsafeType: itemValue })
                        }
                      >
                        <Picker.Item label="Unsafe Action" value="UA" />
                        <Picker.Item label="Unsafe Condition" value="UC" />
                      </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View style={{ flex: 1 }}>
                    <Text style={styles.viewMerk}>Priority</Text>
                    <Form underlineColorAndroid="transparent">
                      <Picker
                        mode="dropdown"
                        iosIcon={
                          <Icon
                            name={
                              Platform.OS
                                ? "ios-arrow-down"
                                : "ios-arrow-down-outline"
                            }
                          />
                        }
                        style={{ width: "100%" }}
                        placeholder="Quantity"
                        placeholderStyle={{ color: "#bfc6ea" }}
                        placeholderIconColor="#007aff"
                        selectedValue={this.state.selectedPriority}
                        onValueChange={itemValue =>
                          this.setState({ selectedPriority: itemValue })
                        }
                      >
                        <Picker.Item label="HIGH" value="1" />
                        <Picker.Item label="MEDIUM" value="2" />
                        <Picker.Item label="LOW" value="3" />
                      </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>

              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.viewMerk}>
                      Code Functional Location
                    </Text>
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <Button
                        block
                        style={{
                          justifyContent: "flex-start",
                          flex: 1,
                          borderWidth: Platform.OS === "ios" ? 1 : 0,
                          borderColor:
                            Platform.OS === "ios" ? colors.lightGray : null,
                          paddingLeft: 10,
                          marginRight: 5,
                          marginLeft: 5,
                          backgroundColor: colors.gray,
                          height: 35
                        }}
                        onPress={() => this.onClickSearchLoc()}
                      >
                        <Text style={{ fontSize: 12 }}>
                          {this.state.lokasi_temuan}
                        </Text>
                      </Button>
                    </View>
                    {this.state.visibleSearchListLoc && (
                      <View
                        style={{
                          height: 300,
                          flexDirection: "column",
                          borderWidth: 1,
                          padding: 10,
                          backgroundColor: colors.gray,
                          margin: 5,
                          borderColor: "#E6E6E6"
                        }}
                      >
                        <View>
                          <Textarea
                            value={this.state.searchWordLocation}
                            style={{
                              marginLeft: 5,
                              marginRight: 5,
                              fontSize: 11
                            }}
                            bordered
                            rowSpan={1.5}
                            onChangeText={text => this.onChangeLocation(text)}
                            placeholder="Ketik nama location"
                          />
                        </View>
                        <FlatList
                          data={this.state.listLocation}
                          renderItem={this._renderItem}
                          keyExtractor={(item, index) => index.toString()}
                        />
                      </View>
                    )}
                  </View>
                  {/* <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.lokasi_temuan} placeholder='Type Something ...' onChangeText={(text) => this.setState({ lokasi_temuan: text })} />
                                    </View> */}
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.viewMerk}>Unit Kerja / Vendor</Text>
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <Button
                        block
                        style={{
                          justifyContent: "flex-start",
                          flex: 1,
                          borderWidth: Platform.OS === "ios" ? 1 : 0,
                          borderColor:
                            Platform.OS === "ios" ? colors.lightGray : null,
                          paddingLeft: 10,
                          marginRight: 5,
                          marginLeft: 5,
                          backgroundColor: colors.gray,
                          height: 35
                        }}
                        onPress={() => this.onClickSearchUnit()}
                      >
                        <Text style={{ fontSize: 12 }}>
                          {this.state.uk_nama}
                        </Text>
                      </Button>
                    </View>
                    {this.state.visibleSearchListUnit && (
                      <View
                        style={{
                          height: 300,
                          flexDirection: "column",
                          borderWidth: 1,
                          padding: 10,
                          backgroundColor: colors.gray,
                          margin: 5,
                          borderColor: "#E6E6E6"
                        }}
                      >
                        <View>
                          <Textarea
                            value={this.state.searchWordUnit}
                            style={{
                              marginLeft: 5,
                              marginRight: 5,
                              fontSize: 11
                            }}
                            bordered
                            rowSpan={1.5}
                            onChangeText={text => this.onChangeUnit(text)}
                            placeholder="Ketik nama unit kerja"
                          />
                        </View>
                        <FlatList
                          data={this.state.listUnit}
                          renderItem={this._renderItemUnit}
                          keyExtractor={(item, index) => index.toString()}
                        />
                      </View>
                    )}
                  </View>
                  {/* <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.lokasi_temuan} placeholder='Type Something ...' onChangeText={(text) => this.setState({ lokasi_temuan: text })} />
                                    </View> */}
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.viewMerk}>Temuan Ketidak Sesuaian</Text>
                  </View>
                  <View>
                    <TextInput
                      returnKeyType={"next"}
                      onSubmitEditing={() => {
                        this.secondTextInput.focus();
                      }}
                      blurOnSubmit={false}
                      // placeholder = "secondTextInput"
                      style={{
                        marginLeft: 5,
                        marginRight: 5,
                        marginBottom: 5,
                        fontSize: 11
                      }}
                      rowSpan={3}
                      bordered
                      value={this.state.deskripsi_temuan}
                      placeholder="Type Something ..."
                      onChangeText={text =>
                        this.setState({ deskripsi_temuan: text })
                      }
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.viewMerk}>Penyebab / Masalah</Text>
                  </View>
                  <View>
                    <TextInput
                      ref={input => {
                        this.secondTextInput = input;
                      }}
                      returnKeyType={"next"}
                      onSubmitEditing={() => {
                        this.thirdTextInput.focus();
                      }}
                      blurOnSubmit={false}
                      style={{
                        marginLeft: 5,
                        marginRight: 5,
                        marginBottom: 5,
                        fontSize: 11
                      }}
                      rowSpan={3}
                      bordered
                      value={this.state.penyebab_temuan}
                      placeholder="Type Something ..."
                      onChangeText={text =>
                        this.setState({ penyebab_temuan: text })
                      }
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.viewMerk}>Potensi Bahaya</Text>
                  </View>
                  <View>
                    <TextInput
                      style={{
                        marginLeft: 5,
                        marginRight: 5,
                        marginBottom: 5,
                        fontSize: 11
                      }}
                      ref={input => {
                        this.thirdTextInput = input;
                      }}
                      returnKeyType={"next"}
                      onSubmitEditing={() => {
                        this.forthTextInput.focus();
                      }}
                      blurOnSubmit={false}
                      rowSpan={3}
                      bordered
                      value={this.state.potensi_bahaya_temuan}
                      placeholder="Type Something ..."
                      onChangeText={text =>
                        this.setState({ potensi_bahaya_temuan: text })
                      }
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.viewMerk}>Rekomendasi Safety</Text>
                  </View>
                  <View>
                    <TextInput
                      style={{
                        marginLeft: 5,
                        marginRight: 5,
                        marginBottom: 5,
                        fontSize: 11
                      }}
                      ref={input => {
                        this.forthTextInput = input;
                      }}
                      returnKeyType={"next"}
                      onSubmitEditing={() => {
                        this.fifthTextInput.focus();
                      }}
                      blurOnSubmit={false}
                      rowSpan={3}
                      bordered
                      value={this.state.rekomendasi_temuan}
                      placeholder="Type Something ..."
                      onChangeText={text =>
                        this.setState({ rekomendasi_temuan: text })
                      }
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.viewMerk}>
                      Rencana Tindak Lanjut Safety
                    </Text>
                  </View>
                  <View>
                    <TextInput
                      style={{
                        marginLeft: 5,
                        marginRight: 5,
                        marginBottom: 5,
                        fontSize: 11
                      }}
                      ref={input => {
                        this.fifthTextInput = input;
                      }}
                      
                      rowSpan={3}
                      bordered
                      value={this.state.tindak_lanjut_temuan}
                      placeholder="Type Something ..."
                      onChangeText={text =>
                        this.setState({ tindak_lanjut_temuan: text })
                      }
                    />
                  </View>
                </View>
              </CardItem>

              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View style={{ paddingBottom: 5 }}>
                    <Text style={styles.viewMerkFoto}>
                      Upload Foto (jpg, png) max : 500 kb
                    </Text>
                  </View>
                  <View>
                    <Ripple
                      style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      rippleSize={176}
                      rippleDuration={600}
                      rippleContainerBorderRadius={15}
                      onPress={this.pickImageHandler}
                      rippleColor={colors.accent}
                    >
                      <View style={styles.placeholder}>
                        <Image
                          source={{ uri: this.state.pickedImage }}
                          style={styles.previewImage}
                        />
                      </View>
                    </Ripple>
                  </View>
                </View>
              </CardItem>
            </View>
            <View style={styles.Contentsave}>
              <Button
                block
                style={{
                  height: 45,
                  marginLeft: 20,
                  marginRight: 20,
                  marginBottom: 20,
                  borderWidth: 1,
                  backgroundColor: "#00b300",
                  borderColor: "#00b300",
                  borderRadius: 4
                }}
                onPress={() => this.createUnsafe()}
              >
                <Text style={styles.buttonText}>Submit</Text>
              </Button>
            </View>
          </Content>

          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
              dialogTitle={<DialogTitle title="Creating Temuan Unsafe .." />}
            >
              <DialogContent>
                {<ActivityIndicator size="large" color="#330066" animating />}
              </DialogContent>
            </Dialog>
          </View>
        </View>
        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}

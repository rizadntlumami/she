import React, { Component } from "react";
import {
    StyleSheet,
    View,
    Image,
    StatusBar,
    ScrollView,
    ActivityIndicator,
    AsyncStorage,
    FlatList,
    Alert,
    Platform,
    Keyboard
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Text,
    Button,
    Left,
    Right,
    Body,
    Card,
    CardItem,
    Form,
    Textarea,
    Icon
} from "native-base";
import Dialog, { DialogTitle, SlideAnimation, DialogContent, DialogButton } from 'react-native-popup-dialog';
import LinearGradient from "react-native-linear-gradient";
import GlobalConfig from '../../components/GlobalConfig';

import styles from "../styles/ApprovalAPD";
import colors from "../../../styles/colors";

const helmet = require("../../../assets/images/helmet.png");

var that;
class ListItem extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            selected2: undefined
        };
    }

    onValueChange2(value) {
        this.setState({
            selected2: value
        });
    }

    render() {
        const { navigate } = that.props.navigation;
        return (
            <View>
                <Card style={{ marginBottom: 15 }}>
                    <CardItem>
                        <View style={{ flex: 1, flexDirection: 'row' }}>
                            <View style={styles.viewLeftHeader2}>
                                <Text style={styles.cardtext}>APD Name</Text>
                                <Text style={styles.cardtext}>Merk</Text>
                                <Text style={styles.cardtext}>Size</Text>
                                <Text style={styles.cardtext}>Quantity</Text>
                                <Text note style={styles.cardtext}>Note</Text>
                            </View>
                            <View>
                                <Text style={styles.cardtext}>: {this.props.data.nama_apd}</Text>
                                <Text style={styles.cardtext}>: {this.props.data.merk}</Text>
                                <Text style={styles.cardtext}>: {this.props.data.get_master_a_p_d[0].size}</Text>
                                <Text style={styles.cardtext}>: {this.props.data.jumlah}</Text>
                                <Text note style={styles.cardtext}>: {this.props.data.keterangan}</Text>
                            </View>
                        </View>
                    </CardItem>
                </Card>
            </View>
        )
    }
}

export default class ActionsApprovalPersonalAPD extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            isloading: true,
            deletedRowKey: null,
            visible: false,
            visibleDialogSubmit:false,
            textDialogSubmit:'',
            posDialog:null,
        };
    }

    static navigationOptions = {
        header: null
    };

    _renderItem = ({ item, index }) => (
        <ListItem data={item} index={index} parentFlatList={this}></ListItem>
    )

    componentDidMount() {
        AsyncStorage.getItem('idAndKode').then((value) => {
            var temp = value.split("+");
            console.log(temp[0]);
            console.log(temp[1]);
            this.setState({ 'id': temp[0] });
            this.setState({ 'kodeOrder': temp[1] });
            this.loadData(temp[0]);
        })

        AsyncStorage.getItem("token").then(token => {
            const url = GlobalConfig.SERVERHOST + 'validasi';
            var formData = new FormData();
            formData.append("token", token)
            fetch(url, {
              headers: {
                "Content-Type": "multipart/form-data"
              },
              method: "POST",
              body: formData
            })
              .then(response => response.json())
              .then(responseJson => {
                // alert(JSON.stringify(responseJson))
                this.setState({
                  listProfile: responseJson.user,
                  name: responseJson.user.name,
                  badge: responseJson.user.no_badge,
                  isloading: false
                });
              })
              .catch(error => {
                console.log(error);
              });
          });
          
    }

    componentWillMount () {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
      }

    componentWillUnmount () {
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
      }
    
      _keyboardDidShow () {
        console.log('keyboard show')
        this.setState({
            posDialog:'10%'
        })
      }
    
      _keyboardDidHide () {
        console.log('keyboard not show')
        this.setState({
            posDialog:null
        })
      }

    loadData(id) {
        AsyncStorage.getItem('token').then((value) => {
            // alert(JSON.stringify(value));
            const url = GlobalConfig.SERVERHOST + 'detailorder/getid';
            var formData = new FormData();
            formData.append("token", value)
            formData.append("kode_order", this.state.id)

            fetch(url, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                body: formData
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    //alert(JSON.stringify(responseJson));
                    this.setState({
                        dataSource: responseJson.data,
                        isloading: false
                    });
                })
                .catch((error) => {
                    console.log(error)
                })
        })
    }

    approvePersonal() {
        this.setState({
            textDialogSubmit:'Approving Order...',
        })
        this.setState({
            visibleDialogSubmit:true,
        })
        AsyncStorage.getItem("token").then(value => {
        // alert(JSON.stringify(this.state.token));
        const url = GlobalConfig.SERVERHOST + 'orderapd/approve';
        var formData = new FormData();
        formData.append("token", value);
        formData.append("id", this.state.id);
        formData.append("update_by", this.state.badge);
        formData.append("status", 'approve');

        // alert(JSON.stringify(url));

        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                // alert(JSON.stringify(responseJson));
                if (responseJson.status == 200) {
                    this.setState({
                        visibleDialogSubmit:false,
                    })
                    Alert.alert('Success', 'Approve Success, Order ID: '+this.state.kodeOrder, [{
                        text: 'Okay'
                    }])
                    this.props.navigation.navigate('ApprovalPersonalAPD')
                } else {
                    this.setState({
                        visibleDialogSubmit:false,
                    })
                    Alert.alert('Error', 'Approve Failed', [{
                        text: 'Okay'
                    }])
                }
            })
            .catch((error) => {
                Alert.alert('Error', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                  console.log(error)
              })
            })
    }

    rejectPersonal() {
        this.setState({
            textDialogSubmit:'Rejecting Order...',
        })
        this.setState({
            visibleDialogSubmit:true,
        })
        // alert(JSON.stringify(this.state.kode_order))
        AsyncStorage.getItem("token").then(value => {
        const url = GlobalConfig.SERVERHOST + 'orderapd/reject';
        // alert(this.state.token);
        var formData = new FormData();
        formData.append("token", value)
        formData.append("id", this.state.id)
        formData.append("reject_by", this.state.badge)
        formData.append("note_reject", this.state.note)
        formData.append("status", 'reject')

        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                // alert(JSON.stringify(responseJson));
                if (responseJson.status == 200) {
                    this.setState({
                        visibleDialogSubmit:false,
                    })
                    Alert.alert('Success', 'Reject Success, Order ID: '+this.state.kodeOrder, [{
                        text: 'Okay'
                    }])
                    this.setState({
                        visible: false
                    });
                    this.props.navigation.navigate('ApprovalPersonalAPD')
                } else {
                    this.setState({
                        visibleDialogSubmit:false,
                    })
                    Alert.alert('Error', 'Reject Failed', [{
                        text: 'Okay'
                    }])
                    this.setState({
                        visible: false
                    });
                }
            })
            .catch((error) => {
                Alert.alert('Error', 'Check Your Internet Connection', [{
                    text: 'Okay'
                }])
                  console.log(error)
              })
            })
    }

    render() {
        that = this;
        return (
            this.state.isloading
                ?
                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
                    <ActivityIndicator size="large" color="#330066" animating />
                </View>
                :
                <Container style={styles.wrapper}>
                    <Header style={styles.header}>
                        <Left style={{flex:1}}>
                            <Button transparent onPress={() => this.props.navigation.navigate("ApprovalPersonalAPD")}>
                                  <Icon
                                      name="ios-arrow-back"
                                      size={20}
                                      style={styles.facebookButtonIcon}
                                  />
                            </Button>
                        </Left>
                        <Body style={{flex:4,alignItems:'center'}}>
                            <Title style={styles.textbody}>Approval Actions</Title>
                        </Body>
                        <Right style={{flex:1}}/>
                    </Header>
                    <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                    <View style={styles.homeWrapper}>
                        <Content style={{ marginLeft: 20, marginRight: 20 }}>
                            <FlatList
                                data={this.state.dataSource}
                                renderItem={this._renderItem}
                                keyExtractor={(item, index) => index.toString()}
                            />
                            <View style={styles.Contentsave}>
                                <Button block style={{
                                    height: 45,
                                    width: 130,
                                    borderWidth: 1,
                                    backgroundColor: '#1c84c6',
                                    borderColor: '#1c84c6',
                                    margin: 10,
                                    borderRadius: 4
                                }}
                                    onPress={() => that.approvePersonal()}>
                                    <Text style={styles.buttonText}>Approve</Text>
                                </Button>
                                <Button block style={{
                                    height: 45,
                                    width: 130,
                                    borderWidth: 1,
                                    backgroundColor: '#ed5565',
                                    borderColor: '#ed5565',
                                    margin: 10,
                                    borderRadius: 4
                                }}
                                    onPress={() => {
                                        this.setState({ visible: true });
                                    }}> 
                                    <Text style={styles.buttonText}>Reject</Text>
                                </Button>
                                <View style={{ width: 270, position: 'absolute' }}>
                                    <Dialog
                                        visible={this.state.visible}
                                        dialogAnimation={new SlideAnimation({
                                            slideFrom: 'bottom',
                                        })}
                                        dialogStyle={{ position: 'absolute', top: this.state.posDialog }}
                                        onTouchOutside={() => {
                                            this.setState({ visible: false });
                                        }}
                                        dialogTitle={<DialogTitle title="Place reject note here" />}
                                        // actions={[
                                        //     <DialogButton
                                        //         style={{ fontSize: 11, backgroundColor: colors.white }}
                                        //         text="SUBMIT"
                                        //         onPress={() => that.rejectPersonal()}
                                        //     />,
                                        // ]}
                                    >
                                        <DialogContent>
                                            {
                                                <Form>
                                                    <Textarea
                                                        style={{ width: 270 }}
                                                        rowSpan={5}
                                                        bordered placeholder="Type something .."
                                                        onChangeText={(text) => this.setState({ note: text })} />
                                                    <DialogButton
                                                        style={{ fontSize: 11, backgroundColor: colors.white }}
                                                        text="SUBMIT"
                                                        onPress={() => that.rejectPersonal()}
                                                    />
                                                </Form>
                                            }
                                        </DialogContent>
                                    </Dialog>
                                </View>
                                <View style={{ width: 270, position: "absolute" }}>
                                <Dialog
                                    visible={this.state.visibleDialogSubmit}
                                    dialogAnimation={
                                    new SlideAnimation({
                                        slideFrom: "bottom"
                                    })
                                    }
                                    dialogTitle={<DialogTitle title={this.state.textDialogSubmit}/>}
                                >
                                    <DialogContent>
                                    {<ActivityIndicator size="large" color="#330066" animating />}
                                    </DialogContent>
                                </Dialog>
                                </View>
                            </View>
                        </Content>
                    </View>
                </Container>
        );
    }
}

import React, { Component } from "react";
import { Platform,Image,AsyncStorage } from "react-native";
import { Footer, FooterTab, Text, Button, Icon, View, Badge } from "native-base";
import GlobalConfig from "./GlobalConfig";

import styles from "../screens/styles/CustomFooter";

class CustomFooter extends Component {
  constructor(props) {
    super(props);
    this.state={
      notif:0
    }
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      this.loadNotif();
    });
    //this.loadData();
    
  }

  loadNotif(){
    console.log('notif called')
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/apd/order/approval_count";
      var formData = new FormData();
      formData.append("token", value);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          if(responseJson.status==200){
            this.setState({
              notif: responseJson.data,
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  render() {
    
    return (
      <View>
      {((this.props.menu != "APD")&&(this.props.menu != "Safety")&&(this.props.menu != "FireSystem")) && (<Footer>
        
        <FooterTab style={{ backgroundColor: "#ebebeb"}}>
        {this.props.menu == "Home" ? (
          <Button
            vertical
            onPress={() => this.props.navigation.navigate("HomeMenu")}
          >
              <Image
                source={require("../../assets/images/Home-Color.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: "#27ae60", fontSize:10}}>Home</Text>
          </Button>
        ):(
          <Button
            vertical
            onPress={() => this.props.navigation.navigate("HomeMenu")}
          >
              <Image
                source={require("../../assets/images/Home.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: "#808080", fontSize:10 }}>Home</Text>
          </Button>
        )}
          

          {this.props.menu == "MyOrder" ? (
            <Button
              vertical
              // active
              onPress={() => this.props.navigation.navigate("MyOrderPersonalAPD")}
              // style={{color:'#808080', fontSize:10, backgroundColor: '#27ae60'}}
            >
              <Image
                source={require("../../assets/images/My-Order-Color.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: "#27ae60", fontSize:10}}>My Order</Text>
            </Button>
          ) : (
            <Button
              vertical
              onPress={() => this.props.navigation.navigate("MyOrderPersonalAPD")}
            >
              <Image
                source={require("../../assets/images/My-Order.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: "#808080", fontSize:10 }}>My Order</Text>
            </Button>
          )}

          {this.props.menu == "Laporan" ? (
            <Button badge
              vertical
              
              onPress={() => this.props.navigation.navigate("LaporanAPD")}
              // style={{color:'#808080', fontSize:10, backgroundColor: '#27ae60'}}
            >  
            {this.state.notif!=0 &&(
              <Badge style={{ position: "absolute", left: '50%', top: '10%', }}><Text>{this.state.notif}</Text></Badge>
            )}
                       
              <Image
                source={require("../../assets/images/Laporan-Color.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              /> 
         
              <Text style={{  color: "#27ae60", fontSize:10 }}>Report</Text>
          
            </Button>
          
          ) : (
            <Button badge
              vertical
              onPress={() => this.props.navigation.navigate("LaporanAPD")}
            >
            {this.state.notif!=0 &&(
              <Badge style={{ position: "absolute", left: '50%', top: '10%', }}><Text>{this.state.notif}</Text></Badge>
            )}
             
              <Image
                source={require("../../assets/images/Laporan.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: "#808080", fontSize:10 }}>Report</Text>
            </Button>
          )}

          {this.props.menu == "Profil" ? (
            <Button
              vertical
              // active
              onPress={() => this.props.navigation.navigate("ProfilUser")}
              // style={{color:'#808080', fontSize:10, backgroundColor: '#27ae60'}}
            >
              <Image
                source={require("../../assets/images/Profil-Color.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{  color: "#27ae60", fontSize:10 }}>Profile</Text>
            </Button>
          ) : (
            <Button
              vertical
              onPress={() => this.props.navigation.navigate("ProfilUser")}
            >
              <Image
                source={require("../../assets/images/Profil.png")}
                style={{ width: 25, height: 25, resizeMode: "contain" }}
              />
              <Text style={{ color: "#808080", fontSize:10 }}>Profile</Text>
            </Button>
          )}


        </FooterTab>
      </Footer>)}

      </View>
      
      
    );
  }
}

export default CustomFooter;

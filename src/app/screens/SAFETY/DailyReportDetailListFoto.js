import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Text,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Form,
  Picker
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";

import moment from 'moment'
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewReportDetail from "../../components/ListViewReportDetail";

class DailyReportDetailListFoto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      dataDetail: [],
      isLoading: true,
      headerUnsafeDetailList: [],
      status: "",
      visibleUpdate: false,
      visibleDialogSubmit:false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  navigateToScreen(route, id_listReport) {
    // alert(this.state.headerUnsafeDetailList.ID);
    AsyncStorage.setItem("id", JSON.stringify(id_listReport)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  componentDidMount() {
    
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        AsyncStorage.getItem("list").then(id_listReport => {
          var temuanDate = moment(JSON.parse(id_listReport).CREATE_AT, 'DD-MMM-YYYY');
          var temuanDateString = temuanDate.format('YYYY-MM-DD')
          var realDate = moment(JSON.parse(id_listReport).TGL_REAL, 'DD-MMM-YYYY');
          var realDateString = realDate.format('YYYY-MM-DD')
          this.setState({ headerUnsafeDetailList: JSON.parse(id_listReport) });
          this.setState({ listFileUpload: this.state.headerUnsafeDetailList });
          this.setState({ status: JSON.parse(id_listReport).STATUS });
          this.setState({ isLoading: false });
          this.setState({ tanggal : temuanDateString})
          this.setState({ jenisTemuan : JSON.parse(id_listReport).JENIS_TEMUAN})
          this.setState({ tgl_real : realDateString})
          console.log(this.state.headerUnsafeDetailList);
          console.log(this.state.status);
          console.log(this.state.tanggal);
        });
        AsyncStorage.getItem('idAndShift').then((idAndShift) => {
          var temp = idAndShift.split('+');
          console.log(temp[0])
          console.log(temp[1])
          var shift = temp[1]
          this.setState({
              selectedShift: shift
          })
        })
        //this.loadData(id_Unsafe);
      }
    );
  }

  konfirmasideleteHeader() {
    Alert.alert(
      "Apakah Anda Yakin Ingin Menghapus",
      this.state.headerUnsafeDetailList.NO_DOKUMEN,
      [
        { text: "Yes", onPress: () => this.deleteDetailTemuan() },
        { text: "Cancel" }
      ],
      { cancelable: false }
    );
  }

  deleteDetailTemuan() {
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/delete_detail";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID", this.state.headerUnsafeDetailList.ID);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            Alert.alert("Success", "Delete Success", [
              {
                text: "Okay"
              }
            ]);
            this.props.navigation.navigate("DailyReportDetailList");
          } else {
            Alert.alert("Error", "Delete Failed", [
              {
                text: "Okay"
              }
            ]);
          }
        });
    });
  }

  updateData() {
        this.setState({
            visibleDialogSubmit:true,
            visibleUpdate:false
        })
        AsyncStorage.getItem("token").then(value => {
            const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe/update_detail';
            var formData = new FormData();
            formData.append("token", value);
            formData.append("ID", this.state.headerUnsafeDetailList.ID);
            formData.append("STATUS", this.state.status);
            formData.append("SHIFT", this.state.selectedShift);
            formData.append("JENIS_TEMUAN", this.state.jenisTemuan);
            formData.append("TANGGAL", this.state.tanggal);
            if (this.state.tgl_real!="Invalid date"){
              formData.append("TGL_REAL", this.state.tgl_real);
            }  
            console.log(formData)
            fetch(url, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                body: formData
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    console.log(responseJson)
                    if (responseJson.status == 200) {
                        this.setState({
                            visibleDialogSubmit:false
                        })
                        Alert.alert('Success', 'Update Temuan Unsafe Success', [{
                            text: 'Okay'
                        }])
                        this.props.navigation.navigate('DailyReportDetailList')
                    } else {
                        this.setState({
                            visibleDialogSubmit:false
                        })
                        Alert.alert('Error', 'Update Temuan Unsafe Failed', [{
                            text: 'Okay'
                        }])
                    }
                })
                .catch(error => {
                    this.setState({
                        visibleDialogSubmit:false
                    })
                    Alert.alert('Error', 'Update Temuan Unsafe Failed', [{
                        text: 'Okay'
                    }])
                    console.log(error)
                })
        })   
}

  render() {
    var status=""
    if (this.state.headerUnsafeDetailList.STATUS!=undefined){
      status=this.state.headerUnsafeDetailList.STATUS
    }
    
    var listReportK3;
    var listDetailReportK3;
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() =>
                this.props.navigation.navigate("DailyReportDetailList")
              }
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>Detail Report</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button
              transparent
              // onPress={() => this.navigateToScreen('DailyReportDetailListUpdate', this.state.headerUnsafeDetailList)}
              onPress={() => this.setState({ visibleUpdate: true })}
            >
              <Icon
                name="ios-create"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            <Button transparent onPress={() => this.konfirmasideleteHeader()}>
              <Icon
                name="ios-trash"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Content style={{ marginTop: 10 }}>
          <ScrollView>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Text
                style={{
                  fontSize: 12,
                  color: colors.green01,
                  fontWeight: "bold"
                }}
              >
                {this.state.headerUnsafeDetailList.NO_DOKUMEN}
              </Text>
            </View>
            <CardItem style={{ borderRadius: 0, marginTop: 5 }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2, marginRight: 10 }}>
                {this.state.headerUnsafeDetailList.FOTO_BEF == null ? (
                    <Image
                      style={{
                        height: 200,
                        width: "100%",
                        borderColor: colors.gray,
                        borderWidth: 3,
                        borderRadius: 10
                      }}
                      source={require("../../../assets/images/imgorder.png")}
                    />
                  ) : (
                    <Image
                    style={{
                      height: 200,
                      width: "100%",
                      borderColor: colors.gray,
                      borderWidth: 3,
                      borderRadius: 10
                    }}
                    source={{
                      uri:
                        GlobalConfig.SERVERHOST +
                        "api" +
                        this.state.headerUnsafeDetailList.FOTO_BEF + '?' + new Date()
                    }}
                  />
                  )}
                  
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0 }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2 }}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Type
                  </Text>
                  {this.state.headerUnsafeDetailList.JENIS_TEMUAN == "UA" ? (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Unsafe Action
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Unsafe Condition
                    </Text>
                  )}
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Location
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnsafeDetailList.LOKASI_KODE} -{" "}
                    {this.state.headerUnsafeDetailList.LOKASI_TEXT}
                  </Text>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Priority
                  </Text>
                  {this.state.headerUnsafeDetailList.PRIORITY == 1 && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      HIGH
                    </Text>
                  )}
                  {this.state.headerUnsafeDetailList.PRIORITY == 2 && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      MEDIUM
                    </Text>
                  )}
                  {this.state.headerUnsafeDetailList.PRIORITY == 3 && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      LOW
                    </Text>
                  )}
                  
                  {((status).toUpperCase())=="OPEN"&&(
                    <View>
                      <Text style={{fontSize:10, paddingTop:10}}>Status</Text>
                      <Text style={{fontSize:10, fontWeight:"bold", color:colors.green01}}>{this.state.headerUnsafeDetailList.STATUS}</Text>
                    </View>
                  )}
                  {((status).toUpperCase())=="CLOSE"&&(
                    <View>
                      <Text style={{fontSize:10, paddingTop:10}}>Status</Text>
                      <Text style={{fontSize:10, fontWeight:"bold", color:colors.red}}>{this.state.headerUnsafeDetailList.STATUS}</Text>
                    </View>
                  )}
                  {((((status).toUpperCase())!="CLOSE")&&(((status).toUpperCase())!="OPEN"))&&(
                    <View>
                      <Text style={{fontSize:10, paddingTop:10}}>Status</Text>
                      <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.headerUnsafeDetailList.STATUS}</Text>
                    </View>
                  )}
                  
                  

                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Inspector (PIC Penemu)
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnsafeDetailList.CREATE_BY}
                  </Text>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Temuan Ketidaksesuaian
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnsafeDetailList.TEMUAN}
                  </Text>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Penyebab / Masalah
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnsafeDetailList.PENYEBAB}
                  </Text>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Potensi Bahaya
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnsafeDetailList.POTENSI_BAHAYA}
                  </Text>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Rekomendasi Safety
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnsafeDetailList.REKOMENDASI}
                  </Text>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Rencana Tindak Lanjut
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnsafeDetailList.TINDAK_LANJUT}
                  </Text>
                  {/* <Text style={{ fontSize: 10, paddingTop: 10 }}>KASI Temuan</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerUnsafeDetailList.KASI} - {this.state.headerUnsafeDetailList.KASI_NAME}</Text> */}
                </View>
              </View>
            </CardItem>
            <CardItem
              style={{ borderRadius: 0, marginTop: 5, marginBottom: 10 }}
            >
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2, marginRight: 10 }}>
                  <Text
                    note
                    style={{ fontSize: 10, paddingTop: 10, marginBottom: 10 }}
                  >
                    Gambar Temuan Setelah Tindak Lanjut
                  </Text>
                  {this.state.headerUnsafeDetailList.FOTO_AFT == null ? (
                    <Image
                      style={{
                        height: 200,
                        width: "100%",
                        borderColor: colors.gray,
                        borderWidth: 3,
                        borderRadius: 10
                      }}
                      source={require("../../../assets/images/imgorder.png")}
                    />
                  ) : (
                    <Image
                      style={{
                        marginTop: 10,
                        height: 200,
                        width: 300,
                        borderColor: colors.gray,
                        borderWidth: 3
                      }}
                      source={{
                        uri:
                          GlobalConfig.SERVERHOST +
                          "api" +
                          this.state.headerUnsafeDetailList.FOTO_AFT + '?' + new Date()
                      }}
                    />
                  )}
                  {/* <Image style={{ marginTop: 10, height: 200, width: 300, borderColor: colors.gray, borderWidth: 3 }} source={{ uri: GlobalConfig.SERVERHOST + 'api' + this.state.headerUnsafeDetailList.FOTO_AFT }} /> */}
                </View>
              </View>
            </CardItem>
          </ScrollView>
        </Content>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleUpdate}
            dialogTitle={<DialogTitle textStyle={{fontSize:13}} title="Update Status Temuan Unsafe" />}
            
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            onTouchOutside={() => {
              this.setState({ visibleUpdate: false });
            }}
            dialogStyle={{ position: "absolute", width: "80%" }}
          >
            <DialogContent
              style={{
                backgroundColor: colors.white
              }}
            >
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Status</Text>
                    <Form
                      style={{
                        borderWidth: 1,
                        borderRadius: 5,
                        marginRight: 5,
                        marginLeft: 5,
                        borderColor: "#E6E6E6",
                        height: 35
                      }}
                    >
                      <Picker
                        mode="dropdown"
                        iosIcon={
                          <Icon
                            name={
                              Platform.OS
                                ? "ios-arrow-down"
                                : "ios-arrow-down-outline"
                            }
                          />
                        }
                        style={{ width: "100%", height: 35 }}
                        placeholder="Quantity"
                        placeholderStyle={{ color: "#bfc6ea" }}
                        placeholderIconColor="#007aff"
                        selectedValue={this.state.status}
                        onValueChange={itemValue =>
                          this.setState({ status: itemValue })
                        }
                      >
                        <Picker.Item label="OPEN" value="OPEN" />
                        <Picker.Item label="CLOSE" value="CLOSE" />
                      </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Button
                    block
                    style={{
                      height: 45,
                      marginLeft: 20,
                      marginRight: 20,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: colors.green0,
                      borderColor: colors.green0,
                      borderRadius: 4
                    }}
                    onPress={() => this.updateData()}
                  >
                    <Text style={styles.buttonText}>Update</Text>
                  </Button>
                </View>
              </View>
            </DialogContent>
          </Dialog>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
                                <Dialog
                                    visible={this.state.visibleDialogSubmit}
                                    dialogTitle={<DialogTitle title='Updating Temuan Unsafe ..'/>}
                                >
                                    <DialogContent>
                                    {<ActivityIndicator size="large" color="#330066" animating />}
                                    </DialogContent>
                                </Dialog>
                                </View>
        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}

export default DailyReportDetailListFoto;

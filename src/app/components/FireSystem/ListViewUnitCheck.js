import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../../styles/colors";
import GlobalConfig from "../GlobalConfig";
import Moment from 'moment'

export default class ListViewUnitCheck extends Component {
    render() {
        var dateNow = Moment().format('YYYY-MM-DD');
        return (
            <View
                style={{
                    height: 95,
                    width: "100%",
                }}
            >
                <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginLeft: 5, marginTop: 5, width: '18%' }}>
                        <Image
                            source={{uri:GlobalConfig.IMAGEHOST + 'MasterVehicle/' + this.props.imageUri}}
                            style={{ marginTop: 5, width: 50, height: 70, resizeMode: "contain" }}
                        />
                    </View>
                    <View style={{ marginTop: 5, paddingRight: 5, marginRight: 5, width: '60%' }}>
                        <Text style={{ fontSize: 10, fontWeight: 'bold' }}>{this.props.vehicleCode}</Text>
                        <Text style={{ fontSize: 9, paddingTop: 4, }}>Merk : {this.props.vehicleMerk}</Text>
                        <Text style={{ fontSize: 9, paddingTop: 2, }}>Tahun : {this.props.tahun}</Text>
                        <Text style={{ fontSize: 9, paddingTop: 2, }}>No Rangka : {this.props.nomorRangka}</Text>
                        <Text style={{ fontSize: 9, paddingTop: 2, }}>Last Inspection : {this.props.last_inspection}</Text>
                    </View>
                    <View style={{ marginTop: 5, paddingRight: 5, marginRight: 5, width: '17%' }}>
                        {this.props.exp_inspection <= dateNow ? (
                            <Text
                                note
                                style={{
                                    alignSelf: "flex-end",
                                    fontSize: 8,
                                    fontWeight: 'bold',
                                    color: colors.Orange,
                                    // paddingRight: 4,
                                    paddingLeft: 4,
                                    paddingBottom: 2,
                                    paddingTop: 2,
                                }}
                            >BELUM DIPERIKSA
                            </Text>
                        ): (
                          <View>
                            {this.props.sumNegative = 'null' || this.props.sumNegative == 0 || this.props.sumNegative == null ?(
                            <Text
                                note
                                style={{
                                    alignSelf: "flex-end",
                                    fontSize: 8,
                                    fontWeight: 'bold',
                                    color: "white",
                                    backgroundColor: colors.green01,
                                    paddingRight: 4,
                                    paddingLeft: 4,
                                    paddingBottom: 2,
                                    paddingTop: 2,
                                    borderRadius: 6
                                }}
                              >NOT NEGATIF
                            </Text>
                            ):(
                            <Text
                                note
                                style={{
                                    alignSelf: "flex-end",
                                    fontSize: 8,
                                    fontWeight: 'bold',
                                    color: "white",
                                    backgroundColor: colors.red,
                                    paddingRight: 4,
                                    paddingLeft: 4,
                                    paddingBottom: 2,
                                    paddingTop: 2,
                                    borderRadius: 6
                                }}
                            >NEGATIF {this.props.sumNegative}
                            </Text>
                            )}
                          </View>
                        )}
                        <Text style={{ fontSize: 8, marginTop: 30, fontStyle: "italic" }}>{this.props.date}</Text>
                        <Text style={{ fontSize: 8, marginTop: 2, fontStyle: "italic" }}>{this.props.time}</Text>
                    </View>
                </View>
            </View>
        );
    }
}

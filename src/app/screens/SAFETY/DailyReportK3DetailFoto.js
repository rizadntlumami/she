import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewDetailFoto from "../../components/ListViewDetailFoto";

class DailyReportK3DetailFoto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isLoading: true,
    };
  }

  static navigationOptions = {
      header: null
  };

  _renderItem = ({ item }) => (
      <ListItem data={item}></ListItem>
  )

  navigateToScreen(route, id_Unsafe, id_Unsafe_RPT) {
    AsyncStorage.setItem('id', id_Unsafe).then(() => {
      AsyncStorage.setItem('id2', id_Unsafe_RPT).then(() => {
        this.props.navigation.navigate(route);
      })
    })
  }

  navigateToScreenCreate(route, id_Unsafe) {
    AsyncStorage.setItem('id', id_Unsafe).then(() => {
        this.props.navigation.navigate(route);
    })
  }

  componentDidMount() {
      AsyncStorage.getItem('id').then((id_Unsafe) => {
        AsyncStorage.getItem('id2').then((id_Unsafe_RPT) => {
          this.setState({
            idReport: id_Unsafe,
            idReportRPT : id_Unsafe_RPT
          });
          this.loadData(id_Unsafe, id_Unsafe_RPT);
        })
      })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        this.loadData(id_Unsafe, id_Unsafe_RPT);
      });
  }

  loadData(id_Unsafe, id_Unsafe_RPT) {
        AsyncStorage.getItem('token').then((value) => {
            const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe_n/view_report_list_detail';
            var formData = new FormData();
            formData.append("token", value)
            formData.append("search", "")
            formData.append("length", 100)
            formData.append("start", 0)
            formData.append("id_unsafe_report", id_Unsafe)

            fetch(url, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                body: formData
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        dataSource: responseJson.data.filter(x => x.DELETE_FLAG == 0 && x.ID_UNSAFE_RPT_DETAIL == id_Unsafe_RPT),
                        isloading: false
                    });
                })
                .catch((error) => {
                    console.log(error)
                })
        })
  }



  render() {
    var listDetailReportK3Foto;
    return (
      <Container style={styles.wrapper}>
          <Header style={styles.header}>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("DailyReportK3Detail")}
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Left>
            <Body>
              <Title style={styles.textbody}>Detail Daily Report K3</Title>
            </Body>
            {/* <Right/> */}
          </Header>
          <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
          <Content style={{marginTop:10}}>

              <ScrollView>
                  {this.state.dataSource.map((listDetailReportK3Foto) => (
                      <View key={listDetailReportK3Foto} style={{ backgroundColor: "#FEFEFE" }}>
                          <Card style={{ marginLeft: 10, marginRight: 10, borderRadius:10}}>
                              <View style={{marginTop:5, marginLeft:5, marginRight:5}}>
                                  <ListViewDetailFoto
                                    unitCode={listDetailReportK3Foto.UNIT_NAME}
                                    status={listDetailReportK3Foto.STATUS}
                                    date={listDetailReportK3Foto.CREATE_DATE}
                                    type={listDetailReportK3Foto.UNSAFE_TYPE}
                                    img={GlobalConfig.SERVERHOST + 'api' + listDetailReportK3Foto.UPLOADED_PATH}
                                  />
                              </View>
                          </Card>
                  </View>
                ))}
              </ScrollView>
          </Content>
          <CustomFooter navigation={this.props.navigation} menu='Safety' />
      </Container>

    );
  }
}

export default DailyReportK3DetailFoto;

import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input,
    Textarea
} from "native-base";
import Dialog, { DialogTitle, SlideAnimation, DialogContent, DialogButton } from 'react-native-popup-dialog';
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewHeader from "../../components/ListViewHeader";
import ListViewDetail from "../../components/ListViewDetail";
import Ripple from "react-native-material-ripple";
import ImagePicker from "react-native-image-picker";
import CustomRadioButton from "../../components/CustomRadioButton";

var that
class ListItemKebakaran extends React.PureComponent {
  konfirmasideleteFireReport(id) {
    Alert.alert(
      'Apakah Anda Yakin Ingin Menghapus Foto', id,
      [
        {text: 'Yes', onPress:()=> this.deleteFireReport(id)},
        {text: 'Cancel'},
      ],
      { cancelable:false}
    )
  }

  deleteFireReport(id) {
      const url = GlobalConfig.SERVERHOST + 'deleteFireReportImage';
      var formData = new FormData();
      formData.append("id", id);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            Alert.alert('Success', 'Delete Success', [{
              text: 'Oke',
            }])
            that.loadImage()
          } else {
            Alert.alert('Error', 'Delete Failed', [{
              text: 'Oke'
            }])
          }
        });
  }

  render(){
    return(
      <View style={{backgroundColor:colors.white, marginBottom:10, borderRadius:10}}>
        <View style={styles.placeholder}>
          <Image style={styles.previewImage} source={{uri : GlobalConfig.IMAGEHOST + 'FireReport/' + this.props.data.image}}/>
        </View>
        <View style={{flex:1, flexDirection: 'row', paddingLeft:5, paddingRight:5}}>
          <View style={{width:'90%', paddingTop:5, paddingBottom:5}}>
            <Text style={{fontSize:10}}>Keterangan : {this.props.data.description}</Text>
          </View>
          <View style={{width:'10%', paddingTop:5, paddingBottom:5}}>
            <Text style={{fontSize:10, fontWeight:'bold', color:colors.red}} onPress={() => this.deleteFireReport(this.props.data.id)}>Delete</Text>
          </View>
        </View>
      </View>
    )
  }
}

class ListItemFollowUp extends React.PureComponent {
  konfirmasideleteFireReport(id) {
    Alert.alert(
      'Apakah Anda Yakin Ingin Menghapus Foto', id,
      [
        {text: 'Yes', onPress:()=> this.deleteFireReport(id)},
        {text: 'Cancel'},
      ],
      { cancelable:false}
    )
  }


  deleteFireReport(id) {
      const url = GlobalConfig.SERVERHOST + 'deleteFireReportImage';
      var formData = new FormData();

      formData.append("id", id);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            Alert.alert('Success', 'Delete Success', [{
              text: 'Oke',
            }])
            that.loadImage()
          } else {
            Alert.alert('Error', 'Delete Failed', [{
              text: 'Oke'
            }])
          }
        });
  }

  render(){
    return(
      <View style={{backgroundColor:colors.white, marginBottom:10, borderRadius:10}}>
        <View style={styles.placeholder}>
          <Image style={styles.previewImage} source={{uri : GlobalConfig.IMAGEHOST + 'FireReport/' + this.props.data.image}}/>
        </View>
        <View style={{flex:1, flexDirection: 'row', paddingLeft:5, paddingRight:5}}>
          <View style={{width:'90%', paddingTop:5, paddingBottom:5}}>
            <Text style={{fontSize:10, fontFamily:'Montserrat-Light'}}>Keterangan : {this.props.data.description}</Text>
          </View>
          <View style={{width:'10%', paddingTop:5, paddingBottom:5}}>
            <Text style={{fontSize:10, fontWeight:'bold', color:colors.red}} onPress={() => this.deleteFireReport(this.props.data.id)}>Delete</Text>
          </View>
        </View>
      </View>
    )
  }
}

class FireReportImage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isLoading: true,
      idFireReport:'',
      listFireReportKebakaran:[],
      listFireReportFollowUp:[],

      listFileKebakaran:[],
      listFileFollowUp:[],
      isHiddenUpload:true,
      isHiddenFollowUp:true,
      pickedImageUpload: '',
      uriUpload: '',
      fileTypeUpload:'',
      fileNameUpload: '',
      pickedImageFollowUp: '',
      uriFollowUp: '',
      fileTypeFollowUp:'',
      fileNameFollowUp: '',
      fireNumber:'',
      id:'',
      status:'',
      visibleFoto:false,
    };
  }

  static navigationOptions = {
      header: null
  };

  _renderItemKebakaran = ({ item }) => (
      <ListItemKebakaran data={item}/>
  )

  _renderItemFollowUp = ({ item }) => (
      <ListItemFollowUp data={item}/>
  )

  componentDidMount() {
      AsyncStorage.getItem('list').then((listFireReport) => {
          this.setState({
            idFireReport:listFireReport,
          })
          this.loadImage()
      })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {

      });
  }

  loadImage(){
    this.setState({
      isLoading: true
    });
      const url = GlobalConfig.SERVERHOST + "getFireReportImage";
      var formData = new FormData();
      formData.append("id_fire_report", this.state.idFireReport);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listFireReportKebakaran: responseJson.data.filter(x => x.status == 'UPLOAD'),
            listFireReportFollowUp: responseJson.data.filter(x => x.status == 'FOLLOWUP'),
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
  }

  pickImageHandlerUpload = () => {
      ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
          if (res.didCancel) {
              console.log("User cancelled!");
          } else if (res.error) {
              console.log("Error", res.error);
          } else {
              this.setState({
                  pickedImageUpload: res.uri,
                  uriUpload: res.uri,
                  fileTypeUpload:res.type
              });

          }
      });
  }

  pickImageHandlerFollowUp = () => {
      ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
          if (res.didCancel) {
              console.log("User cancelled!");
          } else if (res.error) {
              console.log("Error", res.error);
          } else {
              this.setState({
                  pickedImageFollowUp: res.uri,
                  uriFollowUp: res.uri,
                  fileTypeFollowUp:res.type
              });

          }
      });
  }


  uploadFoto() {
  if (this.state.uriUpload == '') {
    alert('Masukkan Foto Temuan');
  } else if (this.state.deskripsi ==null) {
    alert("Masukkan Deskripsi")
  } else {
      var url = GlobalConfig.SERVERHOST + "addFireReportImage";
      var formData = new FormData();
      formData.append("id_fire_report", this.state.idFireReport);
      formData.append("description", this.state.deskripsi);
      formData.append("status", 'UPLOAD');
      if (this.state.uriUpload!='') {
        formData.append("image", {
          uri: this.state.uriUpload,
          type: this.state.fileTypeUpload,
          name: 'FireReport_' + this.state.idFireReport + 'UPLOAD' + this.state.deskripsi
      });
      }
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        if (response.status == 200) {
          Alert.alert('Success', 'Uppload Success', [{
            text: 'Okay'
          }])
          this.setState({isHiddenUpload:true})
          this.loadImage()
        } else {
          Alert.alert('Error', 'Upload Failed', [{
            text: 'Okay'
          }])
        }
      });
  }
}

uploadFollowUp() {
if (this.state.uriFollowUp == '') {
  alert('Masukkan Foto Temuan');
} else if (this.state.deskripsiFollowUp ==null) {
  alert("Masukkan Deskripsi")
} else {
    var url = GlobalConfig.SERVERHOST + "addFireReportImage";
    var formData = new FormData();
    formData.append("id_fire_report", this.state.idFireReport);
    formData.append("description", this.state.deskripsiFollowUp);
    formData.append("status", 'FOLLOWUP');
    if (this.state.uriFollowUp!='') {
      formData.append("image", {
        uri: this.state.uriFollowUp,
        type: this.state.fileTypeFollowUp,
        name: 'FireReport_' + this.state.idFireReport + 'FOLLOWUP' + this.state.deskripsiFollowUp
    });
    }
    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
    .then(response => response.json())
    .then(response => {
      if (response.status == 200) {
        Alert.alert('Success', 'Uppload Success', [{
          text: 'Okay'
        }])
        this.setState({isHiddenFollowUp:true})
        this.loadImage()
      } else {
        Alert.alert('Error', 'Upload Failed', [{
          text: 'Okay'
        }])
      }
    });
  }
}

  render() {
    that = this;
    return (
      <Container style={styles.wrapper}>
          <Header style={styles.header}>
            <Left style={{flex:1}}>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("FireReportDetail")}
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Left>
            <Body style={{ flex:3, alignItems:'center' }}>
              <Title style={styles.textbody}>{this.state.fireNumber}</Title>
            </Body>
            <Right style={{flex:1}}>
              <Button
                transparent
                onPress={() => this.setState({visibleFoto:true, isHiddenFollowUp:true, isHiddenUpload:true })}
              >
                <Icon
                  name="ios-camera"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Right>
          </Header>

          <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
          <Content style={{marginTop:5, backgroundColor:colors.gray}}>

          {this.state.isHiddenUpload==false && (
            <View>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                    <View style={{paddingBottom:5}}>
                      <Text style={styles.titleInput}>Upload Foto Kebakaran</Text>
                    </View>
                    <View>
                      <Ripple
                        style={{
                          flex: 2,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                        rippleSize={176}
                        rippleDuration={600}
                        rippleContainerBorderRadius={15}
                        onPress={this.pickImageHandlerUpload}
                        rippleColor={colors.accent}
                      >
                      <View style={styles.placeholder}>
                          <Image source={{uri:this.state.pickedImageUpload}} style={styles.previewImage}/>
                      </View>
                      </Ripple>
                    </View>
                </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                    <View>
                        <Text style={styles.titleInput}>Deskripsi *</Text>
                    </View>
                    <View>
                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                            rowSpan={3}
                            bordered value={this.state.deskripsi}
                            placeholder='Type something here ...'
                            onChangeText={(text) => this.setState({ deskripsi: text })} />
                    </View>
                </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
              <View style={{ flex: 1}}>
              <View style={styles.Contentsave}>
                <Button
                  block
                  style={{
                    width:'100%',
                    height: 45,
                    marginBottom: 20,
                    borderWidth: 1,
                    backgroundColor: "#00b300",
                    borderColor: "#00b300",
                    borderRadius: 4
                  }}
                  onPress={() => this.uploadFoto()}
                >
                  <Text style={{color:colors.white}}>UPLOAD</Text>
                </Button>
              </View>
              </View>
            </CardItem>
            </View>
          )}

          {this.state.isHiddenFollowUp==false && (
            <View>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                    <View style={{paddingBottom:5}}>
                      <Text style={styles.titleInput}>Upload Foto Follow Up</Text>
                    </View>
                    <View>
                      <Ripple
                        style={{
                          flex: 2,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                        rippleSize={176}
                        rippleDuration={600}
                        rippleContainerBorderRadius={15}
                        onPress={this.pickImageHandlerFollowUp}
                        rippleColor={colors.accent}
                      >
                      <View style={styles.placeholder}>
                          <Image source={{uri:this.state.pickedImageFollowUp}} style={styles.previewImage}/>
                      </View>
                      </Ripple>
                    </View>
                </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                    <View>
                        <Text style={styles.titleInput}>Deskripsi *</Text>
                    </View>
                    <View>
                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                            rowSpan={3}
                            bordered value={this.state.deskripsiFollowUp}
                            placeholder='Type something here ...'
                            onChangeText={(text) => this.setState({ deskripsiFollowUp: text })} />
                    </View>
                </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
              <View style={{ flex: 1}}>
              <View style={styles.Contentsave}>
                <Button
                  block
                  style={{
                    width:'100%',
                    height: 45,
                    marginBottom: 20,
                    borderWidth: 1,
                    backgroundColor: "#00b300",
                    borderColor: "#00b300",
                    borderRadius: 4
                  }}
                  onPress={() => this.uploadFollowUp()}
                >
                  <Text style={{color:colors.white}}>UPLOAD</Text>
                </Button>
              </View>
              </View>
            </CardItem>
            </View>
          )}
          <View style={{}}>
            <CardItem style={{ borderRadius: 0, backgroundColor:colors.gray }}>
              <View style={{flex:1}}>
                <View>
                  <Text style={{fontSize:11, fontWeight:'bold', alignItems:'center', paddingBottom:5}}>Foto Kebakaran</Text>
                </View>
                {this.state.listFireReportKebakaran==''? (
                  <Text style={{fontSize:10}}>Foto Belum Ada</Text>
                ):(
                <FlatList
                  data={this.state.listFireReportKebakaran}
                  renderItem={this._renderItemKebakaran}
                  keyExtractor={(item, index) => index.toString()}
                />
              )}
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, backgroundColor:colors.gray }}>
              <View style={{flex:1}}>
                <View>
                  <Text style={{fontSize:11, fontWeight:'bold', alignItems:'center', paddingBottom:5}}>Foto Follow Up</Text>
                </View>
                {this.state.listFireReportFollowUp==''? (
                  <Text style={{fontSize:10}}>Foto Belum Ada</Text>
                ):(
                <FlatList
                  data={this.state.listFireReportFollowUp}
                  renderItem={this._renderItemFollowUp}
                  keyExtractor={(item, index) => index.toString()}
                />
              )}
              </View>
            </CardItem>
          </View>
          </Content>

          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleFoto}
              dialogAnimation={
                new SlideAnimation({
                  slideFrom: "bottom"
                })
              }
              onTouchOutside={() => {
                this.setState({ visibleFoto: false });
              }}
              dialogStyle={{ position: 'absolute', width:"70%" }}
            >
              <DialogContent
                style={{
                  backgroundColor: colors.white
                }}
              >
              <View>
                <View style={{marginBottom:10,marginTop:20,alignItems:'center',backgroundColor:colors.gray}}>
                  <Text style={{color:colors.black}}>Pilih Upload Foto</Text>
                </View>
                <View style={{flexDirection: "column",marginBottom:20 ,marginLeft:10}}>
                    <CustomRadioButton
                      name='Upload Foto Kebakaran'
                      selected={false}
                      onPress={() => this.setState({isHiddenUpload:!this.state.isHiddenUpload, isHiddenFollowUp:true, visibleFoto:false})}/>

                    <CustomRadioButton
                      name='Upload Foto FollowUp'
                      selected={false}
                      onPress={() => this.setState({isHiddenFollowUp:!this.state.isHiddenFollowUp, isHiddenUpload:true, visibleFoto:false})}/>
                </View>
              </View>
              </DialogContent>
            </Dialog>
          </View>
      </Container>
    );
  }
}

export default FireReportImage;

import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback } from "react-native";
import Ripple from "react-native-material-ripple";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    height: 110,
    width: 70,
    marginLeft: 12
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class SubMenuItems extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 100,
            elevation: 2,
          }}
          rippleSize={276}
          rippleDuration={600}
          rippleContainerBorderRadius={100}
          onPress={handleOnPress}
          rippleColor={colors.accent}
        >
          <Image
            source={this.props.imageUri}
            style={{ width: 32, height: 32, resizeMode: "contain" }}
          />
        </Ripple>
        <View style={{ flex: 1, flexDirection: 'column', paddingTop: 10, alignItems: "center" }}>
          <Text>{this.props.name}</Text>
        </View>
      </View>
    );
  }
}

SubMenuItems.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};

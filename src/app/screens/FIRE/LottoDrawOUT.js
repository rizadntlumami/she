import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input,
    Thumbnail
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SIOcertification";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewTools from "../../components/ListViewTools";
import Ripple from "react-native-material-ripple";

class LottoDrawOUT extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            isLoading: true,
            listLotto:[],
            idLotto:'',
            dataLotto:[],
        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {
        AsyncStorage.getItem('idLotto').then((idLotto) => {
            this.setState({
              idLotto: idLotto,
              isLoading: false,
            });
            this.loadLotto();
        })
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
            this.loadLotto();
        });
    }

    loadLotto(){
          const url = GlobalConfig.SERVERHOST + 'getDetailLotto';
          var formData = new FormData();
          formData.append("id_lotto", this.state.idLotto)

          fetch(url, {
              headers: {
                  'Content-Type': 'multipart/form-data'
              },
              method: 'POST',
              body: formData
          })
              .then((response) => response.json())
              .then((responseJson) => {
                  this.setState({
                      dataLotto:responseJson.data,
                      isLoading: false
                  });
              })
              .catch((error) => {
                  console.log(error)
              })
    }


    navigateToScreen(route, dataDrawout, idUpdate) {
        AsyncStorage.setItem('dataDrawout', JSON.stringify(dataDrawout)).then(() => {
            AsyncStorage.setItem('idUpdate', (idUpdate)).then(() => {
                this.props.navigation.navigate(route);
            })
        })
    }

    render() {
      var list;
      if (this.state.isLoading) {
          list = (
              <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                  <ActivityIndicator size="large" color="#330066" animating />
              </View>
          );
      } else {
        list = (
          <ScrollView>
              <View>
                <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, height:50}}>
                    <Ripple
                      style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      rippleSize={176}
                      rippleDuration={600}
                      rippleContainerBorderRadius={15}
                      onPress={() => this.navigateToScreen('LottoInputDrawOUT', this.state.dataLotto, 'k3DrawOUT')}
                      rippleColor={colors.accent}
                    >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                        <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:220 }}>
                            {this.state.dataLotto.k3_drawout_name == 'null' || this.state.dataLotto.k3_drawout_name == null ? (
                            <Text style={{fontSize: 10, fontWeight: "bold"}}>Data Belum Ada Record</Text>):(
                            <View>
                            <Text style={{fontSize: 10, fontWeight: "bold"}}>{this.state.dataLotto.k3_drawout_badge} - {this.state.dataLotto.k3_drawout_name}</Text>
                            {this.state.dataLotto.k3_drawout_ket=='V' ?(
                              <Text style={{fontSize: 10}}>Datang & Mengamankan</Text>
                            ):this.state.dataLotto.k3_drawout_ket=='Y' ?(
                              <Text style={{fontSize: 10}}>Datang & Tidak Mengamankan</Text>):(
                                <Text style={{fontSize: 10}}>Tidak Datang & Tidak Mengamankan</Text>
                              )
                            }
                            </View>)}
                        </View>
                        <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:80 }}>
                            <Text style={{fontSize: 10, fontWeight: "bold", textAlign:'right', color:colors.green01}}>K3</Text>
                        </View>
                    </View>
                    </Ripple>
                </Card>

                <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, height:50}}>
                  <Ripple
                    style={{
                      flex: 2,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() => this.navigateToScreen('LottoInputDrawOUT', this.state.dataLotto, 'pmlListrikDrawOUT')}
                    rippleColor={colors.accent}
                  >
                  <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:220 }}>
                          {this.state.dataLotto.pmll_drawout_name == 'null' || this.state.dataLotto.pmll_drawout_name == null ? (
                          <Text style={{fontSize: 10, fontWeight: "bold"}}>Data Belum Ada Record</Text>):(
                          <View>
                          <Text style={{fontSize: 10, fontWeight: "bold"}}>{this.state.dataLotto.pmll_drawout_badge} - {this.state.dataLotto.pmll_drawout_name}</Text>
                          {this.state.dataLotto.pmll_drawout_ket=='V' ?(
                            <Text style={{fontSize: 10}}>Datang & Mengamankan</Text>
                          ):this.state.dataLotto.pmll_drawout_ket=='Y' ?(
                            <Text style={{fontSize: 10}}>Datang & Tidak Mengamankan</Text>):(
                              <Text style={{fontSize: 10}}>Tidak Datang & Tidak Mengamankan</Text>
                            )
                          }
                          </View>)}
                      </View>
                      <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:80 }}>
                          <Text style={{fontSize: 10, fontWeight: "bold", textAlign:'right', color:colors.green01}}>PML LISTRIK</Text>
                      </View>
                  </View>
                  </Ripple>
                  </Card>

                  <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, height:50}}>
                  <Ripple
                    style={{
                      flex: 2,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() => this.navigateToScreen('LottoInputDrawOUT', this.state.dataLotto, 'operatorDrawOUT')}
                    rippleColor={colors.accent}
                  >
                  <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:220 }}>
                          {this.state.dataLotto.operator_drawout_name == 'null' || this.state.dataLotto.operator_drawout_name == null ? (
                          <Text style={{fontSize: 10, fontWeight: "bold"}}>Data Belum Ada Record</Text>):(
                          <View>
                          <Text style={{fontSize: 10, fontWeight: "bold"}}>{this.state.dataLotto.operator_drawout_badge} - {this.state.dataLotto.operator_drawout_name}</Text>
                          {this.state.dataLotto.operator_drawout_ket=='V' ?(
                            <Text style={{fontSize: 10}}>Datang & Mengamankan</Text>
                          ):this.state.dataLotto.operator_drawout_ket=='Y' ?(
                            <Text style={{fontSize: 10}}>Datang & Tidak Mengamankan</Text>):(
                              <Text style={{fontSize: 10}}>Tidak Datang & Tidak Mengamankan</Text>
                            )
                          }
                          </View>)}
                      </View>
                      <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:80 }}>
                          <Text style={{fontSize: 10, fontWeight: "bold", textAlign:'right', color:colors.green01}}>OPERATOR</Text>
                      </View>
                  </View>
                  </Ripple>
                  </Card>

                  <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, height:50}}>
                  <Ripple
                    style={{
                      flex: 2,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() => this.navigateToScreen('LottoInputDrawOUT', this.state.dataLotto, 'pmlTerkaitDrawOUT')}
                    rippleColor={colors.accent}
                  >
                  <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:220 }}>
                          {this.state.dataLotto.pmlt_drawout_name == 'null' || this.state.dataLotto.pmlt_drawout_name == null ? (
                          <Text style={{fontSize: 10, fontWeight: "bold"}}>Data Belum Ada Record</Text>):(
                          <View>
                          <Text style={{fontSize: 10, fontWeight: "bold"}}>{this.state.dataLotto.pmlt_drawout_badge} - {this.state.dataLotto.pmlt_drawout_name}</Text>
                          {this.state.dataLotto.pmlt_drawout_ket=='V' ?(
                            <Text style={{fontSize: 10}}>Datang & Mengamankan</Text>
                          ):this.state.dataLotto.pmlt_drawout_ket=='Y' ?(
                            <Text style={{fontSize: 10}}>Datang & Tidak Mengamankan</Text>):(
                              <Text style={{fontSize: 10}}>Tidak Datang & Tidak Mengamankan</Text>
                            )
                          }
                          </View>)}
                      </View>
                      <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:80 }}>
                          <Text style={{fontSize: 10, fontWeight: "bold", textAlign:'right', color:colors.green01}}>PML TERKAIT</Text>
                      </View>
                  </View>
                  </Ripple>
                  </Card>
                </View>
            </ScrollView>
            );
          }
          return (
            <Container style={styles.wrapper}>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                <Header style={styles.header}>
                    <Left style={{flex:1}}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("LottoDetail")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body style={{flex:3,alignItems:'center'}}>
                        <Title style={styles.textbody}>DRAW OUT</Title>
                    </Body>
                    <Right style={{flex:1}}></Right>
                </Header>

                <Footer style={styles.tabHeight}>
                    <FooterTab style={styles.tabfooter}>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("LottoDrawIN")
                        }
                        >
                        <Text style={{color:'white', fontWeight:'bold'}}>Draw IN</Text>
                    </Button>
                    <Button active style={styles.tabfooter}>
                        <View style={{height:'40%'}}></View>
                        <View style={{height:'50%'}}>
                            <Text style={{color:'white', fontWeight:'bold'}}>Draw OUT</Text>
                        </View>
                        <View style={{height:'20%'}}></View>
                        <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.white}}></View>
                    </Button>
                    </FooterTab>
                </Footer>

                <Content style={{ marginTop: 10 }}>
                  {list}
                </Content>
            </Container>
        );
    }
}

export default LottoDrawOUT;

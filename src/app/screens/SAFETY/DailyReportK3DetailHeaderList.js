import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    RefreshControl,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewHeader from "../../components/ListViewHeader";
import ListViewDetail from "../../components/ListViewDetail";

class ListItem extends React.PureComponent {
  render(){
    return(
      <View style={{paddingBottom:10}}>
      {this.props.data.UPLOADED_PATH.includes('TEMUAN')? (
        <Text style={{fontSize:10,fontWeight:"bold"}}>Foto Temuan :</Text>
      ):(
        <Text style={{fontSize:10,fontWeight:"bold"}}>Foto Hasil Tindak Lanjut :</Text>
      )}
      <View style={styles.placeholder}>
        <Image style={styles.previewImage} source={{uri : GlobalConfig.SERVERHOST + 'api' + this.props.data.UPLOADED_PATH+ '?' + new Date()}}/>
      </View>
      </View>
    )
  }
}

class DailyReportK3DetailHeaderList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isLoading: true,
      listHeader:[],
      listFileUpload:[],
      visibleDialogSubmit:false
    };
  }

  static navigationOptions = {
      header: null
  };

  _renderItem = ({ item }) => (
      <ListItem data={item}></ListItem>
  )

  onRefresh() {
    console.log('refreshing')
    this.setState({ isloading: true }, function() {

    });
  }


  navigateToScreen(route, listHeader) {
      AsyncStorage.setItem('list', JSON.stringify(listHeader)).then(() => {
          this.props.navigation.navigate(route);
      })
  }

  componentDidMount() {
      AsyncStorage.getItem('list').then((listReportK3) => {
          this.setState({ listHeader: JSON.parse(listReportK3)},function(){console.log(this.state.listHeader)});
          this.setState({ listFileUpload: this.state.listHeader.LIST_FILE_UPLOAD});
          this.setState({ isLoading: false });
      })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        //this.loadData(id_Unsafe);
      });
  }

  konfirmasideleteList() {
    Alert.alert(
      'Apakah Anda Yakin Ingin Menghapus', this.state.listHeader.ID_UNSAFE_RPT_DETAIL,
      [
        {text: 'Yes', onPress:()=> this.deleteList()},
        {text: 'Cancel'},
      ],
      { cancelable:false}
    )
  }

  deleteList() {
    this.setState({
      visibleDialogSubmit:true
    })
    AsyncStorage.getItem('token').then((value) => {
      // alert(JSON.stringify(value));
      const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe_n/unsafe_detail_list/delete';
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID_UNSAFE_RPT_DETAIL", this.state.listHeader.ID_UNSAFE_RPT_DETAIL);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.Status == 200) {
            this.setState({
              visibleDialogSubmit:false
            })
            Alert.alert('Success', 'Delete Success', [{
              text: 'Oke',
            }])
            this.props.navigation.navigate("DailyReportK3Detail")
          } else {
            this.setState({
              visibleDialogSubmit:false
            })
            Alert.alert('Error', 'Delete Failed', [{
              text: 'Oke'
            }])
          }
        })
        .catch((error)=>{
          console.log(error)
          this.setState({
            visibleDialogSubmit:false
          })
          Alert.alert('Error', 'Delete Failed', [{
            text: 'Oke'
          }])
        })
    })
  }

  render() {
    var status=""
    if (this.state.listHeader.STATUS!=undefined){
      status=this.state.listHeader.STATUS
    }
    return (
      <Container style={styles.wrapper}>
          <Header style={styles.header}>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("DailyReportK3DetailList")}
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Left>
            <Body>
              <Title style={styles.textbody}>Unsafe Detail</Title>
            </Body>
            <Right style={{width:'20%'}}>
              <Button
                transparent
                onPress={() => this.navigateToScreen('UpdateDetailHeaderReportK3', this.state.listHeader)}
              >
                <Icon
                  name="ios-create"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
              <Button
                transparent
                onPress={() => this.konfirmasideleteList()}
              >
                <Icon
                  name="ios-trash"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Right>
          </Header>
          <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
          <Content style={{marginTop:5}}>

          <View style={{}}>
            <CardItem style={{ borderRadius: 0, marginTop:5, }}>
                <FlatList
                data={this.state.listFileUpload}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
                refreshControl={
                    <RefreshControl
                      refreshing={this.state.isloading}
                      onRefresh={this.onRefresh.bind(this)}
                    />}
              />
              {/*{this.state.listFileUpload.map((listFoto) => (
                <View style={styles.placeholder}>
                  <Image style={styles.previewImage} source={{uri : GlobalConfig.SERVERHOST + 'api' + listFoto.UPLOADED_PATH}}/>
                </View>
              ))}*/}
            </CardItem>
            <CardItem style={{ borderRadius: 0, }}>
              <View style={{ flex: 1, flexDirection: "row"}}>
                <View style={{width: 260}}>
                  <Text style={{fontSize:10}}>Kode</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.listHeader.KODE_EN}-00{this.state.listHeader.ID_CATEGORY}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Type</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.listHeader.UNSAFE_TYPE}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Aktifitas</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.listHeader.ACTIVITY_DESCRIPTION}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Report Date</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.listHeader.DATE_REPORT}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Jam</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.listHeader.O_CLOCK_INCIDENT}</Text>
                  {((status).toUpperCase())=="OPEN"&&(
                    <View>
                      <Text style={{fontSize:10, paddingTop:10}}>Status</Text>
                      <Text style={{fontSize:10, fontWeight:"bold", color:colors.green01}}>{this.state.listHeader.STATUS}</Text>
                    </View>
                  )}
                  {((status).toUpperCase())=="CLOSE"&&(
                    <View>
                      <Text style={{fontSize:10, paddingTop:10}}>Status</Text>
                      <Text style={{fontSize:10, fontWeight:"bold", color:colors.red}}>{this.state.listHeader.STATUS}</Text>
                    </View>
                  )}
                  {((((status).toUpperCase())!="CLOSE")&&(((status).toUpperCase())!="OPEN"))&&(
                    <View>
                      <Text style={{fontSize:10, paddingTop:10}}>Status</Text>
                      <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.listHeader.STATUS}</Text>
                    </View>
                  )}
                  <Text style={{fontSize:10, paddingTop:10}}>Potensi</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.listHeader.POTENTIAL_HAZARD}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Unit</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.listHeader.UNIT_NAME}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Inspector</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.listHeader.CREATE_BY}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Follow Up</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.listHeader.FOLLOW_UP}</Text>
                </View>
              </View>
            </CardItem>
            
          </View>
          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
              dialogTitle={<DialogTitle title="Deleting Detail Header Report K3 .." />}
            >
              <DialogContent>
                {<ActivityIndicator size="large" color="#330066" animating />}
              </DialogContent>
            </Dialog>
          </View>
          </Content>
      </Container>
    );
  }
}

export default DailyReportK3DetailHeaderList;

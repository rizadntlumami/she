import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Textarea,
  Icon,
  Picker,
  Form,
  Input
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from "react-native-datepicker";
import DateTimePicker from "react-native-datepicker";
import Ripple from "react-native-material-ripple";

export default class LottoCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      dataHeader: [],
      isLoading: true,
      tools: "",
      listTools: [],
      visibleDialogSubmit: false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.setState({
          visibleDialogSubmit: false
        });
      }
    );
    this.loadData();
  }

  loadData() {}

  createLotto() {
    if (this.state.equipmentNumber == null) {
      alert("Masukkan Equipment Number");
    } else if (this.state.lokasi == null) {
      alert("Masukkan Lokasi");
    } else if (this.state.noER == null) {
      alert("Masukkan Nomor ER");
    } else if (this.state.aktifitas == null) {
      alert("Masukkan Aktifitas");
    } else if (this.state.date == null) {
      alert("Masukkan Date");
    } else if (this.state.time == null) {
      alert("Masukkan Time");
    } else if (this.state.safetyKey == null) {
      alert("Masukkan Safety Key");
    } else if (this.state.listTools == null) {
      alert("Masukkan Safety Tools");
    } else {
      this.setState({
        visibleDialogSubmit: true
      });
        var url = GlobalConfig.SERVERHOST + "addLotto";
        var formData = new FormData();
        formData.append("equipment_number", this.state.equipmentNumber);
        formData.append("no_er", this.state.noER);
        formData.append("location", this.state.lokasi);
        formData.append("activity_description", this.state.aktifitas);
        formData.append("date", this.state.date);
        formData.append("time", this.state.time);
        if (this.state.note != "") {
          formData.append("note", this.state.note);
        } else {
          formData.append("note", "");
        }
        formData.append("safety_key", this.state.safetyKey);
        formData.append("safety_tools", JSON.stringify(this.state.listTools));
        formData.append("status", 'OPEN');

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
          .then(response => response.json())
          .then(response => {
            if (response.status == 200) {
              this.setState({
                visibleDialogSubmit: false
              });
              Alert.alert("Success", "Create Lotto Success", [
                {
                  text: "Oke"
                }
              ]);
              this.props.navigation.navigate("Lotto");
            } else {
              this.setState({
                visibleDialogSubmit: false
              });
              Alert.alert("Error", "Create Lotto Failed", [
                {
                  text: "Oke"
                }
              ]);
            }
          })
          .catch((error)=>{
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Error", "Create Lotto Failed", [
              {
                text: "Oke"
              }
            ]);
            console.log(error)
          })
    }
  }

  tambahTools() {
    let tools = this.state.tools;
    this.setState({
      tools: ""
    });
    if (tools == "") {
    } else {
      this.setState({
        listTools: [...this.state.listTools, tools]
      });
    }
  }

  deleteTools(index) {
    let arr = this.state.listTools;
    arr.splice(index, 1);
    this.setState({
      listTools: arr
    });
  }

  render() {
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("Lotto")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Create Lotto</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ flex: 1 }}>
          <Content style={{ marginTop: 0 }}>
            <View style={{ backgroundColor: "#FEFEFE" }}>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Equipment Number (*)</Text>
                  </View>
                  <View>
                    <Textarea
                      style={{
                        borderRadius: 5,
                        marginLeft: 5,
                        marginRight: 5,
                        fontSize: 10
                      }}
                      rowSpan={2}
                      bordered
                      value={this.state.equipmentNumber}
                      placeholder="Input Equipment ..."
                      onChangeText={text =>
                        this.setState({ equipmentNumber: text })
                      }
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Location (*)</Text>
                  </View>
                  <View>
                    <Textarea
                      style={{
                        borderRadius: 5,
                        marginLeft: 5,
                        marginRight: 5,
                        fontSize: 10
                      }}
                      rowSpan={2}
                      bordered
                      value={this.state.lokasi}
                      placeholder="Input Location ..."
                      onChangeText={text => this.setState({ lokasi: text })}
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>No Er (*)</Text>
                  </View>
                  <View>
                    <Textarea
                      style={{
                        borderRadius: 5,
                        marginLeft: 5,
                        marginRight: 5,
                        fontSize: 10
                      }}
                      rowSpan={2}
                      bordered
                      value={this.state.noER}
                      placeholder="Input Nomor ER ..."
                      onChangeText={text => this.setState({ noER: text })}
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>
                      Activities Description (*)
                    </Text>
                  </View>
                  <View>
                    <Textarea
                      style={{
                        borderRadius: 5,
                        marginLeft: 5,
                        marginRight: 5,
                        fontSize: 10
                      }}
                      rowSpan={2}
                      bordered
                      value={this.state.aktifitas}
                      placeholder="Input Aktifitas ..."
                      onChangeText={text => this.setState({ aktifitas: text })}
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Date (*)</Text>
                    <DatePicker
                      style={{ width: "100%", fontSize: 10, borderRadius: 20 }}
                      date={this.state.date}
                      mode="date"
                      placeholder="Choose Date ..."
                      format="YYYY-MM-DD"
                      minDate="2018-01-01"
                      maxDate="5000-12-31"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateInput: {
                          marginLeft: 5,
                          marginRight: 5,
                          height: 35,
                          borderRadius: 5,
                          fontSize: 10,
                          borderWidth: 1,
                          borderColor: "#E6E6E6"
                        },
                        dateIcon: {
                          position: "absolute",
                          left: 0,
                          top: 5
                        }
                      }}
                      onDateChange={date => {
                        this.setState({ date: date });
                      }}
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Jam (*)</Text>
                    <DateTimePicker
                      style={{ width: "100%", fontSize: 10, borderRadius: 20 }}
                      date={this.state.time}
                      mode="time"
                      placeholder="Choose Time ..."
                      format="hh:mm"
                      minTime="00:00"
                      maxTime="23:59"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateInput: {
                          marginLeft: 5,
                          marginRight: 5,
                          height: 35,
                          borderRadius: 5,
                          fontSize: 10,
                          borderWidth: 1,
                          borderColor: "#E6E6E6"
                        },
                        dateIcon: {
                          position: "absolute",
                          left: 0,
                          top: 5
                        }
                      }}
                      onDateChange={time => {
                        this.setState({ time: time });
                      }}
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Note</Text>
                  </View>
                  <View>
                    <Textarea
                      style={{
                        borderRadius: 5,
                        marginLeft: 5,
                        marginRight: 5,
                        fontSize: 10
                      }}
                      rowSpan={2}
                      bordered
                      value={this.state.note}
                      placeholder="Input Note ..."
                      onChangeText={text => this.setState({ note: text })}
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Safety Key (*)</Text>
                  </View>
                  <View>
                    <Textarea
                      style={{
                        borderRadius: 5,
                        marginLeft: 5,
                        marginRight: 5,
                        fontSize: 10
                      }}
                      rowSpan={2}
                      bordered
                      value={this.state.safetyKey}
                      placeholder="Input Safety Key ..."
                      onChangeText={text => this.setState({ safetyKey: text })}
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{borderRadius: 0, marginTop: 0, backgroundColor: colors.gray}}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Safety Tools (*)</Text>
                  </View>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ width: 280 }}>
                      <Textarea
                        style={{ borderRadius: 5, marginLeft: 5, fontSize: 10 }}
                        rowSpan={1.5}
                        bordered
                        value={this.state.tools}
                        placeholder="Safety Tools ..."
                        onChangeText={text => this.setState({ tools: text })}
                      />
                    </View>
                    <Button
                      onPress={() => this.tambahTools()}
                      style={styles.btnTambah}
                    >
                      <Icon name="ios-add" style={styles.iconPluss} />
                    </Button>
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  {this.state.listTools.map((listTools, index) => (
                    <View key={index} style={{ backgroundColor: "#FEFEFE" }}>
                      <View
                        style={{
                          marginTop: 5,
                          marginBottom: 5,
                          marginLeft: 10,
                          marginRight: 10,
                          height: 30,
                          backgroundColor: colors.gray,
                          borderRadius: 5
                        }}
                      >
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <View style={{ width: 280 }}>
                            <Text
                              style={{
                                paddingLeft: 10,
                                fontSize: 11,
                                paddingTop: 5
                              }}
                            >
                              {listTools}
                            </Text>
                          </View>
                          <View>
                            <Ripple
                              style={{
                                flex: 2,
                                justifyContent: "center",
                                alignItems: "center"
                              }}
                              rippleSize={176}
                              rippleDuration={600}
                              rippleContainerBorderRadius={15}
                              onPress={() => this.deleteTools(index)}
                              rippleColor={colors.accent}
                            >
                              <Icon name="ios-close" style={styles.iconClose} />
                            </Ripple>
                          </View>
                        </View>
                      </View>
                    </View>
                  ))}
                </View>
              </CardItem>
              <View style={styles.Contentsave}>
                <Button
                  block
                  style={{
                    height: 45,
                    marginLeft: 20,
                    marginRight: 20,
                    marginBottom: 20,
                    borderWidth: 1,
                    backgroundColor: "#00b300",
                    borderColor: "#00b300",
                    borderRadius: 4
                  }}
                  onPress={() => this.createLotto()}
                >
                  <Text style={{ color: colors.white }}>SUBMIT</Text>
                </Button>
              </View>
              <View style={{ width: 270, position: "absolute" }}>
                <Dialog
                  visible={this.state.visibleDialogSubmit}
                  dialogTitle={<DialogTitle title="Creating LOTTO.." />}
                >
                  <DialogContent>
                    {
                      <ActivityIndicator
                        size="large"
                        color="#330066"
                        animating
                      />
                    }
                  </DialogContent>
                </Dialog>
              </View>
            </View>
          </Content>
        </View>
      </Container>
    );
  }
}

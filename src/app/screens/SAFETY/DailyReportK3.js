import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl,
  Dimensions
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListView from "../../components/ListView";
import Ripple from "react-native-material-ripple";

class ListItem extends React.PureComponent {
  navigateToScreen(route, listHeaderDailyReport) {
    AsyncStorage.setItem('list', JSON.stringify(listHeaderDailyReport)).then(() => {
        this.props.navigation.navigate(route);
    })
  }

  render() {
    return (
      <View>
        <Ripple

          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.navigateToScreen(
              "DailyReportK3Detail",
              this.props.data
            )
          }
          rippleColor={colors.accent}
        >
        <View style={{ backgroundColor: "#FEFEFE" }}>
          <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
            <View style={{ marginTop: 10, marginLeft: 10, marginRight: 10,marginBottom: 10  }}>
              <ListView
                date={this.props.data.DATE_REPORT}
                area={this.props.data.AREA_TXT}
                subArea={this.props.data.SUB_AREA_TXT}
                inspector={this.props.data.INSPECTOR_NAME}
                plant={this.props.data.PLANT_NAME}
                shift={this.props.data.SHIFT}
              />
            </View>
          </Card>
        </View>
        </Ripple>
      </View>
    );
  }
}

class DailyReportK3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isLoading: true,
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => (
    <ListItem data={item} navigation={this.props.navigation}></ListItem>
  )

  onRefresh() {
    console.log('refreshing')
    this.setState({ isLoading: true }, function() {
      this.setState({ searchData: '' })
      this.loadData();
    });
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      this.setState({ isLoading: true });
      this.loadData();
      this.setState({ searchData: '' })
    });
    //this.setState({ isLoading: false });
  }



  loadData() {
    this.setState({ isLoading: true });
    AsyncStorage.getItem('token').then((value) => {
      const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe_n/view_report_list';
      var formData = new FormData();
      formData.append("token", value)

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'POST',
        body: formData
      })
        .then((response) => response.json())
        .then((responseJson) => {
          //alert(JSON.stringify(responseJson));
          this.setState({
            dataSource: responseJson.data.filter(x => x.DELETE_FLAG == 0),
            isLoading: false
          });
        })
        .catch((error) => {
          this.setState({ isLoading: false });
          console.log(error)
        })
    })
  }

  searchData() {
    this.setState({ isLoading: true });
    AsyncStorage.getItem('token').then((value) => {
      const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe_n/view_report_list';
      var formData = new FormData();
      formData.append("token", value)
      formData.append("search", this.state.searchData)
      formData.append("length", 100)
      formData.append("start", 0)

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'POST',
        body: formData
      })
        .then((response) => response.json())
        .then((responseJson) => {
          //alert(JSON.stringify(responseJson));
          this.setState({
            dataSource: responseJson.data.filter(x => x.DELETE_FLAG == 0),
            isLoading: false
          });
        })
        .catch((error) => {
          console.log(error)
        })
    })
  }

  render() {
    var list;
    if (this.state.isLoading) {
        list = (
          <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <ActivityIndicator size="large" color="#330066" animating />
          </View>
        );
    } else {
        if (this.state.dataSource.length==0) {
            list = (
                <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                    <Thumbnail square large source={require("../../../assets/images/empty.png")} />
                    <Text>No Data!</Text>
                </View>
            )
        } else {
            list =
            <FlatList
            data={this.state.dataSource}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isLoading}
                onRefresh={this.onRefresh.bind(this)}
              />}
          />
}
}
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("SafetyMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:4,alignItems:'center'}}>
            <Title style={styles.textbody}>Daily Report Safety Officer</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={styles.search}>
          <View style={{ flex: 1, flexDirection: 'row' }}>
            <View style={styles.viewLeftHeader}>
              <Item style={styles.searchItem}>
                <Input
                  style={{ fontSize: 15 }}
                  placeholder="Type something here"
                  value={this.state.searchData}
                  onChangeText={(text) => this.setState({ searchData: text })} />
                <Icon name="ios-search" style={{ fontSize: 30, paddingLeft: 0 }} onPress={() => this.searchData()} />
              </Item>
            </View>
          </View>
        </View>
        <View style={{ marginTop: 60 ,flex: 1, flexDirection: "column" }}>
          {list}
        </View>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: colors.green01,  marginBottom: ((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?80:50  }}
          position="bottomRight"

          onPress={() => this.props.navigation.navigate('CreateHeaderReportK3')}>
          <Icon name="ios-add" style={{ fontSize: 50, fontWeight: "bold",paddingTop: Platform.OS === 'ios' ? 25:null, }} />
        </Fab>
        <CustomFooter navigation={this.props.navigation} menu='Safety' />
      </Container>

    );
  }
}

export default DailyReportK3;

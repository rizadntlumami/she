import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input
} from "native-base";

import ImagePicker from "react-native-image-picker";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from 'react-native-datepicker';

export default class InspeksiTemuanUnsafe extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            dataHeader: [],
            isLoading: true,
            selectedShift: '1',
            selectedUnsafeType: 'UA',
            selectedPriority: '1',
            pickedImage: '',
            uri: '',
            fileName: ''

        };
    }

    static navigationOptions = {
        header: null
    };

    reset = () => {
        this.setState({
            pickedImage: null
        });
    }

    pickImageHandler = () => {
        ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
            if (res.didCancel) {
                console.log("User cancelled!");
            } else if (res.error) {
                console.log("Error", res.error);
            } else {
                this.setState({
                    pickedImage: { uri: res.uri },
                    uri: res.uri,
                    fileName: res.fileName
                });

            }
        });
    }

    resetHandler = () => {
        this.reset();
    }

    // _renderItem = ({ item }) => (
    //     <ListItem data={item}></ListItem>
    // )

    // componentDidMount() {
    //     this.loadData();
    // }

    // loadData() {

    // }

    createUnsafe() {
        // if (this.state.TEMUAN == null) {
        //     alert('Masukkan Deskripsi Temuan');
        // }
        // else if (this.state.LOKASI_TEXT == null) {
        //     alert('Masukkan Lokasi Temuan');
        // }
        // else if (this.state.PENYEBAB == null) {
        //     alert('Masukkan Penyebab Temuan');
        // }
        // else if (this.state.REKOMENDASI == null) {
        //     alert('Masukkan Rekomendasi Tindak Lanjut pada Temuan');
        // }
        // else if (this.state.POTENSI_BAHAYA == null) {
        //     alert('Masukkan Potensi Bahaya dari Temuan');
        // }
        // else {
            // alert(JSON.stringify(this.state.id));
            AsyncStorage.getItem("token").then(value => {
                const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe/create/detail';
                var formData = new FormData();
                formData.append("token", value);
                formData.append("SHIFT", this.state.selectedShift);
                formData.append("JENIS_TEMUAN", this.state.selectedUnsafeType);
                formData.append("PRIORITY", this.state.selectedPriority);
                formData.append("TEMUAN", this.state.TEMUAN);
                formData.append("LOKASI_TEXT", this.state.LOKASI_TEXT);
                formData.append("PENYEBAB", this.state.PENYEBAB);
                formData.append("REKOMENDASI", this.state.REKOMENDASI);
                formData.append("POTENSI_BAHAYA", this.state.POTENSI_BAHAYA);
                formData.append("FOTO_BEF", this.state.pickedImage);

                // alert(JSON.stringify(this.state.pickedImage));

                fetch(url, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },
                    method: 'POST',
                    body: formData
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        alert(responseJson);
                        // if (responseJson.status == 200) {
                        //     Alert.alert('Success', 'Create Temuan Unsafe Success', [{
                        //         text: 'Okay'
                        //     }])
                        //     this.props.navigation.navigate('DailyReport')
                        // } else {
                        //     Alert.alert('Error', 'Create Temuan Unsafe Failed', [{
                        //         text: 'Okay'
                        //     }])
                        // }
                    })
            })
        // }
    }

    render() {
        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("SafetyMenu")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={styles.textbody}>Inspeksi Temuan Unsafe</Title>
                    </Body>
                    {/* <Right/> */}
                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                <View style={{ flex: 1 }}>
                    <Content style={{ marginTop: 0 }}>
                        <View style={{ backgroundColor: '#FEFEFE' }}>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Shift</Text>
                                        <Form underlineColorAndroid='transparent'>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                                style={{ width: 300 }}
                                                placeholder="Quantity"
                                                placeholderStyle={{ color: "#bfc6ea" }}
                                                placeholderIconColor="#007aff"
                                                selectedValue={this.state.selectedShift}
                                                onValueChange={(itemValue) => this.setState({ selectedShift: itemValue })}>
                                                <Picker.Item label="Shift 1" value="1" />
                                                <Picker.Item label="Shift 2" value="2" />
                                                <Picker.Item label="Shift 3" value="3" />
                                            </Picker>
                                        </Form>
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Jenis Temuan</Text>
                                        <Form underlineColorAndroid='transparent'>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                                style={{ width: 300 }}
                                                placeholder="Quantity"
                                                placeholderStyle={{ color: "#bfc6ea" }}
                                                placeholderIconColor="#007aff"
                                                selectedValue={this.state.selectedUnsafeType}
                                                onValueChange={(itemValue) => this.setState({ selectedUnsafeType: itemValue })}>
                                                <Picker.Item label="Unsafe Action" value="UA" />
                                                <Picker.Item label="Unsafe Condition" value="UC" />
                                            </Picker>
                                        </Form>
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Priority</Text>
                                        <Form underlineColorAndroid='transparent'>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                                style={{ width: 300 }}
                                                placeholder="Quantity"
                                                placeholderStyle={{ color: "#bfc6ea" }}
                                                placeholderIconColor="#007aff"
                                                selectedValue={this.state.selectedPriority}
                                                onValueChange={(itemValue) => this.setState({ selectedPriority: itemValue })}>
                                                <Picker.Item label="HIGH" value="1" />
                                                <Picker.Item label="MEDIUM" value="2" />
                                                <Picker.Item label="LOW" value="3" />
                                            </Picker>
                                        </Form>
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Temuan</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.TEMUAN} placeholder='Type Something ...' onChangeText={(text) => this.setState({ TEMUAN: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Location</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.LOKASI_TEXT} placeholder='Type Something ...' onChangeText={(text) => this.setState({ LOKASI_TEXT: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Penyebab</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.PENYEBAB} placeholder='Type Something ...' onChangeText={(text) => this.setState({ PENYEBAB: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Rekomendasi</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.REKOMENDASI} placeholder='Type Something ...' onChangeText={(text) => this.setState({ REKOMENDASI: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Potensi Bahaya</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.POTENSI_BAHAYA} placeholder='Type Something ...' onChangeText={(text) => this.setState({ POTENSI_BAHAYA: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <View style={{ marginTop: 0, padding: 20 }}>
                                <Text style={styles.viewMerkFoto}>Upload Foto (jpg, png) max : 500 kb</Text>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View style={styles.viewLeftBtnUpload}>
                                        <Button block style={{ borderRadius: 5, backgroundColor: colors.blue01, marginTop: 5, marginBottom: 10, width: 80 }}>
                                            <Text style={{ fontWeight: 'bold', color: colors.white, marginLeft: 5, marginRight: 5 }} onPress={this.pickImageHandler}>Upload</Text>
                                        </Button>
                                    </View>
                                    <View style={styles.viewLeftBtnUpload}>
                                        <Button block style={{ borderRadius: 5, backgroundColor: colors.red, marginTop: 5, marginBottom: 10, width: 80 }}>
                                            <Text style={{ fontWeight: 'bold', color: colors.white, marginLeft: 5, marginRight: 5 }} onPress={this.resetHandler}>Reset</Text>
                                        </Button>
                                    </View>
                                </View>
                                <View style={styles.placeholder}>
                                    <Image source={this.state.pickedImage} style={styles.previewImage} />
                                </View>
                            </View>
                        </View>
                    </Content>
                    <View style={styles.Contentsave}>
                        <Button
                            block
                            style={{
                                height: 45,
                                marginLeft: 20,
                                marginRight: 20,
                                marginBottom: 20,
                                borderWidth: 1,
                                backgroundColor: "#00b300",
                                borderColor: "#00b300",
                                borderRadius: 4
                            }}
                            onPress={() => this.createUnsafe()}
                        >
                            <Text style={styles.buttonText}>Submit</Text>
                        </Button>
                    </View>
                </View>
                <CustomFooter navigation={this.props.navigation} menu='Safety' />
            </Container>

        );
    }
}

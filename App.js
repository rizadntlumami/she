import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  FlatList,
  AsyncStorage,
  ActivityIndicator
} from "react-native";
import {
  createStackNavigator,
  createDrawerNavigator,
  createAppContainer,
  NavigationActions,
  NavigationAction
} from "react-navigation";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
  Icon
} from "native-base";
// import Icon from "react-native-vector-icons/FontAwesome";
import styles from "./src/app/screens/styles/OrderAPD";
import styles2 from "./src/app/screens/styles/ApdMenu";
import colors from "./src/styles/colors";
import { TouchableOpacity } from "react-native";

import LoggedOut from "./src/app/screens/LoggedOut";
import Login from "./src/app/screens/Login";
import HomeMenu from "./src/app/screens/HomeMenu";
import DetailNews from "./src/app/screens/DetailNews";
import ProfilUser from "./src/app/screens/ProfilUser";
import ApdMenu from "./src/app/screens/APD/ApdMenu";
import LaporanAPD from "./src/app/screens/APD/LaporanAPD";
import ApprovalPersonalAPD from "./src/app/screens/APD/ApprovalPersonalAPD";
import ActionsApprovalPersonalAPD from "./src/app/screens/APD/ActionsApprovalPersonalAPD";
import ApprovalUnitKerjaAPD from "./src/app/screens/APD/ApprovalUnitKerjaAPD";
import ActionsApprovalUnitKerjaAPD from "./src/app/screens/APD/ActionsApprovalUnitKerjaAPD";
import ApprovalPeminjamanAPD from "./src/app/screens/APD/ApprovalPeminjamanAPD";
import ActionsApprovalPeminjamanAPD from "./src/app/screens/APD/ActionsApprovalPeminjamanAPD";
import IndividualReport from "./src/app/screens/APD/IndividualReport";
import IndividualReportPersonal from "./src/app/screens/APD/IndividualReportPersonal";
import IndividualReportUnitKerja from "./src/app/screens/APD/IndividualReportUnitKerja";
import IndividualReportPeminjaman from "./src/app/screens/APD/IndividualReportPeminjaman";
import StockAPD from "./src/app/screens/APD/StockAPD";
import OrderPersonalAPD from "./src/app/screens/APD/OrderPersonalAPD";
import OrderUnitKerjaAPD from "./src/app/screens/APD/OrderUnitKerjaAPD";
import OrderPeminjamanAPD from "./src/app/screens/APD/OrderPeminjamanAPD";
import OrderHilang from "./src/app/screens/APD/OrderHilang";
import OrderRusak from "./src/app/screens/APD/OrderRusak";
import MyOrderPersonalAPD from "./src/app/screens/APD/MyOrderPersonalAPD";
import MyOrderPeminjamanAPD from "./src/app/screens/APD/MyOrderPeminjamanAPD";
import MyOrderUnitKerjaAPD from "./src/app/screens/APD/MyOrderUnitKerjaAPD";
import DetailMyOrderPersonalAPD from "./src/app/screens/APD/DetailMyOrderPersonalAPD";
import DetailMyOrderUnitKerjaAPD from "./src/app/screens/APD/DetailMyOrderUnitKerjaAPD";
import DetailMyOrderPeminjamanAPD from "./src/app/screens/APD/DetailMyOrderPeminjamanAPD";
import OrderPersonalSubmit from "./src/app/screens/APD/OrderPersonalSubmit";
import OrderUnitKerjaSubmit from "./src/app/screens/APD/OrderUnitKerjaSubmit";
import OrderPeminjamanSubmit from "./src/app/screens/APD/OrderPeminjamanSubmit";
import OrderRusakSubmit from "./src/app/screens/APD/OrderRusakSubmit";
import OrderHilangSubmit from "./src/app/screens/APD/OrderHilangSubmit";
import OrderPersonalTroli from "./src/app/screens/APD/OrderPersonalTroli";
import OrderUnitKerjaTroli from "./src/app/screens/APD/OrderUnitKerjaTroli";
import OrderPeminjamanTroli from "./src/app/screens/APD/OrderPeminjamanTroli";
import ReportAPD from "./src/app/screens/APD/ReportAPD";
import ReportPersentageAPD from "./src/app/screens/APD/ReportPersentageAPD";

import SafetyMenu from "./src/app/screens/SAFETY/SafetyMenu";
import DailyReportK3 from "./src/app/screens/SAFETY/DailyReportK3";
import DailyReportK3Detail from "./src/app/screens/SAFETY/DailyReportK3Detail";
import DailyReportK3DetailList from "./src/app/screens/SAFETY/DailyReportK3DetailList";
import DailyReportK3DetailHeaderList from "./src/app/screens/SAFETY/DailyReportK3DetailHeaderList";
//import DailyReportK3DetailFoto from "./src/app/screens/SAFETY/DailyReportK3DetailFoto";
import CreateHeaderReportK3 from "./src/app/screens/SAFETY/CreateHeaderReportK3";
import CreateDetailHeaderReportK3 from "./src/app/screens/SAFETY/CreateDetailHeaderReportK3";
import UpdateHeaderReportK3 from "./src/app/screens/SAFETY/UpdateHeaderReportK3";
import UpdateDetailHeaderReportK3 from "./src/app/screens/SAFETY/UpdateDetailHeaderReportK3";

import DailyReport from "./src/app/screens/SAFETY/DailyReport";
import DailyReportUnsafe from "./src/app/screens/SAFETY/DailyReportUnsafe";
import DailyReportUnsafeDetail from "./src/app/screens/SAFETY/DailyReportUnsafeDetail";
import DailyReportDetailListUpdate from "./src/app/screens/SAFETY/DailyReportDetailListUpdate";
import DailyReportDetailList from "./src/app/screens/SAFETY/DailyReportDetailList";
import DailyReportDetailListFoto from "./src/app/screens/SAFETY/DailyReportDetailListFoto";
import CreateTemuanUnsafe from "./src/app/screens/SAFETY/CreateTemuanUnsafe";
import InspeksiTemuanUnsafe from "./src/app/screens/SAFETY/InspeksiTemuanUnsafe";

import Recapitulations from "./src/app/screens/SAFETY/Recapitulations";
import RecapitulationsDetail from "./src/app/screens/SAFETY/RecapitulationsDetail";
import UpdateRecapitulations from "./src/app/screens/SAFETY/UpdateRecapitulations";



import SIOCertification from "./src/app/screens/SAFETY/SIOCertification";
import SIOCertificationHistori from "./src/app/screens/SAFETY/SIOCertificationHistori";
import SIOCertificationDetail from "./src/app/screens/SAFETY/SIOCertificationDetail";
import SIOCertificationUpdate from "./src/app/screens/SAFETY/SIOCertificationUpdate";
import ToolsCertification from "./src/app/screens/SAFETY/ToolsCertification";
import ToolsCertificationUpdate from "./src/app/screens/SAFETY/ToolsCertificationUpdate";
import ToolsCertificationDetail from "./src/app/screens/SAFETY/ToolsCertificationDetail";
import CreateToolsCertification from "./src/app/screens/SAFETY/CreateToolsCertification"
import CreateSIOCertification from "./src/app/screens/SAFETY/CreateSIOCertification";

import AccidentReport from "./src/app/screens/SAFETY/AccidentReport";
import AccidentReportDetail from "./src/app/screens/SAFETY/AccidentReportDetail";
import AccidentReportUpdate from "./src/app/screens/SAFETY/AccidentReportUpdate";
import CreateAccidentReport from "./src/app/screens/SAFETY/CreateAccidentReport";


/*FIRE SYSTEM*/
import FireMenu from "./src/app/screens/FIRE/FireMenu";
import DailyReportAktifitas from "./src/app/screens/FIRE/DailyReportAktifitas";
import DailyReportAktifitasDetail from "./src/app/screens/FIRE/DailyReportAktifitasDetail";
import DailyReportAktifitasCreate from "./src/app/screens/FIRE/DailyReportAktifitasCreate";
import DailyReportAktifitasUpdate from "./src/app/screens/FIRE/DailyReportAktifitasUpdate";
import DailyReportCO2 from "./src/app/screens/FIRE/DailyReportCO2";
import DailyReportCO2Detail from "./src/app/screens/FIRE/DailyReportCO2Detail";
import DailyReportCO2Create from "./src/app/screens/FIRE/DailyReportCO2Create";
import DailyReportCO2Update from "./src/app/screens/FIRE/DailyReportCO2Update";

import FireReport from "./src/app/screens/FIRE/FireReport";
import FireReportCreate from "./src/app/screens/FIRE/FireReportCreate";
import FireReportDetail from "./src/app/screens/FIRE/FireReportDetail";
import FireReportImage from "./src/app/screens/FIRE/FireReportImage";
import FireReportUpdate from "./src/app/screens/FIRE/FireReportUpdate";

import UnitCheckPickup from "./src/app/screens/FIRE/UnitCheckPickup";
import UnitCheckDetailPickup from "./src/app/screens/FIRE/UnitCheckDetailPickup";
import UnitCheckPMK from "./src/app/screens/FIRE/UnitCheckPMK";
import UnitCheckDetailPMKEngine from "./src/app/screens/FIRE/UnitCheckDetailPMKEngine";
import UnitCheckDetailPMKTools from "./src/app/screens/FIRE/UnitCheckDetailPMKTools";
import UnitCheckCreateVehicle from "./src/app/screens/FIRE/UnitCheckCreateVehicle";
import UnitCheckUpdateVehicle from "./src/app/screens/FIRE/UnitCheckUpdateVehicle";
import UnitCheckPickupCreate from "./src/app/screens/FIRE/UnitCheckPickupCreate";
import UnitCheckPMKCreate from "./src/app/screens/FIRE/UnitCheckPMKCreate";

import DashboardUnitCheckPickup from "./src/app/screens/FIRE/DashboardUnitCheckPickup";
import DashboardUnitCheckPMK from "./src/app/screens/FIRE/DashboardUnitCheckPMK";
import DashboardUnitCheckPerformance from "./src/app/screens/FIRE/DashboardUnitCheckPerformance";
import DashboardUnitCheckDetail from "./src/app/screens/FIRE/DashboardUnitCheckDetail";

import InspectionReportAlarm from "./src/app/screens/FIRE/InspectionReportAlarm";
import InspectionReportAlarmScan from "./src/app/screens/FIRE/InspectionReportAlarmScan";
import InspectionReportAlarmCreate from "./src/app/screens/FIRE/InspectionReportAlarmCreate";
import InspectionReportAlarmDetail from "./src/app/screens/FIRE/InspectionReportAlarmDetail";
import InspectionReportAlarmHistory from "./src/app/screens/FIRE/InspectionReportAlarmHistory";
import InspectionReportAlarmUpdate from "./src/app/screens/FIRE/InspectionReportAlarmUpdate";
import InspectionReportApar from "./src/app/screens/FIRE/InspectionReportApar";
import InspectionReportAparDetail from "./src/app/screens/FIRE/InspectionReportAparDetail";
import InspectionReportAparCreate from "./src/app/screens/FIRE/InspectionReportAparCreate";
import InspectionReportAparUpdate from "./src/app/screens/FIRE/InspectionReportAparUpdate";
import InspectionReportAparScan from "./src/app/screens/FIRE/InspectionReportAparScan";
import InspectionReportHydrant from "./src/app/screens/FIRE/InspectionReportHydrant";
import InspectionReportHydrantScan from "./src/app/screens/FIRE/InspectionReportHydrantScan";
import InspectionReportHydrantCreate from "./src/app/screens/FIRE/InspectionReportHydrantCreate";
import InspectionReportHydrantDetail from "./src/app/screens/FIRE/InspectionReportHydrantDetail";
import InspectionReportHydrantUpdate from "./src/app/screens/FIRE/InspectionReportHydrantUpdate";

import Lotto from "./src/app/screens/FIRE/Lotto";
import LottoDetail from "./src/app/screens/FIRE/LottoDetail";
import LottoCreate from "./src/app/screens/FIRE/LottoCreate";
import LottoUpdate from "./src/app/screens/FIRE/LottoUpdate";
import LottoDrawIN from "./src/app/screens/FIRE/LottoDrawIN";
import LottoDrawOUT from "./src/app/screens/FIRE/LottoDrawOUT";
import LottoInputDrawIN from "./src/app/screens/FIRE/LottoInputDrawIN";
import LottoInputDrawOUT from "./src/app/screens/FIRE/LottoInputDrawOUT";



// const DrawerButton = props => {
//   return (
//     <View>
//       <Button
//         onPress={() => {
//           props.navigation.toggleDrawer();
//         }}
//       >
//         <Icon name="ios-menu" size={20} style={styles.facebookButtonIcon} />
//       </Button>
//     </View>
//   );
// };

const NavStack = createStackNavigator({

  LoggedOut: {
    screen: LoggedOut
  },
  Login: {
    screen: Login
  },
  DetailNews: {
    screen: DetailNews
  },
  HomeMenu:{
    screen: HomeMenu
  },
  SafetyMenu: {
    screen: SafetyMenu,
  },
  FireMenu: {
    screen: FireMenu,
  },
  ProfilUser:{
    screen:ProfilUser
  },
  LaporanAPD:{
    screen:LaporanAPD
  },
  ApdMenu: {
    screen: ApdMenu,
  },
  ApprovalPersonalAPD: {
    screen: ApprovalPersonalAPD
  },
  ActionsApprovalPersonalAPD: {
    screen: ActionsApprovalPersonalAPD
  },
  ApprovalUnitKerjaAPD: {
    screen: ApprovalUnitKerjaAPD
  },
  ActionsApprovalUnitKerjaAPD: {
    screen: ActionsApprovalUnitKerjaAPD
  },
  ApprovalPeminjamanAPD: {
    screen: ApprovalPeminjamanAPD
  },
  ActionsApprovalPeminjamanAPD: {
    screen: ActionsApprovalPeminjamanAPD
  },
  IndividualReport: {
    screen: IndividualReport
  },
  IndividualReportPersonal: {
    screen: IndividualReportPersonal
  },
  IndividualReportUnitKerja: {
    screen: IndividualReportUnitKerja
  },
  IndividualReportPeminjaman: {
    screen: IndividualReportPeminjaman
  },
  StockAPD: {
    screen: StockAPD
  },
  OrderPersonalAPD: {
    screen: OrderPersonalAPD
  },
  OrderUnitKerjaAPD: {
    screen: OrderUnitKerjaAPD
  },
  OrderPeminjamanAPD: {
    screen: OrderPeminjamanAPD
  },
  OrderHilang: {
    screen: OrderHilang
  },
  OrderRusak: {
    screen: OrderRusak
  },
  MyOrderPersonalAPD: {
    screen: MyOrderPersonalAPD
  },
  MyOrderUnitKerjaAPD: {
    screen: MyOrderUnitKerjaAPD
  },
  MyOrderPeminjamanAPD: {
    screen: MyOrderPeminjamanAPD
  },
  DetailMyOrderPersonalAPD: {
    screen: DetailMyOrderPersonalAPD
  },
  DetailMyOrderUnitKerjaAPD: {
    screen: DetailMyOrderUnitKerjaAPD
  },
  DetailMyOrderPeminjamanAPD: {
    screen: DetailMyOrderPeminjamanAPD
  },
  OrderRusakSubmit: {
    screen: OrderRusakSubmit
  },
  OrderHilangSubmit: {
    screen: OrderHilangSubmit
  },
  OrderPersonalSubmit: {
    screen: OrderPersonalSubmit
  },
  OrderUnitKerjaSubmit: {
    screen: OrderUnitKerjaSubmit
  },
  OrderPeminjamanSubmit: {
    screen: OrderPeminjamanSubmit
  },
  OrderPersonalTroli: {
    screen: OrderPersonalTroli
  },
  OrderUnitKerjaTroli: {
    screen: OrderUnitKerjaTroli
  },
  OrderPeminjamanTroli: {
    screen: OrderPeminjamanTroli
  },
  ReportAPD: {
    screen: ReportAPD
  },
  ReportPersentageAPD: {
    screen: ReportPersentageAPD
  },

  DailyReportK3: {
    screen: DailyReportK3
  },
  DailyReportK3Detail: {
    screen: DailyReportK3Detail
  },
  DailyReportK3DetailList: {
    screen: DailyReportK3DetailList
  },
  DailyReportK3DetailHeaderList: {
    screen: DailyReportK3DetailHeaderList
  },
  // DailyReportK3DetailFoto: {
  //   screen: DailyReportK3DetailFoto
  // },
  CreateHeaderReportK3: {
    screen: CreateHeaderReportK3
  },
  CreateDetailHeaderReportK3: {
    screen: CreateDetailHeaderReportK3
  },
  UpdateHeaderReportK3: {
    screen: UpdateHeaderReportK3
  },
  UpdateDetailHeaderReportK3: {
    screen: UpdateDetailHeaderReportK3
  },

  DailyReport: {
    screen: DailyReport
  },
  DailyReportUnsafe: {
    screen: DailyReportUnsafe
  },
  DailyReportUnsafeDetail: {
    screen: DailyReportUnsafeDetail
  },
  DailyReportDetailListUpdate: {
    screen: DailyReportDetailListUpdate
  },
  DailyReportDetailList: {
    screen: DailyReportDetailList
  },
  DailyReportDetailListFoto: {
    screen: DailyReportDetailListFoto
  },
  CreateTemuanUnsafe: {
    screen: CreateTemuanUnsafe
  },
  InspeksiTemuanUnsafe: {
    screen: InspeksiTemuanUnsafe
  },

  Recapitulations: {
    screen: Recapitulations
  },
  RecapitulationsDetail: {
    screen: RecapitulationsDetail
  },
  UpdateRecapitulations: {
    screen: UpdateRecapitulations
  },

  Lotto: {
    screen: Lotto
  },
  LottoCreate: {
    screen: LottoCreate
  },
  LottoDetail: {
    screen: LottoDetail
  },
  LottoUpdate: {
    screen: LottoUpdate
  },
  LottoDrawIN: {
    screen: LottoDrawIN
  },
  LottoDrawOUT: {
    screen: LottoDrawOUT
  },
  LottoInputDrawIN: {
    screen: LottoInputDrawIN
  },
  LottoInputDrawOUT: {
    screen: LottoInputDrawOUT
  },


  SIOCertification: {
    screen: SIOCertification
  },
  SIOCertificationHistori: {
    screen: SIOCertificationHistori
  },
  SIOCertificationDetail: {
    screen: SIOCertificationDetail
  },
  SIOCertificationUpdate: {
    screen: SIOCertificationUpdate
  },
  ToolsCertification: {
    screen: ToolsCertification
  },
  ToolsCertificationDetail: {
    screen: ToolsCertificationDetail
  },
  ToolsCertificationUpdate: {
    screen: ToolsCertificationUpdate
  },
  CreateSIOCertification: {
    screen: CreateSIOCertification
  },
  CreateToolsCertification: {
    screen: CreateToolsCertification
  },

  AccidentReport: {
    screen: AccidentReport
  },
  AccidentReportDetail: {
    screen: AccidentReportDetail
  },
  AccidentReportUpdate: {
    screen: AccidentReportUpdate
  },
  CreateAccidentReport: {
    screen: CreateAccidentReport
  },

  /*FIRE SYSTEM*/
  DailyReportAktifitas: {
    screen: DailyReportAktifitas
  },
  DailyReportAktifitasDetail: {
    screen: DailyReportAktifitasDetail
  },
  DailyReportAktifitasCreate: {
    screen: DailyReportAktifitasCreate
  },
  DailyReportAktifitasUpdate: {
    screen: DailyReportAktifitasUpdate
  },
  DailyReportCO2: {
    screen: DailyReportCO2
  },
  DailyReportCO2Detail: {
    screen: DailyReportCO2Detail
  },
  DailyReportCO2Create: {
    screen: DailyReportCO2Create
  },
  DailyReportCO2Update: {
    screen: DailyReportCO2Update
  },
  FireReport:{
    screen: FireReport
  },
  FireReportCreate:{
    screen: FireReportCreate
  },
  FireReportDetail:{
    screen: FireReportDetail
  },
  FireReportImage:{
    screen: FireReportImage
  },
  FireReportUpdate:{
    screen: FireReportUpdate
  },
  UnitCheckPickup: {
    screen: UnitCheckPickup
  },
  UnitCheckDetailPickup: {
    screen: UnitCheckDetailPickup
  },
  UnitCheckPMK: {
    screen: UnitCheckPMK
  },
  UnitCheckDetailPMKEngine: {
    screen: UnitCheckDetailPMKEngine
  },
  UnitCheckDetailPMKTools: {
    screen: UnitCheckDetailPMKTools
  },
  UnitCheckCreateVehicle: {
    screen: UnitCheckCreateVehicle
  },
  UnitCheckUpdateVehicle: {
    screen: UnitCheckUpdateVehicle
  },
  UnitCheckPickupCreate: {
    screen: UnitCheckPickupCreate
  },
  UnitCheckPMKCreate: {
    screen: UnitCheckPMKCreate
  },
  DashboardUnitCheckPickup: {
    screen: DashboardUnitCheckPickup
  },
  DashboardUnitCheckPMK: {
    screen: DashboardUnitCheckPMK
  },
  DashboardUnitCheckPerformance: {
    screen: DashboardUnitCheckPerformance
  },
  DashboardUnitCheckDetail: {
    screen: DashboardUnitCheckDetail
  },
  InspectionReportAlarm: {
    screen: InspectionReportAlarm
  },
  InspectionReportAlarmDetail: {
    screen: InspectionReportAlarmDetail
  },
  InspectionReportAlarmScan: {
    screen: InspectionReportAlarmScan
  },
  InspectionReportAlarmCreate: {
    screen: InspectionReportAlarmCreate
  },
  InspectionReportAlarmHistory: {
    screen: InspectionReportAlarmHistory
  },
  InspectionReportAlarmUpdate: {
    screen: InspectionReportAlarmUpdate
  },
  InspectionReportApar: {
    screen: InspectionReportApar
  },
  InspectionReportAparDetail: {
    screen: InspectionReportAparDetail
  },
  InspectionReportAparCreate: {
    screen: InspectionReportAparCreate
  },
  InspectionReportAparUpdate: {
    screen: InspectionReportAparUpdate
  },
  InspectionReportAparScan: {
    screen: InspectionReportAparScan
  },
  InspectionReportHydrant: {
    screen: InspectionReportHydrant
  },
  InspectionReportHydrantScan: {
    screen: InspectionReportHydrantScan
  },
  InspectionReportHydrantCreate: {
    screen: InspectionReportHydrantCreate
  },
  InspectionReportHydrantDetail: {
    screen: InspectionReportHydrantDetail
  },
  InspectionReportHydrantUpdate: {
    screen: InspectionReportHydrantUpdate
  },
});

const App = createAppContainer(NavStack);

export default App;

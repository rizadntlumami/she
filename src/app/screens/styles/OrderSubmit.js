import { StyleSheet } from "react-native";
import iPhoneSize from "../../../helpers/utils";
import colors from "../../../styles/colors";
import { Row } from "native-base";

let labelTextSize = 12;
let weatherTextSize = 18;
let weatherTextSmSize = 14;
let headingTextSize = 28;
if (iPhoneSize() === "small") {
  labelTextSize = 10;
  headingTextSize = 24;
  weatherTextSize = 16;
  weatherTextSmSize = 12;
}

const styles = StyleSheet.create({

  viewKonfirmasiLeft: {
    width: 230,
  },
  viewKonfirmasiRight: {
    width: 150,
  },
  viewKode: {
    fontSize: 12,
    color: '#00b300'
  },
  viewNama: {
    fontSize: 14,
    fontWeight:'bold'
  },
  viewMerk: {
    fontSize: 12,
  },

  wrapper: {
    flex: 1,
    display: "flex",
  },
  facebookButtonIcon: {
    color: colors.white
  },
  header: {
    backgroundColor: colors.green01
  },
  tabfooter: {
    backgroundColor: colors.green01
  },
  textbody: {
    justifyContent: 'center',
    fontWeight: 'bold',
    color: colors.white
  },
  homeWrapper: {
    flex: 1,
    display: "flex",
  },
  weatherContent: {
    flexDirection: "row",
    marginBottom: 15,
    borderBottomColor: colors.lightBlack,
    borderBottomWidth: 1,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  newsContent: {
    flexDirection: "row"
  },
  weatherText: {
    fontFamily: "Montserrat-Medium",
    fontSize: weatherTextSize,
    color: colors.lightBlack,
    fontWeight: "300",
    fontSize: headingTextSize,
    marginBottom: 5,
  },
  weatherTextSm: {
    fontFamily: "Montserrat-Light",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 10
  },
  newsText: {
    fontFamily: "Montserrat-Regular",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 5,
    marginLeft: 20
  },
  news: {
    height: 160,
    marginTop: 20
  },
  search: {
    position: 'absolute',
    left: 10,
    right: 10,
    top: 60,
    flexDirection: 'row',
  },
  searchItem: {
    backgroundColor: 'white',
    paddingHorizontal: 10,
    borderRadius: 20
  },
  // itemMenu: {
  //   flexDirection: "row",
  //   padding: 10,
  //   justifyContent: 'center'
  // },
  roundedButton: {
    padding: 10,
    justifyContent: 'center',
    top: 70,
    flexDirection: "row"
  },
  loginButton: {
    margin: 30,
    height: '75%',
  },
  apdorderimage: {
    alignItems: 'center',
    width: 90,
    height: 90,
    marginTop: 2,
    marginLeft: 1,
    marginRight: 2,
  },
  apdorderimg: {
    alignItems: 'center',
    width: 50,
    height: 50,
    marginTop: 2,
    marginLeft: 1,
    marginRight: 2,
  },

  viewLeft: {
    width: 100,
  },
  viewLeftOrder: {
    width: 70,
  },
  viewOrderRusak: {
    width: 300,
  },
  viewRightOrder: {
    width: 150,
  },
  viewLeftBtnUpload: {
    width: 255,
  },
  viewLeftBtnUploadHilang: {
    width: 110,
  },
  viewRightBtnUploadHilang: {
    width: 200,
  },
  viewRightBtnUpload: {
    width: 200,
    textAlign: 'right'
  },
  viewCenter: {
    width: 120,
    fontSize: 15
  },
  viewRight: {
    width: 90,
    textAlign: 'right',
    fontSize: 15
  },
  roundedBtn: {
    padding: 10,
    justifyContent: 'center',
    top: 50,
    flexDirection: "row"
  },

  //IMAGE//
  placeholder: {
    backgroundColor: "#eee",
    width: "100%",
    height: 200,
    borderRadius:10,
  },

  previewImage: {
    width: "100%",
    height: "100%"
  }


});

export default styles;

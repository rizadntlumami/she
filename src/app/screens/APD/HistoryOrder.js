import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  FlatList,
  AsyncStorage,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
import NewsContents from "../../components/NewsContent";
import SubMenuItems from "../../components/SubMenuItems";
import GlobalConfig from '../../components/GlobalConfig';

import styles from "../styles/HistoryOrder";
import colors from "../../../styles/colors";

const helmet = require("../../../assets/images/helmet.png");

class ListItem extends React.PureComponent {
  render() {
    return (
      <View style={{ backgroundColor: '#FEFEFE' }}>
        <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
          <CardItem style={{ borderRadius: 10 }}>
            <View style={{ flex: 1, flexDirection: 'row' }}>
              <View style={styles.viewLeft}>
                <Image
                  style={styles.apdorderimage}
                  source={require('../../../assets/images/imgorder.png')}
                />
              </View>
              <View>
                <Text style={styles.viewCenter}>{this.props.data.LAST_SKU}</Text>
                <Text style={styles.viewCenter}>{this.props.data.NAMA}</Text>
                <Text style={styles.viewCenter}>{this.props.data.CREATE_BY}</Text>
              </View>
              <View>
                <Text style={styles.viewRight}>{this.props.data.ORDER}</Text>
                <Text style={styles.viewRight}>{this.props.data.MERK}</Text>
              </View>
            </View>
          </CardItem>
        </Card>
      </View>
    )
  }
}

export default class HistoryOrder extends Component {
  constructor() {
    super();
    this.state = {
      active: 'true',
      dataSource: [],
      isloading: true,
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => (
    <ListItem data={item}></ListItem>
  )

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    AsyncStorage.getItem('token').then((value) => {
      // alert(JSON.stringify(value));
      const url = GlobalConfig.SERVERHOST + 'api/v_mobile/apd/validasi/order_type/individu';
      var formData = new FormData();
      formData.append("token", value)

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'POST',
        body: formData
      })
        .then((response) => response.json())
        .then((responseJson) => {
          //alert(JSON.stringify(responseJson));
          this.setState({
            dataSource: responseJson.data,
            isloading: false
          });
        })
        .catch((error) => {
          console.log(error)
        })
    })
  }


  render() {
    return (
      // this.state.isloading
      // ?
      // <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      //   <ActivityIndicator size= "large" color= "#330066" animating />
      // </View>
      // :
      <Container style={styles.wrapper}>
        <View style={styles.weatherContent}>
          <View style={{ flex: 0 }}>
            <Text style={styles.weatherText}>History Order</Text>
          </View>
          <View style={{ flex: 1 }} />
        </View>

        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />

        <Content>
          <FlatList
            data={this.state.dataSource}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
          />
        </Content>
      </Container>
    );
  }
}

import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewHeader from "../../components/ListViewHeader";
import ListViewDetail from "../../components/ListViewDetail";

class ListTools extends React.PureComponent {
  render() {
    return (
      <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>

          <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
            <Text>{this.props.data}</Text>
          </View>

      </Card>
    );
  }
}

class LottoDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isLoading: true,
      listHeader:[],
      listLotto:[],
      listTools:[],
      visibleDialogSubmit:false,
      idLotto:'',
      dataLotto:[],
    };
  }

  static navigationOptions = {
      header: null
  };

  _renderItem = ({ item }) => (
      <ListItem data={item}></ListItem>
  )

  navigateToScreen(route, idLotto) {
      AsyncStorage.setItem('idLotto', idLotto).then(() => {
          this.props.navigation.navigate(route);
      })
  }

  navigateToScreenUpdate(route, dataLotto) {
      AsyncStorage.setItem('dataLotto', JSON.stringify(dataLotto)).then(() => {
          this.props.navigation.navigate(route);
      })
  }

  componentDidMount() {
      AsyncStorage.getItem('idLotto').then((idLotto) => {
          this.setState({
            idLotto: idLotto,
            isLoading: false,
          });
          this.loadLotto();
      })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
          this.loadLotto();
      });
  }

  loadLotto(){
    const url = GlobalConfig.SERVERHOST + "getLotto";
    var formData = new FormData();
    formData.append("id_lotto", this.state.idLotto);

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.status==200){
          this.setState({
            dataLotto: responseJson.data,
            isLoading: false
          });
          this.setState({
            listTools: JSON.parse(this.state.dataLotto.safety_tools),
          })
        }
      })
      .catch(error => {
        this.setState({
          isEmpty:true
        })
        console.log(error);
      });
  }

  konfirmasideleteLotto() {
    Alert.alert(
      'Apakah Anda Yakin Ingin Menghapus', this.state.dataLotto.equipment_number,
      [
        {text: 'Yes', onPress:()=> this.deleteLotto()},
        {text: 'Cancel'},
      ],
      { cancelable:false}
    )
  }

  deleteLotto() {
      this.setState({
        visibleDialogSubmit:true
      })
          const url = GlobalConfig.SERVERHOST + 'deleteLotto';
          var formData = new FormData();
          formData.append("id", this.state.dataLotto.id);

          fetch(url, {
              headers: {
                  "Content-Type": "multipart/form-data"
              },
              method: "POST",
              body: formData
          })
              .then(response => response.json())
              .then(response => {
                  if (response.status == 200) {
                      this.setState({
                        visibleDialogSubmit:false
                      })
                      Alert.alert('Success', 'Delete Success', [{
                          text: 'Oke',
                      }])
                      this.props.navigation.navigate("Lotto")
                  } else {
                      this.setState({
                        visibleDialogSubmit:false
                      })
                      Alert.alert('Error', 'Delete Failed', [{
                          text: 'Oke'
                      }])
                  }
              })
              .catch((error)=>{
                console.log(error)
                this.setState({
                  visibleDialogSubmit:false
                })
                Alert.alert('Error', 'Delete Failed', [{
                    text: 'Oke'
                }])
              })

  }

  _renderTools = ({ item }) => <ListTools data={item} />;

  render() {
    return (
      <Container style={styles.wrapper}>
          <Header style={styles.header}>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("Lotto")}
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Left>
            <Body>
              <Title style={styles.textbody}>Detail Lotto</Title>
            </Body>
            <Right style={{width:'20%'}}>
              <Button
                transparent
                onPress={() => this.navigateToScreenUpdate('LottoUpdate', this.state.dataLotto)}
              >
                <Icon
                  name="ios-create"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
              <Button
                transparent
                onPress={() => this.konfirmasideleteLotto()}
              >
                <Icon
                  name="ios-trash"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
              <Button
                transparent
                onPress={() => this.navigateToScreen('LottoDrawIN', this.state.idLotto)}
              >
                <Icon
                  name="ios-git-compare"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Right>
          </Header>
          <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
          <Content style={{marginTop:5}}>
          <View style={{}}>
            <CardItem style={{ borderRadius: 0, marginTop:5, }}>
              <View style={{ flex: 1, flexDirection: "row"}}>
                <View style={{width: 260}}>
                  <Text style={{fontSize:10}}>Nomor Equipment</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.dataLotto.equipment_number}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Nomor ER</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.dataLotto.no_er}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Area</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.dataLotto.location}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Date</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.dataLotto.date}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Time</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.dataLotto.time}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Kegiatan</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.dataLotto.activity_description}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Note</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.dataLotto.note}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Safety Key</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.dataLotto.safety_key}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Safety Tools</Text>
                  <FlatList
                    data={this.state.listTools}
                    renderItem={this._renderTools}
                    keyExtractor={(item, index) => index.toString()}
                  />
                </View>
              </View>
            </CardItem>

            <View style={{ width: 270, position: "absolute" }}>
              <Dialog
                visible={this.state.visibleDialogSubmit}
                dialogTitle={<DialogTitle title="Deleting Lotto .." />}
              >
                <DialogContent>
                  {<ActivityIndicator size="large" color="#330066" animating />}
                </DialogContent>
              </Dialog>
            </View>
          </View>
          </Content>
      </Container>
    );
  }
}

export default LottoDetail;

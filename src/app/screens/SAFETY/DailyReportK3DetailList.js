import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl,
  Dimensions
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewDetail from "../../components/ListViewDetail";
import Ripple from "react-native-material-ripple";

class ListItem extends React.PureComponent {
  navigateToScreen(route, listReportK3) {
    AsyncStorage.setItem("list", JSON.stringify(listReportK3)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View>
        <Ripple
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.navigateToScreen("DailyReportK3DetailHeaderList", this.props.data)
          }
          rippleColor={colors.accent}
        >
          <View style={{ backgroundColor: "#FEFEFE" }}>
            <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
              <View
                style={{
                  marginTop: 10,
                  marginLeft: 10,
                  marginRight: 10,
                  marginBottom: 10
                }}
              >
                <ListViewDetail
                  unit={this.props.data.UNIT_NAME}
                  aktifitas={this.props.data.ACTIVITY_DESCRIPTION}
                  type={this.props.data.UNSAFE_TYPE}
                  potensi={this.props.data.POTENTIAL_HAZARD}
                  followUp={this.props.data.FOLLOW_UP}
                  status={this.props.data.STATUS}
                  kode={this.props.data.KODE_EN}
                  deskripsi={this.props.data.DESKRIPSI}
                  idKategori={this.props.data.ID_CATEGORY}
                />
              </View>
            </Card>
          </View>
        </Ripple>
      </View>
    );
  }
}

class DailyReportK3DetailList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      idUnsafe: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => (
    <ListItem data={item} navigation={this.props.navigation} />
  );

  onRefresh() {
    console.log("refreshing");
    this.setState({ isLoading: true }, function() {
      this.loadData();
    });
  }

  componentDidMount() {
    // AsyncStorage.getItem('id').then((idUnsafe) => {
    //     this.setState({ idUnsafe: idUnsafe });
    //     this.setState({ isLoading: true });
    //     this.loadData(idUnsafe);
    // })
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        AsyncStorage.getItem("id").then(idUnsafe => {
          this.setState({ idUnsafe: idUnsafe });
          this.setState({ isLoading: false });
          this.loadData(idUnsafe);
        });
      }
    );
  }

  

  loadData(idUnsafe) {
    this.setState({ isLoading: true });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/safety/unsafe_n/view_report_list_detail";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("search", "");
      formData.append("length", 100);
      formData.append("start", 0);
      formData.append("id_unsafe_report", idUnsafe);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataSource: responseJson.data.filter(x => x.DELETE_FLAG == 0),
            isLoading: false
          });
        })
        .catch(error => {
          this.setState({ isLoading: false });
          console.log(error);
        });
    });
  }

  navigateToScreenCreate(route,id){
    AsyncStorage.setItem("id", id).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  render() {
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.dataSource.length == 0) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = 
        <FlatList
        data={this.state.dataSource}
        renderItem={this._renderItem}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.isLoading}
            onRefresh={this.onRefresh.bind(this)}
          />}
      />
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() =>
                this.props.navigation.navigate("DailyReportK3Detail")
              }
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Unsafe Detail List</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ marginTop: 10,flex:1,flexDirection:'column' }}>{list}</View>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: colors.green01,  marginBottom: ((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?80:50 }}
          position="bottomRight"
          onPress={() =>
            this.navigateToScreenCreate(
              "CreateDetailHeaderReportK3",
              this.state.idUnsafe
            )
          }
        >
          <Icon name="ios-add" style={{ fontSize: 50, fontWeight: "bold",paddingTop: Platform.OS === 'ios' ? 25:null, }} />
        </Fab>
        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}

export default DailyReportK3DetailList;

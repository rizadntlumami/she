import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  RefreshControl,
  Platform,
  TouchableOpacity,
  Dimensions,
  Alert
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Card,
  CardItem,
  Item,
  Input,
  Icon,
  Thumbnail,
  Badge
} from "native-base";
// import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
import GlobalConfig from "../../components/GlobalConfig";
import CustomFooter from "../../components/CustomFooter";

import styles from "../styles/ApprovalAPD";
import colors from "../../../styles/colors";


export default class LaporanAPD extends Component {
  constructor() {
    super();
    this.state={
      notif:0
    }

  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      this.loadNotif();
    });
    //this.loadData();

  }

  loadNotif(){
    console.log('notif called')
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/apd/order/approval_count";
      var formData = new FormData();
      formData.append("token", value);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          if(responseJson.status==200){
            this.setState({
              notif: responseJson.data,
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  logOut(){
    Alert.alert(
      'Confirmation',
      'Logout SHE Mobile?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => this.props.navigation.navigate("LoggedOut") },
      ],
      { cancelable: false }
    );
  }

  render() {

    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
          <Left style={{flex:1}}>

          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
          <Title style={styles.textbody}>Report</Title>
          </Body>

          <Right style={{flex:1}}/>



        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={styles.homeWrapper}>
          <View style={{ flex: 1, flexDirection: "column" }}>
          <View>
                <Card style={{ marginLeft: 20, marginRight: 20, marginTop:20, borderRadius:10,}}>
                <CardItem
                style={{ borderRadius:10,}}
                    button
                    onPress={() =>
                    this.props.navigation.navigate("OrderHilang")
                    }
                >
                    <View style={{ flexDirection: "column", flex: 1 }}>


                    <Text style={{ fontSize: 15 }}> APD Hilang</Text>
                    <Text style={{ fontSize: 11 }}> Lapor APD Hilang </Text>
                    </View>
                    <Right>
                <Icon name="arrow-forward" />
              </Right>
                </CardItem>

                </Card>
            </View>
            <View>
                <Card style={{ marginLeft: 20, marginRight: 20, marginTop:20, borderRadius:10, }}>
                <CardItem
                style={{ borderRadius:10,}}
                    button
                    onPress={() =>
                    this.props.navigation.navigate("OrderRusak")
                    }
                >
                    <View style={{ flexDirection: "column", flex: 1 }}>


                    <Text style={{ fontSize: 15 }}> APD Rusak</Text>
                    <Text style={{ fontSize: 11 }}> Lapor APD Rusak </Text>
                    </View>
                    <Right>
                <Icon name="arrow-forward" />
              </Right>
                </CardItem>

                </Card>
            </View>
            <View>
                <Card style={{ marginLeft: 20, marginRight: 20, marginTop:20, borderRadius:10, }}>
                <CardItem
                style={{ borderRadius:10,}}
                    button
                    onPress={() =>
                    this.props.navigation.navigate("ApprovalPersonalAPD")
                    }
                >
                    <View style={{ flexDirection: "column", flex: 2 }}>


                    <Text style={{ fontSize: 15 }}> Approval APD</Text>
                    <Text style={{ fontSize: 11 }}> Persetujuan Permintaan APD</Text>
                    </View>

                    <Right>
                    <View style={{flexDirection:'row'}}>
                      {this.state.notif!=0 &&(
                        <Badge style={{marginRight:20}}><Text>{this.state.notif}</Text></Badge>
                      )}
                      <Icon name="arrow-forward" />
                    </View>

              </Right>
                </CardItem>

                </Card>
            </View>

            <View>
                <Card style={{ marginLeft: 20, marginRight: 20, marginTop:20, borderRadius:10,  }}>
                <CardItem
                style={{ borderRadius:10,}}
                    button
                    onPress={() =>
                    this.props.navigation.navigate("ReportPersentageAPD")
                    }
                >
                    <View style={{ flexDirection: "column", flex: 1 }}>


                    <Text style={{ fontSize: 15 }}> Jumlah APD</Text>
                    <Text style={{ fontSize: 11 }}> Presentasi jumlah stock APD</Text>
                    </View>
                    <Right>
                <Icon name="arrow-forward" />
              </Right>
                </CardItem>

                </Card>
            </View>
            <View>
                <Card style={{ marginLeft: 20, marginRight: 20, marginTop:20,borderRadius:10,  }}>
                <CardItem
                    style={{ borderRadius:10,}}
                    button
                    onPress={() =>
                    this.props.navigation.navigate("IndividualReport")
                    }
                >
                    <View style={{ flexDirection: "column", flex: 1 }}>


                    <Text style={{ fontSize: 15 }}> Individual Report</Text>
                    <Text style={{ fontSize: 11 }}> Laporan Pemakaian Personal</Text>
                    </View>
                    <Right>
                <Icon name="arrow-forward" />
              </Right>
                </CardItem>

                </Card>
            </View>


          </View>

        </View>
        <CustomFooter navigation={this.props.navigation} menu="Laporan" />
      </Container>
    );
  }
}

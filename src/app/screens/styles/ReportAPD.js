import { StyleSheet } from "react-native";
import iPhoneSize from "../../../helpers/utils";
import colors from "../../../styles/colors";
import { Row } from "native-base";

let labelTextSize = 12;
let weatherTextSize = 18;
let weatherTextSmSize = 14;
let headingTextSize = 28;
if (iPhoneSize() === "small") {
  labelTextSize = 10;
  headingTextSize = 24;
  weatherTextSize = 16;
  weatherTextSmSize = 12;
}

const styles = StyleSheet.create({
  tabfooter: {
    backgroundColor: colors.green01,
  },
  wrapper: {
    flex: 1,
    display: "flex",
  },
  facebookButtonIcon: {
    color: colors.white
  },
  header: {
    backgroundColor: colors.green01
  },
  textbody: {
    justifyContent: 'center',
    fontWeight: 'bold',
    color: colors.white,
  },
  homeWrapper: {
    flex: 1,
    display: "flex",
  },
  weatherContent: {
    flexDirection: "row",
    marginBottom: 15,
    borderBottomColor: colors.lightBlack,
    borderBottomWidth: 1,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  newsContent: {
    flexDirection: "row"
  },
  weatherText: {
    fontFamily: "Montserrat-Medium",
    fontSize: weatherTextSize,
    color: colors.lightBlack,
    fontWeight: "300",
    fontSize: headingTextSize,
    marginBottom: 5,
  },
  apdimage: {
    alignItems: 'center',
    width: 160,
    height: 160,
    marginTop: 20,
    marginLeft: 80,
    marginRight: 80,
  },
  weatherTextSm: {
    fontFamily: "Montserrat-Light",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 10
  },
  newsText: {
    fontFamily: "Montserrat-Regular",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 5,
    marginLeft: 20
  },
  news: {
    height: 160,
    marginTop: 20
  },
  search: {
    position: 'absolute',
    left: 10,
    right: 10,
    top: 60,
    flexDirection: 'row',
  },
  stockimage: {
    alignItems: 'center',
    width: 160,
    height: 160,
    marginTop: 70,
    marginLeft: 80,
    marginRight: 80,
  },
  weatherText: {
    fontFamily: "Montserrat-Medium",
    color: colors.lightBlack,
    fontWeight: "300",
    fontSize: headingTextSize,
    marginBottom: 5,
    marginLeft: 50,
    marginRight: 50
  },
  searchItem: {
    backgroundColor: 'white',
    paddingHorizontal: 10,
    borderRadius: 20
  },
  roundedButton: {
    padding: 10,
    justifyContent: 'center',
    top: 70,
    flexDirection: "row"
  },
  inputQty: {
    height: 60,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10,
    borderRadius: 5,
    backgroundColor: 'rgba(255, 255, 255, 0.9)'
  },
  loginButton: {
    margin: 30,
    height: '75%',
  }
});

export default styles;

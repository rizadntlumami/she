import React, { Component } from 'react';
import {
  Alert,
  Platform,
  StyleSheet,
  Text,
  View,
  Image,
  Picker,
  TouchableOpacity,
  Linking,
  AsyncStorage,
  FlatList,
  LayoutAnimation,
  BackHandler
} from 'react-native';

import {
  Button,
  Form,
  Item,
  Label,
  Input,
  Content,
  Thumbnail
} from 'native-base';

import { StackNavigator, NavigationActions } from 'react-navigation';
import colors from "../../../styles/colors";
import QRCodeScanner from 'react-native-qrcode-scanner';
import GlobalConfig from '../../components/GlobalConfig';
import Moment from 'moment';

class InspectionReportAparScan extends Component {
  static navigationOptions = {
        header: null,
  }

  constructor(props) {
    super(props);
    this.state = {
      lastScannedUrl: [],
      dataQR: [],
      idLokasi:'',
    };
  }

  navigateToScreen(route, idLokasi) {
      AsyncStorage.setItem('idLokasi', idLokasi).then(() => {
          this.props.navigation.navigate(route);
      })
  }

  _handleQRCodeRead = result => {
    var qrCode = result.data
    var validasi = qrCode.substring(0, 4)
    if(validasi!='APAR') {
      alert("Apar Tidak Ditemukan")
      this.props.navigation.navigate("InspectionReportApar")
    } else {
        var qr = qrCode.replace("APAR~", "")
        LayoutAnimation.spring();

        const url = GlobalConfig.SERVERHOST + "getLokasiApar";
        var formData = new FormData();
        formData.append("id_apar", qr);

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
          .then(response => response.json())
          .then(responseJson => {
            if(responseJson.data!=null) {
              this.navigateToScreen('InspectionReportAparDetail', qr)
            } else {
              alert("Apar Tidak Ditemukan")
              this.props.navigation.navigate("InspectionReportApar")
            }
        })
        .catch((error)=>{
            console.log(error)
            Alert.alert('Error', 'QR Code Tidak Valid', [{
                text: 'Okay'
            }])
        })
    }
}

  render() {
    return (
      <View style={{flex:1, backgroundColor:colors.black}}>
        <View style={styles.container}>
          <QRCodeScanner onRead={this._handleQRCodeRead}/>
        </View>
        <View>
          <Button
              block
              style={{
                height: 45,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 20,
                borderWidth: 1,
                backgroundColor: "#00b300",
                borderColor: "#00b300",
                borderRadius: 4
              }}
              onPress={() => this.props.navigation.navigate("InspectionReportApar")}
            >
              <Text style={{fontSize: 14, fontWeight: 'bold', color: colors.white}}>Kembali</Text>
          </Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  }
});

export default InspectionReportAparScan;

import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TextInput,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Item
} from "native-base";

import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton
} from "react-native-popup-dialog";
import Ripple from "react-native-material-ripple";
import ImagePicker from "react-native-image-picker";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from 'react-native-datepicker';

class ListItem extends React.PureComponent {
    navigateToScreen(route, id_rusak) {
        // alert(route);
        AsyncStorage.setItem("id", id_rusak).then(() => {
            that.props.navigation.navigate(route);
        });
    }

    render() {
        return (
            <View style={{ backgroundColor: "#FEFEFE" }}>
                <Ripple
                    style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() =>
                        this.props.setSelectedPegLabel(this.props.data)
                    }
                    rippleColor={colors.accent}
                >
                    <CardItem
                        style={{
                            borderRadius: 0,
                            marginTop: 4,
                            backgroundColor: colors.gray
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                marginTop: 4,
                                backgroundColor: colors.gray
                            }}
                        >
                            <View >
                                <Text style={{ fontSize: 12 }}>{this.props.data.mk_nopeg} - {this.props.data.mk_nama}</Text>
                            </View>
                        </View>
                    </CardItem>
                </Ripple>
            </View>
        );
    }
}

export default class UnitCheckUpdateVehicle extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            isLoading: true,
            vehicleType: 'Pick Up',
            headerUnitCheck: [],

            visibleLoadingPegawai: false,
            visibleSearchListPeg: false,
            selectedPegLabel: '',
            searchWordPegawai: '',
        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {
        AsyncStorage.getItem('list').then((headerUnitCheck) => {
            this.setState({
                listHeaderUnit: JSON.parse(headerUnitCheck),
                ID: JSON.parse(headerUnitCheck).ID_VEHICLE,
                vehicleType: JSON.parse(headerUnitCheck).VEHICLE_TYPE,
                vehicleCode: JSON.parse(headerUnitCheck).VEHICLE_CODE,
                vehicleMerk: JSON.parse(headerUnitCheck).MERK,
                vehicleTipe: JSON.parse(headerUnitCheck).TIPE,
                vehicleTahun: JSON.parse(headerUnitCheck).TAHUN,
            });
        })
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {

        });
    }


    UpdateVehicle() {
        if (this.state.vehicleType == null) {
            alert('Masukkan Vehicle Type');
        }
        else if (this.state.vehicleCode == null) {
            alert('Masukkan Kode/Nopol Kendaraan');
        }
        else if (this.state.vehicleMerk == null) {
            alert('Masukkan Merk Kendaraan');
        }
        else if (this.state.vehicleTipe == null) {
            alert('Masukkan Tipe Kendaraan');
        }
        else if (this.state.vehicleTahun == null) {
            alert('Masukkan Tahun Kendaraan');
        }
        else {
            this.setState({
                visibleDialogSubmit: true
            })
            // alert(JSON.stringify(this.state.id));
            AsyncStorage.getItem("token").then(value => {
                const url = GlobalConfig.SERVERHOST + 'api/v_mobile/firesystem/unit_check/vehicle_master/update';
                var formData = new FormData();
                formData.append("token", value);
                formData.append("ID_VEHICLE", this.state.ID);
                formData.append("VEHICLE_TYPE", this.state.vehicleType);
                formData.append("VEHICLE_CODE", this.state.vehicleCode);
                formData.append("MERK", this.state.vehicleMerk);
                formData.append("TIPE", this.state.vehicleTipe);
                formData.append("TAHUN", this.state.vehicleTahun);

                // alert(JSON.stringify(this.state.ID_VEHICLE));

                fetch(url, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },
                    method: 'POST',
                    body: formData
                })
                    .then((response) => response.json())
                    .then((responseJson) => {

                        // alert(responseJson);
                        if (responseJson.Status == 200) {
                            this.setState({
                                visibleDialogSubmit: false
                            })
                            Alert.alert('Success', 'Update Vehicle Success', [{
                                text: 'Okay'
                            }])
                            this.props.navigation.navigate('DashboardUnitCheckPickup')
                        } else {
                            this.setState({
                                visibleDialogSubmit: false
                            })
                            Alert.alert('Error', 'Update Vehicle Failed', [{
                                text: 'Okay'
                            }])
                        }
                    })
                    .catch((error) => {
                        this.setState({
                            visibleDialogSubmit: false
                        })
                        Alert.alert('Error', 'Update Vehicle Failed', [{
                            text: 'Okay'
                        }])
                        console.log(error)
                    })
            })
        }
    }

    onChangePegawai(text) {
        this.setState({
            searchWordPegawai: text
        })
        this.setState({
            visibleLoadingPegawai: true
        })
        AsyncStorage.getItem('token').then((value) => {
            const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/employee';
            var formData = new FormData();
            formData.append("token", value)
            formData.append("search", this.state.searchWordPegawai)
            formData.append("limit", 10)
            formData.append("plant", null)
            fetch(url, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                body: formData
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        visibleLoadingPegawai: false
                    })
                    this.setState({
                        listKaryawan: responseJson,
                        listKaryawanMaster: responseJson,
                        isloading: false
                    });
                })
                .catch((error) => {
                    this.setState({
                        visibleLoadingPegawai: false
                    })
                    console.log(error)
                })
        })
        // this.setState({
        //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
        // })
    }

    onClickSearchPeg() {
        console.log('masuk')
        if (this.state.visibleSearchListPeg) {
            this.setState({
                visibleSearchListPeg: false
            })
        } else {
            this.setState({
                visibleSearchListPeg: true
            })
        }
    }

    setSelectedPegLabel(pegawai) {
        var lahir = new Date(pegawai.mk_tgl_lahir);
        var umurDate = new Date(Date.now() - lahir);
        var umur = Math.abs(umurDate.getUTCFullYear() - 1970)
        this.setState({
            badgeVict: pegawai.mk_nopeg,
            nameVict: pegawai.mk_nama,
            dateInVict: pegawai.mk_tgl_masuk,
            posText: pegawai.mk_emp_subgroup_text,
            addressict: pegawai.mk_alamat_rumah,
            ukVict: pegawai.muk_short,
            ukVictText: pegawai.muk_nama,
            ageVict: umur.toString(),
            visibleSearchListPeg: false,
            selectedStatVict: '1',
            selectedStatVictText: 'Karyawan'
        })
    }

    _renderItem = ({ item }) => <ListItem data={item} setSelectedPegLabel={text => this.setSelectedPegLabel(text)} />;

    render() {
        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("DashboardUnitCheckPickup")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body>
                        <Title style={styles.textbody}>Update Data Vehicle</Title>
                    </Body>
                    {/* <Right/> */}
                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                <View style={{ flex: 1 }}>
                    <Content style={{ marginTop: 0 }}>
                        <View style={{ justifyContent: "center", alignItems: "center" }}>
                            <Text style={{ fontSize: 12, marginTop: 20, color: colors.green01 }}>Vehicle Update {this.state.vehicleCode}</Text>
                        </View>
                        <View style={{ backgroundColor: '#FEFEFE' }}>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Type Vehicle</Text>
                                        <Form underlineColorAndroid='transparent'>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name="ios-arrow-down-outline" />}
                                                style={{ width: 300 }}
                                                placeholder="Type of Vehicle"
                                                placeholderStyle={{ color: "#bfc6ea" }}
                                                placeholderIconColor="#007aff"
                                                selectedValue={this.state.vehicleType}
                                                onValueChange={(itemValue) => this.setState({ vehicleType: itemValue })}>
                                                <Picker.Item label="Pick Up" value="Pick Up" />
                                                <Picker.Item label="PMK" value="PMK" />
                                            </Picker>
                                        </Form>
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Kode Kendaraan / Nopol Kendaraan</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.vehicleCode} placeholder='Type Something ...' onChangeText={(text) => this.setState({ vehicleCode: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Merk Kendaraan</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.vehicleMerk} placeholder='Type Something ...' onChangeText={(text) => this.setState({ vehicleMerk: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Tipe Kendaraan</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.vehicleTipe} placeholder='Type Something ...' onChangeText={(text) => this.setState({ vehicleTipe: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Tahun Kendaraan</Text>
                                    </View>
                                    <View>
                                        <Textarea keyboardType='numeric' style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.vehicleTahun} placeholder='Type Something ...' onChangeText={(text) => this.setState({ vehicleTahun: text })} />
                                    </View>
                                </View>
                            </CardItem>
                        </View>
                        <View style={styles.Contentsave}>
                            <Button
                                block
                                style={{
                                    height: 45,
                                    marginLeft: 20,
                                    marginRight: 20,
                                    marginBottom: 20,
                                    borderWidth: 1,
                                    backgroundColor: "#00b300",
                                    borderColor: "#00b300",
                                    borderRadius: 4
                                }}
                                onPress={() => this.UpdateVehicle()}
                            >
                                <Text style={styles.buttonText}>Simpan</Text>
                            </Button>
                        </View>
                        <View style={{ width: 270, position: "absolute" }}>
                            <Dialog
                                visible={this.state.visibleDialogSubmit}
                                dialogTitle={<DialogTitle title="Updating Vehicle Data .." />}
                            >
                                <DialogContent>
                                    {<ActivityIndicator size="large" color="#330066" animating />}
                                </DialogContent>
                            </Dialog>
                        </View>
                    </Content>

                </View>
                <CustomFooter navigation={this.props.navigation} menu='FireSystem' />
            </Container>

        );
    }
}

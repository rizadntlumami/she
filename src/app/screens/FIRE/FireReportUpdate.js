import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Label,
    Item,
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DateTimePicker from 'react-native-datepicker';
import DatePicker from 'react-native-datepicker';
import ImagePicker from "react-native-image-picker";
import Ripple from "react-native-material-ripple";
import CheckBox from 'react-native-check-box';
import moment from 'moment'
import ListView from "../../components/ListView";

class ListItemPegawai extends React.PureComponent {
    render() {
        return (
            <View style={{ backgroundColor: "#FEFEFE" }}>
                <Ripple
                    style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() =>
                        this.props.setSelectedPegawaiLabel(this.props.data)
                    }
                    rippleColor={colors.accent}
                >
                    <CardItem
                        style={{
                            borderRadius: 0,
                            marginTop: 4,
                            backgroundColor: colors.gray
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                marginTop: 4,
                                backgroundColor: colors.gray
                            }}
                        >
                            <View >
                                <Text style={{ fontSize: 12 }}>{this.props.data.mk_nopeg} - {this.props.data.mk_nama}</Text>
                            </View>
                        </View>
                    </CardItem>
                </Ripple>
            </View>
        );
    }
}

export default class FireReportUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isLoading: true,
      tabNumber:'tab1',
      visibleLoadingPegawai: false,
      visibleSearchListPegawai: false,
      selectedPegawaiLabel: '',
      searchWordPegawai: '',
      pegawaiName: '',
      pegawaiUnitKerja: '',
      tempat:'',
      shift:'',
      pmk:'',
      listPMK:[],
      tembusan:'',
      listTembusan:[],
      tools:'',
      listTools:[],
      isCheckedOther:false,
      id:'',
      date:'',
      time:'',
      timeEnd:'',
      noteTempat:'',
      objekTerbakar:'',
      pelapor:'',
      kerusakan:'',
      bahanTerbakar:'',
      jumlahKorban:'',
      penjelasan:'',
      unsafeCondition:'',
      unsafeAction:'',
      pekerjaan:'',
      fireNumber:'',
      saran:'',
    };
  }

  onChangePegawai(text) {
      this.setState({
          searchWordPegawai: text
      })
      this.setState({
          visibleLoadingPegawai: true
      })
          const url = GlobalConfig.SERVERHOST + 'getEmployee';
          var formData = new FormData();
          formData.append("searchEmp", this.state.searchWordPegawai)
          fetch(url, {
              headers: {
                  'Content-Type': 'multipart/form-data'
              },
              method: 'POST',
              body: formData
          })
              .then((response) => response.json())
              .then((responseJson) => {
                  this.setState({
                      visibleLoadingPegawai: false
                  })
                  this.setState({
                      listPegawai: responseJson.data,
                      listPegawaiMaster: responseJson,
                      isloading: false
                  });
              })
              .catch((error) => {
                  this.setState({
                      visibleLoadingPegawai: false
                  })
                  console.log(error)
              })

  }

  onClickSearchPegawai() {
      console.log('masuk')
      if (this.state.visibleSearchListPegawai) {
          this.setState({
              visibleSearchListPegawai: false
          })
      } else {
          this.setState({
              visibleSearchListPegawai: true
          })
      }
  }

  setSelectedPegawaiLabel(pegawai) {
      this.setState({
          pegawaiBadge: pegawai.mk_nopeg,
          pegawaiName: pegawai.mk_nama,
          pegawaiUnitKerja: pegawai.muk_nama,
          pegawaiKodeUnit: pegawai.muk_kode,
          visibleSearchListPegawai: false,
      })
  }

  _renderItemPegawai= ({ item }) => <ListItemPegawai data={item} setSelectedPegawaiLabel={text => this.setSelectedPegawaiLabel(text)} />;

  static navigationOptions = {
      header: null
  };

  _renderItem = ({ item }) => (
      <ListItem data={item}></ListItem>
  )

  componentDidMount() {
      AsyncStorage.getItem('list').then((listFireReport) => {
          this.setState({
            listFireReport: JSON.parse(listFireReport),
            id: JSON.parse(listFireReport).id,
            fireNumber: JSON.parse(listFireReport).fire_number,
            date: JSON.parse(listFireReport).fire_date,
            time: JSON.parse(listFireReport).fire_time,
            timeEnd: JSON.parse(listFireReport).job_walking_timeend,
            pegawaiBadge: JSON.parse(listFireReport).pic_badge,
            pegawaiName: JSON.parse(listFireReport).pic_name,
            tempat: JSON.parse(listFireReport).incident_place,
            noteTempat: JSON.parse(listFireReport).note_place,
            objekTerbakar: JSON.parse(listFireReport).fire_happened_on,
            kerusakan: JSON.parse(listFireReport).damage_from_fire,
            bahanTerbakar: JSON.parse(listFireReport).fire_material_type,
            jumlahKorban: JSON.parse(listFireReport).number_victims,
            penjelasan: JSON.parse(listFireReport).description_of_fire,
            unsafeCondition: JSON.parse(listFireReport).unsafe_condition,
            unsafeAction: JSON.parse(listFireReport).unsafe_action,
            pekerjaan: JSON.parse(listFireReport).work_on_fire,
            shift: JSON.parse(listFireReport).shift,
            saran: JSON.parse(listFireReport).saran,
          });
          this.setState({
            listTools: JSON.parse(this.state.listFireReport.fire_extinguishers),
            listPMK: JSON.parse(this.state.listFireReport.list_employees),
            listTembusan: JSON.parse(this.state.listFireReport.list_tembusan),
          })
      })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {

      });
  }

  tambahPMK() {
    let pmk = this.state.pmk;
    this.setState({
      pmk: ""
    });
    if (pmk == "") {
    } else {
      this.setState({
        listPMK: [...this.state.listPMK, pmk]
      });
    }
  }

  deletePMK(index) {
    let arr = this.state.listPMK;
    arr.splice(index, 1);
    this.setState({
      listPMK: arr
    });
  }

  tambahTembusan() {
    let tembusan = this.state.tembusan;
    this.setState({
      tembusan: ""
    });
    if (tembusan == "") {
    } else {
      this.setState({
        listTembusan: [...this.state.listTembusan, tembusan]
      });
    }
  }

  deleteTembusan(index) {
    let arr = this.state.listTembusan;
    arr.splice(index, 1);
    this.setState({
      listTembusan: arr
    });
  }

  tambahTools() {
    let tools = this.state.tools;
    this.setState({
      tools: ""
    });
    if (tools == "") {
    } else {
      this.setState({
        listTools: [...this.state.listTools, tools]
      });
    }
  }

  deleteTools(index) {
    let arr = this.state.listTools;
    arr.splice(index, 1);
    this.setState({
      listTools: arr
    });
  }

  validasiFieldTab1(){
    if(this.state.date==null){
      alert("Masukkan Tanggal Kejadian")
    } else if (this.state.time==null){
      alert("Masukkan Jam Kejadian")
    } else if (this.state.pegawaiName==''){
      alert("Masukkan Pengawas")
    } else {
      this.setState({
        tabNumber:'tab2'
      })
    }
    let listTools = JSON.stringify(this.state.listTools);
    let apar = listTools.search('Apar')
    let hydrant = listTools.search('Hydrant')
    let pmk = listTools.search('PMK')
    if(apar != -1){
      this.setState({
        isCheckedApar:true
      })
    }
    if(hydrant != -1){
      this.setState({
        isCheckedHydrant:true
      })
    }
    if(pmk != -1){
      this.setState({
        isCheckedPMK:true
      })
    }
  }

  validasiFieldTab2(){
    let apar = 'Apar';
    let hydrant = 'Hydrant';
    let pmk = 'PMK';

    let listTools = JSON.stringify(this.state.listTools);
    let listApar = listTools.search('Apar')
    let listHydrant = listTools.search('Hydrant')
    let listPMK = listTools.search('PMK')

    if(listApar == -1 && this.state.isCheckedApar==true){
      this.setState({
        listTools: [...this.state.listTools, apar]
      });
    }
    if (listHydrant == -1 && this.state.isCheckedHydrant==true) {
      this.setState({
        listTools: [...this.state.listTools, hydrant]
      });
    }
    if (listPMK == -1 && this.state.isCheckedPMK==true) {
      this.setState({
        listTools: [...this.state.listTools, pmk]
      });
    }

    if(this.state.pekerjaan==null){
      alert("Masukkan Pekerjaan")
    } else if (this.state.timeEnd==null){
      alert("Masukkan Jam Selesai Kebakaran")
    } else if (this.state.pelapor==null){
      alert("Masukkan Nama Pelapor")
    } else if (this.state.tempat==null){
      alert("Masukkan Tempat Kejadian")
    } else if (this.state.objekTerbakar==null){
      alert("Masukkan Objek Yang Terbakar")
    } else if (this.state.bahanTerbakar==null){
      alert("Masukkan Bahan Yang Terbakar")
    } else if (this.state.kerusakan==null){
      alert("Masukkan Kerusakan")
    }
    else {
      this.setState({
        tabNumber:'tab3'
      })
    }
  }

  validasiUpdate(){
    if(this.state.unsafeCondition==null) {
      alert("Masukkan Unsafe Condition")
    } else {
      this.fireReportUpdate();
    }
  }

  fireReportUpdate(){
      var url = GlobalConfig.SERVERHOST + "updateFireReport";
      var formData = new FormData();
      formData.append("id", this.state.id);
      formData.append("fire_date", this.state.date);
      formData.append("fire_time", this.state.time);
      formData.append("job_walking_timestart", this.state.time);
      formData.append("job_walking_timeend", this.state.timeEnd);
      formData.append("pic_badge", this.state.pegawaiBadge);
      formData.append("pic_name", this.state.pegawaiName);
      formData.append("incident_place", this.state.tempat);
      if (this.state.noteTempat!='') {
        formData.append("note_place", this.state.noteTempat);
      }
      formData.append("fire_happened_on", this.state.objekTerbakar);
      formData.append("damage_from_fire", this.state.kerusakan);
      formData.append("fire_material_type", this.state.bahanTerbakar);
      if (this.state.jumlahKorban!='') {
        formData.append("number_victims", this.state.jumlahKorban);
      }
      if (this.state.penjelasan!='') {
        formData.append("description_of_fire", this.state.penjelasan);
      }
      formData.append("unsafe_condition", this.state.unsafeCondition);
      if (this.state.unsafeAction!='') {
        formData.append("unsafe_action", this.state.unsafeAction);
      }
      if (this.state.listPMK!='') {
        formData.append("list_employees", JSON.stringify(this.state.listPMK));
      }
      //formData.append("NOTE", this.state.keterangan);
      formData.append("work_on_fire", this.state.pekerjaan);
      if (this.state.listTools!='') {
        formData.append("fire_extinguishers", JSON.stringify(this.state.listTools));
      }
      formData.append("shift", this.state.shift);
      if (this.state.listTembusan!='') {
        formData.append("list_tembusan", JSON.stringify(this.state.listTembusan));
      }
      if (this.state.saran!='') {
        formData.append("saran", this.state.saran);
      }

      formData.append("fire_number", this.state.fireNumber);
      formData.append("company", 5000);
      formData.append("company", 5000);
      formData.append("plant", 5001);
      formData.append("status", 'OPEN');

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        if (response.status == 200) {
          Alert.alert('Success', 'Update Fire Report Success', [{
            text: 'Okay'
          }])
          this.props.navigation.navigate('FireReportDetail')
        } else {
          Alert.alert('Error', 'Update Fire Report Failed', [{
            text: 'Okay'
          }])
        }
      });
  }

  render() {
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            {this.state.tabNumber=='tab1' ? (
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("FireReportDetail")}
              >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          ):this.state.tabNumber=='tab2' ? (
              <Button
                transparent
                onPress={() => this.setState({
                  tabNumber:'tab1'
                })}
              >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            ):(
              <Button
                transparent
                onPress={() => this.setState({
                  tabNumber:'tab2'
                })}
              >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          )}
          </Left>
          <Body style={{ flex:3, alignItems:'center' }}>
            <Title style={styles.textbody}>Update {this.state.fireNumber}</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={{ flex: 1 }}>
        <Content style={{ marginTop: 0, backgroundColor:colors.gray }}>
          <View style={{ backgroundColor:colors.gray }}>
              {this.state.tabNumber=='tab1' ? (
              <View>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                  <View style={{ flex: 1, flexDirection: 'row'}}>
                    <View style={{width:'33%', alignItems:'center'}}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{color: colors.green01, fontSize: 20, paddingLeft:8}}
                        />
                        <Text style={{fontSize:10, fontWeight:'bold'}}>Waktu Kejadian</Text>
                    </View>
                    <View style={{width:'33%', alignItems:'center'}}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{color:colors.graydar, fontSize: 20, paddingLeft:8}}
                        />
                        <Text style={{fontSize:10, fontWeight:'bold', color:colors.graydar}}>Lokasi Kejadian</Text>
                    </View>
                    <View style={{width:'33%', alignItems:'center'}}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{color:colors.graydar, fontSize: 20, paddingLeft:8}}
                        />
                        <Text style={{fontSize:10, fontWeight:'bold', color:colors.graydar}}>Sebab Kejadian</Text>
                    </View>
                  </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                  <View style={{ flex: 1, flexDirection: 'row'}}>
                    <View style={{width: '50%'}}>
                      <Text style={styles.titleInput}>Tanggal Kejadian *</Text>
                      <DatePicker
                          style={{ width: '100%', fontSize:10, borderRadius:20}}
                          date={this.state.date}
                          mode="date"
                          placeholder="Choose Date ..."
                          format="YYYY-MM-DD"
                          minDate="2018-01-01"
                          maxDate="2050-12-31"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          iconSource='{this.timeIcon}'
                          customStyles={{
                            dateInput: {
                              marginLeft: 5, marginRight:5, height: 35, borderRadius:5, fontSize:10, borderWidth:1, borderColor:"#E6E6E6"
                            },
                            dateIcon: {
                              position: 'absolute',
                              left: 0,
                              top: 5,
                            },
                          }}
                          onDateChange={(time) => { this.setState({ date: time }) }}
                      />
                    </View>
                    <View style={{width: '50%'}}>
                    <Text style={styles.titleInput}>Jam Kejadian *</Text>
                      <DatePicker
                          style={{ width: "100%", fontSize: 10, borderRadius: 20 }}
                          date={this.state.time}
                          mode="time"
                          placeholder="Choose Time ..."
                          format="hh:mm"
                          minTime="00:00"
                          maxTime="23:59"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          iconSource='{this.timeIcon}'
                          customStyles={{
                            dateInput: {
                              marginLeft: 5,
                              marginRight: 5,
                              height: 35,
                              borderRadius: 5,
                              fontSize: 10,
                              borderWidth: 1,
                              borderColor: "#E6E6E6"
                            },
                            dateIcon: {
                              position: "absolute",
                              left: 0,
                              top: 5
                            }
                          }}
                          onDateChange={time => {
                            this.setState({ time: time });
                          }}
                      />
                    </View>
                  </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                  <View style={{ flex: 1 }}>
                    <View>
                      <Text style={styles.titleInput}>Shift *</Text>
                      <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                              style={{ width: '100%', height:35}}
                              placeholder="Select Shift ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.shift}
                              onValueChange={(itemValue) => this.setState({shift:itemValue})}>
                              <Picker.Item label="Shift I (Satu)"  value="1" />
                              <Picker.Item label="Shift II (Dua)"  value="2"/>
                              <Picker.Item label="Shift III (Tiga)"  value="3"/>
                          </Picker>
                      </Form>
                    </View>
                  </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                    <View style={{ flex: 1 }}>
                        <View>
                            <Text style={styles.titleInput}>Pengawas *</Text>
                        </View>
                        <View style={{ flex: 1, flexDirection: 'column' }}>
                            <Button
                                block
                                style={{ justifyContent: "flex-start", flex: 1, borderWidth: Platform.OS==='ios'?1:0, borderColor: Platform.OS==='ios'?colors.lightGray:null, paddingLeft: 10, marginRight: 5, marginLeft: 5, backgroundColor: colors.gray, height: 35 }}
                                onPress={() => this.onClickSearchPegawai()}>
                                <Text style={{ fontSize: 12 }}>{this.state.pegawaiBadge} - {this.state.pegawaiName}</Text>
                            </Button>
                        </View>
                        {this.state.visibleSearchListPegawai &&
                            <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.gray, margin: 5, borderColor: "#E6E6E6", }}>
                                <View>
                                    <Textarea value={this.state.searchWordPegawai} style={{ marginLeft: 5, marginRight: 5, fontSize: 11 }} bordered rowSpan={1.5} onChangeText={(text) => this.onChangePegawai(text)} placeholder='Ketik nama karyawan' />
                                </View>
                                <FlatList
                                    data={this.state.listPegawai}
                                    renderItem={this._renderItemPegawai}
                                    keyExtractor={(item, index) => index.toString()}
                                />
                            </View>
                        }
                    </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                  <View style={{ flex: 1}}>
                  <View style={styles.Contentsave}>
                    <Button
                      block
                      style={{
                        width:'100%',
                        height: 45,
                        marginBottom: 20,
                        borderWidth: 1,
                        backgroundColor: "#00b300",
                        borderColor: "#00b300",
                        borderRadius: 4
                      }}
                      onPress={() => this.validasiFieldTab1()}
                    >
                      <Text style={{color:colors.white}}>Lanjutkan</Text>
                    </Button>
                  </View>
                  </View>
                </CardItem>
              </View>


              ):this.state.tabNumber=='tab2' ? (


              <View>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                <View style={{ flex: 1, flexDirection: 'row'}}>
                  <View style={{width:'33%', alignItems:'center'}}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{color: colors.green01, fontSize: 20, paddingLeft:8}}
                      />
                      <Text style={{fontSize:10, fontWeight:'bold'}}>Waktu Kejadian</Text>
                  </View>
                  <View style={{width:'33%', alignItems:'center'}}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{color: colors.green01, fontSize: 20, paddingLeft:8}}
                      />
                      <Text style={{fontSize:10, fontWeight:'bold'}}>Lokasi Kejadian</Text>
                  </View>
                  <View style={{width:'33%', alignItems:'center'}}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{color: colors.graydar, fontSize: 20, paddingLeft:8}}
                      />
                      <Text style={{fontSize:10, fontWeight:'bold', color:colors.graydar}}>Sebab Kejadian</Text>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                  <View style={{ flex: 1 }}>
                      <View>
                          <Text style={styles.titleInput}>Pekerjaan Sewaktu Terjadi Kebakaran *</Text>
                      </View>
                      <View>
                          <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={2}
                              bordered value={this.state.pekerjaan}
                              placeholder='Type something here ...'
                              onChangeText={(text) => this.setState({ pekerjaan: text })} />
                      </View>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                <View style={{ flex: 1, flexDirection: 'row'}}>
                  <View style={{width: '50%'}}>
                    <Text style={styles.titleInput}>Jam Mulai Kebakaran *</Text>
                    <DatePicker
                        style={{ width: '100%', fontSize:10, borderRadius:20}}
                        date={this.state.time}
                        mode="date"
                        placeholder="Choose Time ..."
                        format="hh:mm"
                        minTime="00:00"
                        maxTime="23:59"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource='{this.timeIcon}'
                        customStyles={{
                          dateInput: {
                            marginLeft: 5, marginRight:5, height: 35, borderRadius:5, fontSize:10, borderWidth:1, borderColor:"#E6E6E6"
                          },
                          dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 5,
                          },
                        }}
                        onDateChange={(time) => { this.setState({ time: time }) }}
                    />
                  </View>
                  <View style={{width: '50%'}}>
                  <Text style={styles.titleInput}>Jam Selesai Kebakaran *</Text>
                    <DatePicker
                        style={{ width: "100%", fontSize: 10, borderRadius: 20 }}
                        date={this.state.timeEnd}
                        mode="time"
                        placeholder="Choose Time ..."
                        format="hh:mm"
                        minTime="00:00"
                        maxTime="23:59"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource='{this.timeIcon}'
                        customStyles={{
                          dateInput: {
                            marginLeft: 5,
                            marginRight: 5,
                            height: 35,
                            borderRadius: 5,
                            fontSize: 10,
                            borderWidth: 1,
                            borderColor: "#E6E6E6"
                          },
                          dateIcon: {
                            position: "absolute",
                            left: 0,
                            top: 5
                          }
                        }}
                        onDateChange={time => {
                          this.setState({ timeEnd: time });
                        }}
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                <View style={{flex:1}}>
                  <View>
                  <Text style={styles.titleInput}>Alat Pemadam Yang Digunakan</Text>
                  </View>
                <View style={{ flex: 1, flexDirection: 'row'}}>
                  <View style={{width: '22%'}}>
                    <CheckBox
                      style={{flex:1, paddingTop:0}}
                      onClick={()=>{
                        this.setState({
                          isCheckedApar:!this.state.isCheckedApar,
                        })
                      }}
                      isChecked={this.state.isCheckedApar}
                      rightText={"Apar"}
                    />
                  </View>
                  <View style={{width: '30%'}}>
                    <CheckBox
                      style={{flex:1, paddingTop:0}}
                      onClick={()=>{
                        this.setState({
                          isCheckedHydrant:!this.state.isCheckedHydrant,
                        })
                      }}
                      isChecked={this.state.isCheckedHydrant}
                      rightText={"Hydrant"}
                    />
                  </View>
                  <View style={{width: '23%'}}>
                    <CheckBox
                      style={{flex:1, paddingTop:0}}
                      onClick={()=>{
                        this.setState({
                          isCheckedPMK:!this.state.isCheckedPMK,
                        })
                      }}
                      isChecked={this.state.isCheckedPMK}
                      rightText={"PMK"}
                    />
                  </View>
                  <View style={{width: '25%'}}>
                    <CheckBox
                      style={{flex:1, paddingTop:0}}
                      onClick={()=>{
                        this.setState({
                          isCheckedOther:!this.state.isCheckedOther,
                        })
                      }}
                      isChecked={this.state.isCheckedOther}
                      rightText={"Lainnya"}
                    />
                  </View>
                </View></View>
              </CardItem>
              {this.state.isCheckedOther == true ? (
                <CardItem style={{borderRadius: 0, marginTop: 0, backgroundColor: colors.gray}}>
                  <View style={{ flex: 1 }}>
                    <View>
                      <Text style={styles.titleInput}>Tools</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: 280 }}>
                        <Textarea
                          style={{ borderRadius: 5, marginLeft: 5, fontSize: 10 }}
                          rowSpan={1.5}
                          bordered
                          value={this.state.tools}
                          placeholder="Tambah Tools ..."
                          onChangeText={text => this.setState({ tools: text })}
                        />
                      </View>
                      <Button
                        onPress={() => this.tambahTools()}
                        style={styles.btnTambah}
                      >
                        <Icon name="ios-add" style={styles.iconPluss} />
                      </Button>
                    </View>
                  </View>
                </CardItem>
              ):(
                <View></View>
              )}
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                {this.state.listTools!=null &&(
                  <View>
                  {this.state.listTools.map((listTools, index) => (
                    <View key={index} style={{ backgroundColor: "#FEFEFE" }}>
                      <View
                        style={{marginTop: 5, marginBottom: 5, marginLeft: 10, marginRight: 10, height: 30, backgroundColor: colors.gray, borderRadius: 5
                        }}>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <View style={{ width: 280 }}>
                            <Text
                              style={{ paddingLeft: 10, fontSize: 11, paddingTop: 5}}
                            >
                              {listTools}
                            </Text>
                          </View>
                          <View>
                            <Ripple
                              style={{
                                flex: 2,
                                justifyContent: "center",
                                alignItems: "center"
                              }}
                              rippleSize={176}
                              rippleDuration={600}
                              rippleContainerBorderRadius={15}
                              onPress={() => this.deleteTools(index)}
                              rippleColor={colors.accent}
                            >
                              <Icon name="ios-close" style={styles.iconClose} />
                            </Ripple>
                          </View>
                        </View>
                      </View>
                    </View>
                  ))}
                  </View>
                )}
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                  <View style={{ flex: 1 }}>
                      <View>
                          <Text style={styles.titleInput}>Penjelasan </Text>
                      </View>
                      <View>
                          <Textarea style={{marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={3}
                              bordered value={this.state.penjelasan}
                              placeholder='Type something here ...'
                              onChangeText={(text) => this.setState({ penjelasan: text })} />
                      </View>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                  <View style={{ flex: 1 }}>
                      <View>
                          <Text style={styles.titleInput}>Pelapor *</Text>
                      </View>
                      <View>
                          <Textarea style={{marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={2}
                              bordered value={this.state.pelapor}
                              placeholder='Nama Pelapor'
                              onChangeText={(text) => this.setState({ pelapor: text })} />
                      </View>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                  <View>
                    <Text style={styles.titleInput}>Tempat Kejadian *</Text>
                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                            style={{ width: '100%', height: 35}}
                            placeholder="Select Lisensi ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.tempat}
                            onValueChange={(itemValue) => this.setState({tempat:itemValue})}>
                            <Picker.Item label="Dalam Pabrik" value="Dalam Pabrik" />
                            <Picker.Item label="Luar Pabrik" value="Luar Pabrik" />
                            <Picker.Item label="Proyek" value="Proyek" />
                        </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                  <View style={{ flex: 1 }}>
                      <View>
                          <Text style={styles.titleInput}>Jumlah Korban</Text>
                      </View>
                      <View>
                          <Textarea style={{marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={2}
                              bordered value={this.state.jumlahKorban}
                              placeholder='Jumlah Korban'
                              keyboardType='numeric'
                              onChangeText={(text) => this.setState({ jumlahKorban: text })} />
                      </View>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                  <View style={{ flex: 1 }}>
                      <View>
                          <Text style={styles.titleInput}>Keterangan Tempat Kejadian</Text>
                      </View>
                      <View>
                          <Textarea style={{marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={3}
                              bordered value={this.state.noteTempat}
                              placeholder='Type something here ...'
                              onChangeText={(text) => this.setState({ noteTempat: text })} />
                      </View>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                  <View style={{ flex: 1 }}>
                      <View>
                          <Text style={styles.titleInput}>Objek Yang Terbakar *</Text>
                      </View>
                      <View>
                          <Textarea style={{marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={2}
                              bordered value={this.state.objekTerbakar}
                              placeholder='Objek Yang Terbakar'
                              onChangeText={(text) => this.setState({ objekTerbakar: text })} />
                      </View>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                  <View style={{ flex: 1 }}>
                      <View>
                          <Text style={styles.titleInput}>Jenis Bahan Yang Terbakar *</Text>
                      </View>
                      <View>
                          <Textarea style={{marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={2}
                              bordered value={this.state.bahanTerbakar}
                              placeholder='Jenis Bahan Yang Terbakar'
                              onChangeText={(text) => this.setState({ bahanTerbakar: text })} />
                      </View>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                  <View style={{ flex: 1 }}>
                      <View>
                          <Text style={styles.titleInput}>Kerusakan Yang Ditimbulkan *</Text>
                      </View>
                      <View>
                          <Textarea style={{marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={3}
                              bordered value={this.state.kerusakan}
                              placeholder='Kerusakan Yang Timbul'
                              onChangeText={(text) => this.setState({ kerusakan: text })} />
                      </View>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                <View style={{ flex: 1}}>
                <View style={styles.Contentsave}>
                  <Button
                    block
                    style={{
                      width:'100%',
                      height: 45,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: "#00b300",
                      borderColor: "#00b300",
                      borderRadius: 4
                    }}
                    onPress={() => this.validasiFieldTab2()}
                  >
                    <Text style={{color:colors.white}}>Lanjutkan</Text>
                  </Button>
                </View>
                </View>
              </CardItem>
              </View>


            ):(


              <View>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                <View style={{ flex: 1, flexDirection: 'row'}}>
                  <View style={{width:'33%', alignItems:'center'}}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{color: colors.green01, fontSize: 20, paddingLeft:8}}
                      />
                      <Text style={{fontSize:10, fontWeight:'bold'}}>Waktu Kejadian</Text>
                  </View>
                  <View style={{width:'33%', alignItems:'center'}}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{color: colors.green01, fontSize: 20, paddingLeft:8}}
                      />
                      <Text style={{fontSize:10, fontWeight:'bold'}}>Lokasi Kejadian</Text>
                  </View>
                  <View style={{width:'33%', alignItems:'center'}}>
                      <Icon
                        name="ios-radio-button-on"
                        size={20}
                        style={{color: colors.green01, fontSize: 20, paddingLeft:8}}
                      />
                      <Text style={{fontSize:10, fontWeight:'bold'}}>Sebab Kejadian</Text>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                  <View style={{ flex: 1 }}>
                      <View>
                          <Text style={styles.titleInput}>Unsafe Condition *</Text>
                      </View>
                      <View>
                          <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={3}
                              bordered value={this.state.unsafeCondition}
                              placeholder='Type something here ...'
                              onChangeText={(text) => this.setState({ unsafeCondition: text })} />
                      </View>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                  <View style={{ flex: 1 }}>
                      <View>
                          <Text style={styles.titleInput}>Unsafe Action</Text>
                      </View>
                      <View>
                          <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={3}
                              bordered value={this.state.unsafeAction}
                              placeholder='Type something here ...'
                              onChangeText={(text) => this.setState({ unsafeAction: text })} />
                      </View>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                  <View style={{ flex: 1 }}>
                      <View>
                          <Text style={styles.titleInput}>Saran</Text>
                      </View>
                      <View>
                          <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, borderRadius: 5, fontSize: 11 }}
                              rowSpan={3}
                              bordered value={this.state.saran}
                              placeholder='Type something here ...'
                              onChangeText={(text) => this.setState({ saran: text })} />
                      </View>
                  </View>
              </CardItem>
              <CardItem style={{borderRadius: 0, marginTop: 0, backgroundColor: colors.gray}}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Anggota PMK</Text>
                  </View>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ width: 280 }}>
                      <Textarea
                        style={{ borderRadius: 5, marginLeft: 5, fontSize: 10 }}
                        rowSpan={1.5}
                        bordered
                        value={this.state.pmk}
                        placeholder="Anggota PMK ..."
                        onChangeText={text => this.setState({ pmk: text })}
                      />
                    </View>
                    <Button
                      onPress={() => this.tambahPMK()}
                      style={styles.btnTambah}
                    >
                      <Icon name="ios-add" style={styles.iconPluss} />
                    </Button>
                  </View>
                </View>
              </CardItem>
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                {this.state.listPMK!=null &&(
                  <View>
                  {this.state.listPMK.map((listPMK, index) => (
                    <View key={index} style={{ backgroundColor: "#FEFEFE" }}>
                      <View
                        style={{marginTop: 5, marginBottom: 5, marginLeft: 10, marginRight: 10, height: 30, backgroundColor: colors.gray, borderRadius: 5
                        }}>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <View style={{ width: 280 }}>
                            <Text
                              style={{ paddingLeft: 10, fontSize: 11, paddingTop: 5}}
                            >
                              {listPMK}
                            </Text>
                          </View>
                          <View>
                            <Ripple
                              style={{
                                flex: 2,
                                justifyContent: "center",
                                alignItems: "center"
                              }}
                              rippleSize={176}
                              rippleDuration={600}
                              rippleContainerBorderRadius={15}
                              onPress={() => this.deletePMK(index)}
                              rippleColor={colors.accent}
                            >
                              <Icon name="ios-close" style={styles.iconClose} />
                            </Ripple>
                          </View>
                        </View>
                      </View>
                    </View>
                  ))}
                  </View>
                )}
                </View>
              </CardItem>
              <CardItem style={{borderRadius: 0, marginTop: 0, backgroundColor: colors.gray}}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Tembusan Surat</Text>
                  </View>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ width: 280 }}>
                      <Textarea
                        style={{ borderRadius: 5, marginLeft: 5, fontSize: 10 }}
                        rowSpan={1.5}
                        bordered
                        value={this.state.tembusan}
                        placeholder="Tembusan Surat ..."
                        onChangeText={text => this.setState({ tembusan: text })}
                      />
                    </View>
                    <Button
                      onPress={() => this.tambahTembusan()}
                      style={styles.btnTambah}
                    >
                      <Icon name="ios-add" style={styles.iconPluss} />
                    </Button>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                {this.state.listTembusan!=null &&(
                  <View>
                  {this.state.listTembusan.map((listTembusan, index) => (
                    <View key={index} style={{ backgroundColor: "#FEFEFE" }}>
                      <View
                        style={{marginTop: 5, marginBottom: 5, marginLeft: 10, marginRight: 10, height: 30, backgroundColor: colors.gray, borderRadius: 5
                        }}>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <View style={{ width: 280 }}>
                            <Text
                              style={{ paddingLeft: 10, fontSize: 11, paddingTop: 5}}
                            >
                              {listTembusan}
                            </Text>
                          </View>
                          <View>
                            <Ripple
                              style={{
                                flex: 2,
                                justifyContent: "center",
                                alignItems: "center"
                              }}
                              rippleSize={176}
                              rippleDuration={600}
                              rippleContainerBorderRadius={15}
                              onPress={() => this.deleteTembusan(index)}
                              rippleColor={colors.accent}
                            >
                              <Icon name="ios-close" style={styles.iconClose} />
                            </Ripple>
                          </View>
                        </View>
                      </View>
                    </View>
                  ))}
                  </View>
                )}

                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                <View style={{ flex: 1}}>
                <View style={styles.Contentsave}>
                  <Button
                    block
                    style={{
                      width:'100%',
                      height: 45,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: "#00b300",
                      borderColor: "#00b300",
                      borderRadius: 4
                    }}
                    onPress={() => this.validasiUpdate()}
                  >
                    <Text style={{color:colors.white}}>SUBMIT</Text>
                  </Button>
                </View>
                </View>
              </CardItem>
              </View>
            )}
          </View>
        </Content>
      </View>
      <View style={{ width: 270, position: "absolute" }}>
          <Dialog visible={this.state.visibleLoadingUnitKerja}>
            <DialogContent>
              {
                <ActivityIndicator size="large" color="#330066" animating />
              }
            </DialogContent>
          </Dialog>
      </View>
      <CustomFooter navigation={this.props.navigation} menu='FireSystem' />
    </Container>

    );
  }
}

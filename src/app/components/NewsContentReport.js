import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback ,Platform,AsyncStorage  } from "react-native";
import Ripple from "react-native-material-ripple";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    height: 100,
    width: 100,
    marginLeft: 3,
    marginRight:3,
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class NewsContentReport extends Component {
  navigateToScreen(route, id_Unsafe, shift) {
    var idAndShift = id_Unsafe + '+' + shift;
    console.log(idAndShift)
    AsyncStorage.setItem('idAndShift', idAndShift).then(() => {
        this.props.navigation.navigate(route);
    })
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
        <Ripple
          // style={this.props.clicked ? {
          //   flex: 2,
          //   justifyContent: "center",
          //   alignItems: "center",
          //   borderRadius: 10,
          //   elevation: 1,
          //   borderColor: "#27ae60", borderWidth: 5//j try
          // }: {
          //   flex: 2,
          //   justifyContent: "center",
          //   alignItems: "center",
          //   borderRadius: 10,
          //   elevation: 1,
          // }}
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 5,
            elevation: 1,
            borderWidth:Platform.OS==='ios'?1:0,
    borderColor:(Platform.OS==='ios')?colors.lightGray:null
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          rippleColor={colors.accent}
        >
          <Image
            source={this.props.imageUri}
            style={{ marginTop:10, width: 50, height: 50, resizeMode: "contain" }}
          />
          <View style={{ flex: 1, paddingTop: 3, alignItems: "center" }}>
            <Text style={{fontSize: 9, fontWeight:'bold'}}>{this.props.inspector}</Text>
          </View>
          <View style={{ flex: 1, paddingTop: 3, alignItems: "center" }}>
            <Text style={{fontSize: 9}}>{this.props.date}</Text>
          </View>
        </Ripple>
      </View>
    );
  }
}

NewsContentReport.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};

import React, { Component } from "react";
import {
  Text,
  Image,
  StatusBar,
  KeyboardAvoidingView,
  ScrollView,
  View,
  TextInput,
  AsyncStorage,
  ActivityIndicator,
  Alert,
  BackHandler,
  Platform,
  DeviceEventEmitter
} from "react-native";
import {
  Container,
  Header,
  Form,
  Item,
  Label,
  Input,
  Footer,
  Left,
  Right,
  Button,
  Body,
  Title,
  Icon
  // Toast
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton,
} from "react-native-popup-dialog";
import Icon2 from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";

import styles from "./styles/Login";
import colors from "../../styles/colors";
import RoundedButton from "../components/RoundedButton";
import GlobalConfig from '../components/GlobalConfig';

const shelogo = require("../../assets/images/shelogo.png");
const silogo = require("../../assets/images/Logo-Semen-Indonesia.png");
const grlogo = require("../../assets/images/Logo-Semen-Gresik.png");
const pdlogo = require("../../assets/images/Logo-Semen-Padang.png");
const tnlogo = require("../../assets/images/Logo-Semen-Tonasa.png");

export default class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "sumarji@semenindonesia.com",
      // email: "erfanti.qodarsih@semenindonesia.com",
      password: "semenindonesia",
      visibleDialogSubmit: false,
    };
  }

  componentDidMount() {

  }

  static navigationOptions = {
    header: null
  };

  konfirmasiLogin(){
    if(this.state.email==""){
      Alert.alert(
        'Information',
        'Input Email',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
      this.setState({
        visibleDialogSubmit:false
      })
    }
    else if(this.state.password==""){
      Alert.alert(
        'Information',
        'Input Password',
        [
          { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        ],
        { cancelable: false }
      );
      this.setState({
        visibleDialogSubmit:false
      })
    } else {
      var url = GlobalConfig.SERVERHOST + 'login';
      var formData = new FormData();
      formData.append("email", this.state.email)
      formData.append("password", this.state.password)

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'POST',
        body: formData
      }).then((response) => response.json())
        .then((response) => {
            if(response.response_code == 200) {
              AsyncStorage.setItem('token', response.token).then(() => {
                this.props.navigation.navigate("HomeMenu");
              })
              this.setState({
                visibleDialogSubmit:false
              })
            } else if (response.response_code == 401) {
                Alert.alert(
                  'Login',
                  'Email Not Found',
                  [
                    { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                  ],
                  { cancelable: false }
                );
                this.setState({
                  visibleDialogSubmit:false
                })
            } else {
                Alert.alert(
                  'Login',
                  'Password Salah',
                  [
                    { text: 'OK', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
                  ],
                  { cancelable: false }
                );
                this.setState({
                  visibleDialogSubmit:false
                })
            }
        })
        .catch((error) => {
          Alert.alert('Cannot Log in', 'Check Your Internet Connection', [{
            text: 'Ok'
          }])
          this.setState({
            visibleDialogSubmit:false
          })
          console.log(error)
        })
    }
  }

  onLoginPress = () => {
    this.setState({
      visibleDialogSubmit: true
    })
    this.konfirmasiLogin()
  };

  exitApp(){
    Alert.alert(
      'Confirmation',
      'Exit SHE Mobile?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => BackHandler.exitApp() },
      ],
      { cancelable: false }
    );
  }

  render() {
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left>
            <Button transparent onPress={() => this.props.navigation.navigate("LoggedOut")}>
              <Icon2
                name="chevron-left"
                size={20}
                style={styles.facebookButtonIcon}
              />
            </Button>
          </Left>
          <Body />
          <Right>
          {Platform.OS!='ios'&&(
            <Button transparent onPress={() => this.exitApp()}>
              <Icon2
                name="close"
                size={20}
                style={styles.facebookButtonIcon}
              />
            </Button>
          )}
          </Right>
        </Header>
        <LinearGradient
          colors={[colors.green03, colors.blue02, colors.whiteBlue]}
          style={styles.wrapper}
        >
          <KeyboardAvoidingView style={styles.wrapper} behavior="padding">
            <StatusBar
              backgroundColor={colors.green03}
              barStyle="light-content"
            />
            <View style={styles.scrollViewWrapper}>
              <ScrollView style={styles.scrollView}>
                <Text style={styles.LoginText}>{"Log in"}</Text>
                <Form>
                  <Item stackedLabel style={{ marginLeft: 0 }}>
                    <Label style={styles.label}>Email</Label>
                    <Input returnKeyType='next' style={styles.input} value={this.state.email} onChangeText={(text) => this.setState({ email: text })} />
                  </Item>
                  <Item stackedLabel last style={styles.inputItem}>
                    <Label style={styles.label}>Password</Label>
                    <Input returnKeyType='go' style={styles.input} secureTextEntry={true} value={this.state.password} onChangeText={(text) => this.setState({ password: text })} />
                  </Item>
                  <RoundedButton
                    text="Login"
                    textColor={colors.white}
                    background={colors.green01}
                    handleOnPress={this.onLoginPress}
                    style={styles.loginButton}
                  />
                </Form>
                <Text>{"\n"}</Text>
              </ScrollView>
              <View style={{ width: 270, position: "absolute" }}>
                <Dialog
                  visible={this.state.visibleDialogSubmit}
                >
                  <DialogContent>
                  {
                    <View>
                      <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                        <Text style={{fontSize:20, alignItems:'center', color:colors.black}}>Loading ...</Text>
                      </View>
                      <ActivityIndicator size="large" color="#330066" animating />
                    </View>
                  }
                  </DialogContent>
                </Dialog>
              </View>
            </View>
          </KeyboardAvoidingView>
        </LinearGradient>
        <Footer style={styles.footer}>
          <Image source={silogo} style={styles.footerLogoSI} />
          <Image source={grlogo} style={styles.footerLogo} />
          <Image source={pdlogo} style={styles.footerLogo} />
          <Image source={tnlogo} style={styles.footerLogo} />
        </Footer>
      </Container>
    );
  }
}

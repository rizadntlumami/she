import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TextInput,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Item,
    Switch
} from "native-base";

import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton
  } from "react-native-popup-dialog";
import ImagePicker from "react-native-image-picker";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from 'react-native-datepicker';
import Ripple from "react-native-material-ripple";
import Moment from 'moment'
import SwitchToggle from 'react-native-switch-toggle';

class ListItemApar extends React.PureComponent {
    render() {
        return (
            <View style={{ backgroundColor: "#FEFEFE" }}>
                <Ripple
                    style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() =>
                        this.props.setselectedAparLabel(this.props.data)
                    }
                    rippleColor={colors.accent}
                >
                    <CardItem
                        style={{
                            borderRadius: 0,
                            marginTop: 4,
                            backgroundColor: colors.gray
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                marginTop: 4,
                                backgroundColor: colors.gray
                            }}
                        >
                            <View >
                                <Text style={{ fontSize: 12 }}>{this.props.data.RNUM} - {this.props.data.SUBTYPE_APAR_TEXT} {this.props.data.TYPE_APAR_TEXT}</Text>
                            </View>
                        </View>
                    </CardItem>
                </Ripple>
            </View>
        );
    }
}

export default class InspectionReportAparUpdate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            dataHeader: [],
            isLoading: true,
            listReportApar:[],
            klem: 'K',
            saran: '',
            startDate:'',
            endDate: '',
            kondisi: '',
            jumlahPress:0,
            jumlahWeight:0,
            hose:false,
            seal:false,
            sapot:false,
            kondisiHose:'',
            kondisiSeal:'',
            kondisiSapot:'',
            informasi:'',
            note:'',
            listApar:[],
            aparText: '',
            subAparText:'',
            rnumApar:'',
            visibleLoadingApar: false,
            visibleSearchListApar: false,
            selectedAparLabel: '',
            searchWordApar: '',
            pegawaiUnitKerja: '',
            idApar:'',
            visibleDialogSubmit:false,
            dataLokasi:'',
            id:'',
            picName:'',
            picBadge:'',
            inspectionDate:'',
        };
    }

    static navigationOptions = {
        header: null
    };

    ubahPress(jumlah) {
        let arr = this.state.jumlahPress;
        arr = jumlah;
        this.setState({
          jumlahPress: arr
        });
      }

      tambahPress() {
        let arr = this.state.jumlahPress;
        arr++;
        this.setState({
          jumlahPress: arr
        });

        console.log(this.state.jumlahPress);
      }

      kurangPress() {
        let arr = this.state.jumlahPress;
        if (arr == 0 || arr == null || arr == undefined) {
        } else {
          arr--;
          this.setState({
            jumlahPress: arr
          });
        }
      }

      ubahWeight(jumlah) {
        let arr = this.state.jumlahWeight;
        arr = jumlah;
        this.setState({
          jumlahWeight: arr
        });
      }

      tambahWeight() {
        let arr = this.state.jumlahWeight;
        arr++;
        this.setState({
          jumlahWeight: arr
        });

        console.log(this.state.jumlahWeight);
      }

      kurangWeight() {
        let arr = this.state.jumlahWeight;
        if (arr == 0 || arr == null || arr == undefined) {
        } else {
          arr--;
          this.setState({
            jumlahWeight: arr
          });
        }
      }

    _renderItem = ({ item }) => (
        <ListItem data={item}></ListItem>
    )

    componentDidMount() {
      AsyncStorage.getItem('listReportApar').then((listReportApar) => {
        this.setState({
          listReportApar: JSON.parse(listReportApar),
          id: JSON.parse(listReportApar).id,
          idApar: JSON.parse(listReportApar).id_apar,
          klem: JSON.parse(listReportApar).klem,
          jumlahPress: JSON.parse(listReportApar).press,
          kondisiHose: JSON.parse(listReportApar).hose,
          kondisiSeal: JSON.parse(listReportApar).seal,
          kondisiSapot: JSON.parse(listReportApar).sapot,
          jumlahWeight: JSON.parse(listReportApar).weight,
          kondisi: JSON.parse(listReportApar).status_cond,
          informasi: JSON.parse(listReportApar).note,
          picBadge: JSON.parse(listReportApar).pic_badge,
          picName: JSON.parse(listReportApar).pic_name,
          inspectionDate: JSON.parse(listReportApar).inspection_date,
        });
        this.loadLokasi();
        if(this.state.kondisiHose=='V'){
          this.setState({hose:true})
        } else {
          this.setState({hose:false})
        }
        if(this.state.kondisiSeal=='V'){
          this.setState({seal:true})
        } else {
          this.setState({seal:false})
        }
        if(this.state.kondisiSapot=='V'){
          this.setState({sapot:true})
        } else {
          this.setState({sapot:false})
        }

      })

        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        this.loadData();
      });
    }

    loadLokasi() {
      this.setState({ visibleDialog: false, isLoading: true });
        const url = GlobalConfig.SERVERHOST + "getLokasiApar";
        var formData = new FormData();
        formData.append("id_apar", this.state.idApar);

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
          .then(response => response.json())
          .then(responseJson => {
            this.setState({
              dataLokasi: responseJson.data,
              isLoading: false
            });
          })
          .catch(error => {
            console.log(error);
          });
    }

      updateReportApar(){
        if(this.state.hose == false){
          this.setState({kondisiHose: 'X'})
        } else {
          this.setState({kondisiHose: 'V'})
        }
        if(this.state.seal == false){
          this.setState({kondisiSeal: 'X'})
        } else {
          this.setState({kondisiSeal: 'V'})
        }
        if(this.state.sapot == false){
          this.setState({kondisiSapot: 'X'})
        } else {
          this.setState({kondisiSapot: 'V'})
        }
        if (this.state.jumlahPress == 0) {
            alert('Masukkan Sisa Tekanan');
        }
        else if (this.state.jumlahWeight == 0) {
            alert('Masukkan Berat Apar');
        }
        else if (this.state.kondisi == '') {
            alert('Masukkan Kondisi Apar');
        }
        else {
          this.setState({
              visibleDialogSubmit:true
          })
              var url = GlobalConfig.SERVERHOST + "updateInspectionApar";
              var formData = new FormData();
              formData.append("id", this.state.id);
              formData.append("id_apar", this.state.idApar);
              formData.append("klem", this.state.klem);
              formData.append("press", this.state.jumlahPress);
              formData.append("hose", this.state.kondisiHose);
              formData.append("seal", this.state.kondisiSeal);
              formData.append("sapot", this.state.kondisiSapot);
              formData.append("weight", this.state.jumlahWeight);
              formData.append("status_cond", this.state.kondisi);
              if (this.state.informasi!='') {
                formData.append("note", this.state.informasi);
              }
              formData.append("inspection_date", this.state.inspectionDate);
              formData.append("pic_badge", this.state.picBadge);
              formData.append("pic_name", this.state.picName);

              fetch(url, {
                  headers: {
                      "Content-Type": "multipart/form-data"
                  },
                  method: "POST",
                  body: formData
              })
                  .then(response => response.json())
                  .then(response => {
                      if (response.status == 200) {
                          this.setState({
                              visibleDialogSubmit:false
                          })
                          Alert.alert('Success', 'Update Inspection Report Apar Success', [{
                              text: 'Okay'
                          }])
                          this.props.navigation.navigate('InspectionReportAparDetail')
                      } else {
                          this.setState({
                              visibleDialogSubmit:false
                          })
                          Alert.alert('Error', 'Update Inspection Report Apar Failed', [{
                              text: 'Okay'
                          }])
                      }
                  })
                  .catch((error) => {
                      this.setState({
                          visibleDialogSubmit:false
                      })
                      Alert.alert('Error', 'Check Your Internet Connection', [{
                          text: 'Okay'
                      }])
                        console.log(error)
                  })
        }
    }

    onPressHose = () => {
      this.setState({ hose: !this.state.hose });
    }
    onPressSeal = () => {
      this.setState({ seal: !this.state.seal });
    }
    onPressSapot = () => {
      this.setState({ sapot: !this.state.sapot });
    }

    render() {
        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left style={{flex:1}}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("InspectionReportAparDetail")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body style={{flex:3, alignItems:'center'}}>
                        <Title style={styles.textbody}>{this.state.dataLokasi.area}</Title>
                    </Body>
                    <Right style={{flex:1}}/>
                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />

                <View style={{ flex: 1 }}>
                    <Content style={{ marginTop: 0}}>
                        <View style={{ backgroundColor: colors.gray }}>
                        <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                            <View>
                            <Text style={{fontSize:10}}>Area</Text>
                            <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.dataLokasi.area}</Text>
                            <Text style={{fontSize:10, paddingTop: 5}}>Unit Kerja</Text>
                            <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.dataLokasi.unit_kerja}</Text>
                        </View>
                        </CardItem>


                        <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                            <View style={{ flex: 1}}>
                                <View>
                                    <Text style={styles.titleInput}>Pilih Klem *</Text>
                                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                                        <Picker
                                            mode="dropdown"
                                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                                            style={{ width: '100%', height: 35}}
                                            placeholder="Kelas"
                                            placeholderStyle={{ color: "#bfc6ea" }}
                                            placeholderIconColor="#007aff"
                                            selectedValue={this.state.klem}
                                            onValueChange={(itemValue) => this.setState({ klem: itemValue })}>
                                            <Picker.Item label="K (Klem)" value="K" />
                                            <Picker.Item label="T (Tanduk)" value="T" />
                                            <Picker.Item label="U (U Klem)" value="U" />
                                            <Picker.Item label="B (Box)" value="B" />
                                            <Picker.Item label="D (Duduk)" value="D" />
                                        </Picker>
                                    </Form>
                                </View>
                            </View>
                        </CardItem>

                        <CardItem style={{ borderRadius: 0, backgroundColor:colors.gray }}>
                          <View style={{flex:1}}>
                            <View>
                                <Text style={{fontSize:10}}>Press *</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: "row", marginTop:5, backgroundColor:colors.gray }}>
                                <View style={{width:'50%'}}>
                                    <View style={{ flex: 1, flexDirection: "row"}}>
                                      <Button onPress={() => this.kurangPress()} style={styles.btnQTYLeft}>
                                          <Icon
                                            name="ios-arrow-back"
                                            style={styles.facebookButtonIconQTY}
                                          />
                                      </Button>
                                      <View style={{width:40, height:30, marginTop: 0, backgroundColor: colors.white}}>
                                        <Input
                                          style={{
                                            height:30,
                                            marginTop:0,
                                            fontSize:9,
                                            textAlign:'center'}}
                                          value={this.state.jumlahPress + ""}
                                          keyboardType='numeric'
                                          onChangeText={text => this.ubahPress(text)}
                                        />
                                      </View>
                                      <Button onPress={() => this.tambahPress()} style={styles.btnQTYRight}>
                                        <Icon
                                          name="ios-arrow-forward"
                                          style={styles.facebookButtonIconQTY}
                                        />
                                      </Button>
                                    </View>
                                </View>
                            </View>
                          </View>
                        </CardItem>

                        <CardItem style={{ borderRadius: 0, backgroundColor:colors.gray }}>
                          <View style={{flex:1}}>
                            <View>
                                <Text style={{fontSize:10}}>Weight</Text>
                            </View>
                            <View style={{ flex: 1, flexDirection: "row", marginTop:5, backgroundColor:colors.gray }}>
                                <View style={{width:'50%'}}>
                                    <View style={{ flex: 1, flexDirection: "row"}}>
                                      <Button onPress={() => this.kurangWeight()} style={styles.btnQTYLeft}>
                                          <Icon
                                            name="ios-arrow-back"
                                            style={styles.facebookButtonIconQTY}
                                          />
                                      </Button>
                                      <View style={{width:40, height:30, marginTop: 0, backgroundColor: colors.white}}>
                                        <Input
                                          style={{
                                            height:30,
                                            marginTop:0,
                                            fontSize:9,
                                            textAlign:'center'}}
                                          value={this.state.jumlahWeight + ""}
                                          keyboardType='numeric'
                                          onChangeText={text => this.ubahWeight(text)}
                                        />
                                      </View>
                                      <Button onPress={() => this.tambahWeight()} style={styles.btnQTYRight}>
                                        <Icon
                                          name="ios-arrow-forward"
                                          style={styles.facebookButtonIconQTY}
                                        />
                                      </Button>
                                    </View>
                                </View>
                            </View>
                          </View>
                        </CardItem>

                        <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                            <View style={{ flex: 1, flexDirection: "column"}}>
                                <View style={{ flex: 1, flexDirection: "row" }}>
                                    <View style={{width:'75%', paddingTop: 5}}>
                                        <Text style={{fontSize:10, fontWeight:"bold"}}>Hose</Text>
                                    </View>
                                    <View style={{width: '25%', paddingTop: 5}}>
                                      <SwitchToggle
                                        containerStyle={{
                                          width: 70,
                                          height: 30,
                                          borderRadius: 25,
                                          padding: 5,
                                        }}
                                        backgroundColorOff={colors.graydar}
                                        backgroundColorOn='#00b300'
                                        circleStyle={{
                                          width: 22,
                                          height: 22,
                                          borderRadius: 19,
                                          backgroundColor: 'white',
                                        }}
                                        switchOn={this.state.hose}
                                        onPress={this.onPressHose}
                                        circleColorOff='white'
                                        circleColorOn='white'
                                        duration={500}
                                      />
                                    </View>
                                </View>
                                <View style={{ flex: 1, flexDirection: "row" }}>
                                    <View style={{width:'75%', paddingTop: 10}}>
                                        <Text style={{fontSize:10, fontWeight:"bold"}}>Seal</Text>
                                    </View>
                                    <View style={{width: '25%', paddingTop: 10}}>
                                      <SwitchToggle
                                        containerStyle={{
                                          width: 70,
                                          height: 30,
                                          borderRadius: 25,
                                          padding: 5,
                                        }}
                                        backgroundColorOff={colors.graydar}
                                        backgroundColorOn='#00b300'
                                        circleStyle={{
                                          width: 22,
                                          height: 22,
                                          borderRadius: 19,
                                          backgroundColor: 'white',
                                        }}
                                        switchOn={this.state.seal}
                                        onPress={this.onPressSeal}
                                        circleColorOff='white'
                                        circleColorOn='white'
                                        duration={500}
                                      />
                                    </View>
                                </View>
                                <View style={{ flex: 1, flexDirection: "row" }}>
                                    <View style={{width:'75%', paddingTop: 10}}>
                                        <Text style={{fontSize:10, fontWeight:"bold"}}>Sapot</Text>
                                    </View>
                                    <View style={{width: '25%', paddingTop: 10}}>
                                      <SwitchToggle
                                        containerStyle={{
                                          width: 70,
                                          height: 30,
                                          borderRadius: 25,
                                          padding: 5,
                                        }}
                                        backgroundColorOff={colors.graydar}
                                        backgroundColorOn='#00b300'
                                        circleStyle={{
                                          width: 22,
                                          height: 22,
                                          borderRadius: 19,
                                          backgroundColor: 'white',
                                        }}
                                        switchOn={this.state.sapot}
                                        onPress={this.onPressSapot}
                                        circleColorOff='white'
                                        circleColorOn='white'
                                        duration={500}
                                      />
                                    </View>
                                </View>
                            </View>
                        </CardItem>

                        <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                            <View style={{ flex: 1}}>
                                <View>
                                    <Text style={styles.titleInput}>Status Kondisi</Text>
                                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                                        <Picker
                                            mode="dropdown"
                                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                                            style={{ width: '100%', height: 35}}
                                            placeholder="Kelas"
                                            placeholderStyle={{ color: "#bfc6ea" }}
                                            placeholderIconColor="#007aff"
                                            selectedValue={this.state.kondisi}
                                            onValueChange={(itemValue) => this.setState({ kondisi: itemValue })}>
                                            <Picker.Item label="Belum Dicek" value="" />
                                            <Picker.Item label="Baik" value="BAIK" />
                                            <Picker.Item label="Ganti" value="GANTI" />
                                            <Picker.Item label="Habis" value="HABIS" />
                                            <Picker.Item label="Refill" value="REFILL" />
                                        </Picker>
                                    </Form>
                                </View>
                            </View>
                        </CardItem>

                       <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                            <View style={{ flex: 1 }}>
                                <View>
                                    <Text style={styles.titleInput}>Informasi</Text>
                                </View>
                                <View>
                                    <Textarea style={{
                                        marginLeft: 5,
                                        marginRight: 5,
                                        marginBottom: 5,
                                        borderRadius: 5,
                                        fontSize: 11 }}
                                        rowSpan={2}
                                        bordered value={this.state.informasi}
                                        placeholder='Informasi ...'
                                        onChangeText={(text) => this.setState({ informasi: text })} />
                                </View>
                            </View>
                        </CardItem>
                        <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                          <View style={{ flex: 1}}>
                          <View style={styles.Contentsave}>
                            <Button
                              block
                              style={{
                                width:'100%',
                                height: 45,
                                marginBottom: 20,
                                borderWidth: 1,
                                backgroundColor: "#00b300",
                                borderColor: "#00b300",
                                borderRadius: 4
                              }}
                              onPress={() => this.updateReportApar()}
                            >
                              <Text style={{color:colors.white}}>SUBMIT</Text>
                            </Button>
                          </View>
                          </View>
                        </CardItem>
                    </View>
                    <View style={{ width: 270, position: "absolute" }}>
                        <Dialog
                            visible={this.state.visibleDialogSubmit}
                            dialogTitle={<DialogTitle title="Update Inspection Report Apar .." />}
                        >
                            <DialogContent>
                            {<ActivityIndicator size="large" color="#330066" animating />}
                            </DialogContent>
                        </Dialog>
                    </View>
                    </Content>
                </View>
                <CustomFooter navigation={this.props.navigation} menu='Safety' />
            </Container>

        );
    }
}

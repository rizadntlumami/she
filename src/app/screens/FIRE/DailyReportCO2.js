import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail,
  Picker,
  Form
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewReportCO2 from "../../components/FireSystem/ListViewReportCO2";
import Ripple from "react-native-material-ripple";
import DatePicker from "react-native-datepicker";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, id) {
    AsyncStorage.setItem("id", JSON.stringify(id)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{marginLeft: 10, marginRight: 10, borderRadius: 5, backgroundColor:colors.white, marginBottom:10,
        shadowColor:'#000',
        shadowOffset:{
          width:0,
          height:2,
        },
        shadowOpacity:0.25,
        shadowRadius:3.84,
        elevation:2,
      }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.navigateToScreen("DailyReportCO2Detail", this.props.data.id)
          }
          rippleColor={colors.accent}
        >
          <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
            <ListViewReportCO2
              imageUri={require("../../../assets/images/iconCO2.jpg")}
              pplantdesc={this.props.data.pplantdesc}
              note={this.props.data.note}
              pic={this.props.data.pic_name}
              date={this.props.data.inspection_date}
              time={this.props.data.inspection_time}
              shift={this.props.data.shift}
              residualCemetron={this.props.data.total_residual_cemetron}
              residualSamator={this.props.data.total_residual_samator}
            />
          </View>
        </Ripple>
      </View>
    );
  }
}

class DailyReportCO2 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      listPlant: [],
      isLoading: true,
      visibleDialog: false,
      filterDate: "",
      filterPlant: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  onRefresh() {
    console.log("refreshing");
    this.setState({ isLoading: true }, function() {
      this.loadData();
    });
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
        this.loadPlant();
        this.setState({ isLoading: true });
      }
    );
  }

  loadData() {
    this.setState({ visibleDialog: false, isLoading: true });
      const url = GlobalConfig.SERVERHOST + "getInspectionGas";
      var formData = new FormData();
      if (this.state.filterPlant != "") {
        formData.append("PLANT", this.state.filterPlant);
      } else {
        formData.append("PLANT", "5001");
      }
      if (this.state.filterDate != "") {
        formData.append("INSP_DATETIME", this.state.filterDate);
      } else {
        formData.append("INSP_DATETIME", "");
      }
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataSource: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
  }

  onClickSearch() {
    this.setState({
      visibleDialog: true
    });
  }

  render() {
    that = this;
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.dataSource == '') {
        list = (
          <View
            style={{
              flex: 1,
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <FlatList
            data={this.state.dataSource}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isLoading}
                onRefresh={this.onRefresh.bind(this)}
              />
            }
          />
        );
      }
    }

    let listPlant = this.state.listPlant.map((s, i) => {
      return <Picker.Item key={i} value={s.PLANT} label={s.PLANT_TEXT} />;
    });
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("FireMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>Daily Report Section</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button transparent onPress={() => this.onClickSearch()}>
              <Icon
                name="ios-funnel"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Footer>
          <FooterTab style={styles.tabfooter}>
            <Button
              onPress={() =>
                this.props.navigation.navigate("DailyReportAktifitas")
              }
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>
                Aktifitas
              </Text>
            </Button>
            <Button active style={styles.tabfooter}>
              <View style={{ height: "40%" }} />
              <View style={{ height: "50%" }}>
                <Text style={styles.textbody}>CO2</Text>
              </View>
              <View style={{ height: "20%" }} />
              <View
                style={{
                  borderWidth: 2,
                  marginTop: 2,
                  height: 0.5,
                  width: "100%",
                  borderColor: colors.white
                }}
              />
            </Button>
          </FooterTab>
        </Footer>

        <View style={{ marginTop: 10,flex:1,flexDirection:'column' }}>{list}</View>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: colors.green01, marginBottom: 30 }}
          position="bottomRight"
          onPress={() => this.props.navigation.navigate("DailyReportCO2Create")}
        >
          <Icon
            name="ios-add"
            style={{
              fontSize: 50,
              fontWeight: "bold",
              paddingTop: Platform.OS === "ios" ? 25 : null
            }}
          />
        </Fab>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialog}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ visibleDialog: false });
            }}
            dialogTitle={<DialogTitle title="Select sorting here" />}
            actions={[
              <DialogButton
                style={{
                  fontSize: 11,
                  backgroundColor: colors.white,
                  borderColor: colors.blue01
                }}
                text="SORT"
                onPress={() => this.loadData()}
                // onPress={() => this.setState({ visibleFilter: false })}
              />
            ]}
          >
            <DialogContent>
              {
                <View>
                  <Text
                    style={{ paddingTop: 10, fontWeight: "bold", fontSize: 10 }}
                  >
                    Pilih Plant
                  </Text>
                  <Form
                    style={{
                      borderWidth: 1,
                      borderRadius: 5,
                      marginRight: 5,
                      marginLeft: 5,
                      borderColor: "#E6E6E6",
                      height: 35
                    }}
                  >
                    <Picker
                      mode="dropdown"
                      iosIcon={
                        <Icon
                          name={
                            Platform.OS
                              ? "ios-arrow-down"
                              : "ios-arrow-down-outline"
                          }
                        />
                      }
                      style={{ width: 300, height: 35 }}
                      placeholder="Select Shift ..."
                      placeholderStyle={{ color: "#bfc6ea" }}
                      placeholderIconColor="#007aff"
                      selectedValue={this.state.filterPlant}
                      onValueChange={itemValue =>
                        this.setState({ filterPlant: itemValue })
                      }
                    >
                      <Picker.Item label="Choose Plant..." value="" />
                      {listPlant}
                    </Picker>
                  </Form>
                  <Text
                    style={{ paddingTop: 20, fontWeight: "bold", fontSize: 10 }}
                  >
                    Tanggal Pemeriksaan *
                  </Text>
                  <DatePicker
                    style={{ width: "100%", fontSize: 10, borderRadius: 20 }}
                    date={this.state.filterDate}
                    mode="date"
                    placeholder="Choose Date ..."
                    format="YYYY-MM-DD"
                    minDate="2018-01-01"
                    maxDate="5000-12-31"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    customStyles={{
                      dateInput: {
                        marginLeft: 5,
                        marginRight: 5,
                        height: 35,
                        borderRadius: 5,
                        fontSize: 10,
                        borderWidth: 1,
                        borderColor: "#E6E6E6"
                      },
                      dateIcon: {
                        position: "absolute",
                        left: 0,
                        top: 5
                      }
                    }}
                    onDateChange={date => {
                      this.setState({ filterDate: date });
                    }}
                  />
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>
        <CustomFooter navigation={this.props.navigation} menu="FireSystem" />
      </Container>
    );
  }
}

export default DailyReportCO2;

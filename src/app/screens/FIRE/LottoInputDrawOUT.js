import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Label,
    Item
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import Ripple from "react-native-material-ripple";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";

class ListItem extends React.PureComponent {
  navigateToScreen(route, id_rusak) {
    // alert(route);
    AsyncStorage.setItem("id", id_rusak).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    var arrNoPeg=this.props.data.mk_nopeg.split('');
    var noPeg=''
    var zeroStop=false
    for (let i=0;i<arrNoPeg.length;i++){
      if (arrNoPeg[i]!='0'){
        zeroStop=true
      }
      if (zeroStop){
        noPeg+=arrNoPeg[i]
      }
    }
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.props.setSelectedPegLabel(this.props.data.mk_nama,this.props.data.mk_nopeg)
          }
          rippleColor={colors.accent}
        >
          <CardItem
            style={{
              borderRadius: 0,
              marginTop: 4,
              backgroundColor: colors.gray
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 4,
                backgroundColor: colors.gray
              }}
            >
              <View >
                <Text style={{fontSize:12}}>{noPeg} - {this.props.data.mk_nama}</Text>
              </View>
            </View>
          </CardItem>
        </Ripple>
      </View>
    );
  }
}

export default class LottoInputDrawOUT extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      dataHeader: [],
      isLoading: true,
      listKaryawan:[],
      listKaryawanMaster:[],
      listLotto:[],
      idUpdate:'',
      k3: '',
      k3Ket: '',
      pmlListrik: '',
      pmlListrikKet: '',
      operator: '',
      operatorKet: '',
      pmlTerkait: '',
      pmlTerkaitKet: '',
      visibleLoadingPegawai:false,
      visibleSearchListPeg:false,
      selectedPegLabelK3:'',
      selectedPegLabelPMLListrik:'',
      selectedPegLabelOperator:'',
      selectedPegLabelPMLTerkait:'',
      searchWordPegawai:'',
      visibleDialogSubmit:false,
      idLotto:'',
    };
  }

  static navigationOptions = {
      header: null
  };


  componentDidMount() {
      AsyncStorage.getItem('dataDrawout').then((dataDrawout) => {
        AsyncStorage.getItem('idUpdate').then((idUpdate) => {
          this.setState({
            idUpdate:idUpdate,
            dataDrawout: JSON.parse(dataDrawout),
            idLotto: JSON.parse(dataDrawout).id,
            k3: JSON.parse(dataDrawout).k3_drawout_badge,
            selectedPegLabelK3: JSON.parse(dataDrawout).k3_drawout_name,
            k3Ket: JSON.parse(dataDrawout).k3_drawout_ket,
            pmlListrik: JSON.parse(dataDrawout).pmll_drawout_badge,
            selectedPegLabelPMLListrik: JSON.parse(dataDrawout).pmll_drawout_name,
            pmlListrikKet: JSON.parse(dataDrawout).pmll_drawout_ket,
            operator: JSON.parse(dataDrawout).operator_drawout_badge,
            selectedPegLabelOperator: JSON.parse(dataDrawout).operator_drawout_name,
            operatorKet: JSON.parse(dataDrawout).operator_drawout_ket,
            pmlTerkait: JSON.parse(dataDrawout).pmlt_drawout_badge,
            selectedPegLabelPMLTerkait: JSON.parse(dataDrawout).pmlt_drawout_name,
            pmlTerkaitKet: JSON.parse(dataDrawout).pmlt_drawout_ket,
          });
          this.setState({ isLoading: false });
          console.log(this.state.listLotto);
      })
    })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        //this.loadData(id_Unsafe);
      });
      this.loadKaryawan();
    }


  loadData() {

  }

  loadKaryawan(){
    this.setState({
      visibleLoadingPegawai:true
    })
        const url = GlobalConfig.SERVERHOST + 'getEmployee';
        var formData = new FormData();
        formData.append("searchEmp", this.state.searchWordPegawai)
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
              this.setState({
                visibleLoadingPegawai:false
              })
                this.setState({
                    listKaryawan: responseJson.data,
                    listKaryawanMaster: responseJson.data,
                    isloading: false
                });
            })
            .catch((error) => {
              this.setState({
                visibleLoadingPegawai:false
              })
                console.log(error)
            })
  }

  createDrawKonfirmasi()  {
    if(this.state.idUpdate=='k3DrawOUT'){
      if(this.state.selectedPegLabelK3==null){
        alert("Masukkan Pegawai")
      } else if(this.state.k3Ket==null) {
        alert("Masukkan Keterangan")
      } else {
         this.createDraw()
      }
    } else if(this.state.idUpdate=='pmlListrikDrawOUT'){
      if(this.state.selectedPegLabelPMLListrik==null){
        alert("Masukkan Pegawai")
      } else if(this.state.pmlListrikKet==null) {
        alert("Masukkan Keterangan")
      } else {
         this.createDraw()
      }
    } else if(this.state.idUpdate=='operatorDrawOUT'){
      if(this.state.selectedPegLabelOperator==null){
        alert("Masukkan Pegawai")
      } else if(this.state.operatorKet==null) {
        alert("Masukkan Keterangan")
      } else {
         this.createDraw()
      }
    } else {
      if(this.state.selectedPegLabelPMLTerkait==null){
        alert("Masukkan Pegawai")
      } else if(this.state.pmlTerkaitKet==null) {
        alert("Masukkan Keterangan")
      } else {
         this.createDraw()
      }
    }
  };

  createDraw() {
    this.setState({
      visibleDialogSubmit:true
    })
      var url = GlobalConfig.SERVERHOST + "updateDrawOUTLotto";
      var formData = new FormData();
      formData.append("id", this.state.idLotto);
      formData.append("k3_drawout_badge", this.state.k3);
      formData.append("k3_drawout_name", this.state.selectedPegLabelK3);
      formData.append("k3_drawout_ket", this.state.k3Ket);

      formData.append("pmll_drawout_badge", this.state.pmlListrik);
      formData.append("pmll_drawout_name", this.state.selectedPegLabelPMLListrik);
      formData.append("pmll_drawout_ket", this.state.pmlListrikKet);

      formData.append("operator_drawout_badge", this.state.operator);
      formData.append("operator_drawout_name", this.state.selectedPegLabelOperator);
      formData.append("operator_drawout_ket", this.state.operatorKet);

      formData.append("pmlt_drawout_badge", this.state.pmlTerkait);
      formData.append("pmlt_drawout_name", this.state.selectedPegLabelPMLTerkait);
      formData.append("pmlt_drawout_ket", this.state.pmlTerkaitKet);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        if (response.status == 200) {
          this.setState({
            visibleDialogSubmit:false
          })
          Alert.alert('Success', 'Input Draw OUT Success', [{
            text: 'Okay'
          }])
          this.props.navigation.navigate('LottoDrawOUT')
        } else {
          this.setState({
            visibleDialogSubmit:false
          })
          Alert.alert('Error', 'Input Draw OUT Failed', [{
            text: 'Okay'
          }])
        }
      })
      .catch((error)=>{
        console.log(error)
        this.setState({
          visibleDialogSubmit:false
        })
        Alert.alert('Error', 'Input Draw OUT Failed', [{
          text: 'Okay'
        }])
      })
  }

  /*K3*/
  onChangePegawaiK3(text){
    this.setState({
      searchWordPegawai:text
    })
    this.loadKaryawan();
  }

  onClickSearchPegK3(){
    console.log('masuk')
    if (this.state.visibleSearchListPeg){
      this.setState({
        visibleSearchListPeg:false
      })
    }else{
      this.setState({
        visibleSearchListPeg:true
      })
    }
  }

  setSelectedPegLabelK3(text,badge){
    this.setState({
      selectedPegLabelK3:text,
      visibleSearchListPeg:false,
      k3:badge
    })
  }

  /*PML LISTRIK*/
  onChangePegawaiPMLListrik(text){
    this.setState({
      searchWordPegawai:text
    })
    this.loadKaryawan();
  }

  onClickSearchPegPMLListrik(){
    console.log('masuk')
    if (this.state.visibleSearchListPeg){
      this.setState({
        visibleSearchListPeg:false
      })
    }else{
      this.setState({
        visibleSearchListPeg:true
      })
    }
  }

  setSelectedPegLabelPMLListrik(text,badge){
    this.setState({
      selectedPegLabelPMLListrik:text,
      visibleSearchListPeg:false,
      pmlListrik:badge
    })
  }

  /*PML LISTRIK*/
  onChangePegawaiOperator(text){
    this.setState({
      searchWordPegawai:text
    })
    this.loadKaryawan();
  }

  onClickSearchPegOperator(){
    console.log('masuk')
    if (this.state.visibleSearchListPeg){
      this.setState({
        visibleSearchListPeg:false
      })
    }else{
      this.setState({
        visibleSearchListPeg:true
      })
    }
  }

  setSelectedPegLabelOperator(text,badge){
    this.setState({
      selectedPegLabelOperator:text,
      visibleSearchListPeg:false,
      operator:badge
    })
  }

  /*PML TERKAIT*/
  onChangePegawaiPMLTerkait(text){
    this.setState({
      searchWordPegawai:text
    })
    this.loadKaryawan();
  }

  onClickSearchPegPMLTerkait(){
    console.log('masuk')
    if (this.state.visibleSearchListPeg){
      this.setState({
        visibleSearchListPeg:false
      })
    }else{
      this.setState({
        visibleSearchListPeg:true
      })
    }
  }

  setSelectedPegLabelPMLTerkait(text,badge){
    this.setState({
      selectedPegLabelPMLTerkait:text,
      visibleSearchListPeg:false,
      pmlTerkait:badge
    })
  }

  _renderItemK3 = ({ item }) => <ListItem data={item} setSelectedPegLabel={(text,text2) => this.setSelectedPegLabelK3(text,text2)} />;
  _renderItemPMLListrik = ({ item }) => <ListItem data={item} setSelectedPegLabel={(text,text2) => this.setSelectedPegLabelPMLListrik(text,text2)} />;
  _renderItemOperator = ({ item }) => <ListItem data={item} setSelectedPegLabel={(text,text2)=> this.setSelectedPegLabelOperator(text,text2)} />;
  _renderItemPMLTerkait = ({ item }) => <ListItem data={item} setSelectedPegLabel={(text,text2) => this.setSelectedPegLabelPMLTerkait(text,text2)} />;
  render() {
    // let listKaryawan = this.state.listKaryawan.map( (s, i) => {
    //   return <Picker.Item key={i} value={s.mk_nama} label={s.mk_nama} />
    // });
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DrawOUT")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Draw OUT</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={{ flex: 1 }}>
        <Content style={{ marginTop: 0 }}>
          <View style={{ backgroundColor: '#FEFEFE' }}>
            {this.state.idUpdate=='k3DrawOUT' ? (
              <View>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={{fontWeight:'bold', fontSize:10, color:colors.green01}}>K3</Text>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                  <View>
                    <Text style={styles.titleInput}>No Badge</Text>
                    <View style={{ flex: 1,flexDirection:'column'}}>
                      <Button
                        block
                        style={{flex:1,borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, backgroundColor:colors.gray ,borderColor:"#E6E6E6", height:35}}
                        onPress={() => this.onClickSearchPegK3()}>
                        <Text style={{fontSize:12}}>{this.state.selectedPegLabelK3}</Text>
                      </Button>
                    </View>

                    {this.state.visibleSearchListPeg &&
                    <View style={{height:300,flexDirection:'column',borderWidth:1,padding:10,backgroundColor:colors.gray,margin:5,borderColor:"#E6E6E6",}}>
                      <Form>
                        <Item stackedLabel style={{ marginLeft: 0 }}>
                          <Input value={this.state.searchWordPegawai} style={{borderRadius:5, marginLeft:5, marginRight:5, fontSize:10}} bordered onChangeText={(text) => this.onChangePegawaiK3(text)} placeholder='Ketik nama karyawan' />
                        </Item>
                      </Form>
                      <FlatList
                      data={this.state.listKaryawan}
                      renderItem={this._renderItemK3}
                      keyExtractor={(item, index) => index.toString()}
                      />
                    </View>
                    }
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Officer Note</Text>
                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                            style={{ width: '100%', height:35}}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.k3Ket}
                            onValueChange={(itemValue) => this.setState({k3Ket:itemValue})}>
                            <Picker.Item label="Choose Keterangan"  value=""/>
                            <Picker.Item label="Tidak Datang & Tidak Mengamankan"  value="X"/>
                            <Picker.Item label="Datang & Tidak Mengamankan"  value="Y"/>
                            <Picker.Item label="Datang & Mengamankan"  value="V"/>
                        </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              </View>):this.state.idUpdate=='pmlListrikDrawOUT' ?(
              <View>
              <CardItem style={{ borderRadius: 0, marginTop:5, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={{fontWeight:'bold', fontSize:10, color:colors.green01}}>PML LISTRIK</Text>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                  <View>
                    <Text style={styles.titleInput}>No Badge</Text>
                    <View style={{ flex: 1,flexDirection:'column'}}>
                      <Button
                        block
                        style={{flex:1,borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, backgroundColor:colors.gray ,borderColor:"#E6E6E6", height:35}}
                        onPress={() => this.onClickSearchPegPMLListrik()}>
                        <Text style={{fontSize:12}}>{this.state.selectedPegLabelPMLListrik}</Text>
                      </Button>
                    </View>

                    {this.state.visibleSearchListPeg &&
                    <View style={{height:300,flexDirection:'column',borderWidth:1,padding:10,backgroundColor:colors.gray,margin:5,borderColor:"#E6E6E6",}}>
                      <Form>
                        <Item stackedLabel style={{ marginLeft: 0 }}>
                          <Input value={this.state.searchWordPegawai} style={{borderRadius:5, marginLeft:5, marginRight:5, fontSize:10}} bordered onChangeText={(text) => this.onChangePegawaiPMLListrik(text)} placeholder='Ketik nama karyawan' />
                        </Item>
                      </Form>
                      <FlatList
                      data={this.state.listKaryawan}
                      renderItem={this._renderItemPMLListrik}
                      keyExtractor={(item, index) => index.toString()}
                      />
                    </View>
                    }
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Officer Note</Text>
                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                            style={{ width: '100%', height:35}}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.pmlListrikKet}
                            onValueChange={(itemValue) => this.setState({pmlListrikKet:itemValue})}>
                            <Picker.Item label="Choose Keterangan"  value=""/>
                            <Picker.Item label="Tidak Datang & Tidak Mengamankan"  value="X"/>
                            <Picker.Item label="Datang & Tidak Mengamankan"  value="Y"/>
                            <Picker.Item label="Datang & Mengamankan"  value="V"/>
                        </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              </View>):this.state.idUpdate=='operatorDrawOUT' ?(
              <View>
              <CardItem style={{ borderRadius: 0, marginTop:5, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={{fontWeight:'bold', fontSize:10, color:colors.green01}}>OPERATOR</Text>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                  <View>
                    <Text style={styles.titleInput}>No Badge</Text>
                    <View style={{ flex: 1,flexDirection:'column'}}>
                      <Button
                        block
                        style={{flex:1,borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, backgroundColor:colors.gray ,borderColor:"#E6E6E6", height:35}}
                        onPress={() => this.onClickSearchPegOperator()}>
                        <Text style={{fontSize:12}}>{this.state.selectedPegLabelOperator}</Text>
                      </Button>
                    </View>

                    {this.state.visibleSearchListPeg &&
                    <View style={{height:300,flexDirection:'column',borderWidth:1,padding:10,backgroundColor:colors.gray,margin:5,borderColor:"#E6E6E6",}}>
                      <Form>
                        <Item stackedLabel style={{ marginLeft: 0 }}>
                          <Input value={this.state.searchWordPegawai} style={{borderRadius:5, marginLeft:5, marginRight:5, fontSize:10}} bordered onChangeText={(text) => this.onChangePegawaiOperator(text)} placeholder='Ketik nama karyawan' />
                        </Item>
                      </Form>
                      <FlatList
                      data={this.state.listKaryawan}
                      renderItem={this._renderItemOperator}
                      keyExtractor={(item, index) => index.toString()}
                      />
                    </View>
                    }
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Officer Note</Text>
                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                            style={{ width: '100%', height:35}}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.operatorKet}
                            onValueChange={(itemValue) => this.setState({operatorKet:itemValue})}>
                            <Picker.Item label="Choose Keterangan"  value=""/>
                            <Picker.Item label="Tidak Datang & Tidak Mengamankan"  value="X"/>
                            <Picker.Item label="Datang & Tidak Mengamankan"  value="Y"/>
                            <Picker.Item label="Datang & Mengamankan"  value="V"/>
                        </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              </View>):(
              <View>
              <CardItem style={{ borderRadius: 0, marginTop:5, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={{fontWeight:'bold', fontSize:10, color:colors.green01}}>PML TERKAIT</Text>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                  <View>
                    <Text style={styles.titleInput}>No Badge</Text>
                    <View style={{ flex: 1,flexDirection:'column'}}>
                      <Button
                        block
                        style={{flex:1,borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, backgroundColor:colors.gray ,borderColor:"#E6E6E6", height:35}}
                        onPress={() => this.onClickSearchPegPMLTerkait()}>
                        <Text style={{fontSize:12}}>{this.state.selectedPegLabelPMLTerkait}</Text>
                      </Button>
                    </View>

                    {this.state.visibleSearchListPeg &&
                    <View style={{height:300,flexDirection:'column',borderWidth:1,padding:10,backgroundColor:colors.gray,margin:5,borderColor:"#E6E6E6",}}>
                      <Form>
                        <Item stackedLabel style={{ marginLeft: 0 }}>
                          <Input value={this.state.searchWordPegawai} style={{borderRadius:5, marginLeft:5, marginRight:5, fontSize:10}} bordered onChangeText={(text) => this.onChangePegawaiPMLTerkait(text)} placeholder='Ketik nama karyawan' />
                        </Item>
                      </Form>
                      <FlatList
                      data={this.state.listKaryawan}
                      renderItem={this._renderItemPMLTerkait}
                      keyExtractor={(item, index) => index.toString()}
                      />
                    </View>
                    }
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Officer Note</Text>
                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                            style={{ width: '100%', height:35}}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.pmlTerkaitKet}
                            onValueChange={(itemValue) => this.setState({pmlTerkaitKet:itemValue})}>
                            <Picker.Item label="Choose Keterangan"  value=""/>
                            <Picker.Item label="Tidak Datang & Tidak Mengamankan"  value="X"/>
                            <Picker.Item label="Datang & Tidak Mengamankan"  value="Y"/>
                            <Picker.Item label="Datang & Mengamankan"  value="V"/>
                        </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
            </View>)}
          </View>
        </Content>

        <View style={styles.Contentsave}>
          <Button
            block
            style={{
              height: 45,
              marginLeft: 20,
              marginRight: 20,
              marginBottom: 20,
              borderWidth: 1,
              backgroundColor: "#00b300",
              borderColor: "#00b300",
              borderRadius: 4
            }}
            onPress={() => this.createDrawKonfirmasi()}
          >
            <Text style={{color:colors.white}}>SUBMIT</Text>
          </Button>
        </View>
      </View>
      <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleLoadingPegawai}
          >
            <DialogContent>
              {
                <ActivityIndicator size="large" color="#330066" animating />
              }
            </DialogContent>
          </Dialog>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialogSubmit}
            dialogTitle={<DialogTitle title="Loading Draw Out .." />}
          >
            <DialogContent>
              {<ActivityIndicator size="large" color="#330066" animating />}
            </DialogContent>
          </Dialog>
        </View>
    </Container>
    );
  }
}

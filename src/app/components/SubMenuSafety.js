import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback } from "react-native";
import { Icon } from "native-base";
import Ripple from "react-native-material-ripple";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    height: '33%',
    width: '25%',
    marginLeft: 20,
    marginRight: 20,
    marginBottom: 10
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class SubMenuSafety extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 10,
            elevation: 2,
          }}
          rippleSize={276}
          rippleDuration={600}
          rippleContainerBorderRadius={100}
          onPress={handleOnPress}
          rippleColor={colors.accent}
        >
        <Image
          source={this.props.imageUri}
          style={{ width: 55, height: 55, resizeMode: "contain", marginTop: 2, marginBottom: 2 }}
        />
        </Ripple>
        <View style={{ flex: 1, flexDirection: 'column', paddingTop: 3, alignItems: "center" }}>
            <Text style={{fontSize:9, fontWeight: "bold"}}>{this.props.name}</Text>
          </View>
      </View>
    );
  }
}


SubMenuSafety.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};

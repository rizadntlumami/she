import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewHeader from "../../components/ListViewHeader";
import ListViewDetail from "../../components/ListViewDetail";

class SIOCertificationDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      dataDetail: [],
      isLoading: true,
      detailSIO:[],
      visibleDialogSubmit:false
    };
  }

  static navigationOptions = {
      header: null
  };

  _renderItem = ({ item }) => (
      <ListItem data={item}></ListItem>
  )

  navigateToScreen(route, detailSIO) {
    AsyncStorage.setItem('listSIO', JSON.stringify(detailSIO)).then(() => {
        this.props.navigation.navigate(route);
    })
}

  navigateToScreenHistori(route, noLisensi) {
    AsyncStorage.setItem('noLisensi', noLisensi).then(() => {
        this.props.navigation.navigate(route);
    })
  }

  componentDidMount() {
      AsyncStorage.getItem('listSIO').then((detailSIO) => {
          this.setState({ detailSIO: JSON.parse(detailSIO) });
          this.setState({ isLoading: false });
          console.log(this.state.detailSIO);
      })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        //this.loadData(id_Unsafe);
      });

  }

  konfirmasideleteSIO() {
    Alert.alert(
      'Apakah Anda Yakin Ingin Menghapus Sertifikasi', this.state.detailSIO.NAMA,
      [
        {text: 'Yes', onPress:()=> this.deleteSIO()},
        {text: 'Cancel'},
      ],
      { cancelable:false}
    )
  }

  deleteSIO() {
    this.setState({
      visibleDialogSubmit:true
    })
    AsyncStorage.getItem('token').then((value) => {
      // alert(JSON.stringify(value));
      const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/sertification/delete/sio';
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID", this.state.detailSIO.ID);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit:false
            })
            Alert.alert('Success', 'Delete Success', [{
              text: 'Oke',
            }])
            this.props.navigation.navigate("SIOCertification")
          } else {
            this.setState({
              visibleDialogSubmit:false
            })
            Alert.alert('Error', 'Delete Failed', [{
              text: 'Oke'
            }])
          }
        })
        .catch((error)=>{
          console.log(error)
          this.setState({
            visibleDialogSubmit:false
          })
          Alert.alert('Error', 'Delete Failed', [{
            text: 'Oke'
          }])
        })
    })
  }


  render() {
    var listSIO;
    var listDetailSIO;
    return (
      <Container style={styles.wrapper}>
          <Header style={styles.header}>
            <Left style={{width:'20%'}}>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("SIOCertification")}
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Left>
            <Body>
              <Title style={styles.textbody}>{this.state.detailSIO.NAMA}</Title>
            </Body>
            <Right style={{width:'20%'}}>
              <Button
                transparent
                onPress={() => this.navigateToScreenHistori('SIOCertificationHistori', this.state.detailSIO.NO_LISENSI)}
              >
                <Icon
                  name="ios-stopwatch"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
              <Button
                transparent
                onPress={() => this.navigateToScreen('SIOCertificationUpdate', this.state.detailSIO)}
              >
                <Icon
                  name="ios-create"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
              <Button
                transparent
                onPress={() => this.konfirmasideleteSIO()}
              >
                <Icon
                  name="ios-trash"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Right>
          </Header>
          <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
          <Content style={{marginTop:5}}>

          <View style={{}}>
          <CardItem style={{ borderRadius: 0}}>
              <View style={{ flex: 1}}>
                <Text style={{fontSize:10, paddingBottom:5, fontWeight:'bold'}}>Foto</Text>
                <View style={styles.placeholder}>
                  {this.state.detailSIO.FOTO == null ? (
                      <Image style={styles.previewImage} source={require("../../../assets/images/nopic.png")} />
                  ):(
                      <Image style={styles.previewImage} source={{uri : GlobalConfig.SERVERHOST + 'api' + this.state.detailSIO.FOTO + '?' + new Date()}}/>
                  )}
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:5, }}>
              <View style={{ flex: 1}}>
                <Text style={{fontSize:10}}>Pemegang</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailSIO.NAMA}</Text>
                <Text style={{fontSize:10, paddingTop:10}}>Kelas</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailSIO.KELAS}</Text>
                <Text style={{fontSize:10, paddingTop:10}}>Unit Kerja</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailSIO.UNIT_KERJA_TXT}</Text>
                <Text style={{fontSize:10, paddingTop:10}}>Jenis Lisensi</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailSIO.GROUP_NAME}</Text>
                <Text style={{fontSize:10, paddingTop:10}}>Nomor Lisensi</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailSIO.NO_LISENSI}</Text>
                <Text style={{fontSize:10, paddingTop:10}}>Tanggal Mulai Sertfikasi</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailSIO.MASA_AWAL}</Text>
                <Text style={{fontSize:10, paddingTop:10}}>Waktu</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailSIO.TIME_CERTIFICATION} Tahun</Text>
                <Text style={{fontSize:10, paddingTop:10}}>Tanggal Berakhir Sertifikasi</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.detailSIO.MASA_BERLAKU}</Text>
              </View>
            </CardItem>
            
          </View>
          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
              dialogTitle={<DialogTitle title="Deleting SIO Certification .." />}
            >
              <DialogContent>
                {<ActivityIndicator size="large" color="#330066" animating />}
              </DialogContent>
            </Dialog>
          </View>
          </Content>
      </Container>
    );
  }
}

export default SIOCertificationDetail;

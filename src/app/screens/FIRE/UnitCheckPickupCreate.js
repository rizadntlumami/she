import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  TextInput,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  ListItem,
  Body,
  Fab,
  Textarea,
  Switch,
  Icon,
  Picker,
  Form,
  Input,
  Label,
  Item
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DateTimePicker from "react-native-datepicker";
import DatePicker from "react-native-datepicker";
import ImagePicker from "react-native-image-picker";
import Ripple from "react-native-material-ripple";
import CheckBox from "react-native-check-box";
import Moment from "moment";
import ListView from "../../components/ListView";

export default class UnitCheckPickupCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      visibleDialogShift: true,
      isLoading: true,
      tabNumber: "tab1",
      shiftChecking: 1,
      isNegatifPemanasan: 0,
      visiblePemanasan: false,
      isNegatifSpeedometer: 0,
      visibleSpeedometer: false,
      isNegatifBbm: 0,
      visibleBbm: false,
      bbmChecking: "R",
      oliMesinChecking: "L",
      isNegatifOliMesin: 0,
      visibleOliMesin: false,
      oliRemChecking: "L",
      isNegatifOliRem: 0,
      visibleOliRem: false,
      oliPowerSteeringChecking: "L",
      isNegatifOliPowerSteering: 0,
      visibleOliPowerSteering: false,
      airRadiatorChecking: "R",
      isNegatifAirRadiator: 0,
      visibleAirRadiator: false,
      airCadanganRadiatorChecking: "R",
      isNegatifAirCadanganRadiator: 0,
      visibleAirCadanganRadiator: false,
      airWiperChecking: "R",
      isNegatifAirWiper: 0,
      visibleAirWiper: false,
      isNegatifBanDepanKiri: 0,
      visibleBanDepanKiri: false,
      isNegatifBanDepanKanan: 0,
      visibleBanDepanKanan: false,
      isNegatifBanBelakangKananLuar: 0,
      visibleBanBelakangKananLuar: false,
      isNegatifBanBelakangKiriLuar: 0,
      visibleBanBelakangKiriLuar: false,
      isNegatifBanBelakangKananDalam: 0,
      visibleBanBelakangKananDalam: false,
      isNegatifBanBelakangKiriDalam: 0,
      visibleBanBelakangKiriDalam: false,
      isNegatifKacaSpionKiri: 0,
      isNegatifKacaSpionKanan: 0,
      isNegatifLampuCabin: 0,
      isNegatifLampuKota: 0,
      isNegatifLampuJauh: 0,
      isNegatifLampuSeinKiri: 0,
      isNegatifLampuSeinKanan: 0,
      isNegatifLampuRem: 0,
      isNegatifLampuAtret: 0,
      isNegatifLampuSorot: 0,
      isNegatifDashboard: 0,
      listTools: [],
      headerUnitCheck: [],
      isCheckedOther: false,
      visibleDialogSubmit: false,
      lampuCabinChecking: true,
      lampuKotaChecking: true,
      lampuJauhChecking: true,
      lampuSeinKiriChecking: true,
      lampuSeinKananChecking: true,
      lampuRemChecking: true,
      lampuAtretChecking: true,
      lampuSorotChecking: true,
      dashboardChecking: true,
      spionKiriChecking: true,
      spionKananChecking: true,
      pickedImage: "",
      uri: "",
      fileName: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  pickImageHandler = () => {
    ImagePicker.showImagePicker(
      { title: "Pick an Image", maxWidth: 800, maxHeight: 600 },
      res => {
        if (res.didCancel) {
          console.log("User cancelled!");
        } else if (res.error) {
          console.log("Error", res.error);
        } else {
          this.setState({
            pickedImage: res.uri,
            uri: res.uri,
            fileName: res.fileName,
            fileType: res.type
          });
          console.log(res.type);
        }
      }
    );
  };

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
        this.setState({
          visibleDialogSubmit: false
        });
      }
    );
    AsyncStorage.getItem("list").then(listUnitcheck => {
      this.setState({
        headerUnitCheck: JSON.parse(listUnitcheck),
        ID: JSON.parse(listUnitcheck).ID_VEHICLE,
        pemanasanChecking: JSON.parse(listUnitcheck)
      });
      // this.setState({ isLoading: false });
      // this.loadData();
    });
  }

  loadData() {
    this.setState({
      visibleLoadingUnitKerja: true
    });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/firesystem/unit_check/get_vehicle_checklist";
      var array = "Engine & Acsecoris";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("VEHICLE_CHECK", "Pick Up");
      formData.append("REPORT_DATE", this.state.dateChecking);
      formData.append("ID_VEHICLE", this.state.headerUnitCheck.ID_VEHICLE);
      formData.append("REPORT_SHIFT", this.state.shiftChecking);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log(responseJson);
          // alert(JSON.stringify(responseJson.Data["Engine & Acsecoris"]))
          this.setState(
            {
              dataVehicleChecklist: responseJson.Data["Engine & Acsecoris"],
              isLoading: false,
              visibleLoadingUnitKerja: false
            },
            function() {
              console.log(this.state.dataVehicleChecklist);
              this.setState(
                {
                  pemanasanChecking:
                    this.state.dataVehicleChecklist[0].DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[0].DATA_CHECKED.CHECK_VAL: 0,
                  pemanasanCheckingNote:
                    this.state.dataVehicleChecklist[0].DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[0].DATA_CHECKED.CHECK_NOTE : null,
                  isNegatifPemanasan:
                    this.state.dataVehicleChecklist[0].DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[0].DATA_CHECKED.IS_NEGATIF : 0,
                  speedometerChecking:
                    this.state.dataVehicleChecklist[1].DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[1].DATA_CHECKED.CHECK_VAL : 0,
                  speedometerCheckingNote:
                    this.state.dataVehicleChecklist[1].DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[1].DATA_CHECKED.CHECK_NOTE : null,
                  isNegatifSpeedometer:
                    this.state.dataVehicleChecklist[1].DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[1].DATA_CHECKED.IS_NEGATIF : 0,
                  bbmChecking:
                    this.state.dataVehicleChecklist[2].DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[2].DATA_CHECKED.CHECK_VAL : "R",
                  bbmCheckingNote:
                    this.state.dataVehicleChecklist[2].DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[2].DATA_CHECKED.CHECK_NOTE : null,
                  isNegatifBbm:
                    this.state.dataVehicleChecklist[2].DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[2].DATA_CHECKED.IS_NEGATIF : 0,
                  oliMesinChecking:
                    this.state.dataVehicleChecklist[3].SUB_ITEM[0]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[3].SUB_ITEM[0].DATA_CHECKED
                      .CHECK_VAL : "L",
                  oliMesinCheckingNote:
                    this.state.dataVehicleChecklist[3].SUB_ITEM[0]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[3].SUB_ITEM[0].DATA_CHECKED
                      .CHECK_NOTE : null,
                  isNegatifOliMesin:
                    this.state.dataVehicleChecklist[3].SUB_ITEM[0]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[3].SUB_ITEM[0].DATA_CHECKED
                      .IS_NEGATIF : 0,
                  oliRemChecking:
                    this.state.dataVehicleChecklist[3].SUB_ITEM[1]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[3].SUB_ITEM[1].DATA_CHECKED
                      .CHECK_VAL : "L",
                  oliRemCheckingNote:
                    this.state.dataVehicleChecklist[3].SUB_ITEM[1]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[3].SUB_ITEM[1].DATA_CHECKED
                      .CHECK_NOTE : null,
                  isNegatifOliRem:
                    this.state.dataVehicleChecklist[3].SUB_ITEM[1]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[3].SUB_ITEM[1].DATA_CHECKED
                      .IS_NEGATIF : 0,
                  oliPowerSteeringChecking:
                    this.state.dataVehicleChecklist[3].SUB_ITEM[2]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[3].SUB_ITEM[2].DATA_CHECKED
                      .CHECK_VAL : "L",
                  oliPowerSteeringCheckingNote:
                    this.state.dataVehicleChecklist[3].SUB_ITEM[2]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[3].SUB_ITEM[2].DATA_CHECKED
                      .CHECK_NOTE : null,
                  isNegatifOliPowerSteering:
                    this.state.dataVehicleChecklist[3].SUB_ITEM[2]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[3].SUB_ITEM[2].DATA_CHECKED
                      .IS_NEGATIF : 0,
                  airRadiatorChecking:
                    this.state.dataVehicleChecklist[4].SUB_ITEM[0]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[4].SUB_ITEM[0].DATA_CHECKED
                      .CHECK_VAL : "R",
                  airRadiatorCheckingNote:
                    this.state.dataVehicleChecklist[4].SUB_ITEM[0]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[4].SUB_ITEM[0].DATA_CHECKED
                      .CHECK_NOTE : null,
                  isNegatifAirRadiator:
                    this.state.dataVehicleChecklist[4].SUB_ITEM[0]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[4].SUB_ITEM[0].DATA_CHECKED
                      .IS_NEGATIF : 0,
                  airCadanganRadiatorChecking:
                    this.state.dataVehicleChecklist[4].SUB_ITEM[1]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[4].SUB_ITEM[1].DATA_CHECKED
                      .CHECK_VAL : "R",
                  airCadanganRadiatorCheckingNote:
                    this.state.dataVehicleChecklist[4].SUB_ITEM[1]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[4].SUB_ITEM[1].DATA_CHECKED
                      .CHECK_NOTE : null,
                  isNegatifAirCadanganRadiator:
                    this.state.dataVehicleChecklist[4].SUB_ITEM[1]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[4].SUB_ITEM[1].DATA_CHECKED
                      .IS_NEGATIF : 0,
                  airWiperChecking:
                    this.state.dataVehicleChecklist[4].SUB_ITEM[2]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[4].SUB_ITEM[2].DATA_CHECKED
                      .CHECK_VAL : "R",
                  airWiperCheckingNote:
                    this.state.dataVehicleChecklist[4].SUB_ITEM[2]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[4].SUB_ITEM[2].DATA_CHECKED
                      .CHECK_NOTE : null,
                  isNegatifAirWiper:
                    this.state.dataVehicleChecklist[4].SUB_ITEM[2]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[4].SUB_ITEM[2].DATA_CHECKED
                      .IS_NEGATIF : 0,

                  // lampuCabinChecking:
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[0]
                  //     .DATA_CHECKED != null ?
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[0].DATA_CHECKED
                  //     .CHECK_VAL :,
                  // lampuKotaChecking:
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[1]
                  //     .DATA_CHECKED != null &&
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[1].DATA_CHECKED
                  //     .CHECK_VAL,
                  // lampuJauhChecking:
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[2]
                  //     .DATA_CHECKED != null &&
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[2].DATA_CHECKED
                  //     .CHECK_VAL,
                  // lampuSeinKiriChecking:
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[3]
                  //     .DATA_CHECKED != null &&
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[3].DATA_CHECKED
                  //     .CHECK_VAL,
                  // lampuSeinKananChecking:
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[4]
                  //     .DATA_CHECKED != null &&
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[4].DATA_CHECKED
                  //     .CHECK_VAL,
                  // lampuRemChecking:
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[5]
                  //     .DATA_CHECKED != null &&
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[5].DATA_CHECKED
                  //     .CHECK_VAL,
                  // lampuAtretChecking:
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[6]
                  //     .DATA_CHECKED != null &&
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[6].DATA_CHECKED
                  //     .CHECK_VAL,
                  // lampuSorotChecking:
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[7]
                  //     .DATA_CHECKED != null &&
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[7].DATA_CHECKED
                  //     .CHECK_VAL,
                  // dashboardChecking:
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[8]
                  //     .DATA_CHECKED != null &&
                  //   this.state.dataVehicleChecklist[5].SUB_ITEM[8].DATA_CHECKED
                  //     .CHECK_VAL,

                  banDepanKiriChecking:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[0]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[0].DATA_CHECKED
                      .CHECK_VAL : 0,
                  banDepanKiriCheckingNote:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[0]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[0].DATA_CHECKED
                      .CHECK_NOTE : null,
                  isNegatifBanDepanKiri:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[0]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[0].DATA_CHECKED
                      .IS_NEGATIF : 0,
                  banDepanKananChecking:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[1]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[1].DATA_CHECKED
                      .CHECK_VAL : 0,
                  banDepanKananCheckingNote:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[1]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[1].DATA_CHECKED
                      .CHECK_NOTE : null,
                  isNegatifBanDepanKanan:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[1]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[1].DATA_CHECKED
                      .IS_NEGATIF : 0,
                  banBelakangKiriDalamChecking:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[2]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[2].DATA_CHECKED
                      .CHECK_VAL : 0,
                  banBelakangKiriDalamCheckingNote:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[2]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[2].DATA_CHECKED
                      .CHECK_NOTE : null,
                  isNegatifBanBelakangKiriDalam:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[2]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[2].DATA_CHECKED
                      .IS_NEGATIF : 0,
                  banBelakangKiriLuarChecking:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[3]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[3].DATA_CHECKED
                      .CHECK_VAL : 0,
                  banBelakangKiriLuarCheckingNote:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[3]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[3].DATA_CHECKED
                      .CHECK_NOTE : null,
                  isNegatifBanBelakangKiriLuar:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[3]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[3].DATA_CHECKED
                      .IS_NEGATIF : 0,
                  banBelakangKananDalamChecking:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[4]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[4].DATA_CHECKED
                      .CHECK_VAL : 0,
                  banBelakangKananDalamCheckingNote:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[4]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[4].DATA_CHECKED
                      .CHECK_NOTE : null,
                  isNegatifBanBelakangKananDalam:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[4]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[4].DATA_CHECKED
                      .IS_NEGATIF : 0,
                  banBelakangKananLuarChecking:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[5]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[5].DATA_CHECKED
                      .CHECK_VAL : 0,
                  banBelakangKananLuarCheckingNote:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[5]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[5].DATA_CHECKED
                      .CHECK_NOTE : null,
                  isNegatifBanBelakangKananLuar:
                    this.state.dataVehicleChecklist[6].SUB_ITEM[5]
                      .DATA_CHECKED != null ?
                    this.state.dataVehicleChecklist[6].SUB_ITEM[5].DATA_CHECKED
                      .IS_NEGATIF : 0,

                  // spionKiriChecking:
                  //   this.state.dataVehicleChecklist[7].SUB_ITEM[0]
                  //     .DATA_CHECKED != null &&
                  //   this.state.dataVehicleChecklist[7].SUB_ITEM[0].DATA_CHECKED
                  //     .CHECK_VAL,
                  // spionKananChecking:
                  //   this.state.dataVehicleChecklist[7].SUB_ITEM[1]
                  //     .DATA_CHECKED != null &&
                  //   this.state.dataVehicleChecklist[7].SUB_ITEM[1].DATA_CHECKED
                  //     .CHECK_VAL
                },
                function() {
                  console.log(
                    this.state.pemanasanChecking +
                      "," +
                      this.state.speedometerChecking +
                      "," +
                      this.state.bbmChecking
                  );
                }
              );
              if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[0].DATA_CHECKED ==
                null
              ) {
                this.setState({ lampuCabinChecking: true });
                this.setState({lampuCabinCheckingNote: null});
              } else if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[0].DATA_CHECKED
                  .CHECK_VAL == "BAIK"
              ) {
                this.setState({ lampuCabinChecking: true });
                this.setState({
                  lampuCabinCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[0].DATA_CHECKED.CHECK_NOTE
                });
              } else {
                this.setState({ lampuCabinChecking: false });
                this.setState({
                  lampuCabinCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[0].DATA_CHECKED.CHECK_NOTE
                });
              }
              if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[1].DATA_CHECKED ==
                null
              ) {
                this.setState({ lampuKotaChecking: true });
                this.setState({lampuKotaCheckingNote: null});
              } else if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[1].DATA_CHECKED
                  .CHECK_VAL == "BAIK"
              ) {
                this.setState({ lampuKotaChecking: true });
                this.setState({
                  lampuKotaCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[1].DATA_CHECKED.CHECK_NOTE
                });
              } else {
                this.setState({ lampuKotaChecking: false });
                this.setState({
                  lampuKotaCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[1].DATA_CHECKED.CHECK_NOTE
                });
              }
              if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[2].DATA_CHECKED ==
                null
              ) {
                this.setState({ lampuJauhChecking: true });
                this.setState({
                  lampuJauhCheckingNote: null
                });
              } else if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[2].DATA_CHECKED
                  .CHECK_VAL == "BAIK"
              ) {
                this.setState({ lampuJauhChecking: true });
                this.setState({
                  lampuJauhCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[2].DATA_CHECKED.CHECK_NOTE
                });
              } else {
                this.setState({ lampuJauhChecking: false });
                this.setState({
                  lampuJauhCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[2].DATA_CHECKED.CHECK_NOTE
                });
              }
              if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[3].DATA_CHECKED ==
                null
              ) {
                this.setState({ lampuSeinKiriChecking: true });
                this.setState({
                  lampuSeinKiriCheckingNote: null
                });
              } else if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[3].DATA_CHECKED
                  .CHECK_VAL == "BAIK"
              ) {
                this.setState({ lampuSeinKiriChecking: true });
                this.setState({
                  lampuSeinKiriCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[3].DATA_CHECKED.CHECK_NOTE
                });
              } else {
                this.setState({ lampuSeinKiriChecking: false });
                this.setState({
                  lampuSeinKiriCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[3].DATA_CHECKED.CHECK_NOTE
                });
              }
              if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[4].DATA_CHECKED ==
                null
              ) {
                this.setState({ lampuSeinKananChecking: true });
                this.setState({
                  lampuSeinKananCheckingNote: null
                });
              } else if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[4].DATA_CHECKED
                  .CHECK_VAL == "BAIK"
              ) {
                this.setState({ lampuSeinKananChecking: true });
                this.setState({
                  lampuSeinKananCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[4].DATA_CHECKED.CHECK_NOTE
                });
              } else {
                this.setState({ lampuSeinKananChecking: false });
                this.setState({
                  lampuSeinKananCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[4].DATA_CHECKED.CHECK_NOTE
                });
              }
              if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[5].DATA_CHECKED ==
                null
              ) {
                this.setState({ lampuRemChecking: true });
                this.setState({
                  lampuRemCheckingNote: null
                });
              } else if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[5].DATA_CHECKED
                  .CHECK_VAL == "BAIK"
              ) {
                this.setState({ lampuRemChecking: true });
                this.setState({
                  lampuRemCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[5].DATA_CHECKED.CHECK_NOTE
                });
              } else {
                this.setState({ lampuRemChecking: false });
                this.setState({
                  lampuRemCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[5].DATA_CHECKED.CHECK_NOTE
                });
              }
              if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[6].DATA_CHECKED ==
                null
              ) {
                this.setState({ lampuAtretChecking: true });
                this.setState({
                  lampuAtretCheckingNote: null
                });
              } else if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[6].DATA_CHECKED
                  .CHECK_VAL == "BAIK"
              ) {
                this.setState({ lampuAtretChecking: true });
                this.setState({
                  lampuAtretCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[6].DATA_CHECKED.CHECK_NOTE
                });
              } else {
                this.setState({ lampuAtretChecking: false });
                this.setState({
                  lampuAtretCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[6].DATA_CHECKED.CHECK_NOTE
                });
              }
              if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[7].DATA_CHECKED ==
                null
              ) {
                this.setState({ lampuSorotChecking: true });
                this.setState({
                  lampuSorotCheckingNote: null
                });
              } else if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[7].DATA_CHECKED
                  .CHECK_VAL == "BAIK"
              ) {
                this.setState({ lampuSorotChecking: true });
                this.setState({
                  lampuSorotCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[7].DATA_CHECKED.CHECK_NOTE
                });
              } else {
                this.setState({ lampuSorotChecking: false });
                this.setState({
                  lampuSorotCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[7].DATA_CHECKED.CHECK_NOTE
                });
              }
              if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[8].DATA_CHECKED ==
                null
              ) {
                this.setState({ dashboardChecking: true });
                this.setState({
                  lampuDashboardCheckingNote: null
                });
              } else if (
                this.state.dataVehicleChecklist[5].SUB_ITEM[8].DATA_CHECKED
                  .CHECK_VAL == "BAIK"
              ) {
                this.setState({ dashboardChecking: true });
                this.setState({
                  lampuDashboardCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[8].DATA_CHECKED.CHECK_NOTE
                });
              } else {
                this.setState({ dashboardChecking: false });
                this.setState({
                  lampuDashboardCheckingNote: this.state.dataVehicleChecklist[5]
                    .SUB_ITEM[8].DATA_CHECKED.CHECK_NOTE
                });
              }

              if (
                this.state.dataVehicleChecklist[7].SUB_ITEM[0].DATA_CHECKED ==
                null
              ) {
                this.setState({ spionKiriChecking: true });
                this.setState({
                  kacaSpionKiriCheckingNote: null
                });
              } else if (
                this.state.dataVehicleChecklist[7].SUB_ITEM[0].DATA_CHECKED
                  .CHECK_VAL == "BAIK"
              ) {
                this.setState({ spionKiriChecking: true });
                this.setState({
                  kacaSpionKiriCheckingNote: this.state.dataVehicleChecklist[7]
                    .SUB_ITEM[0].DATA_CHECKED.CHECK_NOTE
                });
              } else {
                this.setState({ spionKiriChecking: false });
                this.setState({
                  kacaSpionKiriCheckingNote: this.state.dataVehicleChecklist[7]
                    .SUB_ITEM[0].DATA_CHECKED.CHECK_NOTE
                });
              }
              if (
                this.state.dataVehicleChecklist[7].SUB_ITEM[1].DATA_CHECKED ==
                null
              ) {
                this.setState({ spionKananChecking: true });
                this.setState({
                  kacaSpionKananCheckingNote: null
                });
              } else if (
                this.state.dataVehicleChecklist[7].SUB_ITEM[1].DATA_CHECKED
                  .CHECK_VAL == "BAIK"
              ) {
                this.setState({ spionKananChecking: true });
                this.setState({
                  kacaSpionKananCheckingNote: this.state.dataVehicleChecklist[7]
                    .SUB_ITEM[1].DATA_CHECKED.CHECK_NOTE
                });
              } else {
                this.setState({ spionKananChecking: false });
                this.setState({
                  kacaSpionKananCheckingNote: this.state.dataVehicleChecklist[7]
                    .SUB_ITEM[1].DATA_CHECKED.CHECK_NOTE
                });
              }
            }
          );
        })
        .catch(error => {
          console.log(error);
          this.setState({
            isLoading: false,
            visibleLoadingUnitKerja: false
          });
        });
    });
    // alert(this.state.spionKananChecking);
  }

  validasiFieldTab1() {
    console.log(this.state.dateChecking + " " + this.state.dateChecking);
    if (this.state.dateChecking == undefined) {
      alert("Field Tanggal Pemeriksaan Wajib Diisi");
    } else if (this.state.timeChecking == undefined) {
      alert("Field Jam Pemeriksaan Wajib Diisi");
    } else {
      this.setState({
        tabNumber: "tab2"
      });
    }
  }

  validasiFieldTab2() {
    this.setState({
      tabNumber: "tab3"
    });
  }

  submitPemanasanNote() {
    this.setState({
      visiblePemanasan: false,
      pemanasanCheckingNote: this.state.pemanasanCheckingNote
    });
  }

  submitSpeedometerNote() {
    this.setState({
      visibleSpeedometer: false,
      speedometerCheckingNote: this.state.speedometerCheckingNote
    });
  }

  submitBbmNote() {
    this.setState({
      visibleBbm: false,
      bbmCheckingNote: this.state.bbmCheckingNote
    });
  }

  submitOliMesinNote() {
    this.setState({
      visibleOliMesin: false,
      oliMesinCheckingNote: this.state.oliMesinCheckingNote
    });
  }

  submitOliRemNote() {
    this.setState({
      visibleOliRem: false,
      oliRemCheckingNote: this.state.oliRemCheckingNote
    });
  }

  submitOliPowerSteeringNote() {
    this.setState({
      visibleOliPowerSteering: false,
      oliPowerSteeringCheckingNote: this.state.oliPowerSteeringCheckingNote
    });
  }

  submitAirRadiatorNote() {
    this.setState({
      visibleAirRadiator: false,
      airRadiatorCheckingNote: this.state.airRadiatorCheckingNote
    });
  }

  submitAirCadanganRadiatorNote() {
    this.setState({
      visibleAirCadanganRadiator: false,
      airCadanganRadiatorCheckingNote: this.state
        .airCadanganRadiatorCheckingNote
    });
  }

  submitAirWiperNote() {
    this.setState({
      visibleAirWiper: false,
      airWiperCheckingNote: this.state.airWiperCheckingNote
    });
  }

  submitBanDepanKiriNote() {
    this.setState({
      visibleBanDepanKiri: false,
      banDepanKiriCheckingNote: this.state.banDepanKiriCheckingNote
    });
  }

  submitBanDepanKananNote() {
    this.setState({
      visibleBanDepanKanan: false,
      banDepanKananCheckingNote: this.state.banDepanKananCheckingNote
    });
  }

  submitBanBelakangKiriDalamNote() {
    this.setState({
      visibleBanBelakangKiriDalam: false,
      banBelakangKiriDalamCheckingNote: this.state
        .banBelakangKiriDalamCheckingNote
    });
  }

  submitBanBelakangKananDalamNote() {
    this.setState({
      visibleBanBelakangKananDalam: false,
      banBelakangKananDalamCheckingNote: this.state
        .banBelakangKananDalamCheckingNote
    });
  }

  submitBanBelakangKiriLuarNote() {
    this.setState({
      visibleBanBelakangKiriLuar: false,
      banBelakangKiriLuarCheckingNote: this.state
        .banBelakangKiriLuarCheckingNote
    });
  }

  submitBanBelakangKananLuarNote() {
    this.setState({
      visibleBanBelakangKananLuar: false,
      banBelakangKananLuarCheckingNote: this.state
        .banBelakangKananLuarCheckingNote
    });
  }

  submitLampuCabinNote() {
    this.setState({
      visibleLampuCabin: false,
      lampuCabinCheckingNote: this.state.lampuCabinCheckingNote
    });
  }

  submitLampuKotaNote() {
    this.setState({
      visibleLampuKota: false,
      lampuKotaCheckingNote: this.state.lampuKotaCheckingNote
    });
  }

  submitLampuJauhNote() {
    this.setState({
      visibleLampuJauh: false,
      lampuJauhCheckingNote: this.state.lampuJauhCheckingNote
    });
  }

  submitLampuSeinKiriNote() {
    this.setState({
      visibleLampuSeinKiri: false,
      lampuSeinKiriCheckingNote: this.state.lampuSeinKiriCheckingNote
    });
  }

  submitLampuSeinKananNote() {
    this.setState({
      visibleLampuSeinKanan: false,
      lampuSeinKananCheckingNote: this.state.lampuSeinKananCheckingNote
    });
  }

  submitLampuRemNote() {
    this.setState({
      visibleLampuRem: false,
      lampuRemCheckingNote: this.state.lampuRemCheckingNote
    });
  }

  submitLampuAtretNote() {
    this.setState({
      visibleLampuAtret: false,
      lampuAtretCheckingNote: this.state.lampuAtretCheckingNote
    });
  }

  submitLampuSorotNote() {
    this.setState({
      visibleLampuSorot: false,
      lampuSorotCheckingNote: this.state.lampuSorotCheckingNote
    });
  }

  submitDashboardNote() {
    this.setState({
      visibleLampuDashboard: false,
      lampuDashboardCheckingNote: this.state.lampuDashboardCheckingNote
    });
  }

  submitKacaSpionKiriNote() {
    this.setState({
      visibleKacaSpionKiri: false,
      kacaSpionKiriCheckingNote: this.state.kacaSpionKiriCheckingNote
    });
  }

  submitKacaSpionKananNote() {
    this.setState({
      visibleKacaSpionKanan: false,
      kacaSpionKananCheckingNote: this.state.kacaSpionKananCheckingNote
    });
  }

  unitCheckPickupCreate() {
    this.setState({
      visibleDialogSubmit: true
    });
    AsyncStorage.getItem("token").then(value => {
      var url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/firesystem/unit_check/checking_unit/save";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID_VEHICLE", this.state.headerUnitCheck.ID_VEHICLE);
      formData.append("REPORT_DATE", this.state.dateChecking);
      formData.append("REPORT_SHIFT", this.state.shiftChecking);
      formData.append("REPORT_CLOCK", this.state.timeChecking);
      // alert(this.state.pemanasanCheckingNote)
      formData.append("ID_STANDARD[0]", 33);
      formData.append("CHECK_VALUE[0]", this.state.pemanasanChecking);
      formData.append("CHECK_NOTE[0]", this.state.pemanasanCheckingNote);
      formData.append("IS_NEGATIF[0]", this.state.isNegatifPemanasan);
      formData.append("OPT_FILE[0]", null);

      formData.append("ID_STANDARD[1]", 34);
      formData.append("CHECK_VALUE[1]", this.state.speedometerChecking);
      formData.append("CHECK_NOTE[1]", this.state.speedometerCheckingNote);
      formData.append("IS_NEGATIF[1]", this.state.isNegatifSpeedometer);
      formData.append("OPT_FILE[1]", null);

      formData.append("ID_STANDARD[2]", 35);
      formData.append("CHECK_VALUE[2]", this.state.bbmChecking);
      formData.append("CHECK_NOTE[2]", this.state.bbmCheckingNote);
      formData.append("IS_NEGATIF[2]", this.state.isNegatifBbm);
      formData.append("OPT_FILE[2]", null);

      formData.append("ID_STANDARD[3]", 40);
      formData.append("CHECK_VALUE[3]", this.state.oliMesinChecking);
      formData.append("CHECK_NOTE[3]", this.state.oliMesinCheckingNote);
      formData.append("IS_NEGATIF[3]", this.state.isNegatifOliMesin);
      formData.append("OPT_FILE[3]", null);

      formData.append("ID_STANDARD[4]", 41);
      formData.append("CHECK_VALUE[4]", this.state.oliRemChecking);
      formData.append("CHECK_NOTE[4]", this.state.oliRemCheckingNote);
      formData.append("IS_NEGATIF[4]", this.state.isNegatifOliRem);
      formData.append("OPT_FILE[4]", null);

      formData.append("ID_STANDARD[5]", 42);
      formData.append("CHECK_VALUE[5]", this.state.oliPowerSteeringChecking);
      formData.append("CHECK_NOTE[5]", this.state.oliPowerSteeringCheckingNote);
      formData.append("IS_NEGATIF[5]", this.state.isNegatifOliPowerSteering);
      formData.append("OPT_FILE[5]", null);

      formData.append("ID_STANDARD[6]", 44);
      formData.append("CHECK_VALUE[6]", this.state.airRadiatorChecking);
      formData.append("CHECK_NOTE[6]", this.state.airRadiatorCheckingNote);
      formData.append("IS_NEGATIF[6]", this.state.isNegatifAirRadiator);
      formData.append("OPT_FILE[6]", null);

      formData.append("ID_STANDARD[7]", 45);
      formData.append("CHECK_VALUE[7]", this.state.airCadanganRadiatorChecking);
      formData.append(
        "CHECK_NOTE[7]",
        this.state.airCadanganRadiatorCheckingNote
      );
      formData.append("IS_NEGATIF[7]", this.state.isNegatifAirCadanganRadiator);
      formData.append("OPT_FILE[7]", null);

      formData.append("ID_STANDARD[8]", 46);
      formData.append("CHECK_VALUE[8]", this.state.airWiperChecking);
      formData.append("CHECK_NOTE[8]", this.state.airWiperCheckingNote);
      formData.append("IS_NEGATIF[8]", this.state.isNegatifAirWiper);
      formData.append("OPT_FILE[8]", null);

      formData.append("ID_STANDARD[9]", 58);
      formData.append("CHECK_VALUE[9]", this.state.banDepanKiriChecking);
      formData.append("CHECK_NOTE[9]", this.state.banDepanKiriCheckingNote);
      formData.append("IS_NEGATIF[9]", this.state.isNegatifBanDepanKiri);
      formData.append("OPT_FILE[9]", null);

      formData.append("ID_STANDARD[10]", 59);
      formData.append("CHECK_VALUE[10]", this.state.banDepanKananChecking);
      formData.append("CHECK_NOTE[10]", this.state.banDepanKananCheckingNote);
      formData.append("IS_NEGATIF[10]", this.state.isNegatifBanDepanKanan);
      formData.append("OPT_FILE[10]", null);

      formData.append("ID_STANDARD[11]", 60);
      formData.append(
        "CHECK_VALUE[11]",
        this.state.banBelakangKiriDalamChecking
      );
      formData.append(
        "CHECK_NOTE[11]",
        this.state.banBelakangKiriDalamCheckingNote
      );
      formData.append(
        "IS_NEGATIF[11]",
        this.state.isNegatifBanBelakangKiriDalam
      );
      formData.append("OPT_FILE[11]", null);

      formData.append("ID_STANDARD[12]", 61);
      formData.append(
        "CHECK_VALUE[12]",
        this.state.banBelakangKiriLuarChecking
      );
      formData.append(
        "CHECK_NOTE[12]",
        this.state.banBelakangKiriLuarCheckingNote
      );
      formData.append(
        "IS_NEGATIF[12]",
        this.state.isNegatifBanBelakangKiriLuar
      );
      formData.append("OPT_FILE[12]", null);

      formData.append("ID_STANDARD[13]", 62);
      formData.append(
        "CHECK_VALUE[13]",
        this.state.banBelakangKananDalamChecking
      );
      formData.append(
        "CHECK_NOTE[13]",
        this.state.banBelakangKananDalamCheckingNote
      );
      formData.append(
        "IS_NEGATIF[13]",
        this.state.isNegatifBanBelakangKananDalam
      );
      formData.append("OPT_FILE[13]", null);

      formData.append("ID_STANDARD[14]", 63);
      formData.append(
        "CHECK_VALUE[14]",
        this.state.banBelakangKananLuarChecking
      );
      formData.append(
        "CHECK_NOTE[14]",
        this.state.banBelakangKananLuarCheckingNote
      );
      formData.append(
        "IS_NEGATIF[14]",
        this.state.isNegatifBanBelakangKananLuar
      );
      formData.append("OPT_FILE[14]", null);

      formData.append("ID_STANDARD[15]", 48);
      if (this.state.lampuCabinChecking == true) {
        formData.append("CHECK_VALUE[15]", "BAIK");
        formData.append("IS_NEGATIF[15]", 0);
        formData.append("CHECK_NOTE[15]", null);
        formData.append("OPT_FILE[15]", null);
      } else {
        formData.append("CHECK_VALUE[15]", "MATI");
        formData.append("IS_NEGATIF[15]", 1);
        formData.append("CHECK_NOTE[15]", this.state.lampuCabinCheckingNote);
        formData.append("OPT_FILE[15]", null);
      }

      formData.append("ID_STANDARD[16]", 49);
      if (this.state.lampuKotaChecking) {
        formData.append("CHECK_VALUE[16]", "BAIK");
        formData.append("IS_NEGATIF[16]", 0);
        formData.append("CHECK_NOTE[16]", null);
        formData.append("OPT_FILE[16]", null);
      } else {
        formData.append("CHECK_VALUE[16]", "MATI");
        formData.append("IS_NEGATIF[16]", 1);
        formData.append("CHECK_NOTE[16]", this.state.lampuKotaCheckingNote);
        formData.append("OPT_FILE[16]", null);
      }

      formData.append("ID_STANDARD[17]", 50);
      if (this.state.lampuJauhChecking == true) {
        formData.append("CHECK_VALUE[17]", "BAIK");
        formData.append("IS_NEGATIF[17]", 0);
        formData.append("CHECK_NOTE[17]", null);
        formData.append("OPT_FILE[17]", null);
      } else {
        formData.append("CHECK_VALUE[17]", "MATI");
        formData.append("IS_NEGATIF[17]", 1);
        formData.append("CHECK_NOTE[17]", this.state.lampuJauhCheckingNote);
        formData.append("OPT_FILE[17]", null);
      }

      formData.append("ID_STANDARD[18]", 51);
      if (this.state.lampuSeinKiriChecking == true) {
        formData.append("CHECK_VALUE[18]", "BAIK");
        formData.append("IS_NEGATIF[18]", 0);
        formData.append("CHECK_NOTE[18]", null);
        formData.append("OPT_FILE[18]", null);
      } else {
        formData.append("CHECK_VALUE[18]", "MATI");
        formData.append("IS_NEGATIF[18]", 1);
        formData.append("CHECK_NOTE[18]", this.state.lampuSeinKiriCheckingNote);
        formData.append("OPT_FILE[18]", null);
      }

      formData.append("ID_STANDARD[19]", 52);
      if (this.state.lampuSeinKananChecking == true) {
        formData.append("CHECK_VALUE[19]", "BAIK");
        formData.append("IS_NEGATIF[19]", 0);
        formData.append("CHECK_NOTE[19]", null);
        formData.append("OPT_FILE[19]", null);
      } else {
        formData.append("CHECK_VALUE[19]", "MATI");
        formData.append("IS_NEGATIF[19]", 1);
        formData.append(
          "CHECK_NOTE[19]",
          this.state.lampuSeinKananCheckingNote
        );
        formData.append("OPT_FILE[19]", null);
      }

      formData.append("ID_STANDARD[20]", 53);
      if (this.state.lampuRemChecking == true) {
        formData.append("CHECK_VALUE[20]", "BAIK");
        formData.append("IS_NEGATIF[20]", 0);
        formData.append("CHECK_NOTE[20]", null);
        formData.append("OPT_FILE[20]", null);
      } else {
        formData.append("CHECK_VALUE[20]", "MATI");
        formData.append("IS_NEGATIF[20]", 1);
        formData.append("CHECK_NOTE[20]", this.state.lampuRemCheckingNote);
        formData.append("OPT_FILE[20]", null);
      }

      formData.append("ID_STANDARD[21]", 54);
      if (this.state.lampuAtretChecking == true) {
        formData.append("CHECK_VALUE[21]", "BAIK");
        formData.append("IS_NEGATIF[21]", 0);
        formData.append("CHECK_NOTE[21]", null);
        formData.append("OPT_FILE[21]", null);
      } else {
        formData.append("CHECK_VALUE[21]", "MATI");
        formData.append("IS_NEGATIF[21]", 1);
        formData.append("CHECK_NOTE[21]", this.state.lampuAtretCheckingNote);
        formData.append("OPT_FILE[21]", null);
      }

      formData.append("ID_STANDARD[22]", 55);
      if (this.state.lampuSorotChecking == true) {
        formData.append("CHECK_VALUE[22]", "BAIK");
        formData.append("IS_NEGATIF[22]", 0);
        formData.append("CHECK_NOTE[22]", null);
        formData.append("OPT_FILE[22]", null);
      } else {
        formData.append("CHECK_VALUE[22]", "MATI");
        formData.append("IS_NEGATIF[22]", 1);
        formData.append("CHECK_NOTE[22]", this.state.lampuSorotCheckingNote);
        formData.append("OPT_FILE[22]", null);
      }

      formData.append("ID_STANDARD[23]", 56);
      if (this.state.dashboardChecking == true) {
        formData.append("CHECK_VALUE[23]", "BAIK");
        formData.append("IS_NEGATIF[23]", 0);
        formData.append("CHECK_NOTE[23]", null);
        formData.append("OPT_FILE[23]", null);
      } else {
        formData.append("CHECK_VALUE[23]", "MATI");
        formData.append("IS_NEGATIF[23]", 1);
        formData.append(
          "CHECK_NOTE[23]",
          this.state.lampuDashboardCheckingNote
        );
        formData.append("OPT_FILE[23]", null);
      }

      formData.append("ID_STANDARD[24]", 65);
      if (this.state.spionKiriChecking == true) {
        formData.append("CHECK_VALUE[24]", "BAIK");
        formData.append("IS_NEGATIF[24]", 0);
        formData.append("CHECK_NOTE[24]", null);
        formData.append("OPT_FILE[24]", null);
      } else {
        formData.append("CHECK_VALUE[24]", "PECAH");
        formData.append("IS_NEGATIF[24]", 1);
        formData.append("CHECK_NOTE[24]", this.state.kacaSpionKiriCheckingNote);
        formData.append("OPT_FILE[24]", null);
      }

      formData.append("ID_STANDARD[25]", 66);
      if (this.state.spionKananChecking == true) {
        formData.append("CHECK_VALUE[25]", "BAIK");
        formData.append("IS_NEGATIF[25]", 0);
        formData.append("CHECK_NOTE[25]", null);
        formData.append("OPT_FILE[25]", null);
      } else {
        formData.append("CHECK_VALUE[25]", "PECAH");
        formData.append("IS_NEGATIF[25]", 1);
        formData.append(
          "CHECK_NOTE[25]",
          this.state.kacaSpionKananCheckingNote
        );
        formData.append("OPT_FILE[25]", null);
      }
      console.log(formData)

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // console.log(responseJson)
          if (responseJson.Status == 200) {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Success", "Update Unit Check Pick Up Success", [
              {
                text: "Okay"
              }
            ]);
            this.props.navigation.navigate("UnitCheckPickup");
          } else {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Error", "Update Unit Check Pick Up Failed", [
              {
                text: "Okay"
              }
            ]);
          }
          // alert(responseJson);
        })
        .catch(error => {
          this.setState({
            visibleDialogSubmit: false
          });
          Alert.alert("Error", "Update Unit Check Pick Up Failed", [
            {
              text: "Okay"
            }
          ]);
          this.props.navigation.navigate("UnitCheckPickup");
          console.log(error);
        });
    });
  }

  onChangeDate(date) {
    this.setState({
      dateChecking: date
    });
    this.loadData();
  }

  onChangeShift(shift) {
    this.setState({
      shiftChecking: shift
    });
    this.loadData();
  }

  render() {
    that = this;
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left>
            {this.state.tabNumber == "tab1" ? (
              <Button
                transparent
                onPress={() => this.props.navigation.goBack()}
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            ) : this.state.tabNumber == "tab2" ? (
              <Button
                transparent
                onPress={() =>
                  this.setState({
                    tabNumber: "tab1"
                  })
                }
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            ) : (
              <Button
                transparent
                onPress={() =>
                  this.setState({
                    tabNumber: "tab2"
                  })
                }
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            )}
          </Left>
          <Body>
            <Title style={styles.textbody}>Unit Check Mobil Siaga</Title>
          </Body>
          {/* <Right/> */}
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ flex: 1 }}>
          <Content style={{ marginTop: 0, backgroundColor: colors.gray }}>
            <View style={{ backgroundColor: colors.gray }}>
              {this.state.tabNumber == "tab1" ? (
                <View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Pemeriksaan
                        </Text>
                      </View>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: colors.graydar
                          }}
                        >
                          Kondisi Pickup
                        </Text>
                      </View>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: colors.graydar
                          }}
                        >
                          Kondisi Sparepart
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={styles.titleInput}>Vehicle Code *</Text>
                      </View>
                      <TextInput
                        style={{
                          height: 40,
                          marginLeft: 5,
                          marginRight: 5,
                          marginBottom: 5,
                          fontSize: 11,
                          borderWidth: 1,
                          borderColor: colors.lightGray
                        }}
                        onChangeText={text =>
                          this.setState({ vehicleCode: text })
                        }
                        value={this.state.headerUnitCheck.VEHICLE_CODE}
                        bordered
                        editable={false}
                        placeholder="Vehicle Code"
                      />
                    </View>
                  </CardItem>
                  {/* <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleDialogShift}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleDialogShift: false });
                        // this.props.navigation.goBack()};
                      }}
                      dialogTitle={
                        <DialogTitle title="Select date and shift here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          onPress={() => this.loadData()}
                          // onPress={() => this.setState({ visibleFilter: false })}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <View>
                            <Form
                              style={{
                                borderWidth: 1,
                                borderRadius: 5,
                                marginRight: 5,
                                marginLeft: 5,
                                marginTop: 15,
                                marginBottom: 10,
                                borderColor: "#E6E6E6",
                                // height: 50,
                                width: 250
                              }}
                            >
                              <Text style={styles.titleInput}>
                                Shift Pemeriksaan *
                              </Text>
                              <Picker
                                mode="dropdown"
                                iosIcon={
                                  <Icon
                                    name={
                                      Platform.OS
                                        ? "ios-arrow-down"
                                        : "ios-arrow-down-outline"
                                    }
                                  />
                                }
                                style={{ width: "100%", height: 35 }}
                                placeholder="Select Shift ..."
                                placeholderStyle={{ color: "#bfc6ea" }}
                                placeholderIconColor="#007aff"
                                selectedValue={this.state.shiftChecking}
                                onValueChange={itemValue =>
                                  this.setState({ shiftChecking: itemValue })
                                }
                              >
                                <Picker.Item label="Shift I (Satu)" value="1" />
                                <Picker.Item label="Shift II (Dua)" value="2" />
                                <Picker.Item
                                  label="Shift III (Tiga)"
                                  value="3"
                                />
                              </Picker>
                            </Form>
                            <Text style={styles.titleInput}>
                              Tanggal Pemeriksaan *
                            </Text>
                            <DatePicker
                              style={{
                                width: "100%",
                                fontSize: 10,
                                borderRadius: 20
                              }}
                              date={this.state.dateChecking}
                              mode="date"
                              placeholder="Choose Date ..."
                              format="YYYY-MM-DD"
                              minDate="2018-01-01"
                              maxDate="2050-12-31"
                              confirmBtnText="Confirm"
                              cancelBtnText="Cancel"
                              // iconSource='{this.timeIcon}'
                              customStyles={{
                                dateInput: {
                                  marginLeft: 5,
                                  marginRight: 5,
                                  height: 35,
                                  borderRadius: 5,
                                  fontSize: 10,
                                  borderWidth: 1,
                                  borderColor: "#E6E6E6"
                                },
                                dateIcon: {
                                  position: "absolute",
                                  left: 0,
                                  top: 5
                                }
                              }}
                              onDateChange={date => {
                                this.setState({ dateChecking: date });
                              }}
                            />
                            <Text style={styles.titleInput}>
                              Jam Pemeriksaan *
                            </Text>
                            <DatePicker
                              style={{
                                width: "100%",
                                fontSize: 10,
                                borderRadius: 20
                              }}
                              date={this.state.timeChecking}
                              mode="time"
                              placeholder="Choose Time ..."
                              format="hh:mm"
                              minDate="00:00"
                              maxDate="23:59"
                              confirmBtnText="Confirm"
                              cancelBtnText="Cancel"
                              // iconSource='{this.timeIcon}'
                              customStyles={{
                                dateInput: {
                                  marginLeft: 5,
                                  marginRight: 5,
                                  height: 35,
                                  borderRadius: 5,
                                  fontSize: 10,
                                  borderWidth: 1,
                                  borderColor: "#E6E6E6"
                                },
                                dateIcon: {
                                  position: "absolute",
                                  left: 0,
                                  top: 5
                                }
                              }}
                              onDateChange={time => {
                                this.setState({ timeChecking: time });
                              }}
                            />
                          </View>
                        }
                      </DialogContent>
                    </Dialog>
                  </View> */}

                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "50%" }}>
                        <Text style={styles.titleInput}>
                          Tanggal Pemeriksaan *
                        </Text>
                        <DatePicker
                          style={{
                            width: "100%",
                            fontSize: 10,
                            borderRadius: 20
                          }}
                          date={this.state.dateChecking}
                          mode="date"
                          placeholder="Choose Date ..."
                          format="YYYY-MM-DD"
                          minDate="2018-01-01"
                          maxDate="2050-12-31"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          // iconSource='{this.timeIcon}'
                          customStyles={{
                            dateInput: {
                              marginLeft: 5,
                              marginRight: 5,
                              height: 35,
                              borderRadius: 5,
                              fontSize: 10,
                              borderWidth: 1,
                              borderColor: "#E6E6E6"
                            },
                            dateIcon: {
                              position: "absolute",
                              left: 0,
                              top: 5
                            }
                          }}
                          onDateChange={date => {
                            this.onChangeDate(date);
                          }}
                        />
                      </View>
                      <View style={{ width: "50%" }}>
                        <Text style={styles.titleInput}>Jam Pemeriksaan *</Text>
                        <DatePicker
                          style={{
                            width: "100%",
                            fontSize: 10,
                            borderRadius: 20
                          }}
                          date={this.state.timeChecking}
                          mode="time"
                          placeholder="Choose Time ..."
                          format="hh:mm"
                          minTime="00:00"
                          maxTime="23:59"
                          confirmBtnText="Confirm"
                          cancelBtnText="Cancel"
                          // iconSource='{this.timeIcon}'
                          customStyles={{
                            dateInput: {
                              marginLeft: 5,
                              marginRight: 5,
                              height: 35,
                              borderRadius: 5,
                              fontSize: 10,
                              borderWidth: 1,
                              borderColor: "#E6E6E6"
                            },
                            dateIcon: {
                              position: "absolute",
                              left: 0,
                              top: 5
                            }
                          }}
                          onDateChange={time => {
                            this.setState({ timeChecking: time });
                          }}
                        />
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View>
                        <Text style={styles.titleInput}>Shift *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: "100%", height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.shiftChecking}
                            onValueChange={itemValue =>
                              this.onChangeShift(itemValue)
                            }
                          >
                            <Picker.Item label="Shift I (Satu)" value="1" />
                            <Picker.Item label="Shift II (Dua)" value="2" />
                            <Picker.Item label="Shift III (Tiga)" value="3" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Pemanasan (Menit)
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            value={this.state.pemanasanChecking}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ pemanasanChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifPemanasan}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifPemanasan: itemValue,
                                  visiblePemanasan: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visiblePemanasan}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visiblePemanasan: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitPemanasanNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.pemanasanCheckingNote}
                                placeholder="Catatan pemanasan (Optional)"
                                onChangeText={text =>
                                  this.setState({ pemanasanCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Speedometer (Km)
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            value={this.state.speedometerChecking}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ speedometerChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifSpeedometer}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifSpeedometer: itemValue,
                                  visibleSpeedometer: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleSpeedometer}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleSpeedometer: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitSpeedometerNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.speedometerCheckingNote}
                                placeholder="Catatan speedometer (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    speedometerCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View>
                        <Text style={styles.titleInput}>Level BBM</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 25,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.bbmChecking}
                            onValueChange={itemValue =>
                              this.setState({ bbmChecking: itemValue })
                            }
                          >
                            <Picker.Item label="R" value="R" />
                            <Picker.Item label="1/4" value="1/4" />
                            <Picker.Item label="1/2" value="1/2" />
                            <Picker.Item label="3/4" value="3/4" />
                            <Picker.Item label="F" value="F" />
                          </Picker>
                        </Form>
                      </View>
                      <View>
                        <Text style={styles.titleInput}>Masuk Negatif *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ height: 35, width: 200 }}
                            placeholder="Select ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.isNegatifBbm}
                            onValueChange={itemValue =>
                              this.setState({
                                isNegatifBbm: itemValue,
                                visibleBbm: true
                              })
                            }
                          >

                            <Picker.Item
                              label="Tidak Masuk Negatif List"
                              value="0"
                              disable={true}
                            />
                            <Picker.Item label="Masuk Negatif List" value="1" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleBbm}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleBbm: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitBbmNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.bbmCheckingNote}
                                placeholder="Catatan bbm (Optional)"
                                onChangeText={text =>
                                  this.setState({ bbmCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  {/* <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                        <View style={{ flex: 1 }}>
                                            <View style={{ paddingBottom: 5 }}>
                                                <Text style={styles.viewMerkFoto}>Upload Foto (jpg, png) max : 500 kb</Text>
                                            </View>
                                            <View>
                                                <Ripple
                                                    style={{
                                                        flex: 2,
                                                        justifyContent: "center",
                                                        alignItems: "center"
                                                    }}
                                                    rippleSize={176}
                                                    rippleDuration={600}
                                                    rippleContainerBorderRadius={15}
                                                    onPress={this.pickImageHandler}
                                                    rippleColor={colors.accent}
                                                >
                                                    <View style={styles.placeholder}>
                                                        <Image source={{ uri: this.state.pickedImage }} style={styles.previewImage} />
                                                    </View>
                                                </Ripple>
                                            </View>
                                        </View>
                                    </CardItem> */}
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View style={styles.Contentsave}>
                        <Button
                          block
                          style={{
                            width: "100%",
                            height: 45,
                            marginBottom: 20,
                            marginTop: 15,
                            borderWidth: 1,
                            backgroundColor: "#00b300",
                            borderColor: "#00b300",
                            borderRadius: 4
                          }}
                          onPress={() => this.validasiFieldTab1()}
                        >
                          <Text
                            style={{ fontWeight: "bold", color: colors.white }}
                          >
                            Lanjutkan
                          </Text>
                        </Button>
                      </View>
                    </View>
                  </CardItem>
                </View>
              ) : this.state.tabNumber == "tab2" ? (
                <View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Pemeriksaan
                        </Text>
                      </View>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Kondisi Pickup
                        </Text>
                      </View>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.graydar,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: colors.graydar
                          }}
                        >
                          Kondisi Sparepart
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ marginLeft: 0, flex: 1 }}>
                      <Text
                        style={{
                          fontSize: 12,
                          paddingTop: 10,
                          fontWeight: "bold"
                        }}
                      >
                        Kondisi Oli :{" "}
                      </Text>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View>
                        <Text style={styles.titleInput}>Oli Mesin</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 25,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.oliMesinChecking}
                            onValueChange={itemValue =>
                              this.setState({ oliMesinChecking: itemValue })
                            }
                          >
                            <Picker.Item label="L" value="L" />
                            <Picker.Item label="M" value="M" />
                            <Picker.Item label="H" value="H" />
                          </Picker>
                        </Form>
                      </View>
                      <View>
                        <Text style={styles.titleInput}>Masuk Negatif *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ height: 35, width: 200 }}
                            placeholder="Select ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.isNegatifOliMesin}
                            onValueChange={itemValue =>
                              this.setState({
                                isNegatifOliMesin: itemValue,
                                visibleOliMesin: true
                              })
                            }
                          >
                            <Picker.Item
                              label="Tidak Masuk Negatif List"
                              value="0"
                            />
                            <Picker.Item label="Masuk Negatif List" value="1" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleOliMesin}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleOliMesin: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitOliMesinNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.oliMesinCheckingNote}
                                placeholder="Catatan oli mesin (Optional)"
                                onChangeText={text =>
                                  this.setState({ oliMesinCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View>
                        <Text style={styles.titleInput}>Oli Rem</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 25,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.oliRemChecking}
                            onValueChange={itemValue =>
                              this.setState({ oliRemChecking: itemValue })
                            }
                          >
                            <Picker.Item label="L" value="L" />
                            <Picker.Item label="M" value="M" />
                            <Picker.Item label="H" value="H" />
                          </Picker>
                        </Form>
                      </View>
                      <View>
                        <Text style={styles.titleInput}>Masuk Negatif *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ height: 35, width: 200 }}
                            placeholder="Select ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.isNegatifOliRem}
                            onValueChange={itemValue =>
                              this.setState({
                                isNegatifOliRem: itemValue,
                                visibleOliRem: true
                              })
                            }
                          >
                            <Picker.Item
                              label="Tidak Masuk Negatif List"
                              value="0"
                            />
                            <Picker.Item label="Masuk Negatif List" value="1" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleOliRem}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleOliRem: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitOliRemNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.oliRemCheckingNote}
                                placeholder="Catatan oli rem (Optional)"
                                onChangeText={text =>
                                  this.setState({ oliRemCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View>
                        <Text style={styles.titleInput}>
                          Oli Power Steering
                        </Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 25,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.oliPowerSteeringChecking}
                            onValueChange={itemValue =>
                              this.setState({
                                oliPowerSteeringChecking: itemValue
                              })
                            }
                          >
                            <Picker.Item label="L" value="L" />
                            <Picker.Item label="M" value="M" />
                            <Picker.Item label="H" value="H" />
                          </Picker>
                        </Form>
                      </View>
                      <View>
                        <Text style={styles.titleInput}>Masuk Negatif *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ height: 35, width: 200 }}
                            placeholder="Select ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.isNegatifOliPowerSteering}
                            onValueChange={itemValue =>
                              this.setState({
                                isNegatifOliPowerSteering: itemValue,
                                visibleOliPowerSteering: true
                              })
                            }
                          >
                            <Picker.Item
                              label="Tidak Masuk Negatif List"
                              value="0"
                            />
                            <Picker.Item label="Masuk Negatif List" value="1" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleOliPowerSteering}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleOliPowerSteering: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitOliPowerSteeringNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.oliPowerSteeringCheckingNote}
                                placeholder="Catatan oli power steering (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    oliPowerSteeringCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ marginLeft: 0, flex: 1 }}>
                      <Text
                        style={{
                          fontSize: 12,
                          paddingTop: 10,
                          fontWeight: "bold"
                        }}
                      >
                        Kondisi Air :{" "}
                      </Text>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View>
                        <Text style={styles.titleInput}>Air Radiator</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 25,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.airRadiatorChecking}
                            onValueChange={itemValue =>
                              this.setState({ airRadiatorChecking: itemValue })
                            }
                          >
                            <Picker.Item label="R" value="R" />
                            <Picker.Item label="1/4" value="1/4" />
                            <Picker.Item label="1/2" value="1/2" />
                            <Picker.Item label="3/4" value="3/4" />
                            <Picker.Item label="F" value="F" />
                          </Picker>
                        </Form>
                      </View>
                      <View>
                        <Text style={styles.titleInput}>Masuk Negatif *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ height: 35, width: 200 }}
                            placeholder="Select ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.isNegatifAirRadiator}
                            onValueChange={itemValue =>
                              this.setState({
                                isNegatifAirRadiator: itemValue,
                                visibleAirRadiator: true
                              })
                            }
                          >
                            <Picker.Item
                              label="Tidak Masuk Negatif List"
                              value="0"
                            />
                            <Picker.Item label="Masuk Negatif List" value="1" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleAirRadiator}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleAirRadiator: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitAirRadiatorNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.airRadiatorCheckingNote}
                                placeholder="Catatan air radiator (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    airRadiatorCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View>
                        <Text style={styles.titleInput}>
                          Air Cadangan Radiator
                        </Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 25,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={
                              this.state.airCadanganRadiatorChecking
                            }
                            onValueChange={itemValue =>
                              this.setState({
                                airCadanganRadiatorChecking: itemValue
                              })
                            }
                          >
                            <Picker.Item label="R" value="R" />
                            <Picker.Item label="1/4" value="1/4" />
                            <Picker.Item label="1/2" value="1/2" />
                            <Picker.Item label="3/4" value="3/4" />
                            <Picker.Item label="F" value="F" />
                          </Picker>
                        </Form>
                      </View>
                      <View>
                        <Text style={styles.titleInput}>Masuk Negatif *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ height: 35, width: 200 }}
                            placeholder="Select ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={
                              this.state.isNegatifAirCadanganRadiator
                            }
                            onValueChange={itemValue =>
                              this.setState({
                                isNegatifAirCadanganRadiator: itemValue,
                                visibleAirCadanganRadiator: true
                              })
                            }
                          >
                            <Picker.Item
                              label="Tidak Masuk Negatif List"
                              value="0"
                            />
                            <Picker.Item label="Masuk Negatif List" value="1" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleAirCadanganRadiator}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleAirCadanganRadiator: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitAirCadanganRadiatorNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={
                                  this.state.airCadanganRadiatorCheckingNote
                                }
                                placeholder="Catatan air cadangan radiator (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    airCadanganRadiatorCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View>
                        <Text style={styles.titleInput}>Air Wiper</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 25,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ width: 100, height: 35 }}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.airWiperChecking}
                            onValueChange={itemValue =>
                              this.setState({ airWiperChecking: itemValue })
                            }
                          >
                            <Picker.Item label="R" value="R" />
                            <Picker.Item label="1/4" value="1/4" />
                            <Picker.Item label="1/2" value="1/2" />
                            <Picker.Item label="3/4" value="3/4" />
                            <Picker.Item label="F" value="F" />
                          </Picker>
                        </Form>
                      </View>
                      <View>
                        <Text style={styles.titleInput}>Masuk Negatif *</Text>
                        <Form
                          style={{
                            borderWidth: 1,
                            borderRadius: 5,
                            marginTop: 5,
                            marginRight: 5,
                            marginLeft: 5,
                            borderColor: "#E6E6E6",
                            height: 35
                          }}
                        >
                          <Picker
                            mode="dropdown"
                            iosIcon={
                              <Icon
                                name={
                                  Platform.OS
                                    ? "ios-arrow-down"
                                    : "ios-arrow-down-outline"
                                }
                              />
                            }
                            style={{ height: 35, width: 200 }}
                            placeholder="Select ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.isNegatifAirWiper}
                            onValueChange={itemValue =>
                              this.setState({
                                isNegatifAirWiper: itemValue,
                                visibleAirWiper: true
                              })
                            }
                          >
                            <Picker.Item
                              label="Tidak Masuk Negatif List"
                              value="0"
                            />
                            <Picker.Item label="Masuk Negatif List" value="1" />
                          </Picker>
                        </Form>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleAirWiper}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleAirWiper: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitAirWiperNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.airWiperCheckingNote}
                                placeholder="Catatan air wiper (Optional)"
                                onChangeText={text =>
                                  this.setState({ airWiperCheckingNote: text })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ marginLeft: 0, flex: 1 }}>
                      <Text
                        style={{
                          fontSize: 12,
                          paddingTop: 10,
                          fontWeight: "bold"
                        }}
                      >
                        Kondisi Tekanan Ban (Bar) :{" "}
                      </Text>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>Ban Depan Kiri</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            value={this.state.banDepanKiriChecking}
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banDepanKiriChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifBanDepanKiri}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifBanDepanKiri: itemValue,
                                  visibleBanDepanKiri: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleBanDepanKiri}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleBanDepanKiri: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitBanDepanKiriNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.banDepanKiriCheckingNote}
                                placeholder="Catatan ban depan kiri (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    banDepanKiriCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>Ban Depan Kanan</Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            value={this.state.banDepanKananChecking}
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({ banDepanKananChecking: text })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.isNegatifBanDepanKanan}
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifBanDepanKanan: itemValue,
                                  visibleBanDepanKanan: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleBanDepanKanan}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleBanDepanKanan: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitBanDepanKananNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.banDepanKananCheckingNote}
                                placeholder="Catatan ban depan kanan (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    banDepanKananCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Ban Belakang Kiri Dalam
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            value={this.state.banBelakangKiriDalamChecking}
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({
                                banBelakangKiriDalamChecking: text
                              })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={
                                this.state.isNegatifBanBelakangKiriDalam
                              }
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifBanBelakangKiriDalam: itemValue,
                                  visibleBanBelakangKiriDalam: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleBanBelakangKiriDalam}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleBanBelakangKiriDalam: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitBanBelakangKiriDalamNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={
                                  this.state.banBelakangKiriDalamCheckingNote
                                }
                                placeholder="Catatan ban belakang kiri dalam (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    banBelakangKiriDalamCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Ban Belakang Kiri Luar
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            value={this.state.banBelakangKiriLuarChecking}
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({
                                banBelakangKiriLuarChecking: text
                              })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={
                                this.state.isNegatifBanBelakangKiriLuar
                              }
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifBanBelakangKiriLuar: itemValue,
                                  visibleBanBelakangKiriLuar: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleBanBelakangKiriLuar}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleBanBelakangKiriLuar: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitBanBelakangKiriLuarNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={
                                  this.state.banBelakangKiriLuarCheckingNote
                                }
                                placeholder="Catatan ban belakang kiri luar (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    banBelakangKiriLuarCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Ban Belakang Kanan Dalam
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            value={this.state.banBelakangKananDalamChecking}
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({
                                banBelakangKananDalamChecking: text
                              })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={
                                this.state.isNegatifBanBelakangKananDalam
                              }
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifBanBelakangKananDalam: itemValue,
                                  visibleBanBelakangKananDalam: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleBanBelakangKananDalam}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleBanBelakangKananDalam: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitBanBelakangKananDalamNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={
                                  this.state.banBelakangKananDalamCheckingNote
                                }
                                placeholder="Catatan ban belakang kanan dalam (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    banBelakangKananDalamCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <View>
                          <Text style={styles.titleInput}>
                            Ban Belakang Kanan Luar
                          </Text>
                          <Textarea
                            style={{
                              marginLeft: 5,
                              marginRight: 55,
                              marginBottom: 5,
                              borderRadius: 5,
                              fontSize: 11,
                              width: 70
                            }}
                            keyboardType="numeric"
                            rowSpan={1.5}
                            bordered
                            value={this.state.banBelakangKananLuarChecking}
                            placeholder="Ex : 0"
                            onChangeText={text =>
                              this.setState({
                                banBelakangKananLuarChecking: text
                              })
                            }
                          />
                        </View>
                        <View>
                          <Text style={styles.titleInput}>Masuk Negatif *</Text>
                          <Form
                            style={{
                              borderWidth: 1,
                              borderRadius: 5,
                              marginTop: 5,
                              marginRight: 5,
                              marginLeft: 5,
                              borderColor: "#E6E6E6",
                              height: 40
                            }}
                          >
                            <Picker
                              mode="dropdown"
                              iosIcon={
                                <Icon
                                  name={
                                    Platform.OS
                                      ? "ios-arrow-down"
                                      : "ios-arrow-down-outline"
                                  }
                                />
                              }
                              style={{ height: 35, width: 200 }}
                              placeholder="Select ..."
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={
                                this.state.isNegatifBanBelakangKananLuar
                              }
                              onValueChange={itemValue =>
                                this.setState({
                                  isNegatifBanBelakangKananLuar: itemValue,
                                  visibleBanBelakangKananLuar: true
                                })
                              }
                            >
                              <Picker.Item
                                label="Tidak Masuk Negatif List"
                                value="0"
                              />
                              <Picker.Item
                                label="Masuk Negatif List"
                                value="1"
                              />
                            </Picker>
                          </Form>
                        </View>
                      </View>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleBanBelakangKananLuar}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleBanBelakangKananLuar: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => that.submitBanBelakangKananLuarNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={
                                  this.state.banBelakangKananLuarCheckingNote
                                }
                                placeholder="Catatan ban belakang kanan luar (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    banBelakangKananLuarCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View style={styles.Contentsave}>
                        <Button
                          block
                          style={{
                            width: "100%",
                            height: 45,
                            marginBottom: 20,
                            marginTop: 15,
                            borderWidth: 1,
                            backgroundColor: "#00b300",
                            borderColor: "#00b300",
                            borderRadius: 4
                          }}
                          onPress={() => this.validasiFieldTab2()}
                        >
                          <Text
                            style={{ fontWeight: "bold", color: colors.white }}
                          >
                            Lanjutkan
                          </Text>
                        </Button>
                      </View>
                    </View>
                  </CardItem>
                </View>
              ) : (
                <View>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Pemeriksaan
                        </Text>
                      </View>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Kondisi Pickup
                        </Text>
                      </View>
                      <View style={{ width: "33%", alignItems: "center" }}>
                        <Icon
                          name="ios-radio-button-on"
                          size={20}
                          style={{
                            color: colors.green01,
                            fontSize: 20,
                            paddingLeft: 8
                          }}
                        />
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          Kondisi Sparepart
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ marginLeft: 10, flex: 2, width: "33%" }}>
                        <Text
                          style={{
                            fontSize: 10,
                            paddingTop: 10,
                            fontWeight: "bold"
                          }}
                        >
                          Kondisi Lampu Lampu :{" "}
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Cabin
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuCabinChecking}
                            onValueChange={itemValue => {
                              this.setState({ lampuCabinChecking: itemValue });
                              if (!itemValue) {
                                this.setState({ visibleLampuCabin: true });
                              }
                            }}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Kota
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuKotaChecking}
                            onValueChange={itemValue => {
                              this.setState({ lampuKotaChecking: itemValue });
                              if (!itemValue) {
                                this.setState({ visibleLampuKota: true });
                              }
                            }}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Jauh
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuJauhChecking}
                            onValueChange={itemValue => {
                              this.setState({ lampuJauhChecking: itemValue });
                              if (!itemValue) {
                                this.setState({ visibleLampuJauh: true });
                              }
                            }}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Sein Kiri
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuSeinKiriChecking}
                            onValueChange={itemValue => {
                              this.setState({
                                lampuSeinKiriChecking: itemValue
                              });
                              if (!itemValue) {
                                this.setState({ visibleLampuSeinKiri: true });
                              }
                            }}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Sein Kanan
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuSeinKananChecking}
                            onValueChange={itemValue => {
                              this.setState({
                                lampuSeinKananChecking: itemValue
                              });
                              if (!itemValue) {
                                this.setState({ visibleLampuSeinKanan: true });
                              }
                            }}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Rem
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuRemChecking}
                            onValueChange={itemValue => {
                              this.setState({ lampuRemChecking: itemValue });
                              if (!itemValue) {
                                this.setState({ visibleLampuRem: true });
                              }
                            }}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Atret
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuAtretChecking}
                            onValueChange={itemValue => {
                              this.setState({ lampuAtretChecking: itemValue });
                              if (!itemValue) {
                                this.setState({ visibleLampuAtret: true });
                              }
                            }}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Lampu Sorot
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.lampuSorotChecking}
                            onValueChange={itemValue => {
                              this.setState({ lampuSorotChecking: itemValue });
                              if (!itemValue) {
                                this.setState({ visibleLampuSorot: true });
                              }
                            }}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Panel Dashboard
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.dashboardChecking}
                            onValueChange={itemValue => {
                              this.setState({ dashboardChecking: itemValue });
                              if (!itemValue) {
                                this.setState({ visibleLampuDashboard: true });
                              }
                            }}
                          />
                        </Right>
                      </ListItem>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleLampuCabin}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleLampuCabin: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => this.submitLampuCabinNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.lampuCabinCheckingNote}
                                placeholder="Catatan lampu cabin (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    lampuCabinCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleLampuKota}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleLampuKota: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => this.submitLampuKotaNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.lampuKotaCheckingNote}
                                placeholder="Catatan lampu kota (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    lampuKotaCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleLampuJauh}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleLampuJauh: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => this.submitLampuJauhNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.lampuJauhCheckingNote}
                                placeholder="Catatan lampu jauh (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    lampuJauhCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleLampuSeinKiri}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleLampuSeinKiri: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => this.submitLampuSeinKiriNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.lampuSeinKiriCheckingNote}
                                placeholder="Catatan lampu sein kiri (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    lampuSeinKiriCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleLampuSeinKanan}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleLampuSeinKanan: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => this.submitLampuSeinKananNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.lampuSeinKananCheckingNote}
                                placeholder="Catatan lampu sein kanan (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    lampuSeinKananCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleLampuRem}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleLampuRem: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => this.submitLampuRemNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.lampuRemCheckingNote}
                                placeholder="Catatan lampu rem (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    lampuRemCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleLampuAtret}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleLampuAtret: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => this.submitLampuAtretNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.lampuAtretCheckingNote}
                                placeholder="Catatan lampu atret (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    lampuAtretCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleLampuSorot}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleLampuSorot: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => this.submitLampuSorotNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.lampuSorotCheckingNote}
                                placeholder="Catatan lampu sorot (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    lampuSorotCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleLampuDashboard}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleLampuDashboard: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => this.submitDashboardNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.lampuDashboardCheckingNote}
                                placeholder="Catatan lampu dashboard (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    lampuDashboardCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ marginLeft: 10, flex: 2, width: "33%" }}>
                        <Text
                          style={{
                            fontSize: 10,
                            paddingTop: 10,
                            fontWeight: "bold"
                          }}
                        >
                          Kondisi Kaca Spion :{" "}
                        </Text>
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Kaca Spion Kiri
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.spionKiriChecking}
                            onValueChange={itemValue => {
                              this.setState({ spionKiriChecking: itemValue });
                              if (!itemValue) {
                                console.log("masuk 2");
                                this.setState({ visibleKacaSpionKiri: true });
                                console.log(this.state.visibleKacaSpionKiri);
                              }
                            }}
                          />
                        </Right>
                      </ListItem>
                      <ListItem icon>
                        <Left>
                          <Button style={{ backgroundColor: "#FF9501" }}>
                            <Icon active name="ios-settings" />
                          </Button>
                        </Left>
                        <Body>
                          <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                            Kaca Spion Kanan
                          </Text>
                        </Body>
                        <Right>
                          <Switch
                            value={this.state.spionKananChecking}
                            onValueChange={itemValue => {
                              this.setState({ spionKananChecking: itemValue });
                              if (!itemValue) {
                                this.setState({ visibleKacaSpionKanan: true });
                              }
                            }}
                          />
                        </Right>
                      </ListItem>
                    </View>
                  </CardItem>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleKacaSpionKiri}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleKacaSpionKiri: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => this.submitKacaSpionKiriNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.kacaSpionKiriCheckingNote}
                                placeholder="Catatan kaca spion kiri (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    kacaSpionKiriCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <View style={{ width: 270, position: "absolute" }}>
                    <Dialog
                      visible={this.state.visibleKacaSpionKanan}
                      dialogAnimation={
                        new SlideAnimation({
                          slideFrom: "bottom"
                        })
                      }
                      dialogStyle={{
                        position: "absolute",
                        top: this.state.posDialog
                      }}
                      onTouchOutside={() => {
                        this.setState({ visibleKacaSpionKanan: false });
                      }}
                      dialogTitle={
                        <DialogTitle title="Place negatif note here" />
                      }
                      actions={[
                        <DialogButton
                          style={{
                            fontSize: 11,
                            backgroundColor: colors.white,
                            borderColor: colors.blue01
                          }}
                          text="SUBMIT"
                          // onPress={() => that.rejectPersonal()}
                          onPress={() => this.submitKacaSpionKananNote()}
                        />
                      ]}
                    >
                      <DialogContent>
                        {
                          <Form>
                            <View>
                              <Textarea
                                style={{
                                  marginLeft: 25,
                                  marginRight: 5,
                                  marginTop: 5,
                                  marginBottom: 5,
                                  borderRadius: 5,
                                  fontSize: 11,
                                  width: 270
                                }}
                                rowSpan={2}
                                bordered
                                value={this.state.kacaSpionKananCheckingNote}
                                placeholder="Catatan kaca spion kanan (Optional)"
                                onChangeText={text =>
                                  this.setState({
                                    kacaSpionKananCheckingNote: text
                                  })
                                }
                              />
                            </View>
                          </Form>
                        }
                      </DialogContent>
                    </Dialog>
                  </View>
                  <CardItem
                    style={{ borderRadius: 0, backgroundColor: colors.gray }}
                  >
                    <View style={{ flex: 1, flexDirection: "column" }}>
                      <View style={{ marginLeft: 10, flex: 2 }}>
                        <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                          Catatan
                        </Text>
                      </View>
                      <View>
                        <Textarea
                          style={{
                            marginLeft: 5,
                            marginRight: 5,
                            marginBottom: 5,
                            borderRadius: 5,
                            fontSize: 11
                          }}
                          rowSpan={2}
                          bordered
                          value={this.state.catatanChecking}
                          placeholder="Type something .. "
                          onChangeText={text =>
                            this.setState({ catatanChecking: text })
                          }
                        />
                      </View>
                    </View>
                  </CardItem>
                  <CardItem
                    style={{
                      borderRadius: 0,
                      marginTop: 0,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{ flex: 1 }}>
                      <View style={styles.Contentsave}>
                        <Button
                          block
                          style={{
                            width: "100%",
                            height: 45,
                            marginBottom: 20,
                            marginTop: 15,
                            borderWidth: 1,
                            backgroundColor: "#00b300",
                            borderColor: "#00b300",
                            borderRadius: 4
                          }}
                          onPress={() => this.unitCheckPickupCreate()}
                        >
                          <Text
                            style={{ color: colors.white, fontWeight: "bold" }}
                          >
                            SUBMIT
                          </Text>
                        </Button>
                      </View>
                    </View>
                  </CardItem>
                </View>
              )}
            </View>
          </Content>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog visible={this.state.visibleLoadingUnitKerja}>
            <DialogContent>
              {<ActivityIndicator size="large" color="#330066" animating />}
            </DialogContent>
          </Dialog>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialogSubmit}
            dialogTitle={<DialogTitle title="Creating Report.." />}
          >
            <DialogContent>
              {<ActivityIndicator size="large" color="#330066" animating />}
            </DialogContent>
          </Dialog>
        </View>
        <CustomFooter navigation={this.props.navigation} menu="FireSystem" />
      </Container>
    );
  }
}

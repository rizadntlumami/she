import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewHeader from "../../components/ListViewHeader";
import ListViewDetail from "../../components/ListViewDetail";

class ListItemPemadam extends React.PureComponent {
  render() {
    return (
      <View style={{ flex: 1, flexDirection: "row" }}>
        <View style={{ width: "5%" }}>
          <Text style={{ fontSize: 10, fontWeight: "bold" }}>-</Text>
        </View>
        <View style={{ width: "90%" }}>
          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
            {this.props.data}
          </Text>
        </View>
      </View>
    );
  }
}

class ListItemTembusan extends React.PureComponent {
  render() {
    return (
      <View style={{ flex: 1, flexDirection: "row" }}>
        <View style={{ width: "5%" }}>
          <Text style={{ fontSize: 10, fontWeight: "bold" }}>-</Text>
        </View>
        <View style={{ width: "90%" }}>
          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
            {this.props.data}
          </Text>
        </View>
      </View>
    );
  }
}

class ListItemPeralatan extends React.PureComponent {
  render() {
    return (
      <View style={{ flex: 1, flexDirection: "row" }}>
        <View style={{ width: "5%" }}>
          <Text style={{ fontSize: 10, fontWeight: "bold" }}>-</Text>
        </View>
        <View style={{ width: "90%" }}>
          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
            {this.props.data}
          </Text>
        </View>
      </View>
    );
  }
}

class FireReportDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      listFireReport: [],
      listPemadam: [],
      listPeralatan: [],
      listTembusan: [],
      listFile: [],
      idFireReport: "",
      visibleDialogSubmit: false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItemPeralatan = ({ item }) => <ListItemPeralatan data={item} />;

  _renderItemPemadam = ({ item }) => <ListItemPemadam data={item} />;

  _renderItemTembusan = ({ item }) => <ListItemTembusan data={item} />;

  navigateToScreen(route, listFireReport) {
    AsyncStorage.setItem("list", JSON.stringify(listFireReport)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        AsyncStorage.getItem("id").then(id => {
          const url = GlobalConfig.SERVERHOST + "detailFireReport";
          var formData = new FormData();
          formData.append("id", id);
          fetch(url, {
            headers: {
              "Content-Type": "multipart/form-data"
            },
            method: "POST",
            body: formData
          })
            .then(response => response.json())
            .then(responseJson => {
              // alert(JSON.stringify(responseJson))
              this.setState({
                listFireReport: responseJson.data,
                isLoading: false,
              });
            })
            .catch(error => {
              console.log(error);
            });
        });
      }
    );
  }

  konfirmasideleteFireReport() {
    Alert.alert(
      "Apakah Anda Yakin Ingin Menghapus Aktivitas",
      this.state.listFireReport.fire_number,
      [
        { text: "Yes", onPress: () => this.deleteFireReport() },
        { text: "Cancel" }
      ],
      { cancelable: false }
    );
  }

  deleteFireReport() {
    this.setState({
      visibleDialogSubmit: true
    });
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "deleteFireReport";
      var formData = new FormData();
      formData.append("id", this.state.listFireReport.id);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Success", "Delete Success", [
              {
                text: "Oke"
              }
            ]);
            this.props.navigation.navigate("FireReport");
          } else {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Error", "Delete Failed", [
              {
                text: "Oke"
              }
            ]);
          }
        });
  }


  render() {

    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.listFireReport == null) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <View style={{}}>
            <CardItem>
            <View>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={{ width: "90%" }}>
                <Text style={{ fontSize: 10 }}>Tanggal</Text>
                <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                  {this.state.listFireReport.fire_date} {this.state.listFireReport.fire_time}
                </Text>
              </View>
              <View style={{ width: "10%" }}>
                <Text style={{ fontSize: 10, textAlign: "right" }}>Shift</Text>
                <Text
                  style={{ fontSize: 10, fontWeight: "bold", textAlign: "right" }}
                >
                  Shift {this.state.listFireReport.shift}
                </Text>
              </View>
            </View>
            <View>
              <Text style={{ fontSize: 10, paddingTop:10 }}>
                Pekerjaan Sewaktu Kebakaran
              </Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.listFireReport.work_on_fire}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 10 }}>Lama Pemadaman</Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.listFireReport.job_walking_timestart} -
                {this.state.listFireReport.job_walking_timeend}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 10 }}>
                Alat Pemadam Yang Digunakan
              </Text>
              <FlatList
                data={JSON.parse(this.state.listFireReport.fire_extinguishers)}
                renderItem={this._renderItemPeralatan}
                keyExtractor={(item, index) => index.toString()}
              />

              <Text style={{ fontSize: 10, paddingTop: 10 }}>Penjelasan</Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.listFireReport.description_of_fire}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 10 }}>Pelapor</Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.listFireReport.pic_name}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 10 }}>Tempat Kejadian</Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.listFireReport.incident_place}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 10 }}>Jumlah Korban</Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.listFireReport.number_victims}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 10 }}>
                Keterangan Tempat Kejadian
              </Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.listFireReport.note_place}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 10 }}>
                Objek Yang Terbakar
              </Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.listFireReport.fire_happened_on}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 10 }}>
                Jenis Bahan Yang Terbakar
              </Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.listFireReport.fire_material_type}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 10 }}>
                Kerusakan Yang Timbul
              </Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.listFireReport.damage_from_fire}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 10 }}>Unsafe Condition</Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.listFireReport.unsafe_condition}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 10 }}>Unsafe Action</Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.listFireReport.unsafe_action}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 10 }}>Saran</Text>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.state.listFireReport.saran}
              </Text>
              <Text style={{ fontSize: 10, paddingTop: 10 }}>
                Anggota PMK Yang Melakukan Pemadaman
              </Text>
              <FlatList
                data={JSON.parse(this.state.listFireReport.list_employees)}
                renderItem={this._renderItemPemadam}
                keyExtractor={(item, index) => index.toString()}
              />

              <Text style={{ fontSize: 10, paddingTop: 10 }}>Tembusan Surat</Text>
              <FlatList
                data={JSON.parse(this.state.listFireReport.list_tembusan)}
                renderItem={this._renderItemTembusan}
                keyExtractor={(item, index) => index.toString()}
              />

            </View>
            </View>
            </CardItem>
          </View>
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("FireReport")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>
              {this.state.listFireReport.fire_number}
            </Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() =>
                this.navigateToScreen(
                  "FireReportImage",
                  this.state.listFireReport.id
                )
              }
            >
              <Icon
                name="ios-images"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            <Button
              transparent
              onPress={() =>
                this.navigateToScreen(
                  "FireReportUpdate",
                  this.state.listFireReport
                )
              }
            >
              <Icon
                name="ios-create"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            <Button
              transparent
              onPress={() => this.konfirmasideleteFireReport()}
            >
              <Icon
                name="ios-trash"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ marginTop: 10, flex: 1, flexDirection: "column" }}>
        <ScrollView>
          {list}
        </ScrollView>
        </View>
          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
              dialogTitle={<DialogTitle title="Deleting Report .." />}
            >
              <DialogContent>
                {<ActivityIndicator size="large" color="#330066" animating />}
              </DialogContent>
            </Dialog>
          </View>
      </Container>
    );
  }
}

export default FireReportDetail;

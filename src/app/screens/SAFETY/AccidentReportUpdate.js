import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Item
} from "native-base";

import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton
  } from "react-native-popup-dialog";
import moment from 'moment';
import Ripple from "react-native-material-ripple";
import ImagePicker from "react-native-image-picker";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from 'react-native-datepicker';

class ListItem extends React.PureComponent {
    navigateToScreen(route, id_rusak) {
        // alert(route);
        AsyncStorage.setItem("id", id_rusak).then(() => {
            that.props.navigation.navigate(route);
        });
    }

    render() {
        return (
            <View style={{ backgroundColor: "#FEFEFE" }}>
                <Ripple
                    style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() =>
                        this.props.setSelectedPegLabel(this.props.data)
                    }
                    rippleColor={colors.accent}
                >
                    <CardItem
                        style={{
                            borderRadius: 0,
                            marginTop: 4,
                            backgroundColor: colors.gray
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                marginTop: 4,
                                backgroundColor: colors.gray
                            }}
                        >
                            <View >
                                <Text style={{ fontSize: 12 }}>{this.props.data.mk_nopeg} - {this.props.data.mk_nama}</Text>
                            </View>
                        </View>
                    </CardItem>
                </Ripple>
            </View>
        );
    }
}

class ListItemSaksi extends React.PureComponent {
    navigateToScreen(route, id_rusak) {
        // alert(route);
        AsyncStorage.setItem("id", id_rusak).then(() => {
            that.props.navigation.navigate(route);
        });
    }

    render() {
        return (
            <View style={{ backgroundColor: "#FEFEFE" }}>
                <Ripple
                    style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() =>
                        this.props.setSelectedSaksiLabel(this.props.data)
                    }
                    rippleColor={colors.accent}
                >
                    <CardItem
                        style={{
                            borderRadius: 0,
                            marginTop: 4,
                            backgroundColor: colors.gray
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                marginTop: 4,
                                backgroundColor: colors.gray
                            }}
                        >
                            <View >
                                <Text style={{ fontSize: 12 }}>{this.props.data.mk_nopeg} - {this.props.data.mk_nama}</Text>
                            </View>
                        </View>
                    </CardItem>
                </Ripple>
            </View>
        );
    }
}

export default class AccidentReportUpdate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            isLoading: true,
            headerAccidentReport: [],
            // AccidentDate: '',
            // selectedShift: '1',
            // AccidentTime: '',
            // selectedStatVict: '0',
            // selectedStatVictText: 'Non-Karyawan',
            // // badgeVict: '2180801',
            // // nameVict: '',
            // ageVict: '',
            // addressict: '',
            // noJamsos: '',
            // ukVict: '',
            // ukVictText: '',
            // nameVict: '',
            // posText: '',
            // dateInVict: '',

            // pickedImage: '',
            uri: '',
            // fileName: '',    

            // workTask: 'Mengerjakan Projek SHE Mobile',
            // spvName: 'INDRA NOFIANDI',
            // locText: 'Ruang Kerja ICT Gedung Semen Indonesia',
            // acdCaused: 'Overload',
            // // saksiName: 'Adhiq Rahmaduha',
            // // saksiPosText: 'Pogrammer',
            // injText: 'Programmer terlalu lama di depan komputer',
            // injNote: 'Muncul bisul',
            // firstAid: 'Buatan Nafas',
            // nextAid: 'Dibawa ke UGD',
            // safetyReq: 'Kerja Looss',
            // acdRptText: 'Bekerja oveload didepan laptop',
            // unsafeAct: 'Memaksakan diri',
            // unsafeCond: 'Laptop tidak memenuhi requirement',
            // note: 'Porsi kerja dioptimalkan',
            // invName: 'Adhiq Rahmaduha',

            visibleLoadingPegawai: false,
            visibleSearchListPeg: false,
            selectedPegLabel: '',
            searchWordPegawai: '',

            visibleLoadingSaksi: false,
            visibleSearchListSaksi: false,
            selectedSaksiLabel: '',
            searchWordSaksi: '',

            visibleDialogSubmit:false
        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {
        AsyncStorage.getItem('list').then((headerAccidentReport) => {
            console.log(JSON.parse(headerAccidentReport).DATE_ACD)
            var acdDate = moment(JSON.parse(headerAccidentReport).DATE_ACD, 'DD-MMM-YYYY');
            var acdDateString = acdDate.format('YYYY-MM-DD')
            console.log(acdDateString)

            console.log(JSON.parse(headerAccidentReport).DATE_IN)
            var vicDateIn = moment(JSON.parse(headerAccidentReport).DATE_IN, 'DD-MMM-YYYY');
            var vicDateInString = vicDateIn.format('YYYY-MM-DD')
            console.log(vicDateIn)

            this.setState({
                listHeader: JSON.parse(headerAccidentReport),
                ID: JSON.parse(headerAccidentReport).ID,
                AccidentDate: acdDateString,
                selectedShift: JSON.parse(headerAccidentReport).SHIFT,
                AccidentTime: JSON.parse(headerAccidentReport).TIME_ACD,
                selectedStatVict: JSON.parse(headerAccidentReport).STAT_VICT,
                selectedStatVictText: JSON.parse(headerAccidentReport).STAT_VICT_TEXT,
                badgeVict: JSON.parse(headerAccidentReport).BADGE_VICT,
                nameVict: JSON.parse(headerAccidentReport).NAME_VICT,
                ageVict: JSON.parse(headerAccidentReport).AGE,
                addressict: JSON.parse(headerAccidentReport).ADDRESS_VICT,
                noJamsos: JSON.parse(headerAccidentReport).NO_JAMSOS,
                ukVict: JSON.parse(headerAccidentReport).UK_VICT,
                ukVictText: JSON.parse(headerAccidentReport).UK_VICT_TEXT,
                posText: JSON.parse(headerAccidentReport).POS_TEXT,
                dateInVict: vicDateInString,

                pickedImage: GlobalConfig.SERVERHOST + 'api' + JSON.parse(headerAccidentReport).PICT_BEFORE,

                workTask: JSON.parse(headerAccidentReport).WORK_TASK,
                spvName: JSON.parse(headerAccidentReport).SPV_NAME,
                locText: JSON.parse(headerAccidentReport).LOCATION_TEXT,
                acdCaused: JSON.parse(headerAccidentReport).ACD_CAUSE,
                saksiName: JSON.parse(headerAccidentReport).S_NAME,
                saksiPosText: JSON.parse(headerAccidentReport).S_POS_TEXT,
                injText: JSON.parse(headerAccidentReport).INJ_TEXT,
                injNote: JSON.parse(headerAccidentReport).INJ_NOTE,
                firstAid: JSON.parse(headerAccidentReport).FIRST_AID,
                nextAid: JSON.parse(headerAccidentReport).NEXT_AID,
                safetyReq: JSON.parse(headerAccidentReport).SAFETY_REQ,
                acdRptText: JSON.parse(headerAccidentReport).ACD_RPT_TEXT,
                unsafeAct: JSON.parse(headerAccidentReport).UNSAFE_ACT,
                unsafeCond: JSON.parse(headerAccidentReport).UNSAFE_COND,
                note: JSON.parse(headerAccidentReport).NOTE,
                invName: JSON.parse(headerAccidentReport).INV_NAME,

            });
        })
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {

        });
    }

    reset = () => {
        this.setState({
            pickedImage: null
        });
    }

    pickImageHandler = () => {
        ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
            if (res.didCancel) {
                console.log("User cancelled!");
            } else if (res.error) {
                console.log("Error", res.error);
            } else {
                this.setState({
                    pickedImage: res.uri,
                    uri: res.uri,
                    fileName: res.fileName,
                    fileType: res.type
                });
                console.log(res.type)
            }
        });
    }

    resetHandler = () => {
        this.reset();
    }

    // _renderItem = ({ item }) => (
    //     <ListItem data={item}></ListItem>
    // )

    // componentDidMount() {
    //     this.loadData();
    // }

    // loadData() {

    // }

    UpdateAccident() {
       console.log(this.state.uri)
        if (this.state.uri == '') {
            this.setState({
                visibleDialogSubmit:true
            })
            AsyncStorage.getItem("token").then(value => {
                const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/accident/update';
                var formData = new FormData();
                formData.append("token", value);
                formData.append("ID", this.state.ID);
                formData.append("DATE_ACD", this.state.AccidentDate);
                formData.append("SHIFT", this.state.selectedShift);
                formData.append("TIME_ACD", this.state.AccidentTime);
                formData.append("STAT_VICT", this.state.selectedStatVict);
                formData.append("STAT_VICT_TEXT", this.state.selectedStatVictText);
                formData.append("BADGE_VICT", this.state.badgeVict);
                formData.append("NAME_VICT", this.state.nameVict);
                formData.append("AGE", this.state.ageVict);
                formData.append("ADDRESS_VICT", this.state.addressict);
                formData.append("NO_JAMSOS", this.state.noJamsos);
                formData.append("UK_VICT", this.state.ukVict);
                formData.append("UK_VICT_TEXT", this.state.ukVictText);
                formData.append("POS_TEXT", this.state.posText);
                formData.append("DATE_IN", this.state.dateInVict);
                // formData.append("PICT_BEFORE", null);
                formData.append("WORK_TASK", this.state.workTask);
                formData.append("SPV_NAME", this.state.spvName);
                formData.append("LOCATION_TEXT", this.state.locText);
                formData.append("ACD_CAUSE", this.state.acdCaused);
                formData.append("S_BADGE", this.state.saksiBadge);
                formData.append("S_NAME", this.state.saksiName);
                formData.append("S_POS_TEXT", this.state.saksiPosText);
                formData.append("INJ_TEXT", this.state.injText);
                formData.append("INJ_NOTE", this.state.injNote);
                formData.append("FIRST_AID", this.state.firstAid);
                formData.append("NEXT_AID", this.state.nextAid);
                formData.append("SAFETY_REQ", this.state.safetyReq);
                formData.append("ACD_RPT_TEXT", this.state.acdRptText);
                formData.append("UNSAFE_ACT", this.state.unsafeAct);
                formData.append("UNSAFE_COND", this.state.unsafeCond);
                formData.append("NOTE", this.state.note);
                formData.append("INV_NAME", this.state.invName);

                // alert(JSON.stringify(url));

                fetch(url, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },
                    method: 'POST',
                    body: formData
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        // alert(responseJson);
                        if (responseJson.status == 200) {
                            this.setState({
                                visibleDialogSubmit:false
                            })
                            Alert.alert('Success', 'Update Accident Success', [{
                                text: 'Okay'
                            }])
                            this.props.navigation.navigate('AccidentReport')
                        } else {
                            this.setState({
                                visibleDialogSubmit:false
                            })
                            Alert.alert('Error', 'Update Accident Failed', [{
                                text: 'Okay'
                            }])
                        }
                    })
                    .catch((error)=>{
                        this.setState({
                            visibleDialogSubmit:false
                        })
                        Alert.alert('Error', 'Update Accident Failed', [{
                            text: 'Okay'
                        }])
                        console.log(error)
                    })
            })
        } else {
            this.setState({
                visibleDialogSubmit:true
            })
            // alert(JSON.stringify(this.state.id));
            AsyncStorage.getItem("token").then(value => {
                const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/accident/update';
                var formData = new FormData();
                formData.append("token", value);
                formData.append("ID", this.state.ID);
                formData.append("DATE_ACD", this.state.AccidentDate);
                formData.append("SHIFT", this.state.selectedShift);
                formData.append("TIME_ACD", this.state.AccidentTime);
                formData.append("STAT_VICT", this.state.selectedStatVict);
                formData.append("STAT_VICT_TEXT", this.state.selectedStatVictText);
                formData.append("BADGE_VICT", this.state.badgeVict);
                formData.append("NAME_VICT", this.state.nameVict);
                formData.append("AGE", this.state.ageVict);
                formData.append("ADDRESS_VICT", this.state.addressict);
                formData.append("NO_JAMSOS", this.state.noJamsos);
                formData.append("UK_VICT", this.state.ukVict);
                formData.append("UK_VICT_TEXT", this.state.ukVictText);
                formData.append("POS_TEXT", this.state.posText);
                formData.append("DATE_IN", this.state.dateInVict);
                formData.append("PICT_BEFORE", {
                    uri: this.state.pickedImage,
                    name: 'image',
                    type: this.state.fileType,
                });
                formData.append("WORK_TASK", this.state.workTask);
                formData.append("SPV_NAME", this.state.spvName);
                formData.append("LOCATION_TEXT", this.state.locText);
                formData.append("ACD_CAUSE", this.state.acdCaused);
                formData.append("S_BADGE", this.state.saksiBadge);
                formData.append("S_NAME", this.state.saksiName);
                formData.append("S_POS_TEXT", this.state.saksiPosText);
                formData.append("INJ_TEXT", this.state.injText);
                formData.append("INJ_NOTE", this.state.injNote);
                formData.append("FIRST_AID", this.state.firstAid);
                formData.append("NEXT_AID", this.state.nextAid);
                formData.append("SAFETY_REQ", this.state.safetyReq);
                formData.append("ACD_RPT_TEXT", this.state.acdRptText);
                formData.append("UNSAFE_ACT", this.state.unsafeAct);
                formData.append("UNSAFE_COND", this.state.unsafeCond);
                formData.append("NOTE", this.state.note);
                formData.append("INV_NAME", this.state.invName);

                console.log(formData)
                // alert(JSON.stringify(url));

                fetch(url, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },
                    method: 'POST',
                    body: formData
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        // alert(responseJson);
                        if (responseJson.status == 200) {
                            this.setState({
                                visibleDialogSubmit:false
                            })
                            Alert.alert('Success', 'Update Accident Success', [{
                                text: 'Okay'
                            }])
                            this.props.navigation.navigate('AccidentReport')
                        } else {
                            this.setState({
                                visibleDialogSubmit:false
                            })
                            Alert.alert('Error', 'Update Accident Failed', [{
                                text: 'Okay'
                            }])
                        }
                    })
                    .catch((error)=>{
                        this.setState({
                            visibleDialogSubmit:false
                        })
                        Alert.alert('Error', 'Update Accident Failed', [{
                            text: 'Okay'
                        }])
                        console.log(error)
                    })
            })
        }
    }

    onChangePegawai(text) {
        this.setState({
            searchWordPegawai: text
        })
        this.setState({
            visibleLoadingPegawai: true
        })
        AsyncStorage.getItem('token').then((value) => {
            const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/employee';
            var formData = new FormData();
            formData.append("token", value)
            formData.append("search", this.state.searchWordPegawai)
            formData.append("limit", 10)
            formData.append("plant", null)
            fetch(url, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                body: formData
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        visibleLoadingPegawai: false
                    })
                    this.setState({
                        listKaryawan: responseJson,
                        listKaryawanMaster: responseJson,
                        isloading: false
                    });
                })
                .catch((error) => {
                    this.setState({
                        visibleLoadingPegawai: false
                    })
                    console.log(error)
                })
        })
        // this.setState({
        //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
        // })
    }

    onChangeSaksi(text) {
        this.setState({
            searchWordSaksi: text
        })
        this.setState({
            visibleLoadingSaksi: true
        })
        AsyncStorage.getItem('token').then((value) => {
            const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/employee';
            var formData = new FormData();
            formData.append("token", value)
            formData.append("search", this.state.searchWordSaksi)
            formData.append("limit", 10)
            formData.append("plant", null)
            fetch(url, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                body: formData
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        visibleLoadingSaksi: false
                    })
                    this.setState({
                        listSaksi: responseJson,
                        listSaksiMaster: responseJson,
                        isloading: false
                    });
                })
                .catch((error) => {
                    this.setState({
                        visibleLoadingSaksi: false
                    })
                    console.log(error)
                })
        })
        // this.setState({
        //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
        // })
    }

    onClickSearchPeg() {
        console.log('masuk')
        if (this.state.visibleSearchListPeg) {
            this.setState({
                visibleSearchListPeg: false
            })
        } else {
            this.setState({
                visibleSearchListPeg: true
            })
        }
    }

    onClickSearchSaksi() {
        console.log('masuk')
        if (this.state.visibleSearchListSaksi) {
            this.setState({
                visibleSearchListSaksi: false
            })
        } else {
            this.setState({
                visibleSearchListSaksi: true
            })
        }
    }

    setSelectedPegLabel(pegawai) {
        var lahir = new Date(pegawai.mk_tgl_lahir);
        var umurDate = new Date(Date.now() - lahir);
        var umur = Math.abs(umurDate.getUTCFullYear() - 1970)
        this.setState({
            badgeVict: pegawai.mk_nopeg,
            nameVict: pegawai.mk_nama,
            dateInVict: pegawai.mk_tgl_masuk,
            posText: pegawai.mk_emp_subgroup_text,
            addressict: pegawai.mk_alamat_rumah,
            ukVict: pegawai.muk_short,
            ukVictText: pegawai.muk_nama,
            ageVict: umur.toString(),
            visibleSearchListPeg: false,
            selectedStatVict: '1',
            selectedStatVictText: 'Karyawan'
        })
    }

    setSelectedSaksiLabel(saksi) {
        this.setState({
            saksiBadge: saksi.mk_nopeg,
            saksiName: saksi.mk_nama,
            saksiPosText: saksi.mk_emp_subgroup_text,
            visibleSearchListSaksi: false,
        })
    }

    _renderItem = ({ item }) => <ListItem data={item} setSelectedPegLabel={text => this.setSelectedPegLabel(text)} />;

    _renderItemSaksi = ({ item }) => <ListItemSaksi data={item} setSelectedSaksiLabel={text => this.setSelectedSaksiLabel(text)} />;

    render() {
        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left style={{flex:1}}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.goBack()}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body style={{flex:3,alignItems:'center'}}>
                        <Title style={styles.textbody}>Update Accident Report</Title>
                    </Body>
                    
                    <Right style={{flex:1}}/>
                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                <View style={{ flex: 1 }}>
                    <Content style={{ marginTop: 0 }}>
                        <View style={{ justifyContent: "center", alignItems: "center" }}>
                            <Text style={{ fontSize: 12, marginTop: 20, color: colors.green01 }}>Accident Update {this.state.ID}</Text>
                        </View>
                        <View style={{ backgroundColor: '#FEFEFE' }}>
                            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                                <View style={{ flex: 1}}>
                                    <View style={{paddingBottom:5}}>
                                    <Text style={styles.viewMerkFoto}>Upload Foto (jpg, png) max : 500 kb</Text>
                                    </View>
                                    <View>
                                    <Ripple
                                        style={{
                                        flex: 2,
                                        justifyContent: "center",
                                        alignItems: "center"
                                        }}
                                        rippleSize={176}
                                        rippleDuration={600}
                                        rippleContainerBorderRadius={15}
                                        onPress={this.pickImageHandler}
                                        rippleColor={colors.accent}
                                    >
                                    <View style={styles.placeholder}>
                                        <Image source={{uri:this.state.pickedImage+ '?' + new Date()}} style={styles.previewImage}/>
                                    </View>
                                    </Ripple>
                                    </View>
                                </View>
                            </CardItem>
                            
                            <CardItem style={{ borderRadius: 0, marginTop: 4, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View>
                                        <Text style={styles.titleInput}>Accident Date (*)</Text>
                                        <DatePicker
                                            style={{ width: 320, fontSize: 10, borderRadius: 20 }}
                                            date={this.state.AccidentDate}
                                            mode="date"
                                            placeholder="Choose Date ..."
                                            format="YYYY-MM-DD"
                                            minDate="2018-01-01"
                                            maxDate="2050-12-31"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'absolute',
                                                    left: 0,
                                                    top: 5,
                                                    marginLeft: 0
                                                },
                                                dateInput: {
                                                    marginLeft: 50, height: 35, borderRadius: 20, fontSize: 10
                                                }
                                            }}
                                            onDateChange={(date) => { this.setState({ AccidentDate: date }) }} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View style={{flex:1}}>
                                        <Text style={styles.viewMerk}>Shift</Text>
                                        <Form underlineColorAndroid='transparent'>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                                                style={{ width: '100%' }}
                                                placeholder="Quantity"
                                                placeholderStyle={{ color: "#bfc6ea" }}
                                                placeholderIconColor="#007aff"
                                                selectedValue={this.state.selectedShift}
                                                onValueChange={(itemValue) => this.setState({ selectedShift: itemValue })}>
                                                <Picker.Item label="Shift 1" value="1" />
                                                <Picker.Item label="Shift 2" value="2" />
                                                <Picker.Item label="Shift 3" value="3" />
                                            </Picker>
                                        </Form>
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View>
                                        <Text style={styles.titleInput}>Accident Time (*)</Text>
                                        <DatePicker
                                            style={{ width: 320, fontSize: 10, borderRadius: 20 }}
                                            date={this.state.AccidentTime}
                                            mode="time"
                                            placeholder="Choose Date ..."
                                            format="HH:mm:ss"
                                            minDate="00:00:00"
                                            maxDate="12:60:60"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'absolute',
                                                    left: 0,
                                                    top: 5,
                                                    marginLeft: 0
                                                },
                                                dateInput: {
                                                    marginLeft: 50, height: 35, borderRadius: 20, fontSize: 10
                                                }
                                            }}
                                            onDateChange={(date) => { this.setState({ AccidentTime: date }) }} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View style={{flex:1}}> 
                                        <Text style={styles.viewMerk}>Status Karyawan</Text>
                                        <Form underlineColorAndroid='transparent'>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                                                style={{ width: '100%'  }}
                                                placeholder="Quantity"
                                                placeholderStyle={{ color: "#bfc6ea" }}
                                                placeholderIconColor="#007aff"
                                                selectedValue={this.state.selectedStatVict}
                                                onValueChange={(itemValue) => this.setState({
                                                    selectedStatVict: itemValue
                                                })}>
                                                <Picker.Item label="0 - Non-Karyawan" value="0" />
                                                <Picker.Item label="1 - Karyawan" value="1" />
                                            </Picker>
                                        </Form>
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>No. Badge Korban</Text>
                                    </View>
                                    {this.state.selectedStatVict == 0 ? (
                                        <View>
                                            <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11, height: 35 }} rowSpan={3} bordered value={this.state.badgeVict} placeholder='Type Something ...' onChangeText={(text) => this.setState({ badgeVict: text })} />
                                        </View>
                                    ) : (
                                            <View style={{ flex: 1, flexDirection: 'column' }}>
                                                <Button
                                                    block
                                                    style={{ justifyContent: "flex-start", flex: 1, borderWidth: Platform.OS==='ios'?1:0, paddingLeft: 10, marginRight: 5, marginLeft: 5, backgroundColor: colors.gray, height: 35 ,borderColor: Platform.OS==='ios'?colors.lightGray:null }}
                                                    onPress={() => this.onClickSearchPeg()}>
                                                    <Text style={{ fontSize: 12 }}>{this.state.badgeVict}</Text>
                                                </Button>
                                            </View>
                                        )}
                                    {this.state.visibleSearchListPeg &&
                                        <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.gray, margin: 5, borderColor: "#E6E6E6", }}>
                                            <View>

                                                <Textarea value={this.state.searchWordPegawai} style={{ marginLeft: 5, marginRight: 5, fontSize: 11 }} bordered rowSpan={1.5} onChangeText={(text) => this.onChangePegawai(text)} placeholder='Ketik nama karyawan' />

                                            </View>
                                            <FlatList
                                                data={this.state.listKaryawan}
                                                renderItem={this._renderItem}
                                                keyExtractor={(item, index) => index.toString()}
                                            />
                                        </View>
                                    }
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Nama Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.nameVict} placeholder='Type Something ...' onChangeText={(text) => this.setState({ nameVict: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Umur Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={this.state.selectedStatVict == "1" ? false : true} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.ageVict} placeholder='Type Something ...' onChangeText={(text) => this.setState({ ageVict: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Alamat Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={this.state.selectedStatVict == "1" ? false : true} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.addressict} placeholder='Type Something ...' onChangeText={(text) => this.setState({ addressict: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={styles.viewMerk}>No. Jamsos </Text>
                                        <Text style={{ fontWeight: 'bold' }}>(Wajib diisi untuk Non-Karyawan)</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={this.state.selectedStatVict == "1" ? false : true} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.noJamsos} placeholder='Type Something ...' onChangeText={(text) => this.setState({ noJamsos: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Kode Unit Kerja Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={this.state.selectedStatVict == "1" ? false : true} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.ukVict} placeholder='Type Something ...' onChangeText={(text) => this.setState({ ukVict: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Nama Unit Kerja Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={this.state.selectedStatVict == "1" ? false : true} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.ukVictText} placeholder='Type Something ...' onChangeText={(text) => this.setState({ ukVictText: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Posisi Korban di Unit Kerja</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={false} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.posText} placeholder='Type Something ...' onChangeText={(text) => this.setState({ posText: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1, flexDirection: 'row' }}>
                                    <View>
                                        <Text style={styles.titleInput}>Date in Korban (*)</Text>
                                        <DatePicker
                                            style={{ width: 320, fontSize: 10, borderRadius: 20 }}
                                            date={this.state.dateInVict}
                                            mode="date"
                                            placeholder="Choose Date ..."
                                            format="YYYY-MM-DD"
                                            minDate="2018-01-01"
                                            maxDate="2050-12-31"
                                            confirmBtnText="Confirm"
                                            cancelBtnText="Cancel"
                                            customStyles={{
                                                dateIcon: {
                                                    position: 'absolute',
                                                    left: 0,
                                                    top: 5,
                                                    marginLeft: 0
                                                },
                                                dateInput: {
                                                    marginLeft: 50, height: 35, borderRadius: 20, fontSize: 10
                                                }
                                            }}
                                            onDateChange={(date) => { this.setState({ dateInVict: date }) }} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 20, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Task Pekerjaan Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.workTask} placeholder='Type Something ...' onChangeText={(text) => this.setState({ workTask: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Nama Supervisor Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.spvName} placeholder='Type Something ...' onChangeText={(text) => this.setState({ spvName: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Lokasi Kejadian</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.locText} placeholder='Type Something ...' onChangeText={(text) => this.setState({ locText: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Penyebab Kejadian</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.acdCaused} placeholder='Type Something ...' onChangeText={(text) => this.setState({ acdCaused: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>No. Badge Saksi</Text>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'column' }}>
                                        <Button
                                            block
                                            style={{ justifyContent: "flex-start", flex: 1, borderWidth: Platform.OS==='ios'?1:0, borderColor: Platform.OS==='ios'?colors.lightGray:null, paddingLeft: 10, marginRight: 5, marginLeft: 5, backgroundColor: colors.gray, height: 35, }}
                                            onPress={() => this.onClickSearchSaksi()}>
                                            <Text style={{ fontSize: 12 }}>{this.state.saksiBadge}</Text>
                                        </Button>
                                    </View>

                                    {this.state.visibleSearchListSaksi &&
                                        <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.gray, margin: 5, borderColor: "#E6E6E6", }}>
                                            <View>

                                                <Textarea value={this.state.searchWordSaksi} style={{ marginLeft: 5, marginRight: 5, fontSize: 11 }} bordered rowSpan={1.5} onChangeText={(text) => this.onChangeSaksi(text)} placeholder='Ketik nama karyawan' />

                                            </View>
                                            <FlatList
                                                data={this.state.listSaksi}
                                                renderItem={this._renderItemSaksi}
                                                keyExtractor={(item, index) => index.toString()}
                                            />
                                        </View>
                                    }
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Nama Saksi</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.saksiName} placeholder='Type Something ...' onChangeText={(text) => this.setState({ saksiName: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Jabatan Saksi Kejadian</Text>
                                    </View>
                                    <View>
                                        <Textarea editable={this.state.selectedStatVict == "1" ? false : true} style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.saksiPosText} placeholder='Type Something ...' onChangeText={(text) => this.setState({ saksiPosText: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Cedera Yang Dialami Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.injText} placeholder='Type Something ...' onChangeText={(text) => this.setState({ injText: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Keterangan Cedera Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.injNote} placeholder='Type Something ...' onChangeText={(text) => this.setState({ injNote: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Pertolongan Pertama Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.firstAid} placeholder='Type Something ...' onChangeText={(text) => this.setState({ firstAid: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Pertolongan Lanjutan Korban</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.nextAid} placeholder='Type Something ...' onChangeText={(text) => this.setState({ nextAid: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Keadaan Syarat Keselamatan Kerja</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.safetyReq} placeholder='Type Something ...' onChangeText={(text) => this.setState({ safetyReq: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Penjelasan Mengenai Kejadian</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.acdRptText} placeholder='Type Something ...' onChangeText={(text) => this.setState({ acdRptText: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Unsafe Action Pada Kejadian</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.unsafeAct} placeholder='Type Something ...' onChangeText={(text) => this.setState({ unsafeAct: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Unsafe Condition Pada Kejadian</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.unsafeCond} placeholder='Type Something ...' onChangeText={(text) => this.setState({ unsafeCond: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Note Terhadap Kejadian (Saran)</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.note} placeholder='Type Something ...' onChangeText={(text) => this.setState({ note: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.viewMerk}>Petugas Kejadian</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.invName} placeholder='Type Something ...' onChangeText={(text) => this.setState({ invName: text })} />
                                    </View>
                                </View>
                            </CardItem>
                        </View>
                        <View style={styles.Contentsave}>
                        <Button
                            block
                            style={{
                                height: 45,
                                marginLeft: 20,
                                marginRight: 20,
                                marginBottom: 20,
                                borderWidth: 1,
                                backgroundColor: "#00b300",
                                borderColor: "#00b300",
                                borderRadius: 4
                            }}
                            onPress={() => this.UpdateAccident()}
                        >
                            <Text style={styles.buttonText}>Submit</Text>
                        </Button>
                    </View>
                    </Content>
                    
                    <View style={{ width: 270, position: "absolute" }}>
                        <Dialog
                            visible={this.state.visibleDialogSubmit}
                            dialogTitle={<DialogTitle title="Updating Accident Report .." />}
                        >
                            <DialogContent>
                            {<ActivityIndicator size="large" color="#330066" animating />}
                            </DialogContent>
                        </Dialog>
                    </View>
                </View>
                <CustomFooter navigation={this.props.navigation} menu='Safety' />
            </Container>

        );
    }
}

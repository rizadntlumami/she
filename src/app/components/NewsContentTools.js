import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback ,Platform, AsyncStorage } from "react-native";
import Ripple from "react-native-material-ripple";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    height: 100,
    width: 100,
    marginLeft: 3,
    marginRight:3,
    paddingTop:3,
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class NewsContentTools extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 5,
            elevation: 1,
            borderWidth:Platform.OS==='ios'?1:0,
    borderColor:(Platform.OS==='ios')?colors.lightGray:null
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          rippleColor={colors.accent}
        >
          <Image
            source={this.props.imageUri}
            style={{ marginTop:2, width: 50, height: 50, resizeMode: "contain" }}
          />
          <View style={{ flex: 1, paddingTop: 3, paddingLeft:10,paddingRight:10, alignItems: "center" }}>
            <Text numberOfLines={1} style={{fontSize: 9}}>{this.props.noBuku}</Text>
          </View>
          <View style={{ flex: 1, paddingTop: 0, paddingLeft:10,paddingRight:10, alignItems: "center" }}>
            <Text numberOfLines={1} style={{fontSize: 9, fontWeight:'bold'}}>{this.props.equipname}</Text>
          </View>
          <View style={{ flex: 1, paddingTop: 0,paddingLeft:10,paddingRight:10, alignItems: "center" }}>
            <Text numberOfLines={1} style={{fontSize: 9}}>{this.props.endDate}</Text>
          </View>
        </Ripple>
      </View>
    );
  }
}

NewsContentTools.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};

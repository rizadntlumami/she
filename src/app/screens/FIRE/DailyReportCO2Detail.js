import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Moment from 'moment';

class DailyReportCO2Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      dataDetail: [],
      isLoading: true,
      listCO2: [],
      listDetail: [],
      listPlant: [],
      visibleDialogSubmit: false,
      delete:false,
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        AsyncStorage.getItem("id").then(id => {
          const url = GlobalConfig.SERVERHOST + "detailInspectionGas";
          var formData = new FormData();
          formData.append("id", id);
          fetch(url, {
            headers: {
              "Content-Type": "multipart/form-data"
            },
            method: "POST",
            body: formData
          })
            .then(response => response.json())
            .then(responseJson => {
              this.setState({
                listCO2: responseJson.data,
                isLoading: false,
              });
            })
            .catch(error => {
              console.log(error);
            });
        });
      }
    );
  }

  navigateToScreen(route, listCO2) {
    AsyncStorage.setItem("list", JSON.stringify(listCO2)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  konfirmasideleteGasC02(){
    this.setState({
      delete: true
    });
  }

  deleteGasCO2() {
    this.setState({
      visibleDialogSubmit: true,
      delete:false,
    });
      const url = GlobalConfig.SERVERHOST + "deleteInspectionGas";
      var formData = new FormData();
      formData.append("id", this.state.listCO2.id);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Success", "Delete Success", [
              {
                text: "Oke"
              }
            ]);
            this.props.navigation.navigate("DailyReportCO2");
          } else {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Error", "Delete Failed", [
              {
                text: "Oke"
              }
            ]);
          }
        });
  }

  render() {
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.listCO2 == null) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center", marginTop: 150 }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <ScrollView>
          <View style={{paddingLeft:10, paddingRight:10, paddingTop:10}}>
            <View style={{flex:1, flexDirection:'row'}}>
              <View style={{width:'50%'}}>
                <View style={{width:'100%', paddingTop:5, paddingBottom:5, paddingLeft:5}}>
                  <Image
                    style={{ marginTop:0, width: '100%', height: '100%', resizeMode: "contain", borderRadius:5}}
                    source={require("../../../assets/images/iconCO2.jpg")}
                  />
                </View>
              </View>
              <View style={{width:'50%', paddingLeft:25, paddingTop:20}}>
                <Text style={styles.detailContent2}>
                  {this.state.listCO2.pplantdesc}
                </Text>
                <Text style={styles.detailTitle}>Pplant</Text>

                <Text style={styles.detailContent2}>
                  PT Semen Indonesia
                </Text>
                <Text style={styles.detailTitle}>Company</Text>

                <Text style={styles.detailContent}>
                  {Moment(this.state.listCO2.inspection_date).format('DD MMMM YYYY')}
                </Text>
                <Text style={styles.detailTitle}>Inspection Date</Text>

                <Text style={styles.detailContent}>
                  {this.state.listCO2.shift}
                </Text>
                <Text style={styles.detailTitle}>Shift</Text>
              </View>
            </View>
            <View style={{marginTop:10,}}>
                <Text style={{fontSize:18, fontWeight:'bold'}}>PIC : {this.state.listCO2.pic_badge} - {this.state.listCO2.pic_name}</Text>
            </View>
            <View style={{flex:1, flexDirection:'row', marginTop:10}}>
              <View style={{width:'50%'}}>
                <View style={{backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, marginRight:5, paddingTop:5,
                    shadowColor:'#000',
                    shadowOffset:{
                      width:0,
                      height:1,
                    },
                    shadowOpacity:1,
                    shadowRadius:2,
                    elevation:3,
                    alignItems:'center'
                  }}>
                    <Icon
                      name="ios-construct"
                      style={{fontSize:40, color:colors.green0}}
                    />
                    <Text style={{fontSize:9}}>Residual Gas Cemetron</Text>
                    <Text style={{fontSize:25, fontWeight:'bold'}}>{this.state.listCO2.total_residual_cemetron}/{this.state.listCO2.total_max_cemetron}</Text>
                </View>
              </View>
              <View style={{width:'50%'}}>
                <View style={{backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, marginLeft:5, paddingTop:5,
                    shadowColor:'#000',
                    shadowOffset:{
                      width:0,
                      height:1,
                    },
                    shadowOpacity:1,
                    shadowRadius:2,
                    elevation:3,
                    alignItems:'center'
                  }}>
                    <Icon
                      name="ios-construct"
                      style={{fontSize:40, color:colors.green0}}
                    />
                    <Text style={{fontSize:9}}>Residual Gas Samator</Text>
                    <Text style={{fontSize:25, fontWeight:'bold'}}>{this.state.listCO2.total_residual_samator}/{this.state.listCO2.total_max_samator}</Text>
                </View>
              </View>
            </View>
            <View style={{marginTop:10, backgroundColor:colors.white, height:100, marginBottom:5, borderRadius:10, paddingLeft:10, paddingRight:10, paddingTop:5,
                shadowColor:'#000',
                shadowOffset:{
                  width:0,
                  height:1,
                },
                shadowOpacity:1,
                shadowRadius:2,
                elevation:3,
              }}>
                <Text style={{fontSize:15, fontWeight:'bold'}}>Note :</Text>
                <Text style={{fontSize:12}}>{this.state.listCO2.note}</Text>
            </View>
          </View>
          </ScrollView>
        );
      }
    }
    return (
      <Container style={styles.wrapper2}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DailyReportCO2")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>
              {this.state.listCO2.pplantdesc}
            </Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() =>
                this.navigateToScreen(
                  "DailyReportCO2Update",
                  this.state.listCO2
                )
              }
            >
              <Icon
                name="ios-create"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            <Button
              transparent
              onPress={() => this.konfirmasideleteGasC02()}
            >
              <Icon
                name="ios-trash"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ marginTop: 5, flex: 1, flexDirection: "column" }}>
          {list}
        </View>
        <View style={{ width: '100%', position: "absolute"}}>
          <Dialog
            visible={this.state.delete}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300}}
            onTouchOutside={() => {
              this.setState({ login: false });
            }}
          >
          <DialogContent>
          {
            <View>
              <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Do you want to remove this data ?</Text>
              </View>
              <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                <View style={{width:'50%', paddingRight:5}}>
                    <Button
                      block
                      style={{
                        width:'100%',
                        marginTop:10,
                        height: 35,
                        marginBottom: 5,
                        borderWidth: 0,
                        backgroundColor: colors.primer,
                        borderRadius: 15
                      }}
                      onPress={() => this.deleteGasCO2()}
                    >
                      <Text style={{color:colors.white}}>Yes</Text>
                    </Button>
                </View>
                <View style={{width:'50%', paddingLeft:5}}>
                    <Button
                      block
                      style={{
                        width:'100%',
                        marginTop:10,
                        height: 35,
                        marginBottom: 5,
                        borderWidth: 0,
                        backgroundColor: colors.primer,
                        borderRadius: 15
                      }}
                      onPress={() => this.setState({
                        delete:false,
                      })}
                    >
                      <Text style={{color:colors.white}}>No</Text>
                    </Button>
                </View>
              </View>
            </View>
          }
          </DialogContent>
          </Dialog>
        </View>
      </Container>
    );
  }
}

export default DailyReportCO2Detail;

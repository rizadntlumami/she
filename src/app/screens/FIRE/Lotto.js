import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl,
  Dimensions
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewLotto from "../../components/ListViewLotto";
import Ripple from "react-native-material-ripple";

class ListItem extends React.PureComponent {
  navigateToScreen(route, idLotto) {
    AsyncStorage.setItem("idLotto", JSON.stringify(idLotto)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View>
        <Ripple
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() => this.navigateToScreen("LottoDetail", this.props.data.id)}
          rippleColor={colors.accent}
        >
          <View  style={{ backgroundColor: "#FEFEFE" }}>
            <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
              <View
                style={{
                  marginTop: 10,
                  marginLeft: 10,
                  marginRight: 10,
                  marginBottom: 10
                }}
              >
                <ListViewLotto
                  equipmentNumber={this.props.data.equipment_number}
                  noER={this.props.data.no_er}
                  area={this.props.data.location}
                  kegiatan={this.props.data.activity_description}
                  date={this.props.data.date}
                  time={this.props.data.time}
                />
              </View>
            </Card>
          </View>
        </Ripple>
      </View>
    );
  }
}

class Lotto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      searchText:'',
      isEmpty:false
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => (
    <ListItem data={item} navigation={this.props.navigation} />
  );

  onRefresh() {
    console.log("refreshing");
    this.setState({ isLoading: true }, function() {
      this.setState({
        searchText:''
      })
      this.loadData();
    });
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.setState({
          searchText:''
        })
        this.loadData();
      }
    );
  }

  navigateToScreen(route, listLotto) {
    AsyncStorage.setItem("list", JSON.stringify(listLotto)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  searchData(){
    this.setState({
      isLoading: true
    });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/firesystem/lotto/view";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("search", this.state.searchText);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log(responseJson)
          if (responseJson.status===200){
            this.setState({
              dataSource: responseJson.data,
              isLoading: false
            });
            this.setState({
              isEmpty:false
            })
          }else{
            this.setState({
              dataSource:[],
              isEmpty:true
            })
            this.setState({
              isLoading:false
            })
          }

        })
        .catch(error => {
          this.setState({
            dataSource:[],
            isEmpty:true
          })
          this.setState({
            isLoading:false
          })
          console.log(error);
        });
    });
  }

  loadData() {
    this.setState({
      isLoading: true
    });
      const url = GlobalConfig.SERVERHOST + "getLotto";
      var formData = new FormData();
      formData.append("token", 'token');

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
            this.setState({
              dataSource: responseJson.data,
              isLoading: false
            });
        })
        .catch(error => {
          this.setState({
            isEmpty:true
          })
          this.setState({
            isLoading:false
          })
          console.log(error);
        });
  }

  render() {
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.dataSource=='') {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list =
        <FlatList
        data={this.state.dataSource}
        renderItem={this._renderItem}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.isLoading}
            onRefresh={this.onRefresh.bind(this)}
          />}
      />
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("FireMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Lotto</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ marginTop: 5 ,flex:1,flexDirection:'column'}}>{list}</View>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: colors.green01,marginBottom: ((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?80:50 }}
          position="bottomRight"
          onPress={() => this.props.navigation.navigate("LottoCreate")}
        >
          <Icon name="ios-add" style={{ fontSize: 50, fontWeight: "bold" ,paddingTop: Platform.OS === 'ios' ? 25:null,}} />
        </Fab>
        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}

export default Lotto;

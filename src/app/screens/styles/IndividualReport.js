import { StyleSheet } from "react-native";
import iPhoneSize from "../../../helpers/utils";
import colors from "../../../styles/colors";
import { Row } from "native-base";

let labelTextSize = 12;
let weatherTextSize = 18;
let weatherTextSmSize = 14;
let headingTextSize = 28;
if (iPhoneSize() === "small") {
  labelTextSize = 10;
  headingTextSize = 24;
  weatherTextSize = 16;
  weatherTextSmSize = 12;
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: "flex",
  },
  facebookButtonIconOrder: {
    color: colors.white,
    fontSize: 30
  },
  facebookButtonIcon: {
    color: colors.green01,
    fontSize: 40
  },
  facebookIcon: {
    color: colors.black,
    fontSize: 30,
    textAlign: 'center'
  },
  header: {
    backgroundColor: colors.green01
  },
  tabfooter: {
    backgroundColor: colors.green01
  },
  textbody: {
    justifyContent: 'center',
    fontWeight: 'bold',
    color: colors.white
  },
  homeWrapper: {
    flex: 1,
    display: "flex",
  },
  weatherContent: {
    flexDirection: "row",
    marginBottom: 15,
    borderBottomColor: colors.lightBlack,
    borderBottomWidth: 1,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  newsContent: {
    flexDirection: "row"
  },
  weatherText: {
    fontFamily: "Montserrat-Medium",
    fontSize: weatherTextSize,
    color: colors.lightBlack,
    fontWeight: "300",
    fontSize: headingTextSize,
    marginBottom: 5,
  },
  weatherTextSm: {
    fontFamily: "Montserrat-Light",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 10
  },
  newsText: {
    fontFamily: "Montserrat-Regular",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 5,
    marginLeft: 20
  },
  news: {
    height: 180,
  },
  search: {
    position: 'absolute',
    left: 10,
    right: 10,
    top: 5,
    flexDirection: 'row',
  },
  searchItem: {
    backgroundColor: 'white',
    paddingHorizontal: 10,
    borderRadius: 20
  },
  itemMenu: {
    flexDirection: "row",
    padding: 2,
    marginTop: 4,
    justifyContent: 'center'
  },
  itemAPDLeft: {
    width: 90
  },

  loginButton: {
    margin: 30,
    height: '75%',
  },
  apdorderimage: {
    alignItems: 'center',
    width: 90,
    height: 90,
    marginTop: 2,
    marginLeft: 1,
    marginRight: 2,
  },
  apdorderimg: {
    width: 60,
    height: 60,
  },
  viewLeft: {
    width: 100,
  },
  viewLeftHeader: {
    width: 350,
  },
  viewLeftHeader2: {
    width: 80,
  },
  viewLeftOrder: {
    width: 70,
  },
  viewRightOrder: {
    width: 150,
  },
  viewCenter: {
    width: 120,
    fontSize: 15
  },
  viewRight: {
    width: 90,
    textAlign: 'right',
    fontSize: 15
  },
  roundedBtn: {
    padding: 10,
    justifyContent: 'center',
    flexDirection: "row"
  },
  cardtext: {
    fontSize: 11
  },
  cardtextKode: {
    fontSize: 12,
    color: colors.red,
    fontWeight: 'bold'
  }
});

export default styles;

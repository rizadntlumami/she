import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  AsyncStorage,
  ActivityIndicator,
  Platform,
  Alert
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
  Icon
} from "native-base";
import LinearGradient from "react-native-linear-gradient";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import RoundedButtons from "../../components/RoundedButton";
import styles from "../styles/OrderAPD";
import colors from "../../../styles/colors";
import SnackBar from "./SnackBarOrderPersonal";
import GlobalConfig from "../../components/GlobalConfig";

var that;
export default class OrderPersonalTroli extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      listAPDPersonal: [],
      jumlahApd: [],
      isloading:true,
      visibleDialogSubmit:false,
      textDialogSubmit:'',
      visibleKonfirmasi:false,
      listOrder:[],
      listID:[],
    };
  }

  static navigationOptions = {
    header: null
  };

  tambahJumlah() {
    alert("Anda Tidak Bisa menambahkan Jumlah")
  }

  kurangJumlah() {
    alert("Anda Tidak Bisa menambahkan Jumlah")
  }

  // tambahOrder(kodeAPD, jumlah){
  //   let kodeAPDLet = kodeAPD;
  //   let jumlahLet = jumlah;
  //
  //       var temp = {'kode_apd': kodeAPDLet, 'jumlah': jumlahLet};
  //       var data = this.state.listOrder;
  //       data.push(temp);
  //
  //       this.setState({
  //         listOrder:data
  //       })
  // }


  sendApd() {
    for (let i = 0; i < this.state.dataSource.length; i++) {
      let idAPD = this.state.dataSource[i].id;
      let kodeAPD = this.state.dataSource[i].kode_apd;
      let jumlahLet = this.state.dataSource[i].jumlah;
      var temp = {'kode_apd': kodeAPD, 'jumlah': jumlahLet};
          var data = this.state.listOrder;
          var idWishlist = this.state.listID;
          data.push(temp);
          idWishlist.push(idAPD)

          this.setState({
            listOrder:data,
            listID:idWishlist,
          })
    }
    this.loadFinalOrder()
  }

  deleteWishlistAll(){
    for (let i = 0; i < this.state.listID.length; i++) {
      let idAPD = this.state.listID[i];

      AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'orderapd/wishlist/delete';
        var formData = new FormData();
        formData.append("token", value);
        formData.append("id", idAPD);

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
      })
    }
    this.loadData();
  }

  loadFinalOrder(){

    this.setState({ visibleKonfirmasi: false })
    this.setState({
      textDialogSubmit:'Creating order...',
    })
    this.setState({
      visibleDialogSubmit:true,
    })

    AsyncStorage.getItem("token").then(value => {
      var url = GlobalConfig.SERVERHOST + "orderapd/create/personal";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("no_badge", this.state.badge);
      formData.append("status", 'ORDER');
      formData.append("detail", JSON.stringify(this.state.listOrder));

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit:false,
              listOrder:[],
            })
            Alert.alert('Success', 'Order Success, ID ORDER: '+response.data.kode_order, [{
              text: 'Okay'
            }])
            this.deleteWishlistAll();
            this.props.navigation.navigate('OrderPersonalAPD')
          } else {
            this.setState({
              visibleDialogSubmit:false,
            })
            Alert.alert('Error', 'Order Failed', [{
              text: 'Okay'
            }])
          }
        })
        .catch((error) => {
          Alert.alert('Error', 'Check Your Internet Connection', [{
              text: 'Okay'
          }])
            console.log(error)
        })
    });
  }

  componentDidMount() {
    AsyncStorage.getItem("token").then(token => {
      const url = GlobalConfig.SERVERHOST + 'validasi';
      var formData = new FormData();
      formData.append("token", token)
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson))
          this.setState({
            listProfile: responseJson.user,
            name: responseJson.user.name,
            badge: responseJson.user.no_badge,
            isloading: false
          });
          this.loadData();
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  loadData() {
    AsyncStorage.getItem('token').then((value) => {
      const url = GlobalConfig.SERVERHOST + 'orderapd/wishlist/get';
      var formData = new FormData();
      formData.append("token", value);
      formData.append("badge", this.state.badge);
      formData.append("status", 'PERSONAL');

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'POST',
        body: formData
      })
        .then((response) => response.json())
        .then((responseJson) => {
          // if (responseJson.data==undefined){
          //   this.setState({
          //     jumlahApd: [],
          //     dataSource: undefined,
          //     isloading: false
          //   });
          // }else{
            this.setState({
              dataSource: responseJson.data,
              isloading: false
            });
          //   var arr = new Array(this.state.dataSource.length);
          //   var tempData = this.state.dataSource;
          //   for (let index = 0; index < arr.length; index++) {
          //     arr[index] = parseInt(tempData[index].jumlah)
          //   }
          //   this.setState({
          //     jumlahApd: arr
          //   });
          // }
        })
        .catch((error) => {
          console.log(error)
        })
    })
  }

  deleteWishlist(id, kodeAPD) {
    this.setState({
      textDialogSubmit:'Deleting Item...',
    })
    this.setState({
      visibleDialogSubmit:true,
    })

    AsyncStorage.getItem('token').then((value) => {
      const url = GlobalConfig.SERVERHOST + 'orderapd/wishlist/cancel';
      var formData = new FormData();
      formData.append("token", value);
      formData.append("id", id);
      formData.append("kode_apd", kodeAPD);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit:false,
            })
            Alert.alert('Success', 'Delete Success', [{
              text: 'Okay',
            }])
            this.loadData();
          } else {
            this.setState({
              visibleDialogSubmit:false,
            })
            Alert.alert('Error', 'Troli Failed', [{
              text: 'Okay'
            }])
          }
        })
        .catch((error) => {
          Alert.alert('Error', 'Check Your Internet Connection', [{
              text: 'Okay'
          }])
            console.log(error)
        })
    })
  }

  render() {
    let arr = this.state.jumlahApd;
    var listAPDPersonal;

    //solving issue map of undefined, map pindah dari blok return, dengan melakukan cek isi dari dataSource lebih dulu
    if (this.state.dataSource) {
      listAPDPersonal = this.state.dataSource.map((apdListPersonal, index) => (
        <View key={index} style={{ backgroundColor: "#FEFEFE" }}>
            <CardItem style={{ borderRadius: 0, marginTop:4, backgroundColor:colors.gray }}>
                <View style={{ flex: 1, flexDirection: "row", marginTop:0, backgroundColor:colors.gray }}>
                    <View style={{width:'60%'}}>
                        <Text style={styles.viewName}>
                          {apdListPersonal.kode_apd}
                        </Text>
                        <Text style={styles.viewMerk}>
                          {apdListPersonal.name_apd}
                        </Text>
                    </View>

                    <View style={{width:'40%'}}>
                        <View style={{ flex: 1, flexDirection: "row"}}>
                          <Button onPress={() => this.kurangJumlah()} style={styles.btnQTYLeft}>
                              <Icon
                                name="ios-arrow-back"
                                style={styles.facebookButtonIconQTY}
                              />
                          </Button>

                          <View style={{width:30,height:30, marginTop: 0, backgroundColor: colors.white}}>
                            <Input
                              style={{
                                height:30,
                                marginTop:-5,
                                fontSize:12,
                                textAlign:'center'}}
                              value="1"
                              keyboardType='numeric'
                            />
                          </View>

                          <Button onPress={() => this.tambahJumlah()} style={styles.btnQTYRight}>
                            <Icon
                              name="ios-arrow-forward"
                              style={styles.facebookButtonIconQTY}
                            />
                          </Button>

                          <View style={styles.btnWishlist}>
                            <Button transparent onPress={() => this.deleteWishlist(apdListPersonal.id, apdListPersonal.kode_apd)}>
                              <Icon
                                name="ios-trash"
                                style={styles.facebookButtonIconDangerous}
                              />
                            </Button>
                          </View>
                        </View>
                    </View>
                </View>
            </CardItem>
        </View>
      ))
    }
    //batas solving

    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("OrderPersonalAPD")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:4,alignItems:'center'}}>
            <Title style={styles.textbody}>Konfirmasi Order</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Footer style={styles.tabHeight}>
          <FooterTab style={styles.tabfooter}>
            <Button active style={styles.tabfooter}>
              <View style={{height:'40%'}}></View>
              <View style={{height:'50%'}}>
              <Text>PERSONAL</Text>
              </View>
              <View style={{height:'20%'}}></View>
              <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.white}}></View>
            </Button>
            <Button onPress={() => this.props.navigation.navigate("OrderUnitKerjaTroli")}>
              <Text style={{color:'white', fontWeight:'bold'}}>UNIT KERJA</Text>
            </Button>
            <Button onPress={() => this.props.navigation.navigate("OrderPeminjamanTroli")}>
              <Text style={{color:'white', fontWeight:'bold'}}>PEMINJAMAN</Text>
            </Button>
          </FooterTab>
        </Footer>
        <View style={{ flex: 1 }}>
          <Content style={{ marginTop: 0 }}>
          {this.state.isloading ?
            (
              <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <ActivityIndicator size="large" color="#330066" animating />
            </View>
            ) :
            listAPDPersonal
          }
          </Content>
          {listAPDPersonal == '' ? (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center"  }}>
              <Thumbnail
                square
                large
                source={require("../../../assets/images/empty.png")}
              />
              <Text>No Item!</Text>
            </View>
          ) : (
            <View style={styles.Contentsave}>
              <Button
                block
                style={{
                  height: 45,
                  marginLeft: 20,
                  marginRight: 20,
                  marginBottom: 20,
                  borderWidth: 1,
                  backgroundColor: "#00b300",
                  borderColor: "#00b300",
                  borderRadius: 4
                }}
                onPress={() => this.setState({ visibleKonfirmasi: true })}
              >
                <Text style={styles.buttonText}>Submit</Text>
              </Button>
            </View>
          )}
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialogSubmit}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogTitle={<DialogTitle title={this.state.textDialogSubmit} />}
          >
            <DialogContent>
              {

                  <ActivityIndicator size="large" color="#330066" animating />

              }
            </DialogContent>
          </Dialog>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleKonfirmasi}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            onTouchOutside={() => {
              this.setState({ visibleKonfirmasi: false });
            }}
            // dialogStyle={{ position: 'absolute', top: this.state.posDialog }}
            dialogTitle={
              <DialogTitle
                title="Apakah yang Anda pesan sudah sesuai?"
                style={{
                  backgroundColor: colors.white,
                  marginTop:20,
                  marginBottom:20
                }}
                textStyle={{fontSize:15}}
                hasTitleBar={false}
                align="center"
              />
            }
          >
            <DialogContent
              style={{
                backgroundColor: colors.white
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Button
                    block
                    style={{
                      height: 45,
                      marginLeft: 20,
                      marginRight: 20,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: colors.gray02,
                      borderColor: colors.gray02,
                      borderRadius: 4
                    }}
                    onPress={() => this.setState({ visibleKonfirmasi: false })}
                  >
                    <Text style={styles.buttonText}>Batal</Text>
                  </Button>
                </View>
                <View style={{ flex: 1 }}>
                  <Button
                    block
                    style={{
                      height: 45,
                      marginLeft: 20,
                      marginRight: 20,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: "#00b300",
                      borderColor: "#00b300",
                      borderRadius: 4
                    }}
                    onPress={() => this.sendApd()}
                  >
                    <Text style={styles.buttonText}>Iya</Text>
                  </Button>
                </View>
              </View>
            </DialogContent>
          </Dialog>
        </View>
      </Container>
    );
  }
}

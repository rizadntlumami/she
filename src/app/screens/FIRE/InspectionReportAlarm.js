import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail,
  Textarea,
  Picker,
  Form
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewAlarm from "../../components/FireSystem/ListViewAlarm";
import Ripple from "react-native-material-ripple";
import DatePicker from "react-native-datepicker";
import Moment from "moment";

var thatperiode;
class ListItem extends React.PureComponent {
  navigateToScreen(route, idLokasi, periode) {
    AsyncStorage.setItem("idLokasi", idLokasi).then(() => {
      AsyncStorage.setItem("periode", periode).then(() => {
        this.props.navigation.navigate(route);
      });
    });
  }

  render() {
    return (
      <View style={{marginLeft: 10, marginRight: 10, borderRadius: 5, backgroundColor:colors.white, marginBottom:10,
        shadowColor:'#000',
        shadowOffset:{
          width:0,
          height:2,
        },
        shadowOpacity:0.25,
        shadowRadius:3.84,
        elevation:2,
      }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.navigateToScreen(
              "InspectionReportAlarmDetail",
              this.props.data.ID_LOKASI_ALARM,
              thatperiode.state.periode
            )
          }
          rippleColor={colors.accent}
        >
          <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
            <ListViewAlarm
              imageUri={require("../../../assets/images/alarm.png")}
              unit_kerja={this.props.data.unit_kerja}
              area={this.props.data.area}
              last_inspection={this.props.data.last_inspection}
              exp_inspection={this.props.data.exp_inspection}
            />
          </View>
        </Ripple>
      </View>
    );
  }
}


class InspectionReportAlarm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      dataSource2: [],
      isLoading: true,
      visibleDialog: false,
      periode: ""
    };
    thatperiode = this;
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => (
    <ListItem data={item} navigation={this.props.navigation} />
  );


  onRefresh() {
    console.log("refreshing");
    this.setState({ isLoading: true }, function() {
      this.loadData();
    });
  }

  componentDidMount() {
    var dateNow = Moment().format("YYYY-MM");
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
        this.setState({
          periode: dateNow
        });
        this.setState({ isLoading: true });
      }
    );
  }

  loadData() {
    this.setState({ visibleDialog: false, isLoading: true });
      const url = GlobalConfig.SERVERHOST + "getLokasiAlarm";
      var formData = new FormData();
      formData.append("token", 'token');

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataSource: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
  }

  onClickSearch() {
    this.setState({
      visibleDialog: true
    });
  }
  render() {
    that = this;
    var listAlarm;
    if (this.state.isLoading) {
      listAlarm = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.dataSource == "" && this.state.dataSource2 == "") {
        listAlarm = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        listAlarm = (
          <View>
            <FlatList
              data={this.state.dataSource}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              onEndThreshold={0.5}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isLoading}
                  onRefresh={this.onRefresh.bind(this)}
                />
              }
            />
          </View>
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("FireMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>Inspection Report</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button transparent onPress={() => this.onClickSearch()}>
              <Icon
                name="ios-funnel"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />

        <Footer>
          <FooterTab style={styles.tabfooter}>
            <Button
              onPress={() =>
                this.props.navigation.navigate("InspectionReportApar")
              }
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>Apar</Text>
            </Button>
            <Button
              onPress={() =>
                this.props.navigation.navigate("InspectionReportHydrant")
              }
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>
                Hydrant
              </Text>
            </Button>
            <Button active style={styles.tabfooter}>
              <View style={{ height: "40%" }} />
              <View style={{ height: "50%" }}>
                <Text style={styles.textbody}>Alarm</Text>
              </View>
              <View style={{ height: "20%" }} />
              <View
                style={{
                  borderWidth: 2,
                  marginTop: 2,
                  height: 0.5,
                  width: "100%",
                  borderColor: colors.white
                }}
              />
            </Button>
          </FooterTab>
        </Footer>

        <View style={{ marginTop: 10,flex:1,flexDirection:'column' }}>{listAlarm}</View>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: colors.green01, marginBottom: 30 }}
          position="bottomRight"
          onPress={() =>
            this.props.navigation.navigate("InspectionReportAlarmScan")
          }
        >
          <Icon
            name="ios-add"
            style={{
              fontSize: 50,
              fontWeight: "bold",
              paddingTop: Platform.OS === "ios" ? 25 : null
            }}
          />
        </Fab>

        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialog}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ visibleDialog: false });
            }}
            dialogTitle={<DialogTitle title="Select sorting here" />}
            actions={[
              <DialogButton
                style={{
                  fontSize: 11,
                  backgroundColor: colors.white,
                  borderColor: colors.blue01
                }}
                text="SORT"
                onPress={() => this.loadData()}
                // onPress={() => this.setState({ visibleFilter: false })}
              />
            ]}
          >
            <DialogContent>
              {
                <View>
                  <Text style={{ paddingTop: 20, fontWeight: 'bold', fontSize: 10 }}>Periode *</Text>
                  <DatePicker
                  style={{ width: 300, fontSize: 10, borderRadius: 20 }}
                  date={this.state.periode}
                  mode="date"
                  placeholder="Choose Start Date ..."
                  format="YYYY-MM"
                  minDate="2018-01"
                  maxDate="5000-12"
                  confirmBtnText="Confirm"
                  cancelBtnText="Cancel"
                  customStyles={{
                    dateInput: {
                      marginLeft: 5,
                      marginRight: 5,
                      height: 35,
                      borderRadius: 5,
                      fontSize: 10,
                      borderWidth: 1,
                      borderColor: "#E6E6E6"
                    },
                    dateIcon: {
                      position: "absolute",
                      left: 0,
                      top: 5
                    }
                  }}
                  onDateChange={date => {
                    this.setState({ periode: date });
                  }}
                />
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>

        <CustomFooter navigation={this.props.navigation} menu="FireSystem" />
      </Container>
    );
  }
}

export default InspectionReportAlarm;

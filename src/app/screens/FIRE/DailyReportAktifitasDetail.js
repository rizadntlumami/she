import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewHeader from "../../components/ListViewHeader";
import ListViewDetail from "../../components/ListViewDetail";
import Moment from 'moment';

class ListTools extends React.PureComponent {
  render() {
    return (
      <View style={{ borderWidth:1, height:50, borderColor:colors.gray, borderRadius:5, paddingLeft:10, paddingRight:10, marginBottom:10,}}>
          <View style={{ flex:1, flexDirection:'row'}}>
            <View style={{width:'15%', justifyContent: "center"}}>
              <Icon
                name="ios-construct"
                style={{fontSize:35, color:colors.green0}}
              />
            </View>
            <View style={{width:'65%', justifyContent: "center"}}>
              <Text style={{fontSize:14, fontWeight:'bold'}}>{this.props.data.tools}</Text>
            </View>
            <View style={{width:'20%', justifyContent: "center"}}>
              <Text style={{fontSize:12, color:colors.graydark}}>{this.props.data.total} Item</Text>
            </View>
          </View>
      </View>
    );
  }
}

class DailyReportAktifitasDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      listAktifitas: [],
      listTools: [],
      listPIC: [],
      visibleDialogSubmit: false,
      delete:false,
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderTools = ({ item }) => <ListTools data={item} />;

  navigateToScreen(route, listAktifitas) {
    AsyncStorage.setItem("list", JSON.stringify(listAktifitas)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        AsyncStorage.getItem("id").then(id => {
          const url = GlobalConfig.SERVERHOST + "detailDailySectionActivity";
          var formData = new FormData();
          formData.append("id", id);
          fetch(url, {
            headers: {
              "Content-Type": "multipart/form-data"
            },
            method: "POST",
            body: formData
          })
            .then(response => response.json())
            .then(responseJson => {
              // alert(JSON.stringify(responseJson))
              this.setState({
                listAktifitas: responseJson.data,
                listTools: JSON.parse(responseJson.data.list_tools),
                isLoading: false,
              });
            })
            .catch(error => {
              console.log(error);
            });
          // this.setState({ listAktifitas: JSON.parse(listAktifitas) });
          // this.setState({ isLoading: false });
          // this.setState({
          //   listPIC: JSON.parse(this.state.listAktifitas.pic),
          //   listTools: JSON.parse(this.state.listAktifitas.list_tools)
          // });
        });
      }
    );
  }


  konfirmasideleteAktifitas(){
    this.setState({
      delete: true
    });
  }

  deleteAktifitas() {
    this.setState({
      visibleDialogSubmit: true,
      delete:false,
    });
      const url = GlobalConfig.SERVERHOST + "deleteDailySectionActivity";
      var formData = new FormData();
      formData.append("id", this.state.listAktifitas.id);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Success", "Delete Success", [
              {
                text: "Oke"
              }
            ]);
            this.props.navigation.navigate("DailyReportAktifitas");
          } else {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Error", "Delete Failed", [
              {
                text: "Oke"
              }
            ]);
          }
        });
  }

  render() {
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.listAktifitas == null) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <ScrollView>
          <View style={{paddingLeft:10, paddingRight:10, paddingTop:10}}>
            <View style={{flex:1, flexDirection:'row'}}>
              <View style={{width:'60%'}}>
                <View style={styles.placeholder}>
                  <Image
                    style={styles.previewImage}
                    source={{
                      uri:
                        GlobalConfig.IMAGEHOST + 'DailySectionReport/' + this.state.listAktifitas.image
                    }}
                  />
                </View>
              </View>
              <View style={{width:'40%', paddingLeft:25, paddingTop:20}}>
                <Text style={styles.detailContent}>
                  {this.state.listAktifitas.type_action}
                </Text>
                <Text style={styles.detailTitle}>Type Action</Text>

                <Text style={styles.detailContent}>
                  {this.state.listAktifitas.timesheet_start}
                </Text>
                <Text style={styles.detailTitle}>Time Start</Text>

                <Text style={styles.detailContent}>
                  {this.state.listAktifitas.timesheet_end}
                </Text>
                <Text style={styles.detailTitle}>Time End</Text>

                <Text style={styles.detailContent}>
                  {this.state.listAktifitas.shift1 == "V" && (
                    1
                  )}
                  {this.state.listAktifitas.shift2 == "V" && (
                    - 2
                  )}
                  {this.state.listAktifitas.shift3 == "V" && (
                    - 3
                  )}
                </Text>
                <Text style={styles.detailTitle}>Shift</Text>
              </View>
            </View>
            <View style={{marginTop:10, flex:1, flexDirection:'row'}}>
              <View style={{width:'75%'}}>
                <Text style={{fontSize:18, fontWeight:'bold'}}>PIC : {this.state.listAktifitas.pic}</Text>
                <Text style={{fontSize:10, color:colors.graydark}}>Tuban, {Moment(this.state.listAktifitas.date_activity).format('DD MMMM YYYY')}</Text>
              </View>
              <View style={{width:'25%'}}>
                <View style={{borderWidth:1, height:40, borderColor:colors.gray, borderRadius:10, alignItems:'center', justifyContent: "center",}}>
                  <Icon
                    name="ios-unlock"
                    style={{fontSize:18}}
                  />
                  <Text style={{fontSize:15, fontWeight:'bold', color:colors.green0}}>{this.state.listAktifitas.status}</Text>
                </View>
              </View>
            </View>
            <View style={{marginTop:10}}>
              <View style={{backgroundColor:colors.white, height:100, borderRadius:10, paddingLeft:10, paddingRight:10, paddingTop:5,
                shadowColor:'#000',
                shadowOffset:{
                  width:0,
                  height:1,
                },
                shadowOpacity:1,
                shadowRadius:2,
                elevation:3,
              }}>
              <Text style={{fontSize:15, fontWeight:'bold'}}>Daily Activity :</Text>
              <Text style={{fontSize:12}}>{this.state.listAktifitas.daily_activity}</Text>
              </View>
            </View>
            <View style={{marginTop:10}}>
              <Text style={{ fontSize: 13, paddingTop: 10, fontWeight:'bold', marginBottom:10 }}>Tools</Text>
              <FlatList
                data={this.state.listTools}
                renderItem={this._renderTools}
                keyExtractor={(item, index) => index.toString()}
              />
            </View>
          </View>
          </ScrollView>
        );
      }
    }
    return (
      <Container style={styles.wrapper2}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() =>
                this.props.navigation.navigate("DailyReportAktifitas")
              }
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>Daily Report Aktifitas</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() =>
                this.navigateToScreen(
                  "DailyReportAktifitasUpdate",
                  this.state.listAktifitas
                )
              }
            >
              <Icon
                name="ios-create"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
            <Button
              transparent
              onPress={() => this.konfirmasideleteAktifitas()}
            >
              <Icon
                name="ios-trash"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ marginTop: 5, flex: 1, flexDirection: "column" }}>
          {list}

          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
            >
              <DialogContent>
                {
                  <View>
                  <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                    <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Deleting Activity ...</Text>
                  </View>
                  <ActivityIndicator size="large" color="#330066" animating />
                  </View>
                }
              </DialogContent>
            </Dialog>
          </View>
        </View>

        <View style={{ width: '100%', position: "absolute"}}>
          <Dialog
            visible={this.state.delete}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300}}
            onTouchOutside={() => {
              this.setState({ login: false });
            }}
          >
          <DialogContent>
          {
            <View>
              <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Do you want to remove this data ?</Text>
              </View>
              <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                <View style={{width:'50%', paddingRight:5}}>
                    <Button
                      block
                      style={{
                        width:'100%',
                        marginTop:10,
                        height: 35,
                        marginBottom: 5,
                        borderWidth: 0,
                        backgroundColor: colors.primer,
                        borderRadius: 15
                      }}
                      onPress={() => this.deleteAktifitas()}
                    >
                      <Text style={{color:colors.white}}>Yes</Text>
                    </Button>
                </View>
                <View style={{width:'50%', paddingLeft:5}}>
                    <Button
                      block
                      style={{
                        width:'100%',
                        marginTop:10,
                        height: 35,
                        marginBottom: 5,
                        borderWidth: 0,
                        backgroundColor: colors.primer,
                        borderRadius: 15
                      }}
                      onPress={() => this.setState({
                        delete:false,
                      })}
                    >
                      <Text style={{color:colors.white}}>No</Text>
                    </Button>
                </View>
              </View>
            </View>
          }
          </DialogContent>
          </Dialog>
        </View>
      </Container>
    );
  }
}

export default DailyReportAktifitasDetail;

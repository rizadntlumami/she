import { StyleSheet,Platform,Dimensions } from "react-native";
import iPhoneSize from "../../../helpers/utils";
import colors from "../../../styles/colors";
import { Row } from "native-base";

let labelTextSize = 12;
let weatherTextSize = 18;
let weatherTextSmSize = 14;
let headingTextSize = 28;
if (iPhoneSize() === "small") {
  labelTextSize = 10;
  headingTextSize = 24;
  weatherTextSize = 16;
  weatherTextSmSize = 12;
}

const styles = StyleSheet.create({
  tabHeight: {
    height:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?22:null
  },
  wrapper: {
    flex: 1,
    display: "flex",
  },
  facebookButtonIcon: {
    color: colors.white
  },
  header: {
    backgroundColor: colors.green01,
  },
  tabfooter: {
    backgroundColor: colors.green01
  },
  textbody: {
    fontWeight: 'bold',
    color: colors.white
  },
  homeWrapper: {
    flex: 1,
    display: "flex",
  },
  weatherContent: {
    flexDirection: "row",
    marginBottom: 15,
    borderBottomColor: colors.lightBlack,
    borderBottomWidth: 1,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  newsContent: {
    flexDirection: "row"
  },
  apdimage: {
    alignItems: 'center',
    width: 160,
    height: 160,
    marginTop: 20,
    marginLeft: 80,
    marginRight: 80,
  },
  weatherText: {
    fontFamily: "Montserrat-Medium",
    color: colors.lightBlack,
    fontWeight: "300",
    fontSize: headingTextSize,
    marginBottom: 5,
    marginLeft: 50,
    marginRight: 50
  },
  weatherTextSm: {
    fontFamily: "Montserrat-Light",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 10
  },
  buttonText: {
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#fff',
  },
  Contentsave: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  viewLeftHeader2: {
    width: 80,
  },
  newsText: {
    fontFamily: "Montserrat-Regular",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 5,
    marginLeft: 20
  },
  news: {
    height: 160,
    marginTop: 20
  },
  viewRightHeader: {
    width: 200,
  },
  viewLeftHeader2: {
    width: 100,
  },
  viewRightHeader2: {
    width: 170,
  },
  search: {
    position: 'absolute',
    left: 10,
    right: 10,
    top: 5,
    flexDirection: 'row',
  },
  searchItem: {
    backgroundColor: 'white',
    paddingHorizontal: 10,
    borderRadius: 20
  },
  viewLeftHeader: {
    width: '100%',
  },
  cardtext: {
    fontSize: 11
  },
  cardtextKode: {
    fontSize: 12,
    color: colors.green01,
    fontWeight: 'bold'
  },
  search: {
    position: 'absolute',
    left: 5,
    right: 5,
    top: 1,
    height: 10,
    flexDirection: 'row',
  },
  searchItem: {
    backgroundColor: 'white',
    paddingHorizontal: 5,
    borderRadius: 20,
  },
  textActiveTab:{
    color:colors.white
  }
});

export default styles;

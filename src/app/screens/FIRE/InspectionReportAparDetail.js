import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Ripple from "react-native-material-ripple";
import SwitchToggle from "react-native-switch-toggle";
import Moment from "moment";

class InspectionReportAparDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataLokasi: [],
      isLoading: true,
      listReportApar: [],
      idApar: "",
      idReport: "",
      hose: false,
      seal: false,
      sapot: false,
      kondisiHose: "",
      kondisiSeal: "",
      kondisiSapot: "",
      visibleDialogSubmit: false,
      periode: "",
      exp_inspection:"",
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => (
    <ListItem data={item} navigation={this.props.navigation} />
  );

  componentDidMount() {
    AsyncStorage.getItem("idLokasi").then(idLokasi => {
        this.setState({
          idApar: idLokasi,
          isLoading: false,
        });
        this.loadLokasi();
    });
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
        // AsyncStorage.getItem("idApar").then(idApar => {
        //   AsyncStorage.getItem("periode").then(periode => {
        //     this.setState({ idApar: idApar, periode: periode });
        //     this.setState({ isLoading: false });
        //     console.log(this.state.idApar);
        //     console.log(this.state.periode);
        //   });
        // });
      }
    );
  }

  loadLokasi() {
    this.setState({ visibleDialog: false, isLoading: true });
      const url = GlobalConfig.SERVERHOST + "getLokasiApar";
      var formData = new FormData();
      formData.append("id_apar", this.state.idApar);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataLokasi: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
  }

  loadData() {
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + "detailInspectionApar";
      var formData = new FormData();
      formData.append("id_apar", this.state.idApar);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listReportApar: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  navigateToScreen(route, listReportApar) {
    AsyncStorage.setItem("listReportApar", JSON.stringify(listReportApar)).then(() => {
        this.props.navigation.navigate(route);
        // alert(JSON.stringify(listReportApar))
      }
    );
  }

  navigateToScreenCreate(route, idApar) {
    AsyncStorage.setItem("idApar", idApar).then(() => {
        this.props.navigation.navigate(route);
        // alert(JSON.stringify(idApar))
      }
    );
  }

  render() {
    var that = this;
    var dateNow = Moment().format('YYYY-MM-DD');

    return (
      <Container style={styles.wrapper2}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() =>
                that.props.navigation.navigate("InspectionReportApar")
              }
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>
              {this.state.dataLokasi.area}
            </Title>
          </Body>
          <Right style={{ flex: 1 }}>
              {/*<View style={{ flex: 0, flexDirection: "row" }}>
                <Button
                  transparent
                  onPress={() =>
                    this.navigateToScreen(
                      "InspectionReportAparUpdate",
                      this.state.listReportApar
                    )
                  }
                >
                  <Icon
                    name="ios-create"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button>
                <Button
                  transparent
                  onPress={() =>
                    this.konfirmasideleteApar(
                      listReport.UK_TEXT,
                      listReport.ID
                    )
                  }
                >
                  <Icon
                    name="ios-history"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button>
              </View>*/}
          </Right>
        </Header>
        <StatusBar
          backgroundColor={colors.green03}
          barStyle="light-content"
        />
        <View style={{marginTop:10, marginLeft:10, marginRight:10, backgroundColor:colors.white, height:120, marginBottom:5, borderRadius:10, paddingLeft:10, paddingRight:10, paddingTop:5,
            shadowColor:'#000',
            shadowOffset:{
              width:0,
              height:1,
            },
            shadowOpacity:1,
            shadowRadius:2,
            elevation:3,
          }}>

            <Text style={{ fontSize: 13, fontWeight: "bold" }}>
              {this.state.dataLokasi.area}
            </Text>
            <Text style={{ fontSize: 10 }}>Area</Text>
            <Text style={{ fontSize: 13, paddingTop: 5, fontWeight: "bold" }}>
              {this.state.dataLokasi.unit_kerja}
            </Text>
            <Text style={{ fontSize: 10 }}>
              Unit Kerja
            </Text>
            <Text style={{ fontSize: 13, paddingTop:5, fontWeight: "bold" }}>
                {Moment(this.state.dataLokasi.last_inspection).format('DD MMMM YYYY')}
            </Text>
            <Text style={{ fontSize: 10 }}>Last Inspection</Text>
        </View>
        <ScrollView>
          <Content>
            <CardItem style={{ borderRadius: 0 }}>
              <View>
                <Text style={{ fontSize: 10 }}>Status</Text>
                {dateNow <= this.state.dataLokasi.exp_inspection ? (
                  <Text
                    style={{
                      fontSize: 10,
                      fontWeight: "bold",
                      color: colors.green01
                    }}
                  >
                    CHECKED
                  </Text>
                ) : (
                  <Text
                    style={{
                      fontSize: 10,
                      fontWeight: "bold",
                      color: "#FF0101"
                    }}
                  >
                    UNCECKED
                  </Text>
                )}
              </View>
            </CardItem>

            {dateNow <= this.state.dataLokasi.exp_inspection ? (
              <View>
                <CardItem style={{ borderRadius: 0 }}>
                  <View>
                    <Text style={{ fontSize: 10, paddingTop: 5 }}>
                      Petugas Periksa
                    </Text>
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.listReportApar.pic_name}
                    </Text>
                  </View>
                </CardItem>

                <CardItem style={{ borderRadius: 0 }}>
                  <View style={{ flexDirection: "row" }}>
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: 10 }}>Tgl</Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {this.state.listReportApar.inspection_date}
                      </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: 10 }}>JK</Text>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          {this.state.listReportApar.klem}
                        </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: 10 }}>Press</Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {this.state.listReportApar.press}
                      </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: 10 }}>Weight</Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {this.state.listReportApar.weight}
                      </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: 10 }}>Hose</Text>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          {this.state.listReportApar.hose}
                        </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: 10 }}>Seal</Text>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          {this.state.listReportApar.seal}
                        </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: 10 }}>Sapot</Text>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          {this.state.listReportApar.sapot}
                        </Text>
                    </View>
                    <View style={{ flex: 1 }}>
                      <Text style={{ fontSize: 10 }}>Kondisi</Text>
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {this.state.listReportApar.status_cond}
                      </Text>
                    </View>
                  </View>
                </CardItem>
                <CardItem style={{ borderRadius: 0 }}>
                  <View>
                    <Text style={{ fontSize: 10, paddingTop: 5 }}>
                      Informasi
                    </Text>
                    {this.state.listReportApar.note != null ? (
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {this.state.listReportApar.note}
                      </Text>
                    ) : (
                      <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                        {" "}
                        -
                      </Text>
                    )}
                  </View>
                </CardItem>
              </View>
            ):(
              <Button
                block
                style={{
                  height: 45,
                  marginLeft: 20,
                  marginRight: 20,
                  marginBottom: 20,
                  borderWidth: 1,
                  backgroundColor: "#00b300",
                  borderColor: "#00b300",
                  borderRadius: 4
                }}
                onPress={() =>
                  this.navigateToScreenCreate("InspectionReportAparCreate", this.state.idApar)
                }
              >
                <Text
                  style={{
                    fontSize: 14,
                    fontWeight: "bold",
                    color: colors.white
                  }}
                >
                  Create Inspection Apar
                </Text>
              </Button>
            )}
            <View style={{ width: 270, position: "absolute" }}>
              <Dialog
                visible={this.state.visibleDialogSubmit}
                dialogTitle={
                  <DialogTitle title="Delete Inspection Report Apar .." />
                }
              >
                <DialogContent>
                  {
                    <ActivityIndicator
                      size="large"
                      color="#330066"
                      animating
                    />
                  }
                </DialogContent>
              </Dialog>
            </View>
          </Content>
        </ScrollView>
      </Container>
    );
  }
}

export default InspectionReportAparDetail;

import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Item
} from "native-base";

import moment from 'moment'
import Dialog, { DialogTitle, SlideAnimation, DialogContent, DialogButton } from 'react-native-popup-dialog';
import Ripple from "react-native-material-ripple";
import ImagePicker from "react-native-image-picker";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from 'react-native-datepicker';

class ListItemFuncloc extends React.PureComponent {
  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.props.setSelectedLocLabel(this.props.data)
          }
          rippleColor={colors.accent}
        >
          <CardItem
            style={{
              borderRadius: 0,
              marginTop: 4,
              backgroundColor: colors.gray
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 4,
                backgroundColor: colors.gray
              }}
            >
              <View >
                <Text style={{fontSize:12}}>{this.props.data.IDEQUPMENT} - {this.props.data.EQUPMENTCODE}</Text>
              </View>
            </View>
          </CardItem>
        </Ripple>
      </View>
    );
  }
}

export default class DailyReportDetailListUpdate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            dataHeader: [],
            listLocation: [],
            isLoading: true,
            lokasi_temuan:'',
            id_lokasi_temuan:'',
            visibleLoadingLocation: false,
            visibleSearchListLoc: false,
            selectedLocLabel: '',
            searchWordLocation: '',
            visibleDialogSubmit:false
        };
    }

    static navigationOptions = {
        header: null
    };

    reset = () => {
        this.setState({
            pickedImage: null
        });
    }

    pickImageHandler = () => {
        ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
            if (res.didCancel) {
                console.log("User cancelled!");
            } else if (res.error) {
                console.log("Error", res.error);
            } else {
                this.setState({
                    pickedImage: res.uri,
                    uri: res.uri,
                    fileName: res.fileName,
                    fileType: res.type
                });
                console.log(res.type)
            }
        });
    }

    resetHandler = () => {
        this.reset();
    }

    componentDidMount() {
        AsyncStorage.getItem('list').then((dataHeader) => {
            // console.log(JSON.parse(dataHeader.CREATE_AT))
            var temuanDate = moment(JSON.parse(dataHeader).CREATE_AT, 'DD-MMM-YYYY');
            var temuanDateString = temuanDate.format('YYYY-MM-DD')
            this.setState({
                listHeader: JSON.parse(dataHeader),
                ID: JSON.parse(dataHeader).ID,
                temuanDate: temuanDateString,
                selectedUnsafeType: JSON.parse(dataHeader).JENIS_TEMUAN,
                selectedPriority: JSON.parse(dataHeader).PRIORITY,
                deskripsi_temuan: JSON.parse(dataHeader).TEMUAN,
                lokasi_temuan: JSON.parse(dataHeader).LOKASI_TEXT,
                id_lokasi_temuan: JSON.parse(dataHeader).LOKASI_KODE,
                penyebab_temuan: JSON.parse(dataHeader).PENYEBAB,
                rekomendasi_temuan: JSON.parse(dataHeader).REKOMENDASI,
                tindak_lanjut_temuan: JSON.parse(dataHeader).TINDAK_LANJUT,
                potensi_bahaya_temuan: JSON.parse(dataHeader).POTENSI_BAHAYA,

                pickedImage: GlobalConfig.SERVERHOST + 'api' + JSON.parse(dataHeader).FOTO_BEF,
            });
        })

        AsyncStorage.getItem('idAndShift').then((idAndShift) => {
            var temp = idAndShift.split('+');
            console.log(temp[0])
            console.log(temp[1])
            var shift = temp[1]
            this.setState({
                selectedShift: shift
            })
        })
        this.onChangeLocation();
    }

    updateUnsafe() {

        if (this.state.fileType == null) {
            this.setState({
                visibleDialogSubmit:true
            })
            AsyncStorage.getItem("token").then(value => {
                const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe/update_detail';
                var formData = new FormData();
                formData.append("token", value);
                formData.append("ID", this.state.ID);
                formData.append("TANGGAL", this.state.temuanDate);
                formData.append("SHIFT", this.state.selectedShift);
                formData.append("JENIS_TEMUAN", this.state.selectedUnsafeType);
                formData.append("PRIORITY", this.state.selectedPriority);
                formData.append("TEMUAN", this.state.deskripsi_temuan);
                formData.append("LOKASI_TEXT", this.state.lokasi_temuan);
                formData.append("LOKASI_KODE", this.state.lokasi_kode);
                formData.append("PENYEBAB", this.state.penyebab_temuan);
                formData.append("REKOMENDASI", this.state.rekomendasi_temuan);
                formData.append("TINDAK_LANJUT", this.state.tindak_lanjut_temuan);
                formData.append("POTENSI_BAHAYA", this.state.potensi_bahaya_temuan);
                // formData.append("FOTO_BEF", {
                //     uri: this.state.pickedImage,
                //     name: 'image',
                //     type: this.state.fileType,
                // });
                console.log(formData)

                // alert(JSON.stringify(url));

                fetch(url, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },
                    method: 'POST',
                    body: formData
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        // console.log(responseJson);
                        if (responseJson.status == 200) {
                            this.setState({
                                visibleDialogSubmit:false
                            })
                            Alert.alert('Success', 'Update Temuan Unsafe Success', [{
                                text: 'Okay'
                            }])
                            this.props.navigation.navigate('DailyReportDetailList')
                        } else {
                            this.setState({
                                visibleDialogSubmit:false
                            })
                            Alert.alert('Error', 'Update Temuan Unsafe Failed', [{
                                text: 'Okay'
                            }])
                        }
                    })
                    .catch(error => {
                        this.setState({
                            visibleDialogSubmit:false
                        })
                        Alert.alert('Error', 'Update Temuan Unsafe Failed', [{
                            text: 'Okay'
                        }])
                        console.log(error)
                    })
            })
        }
        else {
            this.setState({
                visibleDialogSubmit:true
            })
            // alert(JSON.stringify(this.state.id));
            AsyncStorage.getItem("token").then(value => {
                const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe/update_detail';
                var formData = new FormData();
                formData.append("token", value);
                formData.append("ID", this.state.ID);
                formData.append("TANGGAL", this.state.temuanDate);
                formData.append("SHIFT", this.state.selectedShift);
                formData.append("JENIS_TEMUAN", this.state.selectedUnsafeType);
                formData.append("PRIORITY", this.state.selectedPriority);
                formData.append("TEMUAN", this.state.deskripsi_temuan);
                formData.append("LOKASI_TEXT", this.state.lokasi_temuan);
                formData.append("LOKASI_KODE", this.state.lokasi_kode);
                formData.append("PENYEBAB", this.state.penyebab_temuan);
                formData.append("REKOMENDASI", this.state.rekomendasi_temuan);
                formData.append("TINDAK_LANJUT", this.state.tindak_lanjut_temuan);
                formData.append("POTENSI_BAHAYA", this.state.potensi_bahaya_temuan);
                formData.append("FOTO_BEF", {
                    uri: this.state.pickedImage,
                    name: 'image',
                    type: this.state.fileType,
                });
                console.log(formData)

                // alert(JSON.stringify(url));

                fetch(url, {
                    headers: {
                        'Content-Type': 'multipart/form-data'
                    },
                    method: 'POST',
                    body: formData
                })
                    .then((response) => response.json())
                    .then((responseJson) => {
                        // console.log(responseJson);
                        if (responseJson.status == 200) {
                            this.setState({
                                visibleDialogSubmit:false
                            })
                            Alert.alert('Success', 'Update Temuan Unsafe Success', [{
                                text: 'Okay'
                            }])
                            this.props.navigation.navigate('DailyReport')
                        } else {
                            this.setState({
                                visibleDialogSubmit:false
                            })
                            Alert.alert('Error', 'Update Temuan Unsafe Failed', [{
                                text: 'Okay'
                            }])
                        }
                    })
                    .catch(error => {
                        this.setState({
                            visibleDialogSubmit:false
                        })
                        Alert.alert('Error', 'Update Temuan Unsafe Failed', [{
                            text: 'Okay'
                        }])
                        console.log(error)
                    })
            })
        }
    }

    onChangeLocation() {
        AsyncStorage.getItem('token').then((value) => {
            const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/funcloc';
            var formData = new FormData();
            formData.append("token", value)
            formData.append("search", this.state.searchWordLocation)
            formData.append("limit", 10)
            formData.append("plant", null)
            fetch(url, {
                headers: {
                    'Content-Type': 'multipart/form-data'
                },
                method: 'POST',
                body: formData
            })
                .then((response) => response.json())
                .then((responseJson) => {
                    this.setState({
                        listLocation: responseJson,
                        isloading: false
                    });
                })
                .catch((error) => {
                    // this.setState({
                    //     visibleLoadingLocation: false
                    // })
                    console.log(error)
                })
        })
    }

    onClickSearchLoc() {
        console.log('masuk')
        if (this.state.visibleSearchListLoc) {
            this.setState({
                visibleSearchListLoc: false
            })
        } else {
            this.setState({
                visibleSearchListLoc: true
            })
        }
    }

    setSelectedLocLabel(location) {
        this.setState({
            lokasi_temuan: location.EQUPMENTCODE,
            id_lokasi_temuan: location.IDEQUPMENT,
            visibleSearchListLoc: false,
        })
    }

    _renderItemFuncloc = ({ item }) => <ListItemFuncloc data={item} setSelectedLocLabel={text => this.setSelectedLocLabel(text)} />;

    render() {
        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left style={{flex:1}}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("DailyReportDetailListFoto")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body style={{flex:3,alignItems:'center'}}>
                        <Title style={styles.textbody}>Update Temuan Unsafe {this.state.ID}</Title>
                    </Body>

                    <Right style={{flex:1}}/>
                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                <View style={{ flex: 1 }}>
                    <Content style={{ marginTop: 0 }}>
                        <View style={{ backgroundColor: '#FEFEFE' }}>
                            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                                <View style={{ flex: 1}}>
                                    <View style={{paddingBottom:5}}>
                                    <Text style={styles.titleInput}>Upload Foto (jpg, png) max : 500 kb</Text>
                                    </View>
                                    <View>
                                    <Ripple
                                        style={{
                                        flex: 2,
                                        justifyContent: "center",
                                        alignItems: "center"
                                        }}
                                        rippleSize={176}
                                        rippleDuration={600}
                                        rippleContainerBorderRadius={15}
                                        onPress={this.pickImageHandler}
                                        rippleColor={colors.accent}
                                    >
                                    <View style={styles.placeholder}>
                                        <Image source={{uri:this.state.pickedImage}} style={styles.previewImage}/>
                                    </View>
                                    </Ripple>
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1}}>
                                    <View>
                                        <Text style={styles.titleInput}>Shift</Text>
                                        <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                                                style={{ width: '100%', height:35}}
                                                placeholder="Quantity"
                                                placeholderStyle={{ color: "#bfc6ea" }}
                                                placeholderIconColor="#007aff"
                                                selectedValue={this.state.selectedShift}
                                                onValueChange={(itemValue) => this.setState({ selectedShift: itemValue })}>
                                                <Picker.Item label="Shift 1" value="1" />
                                                <Picker.Item label="Shift 2" value="2" />
                                                <Picker.Item label="Shift 3" value="3" />
                                            </Picker>
                                        </Form>
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.titleInput}>Jenis Temuan</Text>
                                        <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                                                style={{ width: '100%', height:35}}
                                                placeholder="Quantity"
                                                placeholderStyle={{ color: "#bfc6ea" }}
                                                placeholderIconColor="#007aff"
                                                selectedValue={this.state.selectedUnsafeType}
                                                onValueChange={(itemValue) => this.setState({ selectedUnsafeType: itemValue })}>
                                                <Picker.Item label="Unsafe Action" value="UA" />
                                                <Picker.Item label="Unsafe Condition" value="UC" />
                                            </Picker>
                                        </Form>
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1}}>
                                    <View>
                                        <Text style={styles.titleInput}>Priority</Text>
                                        <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                                            <Picker
                                                mode="dropdown"
                                                iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                                                style={{ width: '100%', height:35}}
                                                placeholder="Quantity"
                                                placeholderStyle={{ color: "#bfc6ea" }}
                                                placeholderIconColor="#007aff"
                                                selectedValue={this.state.selectedPriority}
                                                onValueChange={(itemValue) => this.setState({ selectedPriority: itemValue })}>
                                                <Picker.Item label="HIGH" value="1" />
                                                <Picker.Item label="MEDIUM" value="2" />
                                                <Picker.Item label="LOW" value="3" />
                                            </Picker>
                                        </Form>
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.titleInput}>Temuan</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ borderRadius:5, marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.deskripsi_temuan} placeholder='Type Something ...' onChangeText={(text) => this.setState({ deskripsi_temuan: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 5, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.titleInput}>Location</Text>
                                    </View>
                                    <View style={{ flex: 1, flexDirection: 'column' }}>
                                        <Button
                                            block
                                            style={{ justifyContent: "flex-start", flex: 1, borderWidth: Platform.OS==='ios'?1:0,borderColor:Platform.OS==='ios'?colors.lightGray:null, paddingLeft: 10, marginRight: 5, marginLeft: 5, borderRadius: 5, backgroundColor: colors.gray, height: 35 }}
                                            onPress={() => this.onClickSearchLoc()}>
                                            <Text style={{ fontSize: 12 }}>{this.state.lokasi_temuan}</Text>
                                        </Button>
                                    </View>

                                    {this.state.visibleSearchListLoc &&
                                        <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.gray, margin: 5, borderColor: "#E6E6E6", }}>
                                            <View>

                                                <Textarea value={this.state.searchWordLocation} style={{ marginLeft: 5, marginRight: 5, fontSize: 11 }} bordered rowSpan={1.5} onChangeText={(text) => this.onChangeLocation(text)} placeholder='Ketik Funcloc' />

                                            </View>
                                            <FlatList
                                                data={this.state.listLocation}
                                                renderItem={this._renderItemFuncloc}
                                                keyExtractor={(item, index) => index.toString()}
                                            />
                                        </View>
                                    }
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.titleInput}>Penyebab</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ borderRadius:5, marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.penyebab_temuan} placeholder='Type Something ...' onChangeText={(text) => this.setState({ penyebab_temuan: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.titleInput}>Rekomendasi</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ borderRadius:5, marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.rekomendasi_temuan} placeholder='Type Something ...' onChangeText={(text) => this.setState({ rekomendasi_temuan: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.titleInput}>Tindak Lanjut</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ borderRadius:5, marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.tindak_lanjut_temuan} placeholder='Type Something ...' onChangeText={(text) => this.setState({ tindak_lanjut_temuan: text })} />
                                    </View>
                                </View>
                            </CardItem>
                            <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                                <View style={{ flex: 1 }}>
                                    <View>
                                        <Text style={styles.titleInput}>Potensi Bahaya</Text>
                                    </View>
                                    <View>
                                        <Textarea style={{ borderRadius:5, marginLeft: 5, marginRight: 5, marginBottom: 5, fontSize: 11 }} rowSpan={3} bordered value={this.state.potensi_bahaya_temuan} placeholder='Type Something ...' onChangeText={(text) => this.setState({ potensi_bahaya_temuan: text })} />
                                    </View>
                                </View>
                            </CardItem>

                            <View style={styles.Contentsave}>
                                <Button
                                    block
                                    style={{
                                        height: 45,
                                        marginLeft: 20,
                                        marginRight: 20,
                                        marginBottom: 20,
                                        borderWidth: 1,
                                        backgroundColor: "#00b300",
                                        borderColor: "#00b300",
                                        borderRadius: 4
                                    }}
                                    onPress={() => this.updateUnsafe()}
                                >
                                    <Text style={styles.buttonText}>Submit</Text>
                                </Button>
                            </View>
                        </View>
                    </Content>

                    <View style={{ width: 270, position: "absolute" }}>
                                <Dialog
                                    visible={this.state.visibleDialogSubmit}
                                    dialogTitle={<DialogTitle title='Updating Temuan Unsafe ..'/>}
                                >
                                    <DialogContent>
                                    {<ActivityIndicator size="large" color="#330066" animating />}
                                    </DialogContent>
                                </Dialog>
                                </View>
                </View>
                <CustomFooter navigation={this.props.navigation} menu='Safety' />
            </Container>

        );
    }
}

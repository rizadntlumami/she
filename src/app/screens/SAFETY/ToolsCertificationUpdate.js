import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TouchableOpacity,
    TouchableHighlight,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from 'react-native-datepicker';
import ImagePicker from "react-native-image-picker";
import Ripple from "react-native-material-ripple";
import Moment from 'moment'

class ListItemUnit extends React.PureComponent {
  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.props.setSelectedUnitKerjaLabel(this.props.data)
          }
          rippleColor={colors.accent}
        >
          <CardItem
            style={{
              borderRadius: 0,
              marginTop: 4,
              backgroundColor: colors.gray
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 4,
                backgroundColor: colors.gray
              }}
            >
              <View >
                <Text style={{fontSize:12}}>{this.props.data.muk_kode} - {this.props.data.muk_nama}</Text>
              </View>
            </View>
          </CardItem>
        </Ripple>
      </View>
    );
  }
}

export default class ToolsCertificationUpdate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      dataHeader: [],
      isLoading: true,
      detailTools:[],
      listUnitKerja:[],
      listPeralatan:[],
      peralatan: '',
      buku:'',
      dataTeknis:'',
      pengesahan: '',
      lokasi: '',
      ujiAwal: '',
      ujiUlang: '',
      time: '',
      unitKerja: '',

      pickedImageBuku: '',
      uriBuku: '',
      fileNameBuku: '',
      pickedImageAktual: '',
      uriAktual: '',
      fileNameAktual: '',

      visibleLoadingUnitKerja:false,
      visibleSearchListUnitKerja:false,
      selectedUnitKerjaLabel:'',
      searchWordUnitKerja:'',
      unitkerjaNama: '',

      visibleDialogSubmit:false
    };
  }


  pickImageHandlerBuku = () => {
      ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
          if (res.didCancel) {
              console.log("User cancelled!");
          } else if (res.error) {
              console.log("Error", res.error);
          } else {
              this.setState({
                pickedImageBuku: res.uri,
                uriBuku: res.uri,
                fileTypeBuku: res.type
            });
          }
      });
  }

  pickImageHandlerAktual = () => {
      ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
          if (res.didCancel) {
              console.log("User cancelled!");
          } else if (res.error) {
              console.log("Error", res.error);
          } else {
            this.setState({
              pickedImageAktual: res.uri,
              uriAktual: res.uri,
              fileTypeAktual: res.type
          });
          }
      });
  }


  static navigationOptions = {
      header: null
  };

  _renderItem = ({ item }) => (
      <ListItem data={item}></ListItem>
  )
  componentDidMount() {
    AsyncStorage.getItem('listAlat').then((detailTools) => {
      var momentObj = Moment(JSON.parse(detailTools).UJI_AWAL, 'DD-MMM-YYYY');
      var momentString = momentObj.format('YYYY-MM-DD')
      this.setState({
        detailTools: JSON.parse(detailTools),
        buku: JSON.parse(detailTools).NO_BUKU,
        lokasi: JSON.parse(detailTools).LOKASI,
        peralatan: JSON.parse(detailTools).EQUIP_NAME,
        dataTeknis: JSON.parse(detailTools).DATA_TEKNIS,
        pengesahan: JSON.parse(detailTools).NOMOR_PENGESAHAN,
        ujiAwal: momentString,
        time: JSON.parse(detailTools).TIME_CERTIFICATION,
        ujiUlang: JSON.parse(detailTools).UJI_ULANG,
        unitkerjaNama: JSON.parse(detailTools).UK_TEXT,
        pickedImageBuku: GlobalConfig.SERVERHOST + 'api' + JSON.parse(detailTools).FILE_BUKU,
        pickedImageAktual: GlobalConfig.SERVERHOST + 'api' + JSON.parse(detailTools).FILE_AKTUAL,
      });
    })
      //this.loadUnitKerja();
      this.loadPeralatan();
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        this.loadData();
      });
  }

  loadData() {

  }

  loadPeralatan(){
    AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/sertification/get_sio_group';
        var formData = new FormData();
        formData.append("token", value)
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listPeralatan: responseJson.data,
                    isloading: false
                });
            })
            .catch((error) => {
                console.log(error)
            })
    })
  }

  onChangeUnitKerja(text) {
    this.setState({
        searchWordUnitKerja: text
    })
    this.setState({
        visibleLoadingUnitKerja: true
    })
    AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/unit_kerja';
        var formData = new FormData();
        formData.append("token", value)
        formData.append("search", this.state.searchWordUnitKerja)
        formData.append("limit", 10)
        formData.append("plant", null)
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    visibleLoadingUnitKerja: false
                })
                this.setState({
                    listUnitKerja: responseJson,
                    listUnitKerjaMaster: responseJson,
                    isloading: false
                });
            })
            .catch((error) => {
                this.setState({
                    visibleLoadingUnitKerja: false
                })
                console.log(error)
            })
    })
    // this.setState({
    //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
    // })
}

  onClickSearchUnitKerja(){
    console.log('masuk')
    if (this.state.visibleSearchListUnitKerja){
      this.setState({
        visibleSearchListUnitKerja:false
      })
    }else{
      this.setState({
        visibleSearchListUnitKerja:true
      })
    }
  }

  setSelectedUnitKerjaLabel(unitkerja) {
    this.setState({
        unitkerjaNama: unitkerja.muk_nama,
        unitkerjaKode: unitkerja.muk_kode,
        visibleSearchListUnitKerja: false,
    })
  }

  _renderItemUnit = ({ item }) => <ListItemUnit data={item} setSelectedUnitKerjaLabel={text => this.setSelectedUnitKerjaLabel(text)} />;

  updateToolsSetRiksa(){
    if (this.state.buku == null) {
      alert('Masukkan No Buku');
    }
    else if (this.state.peralatan == '') {
      alert('Masukkan Peralatan');
    }
    else if (this.state.ujiAwal == null) {
      alert('Masukkan Tanggal Mulai Sertifikasi');
    }
    else if (this.state.time == null) {
      alert('Masukkan Waktu');
    }
    else if (this.state.dataTeknis == null) {
      alert('Masukkan Data Teknis');
    }
    else if (this.state.pengesahan == null) {
      alert('Masukkan No Pengesahan');
    }
    else if (this.state.unitkerjaNama == '') {
      alert('Masukkan Unit Kerja');
    }
    else if (this.state.lokasi == null) {
      alert('Masukkan Lokasi');
    }
    else {
      var ujiAwal = this.state.ujiAwal;
      var stringUjiAwal = ujiAwal.substring(0, 4);
      var stringUjiAkhir = ujiAwal.substring(4, 10);
      var date = parseInt(stringUjiAwal)
      var range = parseInt(this.state.time)
      let dateRiksa = date + range
      this.setState({
          ujiUlang:dateRiksa+stringUjiAkhir
      })
      this.updateTools()
    }
  }

  updateTools() {
    this.setState({
      visibleDialogSubmit:true
    })
    AsyncStorage.getItem("token").then(value => {
    var url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/sertification/update/alat';
    var formData = new FormData();
    formData.append("token", value);
    formData.append("ID", this.state.detailTools.ID);
    formData.append("NO_BUKU", this.state.buku);
    formData.append("KODE", null);
    formData.append("EQUIP_NAME", this.state.peralatan);
    formData.append("DATA_TEKNIS", this.state.dataTeknis);
    formData.append("NOMOR_PENGESAHAN", this.state.pengesahan);
    formData.append("LOKASI", this.state.lokasi);
    formData.append("UJI_AWAL", this.state.ujiAwal);
    formData.append("TIME_CERTIFICATION", this.state.time);
    formData.append("UJI_ULANG", this.state.ujiUlang);
    formData.append("UK_KODE", this.state.unitkerjaKode);
    formData.append("UK_TEXT", this.state.unitkerjaNama);
    if (this.state.uriBuku!='') {
    formData.append("FILE_BUKU", {
        uri: this.state.uriBuku,
        name: 'image',
        type: this.state.fileTypeBuku,
    });
    }
    if (this.state.uriAktual!='') {
    formData.append("FILE_AKTUAL", {
        uri: this.state.uriAktual,
        name: 'image',
        type: this.state.fileTypeAktual,
    });
    }

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
    .then(response => response.json())
    .then(response => {
      if (response.status == 200) {
        this.setState({
          visibleDialogSubmit:false
        })
        Alert.alert('Success', 'Update Tools Certification Success', [{
          text: 'Okay'
        }])
        this.props.navigation.navigate('ToolsCertification')
      } else {
        this.setState({
          visibleDialogSubmit:false
        })
        Alert.alert('Error', 'Update Tools Certification Failed', [{
          text: 'Okay'
        }])
      }
    })
    .catch((error)=>{
      this.setState({
        visibleDialogSubmit:false
      })
      Alert.alert('Error', 'Update Tools Certification Failed', [{
        text: 'Okay'
      }])
      console.log(error)
    })
  })
  }

  render() {

    let listPeralatan = this.state.listPeralatan.map( (s, i) => {
      return <Picker.Item key={i} value={s.NAME} label={s.NAME} />
    });

    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("ToolsCertificationDetail")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Update {this.state.buku}</Title>
          </Body>
          
            <Right style={{flex:1}}></Right>
          
          {/* <Right/> */}
        </Header>

      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />

      <View style={{ flex: 1 }}>
        <Content style={{ marginTop: 0 }}>
          <View style={{ backgroundColor: '#FEFEFE' }}>
          <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                    <View style={{paddingBottom:5}}>
                      <Text style={styles.titleInput}>Upload Foto Buku</Text>
                    </View>
                    <View>
                      <Ripple
                        style={{
                          flex: 2,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                        rippleSize={176}
                        rippleDuration={600}
                        rippleContainerBorderRadius={15}
                        onPress={this.pickImageHandlerBuku}
                        rippleColor={colors.accent}
                      >
                      <View style={styles.placeholder}>
                          <Image source={{uri :this.state.pickedImageBuku + '?' + new Date()}} style={styles.previewImage}/>
                      </View>
                      </Ripple>
                    </View>
                </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                    <View style={{paddingBottom:5}}>
                      <Text style={styles.titleInput}>Upload Foto Aktual</Text>
                    </View>
                    <View>
                      <Ripple
                        style={{
                          flex: 2,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                        rippleSize={176}
                        rippleDuration={600}
                        rippleContainerBorderRadius={15}
                        onPress={this.pickImageHandlerAktual}
                        rippleColor={colors.accent}
                      >
                      <View style={styles.placeholder}>
                          <Image source={{uri :this.state.pickedImageAktual+ '?' + new Date()}} style={styles.previewImage}/>
                      </View>
                      </Ripple>
                    </View>
                </View>
            </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>No. Buku *</Text>
                  </View>
                  <View>
                  <Textarea style={ {marginLeft:5, borderRadius: 5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.buku} placeholder='No Buku ...' onChangeText={(text) => this.setState({ buku: text })} />
                  </View>
                </View>
              </CardItem>

              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1}}>
                <View>
                  <Text style={styles.titleInput}>Peralatan *</Text>
                  <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                      <Picker
                          mode="dropdown"
                          iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                          style={{ width: '100%', height: 35}}
                          placeholder="Select Tools ..."
                          placeholderStyle={{ color: "#bfc6ea" }}
                          placeholderIconColor="#007aff"
                          selectedValue={this.state.peralatan}
                          onValueChange={(itemValue) => this.setState({peralatan:itemValue})}>
                          <Picker.Item label="Choose Peralatan..."  value=""/>
                            {listPeralatan}
                      </Picker>
                  </Form>
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
              <View style={{ flex: 1, flexDirection: 'row'}}>
                <View style={{width: '50%'}}>
                    <Text style={styles.titleInput}>Tanggal Mulai Sertifikasi *</Text>
                    <DatePicker
                    style={{ width: '100%', fontSize:10, borderRadius:20}}
                    date={this.state.ujiAwal}
                    mode="date"
                    placeholder="Choose Date ..."
                    format="YYYY-MM-DD"
                    minDate="2018-01-01"
                    maxDate="2050-12-31"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    iconSource='{this.timeIcon}'
                    customStyles={{
                      dateInput: {
                        marginLeft: 5, marginRight:5, height: 35, borderRadius:5, fontSize:10, borderWidth:1, borderColor:"#E6E6E6"
                      },
                      dateIcon: {
                        position: 'absolute',
                        left: 0,
                        top: 5,
                      },
                    }}
                    onDateChange={(date) => { this.setState({ ujiAwal: date }) }} />
                </View>
                <View style={{width: '50%'}}>
                  <Text style={{fontSize: 10, paddingBottom:2}}>Waktu *</Text>
                  <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                      <Picker
                          mode="dropdown"
                          iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                          style={{ width: '100%', height: 35 }}
                          placeholder="Choose Time ..."
                          placeholderStyle={{ color: "#bfc6ea" }}
                          placeholderIconColor="#007aff"
                          selectedValue={this.state.time}
                          onValueChange={(itemValue) => this.setState({ time: itemValue })}>
                          <Picker.Item label="1 Tahun" value="1" />
                          <Picker.Item label="2 tahun" value="2" />
                          <Picker.Item label="3 tahun" value="3" />
                          <Picker.Item label="4 tahun" value="4" />
                          <Picker.Item label="5 tahun" value="5" />
                      </Picker>
                  </Form>
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>Data Teknis *</Text>
                  </View>
                  <View>
                  <Textarea style={ {marginLeft:5,borderRadius: 5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.dataTeknis} placeholder='Data Teknis ...' onChangeText={(text) => this.setState({ dataTeknis: text })} />
                  </View>
                </View>
              </CardItem>

              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>Nomor Pengesahan *</Text>
                  </View>
                  <View>
                  <Textarea style={ {marginLeft:5, borderRadius: 5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.pengesahan} placeholder='No Pengesahan ...' onChangeText={(text) => this.setState({ pengesahan: text })} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.gray }}>
                <View style={{ flex: 1 }}>
                    <View>
                        <Text style={styles.titleInput}>Unit Kerja</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column' }}>
                        <Button
                            block
                            style={{ justifyContent: "flex-start", flex: 1, borderRadius: 5, borderWidth: 0, paddingLeft: 10, marginRight: 5, marginLeft: 5, backgroundColor: colors.gray, height: 35 }}
                            onPress={() => this.onClickSearchUnitKerja()}>
                            <Text style={{ fontSize: 12 }}>{this.state.unitkerjaNama}</Text>
                        </Button>
                    </View>
                    {this.state.visibleSearchListUnitKerja &&
                        <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.gray, margin: 5, borderColor: "#E6E6E6", }}>
                            <View>
                                <Textarea value={this.state.searchWordUnitKerja} style={{ marginLeft: 5, marginRight: 5, fontSize: 11 }} bordered rowSpan={1.5} onChangeText={(text) => this.onChangeUnitKerja(text)} placeholder='Ketik unit kerja' />
                            </View>
                            <FlatList
                                data={this.state.listUnitKerja}
                                renderItem={this._renderItemUnit}
                                keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                    }
                </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>Lokasi *</Text>
                  </View>
                  <View>
                  <Textarea style={ {marginLeft:5, borderRadius: 5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.lokasi} placeholder='Lokasi ...' onChangeText={(text) => this.setState({ lokasi: text })} />
                  </View>
                </View>
            </CardItem>
            
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
              <View style={{ flex: 1}}>
              <View style={styles.Contentsave}>
                  <Button
                    block
                    style={{
                      width:'100%',
                      height: 45,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: "#00b300",
                      borderColor: "#00b300",
                      borderRadius: 4
                    }}
                    onPress={() => this.updateToolsSetRiksa()}
                  >
                    <Text style={{color:colors.white}}>UPDATE</Text>
                  </Button>
              </View>
              </View>
            </CardItem>
          </View>
          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
              dialogTitle={<DialogTitle title="Updating Tools Certification .." />}
            >
              <DialogContent>
                {<ActivityIndicator size="large" color="#330066" animating />}
              </DialogContent>
            </Dialog>
          </View>
        </Content>
      </View>
      <CustomFooter navigation={this.props.navigation} menu='Safety' />
    </Container>

    );
  }
}

import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback ,Platform, AsyncStorage } from "react-native";
import Ripple from "react-native-material-ripple";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    height: 100,
    width: 150,
    marginLeft: 3,
    marginRight:3,
    paddingTop:3,
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class NewsContentFireReport extends Component {

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={[styles.container]}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 5,
            elevation: 1,
            borderWidth:Platform.OS==='ios'?1:0,
            borderColor:(Platform.OS==='ios')?colors.lightGray:null
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          rippleColor={colors.accent}
        >
          <Image
            source={this.props.imageUri}
            style={{ marginTop:10, width: 50, height: 50, resizeMode: "contain" }}
          />
          
          <View style={{ flex: 1, paddingTop: 3, paddingLeft:10,paddingRight:10}}>
            <Text numberOfLines={1} style={{fontSize: 9, fontWeight:'bold'}}>{this.props.fireNumber}</Text>
          </View>

          <View style={{ flex: 1, flexDirection: "row", alignItems: 'center', marginLeft: 8}}>
                <View style={{width: '70%'}}>
                <Text numberOfLines={1} style={{fontSize: 9}}>{this.props.fireDate} {this.props.fireTime}</Text>
                </View>
                <View style={{width: '30%'}}>
                {this.props.status=='OPEN' ? (
                    <Text numberOfLines={1} style={{fontSize: 9, fontWeight:'bold', color: colors.green01}}>OPEN</Text>
                    ):(
                    <Text numberOfLines={1} style={{fontSize: 9, fontWeight:'bold', color: '#FF0101'}}>CLOSE</Text>)}
                </View>
            </View>
        </Ripple>
      </View>
    );
  }
}

NewsContentFireReport.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};

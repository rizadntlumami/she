import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input,
    Thumbnail
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewSIO from "../../components/ListViewSIO";
import Ripple from "react-native-material-ripple";

class LottoDrawIN extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            dataSource: [],
            isLoading: true,
            dataLotto:[],
            idLotto:'',
        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {
        AsyncStorage.getItem('idLotto').then((idLotto) => {
            this.setState({
              idLotto: idLotto,
              isLoading: false,
            });
            this.loadLotto();
        })
        this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
            this.loadLotto();
        });
    }

    loadLotto(){
          const url = GlobalConfig.SERVERHOST + 'getDetailLotto';
          var formData = new FormData();
          formData.append("id_lotto", this.state.idLotto)

          fetch(url, {
              headers: {
                  'Content-Type': 'multipart/form-data'
              },
              method: 'POST',
              body: formData
          })
              .then((response) => response.json())
              .then((responseJson) => {
                  this.setState({
                      dataLotto:responseJson.data,
                      isLoading: false
                  });
              })
              .catch((error) => {
                  console.log(error)
              })
    }

    navigateToScreen(route, dataDrawin, idUpdate) {
        AsyncStorage.setItem('dataDrawin', JSON.stringify(dataDrawin)).then(() => {
            AsyncStorage.setItem('idUpdate', (idUpdate)).then(() => {
                this.props.navigation.navigate(route);
            })
        })
    }

    render() {
      var list;
      if (this.state.isLoading) {
          list = (
              <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
                  <ActivityIndicator size="large" color="#330066" animating />
              </View>
          );
      } else {
        list = (
          <ScrollView>
              <View>
              <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, height:50}}>
                  <Ripple
                    style={{
                      flex: 2,
                      justifyContent: "center",
                      alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() => this.navigateToScreen('LottoInputDrawIN', this.state.dataLotto, 'k3DrawIN')}
                    rippleColor={colors.accent}
                  >
                  <View style={{ flex: 1, flexDirection: "row" }}>
                      <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:220 }}>
                          {this.state.dataLotto.k3_drawin_name == 'null' || this.state.dataLotto.k3_drawin_name == null ? (
                          <Text style={{fontSize: 10, fontWeight: "bold"}}>Data Belum Ada Record</Text>):(
                          <View>
                          <Text style={{fontSize: 10, fontWeight: "bold"}}>{this.state.dataLotto.k3_drawin_badge} - {this.state.dataLotto.k3_drawin_name}</Text>
                          {this.state.dataLotto.k3_drawin_ket=='V' ?(
                            <Text style={{fontSize: 10}}>Datang & Mengamankan</Text>
                          ):this.state.dataLotto.k3_drawin_ket=='Y' ?(
                            <Text style={{fontSize: 10}}>Datang & Tidak Mengamankan</Text>):(
                              <Text style={{fontSize: 10}}>Tidak Datang & Tidak Mengamankan</Text>
                            )
                          }
                          </View>)}
                      </View>
                      <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:80 }}>
                          <Text style={{fontSize: 10, fontWeight: "bold", textAlign:'right', color:colors.green01}}>K3</Text>
                      </View>
                  </View>
                  </Ripple>
              </Card>

              <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, height:50}}>
                <Ripple
                  style={{
                    flex: 2,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  rippleSize={176}
                  rippleDuration={600}
                  rippleContainerBorderRadius={15}
                  onPress={() => this.navigateToScreen('LottoInputDrawIN', this.state.dataLotto, 'pmlListrikDrawIN')}
                  rippleColor={colors.accent}
                >
                <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:220 }}>
                        {this.state.dataLotto.pmll_drawin_name == 'null' || this.state.dataLotto.pmll_drawin_name == null ? (
                        <Text style={{fontSize: 10, fontWeight: "bold"}}>Data Belum Ada Record</Text>):(
                        <View>
                        <Text style={{fontSize: 10, fontWeight: "bold"}}>{this.state.dataLotto.pmll_drawin_badge} - {this.state.dataLotto.pmll_drawin_name}</Text>
                        {this.state.dataLotto.pmll_drawin_ket=='V' ?(
                          <Text style={{fontSize: 10}}>Datang & Mengamankan</Text>
                        ):this.state.dataLotto.pmll_drawin_ket=='Y' ?(
                          <Text style={{fontSize: 10}}>Datang & Tidak Mengamankan</Text>):(
                            <Text style={{fontSize: 10}}>Tidak Datang & Tidak Mengamankan</Text>
                          )
                        }
                        </View>)}
                    </View>
                    <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:80 }}>
                        <Text style={{fontSize: 10, fontWeight: "bold", textAlign:'right', color:colors.green01}}>PML LISTRIK</Text>
                    </View>
                </View>
                </Ripple>
                </Card>

                <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, height:50}}>
                <Ripple
                  style={{
                    flex: 2,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  rippleSize={176}
                  rippleDuration={600}
                  rippleContainerBorderRadius={15}
                  onPress={() => this.navigateToScreen('LottoInputDrawIN', this.state.dataLotto, 'operatorDrawIN')}
                  rippleColor={colors.accent}
                >
                <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:220 }}>
                        {this.state.dataLotto.operator_drawin_name == 'null' || this.state.dataLotto.operator_drawin_name == null ? (
                        <Text style={{fontSize: 10, fontWeight: "bold"}}>Data Belum Ada Record</Text>):(
                        <View>
                        <Text style={{fontSize: 10, fontWeight: "bold"}}>{this.state.dataLotto.operator_drawin_badge} - {this.state.dataLotto.operator_drawin_name}</Text>
                        {this.state.dataLotto.operator_drawin_ket=='V' ?(
                          <Text style={{fontSize: 10}}>Datang & Mengamankan</Text>
                        ):this.state.dataLotto.operator_drawin_ket=='Y' ?(
                          <Text style={{fontSize: 10}}>Datang & Tidak Mengamankan</Text>):(
                            <Text style={{fontSize: 10}}>Tidak Datang & Tidak Mengamankan</Text>
                          )
                        }
                        </View>)}
                    </View>
                    <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:80 }}>
                        <Text style={{fontSize: 10, fontWeight: "bold", textAlign:'right', color:colors.green01}}>OPERATOR</Text>
                    </View>
                </View>
                </Ripple>
                </Card>

                <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10, height:50}}>
                <Ripple
                  style={{
                    flex: 2,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                  rippleSize={176}
                  rippleDuration={600}
                  rippleContainerBorderRadius={15}
                  onPress={() => this.navigateToScreen('LottoInputDrawIN', this.state.dataLotto, 'pmlTerkaitDrawIN')}
                  rippleColor={colors.accent}
                >
                <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:220 }}>
                        {this.state.dataLotto.pmlt_drawin_name == 'null' || this.state.dataLotto.pmlt_drawin_name == null ? (
                        <Text style={{fontSize: 10, fontWeight: "bold"}}>Data Belum Ada Record</Text>):(
                        <View>
                        <Text style={{fontSize: 10, fontWeight: "bold"}}>{this.state.dataLotto.pmlt_drawin_badge} - {this.state.dataLotto.pmlt_drawin_name}</Text>
                        {this.state.dataLotto.pmlt_drawin_ket=='V' ?(
                          <Text style={{fontSize: 10}}>Datang & Mengamankan</Text>
                        ):this.state.dataLotto.pmlt_drawin_ket=='Y' ?(
                          <Text style={{fontSize: 10}}>Datang & Tidak Mengamankan</Text>):(
                            <Text style={{fontSize: 10}}>Tidak Datang & Tidak Mengamankan</Text>
                          )
                        }
                        </View>)}
                    </View>
                    <View style={{ marginTop: 10, marginLeft: 10, marginRight: 5, width:80 }}>
                        <Text style={{fontSize: 10, fontWeight: "bold", textAlign:'right', color:colors.green01}}>PML TERKAIT</Text>
                    </View>
                </View>
                </Ripple>
                </Card>
              </View>
          </ScrollView>
          );
        }
        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left style={{flex:1}}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("LottoDetail")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body style={{flex:3,alignItems:'center'}}>
                        <Title style={styles.textbody}>DRAW IN </Title>
                    </Body>
                    <Right style={{flex:1}}></Right>
                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
                  <Footer style={styles.tabHeight}>
                    <FooterTab style={styles.tabfooter}>
                    <Button active style={styles.tabfooter}>
                        <View style={{height:'40%'}}></View>
                        <View style={{height:'50%'}}>
                            <Text style={{color:'white', fontWeight:'bold'}}>Draw IN</Text>
                        </View>
                        <View style={{height:'20%'}}></View>
                        <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.white}}></View>
                    </Button>
                    <Button
                        onPress={() =>
                        this.props.navigation.navigate("LottoDrawOUT")
                        }>
                        <Text style={{color:'white', fontWeight:'bold'}}>Draw OUT</Text>
                    </Button>
                    </FooterTab>
                </Footer>

                <Content style={{ marginTop: 10 }}>
                  {list}
                </Content>
            </Container>

        );
    }
}

export default LottoDrawIN;

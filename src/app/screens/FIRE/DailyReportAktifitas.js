import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail,
  Textarea,
  Picker,
  Form
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewReportAktifitas from "../../components/FireSystem/ListViewReportAktifitas";
import Ripple from "react-native-material-ripple";
import DatePicker from "react-native-datepicker";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, id) {
    AsyncStorage.setItem("id", JSON.stringify(id)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{marginLeft: 10, marginRight: 10, borderRadius: 5, backgroundColor:colors.white, marginBottom:10,
        shadowColor:'#000',
        shadowOffset:{
          width:0,
          height:2,
        },
        shadowOpacity:0.25,
        shadowRadius:3.84,
        elevation:2,
      }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.navigateToScreen("DailyReportAktifitasDetail", this.props.data.id)
          }
          rippleColor={colors.accent}
        >
          <View>
            <ListViewReportAktifitas
              imageUri={this.props.data.image}
              aktifitas={this.props.data.daily_activity}
              date={this.props.data.date_activity}
              aksi={this.props.data.type_action}
              status={this.props.data.status}
              timeStart={this.props.data.timesheet_start}
              timeEnd={this.props.data.timesheet_end}
            />
          </View>
        </Ripple>
      </View>
    );
  }
}

class DailyReportAktifitas extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      visibleDialog: false,
      filterDate: "",
      filterType: "",
      filterStatus: "",
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  onRefresh() {
    console.log("refreshing");
    this.setState({ isLoading: true }, function() {
      this.loadData();
    });
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
        this.setState({ isLoading: true });
      }
    );
  }

  loadData() {
    this.setState({ visibleDialog: false, isLoading: true });
      const url = GlobalConfig.SERVERHOST + "getDailySectionActivity";
      var formData = new FormData();
      formData.append("token", 'token');
      if (this.state.filterDate != "") {
        formData.append("date_activity", this.state.filterDate);
      }
      // if (this.state.filterStartDate != "") {
      //   formData.append("start_date", this.state.filterStartDate);
      // }
      // if (this.state.filterEndDate != "") {
      //   formData.append("end_date", this.state.filterEndDate);
      // }
      if (this.state.filterType != "") {
        formData.append("type_action", this.state.filterType);
      }
      if (this.state.filterStatus != "") {
        formData.append("status", this.state.filterStatus);
      }
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson))
          this.setState({
            dataSource: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
  }

  onClickSearch() {
    this.setState({
      visibleDialog: true
    });
  }

  render() {
    that = this;
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: 'center', alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.dataSource == '') {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
            <FlatList
              data={this.state.dataSource}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isLoading}
                  onRefresh={this.onRefresh.bind(this)}
                />
              }
            />

        );
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("FireMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>Daily Report Section</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button transparent onPress={() => this.onClickSearch()}>
              <Icon
                name="ios-funnel"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Right>
        </Header>
        <Footer>
          <FooterTab style={styles.tabfooter}>
            <Button active style={styles.tabfooter}>
              <View style={{ height: "40%" }} />
              <View style={{ height: "50%" }}>
                <Text style={styles.textbody}>Aktifitas</Text>
              </View>
              <View style={{ height: "20%" }} />
              <View
                style={{
                  borderWidth: 2,
                  marginTop: 2,
                  height: 0.5,
                  width: "100%",
                  borderColor: colors.white
                }}
              />
            </Button>
            <Button
              onPress={() => this.props.navigation.navigate("DailyReportCO2")}
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>CO2</Text>
            </Button>
          </FooterTab>
        </Footer>

        <View style={{ marginTop: 10, flex:1, flexDirection:'column' }}>{list}</View>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: colors.green01, marginBottom: 30 }}
          position="bottomRight"
          onPress={() =>
            this.props.navigation.navigate("DailyReportAktifitasCreate")
          }
        >
          <Icon
            name="ios-add"
            style={{
              fontSize: 50,
              fontWeight: "bold",
              paddingTop: Platform.OS === "ios" ? 25 : null
            }}
          />
        </Fab>

        <View style={{ width: 270, position: "absolute",}}>
          <Dialog
            visible={this.state.visibleDialog}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ visibleDialog: false });
            }}
          >
            <DialogContent>
              {
                <View>
                  <View style={{alignItems:'center', justifyContent:'center', paddingTop:20, width:'100%'}}>
                    <Text style={{fontSize:17, alignItems:'center', color:colors.black}}>Select sorting here</Text>
                  </View>
                  <View style={{flex:1, flexDirection:'row'}}>
                    <View style={{width:130}}>
                      <Text style={{ paddingTop: 10, fontSize: 10, marginBottom:5 }}>
                        Status
                      </Text>
                      <Form
                        style={{
                          borderWidth: 1,
                          borderRadius: 5,
                          marginRight: 5,
                          marginLeft: 5,
                          borderColor: "#E6E6E6",
                          height: 35
                        }}
                      >
                        <Picker
                          mode="dropdown"
                          iosIcon={
                            <Icon
                              name={
                                Platform.OS
                                  ? "ios-arrow-down"
                                  : "ios-arrow-down-outline"
                              }
                            />
                          }
                          style={{ width: 130, height: 35 }}
                          placeholder="Select Shift ..."
                          placeholderStyle={{ color: "#bfc6ea" }}
                          placeholderIconColor="#007aff"
                          selectedValue={this.state.filterStatus}
                          onValueChange={itemValue =>
                            this.setState({ filterStatus: itemValue })
                          }
                        >
                          <Picker.Item label="All Status" value="" />
                          <Picker.Item label="Open" value="OPEN" />
                          <Picker.Item label="Close" value="CLOSE" />
                        </Picker>
                      </Form>
                    </View>
                    <View style={{width:130}}>
                      <Text style={{ paddingTop: 10, fontSize: 10, marginBottom:5 }}>
                        Type
                      </Text>
                      <Form
                        style={{
                          borderWidth: 1,
                          borderRadius: 5,
                          marginRight: 5,
                          marginLeft: 5,
                          borderColor: "#E6E6E6",
                          height: 35
                        }}
                      >
                        <Picker
                          mode="dropdown"
                          iosIcon={
                            <Icon
                              name={
                                Platform.OS
                                  ? "ios-arrow-down"
                                  : "ios-arrow-down-outline"
                              }
                            />
                          }
                          style={{ width: 130, height: 35 }}
                          placeholder="Select Shift ..."
                          placeholderStyle={{ color: "#bfc6ea" }}
                          placeholderIconColor="#007aff"
                          selectedValue={this.state.filterType}
                          onValueChange={itemValue =>
                            this.setState({ filterType: itemValue })
                          }
                        >
                          <Picker.Item label="All Type" value="" />
                          <Picker.Item label="Siaga" value="SIAGA" />
                          <Picker.Item label="Inspeksi" value="INSPEKSI" />
                          <Picker.Item label="Pemadaman" value="PEMADAMAN" />
                        </Picker>
                      </Form>
                    </View>
                  </View>

                  <Text style={{ paddingTop: 10, fontSize: 10, marginBottom:5 }}>Tanggal Pemeriksaan *</Text>
                  <DatePicker
                    style={{
                      width: "100%",
                      fontSize: 10,
                      borderRadius: 20
                    }}
                    date={this.state.filterDate}
                    mode="date"
                    placeholder="Choose Date ..."
                    format="YYYY-MM-DD"
                    minDate="2018-01-01"
                    maxDate="2050-12-31"
                    confirmBtnText="Confirm"
                    cancelBtnText="Cancel"
                    // iconSource='{this.timeIcon}'
                    customStyles={{
                      dateInput: {
                        marginLeft: 5,
                        marginRight: 5,
                        height: 35,
                        borderRadius: 5,
                        fontSize: 10,
                        borderWidth: 1,
                        borderColor: "#E6E6E6"
                      },
                      dateIcon: {
                        position: "absolute",
                        left: 0,
                        top: 5
                      }
                    }}
                    onDateChange={date => {
                      this.setState({ filterDate: date });
                    }}
                  />
                  <View style={{paddingTop:10, width:'100%', alignItems:'center'}}>
                    <View style={{width:'40%', paddingRight:5}}>
                        <Button
                          block
                          style={{
                            width:'100%',
                            marginTop:10,
                            height: 35,
                            marginBottom: 5,
                            borderWidth: 0,
                            backgroundColor: colors.primer,
                            borderRadius: 15
                          }}
                          onPress={() => this.loadData()}
                        >
                          <Text style={{color:colors.white}}>Sort</Text>
                        </Button>
                    </View>
                  </View>
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>
      </Container>
    );
  }
}

export default DailyReportAktifitas;

import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from '../../styles/colors';

export default class ListViewDetail extends Component {
  render() {
    return (
      <View style={{ flexDirection: "column", flex: 1 }}>
        <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{flex: 1}}>
                <Text numberOfLines={1} style={{fontSize:10, fontWeight:"bold"}}>Unit Kerja : {this.props.unit}</Text>
                <Text numberOfLines={1} style={{fontSize:9, paddingTop:5}}>Uraian Aktifitas : {this.props.aktifitas}</Text>
                <Text style={{fontSize:9, paddingTop:2}}>Unsafe Type : {this.props.type}</Text>
                <Text numberOfLines={1} style={{fontSize:9, paddingTop:2}}>Deskripsi : {this.props.deskripsi}</Text>
            </View>
            <View style={{flex: 1}}> 
            {this.props.status.toUpperCase() == 'OPEN' && (
              <Text
                note
                style={{
                  alignSelf: "flex-end",
                  fontSize: 10,
                  fontWeight: 'bold', 
                  color: "white",
                  backgroundColor: colors.green01,
                  paddingRight: 4,
                  paddingLeft: 4,
                  paddingBottom: 2,
                  paddingTop: 2,
                  borderRadius: 6
                }}
              >OPEN
              </Text>
            )}
            {this.props.status.toUpperCase() == 'CLOSE' && (
                <Text
                  note
                  style={{
                    alignSelf: "flex-end",
                    fontSize: 10,
                    fontWeight: 'bold',
                    color: "white",
                    backgroundColor: colors.red,
                    paddingRight: 4,
                    paddingLeft: 4,
                    paddingBottom: 2,
                    paddingTop: 2,
                    borderRadius: 6
                  }}
                >CLOSE
                </Text>
              )}
              {((this.props.status.toUpperCase() != 'CLOSE')&&(this.props.status.toUpperCase() != 'OPEN')) && (
                <Text
                  note
                  style={{
                    alignSelf: "flex-end",
                    fontSize: 10,
                    fontWeight: 'bold',
                    color: "white",
                    backgroundColor: colors.gray,
                    paddingRight: 4,
                    paddingLeft: 4,
                    paddingBottom: 2,
                    paddingTop: 2,
                    borderRadius: 6
                  }}
                >{this.props.status}
                </Text>
              )}
                <Text style={{fontSize:9, paddingTop:2, textAlign:'right'}}>Kode : {this.props.kode}-00{this.props.idKategori}</Text>
                <Text numberOfLines={1} style={{fontSize:9, paddingTop:2, textAlign:'right'}}>Potensi Bahaya : {this.props.potensi}</Text>
            </View>
        </View>
      </View>
    );
  }
}

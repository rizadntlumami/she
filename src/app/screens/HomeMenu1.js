import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Textarea
} from "native-base";
import LinearGradient from "react-native-linear-gradient";

import MenuItems from "../components/MenuItems";
import SubNews from "../components/SubNews";

import styles from "./styles/HomeMenu";
import colors from "../../styles/colors";
import GlobalConfig from "../components/GlobalConfig";
import CustomFooter from "../components/CustomFooter";

var that;
class ListNews extends React.PureComponent {
  navigateToScreen(route, idArtikel) {
    AsyncStorage.setItem("idArtikel", JSON.stringify(idArtikel)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View style={{width:300, paddingRight:5}}>
      <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("Artikel", this.props.data.id)}>
          <SubNews
            imageUri={{ uri: GlobalConfig.IMAGEHOST + "News/" + this.props.data.image }}
            head={this.props.data.head}
            content={this.props.data.content}
          />
        </TouchableOpacity>
      </View>
    );
  }
}

export default class HomeMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      isLoading: true,
      token:'',
      name:'',
      listNews:[],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this.setState({
      isLoading:false,
    })
    AsyncStorage.getItem("token").then(token => {
      const url = GlobalConfig.SERVERHOST + 'validasi';
      var formData = new FormData();
      formData.append("token", token)
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            name: responseJson.user.name,
            isloading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
    this.loadNews();
  }

  loadNews(){
    const url = GlobalConfig.SERVERHOST + 'getNews';
    var formData = new FormData();
    formData.append("news", 'news')
    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then(response => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  listNews: responseJson.data,
                  isLoading: false
              });
          }
      })
      .catch(error => {
        console.log(error);
      });
  }


  logOut(){
    Alert.alert(
      'Confirmation',
      'Logout SHE Mobile?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => this.methLogOut() },
      ],
      { cancelable: false }
    );
  }

  methLogOut(){
    this.props.navigation.navigate("LoggedOut")
  }

  Konfirmasi(){
    alert("Null")
  }

  _renderNews = ({ item }) => <ListNews data={item} />;

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <View style={styles.homeWrapper}>
        <View style={{ flex: 1, flexDirection: "column", backgroundColor: "#fff" }}>
          <View>
            <ScrollView>
              <StatusBar
                backgroundColor={colors.green03}
                barStyle="light-content"
              />
              <ImageBackground
                style={{
                  alignSelf: "stretch",
                  width: Dimensions.get("window").width,
                  height: (Dimensions.get("window").height) / 4,
                }}
                source={require("../../assets/images/headprof-img.jpg")}>
                <TouchableOpacity
                  transparent
                  style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:20}}
                  onPress={()=>this.logOut()}
                >
                  <Icon
                    name='ios-contact'
                    size={20}
                    style={{color:colors.graydark}}
                  />
                </TouchableOpacity>
              </ImageBackground>
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 5, marginTop:-50, paddingLeft:0, paddingRight:0}}>
                <View
                  style={{
                    borderBottomColor: colors.lightBlack,
                    borderBottomWidth: 1,
                    marginTop: 15,
                    marginLeft: 20,
                    marginRight: 20
                  }}
                >
                  <Text style={styles.weatherTextTitle}>
                    Welcome, {this.state.name}
                  </Text>
                  <Text style={styles.weatherTextContent}>Have a nice day !</Text>
                </View>
                <View style={{flex:1, flexDirection:'row'}}>
                <View style={{width:'33%', paddingTop:15}}>
                    <MenuItems
                      handleOnPress={() =>
                        this.Konfirmasi()
                      }
                      imageUri={require("../../assets/images/iconAPD.png")}
                      name="A.P.D"
                    />
                </View>
                <View style={{width:'33%', paddingTop:15}}>
                    <MenuItems
                      handleOnPress={() =>
                        this.Konfirmasi()
                      }
                      imageUri={require("../../assets/images/iconSafety.png")}
                      name="Safety"
                    />
                </View>
                <View style={{width:'33%', paddingTop:15}}>
                    <MenuItems
                      handleOnPress={() =>
                        this.props.navigation.navigate("FireMenu")
                      }
                      imageUri={require("../../assets/images/iconFireSystem.png")}
                      name="Fire System"
                    />
                </View>
                </View>
              </Card>

              <View style={{flex:1, flexDirection:'row', paddingTop:10}}>
                <View style={{width:'50%', paddingLeft:25}}>
                  <Text style={styles.weatherTextTitle}>Hot News</Text>
                </View>
                <View style={{width:'50%', paddingRight:25,}}>
                  <TouchableOpacity
                    transparent
                    onPress={() => this.props.navigation.navigate("AllArtikel")}
                  >
                    <Text style={styles.weatherTextContentRight}>View All></Text>
                  </TouchableOpacity>
                </View>
              </View>

              <View style={{width:'100%', marginTop:0, paddingBottom:5, backgroundColor:colors.white, paddingLeft:25, paddingRight:25}}>
                <View style={{marginTop:0}}>
                  <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                    <FlatList
                      horizontal={true}
                      data={this.state.listNews}
                      renderItem={this._renderNews}
                      keyExtractor={(item, index) => index.toString()}
                    />
                  </ScrollView>
                </View>
              </View>
            </ScrollView>
          </View>
        </View>
      {/*<CustomFooter navigation={this.props.navigation} menu="Home" />*/}
      </View>
      </Container>
      );
  }
}

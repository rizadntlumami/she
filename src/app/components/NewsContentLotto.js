import React, { Component } from "react";
import { View, Text, Image, TouchableNativeFeedback ,Platform, AsyncStorage} from "react-native";
import Ripple from "react-native-material-ripple";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    height: 100,
    width: 150,
    marginLeft: 3,
    marginRight:3,
    paddingTop:3,
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class NewsContentLotto extends Component {
  navigateToScreen(route, listLotto) {
    AsyncStorage.setItem("list", JSON.stringify(listLotto)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    return (
      <View style={styles.container}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 5,
            borderWidth:Platform.OS==='ios'?1:0,
            borderColor:(Platform.OS==='ios')?colors.lightGray:null,
            elevation: 1
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() => this.navigateToScreen("LottoDetail", this.props.id)}
          rippleColor={colors.accent}
        >
        <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ marginLeft:0 , paddingTop:3, width: 100 }}>
                <Text style={{fontSize: 10, fontWeight:'bold'}}>{this.props.number}</Text>
            </View>
            <View>
                <Text style={{fontSize: 8, paddingTop:3, textAlign:'right'}}>{this.props.date}</Text>
                <Text style={{fontSize: 8, paddingTop:2, textAlign:'right'}}>{this.props.time}</Text>
            </View>
        </View>
        <View>
            <Text numberOfLines={1} style={{fontSize: 9, fontWeight:'bold', textAlign:'left'}}>PIC : {this.props.create}</Text>
            <Text numberOfLines={1} style={{fontSize: 9, fontWeight:'bold'}}>{this.props.kegiatan}</Text>
            <Text numberOfLines={1} style={{fontSize: 9}}>{this.props.note}</Text>
        </View>
        </Ripple>
      </View>
    );
  }
}

NewsContentLotto.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};
 
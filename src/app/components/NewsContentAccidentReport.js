import React, { Component} from "react";
import { View, Text, Image, TouchableNativeFeedback ,Platform,AsyncStorage } from "react-native";
import Ripple from "react-native-material-ripple";
import colors from "../../styles/colors";
import PropTypes from "prop-types";

let styles = {
  container: {
    height: 100,
    width: 155,
    marginLeft: 3,
    marginRight: 3,
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class NewsContentAccidentReport extends Component {
  navigateToScreen(route, id_Accident) {
    // alert(id_Unsafe)
    AsyncStorage.setItem("list", JSON.stringify(id_Accident)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { loading, disabled, handleOnPress } = this.props;
    var arrNoPeg=this.props.badge.split('');
    var noPeg=''
    var zeroStop=false
    for (let i=0;i<arrNoPeg.length;i++){
      if (arrNoPeg[i]!='0'){
        zeroStop=true
      }
      if (zeroStop){
        noPeg+=arrNoPeg[i]
      }
    }
    console.log(noPeg)
    return (
      <View style={[styles.container]}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center",
            borderRadius: 5,
            elevation: 1,
            borderWidth:Platform.OS==='ios'?1:0,
            borderColor:(Platform.OS==='ios')?colors.lightGray:null
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          rippleColor={colors.accent}
        >
          <Image
            source={this.props.imageUri}
            style={{ marginTop: 10, width: 130, height: 60 }}
          />
          <View style={{ flex: 1, paddingTop: 3, alignItems: "center" }}>
            <Text numberOfLines={1} style={{ fontSize: 9, fontWeight: 'bold', marginLeft: 10, marginRight: 10, marginBottom: 10 }}>[{noPeg}] {this.props.name}</Text>
          </View>
          <View style={{ flex: 1, marginTop: 20, alignItems: "center" }}>
            <Text style={{ fontSize: 9 }}>{this.props.date}</Text>
          </View>
        </Ripple>
      </View>
    );
  }
}

NewsContentAccidentReport.propTypes = {
  handleOnPress: PropTypes.func,
  disabled: PropTypes.bool
};

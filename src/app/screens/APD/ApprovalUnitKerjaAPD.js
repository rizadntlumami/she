import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  RefreshControl,
  Platform
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Card,
  CardItem,
  Item,
  Input,
  Icon,
  Thumbnail
} from "native-base";
// import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
import GlobalConfig from "../../components/GlobalConfig";
import CustomFooter from "../../components/CustomFooter";
import Moment from "moment";
import styles from "../styles/ApprovalAPD";
import colors from "../../../styles/colors";

const helmet = require("../../../assets/images/helmet.png");

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, id_personal,kode) {
    // alert(route);
    AsyncStorage.setItem("idAndKode", id_personal+'+'+kode).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View>
        <Card style={{ marginLeft: 10, marginRight: 10 }}>
          <CardItem
            button
            onPress={() =>
              this.navigateToScreen(
                "ActionsApprovalUnitKerjaAPD",
                this.props.data.id,
                this.props.data.kode_order
              )
            }
          >
            <Left>
              <Body>
                <Text
                  style={styles.cardtextKode}
                  // onPress={() =>
                  //   this.navigateToScreen(
                  //     "ActionsApprovalUnitKerjaAPD",
                  //     this.props.data.ID
                  //   )
                  // }
                >
                  {this.props.data.kode_order}
                </Text>
                <Text style={styles.cardtext}>
                  {this.props.data.name} / {this.props.data.no_badge}
                </Text>
                <Text style={styles.cardtext}>{this.props.data.uk_text}</Text>
              </Body>
            </Left>
          </CardItem>
          <CardItem>
            <Left>
              <Body>
                <Text style={{ fontSize: 11 }}>
                  Order Date : {Moment(this.props.data.create_at).format('DD MMMM YYYY')}
                </Text>
              </Body>
            </Left>
          </CardItem>
        </Card>
      </View>
    );
  }
}

export default class ApprovalUnitKerjaAPD extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isloading: true,
      searchApproval: "",
      isEmpty: false,
      role: ''
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;
  
  componentDidMount() {
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + 'validasi';
      var formData = new FormData();
      formData.append("token", value)
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            name: responseJson.user.name,
            role : responseJson.user.role_id,
            isloading: false
          });
          this.loadData();
        })
        .catch(error => {
          console.log(error);
        });
    })

    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.setState({
          searchApproval: ""
        });
        this.loadData();
      }
    );
  }

  onRefresh() {
    console.log('refreshing')
    this.setState({ isloading: true }, function() {
      this.loadData();
    });
  }

  loadData() {
    this.setState({
      isloading: true
    });
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "approvelist/unitkerja";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("role_id", this.state.role);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson));
          if (responseJson.status == 422) {
            this.setState({
              isloading: false
            });
            this.setState({
              isEmpty: true
            });
          } else {
            this.setState({
              dataSource: responseJson.data,
              isloading: false,
              isEmpty:false
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  searchData() {
    this.setState({
      isloading: true
    });
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "approvelist/unitkerja";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("search", this.state.searchApproval);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.status == 422) {
            this.setState({
              isloading: false
            });
            alert("Data Tidak Ditemukan");
          } else {
            this.setState({
              dataSource: responseJson.data,
              isloading: false
            });
          }
        })
        .catch(error => {
          this.setState({
            isloading: false
          });
          console.log(error);
        });
    });
  }

  render() {
    that = this;
    var list;
    if (this.state.isloading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.dataSource=='') {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          // <ScrollView>
            <FlatList
              data={this.state.dataSource}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isloading}
                  onRefresh={this.onRefresh.bind(this)}
                />}
            />
          // </ScrollView>
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("ApdMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Approval APD</Title>
          </Body>

            <Right style={{flex:1}}/>

        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={styles.homeWrapper}>
          <Footer style={styles.tabHeight}>
            <FooterTab style={styles.tabfooter}>
              <Button
                onPress={() =>
                  this.props.navigation.navigate("ApprovalPersonalAPD")
                }
              >
                <Text style={{color:'white', fontWeight:'bold'}}>PERSONAL</Text>
              </Button>
              <Button active style={styles.tabfooter}>
                <View style={{height:'40%'}}></View>
                <View style={{height:'50%'}}>
                <Text>UNIT KERJA</Text>
                </View>
                <View style={{height:'20%'}}></View>
                <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.white}}></View>
              </Button>
              <Button
                onPress={() =>
                  this.props.navigation.navigate("ApprovalPeminjamanAPD")
                }
              >
                <Text style={{color:'white', fontWeight:'bold'}}>PEMINJAMAN</Text>
              </Button>
            </FooterTab>
          </Footer>
          <View>
            <View>
              <View style={styles.viewLeftHeader}>
                
              </View>
            </View>
          </View>
          <View style={{ flex: 1, flexDirection: "column" }}>
            {list}
            {/* <ScrollView></ScrollView> */}
          </View>
        </View>
        {/* <CustomFooter navigation={this.props.navigation} menu="APD" /> */}
      </Container>
    );
  }
}

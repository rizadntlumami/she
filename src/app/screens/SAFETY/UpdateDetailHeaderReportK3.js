import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Label,
    Item,
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DateTimePicker from 'react-native-datepicker';
import ImagePicker from "react-native-image-picker";
import Ripple from "react-native-material-ripple";
import CheckBox from 'react-native-check-box';

class ListItem extends React.PureComponent {
  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.props.setSelectedUnitKerjaLabel(this.props.data.muk_nama)
          }
          rippleColor={colors.accent}
        >
          <CardItem
            style={{
              borderRadius: 0,
              marginTop: 4,
              backgroundColor: colors.gray
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 4,
                backgroundColor: colors.gray
              }}
            >
              <View >
                <Text style={{fontSize:12}}>{this.props.data.muk_kode} - {this.props.data.muk_nama}</Text>
              </View>
            </View>
          </CardItem>
        </Ripple>
      </View>
    );
  }
}

export default class UpdateDetailHeaderReportK3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      dataHeader: [],
      isLoading: true,
      listHeader:[],
      aktifitas:'',

      type: '',
      penyebab: '',
      potensi: '',
      rekomendasi: '',
      followup:'',
      unitCode: '',
      unitName: '',
      waktu:'',
      idCategori: '',
      status: '',
      fotoTemuan:'',
      fotoFollowup: '',
      //idReport:'',
      listUnitKerja:[],
      listVendor:[],
      listKategori:[],
      pickedImage: '',
      uri: '',
      fileType:'',
      fileName: '',
      pickedImage2: '',
      uri2: '',
      fileType2:'',
      fileName2: '',
      isChecked:false,
      value:'',
      visibleLoadingUnitKerja:false,
      visibleSearchListUnitKerja:false,
      selectedUnitKerjaLabel:'',
      searchWordUnitKerja:'',
      listFileUpload:[],
      visibleDialogSubmit:false
    };
  }

  componentDidMount() {
      AsyncStorage.getItem('list').then((listHeader) => {
        // console.log(JSON.parse(listHeader))
        this.setState({
          listHeader: JSON.parse(listHeader),
          // pickedImage2 : GlobalConfig.SERVERHOST+'api'+ JSON.parse(listHeader).LIST_FILE_UPLOAD[1].UPLOADED_PATH,
        });
        this.setState({
          aktifitas: this.state.listHeader.ACTIVITY_DESCRIPTION,
          type: this.state.listHeader.UNSAFE_TYPE,
          penyebab: this.state.listHeader.TROUBLE_MAKER,
          potensi: this.state.listHeader.POTENTIAL_HAZARD,
          rekomendasi: this.state.listHeader.RECOMENDATION,
          followup: this.state.listHeader.FOLLOW_UP,
          unitCode: this.state.listHeader.UNIT_CODE,
          selectedUnitKerjaLabel: this.state.listHeader.UNIT_NAME,
          value: this.state.listHeader.IS_SMIG,
          waktu: this.state.listHeader.O_CLOCK_INCIDENT,
          idCategori: this.state.listHeader.ID_CATEGORY,
          status: (this.state.listHeader.STATUS).toUpperCase(),
          // listFileUpload: JSON.parse(listHeader).LIST_FILE_UPLOAD

        })
        if (this.state.listHeader.LIST_FILE_UPLOAD[0]!=undefined){
          if (this.state.listHeader.LIST_FILE_UPLOAD[1]!=undefined){
            this.setState({
              pickedImage :  GlobalConfig.SERVERHOST+'api'+ this.state.listHeader.LIST_FILE_UPLOAD[0].UPLOADED_PATH,
              pickedImage2 :  GlobalConfig.SERVERHOST+'api'+ this.state.listHeader.LIST_FILE_UPLOAD[1].UPLOADED_PATH,
            })
          }else{
            this.setState({
              pickedImage :  GlobalConfig.SERVERHOST+'api'+ this.state.listHeader.LIST_FILE_UPLOAD[0].UPLOADED_PATH,
              // pickedImage2 :  GlobalConfig.SERVERHOST+'api'+ this.state.listHeader.LIST_FILE_UPLOAD[1].UPLOADED_PATH,
            })
          }
      }
      })

      this.loadUnitKerja();
      this.loadVendor();
      this.loadKategori();
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        this.loadData();
      });
  }


  pickImageHandler = () => {
      ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
          if (res.didCancel) {
              console.log("User cancelled!");
          } else if (res.error) {
              console.log("Error", res.error);
          } else {
              this.setState({
                  pickedImage: res.uri,
                  uri: res.uri,
                  fileType:res.type
              });

          }
      });
  }

  pickImageHandler2 = () => {
      ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
          if (res.didCancel) {
              console.log("User cancelled!");
          } else if (res.error) {
              console.log("Error", res.error);
          } else {
              this.setState({
                  pickedImage2: res.uri ,
                  uri2: res.uri,
                  fileType2:res.type
              });

          }
      });
  }

  static navigationOptions = {
      header: null
  };

  loadData() {

  }

  loadUnitKerja(){
    this.setState({
      visibleLoadingUnitKerja:true
    })
    AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/unit_kerja';
        var formData = new FormData();
        formData.append("token", value)
        formData.append("search", this.state.searchWordUnitKerja)
        formData.append("limit", 250)
        formData.append("plant", 0)
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                  visibleLoadingUnitKerja:false
                })
                this.setState({
                    listUnitKerja: responseJson,
                    isloading: false
                });
            })
            .catch((error) => {
              this.setState({
                visibleLoadingUnitKerja:false
              })
                console.log(error)
            })
    })
  }

  loadVendor(){
    AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/vendor';
        var formData = new FormData();
        formData.append("token", value)
        formData.append("search", "")
        formData.append("limit", "")
        formData.append("plant", "")
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listVendor: responseJson,
                    isloading: false
                });
            })
            .catch((error) => {
                console.log(error)
            })
    })
  }

  loadKategori(){
    AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/category';
        var formData = new FormData();
        formData.append("token", value)
        formData.append("search", 0)
        formData.append("limit", 250)
        formData.append("plant", 0)
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listKategori: responseJson,
                    isloading: false
                });
            })
            .catch((error) => {
                console.log(error)
            })
    })
  }

  UpdateDetailHeader() {
  if (this.state.waktu == null) {
    alert('Masukkan Waktu Report');
  }
  else if (this.state.selectedUnitKerjaLabel == '') {
    alert('Masukkan Unit Kerja');
  }
  else if (this.state.selectedKategori == '') {
    alert('Masukkan Kategori');
  }
  else if (this.state.aktifitas == null) {
    alert('Masukkan Aktifitas Temuan');
  }
  else if (this.state.potensi == null) {
    alert('Masukkan Potensi Bahaya');
  }
  else {
    this.setState({
      visibleDialogSubmit:true
    })
    if(this.state.isChecked == false){
      this.setState({
          value: 1,
      });
    } else {
      this.setState({
          value: 0,
      });
    }
    AsyncStorage.getItem("token").then(value => {
      var url = GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe_n/unsafe_detail_list/update";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID_UNSAFE_RPT_DETAIL", this.state.listHeader.ID_UNSAFE_RPT_DETAIL);
      formData.append("ID_UNSAFE_REPORT", this.state.listHeader.ID_UNSAFE_REPORT);
      formData.append("ACTIVITY_DESCRIPTION", this.state.aktifitas);
      formData.append("UNSAFE_TYPE", this.state.type);
      if (this.state.penyebab!='') {
        formData.append("TROUBLE_MAKER", this.state.penyebab);
      }
      else {
        formData.append("TROUBLE_MAKER", '');
      }
      formData.append("POTENTIAL_HAZARD", this.state.potensi);
      if (this.state.rekomendasi!='') {
        formData.append("RECOMENDATION", this.state.rekomendasi);
      } else {
        formData.append("RECOMENDATION", '');
      }
      if (this.state.followup!='') {
        formData.append("FOLLOW_UP", this.state.followup);
      } else {
        formData.append("FOLLOW_UP", '');
      }
      formData.append("IS_SMIG", this.state.value);
      formData.append("UNIT_CODE", this.state.unitCode);
      formData.append("UNIT_NAME", this.state.selectedUnitKerjaLabel);
      formData.append("O_CLOCK_INCIDENT", this.state.waktu);
      formData.append("ID_CATEGORY", this.state.idCategori);
      formData.append("STATUS", this.state.status);
      if (this.state.uri!='') {
        formData.append("FOTO_TEMUAN", {
          uri: this.state.uri,
          type: this.state.fileType,
          name: 'image'
      });
      }
      if (this.state.uri2!='') {
        formData.append("FOTO_FOLLOW_UP", {
          uri: this.state.uri2,
          type: this.state.fileType2,
          name: 'image'
      });
      }

      // console.log(formData)

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        if (response.Status == 200) {
          this.setState({
            visibleDialogSubmit:false
          })
          Alert.alert('Success', 'Update Detail Success', [{
            text: 'Oke'
          }])
          this.props.navigation.navigate('DailyReportK3DetailList')
        } else {
          this.setState({
            visibleDialogSubmit:false
          })
          Alert.alert('Error', 'Update Detail Failed', [{
            text: 'Oke'
          }])
        }
      })
      .catch((error)=>{
        console.log(error)
        this.setState({
          visibleDialogSubmit:false
        })
        Alert.alert('Error', 'Update Detail Failed', [{
          text: 'Oke'
        }])
      })
  })
  }
}


  onChangeUnitKerja(text){
    this.setState({
      searchWordUnitKerja:text
    })
    this.loadUnitKerja();
    // this.setState({
    //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
    // })
  }

  onClickSearchUnitKerja(){
    console.log('masuk')
    if (this.state.visibleSearchListUnitKerja){
      this.setState({
        visibleSearchListUnitKerja:false
      })
    }else{
      this.setState({
        visibleSearchListUnitKerja:true
      })
    }
  }

  setSelectedUnitKerjaLabel(text){
    this.setState({
      selectedUnitKerjaLabel:text,
      visibleSearchListUnitKerja:false
    })
  }

  _renderItem = ({ item }) => <ListItem data={item} setSelectedUnitKerjaLabel={text => this.setSelectedUnitKerjaLabel(text)} />;

  render() {
    let listUnitKerja = this.state.listUnitKerja.map( (s, i) => {
      return <Picker.Item key={i} value={s.muk_nama} label={s.muk_nama} />
    });

    let listVendor = this.state.listVendor.map( (s, i) => {
      return <Picker.Item key={i} value={s.NAME1} label={s.NAME1} />
    });

    let listKategori = this.state.listKategori.map( (s, i) => {
      return <Picker.Item key={i} value={s.ID} label={s.DESKRIPSI} />
    });

    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DailyReportK3DetailHeaderList")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Update Daily Report K3</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={{ flex: 1 }}>
        <Content style={{ marginTop: 0 }}>
          <View style={{ backgroundColor: '#FEFEFE' }}>
          <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1}}>
                  <View style={{paddingBottom:5}}>
                    <Text style={styles.titleInput}>Upload Foto Temuan</Text>
                  </View>
                  <View>
                    <Ripple
                      style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      rippleSize={176}
                      rippleDuration={600}
                      rippleContainerBorderRadius={15}
                      onPress={this.pickImageHandler}
                      rippleColor={colors.accent}
                    >
                    <View style={styles.placeholder}>
                        <Image source={{uri:this.state.pickedImage + '?' + new Date()}} style={styles.previewImage}/>
                    </View>
                    </Ripple>
                  </View>
              </View>
          </CardItem>

          <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1}}>
                  <View style={{paddingBottom:5}}>
                    <Text style={styles.titleInput}>Upload Foto Follow Up</Text>
                  </View>
                  <View>
                    <Ripple
                      style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                      }}
                      rippleSize={176}
                      rippleDuration={600}
                      rippleContainerBorderRadius={15}
                      onPress={this.pickImageHandler2}
                      rippleColor={colors.accent}
                    >
                    <View style={styles.placeholder}>
                        <Image source={{uri:this.state.pickedImage2 + '?' + new Date()}} style={styles.previewImage}/>
                    </View>
                    </Ripple>
                  </View>
              </View>
          </CardItem>


          <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
            <View style={{ flex: 1}}>
              <View>
                <Text style={styles.titleInput}>Status (*)</Text>
                <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                    <Picker
                        mode="dropdown"
                        iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                        style={{ width: '100%', height:35}}
                        placeholder="Select Status ..."
                        placeholderStyle={{ color: "#bfc6ea" }}
                        placeholderIconColor="#007aff"
                        selectedValue={this.state.status}
                        onValueChange={(itemValue) => this.setState({status:itemValue})}>
                        <Picker.Item label="Open"  value="OPEN"/>
                        <Picker.Item label="Close"  value="CLOSE"/>
                    </Picker>
                </Form>
              </View>
            </View>
          </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1}}>
                <View>
                  <Text style={styles.titleInput}>Unsafe Type (*)</Text>
                  <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                      <Picker
                          mode="dropdown"
                          iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                          style={{ width: '100%', height:35}}
                          placeholder="Select Shift ..."
                          placeholderStyle={{ color: "#bfc6ea" }}
                          placeholderIconColor="#007aff"
                          selectedValue={this.state.type}
                          onValueChange={(itemValue) => this.setState({type:itemValue})}>
                          <Picker.Item label="UA / Unsafe Action"  value="Unsafe Action" Sele/>
                          <Picker.Item label="UC / Unsafe Condition"  value="Unsafe Condition"/>
                      </Picker>
                  </Form>
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
              <View style={{ flex: 1}}>
                <View>
                  <Text style={styles.titleInput}>Jam (*)</Text>
                  <DateTimePicker
                      style={{ width: '100%', fontSize:10, borderRadius:20}}
                      date={this.state.waktu}
                      mode="time"
                      placeholder="Choose Time ..."
                      format="hh:mm"
                      minTime="00:00"
                      maxTime="23:59"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateInput: {
                          marginLeft: 5, marginRight:5, height: 35, borderRadius:5, fontSize:10, borderWidth:1, borderColor:"#E6E6E6"
                        },
                        dateIcon: {
                          position: 'absolute',
                          left: 0,
                          top: 5,
                        },
                      }}
                      onDateChange={(time) => { this.setState({ waktu: time }) }}
                  />
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1 }}>
                <View>
                  <Text style={styles.titleInput}>Unit Kerja/Instansi/Vendor (*)</Text>
                  {this.state.isChecked == false
                    ? (
                  <View>
                      <View style={{ flex: 1,flexDirection:'column'}}>
                          <Button
                              block
                              style={{flex:1,borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, backgroundColor:colors.gray ,borderColor:"#E6E6E6", height:35}}
                              onPress={() => this.onClickSearchUnitKerja()}>
                              <Text style={{fontSize:12}}>{this.state.selectedUnitKerjaLabel}</Text>
                          </Button>
                      </View>

                      {this.state.visibleSearchListUnitKerja &&
                      <View style={{height:300,flexDirection:'column',borderWidth:1,padding:10,backgroundColor:colors.gray,margin:5,borderColor:"#E6E6E6",}}>
                          <Form>
                              <Item stackedLabel style={{ marginLeft: 0 }}>
                                  <Input value={this.state.searchWordUnitKerja} style={{borderRadius:5, marginLeft:5, marginRight:5, fontSize:10}} bordered onChangeText={(text) => this.onChangeUnitKerja(text)} placeholder='Ketik Unit Kerja' />
                              </Item>
                          </Form>
                          <FlatList
                            data={this.state.listUnitKerja}
                            renderItem={this._renderItem}
                            keyExtractor={(item, index) => index.toString()}
                          />
                      </View>
                      }
                  </View>
                  ):(
                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                            style={{ width: '100%', height:35 }}
                            placeholder="Unit Kerja"
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selectedUnitKerjaLabel}
                            onValueChange={(itemValue) => this.setState({selectedUnitKerjaLabel:itemValue})}>
                            <Picker.Item label="Choose Vendor..."  value=""/>
                            {listVendor}
                        </Picker>
                    </Form>
                  )}
                </View>
                <CheckBox
                  style={{flex:1, paddingTop:5}}
                  onClick={()=>{
                    this.setState({
                      isChecked:!this.state.isChecked,
                    })
                  }}
                  isChecked={this.state.isChecked}
                  rightText={"Non SMIG"}
                />
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1}}>
                <View>
                  <Text style={styles.titleInput}>Kategori (*)</Text>
                  <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                      <Picker
                          mode="dropdown"
                          iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                          style={{ width: '100%', height:35 }}
                          placeholder="Kategori"
                          placeholderStyle={{ color: "#bfc6ea" }}
                          placeholderIconColor="#007aff"
                          selectedValue={this.state.idCategori}
                          onValueChange={(itemValue) => this.setState({idCategori:itemValue})}>
                          <Picker.Item label="Choose Kategori..."  value=""/>
                          {listKategori}
                      </Picker>
                  </Form>
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1 }}>
                <View>
                <Text style={styles.titleInput}>Uraian Aktifitas (*)</Text>
                </View>
                <View>
                <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.aktifitas} placeholder='Type Aktifitas ...' onChangeText={(text) => this.setState({ aktifitas: text })} />
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1 }}>
                <View>
                <Text style={styles.titleInput}>Penyebab Masalah</Text>
                </View>
                <View>
                <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.penyebab} placeholder='Type Penyebab ...' onChangeText={(text) => this.setState({ penyebab: text })} />
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1 }}>
                <View>
                <Text style={styles.titleInput}>Potensi Bahaya (*)</Text>
                </View>
                <View>
                <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.potensi} placeholder='Type Potensi Bahaya ...' onChangeText={(text) => this.setState({ potensi: text })} />
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1 }}>
                <View>
                <Text style={styles.titleInput}>Rekomendasi Safety</Text>
                </View>
                <View>
                <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.rekomendasi} placeholder='Type Rekomen Safety ...' onChangeText={(text) => this.setState({ rekomendasi: text })} />
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1 }}>
                <View>
                <Text style={styles.titleInput}>Rencana Tindak Lanjut</Text>
                </View>
                <View>
                <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.followup} placeholder='Type Tindak Lanjut ...' onChangeText={(text) => this.setState({ followup: text })} />
                </View>
              </View>
            </CardItem>
        </View>
        <View style={styles.Contentsave}>
          <Button
            block
            style={{
              height: 45,
              marginLeft: 20,
              marginRight: 20,
              marginBottom: 20,
              borderWidth: 1,
              backgroundColor: "#00b300",
              borderColor: "#00b300",
              borderRadius: 4
            }}
            onPress={() => this.UpdateDetailHeader()}
          >
            <Text style={{color:colors.white}}>SUBMIT</Text>
          </Button>
        </View>
      </Content>

        
      </View>
      <View style={{ width: 270, position: "absolute" }}>
          <Dialog visible={this.state.visibleLoadingUnitKerja}>
            <DialogContent>
              {
                <ActivityIndicator size="large" color="#330066" animating />
              }
            </DialogContent>
          </Dialog>
      </View>
      <View style={{ width: 270, position: "absolute" }}>
        <Dialog
          visible={this.state.visibleDialogSubmit}
          dialogTitle={<DialogTitle title="Updating Detail Header Report K3 .." />}
        >
          <DialogContent>
            {<ActivityIndicator size="large" color="#330066" animating />}
          </DialogContent>
        </Dialog>
      </View>
      <CustomFooter navigation={this.props.navigation} menu='Safety' />
    </Container>

    );
  }
}

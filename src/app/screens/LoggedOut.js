import React, { Component } from "react";
import {
  StyleSheet,
  Alert,
  View,
  Image,
  Text,
  StatusBar,
  TouchableHighlight,
  BackHandler,
  Dimensions,
  Platform,
  TouchableOpacity
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body
} from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";

// import OneSignal from 'react-native-onesignal';

import styles from "./styles/LoggedOut";
import colors from "../../styles/colors";
import RoundedButton from "../components/RoundedButton";

const shelogo = require("../../assets/images/shelogo.png");
const silogo = require("../../assets/images/Logo-Semen-Indonesia.png");
const grlogo = require("../../assets/images/Logo-Semen-Gresik.png");
const pdlogo = require("../../assets/images/Logo-Semen-Padang.png");
const tnlogo = require("../../assets/images/Logo-Semen-Tonasa.png");

export default class LoggedOut extends Component {
  constructor(props) {
    super(props);
    // OneSignal.init("2316e5db-94ae-4d69-89c0-9e2ba54429d2");

    // OneSignal.addEventListener("received", this.onReceived);
    // OneSignal.addEventListener("opened", this.onOpened);
    // OneSignal.addEventListener("ids", this.onIds);
    
  }

  static navigationOptions = {
    header: null
  };

  

  componentWillMount() {
    BackHandler.addEventListener("hardwareBackPress", this.backPressed);
    // OneSignal.removeEventListener('received', this.onReceived);
    // OneSignal.removeEventListener('opened', this.onOpened);
    // OneSignal.removeEventListener('ids', this.onIds);
  }

  componentWillUnmount() {
    BackHandler.removeEventListener("hardwareBackPress", this.backPressed);
  }

  onReceived(notification) {
    console.log("Notification received: ", notification);
  }

  onOpened(openResult) {
    console.log('Message: ', openResult.notification.payload.body);
    console.log('Data: ', openResult.notification.payload.additionalData);
    console.log('isActive: ', openResult.notification.isAppInFocus);
    console.log('openResult: ', openResult);
  }

  onIds(device) {
    console.log('Device info: ', device);
  }

  backPressed = () => {
    // Alert.alert(
    //   'Confirmation',
    //   'Exit SHE Mobile?',
    //   [
    //     { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
    //     { text: 'Yes', onPress: () => BackHandler.exitApp() },
    //   ],
    //   { cancelable: false }
    // );
    // return true;
    this.props.navigation.goBack(null);
    return true;
  };

  onLoginPress = () => {
      this.props.navigation.navigate("Login");
  };

  exitApp() {
    Alert.alert(
      "Confirmation",
      "Exit SHE Mobile?",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Yes", onPress: () => BackHandler.exitApp() }
      ],
      { cancelable: false }
    );
  }

  render() {
    return (
      <Container style={styles.wrapper}>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <LinearGradient
          style={styles.welcomeWrapper}
          colors={[colors.green03, colors.blue02, colors.whiteBlue]}
        >
          {Platform.OS != "ios" && (
            <TouchableOpacity
              visible={Platform.OS === "ios" ? false : true}
              transparent
              style={{
                position: "absolute",
                top:
                  (Dimensions.get("window").height === 812 ||
                    Dimensions.get("window").height === 896) &&
                  Platform.OS === "ios"
                    ? 40
                    : 20,
                right: 20
              }}
              onPress={() => this.exitApp()}
            >
              <Icon name="close" size={20} style={{ color: colors.white }} />
            </TouchableOpacity>
          )}
          <Image source={shelogo} style={styles.logo} />
          <Text style={styles.welcomeText}>Welcome to SHE Mobile. </Text>
          <RoundedButton
            text="Continue with Login"
            textColor={colors.white}
            background={colors.green01}
            icon={
              <Icon
                name="user-circle"
                size={20}
                style={styles.facebookButtonIcon}
              />
            }
            handleOnPress={this.onLoginPress}
          />
          <View style={styles.termsAndConditions}>
            <Text style={styles.termsText}>
              By tapping continue, I agree to Safety Health Environment
            </Text>
            <Text style={styles.termsText}>{"Mobile's "}</Text>
            <TouchableHighlight style={styles.linkButton}>
              <Text style={styles.termsText}>Terms of Services</Text>
            </TouchableHighlight>
            <Text style={styles.termsText}>, </Text>
            <TouchableHighlight style={styles.linkButton}>
              <Text style={styles.termsText}>Payments Terms of Services</Text>
            </TouchableHighlight>
            <Text style={styles.termsText}>, </Text>
            <TouchableHighlight style={styles.linkButton}>
              <Text style={styles.termsText}>and Privacy Policy </Text>
            </TouchableHighlight>
            <Text style={styles.termsText}>by Semen Indonesia. </Text>
          </View>
        </LinearGradient>
        <Footer style={styles.footer}>
          <Image source={silogo} style={styles.footerLogoSI} />
          <Image source={grlogo} style={styles.footerLogo} />
          <Image source={pdlogo} style={styles.footerLogo} />
          <Image source={tnlogo} style={styles.footerLogo} />
        </Footer>
      </Container>
    );
  }
}

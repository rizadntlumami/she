import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Icon,
    Item,
    Input
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewHeader from "../../components/ListViewHeader";
import ListViewDetail from "../../components/ListViewDetail";

class DailyReportK3Detail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      dataDetail: [],
      isLoading: true,
      headerRekapitulasi:[],
      visibleDialogSubmit:false
    };
  }

  static navigationOptions = {
      header: null
  };

  _renderItem = ({ item }) => (
      <ListItem data={item}></ListItem>
  )

  navigateToScreen(route, headerRekapitulasi) {
      AsyncStorage.setItem('list', JSON.stringify(headerRekapitulasi)).then(() => {
          this.props.navigation.navigate(route);
      })
  }

  navigateToScreenDetail(route, idUnsafe) {
      AsyncStorage.setItem('id', idUnsafe).then(() => {
          this.props.navigation.navigate(route);
      })
  }

  componentDidMount() {
      AsyncStorage.getItem('list').then((listHeaderDailyReport) => {
          this.setState({ headerRekapitulasi: JSON.parse(listHeaderDailyReport) });
          this.setState({ isLoading: false });
          console.log(this.state.headerRekapitulasi);
      })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        //this.loadData(id_Unsafe);
      });
  }

  konfirmasideleteHeader() {
    Alert.alert(
      'Apakah Anda Yakin Ingin Menghapus', this.state.headerRekapitulasi.AREA_TXT,
      [
        {text: 'Yes', onPress:()=> this.deleteHeader()},
        {text: 'Cancel'},
      ],
      { cancelable:false}
    )
  }

  deleteHeader() {
    this.setState({
      visibleDialogSubmit:true
    })
    AsyncStorage.getItem('token').then((value) => {
      // alert(JSON.stringify(value));
      const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe_n/delete_unsafe_report';
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID_UNSAFE_REPORT", this.state.headerRekapitulasi.ID_UNSAFE_REPORT);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.Status == 200) {
            this.setState({
              visibleDialogSubmit:false
            })
            Alert.alert('Success', 'Delete Success', [{
              text: 'Oke',
            }])
            this.props.navigation.navigate("DailyReportK3")
          } else {
            this.setState({
              visibleDialogSubmit:false
            })
            Alert.alert('Error', 'Delete Failed', [{
              text: 'Oke'
            }])
          }
        })
        .catch((error)=>{
          console.log(error)
          this.setState({
            visibleDialogSubmit:false
          })
          Alert.alert('Error', 'Delete Failed', [{
            text: 'Oke'
          }])
        })
    })
  }

  render() {
    var listReportK3;
    var listDetailReportK3;
    return (
      <Container style={styles.wrapper}>
          <Header style={styles.header}>
            <Left>
              <Button
                transparent
                onPress={() => this.props.navigation.navigate("DailyReportK3")}
              >
                <Icon
                  name="ios-arrow-back"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Left>
            <Body>
              <Title style={styles.textbody}>Safety Report Detail</Title>
            </Body>
            <Right style={{width:'20%'}}>
              <Button
                transparent
                onPress={() => this.navigateToScreen('UpdateHeaderReportK3', this.state.headerRekapitulasi)}
              >
                <Icon
                  name="ios-create"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
              <Button
                transparent
                onPress={() => this.konfirmasideleteHeader()}
              >
                <Icon
                  name="ios-trash"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
              <Button
                transparent
                onPress={() => this.navigateToScreenDetail('DailyReportK3DetailList', this.state.headerRekapitulasi.ID_UNSAFE_REPORT)}
              >
                <Icon
                  name="ios-list"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            </Right>
          </Header>
          <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
          <Content style={{marginTop:5}}>

          <View style={{}}>
            <CardItem style={{ borderRadius: 0, marginTop:5, }}>
              <View style={{ flex: 1, flexDirection: "row"}}>
                <View style={{width: 260}}>
                  <Text style={{fontSize:10}}>Area</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.headerRekapitulasi.AREA_TXT}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Sub Area</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.headerRekapitulasi.SUB_AREA_TXT}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Report Date</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.headerRekapitulasi.REPORT_DATE}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Shift</Text>
                  <Text style={{fontSize:10, fontWeight:"bold", color:colors.green01}}>{this.state.headerRekapitulasi.SHIFT}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Plant</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.headerRekapitulasi.PLANT_NAME}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Nearmiss</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.headerRekapitulasi.NEARMISS}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Accident</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.headerRekapitulasi.ACCIDENT}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Incident</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.headerRekapitulasi.INCIDENT}</Text>
                  <Text style={{fontSize:10, paddingTop:10}}>Inspector</Text>
                  <Text style={{fontSize:10, fontWeight:"bold"}}>{this.state.headerRekapitulasi.INSPECTOR_NAME}</Text>
                </View>
                <View style={{width: 65}}>
                  
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0 }}>
                <View>
                  
                </View>
            </CardItem>
            
          </View>
          <View style={{ width: 270, position: "absolute" }}>
            <Dialog
              visible={this.state.visibleDialogSubmit}
              dialogTitle={<DialogTitle title="Deleting Header Report K3 .." />}
            >
              <DialogContent>
                {<ActivityIndicator size="large" color="#330066" animating />}
              </DialogContent>
            </Dialog>
          </View>
          </Content>
      </Container>
    );
  }
}

export default DailyReportK3Detail;

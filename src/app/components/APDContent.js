import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import GlobalConfig from "./GlobalConfig";

export default class APDContent extends Component {
  render() {
    return (
      <View
        style={{
          height: 130,
          width: '100%',
          marginTop: 0,
          marginLeft: 0,
        }}
      >
        <View style={{ flex: 2, paddingTop:0}}>
          <Image
            source={this.props.imageUri}
            style={{ flex: 1, paddingLeft:0, marginLeft:0, width: null, resizeMode: "cover" }}
          />
        </View>
        <View style={{ flex: 1, paddingLeft: 5, paddingTop: 1 }}>
          <Text style={{fontSize:10, color:'#00b300'}}>{this.props.name}</Text>
          <Text style={{fontSize:9}}>{this.props.merk}</Text>
          <Text style={{fontSize:8}}>Stok : {this.props.stok}</Text>
        </View>
      </View>
    );
  }
}

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewReportDetail from "../../components/ListViewReportDetail";
import Ripple from "react-native-material-ripple";

class ListItem extends React.PureComponent {
  navigateToScreen(route, id_listReport) {
    // alert(id_listReport)
    AsyncStorage.setItem("list", JSON.stringify(id_listReport)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View>
        <Ripple
          style={
            {
              // flex: 2,
              // justifyContent: "center",
              // alignItems: "center"
            }
          }
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={
            () =>
              this.navigateToScreen(
                "DailyReportDetailListFoto",
                this.props.data
              )
          }
          rippleColor={colors.accent}
        >
          <View style={{ backgroundColor: "#FEFEFE" }}>
            <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
              <View style={{ marginTop: 5, marginLeft: 5, marginRight: 5 }}>
                <ListViewReportDetail
                  document={this.props.data.NO_DOKUMEN}
                  type_priority={this.props.data.PRIORITY}
                  type_found={this.props.data.JENIS_TEMUAN}
                  employee={this.props.data.CREATE_BY}
                  found={this.props.data.TEMUAN}
                  location={this.props.data.LOKASI_TEXT}
                  caused={this.props.data.PENYEBAB}
                  recomendation={this.props.data.REKOMENDASI}
                  date={this.props.data.CREATE_AT}
                  status={this.props.data.STATUS}
                  image_bef={
                    GlobalConfig.SERVERHOST + "api" + this.props.data.FOTO_BEF
                  }
                  tlanjut={this.props.data.TINDAK_LANJUT}
                  image_aft={
                    GlobalConfig.SERVERHOST + "api" + this.props.data.FOTO_AFT
                  }
                />
              </View>
            </Card>
          </View>
        </Ripple>
      </View>
    );
  }
}

class DailyReportDetailList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      idUnsafe: "",
      isEmpty: false,
      noDokumen:''
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} navigation={this.props.navigation} />;

  componentDidMount() {
    
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        AsyncStorage.getItem("noDokumen").then(noDokumen => {
          this.setState({
            noDokumen:noDokumen
          })
        });
        AsyncStorage.getItem("idAndShift").then(idAndShift => {
            var temp = idAndShift.split("+");
            console.log(temp[0]);
            console.log(temp[1]);
            var idUnsafe = temp[0];
            this.setState({ idUnsafe: idUnsafe });
            this.setState({ isLoading: true });
            this.loadData(idUnsafe);
          });
      }
    );
  }

  onRefresh() {
    console.log('refreshing')
    this.setState({ isLoading: true }, function() {
        AsyncStorage.getItem("idAndShift").then(idAndShift => {
            var temp = idAndShift.split("+");
            console.log(temp[0]);
            console.log(temp[1]);
            var idUnsafe = temp[0];
            this.setState({ idUnsafe: idUnsafe });
            this.loadData(idUnsafe);
          });
    });
  }

  navigateToScreen(route, id_listReport) {
    // alert(id_listReport)
    AsyncStorage.setItem("list", JSON.stringify(id_listReport)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  loadData(id_Unsafe) {
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/list_view_detail";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID", id_Unsafe);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataSource: responseJson.data
          });
          this.setState({
            isLoading: false
          });
          if (this.state.dataSource.length() == 0) {
            this.setState({
              isEmpty: true
            });
          }
          console.log(this.state.dataSource);
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  render() {
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.isEmpty) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
            <FlatList
            data={this.state.dataSource}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isloading}
                onRefresh={this.onRefresh.bind(this)}
              />}
          />
        );
      }
    }

    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DailyReport")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Unsafe Detail Report</Title>
          </Body>

          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ flex:1,flexDirection:"column", marginTop: 10 }}>
          {/* <ScrollView> */}
              <Text style={{ fontSize: 12, color: colors.green01, marginLeft:10 }}>
                {this.state.noDokumen}
              </Text>
            {list}
          {/* </ScrollView> */}
        </View>

        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}

export default DailyReportDetailList;

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewRecapitulations from "../../components/ListViewRecapitulations";
import Ripple from "react-native-material-ripple";

class ListItem extends React.PureComponent {
  navigateToScreen(route, listRekapitulasi) {
    AsyncStorage.setItem("list", JSON.stringify(listRekapitulasi)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View>
        <Ripple
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.navigateToScreen("RecapitulationsDetail", this.props.data)
          }
          rippleColor={colors.accent}
        >
          <View style={{ backgroundColor: "#FEFEFE" }}>
            <Card
              style={{ paddingLeft: 10, paddingRight: 10, borderRadius: 10,marginLeft:10,marginRight:10 }}
            >
              <View
                style={{
                  marginTop: 10,
                  marginLeft: 10,
                  marginRight: 10,
                  marginBottom: 10
                }}
              >
                <ListViewRecapitulations
                  lokasi={this.props.data.NOTE}
                  noRekap={this.props.data.NO_REKAP}
                  type="Unsafe Condition"
                  tglTemuan={this.props.data.CREATE_AT}
                  status={this.props.data.STATUS}
                  batasWaktu={this.props.data.BATAS_WAKTU}
                />
              </View>
            </Card>
          </View>
        </Ripple>
      </View>
    );
  }
}

class Recapitulations extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isloading: true,
      searchText:''
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.setState({
          searchText:''
        })
        this.loadData();

      }
    );
  }

  _renderItem = ({ item }) => (
    <ListItem data={item} navigation={this.props.navigation} />
  );

  onRefresh() {
    console.log("refreshing");
    this.setState({ isloading: true }, function() {
      this.setState({
        searchText:''
      })
      this.loadData();

    });
  }

  searchData(){
    this.setState({
      isloading: true
    });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/recapitulation";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("search", this.state.searchText);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson.data.list_temuan));
          if(responseJson.status==200){
            this.setState({
            dataSource: responseJson.data,
            isloading: false
          });
            this.setState({
              isEmpty:false
            })
          }else{
            this.setState({
              dataSource:[],
              isloading:false
            })
            this.setState({
              isEmpty:true
            })
          }

        })
        .catch(error => {
          this.setState({
            dataSource:[],
            isloading: false
          });
          this.setState({
            isEmpty:true
          })
          console.log(error);
        });
    });
  }

  loadData() {
    this.setState({
      isloading: true
    });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/recapitulation";
      var formData = new FormData();
      formData.append("token", value);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log(responseJson)
          // alert(JSON.stringify(responseJson.data.list_temuan));
          if(responseJson.status==200){
            this.setState({
            dataSource: responseJson.data,
            isloading: false
          });
            this.setState({
              isEmpty:false
            })
          }else{
            this.setState({
              isloading:false
            })
            this.setState({
              isEmpty:true
            })
          }
        })
        .catch(error => {
          this.setState({
            isloading: false
          });
          this.setState({
            isEmpty:true
          })
          console.log(error);
        });
    });
  }

  render() {
    var list;
    if (this.state.isloading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.isEmpty) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list =
        <FlatList
        data={this.state.dataSource}
        renderItem={this._renderItem}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.isloading}
            onRefresh={this.onRefresh.bind(this)}
          />}
      />
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("SafetyMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Recapitulation</Title>
          </Body>

          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={styles.search}>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={styles.viewLeftHeader}>
              <Item style={styles.searchItem}>
                <Input
                  style={{ fontSize: 15 }}
                  placeholder="Type something here"
                  value={this.state.searchText}
                  onChangeText={text => this.setState({ searchText: text })}
                />
                <Icon
                  name="ios-search"
                  style={{ fontSize: 30, paddingLeft: 0 }}
                  onPress={() => this.searchData()}
                />
              </Item>
            </View>
          </View>
        </View>
        <View style={{ marginTop: 60,flex:1,flexDirection:'column' }}>{list}</View>
        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}
export default Recapitulations;

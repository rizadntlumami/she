import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  RefreshControl,
  Platform,
  TouchableOpacity,
  Dimensions,
  Alert
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Card,
  CardItem,
  Item,
  Input,
  Icon,
  Thumbnail
} from "native-base";
// import Icon from "react-native-vector-icons/FontAwesome";
import LinearGradient from "react-native-linear-gradient";
import GlobalConfig from "../../components/GlobalConfig";
import CustomFooter from "../../components/CustomFooter";

import styles from "../styles/ApprovalAPD";
import colors from "../../../styles/colors";
import Moment from "moment";
const helmet = require("../../../assets/images/helmet.png");

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, dataOrder) {
    AsyncStorage.setItem("idOrder", JSON.stringify(dataOrder)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    let title = "Status Order : ";
    let status, approver1, approver2, rejecter, noteReject,closer;

    if (this.props.data.reject_by != null) {
      status = "Rejected";
      rejecter =
        this.props.data.reject_by +
        " [" +
        Moment(this.props.data.reject_at).format('DD MMMM YYYY') +
        "]";
      noteReject = this.props.data.note_reject;
    } else {
      if (this.props.data.approve1_by != null) {
        if (this.props.data.approve2_by != null) {
          status = "Diapprove K3";
          approver1 =
            this.props.data.approve1_by +
            " [" +
            Moment(this.props.data.approve1_at).format('DD MMMM YYYY')+
            "]";
          approver2 =
            this.props.data.approve2_by +
            " [" +
            Moment(this.props.data.approve2_at).format('DD MMMM YYYY') +
            "]";
        } else {
          status = "Diapprove Atasan";
          approver1 =
            this.props.data.approve1_by +
            " [" +
            Moment(this.props.data.approve1_at).format('DD MMMM YYYY') +
            "]";
        }
      } else {
        status = "Belum Diapprove Atasan";
      }
    }

    // if (this.props.data.DETAIL[0].CLOSED_BY != null) {
    //   status = "Order Closed";
    //   approver1=undefined;
    //   approver2=undefined;
    //   closer =
    //     this.props.data.DETAIL[0].CLOSED_BY +
    //     " [" +
    //     this.props.data.DETAIL[0].CLOSED_AT +
    //     "]";
    // }

    return (
      <View>
        <Card style={{ marginLeft: 10, marginRight: 10, borderRadius:10}}>
          <CardItem
            button
            onPress={() =>
              this.navigateToScreen(
                "DetailMyOrderPersonalAPD",
                this.props.data.id
              )
              // alert("Pressed")
            }
            style={{borderRadius:10}}
          >
            <View style={{ flexDirection: "column", flex: 1 }}>
              <View style={{ flexDirection: "row", flex: 1 }}>
                <View style={{ flex: 1 }}>
                  <Text style={styles.cardtextKode}>
                    {this.props.data.kode_order}
                  </Text>
                </View>
                <View style={{ flex: 1 }}>
                  <Text note style={{ alignSelf: "flex-end", fontSize: 11 }}>
                    Order Date: {Moment(this.props.data.create_at).format('DD MMMM YYYY')}
                  </Text>
                </View>
              </View>
              <View style={{ flex: 1 }}>
                <View style={{ flexDirection: "row" }}>
                  <Text note style={styles.cardtext}>
                    {title}
                  </Text>
                  {status == "Belum Diapprove Atasan" && (
                    <Text
                      note
                      style={{
                        alignSelf: "flex-end",
                        fontSize: 11,
                        color: "white",
                        backgroundColor: "#787878",
                        paddingRight: 4,
                        paddingLeft: 4,
                        paddingBottom: 2,
                        paddingTop: 2,
                        borderRadius: 6
                      }}
                    >
                      {status}
                    </Text>
                  )}
                  {status == "Diapprove Atasan" && (
                    <Text
                      note
                      style={{
                        alignSelf: "flex-end",
                        fontSize: 11,
                        color: "white",
                        backgroundColor: "#ECA356",
                        paddingRight: 4,
                        paddingLeft: 4,
                        paddingBottom: 2,
                        paddingTop: 2,
                        borderRadius: 6
                      }}
                    >
                      {status}
                    </Text>
                  )}
                  {status == "Diapprove K3" && (
                    <Text
                      note
                      style={{
                        alignSelf: "flex-end",
                        fontSize: 11,
                        color: "white",
                        backgroundColor: "#1D84C6",
                        paddingRight: 4,
                        paddingLeft: 4,
                        paddingBottom: 2,
                        paddingTop: 2,
                        borderRadius: 6
                      }}
                    >
                      {status}
                    </Text>
                  )}
                  {status == "Rejected" && (
                    <Text
                      note
                      style={{
                        alignSelf: "flex-end",
                        fontSize: 11,
                        color: "white",
                        backgroundColor: "#FF0101",
                        paddingRight: 4,
                        paddingLeft: 4,
                        paddingBottom: 2,
                        paddingTop: 2,
                        borderRadius: 6
                      }}
                    >
                      {status}
                    </Text>
                  )}
                  {/* {status == "Order Closed" && (
                    <Text
                      note
                      style={{
                        alignSelf: "flex-end",
                        fontSize: 11,
                        color: "white",
                        backgroundColor: "#1BB394",
                        paddingRight: 4,
                        paddingLeft: 4,
                        paddingBottom: 2,
                        paddingTop: 2,
                        borderRadius: 6
                      }}
                    >
                      {status}
                    </Text>
                  )} */}
                </View>
                {rejecter != undefined && (
                  <View style={{ flexDirection: "column" }}>
                    <Text note style={styles.cardtext}>
                      Order telah direject oleh : {rejecter}
                    </Text>
                    <Text note style={styles.cardtext}>
                      Keterangan : {noteReject}
                    </Text>
                  </View>
                )}
                {(approver1 != undefined || approver2 != undefined) && (
                  <Text note style={styles.cardtext}>
                    Telah diapprove oleh :
                  </Text>
                )}
                {approver1 != undefined && (
                  <Text note style={styles.cardtext}>
                    - Atasan : {approver1}
                  </Text>
                )}
                {approver2 != undefined && (
                  <Text note style={styles.cardtext}>
                    - K3 : {approver2}
                  </Text>
                )}
                {/* {closer != undefined && (
                  <Text note style={styles.cardtext}>
                    - Order diclose oleh : {colser}
                  </Text>
                )} */}
              </View>
            </View>
          </CardItem>
        </Card>
      </View>
    );
  }
}

export default class MyOrderPersonalAPD extends Component {
  constructor() {
    super();
    this.state = {
      active: "true",
      dataSource: [],
      isloading: true,
      // searchApproval: "",
      isEmpty: false,
      badge: ''
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  componentDidMount() {
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + 'validasi';
      var formData = new FormData();
      formData.append("token", value)
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            name: responseJson.user.name,
            badge : responseJson.user.no_badge,
            isloading: false
          });
          this.loadData();
        })
        .catch(error => {
          console.log(error);
        });
    })
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.setState({
          searchApproval: ""
        });
        this.loadData();
      }
    );
  }

  loadData() {
    this.setState({
      isloading: true
    });

    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "orderapd/personal";
      var formData = new FormData();
      formData.append("token", value);
      // formData.append("my_order", null);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson));
          if (responseJson.status == 422) {
            this.setState({
              isloading: false
            });
            this.setState({
              isEmpty: true
            });
          } else {
            this.setState({
              dataSource: responseJson.data.filter(x => x.no_badge == this.state.badge),
              isloading: false,
              isEmpty: false
            });
          }
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  onRefresh() {
    console.log('refreshing')
    this.setState({ isloading: true }, function() {
      this.loadData();
    });
  }

  render() {
    that = this;
    var list;
    if (this.state.isloading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.dataSource=='') {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <ScrollView>
            <FlatList
              data={this.state.dataSource}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isloading}
                  onRefresh={this.onRefresh.bind(this)}
                />}
            />
          </ScrollView>
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
          <Left style={{flex:1}}>
          <Button
              transparent
              onPress={() => this.props.navigation.navigate("ApdMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>My Order</Title>
          </Body>

            <Right style={{flex:1}}/>

        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={styles.homeWrapper}>
          <Footer style={styles.tabHeight}>
            <FooterTab style={styles.tabfooter}>
              <Button active style={styles.tabfooter}>
                <View style={{height:'40%'}}></View>
                <View style={{height:'50%'}}>
                <Text>PERSONAL</Text>
                </View>
                <View style={{height:'20%'}}></View>
                <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.white}}></View>
              </Button>
              <Button
                onPress={() =>
                  this.props.navigation.navigate("MyOrderUnitKerjaAPD")
                }
              >
                <Text style={{color:'white', fontWeight:'bold'}}>UNIT KERJA</Text>
              </Button>
              <Button
                onPress={() =>
                  this.props.navigation.navigate("MyOrderPeminjamanAPD")
                }
              >
                <Text style={{color:'white', fontWeight:'bold'}}>PEMINJAMAN</Text>
              </Button>
            </FooterTab>
          </Footer>

          <View style={{ flex: 1, flexDirection: "column" }}>{list}</View>
        </View>
      </Container>
    );
  }
}

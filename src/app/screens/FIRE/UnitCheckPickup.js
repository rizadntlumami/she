import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail,
  Textarea,
  Picker,
  Form
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Ripple from "react-native-material-ripple";
import DatePicker from "react-native-datepicker";
import ListViewUnitCheck from "../../components/FireSystem/ListViewUnitCheck";

var that;
class ListItem extends React.PureComponent {
  navigateToScreen(route, listUnitcheck) {
    // alert(JSON.stringify(listUnitcheck))
    AsyncStorage.setItem("list", JSON.stringify(listUnitcheck)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.navigateToScreen("UnitCheckDetailPickup", this.props.data)
          }
          // onPress={() => alert("Maintenance")}
          rippleColor={colors.accent}
        >
          <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
            <ListViewUnitCheck
              imageUri={this.props.data.image}
              vehicleCode={this.props.data.vehicle_code}
              vehicleMerk={this.props.data.vehicle_merk}
              tahun={this.props.data.tahun}
              nomorRangka={this.props.data.nomor_rangka}
              sumNegative={this.props.data.sum_negative}
              lastInspection={this.props.data.last_inspection}
              expInspection={this.props.data.exp_inspection}
            />
          </View>
        </Ripple>
      </Card>
    );
  }
}

class UnitCheckPickup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      visibleDialog: false,
      filterDate: "",
      filterType: "",
      filterStatus: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  onRefresh() {
    console.log("refreshing");
    this.setState({ isLoading: true }, function() {
      this.loadData();
    });
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
        this.setState({ isLoading: true });
      }
    );
  }

  loadData() {
    this.setState({ visibleDialog: false });
      const url = GlobalConfig.SERVERHOST + "getListVehicle";
      var formData = new FormData();
      formData.append("type", 'PICKUP');

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataSource: responseJson.data,
            isLoading: false
          });
        })
        .catch(error => {
          console.log(error);
          this.setState({
            isLoading: false
          });
        });
  }

  onClickSearch() {
    this.setState({
      visibleDialog: true
    });
  }

  render() {
    that = this;
    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.dataSource == null) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
            <FlatList
              data={this.state.dataSource}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
              refreshControl={
                <RefreshControl
                  refreshing={this.state.isLoading}
                  onRefresh={this.onRefresh.bind(this)}
                />
              }
            />
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("FireMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body>
            <Title style={styles.textbody}>Unit Check</Title>
          </Body>
          {/* <Right>
                        <Button
                            transparent
                            onPress={() => this.onClickSearch()}
                        >
                            <Icon
                                name="ios-funnel"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Right> */}
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Footer>
          <FooterTab style={styles.tabfooter}>
            <Button active style={styles.tabfooter}>
              <View style={{ height: "40%" }} />
              <View style={{ height: "50%" }}>
                <Text style={styles.textbody}> Pick Up </Text>
              </View>
              <View style={{ height: "20%" }} />
              <View
                style={{
                  borderWidth: 2,
                  marginTop: 2,
                  height: 0.5,
                  width: "100%",
                  borderColor: colors.white
                }}
              />
            </Button>
            <Button
              onPress={() => this.props.navigation.navigate("UnitCheckPMK")}
            >
              <Text style={{ color: "white", fontWeight: "bold" }}> PMK </Text>
            </Button>
          </FooterTab>
        </Footer>

        <View style={{ marginTop: 10,flex:1,flexDirection:'column' }}>
          {list}
        </View>
      </Container>
    );
  }
}

export default UnitCheckPickup;

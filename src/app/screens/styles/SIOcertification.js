import { StyleSheet } from "react-native";
import iPhoneSize from "../../../helpers/utils";
import colors from "../../../styles/colors";
import { Row } from "native-base";

let labelTextSize = 12;
let weatherTextSize = 18;
let weatherTextSmSize = 14;
let header = 15;
let headingTextSize = 28;
if (iPhoneSize() === "small") {
  labelTextSize = 10;
  headingTextSize = 24;
  weatherTextSize = 16;
  weatherTextSmSize = 12;
}

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    display: "flex",
  },
  btnQTYLeft:{
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    width:30,
    height:30,
    backgroundColor: colors.green01,
    marginTop: 0,
  },
  btnQTYRight:{
    borderTopRightRadius: 10,
    borderBottomRightRadius: 10,
    width:30,
    height:30,
    backgroundColor: colors.green01,
    marginTop: 0,
  },
  btnWishlist:{
    marginLeft:5,
    paddingRight:-30,
    paddingLeft:6,
    marginTop:-12,
    marginBottom:-15,
    backgroundColor:colors.graydar
  },
  facebookButtonIconOrder: {
    color: colors.white,
    fontSize: 20,
    paddingLeft: 18,
  },
  facebookButtonIconQTY: {
    color: colors.white,
    fontSize: 20,
    paddingTop:5,
    width:30,
    height:30,
  },
  facebookButtonIconOrderOrder: {
    color: colors.green01,
    fontSize: 20,
    paddingLeft: 18,
  },
  facebookButtonIconOrder2: {
    color: colors.white,
    fontSize: 20
  },
  facebookButtonIconDangerous: {
    paddingTop: 8,
    paddingLeft: -2,
    paddingRight: 0,
    color: colors.red,
    fontSize: 25
  },
  facebookButtonIcon: {
    color: colors.green01,
    fontSize: 30
  },
  facebookIcon: {
    color: colors.black,
    fontSize: 30,
    textAlign: 'center'
  },
  header: {
    backgroundColor: colors.green01
  },
  tabfooter: {
    backgroundColor: colors.green01
  },
  textbody: {
    justifyContent: 'center',
    fontWeight: 'bold',
    color: colors.white
  },
  homeWrapper: {
    flex: 1,
    display: "flex",
  },
  weatherContent: {
    flexDirection: "row",
    marginBottom: 15,
    borderBottomColor: colors.lightBlack,
    borderBottomWidth: 1,
    marginTop: 20,
    marginLeft: 20,
    marginRight: 20,
  },
  newsContent: {
    flexDirection: "row"
  },
  weatherText: {
    fontFamily: "Montserrat-Medium",
    fontSize: weatherTextSize,
    color: colors.lightBlack,
    fontWeight: "300",
    fontSize: headingTextSize,
    marginBottom: 5,
  },
  weatherTextSm: {
    fontFamily: "Montserrat-Light",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 10
  },
  newsText: {
    fontFamily: "Montserrat-Regular",
    fontSize: weatherTextSmSize,
    color: colors.lightBlack,
    fontWeight: "300",
    marginBottom: 5,
    marginLeft: 20
  },
  news: {
    height: 150,
  },
  search: {
    position: 'absolute',
    // left: 5,
    // right: 5,
    top: 60,
    height: 10,
    flexDirection: 'row',
  },
  searchItem: {
    borderWidth: 2,
    borderColor: colors.green01,
    paddingHorizontal: 5,
    borderRadius: 20,
    marginTop: 60,
  },
  font: {
    fontSize: header,
  },
  viewLeftHeader: {
    width: 350,
  },
  itemMenu: {
    flexDirection: "row",
    padding: 1,
    marginTop: 4,
    justifyContent: 'center'
  },
  itemAPDLeft: {
    width: 90
  },

  loginButton: {
    margin: 30,
    height: '75%',
  },
  apdorderimage: {
    alignItems: 'center',
    width: 90,
    height: 90,
    marginTop: 2,
    marginLeft: 1,
    marginRight: 2,
  },
  apdorderimg: {
    width: 60,
    height: 60,
  },
  viewKonfirmasiLeft: {
    width: 230,
  },
  viewKonfirmasiRight: {
    width: 150,
  },
  viewKonfirmasiLeftTroli: {
    width: 200,
  },
  viewKonfirmasiRightTroli: {
    width: 100,
  },
  viewName: {
    fontSize: 12,
    color: '#00b300'
  },
  viewMerk: {
    fontSize: 12,
  },
  viewQTY: {
    fontSize: 12,
  },
  viewLeft: {
    width: 100,
  },
  viewLeftHeader: {
    width: 300,
    paddingRight: 0,
  },
  viewRightHeader: {
    width: 80,
  },
  viewLeftOrder: {
    width: 70,
    fontSize: 12,
  },
  viewRightOrder: {
    width: 150,
  },
  viewCenter: {
    width: 120,
    fontSize: 15
  },
  viewRight: {
    width: 90,
    textAlign: 'right',
    fontSize: 15
  },
  tabfooter: {
    backgroundColor: colors.green01
  },
  roundedBtn: {
    paddingTop: 5,
    padding: 5,
    justifyContent: 'center',
    top: 45,
    flexDirection: "row"
  }
});

export default styles;

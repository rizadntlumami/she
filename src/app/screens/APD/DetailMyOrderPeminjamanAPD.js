import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Alert
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Card,
  CardItem,
  Form,
  Textarea,
  Icon
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import LinearGradient from "react-native-linear-gradient";
import GlobalConfig from "../../components/GlobalConfig";

import styles from "../styles/ApprovalAPD";
import colors from "../../../styles/colors";
import Moment from "moment";
const helmet = require("../../../assets/images/helmet.png");

var that;
class ListItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selected2: undefined
    };
  }

  onValueChange2(value) {
    this.setState({
      selected2: value
    });
  }

  render() {
    const { navigate } = that.props.navigation;
    return (
      <View>
        <Card style={{ marginBottom: 10, borderRadius:10 }}>
          <CardItem style={{borderRadius:10}}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={styles.viewLeftHeader2}>
                <Text style={styles.cardtext}>APD Name</Text>
                <Text style={styles.cardtext}>Merk</Text>
                {/* <Text style={styles.cardtext}>Size</Text> */}
                <Text style={styles.cardtext}>Quantity</Text>
                
                {this.props.data.reject_by!=null&&(
                  <View>
                    <Text style={styles.cardtext}>Reject</Text>
                    <Text style={styles.cardtext}>Note Reject</Text>
                  </View>
                )}
                {/* <Text style={styles.cardtext}>Released</Text>
                <Text style={styles.cardtext}>Returned</Text> */}

              </View>
              <View>
                <Text style={styles.cardtext}>: {this.props.data.nama_apd}</Text>
                <Text style={styles.cardtext}>: {this.props.data.merk}</Text>
                {/* <Text style={styles.cardtext}>: {this.props.data.SIZE}</Text> */}
                <Text style={styles.cardtext}>: {this.props.data.jumlah}</Text>
                
                {this.props.data.reject_by!=null&&(
                  <View>
                    <Text style={styles.cardtext}>: {this.props.data.reject_by} [{Moment(this.props.data.reject_at).format('DD MMMM YYYY')}]</Text>
                    <Text style={styles.cardtext}>: {this.props.data.note_reject}</Text>
                  </View>
                )}
                {/* <Text style={styles.cardtext}>: {this.props.data.JUMLAH_RELEASE}/{this.props.data.JUMLAH}</Text>
                <Text style={styles.cardtext}>: {this.props.data.JUMLAH_RETUR}</Text> */}
              </View>
            </View>
          </CardItem>
        </Card>
      </View>
    );
  }
}

export default class DetailMyOrderPeminjamanAPD extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      listDetail: [],
      isloading: true
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item, index }) => (
    <ListItem data={item} index={index} parentFlatList={this} />
  );

  componentDidMount() {
    AsyncStorage.getItem("idOrder").then(idOrder => {
      this.loadData(idOrder);
    });
  }

  loadData(idOrder) {
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "pinjamapd/getid";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("id", idOrder);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson));
          this.setState({
            dataSource: responseJson.data,
            listDetail: responseJson.data[0].get_detail_pinjam,
            isloading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  render() {
    that = this;
    let status, approver1, approver2, rejecter, noteReject, statusPinjam;
    if (!this.state.isloading){
      if (this.state.dataSource[0].reject_by != null) {
        status = "Rejected";
        statusPinjam = "Order Closed"
        rejecter =
          this.state.dataSource[0].reject_by+
          " [" +
          Moment(this.state.dataSource[0].reject_at).format('DD MMMM YYYY') +
         "]";
        noteReject = this.state.dataSource[0].note_reject;
      } else {
        if (this.state.dataSource[0].approve1_by != null) {
          if (this.state.dataSource[0].approve2_by != null) {
            status = "Diapprove K3";
            statusPinjam = "Belum Dikembalikan";
            approver1 =
              this.state.dataSource[0].approve1_by +
              " [" +
              Moment(this.state.dataSource[0].approve1_at).format('DD MMMM YYYY') +
              "]";
            approver2 =
              this.state.dataSource[0].approve2_by +
              " [" +
              Moment(this.state.dataSource[0].approve2_at).format('DD MMMM YYYY') +
              "]";
          } else {
            status = "Diapprove Atasan";
            statusPinjam = "Belum Dikembalikan";
            approver1 =
              this.state.dataSource[0].approve1_by +
              " [" +
              Moment(this.state.dataSource[0].approve1_at).format('DD MMMM YYYY') +
              "]";
          }
        } else {
          status = "Belum Diapprove Atasan";
        }
      }
      if (this.state.dataSource[0].retur_at != null) {
        status = "Order Closed";
        approver1=undefined;
        statusPinjam = "Telah Dikembalikan"
        // closer =
        //   this.props.data.DETAIL[0].CLOSED_BY +
        //   " [" +
        //   this.props.data.DETAIL[0].CLOSED_AT +
        //   "]";
      }
  }
    return this.state.isloading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() =>
                this.props.navigation.navigate("MyOrderPeminjamanAPD")
              }
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIcon}
              />
            </Button>
          </Left>
          <Body style={{flex:4,alignItems:'center'}}>
            <Title style={styles.textbody}>Detail Order</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View>
        <Card style={{ marginBottom: 15, borderWidth:0, borderRadius:10}}>
          <CardItem style={{ borderWidth:0, borderRadius:10 }}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={{ flex: 1 }}>
                <Text style={styles.cardtext}>Tanggal Order</Text>
                <Text style={styles.cardtext}>Tanggal Pinjam</Text>
                <Text style={styles.cardtext}>Batas Pinjam</Text>
                <Text style={styles.cardtext}>Kode Order / SKU</Text>
                <Text style={styles.cardtext}>Status Order</Text>
                <Text style={styles.cardtext}>Status Peminjaman</Text>
                <Text style={styles.cardtext}>Approve</Text>
              </View>
              <View style={{ flex: 2 }}>
                <Text style={styles.cardtext}>: {Moment(this.state.dataSource[0].create_at).format('DD MMMM YYYY')}</Text>
                <Text style={styles.cardtext}>: {Moment(this.state.dataSource[0].tanggal_pinjam).format('DD MMMM YYYY')}</Text>
                <Text style={styles.cardtext}>: {Moment(this.state.dataSource[0].retur_date).format('DD MMMM YYYY')}</Text>
                <Text style={styles.cardtext}>: {this.state.dataSource[0].kode_pinjam}</Text>
                <Text style={styles.cardtext}>: {status}</Text>
                {statusPinjam == "Belum Dikembalikan" && (
                    <Text
                    style={styles.cardtext}
                    >
                     : {statusPinjam}
                    </Text>
                  )}
                  {statusPinjam == "Order Closed" && (
                    <Text
                    style={styles.cardtext}
                    >
                     : {statusPinjam}
                    </Text>
                  )}
                  {statusPinjam == "Telah Dikembalikan" && (
                    <Text
                    style={styles.cardtext}
                    >
                     : {statusPinjam}
                    </Text>
                  )}
                
                <Text style={styles.cardtext}>: {approver1}</Text>
              </View>
            </View>
          </CardItem>
        </Card>
      </View>
          {/* <View style={{ alignItems: "baseline", borderWidth: 5 }}>
          <View style={{flex:1, flexDirection: "row" }}>
            <View style={{flex:1}}>
              <Text style={styles.cardtext}>Tanggal Request</Text>
              <Text style={styles.cardtext}>Kode Order / SKU</Text>
            </View>
            <View style={{flex:1}}>
              <Text style={styles.cardtext}>: {this.state.dataSource.CREATE_AT}</Text>
              <Text style={styles.cardtext}>: {this.state.dataSource.KODE_ORDER}</Text>
            </View>
          </View>
        </View> */}
        <View style={styles.homeWrapper}>
          <Content style={{ marginLeft: 20, marginRight: 20 }}>
            <FlatList
              data={this.state.listDetail}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          </Content>
        </View>
      </Container>
    );
  }
}

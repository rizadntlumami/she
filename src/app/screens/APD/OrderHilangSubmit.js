import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Alert
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  Button,
  FooterTab,
  Text,
  Left,
  Right,
  Body,
  Card,
  CardItem,
  Form,
  Textarea,
  Icon
} from "native-base";

import {
  DocumentPicker,
  DocumentPickerUtil
} from "react-native-document-picker";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";

import Moment from "moment";
import LinearGradient from "react-native-linear-gradient";
import GlobalConfig from "../../components/GlobalConfig";

import styles from "../styles/OrderSubmit";
import colors from "../../../styles/colors";

var that;
class ListItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selected2: undefined,
      fileUri: "",
      fileType: "",
      fileName: "",
      fileSize: "",

    };
  }

  onValueChange2(value) {
    this.setState({
      selected2: value
    });
  }

  handleChange() {
    //Opening Document Picker
    DocumentPicker.show(
      {
        filetype: [DocumentPickerUtil.allFiles()]
        //All type of Files DocumentPickerUtil.allFiles()
        //Only PDF DocumentPickerUtil.pdf()
        //Audio DocumentPickerUtil.audio()
        //Plain Text DocumentPickerUtil.plainText()
      },
      (error, res) => {
        this.setState({ fileUri: res.uri });
        this.setState({ fileType: res.type });
        this.setState({ fileName: res.fileName });
        this.setState({ fileSize: res.fileSize });

        console.log("res : " + JSON.stringify(res));
        console.log("URI : " + res.uri);
        console.log("Type : " + res.type);
        console.log("File Name : " + res.fileName);
        console.log("File Size : " + res.fileSize);
      }
    );
  }

  orderHilang = () => {
    this.props.parentFlatList.setState({
      visibleDialogSubmit:true
    })
    // alert(JSON.stringify(this.state.id));
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "hilang";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("id", this.props.data.id);
      formData.append("file", {
        uri: this.state.fileUri,
        type: this.state.fileType,
        name: this.state.fileName
      });
      //alert(JSON.stringify(url));

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson));
          if (responseJson.status == 200) {
            this.props.parentFlatList.setState({
              visibleDialogSubmit:false
            })
            Alert.alert("Success", "Aduan Kehilangan Sukses", [
              {
                text: "Okay"
              }
            ]);
            this.props.navigation.navigate("OrderHilang");
          } else {
            this.props.parentFlatList.setState({
              visibleDialogSubmit:false
            })
            Alert.alert("Error", "Aduan Kehilangan Gagal", [
              {
                text: "Okay"
              }
            ]);
          }
        })
        .catch((error) => {
          Alert.alert('Error', 'Check Your Internet Connection', [{
              text: 'Okay'
          }])
            console.log(error)
        });
    });
  };

  render() {
    const { navigate } = that.props.navigation;

    return this.state.isloading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <CardItem
          style={{
            borderRadius: 0,
            marginTop: 4,
            backgroundColor: colors.gray
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              marginTop: 4,
              backgroundColor: colors.gray
            }}
          >
            {/* <View style={styles.viewKonfirmasiLeft}> */}
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 1 }}>
                <Text style={{fontSize:12}}>Kode Order</Text>
                <Text style={{fontSize:12}}>APD Name</Text>
                <Text style={{fontSize:12}}>Merk</Text>
                <Text style={{fontSize:12}}>Released Date</Text>
                {/* <Text style={{fontSize:12}}>Expired Days</Text> */}
              </View>
              <View style={{ flex: 2 }}>
                <Text style={styles.viewKode}>
                  : {this.props.data.nomor_order}
                </Text>
                <Text style={styles.viewNama}>: {this.props.data.nama_apd}</Text>
                <Text style={styles.viewMerk}>: {this.props.data.merk}</Text>
                <Text style={styles.viewMerk}>: {Moment(this.props.data.release_at).format('DD MMMM YYYY')}</Text>
                {/* <Text style={styles.viewMerk}>
                  : {this.props.data.EXP_DAYS} Days
                </Text> */}
              </View>
              {/* <Text style={styles.viewKode}>
                          {this.props.data.KODE_ORDER}
                        </Text>
                        <Text style={styles.viewNama}>
                          {this.props.data.NAMA}
                        </Text>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                            <View style={{width:20}}>
                                <Text style={styles.viewMerk}>
                                  {this.props.data.MERK}
                                </Text>
                            </View>
                            <View>
                                <Text style={styles.viewMerk}>
                                  Exp : {this.props.data.EXP_DAYS} Days
                                </Text>
                            </View>
                        </View> */}
            </View>
          </View>
        </CardItem>

        <View style={{ marginLeft: 10, marginRight: 10 }}>
          <Text style={{ fontSize: 10, marginTop: 5 }}>
            Upload File PDF max : 500 kb
          </Text>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={styles.viewLeftBtnUploadHilang}>
                <Button
                  style={{
                    borderRadius: 5,
                    backgroundColor: "#00b300",
                    marginTop: 10
                  }}
                >
                  <Text
                    style={{
                      fontWeight: "bold",
                      color: "#ffffff",
                      marginLeft: 10,
                      marginRight: 10
                    }}
                    onPress={this.handleChange.bind(this)}
                  >
                    Upload
                  </Text>
                </Button>
              </View>
              <View style={styles.viewRightBtnUploadHilang}>
                <Text style={{ fontSize: 12, marginTop: 10 }}>
                  {this.state.fileName}
                </Text>
              </View>
            </View>
          </View>

          <View style={styles.Contentsave}>
            <Button
              block
              style={{
                height: 45,
                marginTop: 300,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 20,
                borderWidth: 1,
                backgroundColor: "#00b300",
                borderColor: "#00b300",
                borderRadius: 4
              }}
              onPress={() => this.orderHilang()}
            >
              <Text style={styles.buttonText}>Submit</Text>
            </Button>
          </View>
        </View>
      </View>
    );
  }
}

export default class OrderHilangSubmit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isloading: true,
      deletedRowKey: null,
      visibleDialogSubmit:false,
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item, index }) => (
    <ListItem data={item} index={index} parentFlatList={this} navigation={this.props.navigation} />
  );

  componentDidMount() {
    AsyncStorage.getItem("idHilang").then(idHilang => {
      // this.setState({ id: value });
      this.loadData(idHilang);
    });
    // AsyncStorage.getItem("token").then(value => {
    //   this.setState({ token: value });
    // });
  }

  loadData(idHilang) {
    AsyncStorage.getItem("token").then(value => {
      //alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "releaselist/personal/getid";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("id", idHilang);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          //alert(JSON.stringify(responseJson));
          this.setState({
            dataSource: responseJson.data,
            isloading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  render() {
    that = this;
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("OrderHilang")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIcon}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Order Hilang</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={styles.homeWrapper}>
          <Content style={{}}>
          {this.state.isloading ? (
            <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <ActivityIndicator size="large" color="#330066" animating />
            </View>
          ) :(
            <FlatList
              data={this.state.dataSource}
              renderItem={this._renderItem}
              keyExtractor={(item, index) => index.toString()}
            />
          )}

          </Content>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialogSubmit}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogTitle={<DialogTitle title="Creating Order.." />}
          >
            <DialogContent>
              {<ActivityIndicator size="large" color="#330066" animating />}
            </DialogContent>
          </Dialog>
        </View>
      </Container>
    );
  }
}

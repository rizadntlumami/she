import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl,
  Dimensions
} from "react-native";
import {
  Thumbnail,
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";

import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewAccident from "../../components/ListViewAccident";
import Ripple from "react-native-material-ripple";

class ListItem extends React.PureComponent {
  navigateToScreen(route, id_Accident) {
    // alert(id_Unsafe)
    AsyncStorage.setItem("list", JSON.stringify(id_Accident)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <View>
        <Ripple
          style={
            {
              // flex: 2,
              // justifyContent: "center",
              // alignItems: "center"
            }
          }
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.navigateToScreen("AccidentReportDetail", this.props.data)
          }
          rippleColor={colors.accent}
        >
          <View style={{ backgroundColor: "#FEFEFE" }}>
            <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
              <View style={{ marginTop: 5, marginLeft: 5, marginRight: 5 }}>
                <ListViewAccident
                  nopeg={this.props.data.BADGE_VICT}
                  namapeg={this.props.data.NAME_VICT}
                  position={this.props.data.UK_VICT_TEXT}
                  statpos={this.props.data.STAT_VICT_TEXT}
                  post={this.props.data.POS_TEXT}
                  image_bef={
                    GlobalConfig.SERVERHOST + "api" + this.props.data.PICT_BEFORE
                  }
                  date={this.props.data.DATE_ACD}
                  status={this.props.data.STATUS_REPORT}
                />
              </View>
            </Card>
          </View>
        </Ripple>
      </View>
    );
  }
}

class AccidentReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      isEmpty: false,
      refreshing: false,
      searchText:''
    };
  }

  static navigationOptions = {
    header: null
  };

  onRefresh = () => {
    this.setState({ isloading: true }, function () {
      this.setState({
        searchText:''
      })
      this.loadData();
    });
  };

  _renderItem = ({ item }) => <ListItem data={item} navigation={this.props.navigation} />;

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.setState({
          searchText:''
        })
        this.loadData();
      }
    );
  }

  searchData(){
    this.setState({
      isloading: true
    });
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + "api/v_mobile/safety/accident/view";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("search", this.state.searchText);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log(responseJson)
          // alert(JSON.stringify(responseJson.data.list_temuan));
          if (responseJson.status == 200) {
            this.setState({
              dataSource: responseJson.data,
              isloading: false,
              isEmpty: false
            });
          } else {
            this.setState({
              dataSource:[],
              isloading: false
            });
            this.setState({
              isEmpty: true
            });
          }
        })
        .catch(error => {
          console.log(error);
          this.setState({
            dataSource:[],
            isloading: false
          });
        });
    });
  }

  loadData() {
    this.setState({
      isloading: true
    });
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + "api/v_mobile/safety/accident/view";
      var formData = new FormData();
      formData.append("token", value);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson.data.list_temuan));
          if (responseJson.status == 200) {
            this.setState({
              dataSource: responseJson.data,
              isloading: false,
              isEmpty: false
            });
          } else {
            this.setState({
              isloading: false
            });
            this.setState({
              isEmpty: true
            });
          }
        })
        .catch(error => {
          console.log(error);
          this.setState({
            isloading: false
          });
        });
    });
  }

  render() {
    var list;
    if (this.state.isloading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.isEmpty) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <FlatList
            data={this.state.dataSource}
            renderItem={this._renderItem}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isloading}
                onRefresh={this.onRefresh.bind(this)}
              />}
          />
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("SafetyMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={{color:colors.white}}
              />
            </Button>
          </Left>
          <Body style={{ flex:3, alignItems:'center' }}>
            <Title style={styles.textbody}>Accident Report</Title>
          </Body>

          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={styles.search}>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={styles.viewLeftHeader}>
              <Item style={styles.searchItem}>
                <Input
                  style={{ fontSize: 15 }}
                  placeholder="Type something here"
                  value={this.state.searchText}
                  onChangeText={text => this.setState({ searchText: text })}
                />
                <Icon
                  name="ios-search"
                  style={{ fontSize: 30, paddingLeft: 0 }}
                  onPress={() => this.searchData()}
                />
              </Item>
            </View>
          </View>
        </View>
        {/* <Content style={{ marginTop: 60 }}> */}

          
          <View style={{ flex: 1, flexDirection: "column",marginTop: 60 }}>{list}</View>

        {/* </Content> */}
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: colors.green01, marginBottom: ((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?80:50 }}
          position="bottomRight"
          onPress={() => this.props.navigation.navigate("CreateAccidentReport")}
        >
          <Icon name="ios-add" style={{ fontSize: 50, fontWeight: "bold",paddingTop: Platform.OS === 'ios' ? 25:null, }} />
        </Fab>
        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}

export default AccidentReport;

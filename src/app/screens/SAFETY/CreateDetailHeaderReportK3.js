import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Label,
    Item,
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DateTimePicker from 'react-native-datepicker';
import ImagePicker from "react-native-image-picker";
import Ripple from "react-native-material-ripple";
import CheckBox from 'react-native-check-box';

class ListItem extends React.PureComponent {
  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.props.setSelectedUnitKerjaLabel(this.props.data.muk_nama)
          }
          rippleColor={colors.accent}
        >
          <CardItem
            style={{
              borderRadius: 0,
              marginTop: 4,
              backgroundColor: colors.gray
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 4,
                backgroundColor: colors.gray
              }}
            >
              <View >
                <Text style={{fontSize:12}}>{this.props.data.muk_kode} - {this.props.data.muk_nama}</Text>
              </View>
            </View>
          </CardItem>
        </Ripple>
      </View>
    );
  }
}

export default class CreateDetailHeaderReportK3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      dataHeader: [],
      isLoading: true,
      idUnsafe:'',
      listUnitKerja:[],
      listVendor:[],
      listKategori:[],
      status:'OPEN',
      selectedType: 'Unsafe Action',
      penyebab:'',
      rekomendasi:'',
      tindakLanjut:'',
      pickedImage: '',
      uri: '',
      fileType:'',
      fileName: '',
      pickedImage2: '',
      uri2: '',
      fileType2:'',
      fileName2: '',
      isChecked:false,
      value:'1',
      visibleLoadingUnitKerja:false,
      visibleSearchListUnitKerja:false,
      selectedUnitKerjaLabel:'',
      searchWordUnitKerja:'',
      visibleDialogSubmit:false
    };
  }


  pickImageHandler = () => {
      ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
          if (res.didCancel) {
              console.log("User cancelled!");
          } else if (res.error) {
              console.log("Error", res.error);
          } else {
              this.setState({
                  pickedImage: res.uri,
                  uri: res.uri,
                  fileType:res.type
              });

          }
      });
  }

  pickImageHandler2 = () => {
      ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
          if (res.didCancel) {
              console.log("User cancelled!");
          } else if (res.error) {
              console.log("Error", res.error);
          } else {
              this.setState({
                  pickedImage2: res.uri ,
                  uri2: res.uri,
                  fileType2:res.type
              });

          }
      });
  }

  static navigationOptions = {
      header: null
  };

  _renderItem = ({ item }) => (
      <ListItem data={item}></ListItem>
  )
  componentDidMount() {
      AsyncStorage.getItem('id').then((idUnsafe) => {
          this.setState({
            idUnsafe: idUnsafe
          });
          this.loadData();
      })
      this.loadUnitKerja();
      this.loadVendor();
      this.loadKategori();
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
        this.loadData();
      });
  }

  loadData() {

  }
  loadUnitKerja(){
    this.setState({
        visibleLoadingUnitKerja:true
    })
    AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/unit_kerja';
        var formData = new FormData();
        formData.append("token", value)
        formData.append("search", this.state.searchWordUnitKerja)
        formData.append("limit", 10)
        formData.append("plant", 0)
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    visibleLoadingUnitKerja:false
                })
                this.setState({
                    listUnitKerja: responseJson,
                    isloading: false
                });
            })
            .catch((error) => {
              this.setState({
                visibleLoadingUnitKerja:false
              })
                console.log(error)
            })
    })
  }

  loadVendor(){
    AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/vendor';
        var formData = new FormData();
        formData.append("token", value)
        formData.append("search", "")
        formData.append("limit", "")
        formData.append("plant", "")
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listVendor: responseJson,
                    isloading: false
                });
            })
            .catch((error) => {
                console.log(error)
            })
    })
  }

  loadKategori(){
    AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/category';
        var formData = new FormData();
        formData.append("token", value)
        formData.append("search", 0)
        formData.append("limit", 250)
        formData.append("plant", 0)
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listKategori: responseJson,
                    isloading: false
                });
            })
    })
  }

  createDetailHeader() {
  if (this.state.uri == '') {
    alert('Masukkan Foto Temuan');
  }
  else if (this.state.reportTime == null) {
    alert('Masukkan Waktu Report');
  }
  else if (this.state.selectedUnitKerjaLabel == '') {
    alert('Masukkan Unit Kerja');
  }
  else if (this.state.selectedKategori == '') {
    alert('Masukkan Kategori');
  }
  else if (this.state.aktifitas == null) {
    alert('Masukkan Aktifitas Temuan');
  }
  else if (this.state.potensi == null) {
    alert('Masukkan Potensi Bahaya');
  }
  else {
    if(this.state.isChecked == false){
      this.setState({
          value: 1,
      });
    } else {
      this.setState({
          value: 0,
      });
    }
    AsyncStorage.getItem("token").then(value => {
      this.setState({
        visibleDialogSubmit:true
      })
      var url = GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe_n/unsafe_detail_list/create";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID_UNSAFE_REPORT", this.state.idUnsafe);
      formData.append("ACTIVITY_DESCRIPTION", this.state.aktifitas);
      formData.append("UNSAFE_TYPE", this.state.selectedType);
      if (this.state.penyebab!='') {
        formData.append("TROUBLE_MAKER", this.state.penyebab);
      } else {
        formData.append("TROUBLE_MAKER", '');
      }
      formData.append("POTENTIAL_HAZARD", this.state.potensi);
      if (this.state.rekomendasi!='') {
        formData.append("RECOMENDATION", this.state.rekomendasi);
      } else {
        formData.append("RECOMENDATION", '');
      }
      if (this.state.tindakLanjut!='') {
        formData.append("FOLLOW_UP", this.state.tindakLanjut);
      } else {
        formData.append("FOLLOW_UP", '');
      }
      formData.append("IS_SMIG", this.state.value);
      formData.append("UNIT_CODE", null);
      formData.append("UNIT_NAME", this.state.selectedUnitKerjaLabel);
      formData.append("O_CLOCK_INCIDENT", this.state.reportTime);
      formData.append("ID_CATEGORY", this.state.selectedKategori);
      formData.append("STATUS", this.state.status);
      if (this.state.uri!='') {
        formData.append("FOTO_TEMUAN", {
          uri: this.state.uri,
          type: this.state.fileType,
          name: 'image'
      });
      }
      if (this.state.uri2!='') {
        formData.append("FOTO_FOLLOW_UP", {
          uri: this.state.uri2,
          type: this.state.fileType2,
          name: 'image'
      });
      }

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        if (response.Status == 200) {
          this.setState({
            visibleDialogSubmit:false
          })
          Alert.alert('Success', 'Create Detail Success', [{
            text: 'Okay'
          }])
          this.props.navigation.navigate('DailyReportK3Detail')
        } else {
          this.setState({
            visibleDialogSubmit:false
          })
          Alert.alert('Error', 'Create Detail Failed', [{
            text: 'Okay'
          }])
        }
      })
      .catch((error)=>{
        this.setState({
          visibleDialogSubmit:false
        })
        Alert.alert('Error', 'Create Detail Failed', [{
          text: 'Okay'
        }])
        console.log(error)
      })
  })
  }
}

  onChangeUnitKerja(text){
    this.setState({
      searchWordUnitKerja:text
    })
    this.loadUnitKerja();
    // this.setState({
    //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
    // })
  }

  onClickSearchUnitKerja(){
    console.log('masuk')
    if (this.state.visibleSearchListUnitKerja){
      this.setState({
        visibleSearchListUnitKerja:false
      })
    }else{
      this.setState({
        visibleSearchListUnitKerja:true
      })
    }
  }

  setSelectedUnitKerjaLabel(text){
    this.setState({
      selectedUnitKerjaLabel:text,
      visibleSearchListUnitKerja:false
    })
  }

  _renderItem = ({ item }) => <ListItem data={item} setSelectedUnitKerjaLabel={text => this.setSelectedUnitKerjaLabel(text)} />;

  render() {
    let listUnitKerja = this.state.listUnitKerja.map( (s, i) => {
      return <Picker.Item key={i} value={s.muk_nama} label={s.muk_nama} />
    });

    let listVendor = this.state.listVendor.map( (s, i) => {
      return <Picker.Item key={i} value={s.NAME1} label={s.NAME1} />
    });

    let listKategori = this.state.listKategori.map( (s, i) => {
      return <Picker.Item key={i} value={s.ID} label={s.DESKRIPSI} />
    });

    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DailyReportK3DetailList")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:4,alignItems:'center'}}>
            <Title style={styles.textbody}>Create Detail Daily Report K3</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={{ flex: 1 }}>
        <Content style={{ marginTop: 0 }}>
          <View style={{ backgroundColor: '#FEFEFE' }}>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                    <View style={{paddingBottom:5}}>
                      <Text style={styles.titleInput}>Upload Foto Temuan</Text>
                    </View>
                    <View>
                      <Ripple
                        style={{
                          flex: 2,
                          justifyContent: "center",
                          alignItems: "center"
                        }}
                        rippleSize={176}
                        rippleDuration={600}
                        rippleContainerBorderRadius={15}
                        onPress={this.pickImageHandler}
                        rippleColor={colors.accent}
                      >
                      <View style={styles.placeholder}>
                          <Image source={{uri:this.state.pickedImage}} style={styles.previewImage}/>
                      </View>
                      </Ripple>
                    </View>
                </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
              <View style={{ flex: 1}}>
                <View>
                  <Text style={styles.titleInput}>Status *</Text>
                  <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                      <Picker
                          mode="dropdown"
                          iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                          style={{ width: '100%', height:35}}
                          placeholder="Select Status ..."
                          placeholderStyle={{ color: "#bfc6ea" }}
                          placeholderIconColor="#007aff"
                          selectedValue={this.state.status}
                          onValueChange={(itemValue) => this.setState({status:itemValue})}>
                          <Picker.Item label="Open"  value="OPEN" Sele/>
                          <Picker.Item label="Close"  value="CLOSE"/>
                      </Picker>
                  </Form>
                </View>
              </View>
            </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                  <View>
                    <Text style={styles.titleInput}>Unsafe Type *</Text>
                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                            style={{ width: '100%', height:35}}
                            placeholder="Select Type ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selectedType}
                            onValueChange={(itemValue) => this.setState({selectedType:itemValue})}>
                            <Picker.Item label="UA / Unsafe Action"  value="Unsafe Action" Sele/>
                            <Picker.Item label="UC / Unsafe Condition"  value="Unsafe Condition"/>
                        </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                <View style={{ flex: 1}}>
                  <View>
                    <Text style={styles.titleInput}>Jam *</Text>
                    <DateTimePicker
                        style={{ width: '100%', fontSize:10, borderRadius:20}}
                        date={this.state.reportTime}
                        mode="time"
                        placeholder="Choose Time ..."
                        format="hh:mm"
                        minTime="00:00"
                        maxTime="23:59"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        customStyles={{
                          dateInput: {
                            marginLeft: 5, marginRight:5, height: 35, borderRadius:5, fontSize:10, borderWidth:1, borderColor:"#E6E6E6"
                          },
                          dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 5,
                          },
                        }}
                        onDateChange={(time) => { this.setState({ reportTime: time }) }}
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Unit Kerja/Instansi/Vendor *</Text>
                    {this.state.isChecked == false
                      ? (
                    <View>
                        <View style={{ flex: 1,flexDirection:'column'}}>
                            <Button
                                block
                                style={{flex:1,borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, backgroundColor:colors.gray ,borderColor:"#E6E6E6", height:35}}
                                onPress={() => this.onClickSearchUnitKerja()}>
                                <Text style={{fontSize:12}}>{this.state.selectedUnitKerjaLabel}</Text>
                            </Button>
                        </View>

                        {this.state.visibleSearchListUnitKerja &&
                        <View style={{height:300,flexDirection:'column',borderWidth:1,padding:10,backgroundColor:colors.gray,margin:5,borderColor:"#E6E6E6",}}>
                            <Form>
                                <Item stackedLabel style={{ marginLeft: 0 }}>
                                    <Input value={this.state.searchWordUnitKerja} style={{borderRadius:5, marginLeft:5, marginRight:5, fontSize:10}} bordered onChangeText={(text) => this.onChangeUnitKerja(text)} placeholder='Ketik Unit Kerja' />
                                </Item>
                            </Form>
                            <FlatList
                              data={this.state.listUnitKerja}
                              renderItem={this._renderItem}
                              keyExtractor={(item, index) => index.toString()}
                            />
                        </View>
                        }
                    </View>
                    ):(
                      <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                          <Picker
                              mode="dropdown"
                              iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                              style={{ width: '100%', height:35 }}
                              placeholder="Unit Kerja"
                              placeholderStyle={{ color: "#bfc6ea" }}
                              placeholderIconColor="#007aff"
                              selectedValue={this.state.selectedUnitKerjaLabel}
                              onValueChange={(itemValue) => this.setState({selectedUnitKerjaLabel:itemValue})}>
                              <Picker.Item label="Choose Vendor..."  value=""/>
                              {listVendor}
                          </Picker>
                      </Form>
                    )}
                  </View>
                  <CheckBox
                    style={{flex:1, paddingTop:5}}
                    onClick={()=>{
                      this.setState({
                        isChecked:!this.state.isChecked,
                      })
                    }}
                    isChecked={this.state.isChecked}
                    rightText={"Non SMIG"}
                  />
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                  <View>
                    <Text style={styles.titleInput}>Kategori *</Text>
                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                            style={{ width: '100%', height:35 }}
                            placeholder="Kategori"
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selectedKategori}
                            onValueChange={(itemValue) => this.setState({selectedKategori:itemValue})}>
                            <Picker.Item label="Choose Kategori..."  value=""/>
                            {listKategori}
                        </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>Uraian Aktifitas *</Text>
                  </View>
                  <View>
                  <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.aktifitas} placeholder='Type Aktifitas ...' onChangeText={(text) => this.setState({ aktifitas: text })} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>Penyebab Masalah</Text>
                  </View>
                  <View>
                  <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.penyebab} placeholder='Type Penyebab ...' onChangeText={(text) => this.setState({ penyebab: text })} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>Potensi Bahaya *</Text>
                  </View>
                  <View>
                  <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.potensi} placeholder='Type Potensi Bahaya ...' onChangeText={(text) => this.setState({ potensi: text })} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>Rekomendasi Safety</Text>
                  </View>
                  <View>
                  <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.rekomendasi} placeholder='Type Rekomendasi Safety ...' onChangeText={(text) => this.setState({ rekomendasi: text })} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>Rencana Tindak Lanjut</Text>
                  </View>
                  <View>
                  <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={2} bordered value={this.state.tindakLanjut} placeholder='Type Tindak Lanjut ...' onChangeText={(text) => this.setState({ tindakLanjut: text })} />
                  </View>
                </View>
              </CardItem>
          </View>
          <View style={styles.Contentsave}>
          <Button
            block
            style={{
              height: 45,
              marginLeft: 20,
              marginRight: 20,
              marginBottom: 20,
              borderWidth: 1,
              backgroundColor: "#00b300",
              borderColor: "#00b300",
              borderRadius: 4
            }}
            onPress={() => this.createDetailHeader()}
          >
            <Text style={{color:colors.white}}>SUBMIT</Text>
          </Button>
        </View>
        </Content>


      </View>
      <View style={{ width: 270, position: "absolute" }}>
          <Dialog visible={this.state.visibleLoadingUnitKerja}>
            <DialogContent>
              {
                <ActivityIndicator size="large" color="#330066" animating />
              }
            </DialogContent>
          </Dialog>
      </View>
      <View style={{ width: 270, position: "absolute" }}>
        <Dialog
          visible={this.state.visibleDialogSubmit}
          dialogTitle={<DialogTitle title="Creating Detail Haeder Report K3 .." />}
        >
          <DialogContent>
            {<ActivityIndicator size="large" color="#330066" animating />}
          </DialogContent>
        </Dialog>
      </View>
      <CustomFooter navigation={this.props.navigation} menu='Safety' />
    </Container>

    );
  }
}

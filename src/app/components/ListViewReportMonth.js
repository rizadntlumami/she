import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../styles/colors";

export default class ListViewReportMonth extends Component {
  render() {
    return (
      <View
        style={{
          height: "100%",
          width: "100%",
        }}
      >
        <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{marginTop:5, paddingLeft:10, marginBottom:5, width:'60%'}}>
                <Text style={{fontSize:9, fontWeight:'bold'}}>{this.props.typeKondisi}</Text>
                <Text style={{fontSize:9}}>{this.props.kondisi}</Text>
                <Text style={{fontSize:9}}>Type : {this.props.periode}</Text>
                <Text style={{fontSize:9}}>{this.props.parameter}</Text>
                <Text style={{fontSize:9}}>Kondisi : {this.props.status}</Text>
                <Text style={{fontSize:9}}>Note : {this.props.note}</Text>
            </View>
            <View style={{marginTop:5, marginBottom:5, paddingRight:10, width:'40%'}}>
                <Text style={{fontSize:9, textAlign: 'right'}}>Periode</Text>
                <Text style={{fontSize:9, textAlign: 'right'}}>{this.props.year} - {this.props.month}</Text>
                <Text style={{fontSize:9, paddingTop:5, textAlign: 'right'}}>Pengecekan</Text>
                <Text style={{fontSize:9, textAlign: 'right'}}>{this.props.dateCek}</Text>
            </View>
        </View>
      </View>
    );
  }
}

import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Alert
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  Button,
  FooterTab,
  Text,
  Left,
  Right,
  Body,
  Card,
  CardItem,
  Form,
  Textarea,
  Icon
} from "native-base";
import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton
  } from "react-native-popup-dialog";

import Moment from "moment";
import LinearGradient from "react-native-linear-gradient";
import GlobalConfig from "../../components/GlobalConfig";
import ImagePicker from "react-native-image-picker";
import Ripple from "react-native-material-ripple";
import styles from "../styles/OrderSubmit";
import colors from "../../../styles/colors";

var that;
class ListItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selected2: undefined,
      pickedImage: "",
      uri: "",
      fileName: ""
    };
  }

  onValueChange2(value) {
    this.setState({
      selected2: value
    });
  }

  reset = () => {
    this.setState({
      pickedImage: null
    });
  };

  pickImageHandler = () => {
    ImagePicker.showImagePicker(
      { title: "Pick an Image", maxWidth: 800, maxHeight: 600 },
      res => {
        if (res.didCancel) {
          console.log("User cancelled!");
        } else if (res.error) {
          console.log("Error", res.error);
        } else {
          this.setState({
            pickedImage: { uri: res.uri },
            uri: res.uri,
            fileName: res.fileName
          });
        }
      }
    );
  };

  resetHandler = () => {
    this.reset();
  };

  orderRusak = () => {
    if(this.state.uri==''){
      alert("Masukkan Foto")
    } else {
      this.props.parentFlatList.setState({
        visibleDialogSubmit:true
      })
    // alert(JSON.stringify(this.state.id));
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "rusak";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("id", this.props.data.id);
      formData.append("foto", {
        uri: this.state.uri,
        type: "image/jpeg",
        name: this.state.fileName
      });
      //alert(JSON.stringify(url));

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson));
          if (responseJson.status == 200) {
            this.props.parentFlatList.setState({
                visibleDialogSubmit:false
              })
            Alert.alert("Success", "Aduan Kerusakan Sukses", [
              {
                text: "Okay"
              }
            ]);
            this.props.navigation.navigate("OrderRusak");
          } else {
            this.props.parentFlatList.setState({
                visibleDialogSubmit:false
              })
            Alert.alert("Error", "Aduan Kerusakan Gagal", [
              {
                text: "Okay"
              }
            ]);
          }
        })
        .catch((error) => {
          Alert.alert('Error', 'Check Your Internet Connection', [{
              text: 'Okay'
          }])
            console.log(error)
        })
    });
    }
  };

  render() {
    const { navigate } = that.props.navigation;

    return this.state.isloading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <CardItem
          style={{
            borderRadius: 0,
            marginTop: 4,
            backgroundColor: colors.gray
          }}
        >
          <View
            style={{
              flex: 1,
              flexDirection: "row",
              marginTop: 4,
              backgroundColor: colors.gray
            }}
          >
            <View style={{ flexDirection: "row" }}>
              <View style={{ flex: 1 }}>
                <Text style={{fontSize:12}}>Kode Order</Text>
                <Text style={{fontSize:12}}>APD Name</Text>
                <Text style={{fontSize:12}}>Merk</Text>
                <Text style={{fontSize:12}}>Released Date</Text>
                {/* <Text style={{fontSize:12}}>Expired Days</Text> */}
              </View>
              <View style={{ flex: 2 }}>
                <Text style={styles.viewKode}>
                  : {this.props.data.nomor_order}
                </Text>
                <Text style={styles.viewNama}>: {this.props.data.nama_apd}</Text>
                <Text style={styles.viewMerk}>: {this.props.data.merk}</Text>
                <Text style={styles.viewMerk}>: {Moment(this.props.data.release_at).format('DD MMMM YYYY')}</Text>
                {/* <Text style={styles.viewMerk}>
                  : {this.props.data.EXP_DAYS} Days
                </Text> */}
              </View>
              {/* <Text style={styles.viewKode}>
                          {this.props.data.KODE_ORDER}
                        </Text>
                        <Text style={styles.viewNama}>
                          {this.props.data.NAMA}
                        </Text>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                            <View style={{width:20}}>
                                <Text style={styles.viewMerk}>
                                  {this.props.data.MERK}
                                </Text>
                            </View>
                            <View>
                                <Text style={styles.viewMerk}>
                                  Exp : {this.props.data.EXP_DAYS} Days
                                </Text>
                            </View>
                        </View> */}
            </View>
          </View>
        </CardItem>
        <View style={{ marginLeft: 10, marginRight: 10 }}>
          <Text style={{ fontSize: 10, marginTop: 5 }}>
            Upload Foto (jpg, png) max : 500 kb
          </Text>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={styles.viewLeftBtnUpload}>
              <Button
                style={{
                  borderRadius: 5,
                  backgroundColor: "#00b300",
                  marginTop: 5,
                  marginBottom:10
                }}
              >
                <Text
                  style={{
                    fontWeight: "bold",
                    color: "#ffffff",
                    marginLeft: 5,
                    marginRight: 5
                  }}
                  onPress={this.pickImageHandler}
                >
                  Upload
                </Text>
              </Button>
            </View>
            <View style={styles.viewLeftBtnUpload}>
              <Button
                style={{
                  borderRadius: 5,
                  backgroundColor: "#00b300",
                  marginTop: 5,
                  marginBottom:10
                }}
              >
                <Text
                  style={{
                    fontWeight: "bold",
                    color: "#ffffff",
                    marginLeft: 5,
                    marginRight: 5
                  }}
                  onPress={this.resetHandler}
                >
                  Reset
                </Text>
              </Button>
            </View>
          </View>

          <View>
            <Ripple
              style={{
                flex: 2,
                justifyContent: "center",
                alignItems: "center"
              }}
              rippleSize={176}
              rippleDuration={600}
              rippleContainerBorderRadius={15}
              onPress={this.pickImageHandler}
              rippleColor={colors.accent}
            >
            <View style={styles.placeholder}>
                <Image source={this.state.pickedImage} style={styles.previewImage}/>
            </View>
            </Ripple>
          </View>

          <View style={styles.Contentsave}>
            <Button
              block
              style={{
                height: 45,
                marginTop: 100,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 20,
                borderWidth: 1,
                backgroundColor: "#00b300",
                borderColor: "#00b300",
                borderRadius: 4
              }}
              onPress={() => this.orderRusak()}
            >
              <Text style={styles.buttonText}>Submit</Text>
            </Button>
          </View>
        </View>
      </View>
    );
  }
}

export default class OrderRusakSubmit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isloading: true,
      deletedRowKey: null,
      visibleDialogSubmit:false,
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item, index }) => (
    <ListItem data={item} index={index} parentFlatList={this} navigation={this.props.navigation}/>
  );

  componentDidMount() {
    AsyncStorage.getItem("idRusak").then(idRusak => {
      // this.setState({ idRusak: value });
      this.loadData(idRusak);
    });
    // AsyncStorage.getItem("token").then(value => {
    //   this.setState({ token: value });
    // });
  }

  loadData(idRusak) {
    AsyncStorage.getItem("token").then(value => {
      //alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "releaselist/personal/getid";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("id", idRusak);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson));
          this.setState({
            dataSource: responseJson.data,
            isloading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  render() {
    that = this;
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("OrderRusak")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIcon}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Order Rusak</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={styles.homeWrapper}>
          <Content style={{}}>
            {this.state.isloading ? (
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <ActivityIndicator size="large" color="#330066" animating />
              </View>
            ) : (
              <FlatList
                data={this.state.dataSource}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
              />
            )}
          </Content>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialogSubmit}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogTitle={<DialogTitle title="Creating Order.." />}
          >
            <DialogContent>
              {<ActivityIndicator size="large" color="#330066" animating />}
            </DialogContent>
          </Dialog>
        </View>
      </Container>
    );
  }
}

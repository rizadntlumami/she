import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  Card,
  CardItem,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon
} from "native-base";

import NewsContentsReport from "../../components/NewsContentReport";
import NewsContentsReportK3 from "../../components/NewsContentReportK3";
import NewsContentsRekapitulasi from "../../components/NewsContentRekapitulasi";
import NewsContentsLotto from "../../components/NewsContentLotto";
import NewsContentsAccidentReport from "../../components/NewsContentAccidentReport";
import LinearGradient from "react-native-linear-gradient";
import Ripple from "react-native-material-ripple";
import SubMenuSafety from "../../components/SubMenuSafety";
import styles from "../styles/SafetyMenu";
import GlobalConfig from "../../components/GlobalConfig";
import NewsContentsTools from "../../components/NewsContentTools";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";

var that
class ListItemNewsDailyReport extends React.PureComponent {
    navigateToScreen(route, id_Unsafe, shift) {
        var idAndShift = id_Unsafe + '+' + shift;
        console.log(idAndShift)
        AsyncStorage.setItem('noDokumen', this.props.data.NO_DOKUMEN).then(() => {
            AsyncStorage.setItem('idAndShift', idAndShift).then(() => {
                that.props.navigation.navigate(route);
            })
        })
    }

    render() {
      return (
          <Ripple
            rippleSize={176}
            rippleDuration={600}
            rippleContainerBorderRadius={15}
            onPress={() =>
              this.navigateToScreen(
                "DailyReportDetailList",
                this.props.data.ID,
                this.props.data.SHIFT
              )
            }
            rippleColor={colors.accent}
          >
                <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
                  <NewsContentsReport
                    imageUri={require("../../../assets/images/imgorder.png")}
                    id={this.props.data.ID}
                    shift={this.props.data.SHIFT}
                    date={this.props.data.TANGGAL}
                    inspector={this.props.data.NO_DOKUMEN}
                    navigation={this.props.navigation}
                  />
                </View>
          </Ripple>
      );
    }
  }

  class ListItemNewsAccident extends React.PureComponent {
    navigateToScreen(route, id_Accident) {
      // alert(id_Unsafe)
      AsyncStorage.setItem("list", JSON.stringify(id_Accident)).then(() => {
        that.props.navigation.navigate(route);
      });
    }

    render() {
      return (
          <Ripple
            rippleSize={176}
            rippleDuration={600}
            rippleContainerBorderRadius={15}
            onPress={() =>
              this.navigateToScreen("AccidentReportDetail", this.props.data)
            }
            rippleColor={colors.accent}
          >
                <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
                  <NewsContentsAccidentReport
                    imageUri={{ uri: GlobalConfig.SERVERHOST + 'api' + this.props.data.PICT_BEFORE }}
                    name={this.props.data.NAME_VICT}
                    badge={this.props.data.BADGE_VICT}
                    navigation={this.props.navigation}
                  />
                </View>
          </Ripple>
      );
    }
  }

  class ListItemNewsTools extends React.PureComponent {
    navigateToScreen(route, listToolsCertification) {
      AsyncStorage.setItem(
        "listAlat",
        JSON.stringify(listToolsCertification)
      ).then(() => {
        that.props.navigation.navigate(route);
      });
    }

    render() {
      return (
          <Ripple
            style={{
              flex: 2,
              justifyContent: "center",
              alignItems: "center"
            }}
            rippleSize={176}
            rippleDuration={600}
            rippleContainerBorderRadius={15}
            onPress={() =>
              this.navigateToScreen("ToolsCertificationDetail", this.props.data)
            }
            rippleColor={colors.accent}
          >
                <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
                  <NewsContentsTools
                    imageUri={{ uri: GlobalConfig.SERVERHOST + 'api' + this.props.data.FILE_BUKU }}
                    noBuku={this.props.data.NO_BUKU}
                    equipname={this.props.data.EQUIP_NAME}
                    endDate={this.props.data.UJI_ULANG}
                  />
                </View>
          </Ripple>
      );
    }
  }

  class ListItemNewsReportK3 extends React.PureComponent {
    navigateToScreen(route, listHeaderDailyReport) {
      AsyncStorage.setItem('list', JSON.stringify(listHeaderDailyReport)).then(() => {
          that.props.navigation.navigate(route);
      })
    }

    render() {
      return (
          <Ripple

            rippleSize={176}
            rippleDuration={600}
            rippleContainerBorderRadius={15}
            onPress={() =>
              this.navigateToScreen(
                "DailyReportK3Detail",
                this.props.data
              )
            }
            rippleColor={colors.accent}
          >
              <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5}}>
                <NewsContentsReportK3
                  imageUri={require("../../../assets/images/imgorder.png")}
                  id={this.props.data}
                  type={this.props.data.SUB_AREA_TXT}
                  inspector={this.props.data.CREATE_BY}
                />
              </View>
          </Ripple>
      );
    }
  }

  class ListItemNewsLotto extends React.PureComponent {
    navigateToScreen(route, listLotto) {
      AsyncStorage.setItem("list", JSON.stringify(listLotto)).then(() => {
        that.props.navigation.navigate(route);
      });
    }

    render() {
      return (
          <Ripple
            rippleSize={176}
            rippleDuration={600}
            rippleContainerBorderRadius={15}
            onPress={() => this.navigateToScreen("LottoDetail", this.props.data)}
            rippleColor={colors.accent}
          >
                <View
                  style={{
                    marginTop: 0,
                    marginLeft: 5,
                    marginRight: 5,
                  }}
                >
                  <NewsContentsLotto
                    number={this.props.data.EQUIPMENT_NUMBER}
                    kegiatan={this.props.data.KEGIATAN}
                    date={this.props.data.DRAWOUT_DATE}
                    time={this.props.data.DRAWOUT_TIME}
                    kegiatan={this.props.data.KEGIATAN}
                    note={this.props.data.NOTE}
                    create={this.props.data.CREATED_BY}
                    navigation={this.props.navigation}
                  />
                </View>
          </Ripple>
      );
    }
  }

  class ListItemNewsRekapitulasi extends React.PureComponent {
    navigateToScreen(route, listRekapitulasi) {
      AsyncStorage.setItem("list", JSON.stringify(listRekapitulasi)).then(() => {
        that.props.navigation.navigate(route);
      });
    }

    render() {
      return (
          <Ripple
            rippleSize={176}
            rippleDuration={600}
            rippleContainerBorderRadius={15}
            onPress={() =>
              this.navigateToScreen("RecapitulationsDetail", this.props.data)
            }
            rippleColor={colors.accent}
          >
                <View
                  style={{
                    marginTop: 0,
                    marginLeft: 5,
                    marginRight: 5,
                  }}
                >
                <NewsContentsRekapitulasi
                  noRekap={this.props.data.NO_REKAP}
                  dateLimit={this.props.data.BATAS_WAKTU}
                  dateCommit={this.props.data.DATE_COMMITMENT}
                  evaluasi={this.props.data.EVALUASI}
                  status={this.props.data.STATUS}
                  create={this.props.data.CREATE_BY}
                  note={this.props.data.NOTE}
                />
                </View>
          </Ripple>
      );
    }
  }

class SafetyMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: true,
      isLoadingNews: false,
      dataSource: [],
      dataNewsDailyReport: [],
      dataNewsAccident: [],
      dataNewsReportK3: [],
      dataNewsReport: [],
      dataNewsLotto: [],
      dataNewsRekapitulasi: [],
      dataNewsTools: [],
      listFileUpload: [],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("token")
      .then(value => {
        this.setState({ token: value });
      })
      .then(res => {
        this.loadNewsDailyReport();
        this.loadNewsAccident();
        this.loadNewsRekapitulasi();
        this.loadNewsDailyReportK3();
        this.loadNewsLotto();
        this.loadNewsTools();
      })
  }

  loadNewsDailyReport() {
    // load category
    const url = GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/list_view";
    var formData = new FormData();
    formData.append("token", this.state.token);
    formData.append("limit", 4);

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.data.list_temuan != undefined) {
          this.setState({
          dataNewsDailyReport: responseJson.data.list_temuan,
          // isloadingNews: false
        });
        }
        //alert(JSON.stringify(responseJson));

      })
      // console.log(this.state.isLoadingNews)
      .catch(error => {
        console.log(error)
      })
  }

  loadNewsAccident() {
    // load category
    const url = GlobalConfig.SERVERHOST + "api/v_mobile/safety/accident/view";
    var formData = new FormData();
    formData.append("token", this.state.token);
    formData.append("limit", 4);

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson.status=="200"){
          this.setState({
          dataNewsAccident: responseJson.data,
          // isloadingNews: false
        });
        }
        // alert(JSON.stringify(responseJson));

      })
      // console.log(this.state.isLoadingNews)
      .catch(error => {
        console.log(error)
      })
  }

  loadNewsTools() {
    // load category
    const url = GlobalConfig.SERVERHOST + "api/v_mobile/safety/sertification/view/alat";
    var formData = new FormData();
    formData.append("token", this.state.token);
    formData.append("limit", 4);

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson.status=="200"){
          this.setState({
          dataNewsTools: responseJson.data,
          // isloadingNews: false
        });
        }
        // alert(JSON.stringify(responseJson));

      })
      // console.log(this.state.isLoadingNews)
      .catch(error => {
        console.log(error)
      })
  }

  loadNewsRekapitulasi() {
    // load category
    const url = GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/recapitulation";
    var formData = new FormData();
    formData.append("token", this.state.token);
    formData.append("limit", 4);

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson.status==200){
          this.setState({
          dataNewsRekapitulasi: responseJson.data,
          // isloadingNews: false
        });
        }
        // alert(JSON.stringify(responseJson));

      })
      // console.log(this.state.isLoadingNews)
      .catch(error => {
        console.log(error)
      })
  }

  loadNewsDailyReportK3() {
    // load category
    const url = GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe_n/view_report_list";
    var formData = new FormData();
    formData.append("token", this.state.token);
    formData.append("search", '');
    formData.append("length", 5);
    formData.append("start", 0);
    // formData.append("id_unsafe_report", '');

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then(response => response.json())
      .then(responseJson => {

          this.setState({
          dataNewsReportK3: responseJson.data,
          // isloadingNews: false
        });

        // alert(JSON.stringify(responseJson));

      })
      // console.log(this.state.isLoadingNews)
      .catch(error => {
        console.log(error)
      })
  }

  loadNewsLotto() {
    // load category
    const url = GlobalConfig.SERVERHOST + "api/v_mobile/firesystem/lotto/view";
    var formData = new FormData();
    formData.append("token", this.state.token);
    formData.append("limit", 4);

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then(response => response.json())
      .then(responseJson => {
        if(responseJson.status==200){
          this.setState({
          dataNewsLotto: responseJson.data,
          // isloadingNews: false
        });
        }
        // alert(responseJson)

      })
      // console.log(this.state.isLoadingNews)
      .catch(error => {
        console.log(error)
      })
  }

  _renderItemNewsDailyReport = ({ item }) => (
      <ListItemNewsDailyReport data={item}/>
  )

  _renderItemNewsAccident = ({ item }) => (
      <ListItemNewsAccident data={item}/>
  )

  _renderItemNewsTools = ({ item }) => (
      <ListItemNewsTools data={item}/>
  )

  _renderItemNewsReportK3 = ({ item }) => (
      <ListItemNewsReportK3 data={item}/>
  )

  _renderItemNewsLotto = ({ item }) => (
      <ListItemNewsLotto data={item}/>
  )

  _renderItemNewsRekapitulasi = ({ item }) => (
      <ListItemNewsRekapitulasi data={item}/>
  )

  render() {
    that = this;
    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("HomeMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={{color:colors.white}}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Safety Menu</Title>
          </Body>

          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <ScrollView>
          <View style={{marginLeft: 20,
              marginRight: 20,
              marginTop: 15}}>
            <View style={styles.itemMenu}>
              <SubMenuSafety
                handleOnPress={() => this.props.navigation.navigate("DailyReport")}
                imageUri={require("../../../assets/images/iconDaily.png")}
                name="Daily Report"
              />
              <SubMenuSafety
                handleOnPress={() => this.props.navigation.navigate("AccidentReport")}
                imageUri={require("../../../assets/images/iconAccident.png")}
                name="Accident Report"
              />
              <SubMenuSafety
                handleOnPress={() => this.props.navigation.navigate("ToolsCertification")}
                imageUri={require("../../../assets/images/iconSIO.png")}
                name="SIO Sertification"
              />
            </View>
            <View style={styles.itemMenu}>
              <SubMenuSafety
                handleOnPress={() => this.props.navigation.navigate("Recapitulations")}
                imageUri={require("../../../assets/images/iconRekapitulasi.png")}
                name="Recapitulations"
              />
              <SubMenuSafety
                handleOnPress={() => this.props.navigation.navigate("DailyReportK3")}
                imageUri={require("../../../assets/images/iconDailyK3.png")}
                name="Daily Report K3"
              />
              <SubMenuSafety
                handleOnPress={() => this.props.navigation.navigate("Lotto")}
                imageUri={require("../../../assets/images/iconLotto.png")}
                name="Lotto"
              />
            </View>
          </View>
          <View style={styles.newsWrapper}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={{ marginLeft: 5, width: 260 }}>
                <Text style={{ fontSize: 12, fontWeight: "bold" }}>Daily Report</Text>
              </View>
              <View>
                <Text style={{ fontSize: 11, fontWeight: "bold", color: colors.green01 }}  onPress={() => this.props.navigation.navigate("DailyReport")}>Lihat Semua</Text>
              </View>
            </View>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <FlatList
                horizontal={true}
                data={this.state.dataNewsDailyReport}
                renderItem={this._renderItemNewsDailyReport}
                keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
          </View>
          <View style={styles.newsWrapper}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={{ marginLeft: 5, width: 260 }}>
                <Text style={{ fontSize: 12, fontWeight: "bold" }}>Accident Report</Text>
              </View>
              <View>
                <Text style={{ fontSize: 11, fontWeight: "bold", color: colors.green01 }}  onPress={() => this.props.navigation.navigate("AccidentReport")}>Lihat Semua</Text>
              </View>
            </View>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <FlatList
                horizontal={true}
                data={this.state.dataNewsAccident}
                renderItem={this._renderItemNewsAccident}
                keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
          </View>
          <View style={styles.newsWrapper}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={{ marginLeft: 5, width: 260 }}>
                <Text style={{ fontSize: 12, fontWeight: "bold" }}>Tools & SIO</Text>
              </View>
              <View>
                <Text style={{ fontSize: 11, fontWeight: "bold", color: colors.green01 }}  onPress={() => this.props.navigation.navigate("ToolsCertification")}>Lihat Semua</Text>
              </View>
            </View>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <FlatList
                horizontal={true}
                data={this.state.dataNewsTools}
                renderItem={this._renderItemNewsTools}
                keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
          </View>
          <View style={styles.newsWrapper}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={{ marginLeft: 5, width: 260 }}>
                <Text style={{ fontSize: 12, fontWeight: "bold" }}>Daily Report K3</Text>
              </View>
              <View>
                <Text style={{ fontSize: 11, fontWeight: "bold", color: colors.green01 }} onPress={() => this.props.navigation.navigate("DailyReportK3")}>Lihat Semua</Text>
              </View>
            </View>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <FlatList
                horizontal={true}
                data={this.state.dataNewsReportK3}
                renderItem={this._renderItemNewsReportK3}
                keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
          </View>
          <View style={styles.newsWrapper}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={{ marginLeft: 5, width: 260 }}>
                <Text style={{ fontSize: 12, fontWeight: "bold" }}>LOTTO</Text>
              </View>
              <View>
                <Text style={{ fontSize: 11, fontWeight: "bold", color: colors.green01 }} onPress={() => this.props.navigation.navigate("Lotto")}>Lihat Semua</Text>
              </View>
            </View>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <FlatList
                horizontal={true}
                data={this.state.dataNewsLotto}
                renderItem={this._renderItemNewsLotto}
                keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
          </View>
          <View style={styles.newsWrapper}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={{ marginLeft: 5, width: 260 }}>
                <Text style={{ fontSize: 12, fontWeight: "bold" }}>Recapitulations</Text>
              </View>
              <View>
                <Text style={{ fontSize: 11, fontWeight: "bold", color: colors.green01 }}  onPress={() => this.props.navigation.navigate("Recapitulations")}>Lihat Semua</Text>
              </View>
            </View>
            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
              <FlatList
                horizontal={true}
                data={this.state.dataNewsRekapitulasi}
                renderItem={this._renderItemNewsRekapitulasi}
                keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
          </View>
        </ScrollView>
        <CustomFooter navigation={this.props.navigation} menu='Safety' />
      </Container>
    );
  }
}

export default SafetyMenu;

import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Text,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input
} from "native-base";

import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton
  } from "react-native-popup-dialog";

  import moment from 'moment'
  import DatePicker from 'react-native-datepicker';
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewReportDetail from "../../components/ListViewReportDetail";

class DailyReportDetailListFoto extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      dataDetail: [],
      isLoading: true,
      headerUnsafeDetailList: []
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} />;

  navigateToScreen(route, id_listReport) {
    // alert(this.state.headerUnsafeDetailList.ID);
    AsyncStorage.setItem("id", JSON.stringify(id_listReport)).then(() => {
      this.props.navigation.navigate(route);
    });
  }

  componentDidMount() {
    AsyncStorage.getItem("unsafe").then(id_listReport => {
    //     var batasDate = moment(JSON.parse(id_listReport).BATAS_WAKTU, 'DD-MMM-YYYY');
    //   var batasDateString = batasDate.format('YYYY-MM-DD')
      var realDate = moment(JSON.parse(id_listReport).TGL_REAL, 'DD-MMM-YYYY');
      var realDateString = realDate.format('YYYY-MM-DD')
      var tglDate = moment(JSON.parse(id_listReport).TANGGAL, 'DD-MMM-YYYY');
      var tglDateString = tglDate.format('YYYY-MM-DD')
      this.setState({ headerUnsafeDetailList: JSON.parse(id_listReport) });
      this.setState({ listFileUpload: this.state.headerUnsafeDetailList });
      this.setState({ isLoading: false });
      this.setState({ batas_waktu:JSON.parse(id_listReport).BATAS_WAKTU})
      this.setState({ shift:JSON.parse(id_listReport).SHIFT})
      this.setState({ tgl_real:realDateString})
      this.setState({ tanggal:tglDateString})
      this.setState({ jenis_temuan:JSON.parse(id_listReport).JENIS_TEMUAN})
      this.setState({ status:JSON.parse(id_listReport).STATUS})
      console.log(this.state.headerUnsafeDetailList);
    });
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        //this.loadData(id_Unsafe);
      }
    );
  }

  updateTglReal() {
    this.setState({
        visibleDialogSubmit:true,
        visibleUpdate:false
    })
    AsyncStorage.getItem("token").then(value => {
        const url = GlobalConfig.SERVERHOST + 'api/v_mobile/safety/unsafe/update_detail';
        var formData = new FormData();
        formData.append("token", value);
        formData.append("ID", this.state.headerUnsafeDetailList.ID);
        formData.append("STATUS", this.state.status);
        formData.append("SHIFT", this.state.shift);
        formData.append("JENIS_TEMUAN", this.state.jenis_temuan);
        formData.append("TANGGAL", this.state.tanggal);
        formData.append("TGL_REAL", this.state.tgl_real);
        console.log(formData)
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                // console.log(responseJson);
                if (responseJson.status == 200) {
                    this.setState({
                        visibleDialogSubmit:false
                    })
                    Alert.alert('Success', 'Update Tanggal Komitmen/Real Success', [{
                        text: 'Okay'
                    }])
                    this.props.navigation.navigate("DailyReportUnsafe");
                } else {
                    this.setState({
                        visibleDialogSubmit:false
                    })
                    Alert.alert('Error', 'Update Tanggal Komitmen/Real Failed', [{
                        text: 'Okay'
                    }])
                }
            })
            .catch(error => {
                this.setState({
                    visibleDialogSubmit:false
                })
                Alert.alert('Error', 'Update Tanggal Komitmen/Real Failed', [{
                    text: 'Okay'
                }])
                console.log(error)
            })
    })   
}

  updateBatasWaktu() {
    this.setState({ 
        visibleUpdate: false ,
        visibleDialogSubmit:true
    })
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/update_recap";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("BATAS_WAKTU", this.state.batas_waktu);
      formData.append("ID_TEMUAN", this.state.headerUnsafeDetailList.ID);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            
            this.updateTglReal()
            
          } else {
            this.setState({ 
                visibleDialogSubmit:false
            })
            Alert.alert("Error", "Update Tanggal Komitmen/Real Failed", [
              {
                text: "Okay"
              }
            ]);
          }
        })
        .catch(error => {
            this.setState({ 
                visibleDialogSubmit:false
            })
          Alert.alert("Error", "Update Tanggal Komitmen/Real Failed", [
            {
              text: "Okay"
            }
          ]);
          console.log(error);
        });
    });
  }

  deleteDetailTemuan() {
    AsyncStorage.getItem("token").then(value => {
      // alert(JSON.stringify(value));
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe/delete_detail";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID", this.state.headerUnsafeDetailList.ID);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            Alert.alert("Success", "Delete Success", [
              {
                text: "Okay"
              }
            ]);
            this.props.navigation.navigate("DailyReportDetailList");
          } else {
            Alert.alert("Error", "Delete Failed", [
              {
                text: "Okay"
              }
            ]);
          }
        });
    });
  }

  render() {
    var listReportK3;
    var listDetailReportK3;
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() =>
                this.props.navigation.navigate("DailyReportUnsafe")
              }
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body>
            <Title style={styles.textbody}>Detail Unsafe</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            {this.state.headerUnsafeDetailList.JENIS_TEMUAN == "UC" && (
              <Button
                transparent
                onPress={() => this.setState({ visibleUpdate: true })}
              >
                <Icon
                  name="ios-create"
                  size={20}
                  style={styles.facebookButtonIconOrder2}
                />
              </Button>
            )}
          </Right>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Content style={{ marginTop: 10 }}>
          <ScrollView>
            <View style={{ justifyContent: "center", alignItems: "center" }}>
              <Text
                style={{
                  fontSize: 12,
                  color: colors.green01,
                  fontWeight: "bold"
                }}
              >
                {this.state.headerUnsafeDetailList.NO_DOKUMEN}
              </Text>
            </View>
            <CardItem style={{ borderRadius: 0, marginTop: 5 }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2, marginRight: 10 }}>
                  <Image
                    style={{
                      height: 200,
                      width: "100%",
                      borderColor: colors.gray,
                      borderWidth: 3,
                      borderRadius: 10
                    }}
                    source={{
                      uri:
                        GlobalConfig.SERVERHOST +
                        "api" +
                        this.state.headerUnsafeDetailList.FOTO_BEF
                    }}
                  />
                </View>
              </View>
            </CardItem>
            <CardItem style={{ borderRadius: 0 }}>
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2 }}>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Type
                  </Text>
                  {this.state.headerUnsafeDetailList.JENIS_TEMUAN == "UA" ? (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Unsafe Action
                    </Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      Unsafe Condition
                    </Text>
                  )}
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Priority
                  </Text>
                  {this.state.headerUnsafeDetailList.PRIORITY == 1 && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      HIGH
                    </Text>
                  )}
                  {this.state.headerUnsafeDetailList.PRIORITY == 2 && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      MEDIUM
                    </Text>
                  )}
                  {this.state.headerUnsafeDetailList.PRIORITY == 3 && (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      LOW
                    </Text>
                  )}
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Location
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnsafeDetailList.LOKASI_KODE} -{" "}
                    {this.state.headerUnsafeDetailList.LOKASI_TEXT}
                  </Text>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Inspector (PIC Penemu)
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnsafeDetailList.CREATE_BY}
                  </Text>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Temuan Ketidaksesuaian
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnsafeDetailList.TEMUAN}
                  </Text>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Penyebab / Masalah
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnsafeDetailList.PENYEBAB}
                  </Text>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Potensi Bahaya
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnsafeDetailList.POTENSI_BAHAYA}
                  </Text>
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Rekomendasi Safety
                  </Text>
                  {this.state.headerUnsafeDetailList.SAFETY_REQ == null ? (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>-</Text>
                  ) : (
                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                      {this.state.headerUnsafeDetailList.SAFETY_REQ}
                    </Text>
                  )}
                  <Text note style={{ fontSize: 10, paddingTop: 10 }}>
                    Rencana Tindak Lanjut
                  </Text>
                  <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                    {this.state.headerUnsafeDetailList.TINDAK_LANJUT}
                  </Text>
                  {/* <Text style={{ fontSize: 10, paddingTop: 10 }}>KASI Temuan</Text>
                                    <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.state.headerUnsafeDetailList.KASI} - {this.state.headerUnsafeDetailList.KASI_NAME}</Text> */}
                </View>
              </View>
            </CardItem>
            <CardItem
              style={{ borderRadius: 0, marginTop: 5, marginBottom: 10 }}
            >
              <View style={{ flex: 1, flexDirection: "row" }}>
                <View style={{ marginLeft: 10, flex: 2, marginRight: 10 }}>
                  <Text
                    note
                    style={{ fontSize: 10, paddingTop: 10, marginBottom: 10 }}
                  >
                    Gambar Temuan Setelah Tindak Lanjut
                  </Text>
                  {this.state.headerUnsafeDetailList.FOTO_AFT == null ? (
                    <Image
                      style={{
                        height: 200,
                        width: "100%",
                        borderColor: colors.gray,
                        borderWidth: 3,
                        borderRadius: 10
                      }}
                      source={require("../../../assets/images/imgorder.png")}
                    />
                  ) : (
                    <Image
                      style={{
                        marginTop: 10,
                        height: 200,
                        width: 300,
                        borderColor: colors.gray,
                        borderWidth: 3
                      }}
                      source={{
                        uri:
                          GlobalConfig.SERVERHOST +
                          "api" +
                          this.state.headerUnsafeDetailList.FOTO_AFT
                      }}
                    />
                  )}
                  {/* <Image style={{ marginTop: 10, height: 200, width: 300, borderColor: colors.gray, borderWidth: 3 }} source={{ uri: GlobalConfig.SERVERHOST + 'api' + this.state.headerUnsafeDetailList.FOTO_AFT }} /> */}
                </View>
              </View>
            </CardItem>
          </ScrollView>
        </Content>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleUpdate}
            dialogTitle={
              <DialogTitle
                textStyle={{ fontSize: 13 }}
                title="Update Tanggal Penyelesaian"
              />
            }
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            onTouchOutside={() => {
              this.setState({ visibleUpdate: false });
            }}
            dialogStyle={{ position: "absolute", width: "80%" }}
          >
            <DialogContent
              style={{
                backgroundColor: colors.white
              }}
            >
              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={{fontSize:12}}>Tanggal Komitmen Penyelesaian</Text>
                    <DatePicker
                      style={{ width: "100%", fontSize: 10, borderRadius: 20 }}
                      date={this.state.batas_waktu}
                      mode="date"
                      placeholder="Choose Date ..."
                      format="YYYY-MM-DD"
                      minDate="2018-01-01"
                      maxDate="2050-12-31"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: "absolute",
                          left: 0,
                          top: 5,
                          marginLeft: 0
                        },
                        dateInput: {
                          marginLeft: 50,
                          height: 35,
                          borderRadius: 20,
                          fontSize: 10
                        }
                      }}
                      onDateChange={date => {
                        this.setState({ batas_waktu: date });
                      }}
                    />
                  </View>
                </View>
              </CardItem>

              <CardItem
                style={{
                  borderRadius: 0,
                  marginTop: 0,
                  backgroundColor: colors.gray
                }}
              >
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={{fontSize:12}}>Tanggal Real Penyelesaian</Text>
                    <DatePicker
                      style={{ width: "100%", fontSize: 10, borderRadius: 20 }}
                      date={this.state.tgl_real}
                      mode="date"
                      placeholder="Choose Date ..."
                      format="YYYY-MM-DD"
                      minDate="2018-01-01"
                      maxDate="2050-12-31"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: "absolute",
                          left: 0,
                          top: 5,
                          marginLeft: 0
                        },
                        dateInput: {
                          marginLeft: 50,
                          height: 35,
                          borderRadius: 20,
                          fontSize: 10
                        }
                      }}
                      onDateChange={date => {
                        this.setState({ tgl_real: date });
                      }}
                    />
                  </View>
                </View>
              </CardItem>

              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Button
                    block
                    style={{
                      height: 45,
                      marginLeft: 20,
                      marginRight: 20,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: colors.green0,
                      borderColor: colors.green0,
                      borderRadius: 4
                    }}
                    onPress={() => this.updateBatasWaktu()}
                  >
                    <Text style={styles.buttonText}>Update</Text>
                  </Button>
                </View>
              </View>
            </DialogContent>
          </Dialog>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
                                <Dialog
                                    visible={this.state.visibleDialogSubmit}
                                    dialogTitle={<DialogTitle title='Updating Tanggal Komitmen ..'/>}
                                >
                                    <DialogContent>
                                    {<ActivityIndicator size="large" color="#330066" animating />}
                                    </DialogContent>
                                </Dialog>
                                </View>
        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}

export default DailyReportDetailListFoto;

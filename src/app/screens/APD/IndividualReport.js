import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
  RefreshControl
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
  Form,
  Picker,
  Icon
} from "native-base";
// import Icon from "react-native-vector-icons/FontAwesome";

import GlobalConfig from "../../components/GlobalConfig";
//import APDItems from '../../components/APDItem'
import MenuItems from "../../components/APDItems";
import styles from "../styles/IndividualReport";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";

class ListItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      active: "true"
    };
  }

  componentDidMount() {
    AsyncStorage.getItem("token").then(value => {
      this.setState({ token: value });
    });
  }

  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Card style={{ marginLeft: 10, marginRight: 10, borderRadius: 10 }}>
          <CardItem style={{borderRadius:10}}>
            <View style={{ flex: 1, flexDirection: "row" }}>
              <View style={styles.viewLeftHeader2}>
                <Text note style={styles.cardtext}>
                  Nama APD
                </Text>
                <Text note style={styles.cardtext}>
                  Note
                </Text>
                <Text note style={styles.cardtext}>
                  Release
                </Text>
                <Text note style={styles.cardtext}>
                  Expired
                </Text>
                <Text note style={styles.cardtext}>
                  Expired
                </Text>
              </View>
              <View>
                <Text style={styles.cardtext}>: {this.props.data.nama_apd}</Text>
                <Text note style={styles.cardtext}>
                  : {this.props.data.keterangan}
                </Text>
                <Text style={styles.cardtext}>
                  : {this.props.data.release_at}
                </Text>
                {/* <Text style={styles.cardtext}>
                  : {this.props.data.EXPIRED_DATE}
                </Text>
                <Text style={styles.cardtextKode}>
                  : {this.props.data.EXPIRED_DAYS_LEFT}
                </Text> */}
              </View>
            </View>
          </CardItem>
        </Card>
      </View>
    );
  }
}

export default class IndividualReport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      dataProfile: [],
      isloading: true,
      selectedAPD: "100",
      isEmpty: false,
      listClickedKatAPD: [],
      clickedKatAPD: 0,
      arrKodeApd: [
        "100",
        "200",
        "300",
        "400",
        "500",
        "600",
        "700",
        "800",
        "900",
        "910"
      ]
    };
  }

  static navigationOptions = {
    header: null
  };

  onValueChange2(value) {
    setTimeout(() => {
      this.setState({
        selectedAPD: value
      });
    }, 1000);
  }

  _renderItem = ({ item }) => <ListItem data={item} />;

  filterData(groupAPD, clicked) {
    this.setClickedKategori(clicked);
    this.setState({
      isloading: true
    });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/apd/order/history/personal";
      // alert(url)
      var formData = new FormData();
      formData.append("token", value);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson.data));
          this.setState({
            dataSource: responseJson.data[groupAPD],
            isloading: false
          });
          console.log(this.state.dataSource);
          if (this.state.dataSource==undefined){
            this.setState({
              isEmpty: true
            });
          }else{
            this.setState({
              isEmpty: false
            });
          }

        })
        .catch(error => {
          this.setState({
            isloading: false
          });
          console.log(error);
        });
    });
  }

  componentDidMount() {
    this.loadData();
  }

  loadData() {
    this.setClickedKategori(0);
    this.setState({
      isloading: true
    });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "releaselist/personal";
      // alert(url)
      var formData = new FormData();
      formData.append("token", value);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          console.log(responseJson);
          if (responseJson.data == undefined) {
            this.setState({
              isloading: false
            });
            this.setState({
              isEmpty: true
            });
          } else {
            // this.setState({
            //   dataSource: responseJson.data,
            //   isloading: false
            // });
            // var tempArrData=this.state.dataSource
            var tempArrData = responseJson.data;
            console.log(tempArrData.length);
            var tempArrKode = this.state.arrKodeApd;
            var arr = [];
            for (let i = 0; i < tempArrKode.length; i++) {
              if (tempArrData[tempArrKode[i]] != undefined) {
                // console.log('masuk1')
                var tempArrDataKate = tempArrData[tempArrKode[i]];
                for (let j = 0; j < tempArrDataKate.length; j++) {
                  // console.log('masuk2'+j)
                  arr.push(tempArrDataKate[j]);
                }
              }
            }
            this.setState({
              dataSource: arr,
              isloading: false
            });
            this.setState({
              isEmpty: false
            });
            console.log(arr);
          }
        })
        .catch(error => {
          this.setState({
            isloading: false
          });
          console.log(error);
        });
    });
  }

  setClickedKategori(clickedIndex) {
    var tempclickAPD = [];
    for (let i = 0; i < 11; i++) {
      if (i == clickedIndex) {
        tempclickAPD.push(true);
      } else {
        tempclickAPD.push(false);
      }
    }
    this.setState({
      listClickedKatAPD: tempclickAPD
    });
  }

  onRefresh() {
    console.log('refreshing')
    this.setState({ isloading: true }, function() {
      this.loadData();
    });
  }

  render() {
    return (
      <Container style={styles.wrapper}>
      <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("ApdMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Individual Report</Title>
          </Body>

            <Right style={{flex:1}}/>

        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ flex: 1 }}>
          <View style={styles.roundedBtn}>
          <ScrollView
              horizontal={true}
              showsHorizontalScrollIndicator={false}
            >
              <Left>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/all.png")}
                    name="All"
                    clicked={this.state.listClickedKatAPD[0] ? true : false} //coba j
                    handleOnPress={() => this.loadData()}
                  />
                </View>
              </Left>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/body.jpg")}
                    name="Body"
                    clicked={this.state.listClickedKatAPD[1] ? true : false} //coba j
                    handleOnPress={() => this.filterData(600, 1)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/head.jpg")}
                    name="Head"
                    clicked={this.state.listClickedKatAPD[2] ? true : false} //coba j
                    handleOnPress={() => this.filterData(100, 2)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/eye.jpg")}
                    name="Eyes"
                    clicked={this.state.listClickedKatAPD[3] ? true : false} //coba j
                    handleOnPress={() => this.filterData(200, 3)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/ear.jpg")}
                    name="Ear"
                    clicked={this.state.listClickedKatAPD[4] ? true : false} //coba j
                    handleOnPress={() => this.filterData(300, 4)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/cloves.jpg")}
                    name="Cloves"
                    clicked={this.state.listClickedKatAPD[5] ? true : false} //coba j
                    handleOnPress={() => this.filterData(500, 5)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/boots.jpg")}
                    name="Boots"
                    clicked={this.state.listClickedKatAPD[6] ? true : false} //coba j
                    handleOnPress={() => this.filterData(700, 6)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/nose.png")}
                    name="Nose"
                    clicked={this.state.listClickedKatAPD[7] ? true : false} //coba j
                    handleOnPress={() => this.filterData(400, 7)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/p3k.png")}
                    name="P3K"
                    clicked={this.state.listClickedKatAPD[8] ? true : false} //coba j
                    handleOnPress={() => this.filterData(900, 8)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/full.jpg")}
                    name="Full Body"
                    clicked={this.state.listClickedKatAPD[9] ? true : false} //coba j
                    handleOnPress={() => this.filterData(800, 9)}
                  />
                </View>
              </Body>
              <Body>
                <View style={styles.itemMenu}>
                  <MenuItems
                    imageUri={require("../../../assets/images/stock.png")}
                    name="Lain"
                    clicked={this.state.listClickedKatAPD[10] ? true : false} //coba j
                    handleOnPress={() => this.filterData(910, 10)}
                  />
                </View>
              </Body>
            </ScrollView>
          </View>

          <View
            style={{
              flex: 1,
              flexDirection: "column",

            }}
          >
            {/* <View style={{ flex: 1, flexDirection: "column",borderWidth:5 }}> */}
            {this.state.isloading ? (
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <ActivityIndicator size="large" color="#330066" animating />
              </View>
            ) : this.state.isEmpty ? (
              <View
                style={{
                  flex: 1,
                  justifyContent: "center",
                  alignItems: "center"
                }}
              >
                <Thumbnail
                  square
                  large
                  source={require("../../../assets/images/empty.png")}
                />
                <Text>No Data!</Text>
              </View>
            ) : (
              <FlatList
                data={this.state.dataSource}
                renderItem={this._renderItem}
                keyExtractor={(item, index) => index.toString()}
                refreshControl={
                <RefreshControl
                  refreshing={this.state.isloading}
                  onRefresh={this.onRefresh.bind(this)}
                />}
              />
            )}
            {/* </View> */}
          </View>
        </View>
        {/* <CustomFooter navigation={this.props.navigation} menu="APD" /> */}
      </Container>
    );
  }
}

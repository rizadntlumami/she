import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
} from "native-base";
import SubMenuFire from "../../components/FireSystem/SubMenuFire";
import styles from "../styles/ApdMenu";
import GlobalConfig from "../../components/GlobalConfig";
import Ripple from "react-native-material-ripple";
import colors from "../../../styles/colors";
import Moment from 'moment';

class ApdMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: true,
      isLoadingNews: false,
      dataSource: [],
      roleID:'',
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("token").then(token => {
      const url = GlobalConfig.SERVERHOST + 'validasi';
      var formData = new FormData();
      formData.append("token", token)
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listProfile: responseJson.user,
            name: responseJson.user.name,
            roleID: responseJson.user.role_id,
            isloading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  render() {
    return (
      <Container style={styles.wrapper}>
        <View style={{backgroundColor:colors.primer, height:130, paddingBottom:20}}>
          <View style={{flex:1, flexDirection:'row', marginLeft:20, marginRight:20, marginTop:30}}>
            <View style={{width:'10%'}}>
              <View style={{backgroundColor:colors.second, borderRadius:5, height:30, justifyContent:'center', alignItems:'center'}}>
              <TouchableOpacity
                transparent
                  onPress={() => this.props.navigation.navigate("HomeMenu")}>
                  <Icon
                      name='ios-home'
                      style={{fontSize:20, color:colors.white}}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{width:'90%', paddingLeft:20}}>
              <Text style={{fontSize:18, fontWeight:'bold', color:colors.white}}>MENU APD</Text>
            </View>
          </View>
        </View>
        <View style={{marginLeft:20, marginRight:20, marginTop:-50, backgroundColor:colors.white, borderRadius:5, height:200,
          shadowColor:'#000',
          shadowOffset:{
            width:0,
            height:1,
          },
          shadowOpacity:1,
          shadowRadius:2,
          elevation:3,
        }}>
          <View style={{flex:1, flexDirection:'row'}}>
          <View style={{width:'33%', paddingTop:25}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("OrderPersonalAPD")}
                imageUri={require("../../../assets/images/iconDailyReportSection.png")}
                name="Order APD"
              />
          </View>
          <View style={{width:'33%', paddingTop:25}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("MyOrderPersonalAPD")}
                imageUri={require("../../../assets/images/iconInspectionReport.png")}
                name="My Order"
              />
          </View>
          <View style={{width:'33%', paddingTop:25}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("OrderRusak")}
                imageUri={require("../../../assets/images/iconFireReport.png")}
                name="Report APD"
              />
          </View>
          </View>
          <View style={{flex:1, flexDirection:'row'}}>
          {(this.state.roleID== 1 || this.state.roleID== 2 || this.state.roleID== 3) && (
          <View style={{width:'33%', paddingTop:10}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("ApprovalPersonalAPD")}
                imageUri={require("../../../assets/images/iconUnitCheck.png")}
                name="Approval APD"
              />
          </View>
          )}
          <View style={{width:'33%', paddingTop:10}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("ReportPersentageAPD")}
                imageUri={require("../../../assets/images/iconUnitCheckDashboard.png")}
                name="Stock APD"
              />
          </View>
          <View style={{width:'33%', paddingTop:10}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("IndividualReportPersonal")}
                imageUri={require("../../../assets/images/iconLotto.png")}
                name="Individual Report"
              />
          </View>
          </View>
        </View>
        <ScrollView>

        </ScrollView>
      </Container>
    );
  }
}

export default ApdMenu;

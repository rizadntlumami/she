import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform,
  BackHandler
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Textarea
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import MenuItems from "../components/MenuItems";
import SubNews from "../components/SubNews";
import styles from "./styles/HomeMenu";
import colors from "../../styles/colors";
import GlobalConfig from "../components/GlobalConfig";

var that;
class ListNews extends React.PureComponent {
  navigateToScreen(route, idNews) {
    AsyncStorage.setItem("idNews", JSON.stringify(idNews)).then(() => {
      that.props.navigation.navigate(route);
    });
  }

  render() {
    return (
      <TouchableOpacity
        transparent
        onPress={() => this.navigateToScreen("DetailNews", this.props.data.id)}>
          <SubNews
            imageUri={{ uri: GlobalConfig.IMAGEHOST + "News/" + this.props.data.image }}
            head={this.props.data.head_title}
            content={this.props.data.content_title}
            writer={this.props.data.writer}
            date={this.props.data.date}
            time={this.props.data.time}
          />
        </TouchableOpacity>
    );
  }
}

export default class HomeMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      isLoading: true,
      token:'',
      name:'',
      listNews:[],
      listProfile:[],
      profile:false,
      logOut:false,
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this.setState({
      isLoading:false,
    })
    AsyncStorage.getItem("token").then(token => {
      const url = GlobalConfig.SERVERHOST + 'validasi';
      var formData = new FormData();
      formData.append("token", token)
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listProfile: responseJson.user,
            name: responseJson.user.name,
            isloading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
    this.loadNews();
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.backHandler =BackHandler.addEventListener('hardwareBackPress', this.handleBackPress);
        this.setState({
          dataNews: [],
          isLoadingNews: true,
        });
        this.loadNews();
      }
    );

    const didBlurSubscription = this.props.navigation.addListener(
      'didBlur',
      payload => {
        console.log("masuk blur")
        this.backHandler.remove();
      }
    );
  }

  loadNews(){
    const url = GlobalConfig.SERVERHOST + 'getNews';
    var formData = new FormData();
    formData.append("news", 'news')
    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then(response => response.json())
      .then((responseJson) => {
          if(responseJson.status == 200) {
              this.setState({
                  listNews: responseJson.data,
                  isLoading: false
              });
          }
      })
      .catch(error => {
        console.log(error);
      });
  }

  componentWillUnmount() {
    console.log("masuk unmount")
    
  }

  handleBackPress = () => {
    this.exitApp(); // works best when the goBack is async
    return true;
  }

  exitApp() {
    Alert.alert(
      "Confirmation",
      "Exit SHE Mobile?",
      [
        {
          text: "No",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "Yes", onPress: () => BackHandler.exitApp() }
      ],
      { cancelable: false }
    );
  }

  async removeItemValue(key) {
    try {
      await AsyncStorage.removeItem(key);
      return true;
    }
    catch(exception) {
      return false;
    }
  }

  close(){
    this.setState({
      profile:false,
    })
  }

  logOut(){
    this.setState({
      profile:false,
      logOut:true,
    })
  }

  methLogOut(){
    this.setState({
      logOut:false,
    })
    //BackHandler.exitApp()
    this.props.navigation.navigate("LoggedOut")
  }

  Konfirmasi(){
    alert("Null")
  }

  _renderNews = ({ item }) => <ListNews data={item} />;

  render() {
    that=this;
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
        <View style={{backgroundColor:colors.primer, height:130, paddingBottom:20}}>
          <View style={{flex:1, flexDirection:'row', marginLeft:20, marginRight:20, marginTop:30}}>
            <View style={{width:'70%',}}>
              <Text style={{fontSize:18, fontWeight:'bold', color:colors.white}}>SHE MOBILE</Text>
            </View>
            <View style={{width:'30%'}}>
              <View style={{backgroundColor:colors.second, borderRadius:5, height:30}}>
                <View style={{flex:1, flexDirection:'row'}}>
                  <View style={{width:'70%', alignItems:'center', justifyContent:'center'}}>
                    <TouchableOpacity
                      transparent
                        onPress={() => this.setState({profile:true})}>
                        <Text style={{fontSize:12, fontWeight:'bold', color:colors.white}}>Account</Text>
                    </TouchableOpacity>
                  </View>
                  <View style={{width:'30%', justifyContent:'center'}}>
                    <TouchableOpacity
                      transparent
                        onPress={() => this.setState({profile:true})}>
                        <Icon
                          name='ios-contact'
                          style={{fontSize:20, color:colors.white}}
                        />
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
        <View style={{marginLeft:20, marginRight:20, marginBottom:10, marginTop:-50, backgroundColor:colors.white, borderRadius:5, height:130,
          shadowColor:'#000',
          shadowOffset:{
            width:0,
            height:1,
          },
          shadowOpacity:1,
          shadowRadius:2,
          elevation:3,
        }}>
          <View style={{flex:1, flexDirection:'row'}}>
          <View style={{width:'33%', paddingTop:25}}>
              <MenuItems
                handleOnPress={() =>
                  this.props.navigation.navigate("ApdMenu")
                }
                imageUri={require("../../assets/images/iconAPD.png")}
                name="A.P.D"
              />
          </View>
          <View style={{width:'33%', paddingTop:25}}>
              <MenuItems
                handleOnPress={() =>
                  this.props.navigation.navigate("SafetyMenu")
                }
                imageUri={require("../../assets/images/iconSafety.png")}
                name="Safety"
              />
          </View>
          <View style={{width:'33%', paddingTop:25}}>
              <MenuItems
                handleOnPress={() =>
                  this.props.navigation.navigate("FireMenu")
                }
                imageUri={require("../../assets/images/iconFireSystem.png")}
                name="Fire System"
              />
          </View>
          </View>

        </View>
        <ScrollView>
        <View style={{flex:1, flexDirection:'row', paddingTop:5}}>
          <View style={{width:'50%', paddingLeft:20}}>
            <Text style={{fontSize:15, fontWeight:'bold'}}>Hot News</Text>
          </View>
          <View style={{width:'50%', paddingRight:20}}>

          </View>
        </View>

        <View style={{width:'100%', marginTop:0, paddingBottom:5, paddingLeft:15, paddingRight:20, marginBottom:20}}>
          <View style={{marginTop:0}}>
            <ScrollView>
              <FlatList
                data={this.state.listNews}
                renderItem={this._renderNews}
                keyExtractor={(item, index) => index.toString()}
              />
            </ScrollView>
          </View>
        </View>
        </ScrollView>

        <View style={{ width: 270, position: "absolute",}}>
          <Dialog
            visible={this.state.profile}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog }}
            onTouchOutside={() => {
              this.setState({ profile: false });
            }}
          >
            <DialogContent>
              {
                <View>
                  <View style={{alignItems:'center', justifyContent:'center', paddingTop:20, width:'100%'}}>
                    <Text style={{fontSize:13, alignItems:'center', color:colors.black}}>Profile User</Text>
                  </View>
                  <View style={{flex:1, flexDirection:'row'}}>
                    <View style={{width:70}}>
                      <Icon
                        name='ios-contact'
                        style={{fontSize:70, color:colors.graydark}}
                      />
                    </View>
                    <View style={{width:200}}>
                      <Text style={{ paddingTop: 10, fontSize: 12 }}>
                        {this.state.listProfile.no_badge}
                      </Text>
                      <Text style={{ fontSize: 15, fontWeight:'bold' }}>
                        {this.state.listProfile.name}
                      </Text>
                    </View>
                  </View>
                  <Text style={{ fontSize: 12}}>
                    Position
                  </Text>
                  <Text style={{ fontSize: 12, fontWeight:'bold'}}>
                    {this.state.listProfile.pos_text}
                  </Text>
                  <Text style={{ fontSize: 12, marginTop:5}}>
                    Unit Kerja
                  </Text>
                  <Text style={{ fontSize: 12, fontWeight:'bold'}}>
                    {this.state.listProfile.unit_kerja}
                  </Text>
                  <Text style={{ fontSize: 12, marginTop:5}}>
                    Company
                  </Text>
                  <Text style={{ fontSize: 12, fontWeight:'bold'}}>
                    {this.state.listProfile.company}
                  </Text>

                  <View style={{paddingTop:10, width:'100%', alignItems:'center'}}>
                    <View style={{flex:1, flexDirection:'row'}}>
                      <View style={{width:'40%', paddingRight:5}}>
                          <Button
                            block
                            style={{
                              width:'100%',
                              marginTop:10,
                              height: 35,
                              marginBottom: 5,
                              borderWidth: 0,
                              backgroundColor: colors.primer,
                              borderRadius: 15
                            }}
                            onPress={() => this.close()}
                          >
                            <Text style={{color:colors.white}}>Close</Text>
                          </Button>
                      </View>
                      <View style={{width:'40%', paddingRight:5}}>
                          <Button
                            block
                            style={{
                              width:'100%',
                              marginTop:10,
                              height: 35,
                              marginBottom: 5,
                              borderWidth: 0,
                              backgroundColor: colors.primer,
                              borderRadius: 15
                            }}
                            onPress={() => this.logOut()}
                          >
                            <Text style={{color:colors.white}}>LogOut</Text>
                          </Button>
                      </View>
                    </View>
                  </View>
                </View>
              }
            </DialogContent>
          </Dialog>
        </View>

        <View style={{ width: '100%', position: "absolute"}}>
          <Dialog
            visible={this.state.logOut}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300}}
            onTouchOutside={() => {
              this.setState({ logOut: false });
            }}
          >
          <DialogContent>
          {
            <View>
              <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>LogOut SHE Mobile ?</Text>
              </View>
              <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
                <View style={{width:'50%', paddingRight:5}}>
                    <Button
                      block
                      style={{
                        width:'100%',
                        marginTop:10,
                        height: 35,
                        marginBottom: 5,
                        borderWidth: 0,
                        backgroundColor: colors.primer,
                        borderRadius: 15
                      }}
                      onPress={() => this.methLogOut()}
                    >
                      <Text style={{color:colors.white}}>Yes</Text>
                    </Button>
                </View>
                <View style={{width:'50%', paddingLeft:5}}>
                    <Button
                      block
                      style={{
                        width:'100%',
                        marginTop:10,
                        height: 35,
                        marginBottom: 5,
                        borderWidth: 0,
                        backgroundColor: colors.primer,
                        borderRadius: 15
                      }}
                      onPress={() => this.setState({
                        logOut:false,
                      })}
                    >
                      <Text style={{color:colors.white}}>No</Text>
                    </Button>
                </View>
              </View>
            </View>
          }
          </DialogContent>
          </Dialog>
        </View>
      </Container>
    );
  }
}

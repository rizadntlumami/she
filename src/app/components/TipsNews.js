import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../styles/colors";

export default class TipsNews extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View
        style={{
          height: 130,
          width: 230,
          marginTop: 10,
          marginLeft: 20,
          borderWidth: 0.5,
          borderColor: "#dddddd",
          elevation: 1
        }}
      >
        <View style={{ flex: 2 }}>
          <Image
            source={this.props.imageUri}
            style={{
              flex: 1,
              width: null,
              height: null,
              resizeMode: "cover"
            }}
          />
        </View>
        <View style={{ flex: 1, paddingLeft: 10, paddingTop: 10 }}>
          <Text
            style={{
              fontFamily: "HELR45W",
              fontSize: 14,
              color: colors.black,
              fontWeight: "600",
              textAlign: "center"
            }}
          >
            {this.props.name}
          </Text>
        </View>
      </View>
    );
  }
}

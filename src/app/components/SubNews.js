import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import { Icon } from "native-base";
import colors from "../../styles/colors";
import Moment from 'moment'

let styles = {
  container: {
    flex: 1,
    height: '100%',
    width: '100%',
    marginLeft: 5,
    marginRight: 5,
    marginBottom: 0,
    borderRadius:10,
    marginTop:10,
    backgroundColor:colors.white,
  },
  b: {
    backgroundColor: colors.primary
  }
};

export default class SubNews extends Component {
  render() {
    return (
      <View style={[styles.container]}>
      <View style={{flexDirection:'row'}}>
        <View style={{width:'40%', justifyContent: "center", flex:1, flexDirection:'row', borderBottomLeftRadius:5, borderTopLeftRadius:5}}>
          <View style={{width:'35%', justifyContent: "center", backgroundColor:colors.primer, borderBottomLeftRadius:5, borderTopLeftRadius:5}}>
            <Text style={{color:colors.white, textAlign:'center', fontWeight:'bold' }}>{Moment(this.props.date).format('DD')}</Text>
            <Text style={{color:colors.white, textAlign:'center', fontWeight:'bold' }}>{Moment(this.props.date).format('MMM')}</Text>
          </View>
          <View style={{width:'65%'}}>
          <Image
              source={this.props.imageUri}
              style={{ width: '100%', height: '100%'}}
            />
          </View>
        </View>
        <View style={{width:'60%', marginBottom:10, marginTop:10, marginLeft:10, paddingRight:10}}>
          <Text style={{fontSize:10, color:colors.graydark}}>{this.props.writer}, {this.props.time}</Text>
          <Text style={{fontSize:13, color:colors.black, fontWeight:'bold'}}>{this.props.head} ..</Text>
          <Text style={{fontSize:12, color:colors.graydark}}>{this.props.content} ..</Text>
        </View>
      </View>
      </View>
    );
  }
}

import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TextInput,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Item
} from "native-base";

import Dialog, {
    DialogTitle,
    SlideAnimation,
    DialogContent,
    DialogButton
  } from "react-native-popup-dialog";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import Ripple from "react-native-material-ripple";
import Moment from 'moment'
import SwitchToggle from 'react-native-switch-toggle';

export default class InspectionReportAlarmUpdate extends Component {
    constructor(props) {
        super(props);
        this.state = {
            active: 'true',
            isLoading: true,
            visibleDialogSubmit:false,
            id:'',
            paramID:'',
            paramName:'',
            locationID:'',
            note:'',
            statusCond:'',
            status:false,
        };
    }

    static navigationOptions = {
        header: null
    };

    componentDidMount() {
      AsyncStorage.getItem('dataDetail').then((dataDetail) => {
        this.setState({
          id: JSON.parse(dataDetail).ID,
          paramID: JSON.parse(dataDetail).PARAM_ID,
          paramName: JSON.parse(dataDetail).PARAM_NAME,
          locationID: JSON.parse(dataDetail).LOCATION_ID,
          note: JSON.parse(dataDetail).NOTE,
          statusCond: JSON.parse(dataDetail).STATUS_COND
        });
        if(this.state.statusCond=='V') {
          this.setState({
            status:true
          })
        } else {
          this.setState({
            status:false
          })
        }
      })
    }

    updateReportAlarm(){
        if (this.state.note == '') {
            alert('Masukkan Catatan');
        }
        else if (this.state.statusCond == null) {
            alert('Masukkan Kondisi');
        }
        else {
          this.updateAlarm()
        }
      }

    updateAlarm() {
            this.setState({
                visibleDialogSubmit:true
            })
            AsyncStorage.getItem("token").then(value => {
                var url = GlobalConfig.SERVERHOST + "api/v_mobile/firesystem/alarm_n/m_log_cek_alarm/update";
                var formData = new FormData();
                formData.append("token", value);
                formData.append("LOCATION_ID", this.state.locationID);
                formData.append("PARAM_ID", this.state.paramID);
                if(this.state.status==true) {
                  formData.append("STATUS_COND", 'V');
                } else {
                  formData.append("STATUS_COND", 'X');
                }
                formData.append("NOTE", this.state.note);
                formData.append("ID", this.state.id);
                fetch(url, {
                    headers: {
                        "Content-Type": "multipart/form-data"
                    },
                    method: "POST",
                    body: formData
                })
                    .then(response => response.json())
                    .then(response => {
                        if (response.Status == 200) {
                            this.setState({
                                visibleDialogSubmit:false
                            })
                            Alert.alert('Success', 'Update Alarm Success', [{
                                text: 'Okay'
                            }])
                            this.props.navigation.navigate('InspectionReportAlarmHistory')
                        } else {
                            this.setState({
                                visibleDialogSubmit:false
                            })
                            Alert.alert('Error', 'Update Alarm Failed', [{
                                text: 'Okay'
                            }])
                        }
                    })
                    .catch((error)=>{
                        console.log(error)
                        this.setState({
                            visibleDialogSubmit:false
                        })
                        Alert.alert('Error', 'Update Alarm Failed', [{
                            text: 'Okay'
                        }])
                    })
            })
    };

    onPressStatus() {
        let cond = !this.state.status;
        this.setState({
          status: cond
        });
    }

    render() {

        return (
            <Container style={styles.wrapper}>
                <Header style={styles.header}>
                    <Left style={{flex:1}}>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.navigate("InspectionReportAlarmHistory")}
                        >
                            <Icon
                                name="ios-arrow-back"
                                size={20}
                                style={styles.facebookButtonIconOrder2}
                            />
                        </Button>
                    </Left>
                    <Body style={{flex:3,alignItems:'center'}}>
                        <Title style={styles.textbody}>Update</Title>
                    </Body>

                    <Right style={{flex:1}}></Right>
                </Header>
                <StatusBar backgroundColor={colors.green03} barStyle="light-content" />

                <View style={{ flex: 1 }}>
                    <Content style={{ marginTop: 0 }}>
                        <View style={{ backgroundColor: '#FEFEFE' }}>
                        <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                        <View style={{marginTop: 5, marginBottom: 5,}}>
                          <View style={{ flex: 1, flexDirection: "row" }}>
                              <View style={{ width: '78%' }}>
                                  <Text style={{ fontSize: 10, paddingTop: 5}}>
                                  {this.state.paramName}
                                  </Text>
                              </View>
                              <View style={{ width: '22%' }}>
                                  <SwitchToggle
                                    containerStyle={{
                                      width: 70,
                                      height: 30,
                                      borderRadius: 25,
                                      padding: 5,
                                    }}
                                    backgroundColorOff={colors.graydar}
                                    backgroundColorOn='#00b300'
                                    circleStyle={{
                                      width: 22,
                                      height: 22,
                                      borderRadius: 19,
                                      backgroundColor: 'white',
                                    }}

                                    switchOn={this.state.status}
                                    onPress={() => this.onPressStatus()}
                                    circleColorOff='white'
                                    circleColorOn='white'
                                    duration={500}
                                  />
                              </View>
                          </View>
                          <View style={{ flex: 1 }}>
                              <View>
                                  <Textarea style={{
                                      marginBottom: 5,
                                      borderRadius: 5,
                                      paddingTop:10,
                                      fontSize: 11 }}
                                      rowSpan={2}
                                      bordered value={this.state.note}
                                      placeholder='Keterangan ...'
                                      onChangeText={(text) => this.setState({ note: text })} />
                              </View>
                          </View>
                          </View>
                        </CardItem>

                        <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray }}>
                          <View style={{ flex: 1}}>
                          <View style={styles.Contentsave}>
                            <Button
                              block
                              style={{
                                width:'100%',
                                height: 45,
                                marginBottom: 20,
                                borderWidth: 1,
                                backgroundColor: "#00b300",
                                borderColor: "#00b300",
                                borderRadius: 4
                              }}
                              onPress={() => this.updateReportAlarm()}
                            >
                              <Text style={{color:colors.white}}>SUBMIT</Text>
                            </Button>
                          </View>
                          </View>
                        </CardItem>
                    </View>
                    <View style={{ width: 270, position: "absolute" }}>
                        <Dialog
                            visible={this.state.visibleDialogSubmit}
                            dialogTitle={<DialogTitle title="Updating Alarm .." />}
                        >
                            <DialogContent>
                            {<ActivityIndicator size="large" color="#330066" animating />}
                            </DialogContent>
                        </Dialog>
                    </View>
                    </Content>
                </View>
            </Container>

        );
    }
}

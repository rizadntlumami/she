import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  TouchableOpacity,
  ActivityIndicator,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail,
  Textarea,
  Picker,
  Form
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import ChartView from "react-native-highcharts";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from "react-native-datepicker";

var that;

class DashboardUnitCheckPerformance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      visibleDialog: false,
      filterDate: "",
      filterType: "",
      filterStatus: "",
      arrStokApd: [0, 0, 0, 0, 0, 0, 0, 0, 0, 0],
      arrNegatif: [],
      arrVehicle: [],
      isloading: true,
      dataPlant: "",
      tempArrVehicle: [],
      tempArrCount: [],
      tempArrExist: [],
      arrJumlah: [],
      arrVehicle: [],
      arrExistNegatif: [],
      arrCountNegatif: [],

      vehicle:[],
      vehicleCode:[],
      vehicleCountNegatif:[],
      vehicleExistNegatif:[],
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this.loadData();
    this.setState({ isLoading: true });
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadPlant();
        this.loadData();
      }
    );
  }

  loadPlant() {
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + "api/Auth/user?token=" + value;

      fetch(url)
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson));
          this.setState({
            dataPlant: responseJson.data.data.plant
          });
          this.setState({
            isLoading: false
          });
          this.loadData(this.state.dataPlant);
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  loadData() {
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + "api/v_mobile/firesystem/unit_check/get_vehicle_n_list";
      var formData = new FormData();
      formData.append("token", value);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            vehicle: responseJson.Data,
            vehicleType:[],
            vehicleCountNegatif:[],
            vehicleExistNegatif:[]
          });
          for (x = 0; x < this.state.vehicle.length; x++) {
            this.state.vehicleCode.push(this.state.vehicle[x].VEHICLE_CODE)
            this.state.vehicleCountNegatif.push(this.state.vehicle[x].COUNT_NEGATIF_LIST)
            this.state.vehicleExistNegatif.push(this.state.vehicle[x].EXIST_NEGATIF_LIST)
          }
          this.setState({
            isloading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  render() {
    that = this;

    var Highcharts = "Highcharts";
    var conf = {
      chart: {
        type: "bar",
        animation: Highcharts.svg, // don't animate in old IE
        marginRight: 10
      },
      title: {
        text: "Report Vehicle Performances"
      },
      credits: {
        enabled: false
      },
      xAxis: {
        categories: this.state.vehicleCode,
        title: {
          text: "Vehicle Code"
        }
      },
      yAxis: {
        min: 0,
        title: {
          text: "Negatif List",
          align: "high"
        },
        labels: {
          overflow: "justify"
        }
      },
      tooltip: {
        valueSuffix: " negatif list"
      },
      plotOptions: {
        bar: {
          dataLabels: {
            enabled: true
          }
        }
      },
      // legend: {
      //   enabled: false
      // },
      legend: {
        layout: 'vertical',
        align: 'right',
        verticalAlign: 'top',
        // x: -20,
        y: 30,
        floating: true,
        borderWidth: 1,
        backgroundColor: colors.white,
        shadow: true
    },
      exporting: {
        enabled: false
      },
      series: [
        {
          name: "Count Negatif List",
          data: this.state.vehicleCountNegatif,
          color: "#ffff00"
        },
        {
          name: "Exist Negatif List",
          data: this.state.vehicleExistNegatif,
          color: colors.red
        }
      ]
    };

    const options = {
      global: {
        useUTC: false
      },
      lang: {
        decimalPoint: ",",
        thousandsSep: "."
      }
    };

    var list;
    if (this.state.isLoading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.vehicle == null) {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        list = (
          <ScrollView>
            <ChartView
              style={{ height: 800 }}
              config={conf}
              options={options}
              originWhitelist={[""]}
            />
          </ScrollView>
        );
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("FireMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex:3, alignItems:'center' }}>
            <Title style={styles.textbody}>Dashboard Unit Check</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Footer>
          <FooterTab style={styles.tabfooter}>
            <Button
              onPress={() =>
                this.props.navigation.navigate("DashboardUnitCheckPickup")
              }
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>
                Mobil Pick Up
              </Text>
            </Button>
            <Button
              onPress={() =>
                this.props.navigation.navigate("DashboardUnitCheckPMK")
              }
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>
                Mobil PMK
              </Text>
            </Button>
            <Button active style={styles.tabfooter}>
              <View style={{ height: "40%" }} />
              <View style={{ height: "50%" }}>
                <Text style={styles.textbody}>Performance</Text>
              </View>
              <View style={{ height: "20%" }} />
              <View
                style={{
                  borderWidth: 2,
                  marginTop: 2,
                  height: 0.5,
                  width: "100%",
                  borderColor: colors.white
                }}
              />
            </Button>
          </FooterTab>
        </Footer>

        <Content style={{ marginTop: 10 }}>{list}</Content>

        <CustomFooter navigation={this.props.navigation} menu="FireSystem" />
      </Container>
    );
  }
}

export default DashboardUnitCheckPerformance;

import React, { Component } from "react";
import { View, Text, Image,TouchableHighlight,TouchableOpacity,Dimensions,AsyncStorage } from "react-native";

export default class NewsContent extends Component {

  navigateToScreen(navigation,itemNews){
    AsyncStorage.setItem('itemNews', JSON.stringify(itemNews)).then(() => {
      navigation.navigate('DetailNews');
    })
  }

  render() {
    return (
      <View
        style={{
          height: 150,
          width: 130,
          marginTop: 10,
          marginLeft: 10,
          // borderWidth: 0,
          // borderColor: "#dddddd",
          elevation: 1
        }}
        
      >
        {/* <View style={{ flex: 4 }}> */}
        <TouchableOpacity style={{ flex: 4 }} onPress={() => this.navigateToScreen(this.props.navigation,this.props.newsList)}>
          <Image
            // source={this.props.imageUri}
            source={{uri: "http://10.15.5.150/dev/she/api" + this.props.newsList.GAMBAR}}
            style={{ flex: 1, width: null, height: null, resizeMode: "cover" ,borderRadius:5}}
          />
          </TouchableOpacity>
        {/* </View> */}
        <View style={{ flex: 1, paddingLeft: 10, paddingTop: 5}}>
          <Text numberOfLines={1} style={{fontSize:9,fontWeight: 'bold'}}>{this.props.newsList.JUDUL}</Text>
          <Text style={{fontSize:8}}>{this.props.newsList.KATEGORI} | {this.props.newsList.TGL_POSTING}</Text>
        </View>
      </View> 
    );
  }
}

import React, { Component } from "react";
import {
  View,
  Text,
  Image,
  TouchableHighlight,
  TouchableOpacity,
  Dimensions,
  ImageBackground,
  AsyncStorage,
  ScrollView,
  WebView,
  StyleSheet
} from "react-native";
import {
  Icon,
  Container,
  Header,
  Left,
  Body,
  Title,
  Button,
  Right
} from "native-base";
import GlobalConfig from "../components/GlobalConfig";
import styles from "./styles/FireMenu";
import colors from "../../styles/colors";
import Moment from 'moment';

export default class DetailNews extends Component {
  constructor(props) {
    super(props);
    this.state = {
      listNews: [],
      isLoading: true,
      idNews:'',
    };
  }
  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    AsyncStorage.getItem("idNews").then(idNews => {
      this.setState({ idNews: idNews });
      this.setState({ isLoading: false });
      this.loadNews()
    });

  }

  loadNews(){
    const url = GlobalConfig.SERVERHOST + 'getNews';
    var formData = new FormData();
    formData.append("id", this.state.idNews)
    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then(response => response.json())
      .then((responseJson) => {
          this.setState({
              listNews: responseJson.data,
              isLoading: false
          });

      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <Container style={styles.wrapper2}>
        <ScrollView>
          <ImageBackground
              style={{
              alignSelf: "center",
              width: Dimensions.get("window").width,
              height: 245,
          }}
            source={{ uri: GlobalConfig.IMAGEHOST + "News/" + this.state.listNews.image }}>
            <TouchableOpacity
                transparent
                style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:320}}
                onPress={()=>this.props.navigation.goBack()}
            >
              <View style={{backgroundColor:colors.black, opacity:1, width:30, height:30, borderRadius:30, alignItems:'center', justifyContent:'center'}}>
                <Icon
                    name='arrow-back'
                    size={10}
                    style={{color:colors.white, fontSize:20}}
                />
              </View>
            </TouchableOpacity>
          </ImageBackground>
          <View style={{borderTopLeftRadius:30, borderTopRightRadius:30, backgroundColor:colors.white, marginTop:-25, flex:1}}>
              <Text style={{fontSize:16, marginLeft:20, marginRight:20, color:colors.black, fontWeight:'bold', marginTop:15, marginBottom:5}}>{this.state.listNews.head}</Text>
          </View>

          <View style={{marginTop:0, height:1.2, backgroundColor:colors.green01, marginLeft:20, marginRight:20}}>
          </View>
                  <View style={{marginLeft:20, marginRight:20, marginTop:15, flex:1, flexDirection:'row'}}>
                    <View style={{width:'5%'}}>
                      <Icon
                        name='person'
                        style={{color:colors.green01, fontSize:15}}
                      />
                      <Icon
                        name='time'
                        style={{color:colors.green01, fontSize:15}}
                      />
                      <Icon
                        name='eye'
                        style={{color:colors.green01, fontSize:15}}
                      />
                    </View>
                    <View style={{width:'95%'}}>
                      <Text style={{fontSize:11}}>{this.state.listNews.writer}</Text>
                      <Text style={{fontSize:11}}>{Moment(this.state.listNews.date).format('DD MMMM YYYY')}</Text>
                      <Text style={{fontSize:11}}>350</Text>
                    </View>
                  </View>
                  <View style={{marginLeft:20, marginRight:20, marginTop:15, marginBottom:20}}>
                    <Text style={{fontSize:14}}>{this.state.listNews.content}</Text>
                  </View>
          </ScrollView>
      </Container>
    );
  }
}

const styleshtml = StyleSheet.create({
  a: {
    fontWeight: '300',
    color: '#FF3366', // make links coloured pink
  },
});

import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../styles/colors";

export default class ListViewSIOHistori extends Component {
    render() {
        return (
            <View
                style={{
                    height: 65,
                    width: "100%",
                }}
            >
                <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{ marginLeft: 5, marginTop: 5, width: '18%'}}>
                        <Image style={{marginTop:0, height: 50, width: 50, borderRadius:10}} source={{uri : this.props.image}}/>
                    </View>
                    <View style={{ marginLeft: 5, marginTop: 5, width: '52%'}}>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>{this.props.kelas}</Text>
                        <Text style={{ fontSize: 9, paddingTop: 5 }}>{this.props.noLisensi}</Text>
                        <Text style={{ fontSize: 9, paddingTop: 2 }}>{this.props.nama}</Text>
                    </View>
                    <View style={{ marginTop: 40, width: '30%'}}>
                        <Text style={{ fontSize: 9, textAlign: 'right', fontStyle: "italic", marginRight: 15 }}>{this.props.masaBerlaku}</Text>
                    </View>
                </View>
            </View>
        );
    }
}

import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../styles/colors";

export default class ListView extends Component {
  render() {
    return (
      <View style={{ flexDirection: "column", flex: 1 }}>
        <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{flex: 1}}>
                <Text style={{fontSize:10, fontWeight:"bold"}}>Area : {this.props.area}</Text>
                <Text style={{fontSize:10, fontWeight:"bold"}}>Sub Area : {this.props.subArea}</Text>
                <Text numberOfLines={1} style={{fontSize:9, paddingTop:5}}>Inspector : {this.props.inspector}</Text>
                <Text style={{fontSize:9, paddingTop:2}}>Plant : {this.props.plant}</Text>
            </View>
            <View style={{flex: 1}}>
                <Text style={{fontSize:8, fontStyle:"italic", textAlign:'right'}}>Report Date : {this.props.date}</Text>
                <Text style={{fontSize:8, fontWeight:"bold", textAlign:'right', color:colors.green01}}>Shift : {this.props.shift}</Text>
            </View>
        </View>
      </View>
    );
  }
}

import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TouchableOpacity,
    ActivityIndicator
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Label,
    Item,
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DateTimePicker from 'react-native-datepicker';
import DatePicker from 'react-native-datepicker';
import ImagePicker from "react-native-image-picker";
import Ripple from "react-native-material-ripple";
import CheckBox from 'react-native-check-box';
import Moment from 'moment'
import ListView from "../../components/ListView";

export default class DailyReportAktifitasCreate extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isLoading: true,
      pickedImage: '',
      uri: '',
      fileType:'',
      fileName: '',
      isCheckedShift1:false,
      isCheckedShift2:false,
      isCheckedShift3:false,
      date:'',
      time:'',
      typeAksi:'SIAGA',
      pic : '',
      picText:"",
      shift1:'',
      shift2:'',
      shift3:'',
      date:'',
      listPetugasShift1:[],
      listPetugasShift2:[],
      listPetugasShift3:[],
      tools:'',
      listTools:[],
      jumlahTools:'',
      cekStatusShift1:false,
      cekStatusShift2:false,
      cekStatusShift3:false,
      visibleDialogSubmit: false,
      save:false,
    };
  }


  pickImageHandler = () => {
      ImagePicker.showImagePicker({ title: "Pick an Image", maxWidth: 800, maxHeight: 600 }, res => {
          if (res.didCancel) {
              console.log("User cancelled!");
          } else if (res.error) {
              console.log("Error", res.error);
          } else {
              this.setState({
                  pickedImage: res.uri,
                  uri: res.uri,
                  fileType:res.type
              });

          }
      });
  }

  static navigationOptions = {
      header: null
  };

  _renderItem = ({ item }) => (
      <ListItem data={item}></ListItem>
  )

  componentDidMount() {
      this._onFocusListener = this.props.navigation.addListener(
        "didFocus",
        payload => {
          this.setState({
            visibleDialogSubmit: false
          });
        }
      );
      var dateNow = Moment().format('YYYY-MM-DD');
      this.setState({
          date:dateNow,
      })
      this.listPetugas();
  }

  listPetugas() {
      const url = GlobalConfig.SERVERHOST + 'getOfficer';
      var formData = new FormData();
      formData.append("date_activity", this.state.date)

      fetch(url, {
        headers: {
          'Content-Type': 'multipart/form-data'
        },
        method: 'POST',
        body: formData
      })
        .then((response) => response.json())
        .then((responseJson) => {
          var shift0 = responseJson.data.filter(x => x.shift == 0);
          var shift1 = responseJson.data.filter(x => x.shift == 1);
          var shift2 = responseJson.data.filter(x => x.shift == 2);
          var shift3 = responseJson.data.filter(x => x.shift == 3);
          if(this.state.cekStatusShift1==true) {
            this.setState({
              listPetugasShift1: shift1,
              isLoading: false,
              cekStatusShift1:false
            });
          }
          if(this.state.cekStatusShift2==true) {
            this.setState({
              listPetugasShift2: shift2,
              isLoading: false,
              cekStatusShift2:false
            });
          }
          if(this.state.cekStatusShift3==true) {
            this.setState({
              listPetugasShift3: shift3,
              isLoading: false,
              cekStatusShift3:false
            });
          }

        })
        .catch((error) => {
          console.log(error)
        })
  }

  statusShift1(){
    if(this.state.isCheckedShift1==false) {
      this.setState({
          cekStatusShift1: true,
      })
    } else {
      this.setState({
          listPetugasShift1: []
      })
    }
    this.listPetugas();
  }
  statusShift2(){
    if(this.state.isCheckedShift2==false) {
      this.setState({
          cekStatusShift2: true
      })
    } else {
      this.setState({
          listPetugasShift2: []
      })
    }
    this.listPetugas();
  }
  statusShift3(){
    if(this.state.isCheckedShift3==false) {
      this.setState({
          cekStatusShift3: true
      })
    } else {
      this.setState({
          listPetugasShift3: []
      })
    }
    this.listPetugas();
  }


  ubahJumlah(jumlah) {
    let arr = this.state.jumlahTools;
    arr = jumlah;
    this.setState({
      jumlahTools: arr
    });
  }

  tambahJumlah() {
    let arr = this.state.jumlahTools;
    arr++;
    this.setState({
      jumlahTools: arr
    });
    console.log(this.state.jumlahTools);
  }

  kurangJumlah() {
    let arr = this.state.jumlahTools;
    if (arr == 0 || arr == null || arr == undefined) {
    } else {
      arr--;
      this.setState({
        jumlahTools: arr
      });
    }
  }

  createAktifitas() {
    if(this.state.isCheckedShift1==true) {
      this.setState({shift1:'V'})
    } else {
      this.setState({shift1:''})
    }
    if(this.state.isCheckedShift2==true) {
      this.setState({shift2:'V'})
    } else {
      this.setState({shift2:''})
    }
    if(this.state.isCheckedShift3==true) {
      this.setState({shift3:'V'})
    } else {
      this.setState({shift3:''})
    }
    if (this.state.typeAksi == null) {
      alert('Input Type Aksi');
    }
    else if (this.state.timeStart == null) {
      alert('Input Time Start');
    }
    else if (this.state.timeEnd == null) {
      alert('Input Time End');
    }
    else if (this.state.aktifitas == null) {
      alert('Input Activity');
    }
    else if (this.state.pic == '') {
      alert('Input PIC');
    }
    else if (this.state.uri == '') {
      alert('Input Photo');
    }
    else {
      this.setState({
        save: true
      });
    }
  }

  createAktifitasFinal(){
    this.setState({
      visibleDialogSubmit: true,
      save:false,
    });
    var url = GlobalConfig.SERVERHOST + "addDailySectionActivity";
    var formData = new FormData();
    formData.append("type_action", this.state.typeAksi);
    formData.append("date_activity", this.state.date);
    formData.append("timesheet_start", this.state.timeStart);
    formData.append("timesheet_end", this.state.timeEnd);
    formData.append("daily_activity", this.state.aktifitas);
    formData.append("shift1", this.state.shift1);
    formData.append("shift2", this.state.shift2);
    formData.append("shift3", this.state.shift3);
    formData.append("pic", this.state.pic);
    formData.append("note", this.state.note);
    formData.append("company", '5000');
    formData.append("plant", '5001');
    formData.append("status", 'OPEN');
    formData.append("list_tools", JSON.stringify(this.state.listTools));
    formData.append("image", {
        uri: this.state.uri,
        type: this.state.fileType,
        name: 'dailyreport_' + this.state.date + '_' + this.state.pic,
    });
    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
    .then(response => response.json())
    .then(response => {
      if (response.status == 200) {
        this.setState({
          visibleDialogSubmit: false
        });
        Alert.alert('Success', 'Create Activity Success', [{
          text: 'Oke'
        }])
        this.props.navigation.navigate('DailyReportAktifitas')
      } else {
        this.setState({
          visibleDialogSubmit: false
        });
        Alert.alert('Error', 'Create Activity Failed', [{
          text: 'Oke'
        }])
      }
    });
  }

  tambahTools(){
    let tools = this.state.tools;
    let jumlahTools = this.state.jumlahTools;

    if(tools == '' || jumlahTools == ''){
      alert("Tools dan Jumlah Tidak Boleh Kosong")
    } else {
        var temp = {'tools': tools, 'total': jumlahTools};
        var data = this.state.listTools;
        data.push(temp);

        this.setState({
          listTools:data
        })
    }
    this.setState({
      jumlahTools:1,
      tools:''
    })
  }

  deleteTools(index){
    let arr=this.state.listTools;
    arr.splice(index,1)
    this.setState({
        listTools: arr
    })
  }

  tambahPIC(){
    alert("Input PIC from Shift")
  }

  pushPIC(name){
    this.setState({
      pic:name,
    })
    // var ketemu = false;
    // for (let index = 0; index < this.state.pic.length; index++) {
    //   if (name == this.state.pic[index]) {
    //     ketemu = true;
    //   }
    // }
    // if (ketemu == false) {
    //   this.state.pic.push(name);
    // } else {
    //   let idx = this.state.pic.indexOf(name);
    //   this.state.pic.splice(idx, 1);
    // }
    //
    // this.setState({
    //   picText:this.state.pic.toString()
    // })
    // console.log(this.state.pic)
    // console.log(this.state.picText)
  }

  render() {

    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DailyReportAktifitas")}
            >
              <Icon
                name="ios-arrow-back"
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex:3, alignItems:'center' }}>
            <Title style={styles.textbody}>Form Daily Activity</Title>
          </Body>
          <Right style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.createAktifitasFinal()}
            >
              <Icon
                name="ios-checkmark"
                style={{fontSize:40, color:colors.white}}
              />
            </Button>
          </Right>

        </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={{ flex: 1 }}>
        <Content style={{ marginTop: 0 }}>
          <View style={{ backgroundColor: '#FEFEFE' }}>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }}>
              <View style={{ flex: 1}}>
                <View>
                  <Text style={styles.titleInput}>Type Action *</Text>
                  <Form style={{borderWidth:1, borderRadius:5, marginRight:0, marginTop:5, marginLeft:0, borderColor:"#E6E6E6", height:35}}>
                      <Picker
                          mode="dropdown"
                          iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                          style={{ width: '100%', height:35}}
                          placeholder="Select Status ..."
                          placeholderStyle={{ color: "#bfc6ea" }}
                          placeholderIconColor="#007aff"
                          selectedValue={this.state.typeAksi}
                          onValueChange={(itemValue) => this.setState({typeAksi:itemValue})}>
                          <Picker.Item label="Siaga"  value="SIAGA" Sele/>
                          <Picker.Item label="Inspeksi"  value="INSPEKSI"/>
                          <Picker.Item label="Pemadaman"  value="PEMADAMAN"/>
                      </Picker>
                  </Form>
                </View>
              </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white }}>
                <View style={{ flex: 1, flexDirection: 'row'}}>
                  <View style={{width: '50%'}}>
                    <Text style={styles.titleInput}>Time Start *</Text>
                    <DateTimePicker
                        style={{ width: '100%', fontSize:10, borderRadius:20}}
                        date={this.state.timeStart}
                        mode="time"
                        placeholder="Choose Time ..."
                        format="hh:mm:ss"
                        minTime="00:00:00"
                        maxTime="23:59:59"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource='{this.timeIcon}'
                        customStyles={{
                          dateInput: {
                            marginLeft: 0, marginRight:10, height: 35, borderRadius:5, fontSize:10, borderWidth:1, borderColor:"#E6E6E6"
                          },
                          dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 5,
                          },
                        }}
                        onDateChange={(time) => { this.setState({ timeStart: time }) }}
                    />
                  </View>
                  <View style={{width: '50%'}}>
                    <Text style={styles.titleInput}>Time End *</Text>
                    <DateTimePicker
                        style={{ width: '100%', fontSize:10, borderRadius:20}}
                        date={this.state.timeEnd}
                        mode="time"
                        placeholder="Choose Time ..."
                        format="hh:mm:ss"
                        minTime="00:00:00"
                        maxTime="23:59:59"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        iconSource='{this.timeIcon}'
                        customStyles={{
                          dateInput: {
                            marginLeft: 0, marginRight:0, height: 35, borderRadius:5, fontSize:10, borderWidth:1, borderColor:"#E6E6E6"
                          },
                          dateIcon: {
                            position: 'absolute',
                            left: 0,
                            top: 5,
                          },
                        }}
                        onDateChange={(time) => { this.setState({ timeEnd: time }) }}
                    />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>Activity *</Text>
                  </View>
                  <View>
                  <Textarea style={ {borderRadius:5, marginBottom:5, fontSize:11}} rowSpan={3} bordered value={this.state.aktifitas} placeholder='Type something here ...' onChangeText={(text) => this.setState({ aktifitas: text })} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>Note</Text>
                  </View>
                  <View>
                  <Textarea style={ {borderRadius:5, marginBottom:5, fontSize:11}} rowSpan={3} bordered value={this.state.note} placeholder='Type something here ...' onChangeText={(text) => this.setState({ note: text })} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }}>
                <View style={{ flex: 1}}>
                  <Text style={styles.titleInput}>PIC *</Text>
                  <View style={{ flex: 1, flexDirection:'column', paddingTop:5}}>
                      <Button
                        block
                        onPress={() => this.tambahPIC()}
                        style={{ justifyContent: "flex-start", flex: 1, borderWidth: Platform.OS==='ios'?1:0, borderColor: Platform.OS==='ios'?colors.lightwhite:null, paddingLeft: 10, backgroundColor: colors.white, height: 35 }}>
                        <Text style={{fontSize:12}}>{this.state.pic}</Text>
                      </Button>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }} visible={this.state.visibleLoadingInspector}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View style={{width:'30%'}}>
                    <CheckBox
                      style={{flex:1}}
                      onClick={()=>{
                        this.setState({
                          isCheckedShift1:!this.state.isCheckedShift1,
                        })
                        this.statusShift1()
                      }}
                      isChecked={this.state.isCheckedShift1}
                      rightText={"Shift 1"}
                    />
                  </View>
                  <View style={{width:'30%'}}>
                    <CheckBox
                      style={{flex:1}}
                      onClick={()=>{
                        this.setState({
                          isCheckedShift2:!this.state.isCheckedShift2,
                        })
                        this.statusShift2()
                      }}
                      isChecked={this.state.isCheckedShift2}
                      rightText={"Shift 2"}
                    />
                  </View>
                  <View style={{width:'30%'}}>
                    <CheckBox
                      style={{flex:1}}
                      onClick={()=>{
                        this.setState({
                          isCheckedShift3:!this.state.isCheckedShift3,
                        })
                        this.statusShift3()
                      }}
                      isChecked={this.state.isCheckedShift3}
                      rightText={"Shift 3"}
                    />
                    </View>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }}>
                <View style={{ flex: 1 }}>
                <View style={{ backgroundColor: "#FEFEFE" }}>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{width:'50%'}}>
                        <Text style={styles.textTable}>Petugas</Text>
                    </View>
                    <View style={{width:'15%'}}>
                        <Text style={styles.textTable}>Shift</Text>
                    </View>
                    <View style={{width:'15%'}}>
                        <Text style={styles.textTable}>PIC</Text>
                    </View>
                    <View style={{width:'15%'}}>
                        <Text style={styles.textTable}>Pilih</Text>
                    </View>
                  </View>
                  {this.state.listPetugasShift1=='' ? (
                    <View style={{ backgroundColor: "#FEFEFE" }}>
                      <View style={{marginTop:5, marginBottom:5, marginLeft:10, marginRight:10, height:30, backgroundColor:colors.white, borderRadius:5}}>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <Text style={styles.textTable}>Petugas Shift 1 Belum Ada</Text>
                        </View>
                      </View>
                    </View>
                  ):(
                  <View>
                  {this.state.listPetugasShift1.map((listPetugas, index) => (
                    <View key={index} style={{ backgroundColor: "#FEFEFE" }}>
                      <View style={{marginTop:5, marginBottom:5, marginLeft:10, marginRight:10, height:30, backgroundColor:colors.white, borderRadius:5}}>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <View style={{width:'50%'}}>
                              <Text style={styles.textTable}>{listPetugas.officer}</Text>
                          </View>
                          <View style={{width:'20%'}}>
                              <Text style={styles.textTable}>{listPetugas.shift}</Text>
                          </View>
                          <View style={{width:'15%'}}>
                            {listPetugas.status_pic == 'V' ? (
                              <Icon
                                name="ios-checkmark"
                                style={styles.iconClose}
                              />
                            ) : (
                              <Icon
                                name=""
                                style={styles.iconClose}
                              />
                            )}

                          </View>
                          <View style={{width:'15%'}}>
                              <Ripple
                                style={{
                                  flex: 2,
                                  justifyContent: "center",
                                  alignItems: "center"
                                }}
                                rippleSize={176}
                                rippleDuration={600}
                                rippleContainerBorderRadius={15}
                                onPress={() =>
                                  this.pushPIC(listPetugas.officer)
                                }
                                rippleColor={colors.accent}
                              >
                                <Icon
                                  name="ios-checkbox-outline"
                                  style={styles.iconClose}
                                />
                              </Ripple>
                          </View>
                        </View>
                      </View>
                    </View>
                  ))}
                  </View>
                  )}

                  {this.state.listPetugasShift2=='' ? (
                    <View style={{ backgroundColor: "#FEFEFE" }}>
                      <View style={{marginTop:5, marginBottom:5, marginLeft:10, marginRight:10, height:30, backgroundColor:colors.white, borderRadius:5}}>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <Text style={styles.textTable}>Petugas Shift 2 Belum Ada</Text>
                        </View>
                      </View>
                    </View>
                  ):(
                  <View>
                  {this.state.listPetugasShift2.map((listPetugas, index) => (
                    <View key={index} style={{ backgroundColor: "#FEFEFE" }}>
                      <View style={{marginTop:5, marginBottom:5, marginLeft:10, marginRight:10, height:30, backgroundColor:colors.white, borderRadius:5}}>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <View style={{width:'50%'}}>
                              <Text style={styles.textTable}>{listPetugas.officer}</Text>
                          </View>
                          <View style={{width:'20%'}}>
                              <Text style={styles.textTable}>{listPetugas.shift}</Text>
                          </View>
                          <View style={{width:'15%'}}>
                            {listPetugas.status_pic == 'V' ? (
                              <Icon
                                name="ios-checkmark"
                                style={styles.iconClose}
                              />
                            ) : (
                              <Icon
                                name=""
                                style={styles.iconClose}
                              />
                            )}

                          </View>
                          <View style={{width:'15%'}}>
                              <Ripple
                                style={{
                                  flex: 2,
                                  justifyContent: "center",
                                  alignItems: "center"
                                }}
                                rippleSize={176}
                                rippleDuration={600}
                                rippleContainerBorderRadius={15}
                                onPress={() =>
                                  this.pushPIC(listPetugas.officer)
                                }
                                rippleColor={colors.accent}
                              >
                                <Icon
                                  name="ios-checkbox-outline"
                                  style={styles.iconClose}
                                />
                              </Ripple>
                          </View>
                        </View>
                      </View>
                    </View>
                  ))}
                  </View>
                  )}

                  {this.state.listPetugasShift3=='' ? (
                    <View style={{ backgroundColor: "#FEFEFE" }}>
                      <View style={{marginTop:5, marginBottom:5, marginLeft:10, marginRight:10, height:30, backgroundColor:colors.white, borderRadius:5}}>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <Text style={styles.textTable}>Petugas Shift 3 Belum Ada</Text>
                        </View>
                      </View>
                    </View>
                  ):(
                  <View>
                  {this.state.listPetugasShift3.map((listPetugas, index) => (
                    <View key={index} style={{ backgroundColor: "#FEFEFE" }}>
                      <View style={{marginTop:5, marginBottom:5, marginLeft:10, marginRight:10, height:30, backgroundColor:colors.white, borderRadius:5}}>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <View style={{width:'50%'}}>
                              <Text style={styles.textTable}>{listPetugas.officer}</Text>
                          </View>
                          <View style={{width:'20%'}}>
                              <Text style={styles.textTable}>{listPetugas.shift}</Text>
                          </View>
                          <View style={{width:'15%'}}>
                            {listPetugas.status_pic == 'V' ? (
                              <Icon
                                name="ios-checkmark"
                                style={styles.iconClose}
                              />
                            ) : (
                              <Icon
                                name=""
                                style={styles.iconClose}
                              />
                            )}

                          </View>
                          <View style={{width:'15%'}}>
                              <Ripple
                                style={{
                                  flex: 2,
                                  justifyContent: "center",
                                  alignItems: "center"
                                }}
                                rippleSize={176}
                                rippleDuration={600}
                                rippleContainerBorderRadius={15}
                                onPress={() =>
                                  this.pushPIC(listPetugas.officer)
                                }
                                rippleColor={colors.accent}
                              >
                                <Icon
                                  name="ios-checkbox-outline"
                                  style={styles.iconClose}
                                />
                              </Ripple>
                          </View>
                        </View>
                      </View>
                    </View>
                  ))}
                  </View>
                  )}
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }}>
                <View style={{ flex: 1 }}>
                  <View style={{ flex: 1, flexDirection: "row"}}>
                    <View style={{width:'50%'}}>
                      <Text style={styles.titleInput}>Tools</Text>
                    </View>
                  </View>

                  <View style={{ flex: 1, flexDirection: "row"}}>
                      <View style={{flex: 1, marginRight:20}}>
                        <Textarea style={{borderRadius:5, fontSize:10}} rowSpan={1.5} bordered value={this.state.tools} placeholder='Saafety Tools ...' onChangeText={(text) => this.setState({ tools: text })} />
                      </View>
                      <View style={{ flex: 1, flexDirection: "row"}}>
                        <Button onPress={() => this.kurangJumlah()} style={styles.btnQTYLeft}>
                            <Icon
                              name="ios-arrow-back"
                              style={styles.facebookButtonIconQTY}
                            />
                        </Button>

                        <View style={{width:30, height:30, marginTop: 10, backgroundColor: colors.white}}>
                            <Input
                              style={{
                                height:30,
                                marginTop:0,
                                fontSize:9,
                                textAlign:'center'}}
                              keyboardType='numeric'
                              value={this.state.jumlahTools + ""}
                              onChangeText={text => this.ubahJumlah(text)}
                            />
                        </View>
                        <Button onPress={() => this.tambahJumlah()} style={styles.btnQTYRight}>
                            <Icon
                              name="ios-arrow-forward"
                              style={styles.facebookButtonIconQTY}
                            />
                        </Button>
                        <Button onPress={() => this.tambahTools()} style={{backgroundColor:colors.green01,height:30,marginTop: 10,marginLeft:10,paddingLeft:10,paddingRight:10}}>
                            <Text style={{ fontSize:10, fontWeight:'bold', textAlign:'right',color:colors.white}}>Tambah</Text>
                        </Button>
                      </View>
                    </View>
                  </View>

              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }}>
                <View style={{ flex: 1 }}>
                <View style={{ backgroundColor: "#FEFEFE" }}>
                  <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{width:'50%'}}>
                        <Text style={styles.textTable}>Nama Tools</Text>
                    </View>
                    <View style={{width:'30%'}}>
                        <Text style={styles.textTable}>Jumlah</Text>
                    </View>
                    <View style={{width:'15%'}}>
                        <Text style={styles.textTable}>Cancel</Text>
                    </View>
                  </View>
                  {this.state.listTools.map((listTools, index) => (
                    <View key={index} style={{ backgroundColor: "#FEFEFE" }}>
                      <View style={{marginTop:5, marginBottom:5, marginLeft:10, marginRight:10, height:30, backgroundColor:colors.white, borderRadius:5}}>
                        <View style={{ flex: 1, flexDirection: "row" }}>
                          <View style={{width:'50%'}}>
                              <Text style={{paddingLeft:10, fontSize:11, paddingTop:5}}>{listTools.tools}</Text>
                          </View>
                          <View style={{width:'35%'}}>
                              <Text style={{paddingLeft:10, fontSize:11, paddingTop:5}}>{listTools.total}</Text>
                          </View>
                          <View style={{width:'15%'}}>
                              <Ripple
                                style={{
                                  flex: 2,
                                  justifyContent: "center",
                                  alignItems: "center"
                                }}
                                rippleSize={176}
                                rippleDuration={600}
                                rippleContainerBorderRadius={15}
                                onPress={() => this.deleteTools(index)}
                                rippleColor={colors.accent}
                              >
                                <Icon
                                  name="ios-close"
                                  style={styles.iconClose}
                                />
                              </Ripple>
                          </View>
                        </View>
                      </View>
                    </View>
                  ))}
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }}>
                  <View style={{ flex: 1}}>
                      <View style={{paddingBottom:5}}>
                        <Text style={styles.titleInput}>Photo</Text>
                      </View>
                      <View>
                        <Ripple
                          style={{
                            flex: 2,
                            justifyContent: "center",
                            alignItems: "center"
                          }}
                          rippleSize={176}
                          rippleDuration={600}
                          rippleContainerBorderRadius={15}
                          onPress={this.pickImageHandler}
                          rippleColor={colors.accent}
                        >
                        <View style={styles.placeholder}>
                            <Image source={{uri:this.state.pickedImage}} style={styles.previewImage}/>
                        </View>
                        </Ripple>
                      </View>
                  </View>
              </CardItem>
          </View>
        </Content>
      </View>
      <View style={{ width: '100%', position: "absolute"}}>
        <Dialog
          visible={this.state.save}
          dialogAnimation={
            new SlideAnimation({
              slideFrom: "bottom"
            })
          }
          dialogStyle={{ position: "absolute", top: this.state.posDialog, width:300}}
          onTouchOutside={() => {
            this.setState({ login: false });
          }}
        >
        <DialogContent>
        {
          <View>
            <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
              <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Do you want to save this inspection ?</Text>
            </View>
            <View style={{flexDirection:'row', flex:1, paddingTop:10, width:'100%'}}>
              <View style={{width:'50%', paddingRight:5}}>
                  <Button
                    block
                    style={{
                      width:'100%',
                      marginTop:10,
                      height: 35,
                      marginBottom: 5,
                      borderWidth: 0,
                      backgroundColor: colors.primer,
                      borderRadius: 15
                    }}
                    onPress={() => this.createAktifitas()}
                  >
                    <Text style={{color:colors.white}}>Yes</Text>
                  </Button>
              </View>
              <View style={{width:'50%', paddingLeft:5}}>
                  <Button
                    block
                    style={{
                      width:'100%',
                      marginTop:10,
                      height: 35,
                      marginBottom: 5,
                      borderWidth: 0,
                      backgroundColor: colors.primer,
                      borderRadius: 15
                    }}
                    onPress={() => this.setState({
                      save:false,
                    })}
                  >
                    <Text style={{color:colors.white}}>No</Text>
                  </Button>
              </View>
            </View>
          </View>
        }
        </DialogContent>
        </Dialog>
      </View>
      <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialogSubmit}
          >
            <DialogContent> {
                <View>
                  <View style={{alignItems:'center', justifyContent:'center', paddingTop:10, width:'100%'}}>
                    <Text style={{fontSize:15, alignItems:'center', color:colors.black}}>Creating Activity ...</Text>
                  </View>
                  <ActivityIndicator size="large" color="#330066" animating />
                </View>
            }
            </DialogContent>
          </Dialog>
        </View>
      <CustomFooter navigation={this.props.navigation} menu='FireSystem' />
    </Container>

    );
  }
}

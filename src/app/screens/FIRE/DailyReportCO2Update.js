import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TouchableOpacity,
    ActivityIndicator,
    Slider,
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Label,
    Item,
    Thumbnail
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DateTimePicker from 'react-native-datepicker';
import DatePicker from 'react-native-datepicker';
import ImagePicker from "react-native-image-picker";
import Ripple from "react-native-material-ripple";
import CheckBox from 'react-native-check-box';
import moment from 'moment'
import ListView from "../../components/ListView";

class ListItemPegawai extends React.PureComponent {
    render() {
        return (
            <View style={{ backgroundColor: "#FEFEFE" }}>
                <Ripple
                    style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() =>
                        this.props.setSelectedPegawaiLabel(this.props.data)
                    }
                    rippleColor={colors.accent}
                >
                    <CardItem
                        style={{
                            borderRadius: 0,
                            marginTop: 4,
                            backgroundColor: colors.white
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                marginTop: 4,
                                backgroundColor: colors.white
                            }}
                        >
                            <View >
                                <Text style={{ fontSize: 12 }}>{this.props.data.mk_nopeg} - {this.props.data.mk_nama}</Text>
                            </View>
                        </View>
                    </CardItem>
                </Ripple>
            </View>
        );
    }
}

export default class DailyReportCO2Update extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isLoading: true,
      date:'',
      visibleLoadingPegawai: false,
      visibleSearchListPegawai: false,
      selectedPegawaiLabel: '',
      searchWordPegawai: '',
      pegawaiName: '',
      pegawaiBadge: '',
      pegawaiUnitKerja: '',

      listCO2:[],
      plant:'',
      pplant:'',
      pplantdesc:'',
      total_max_cemetron:'',
      total_residual_cemetron:'',
      total_max_samator:'',
      total_residual_samator:'',
      inspection_date:'',
      inspection_time:'',
      shift:'',
      note:'',
    };
  }


  static navigationOptions = {
      header: null
  };


  _renderItem = ({ item }) => (
    <ListItem data={item}></ListItem>
  )

  componentDidMount() {
      AsyncStorage.getItem('list').then((listCO2) => {
          this.setState({
            listCO2: JSON.parse(listCO2),
            plant: JSON.parse(listCO2).plant,
            pplant: JSON.parse(listCO2).pplant,
            pplantdesc: JSON.parse(listCO2).pplantdesc,
            total_max_cemetron: JSON.parse(listCO2).total_max_cemetron,
            residualCemetron: JSON.parse(listCO2).total_residual_cemetron,
            total_max_samator: JSON.parse(listCO2).total_max_samator,
            residualSamator: JSON.parse(listCO2).total_residual_samator,
            inspection_date: JSON.parse(listCO2).inspection_date,
            inspection_time: JSON.parse(listCO2).inspection_time,
            pegawaiName: JSON.parse(listCO2).pic_name,
            pegawaiBadge: JSON.parse(listCO2).pic_badge,
            shift: JSON.parse(listCO2).shift,
            note: JSON.parse(listCO2).note,
          });

          this.setState({ isLoading: false });
      })
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      });
  }

  onChangePegawai(text) {
      this.setState({
          searchWordPegawai: text
      })
      this.setState({
          visibleLoadingPegawai: true
      })
          const url = GlobalConfig.SERVERHOST + 'getEmployee';
          var formData = new FormData();
          formData.append("searchEmp", this.state.searchWordPegawai)
          fetch(url, {
              headers: {
                  'Content-Type': 'multipart/form-data'
              },
              method: 'POST',
              body: formData
          })
              .then((response) => response.json())
              .then((responseJson) => {
                  this.setState({
                      visibleLoadingPegawai: false
                  })
                  this.setState({
                      listPegawai: responseJson.data,
                      listPegawaiMaster: responseJson.data,
                      isloading: false
                  });
              })
              .catch((error) => {
                  this.setState({
                      visibleLoadingPegawai: false
                  })
                  console.log(error)
              })
  }

  onClickSearchPegawai() {
      console.log('masuk')
      if (this.state.visibleSearchListPegawai) {
          this.setState({
              visibleSearchListPegawai: false
          })
      } else {
          this.setState({
              visibleSearchListPegawai: true
          })
      }
  }

  setSelectedPegawaiLabel(pegawai) {
      this.setState({
          pegawaiBadge: pegawai.mk_nopeg,
          pegawaiName: pegawai.mk_nama,
          pegawaiUnitKerja: pegawai.muk_nama,
          pegawaiKodeUnit: pegawai.muk_kode,
          visibleSearchListPegawai: false,
      })
  }

  _renderItemPegawai= ({ item }) => <ListItemPegawai data={item} setSelectedPegawaiLabel={text => this.setSelectedPegawaiLabel(text)} />;


  ubahResidualCemetron(jumlah) {
    let res = this.state.residualCemetron;
    res = jumlah;
    this.setState({
      residualCemetron: res
    });
  }

  tambahResidualCemetron() {
    let res = this.state.residualCemetron;
    if (res >= 6) {
    } else {
      res = res + 1;
      this.setState({
        residualCemetron: res
      });
    }
  }

  kurangResidualCemetron() {
    let res = this.state.residualCemetron;
    if (res == 0 || res == null || res == undefined) {
    } else {
      res = res - 1;
      this.setState({
        residualCemetron: res
      });
    }
  }

  ubahResidualSamator(jumlah) {
    let res = this.state.residualSamator;
    res = jumlah;
    this.setState({
      residualSamator: res
    });
  }

  tambahResidualSamator() {
    let res = this.state.residualSamator;
    if (res >= 19) {
    } else {
      res = res + 1;
      this.setState({
        residualSamator: res
      });
    }
  }

  kurangResidualSamator() {
    let res = this.state.residualSamator;
    if (res == 0 || res == null || res == undefined) {
    } else {
      res = res - 1;
      this.setState({
        residualSamator: res
      });
    }
  }

  updateReportCO2() {
  if (this.state.note == null) {
    alert('Input Note');
  }
  else {
      var url = GlobalConfig.SERVERHOST + "updateInspectionGas";
      var formData = new FormData();
      formData.append("id", this.state.listCO2.id);
      formData.append("plant", this.state.plant);
      formData.append("pplant", this.state.pplant);
      formData.append("pplantdesc", this.state.pplantdesc);
      formData.append("total_max_cemetron", this.state.total_max_cemetron);
      formData.append("total_residual_cemetron", this.state.residualCemetron);
      formData.append("total_max_samator", this.state.total_max_samator);
      formData.append("total_residual_samator", this.state.residualSamator);
      formData.append("inspection_date", this.state.inspection_date);
      formData.append("inspection_time", this.state.inspection_time);
      formData.append("pic_name", this.state.pegawaiName);
      formData.append("pic_badge", this.state.pegawaiBadge);
      formData.append("shift", this.state.shift);
      formData.append("note", this.state.note);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        if (response.status == 200) {
          Alert.alert('Success', 'Update Report CO2 Success', [{
            text: 'Oke'
          }])
          this.props.navigation.navigate('DailyReportCO2Detail')
        } else {
          Alert.alert('Error', 'Update Report CO2 Failed', [{
            text: 'Oke'
          }])
        }
      });
    }
  }

  render() {

    return (
      <Container style={styles.wrapper2}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DailyReportCO2")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex:3, alignItems:'center' }}>
            <Title style={styles.textbody}>Update Inspection CO2</Title>
          </Body>
          <Right style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.updateReportCO2()}
            >
              <Icon
                name="ios-checkmark"
                style={{fontSize:40, color:colors.white}}
              />
            </Button>
          </Right>
        </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={{ flex: 1 }}>
        <Content style={{ marginTop: 0 }}>
          <View style={{ backgroundColor:colors.white }}>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }}>
              <View style={{ flex: 1}}>
                <View>
                  <Text style={styles.titleInput}>PPlant *</Text>
                  <View style={{ flex: 1, flexDirection: 'column' }}>
                      <Button
                          block
                          style={{ justifyContent: "flex-start", flex: 1, borderRadius:5, borderWidth: 0, paddingLeft: 10, marginRight: 5, marginLeft: 5, backgroundColor: colors.white, height: 35 }}>
                          <Text style={{ fontSize: 12 }}>{this.state.pplantdesc}</Text>
                      </Button>
                  </View>
                </View>
              </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.white }}>
                  <View style={{ flex: 1 }}>
                      <View>
                          <Text style={styles.titleInput}>Pic *</Text>
                      </View>
                      <View style={{ flex: 1, flexDirection: 'column' }}>
                          <Button
                              block
                              style={{ justifyContent: "flex-start", flex: 1, borderWidth: Platform.OS==='ios'?1:0, borderColor: Platform.OS==='ios'?colors.lightwhite:null, borderRadius:5, paddingLeft: 10, marginRight: 5, marginLeft: 5, backgroundColor: colors.white, height: 35 }}
                              onPress={() => this.onClickSearchPegawai()}>
                              <Text style={{ fontSize: 12 }}>{this.state.pegawaiBadge} - {this.state.pegawaiName}</Text>
                          </Button>
                      </View>
                      {this.state.visibleSearchListPegawai &&
                          <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.white, margin: 5, borderColor: "#E6E6E6", }}>
                              <View>
                                  <Textarea value={this.state.searchWordPegawai} style={{ marginLeft: 5, marginRight: 5, fontSize: 11 }} bordered rowSpan={1.5} onChangeText={(text) => this.onChangePegawai(text)} placeholder='Ketik nama karyawan' />
                              </View>
                              <FlatList
                                  data={this.state.listPegawai}
                                  renderItem={this._renderItemPegawai}
                                  keyExtractor={(item, index) => index.toString()}
                              />
                          </View>
                      }
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>Note *</Text>
                  </View>
                  <View>
                  <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={3} bordered value={this.state.note} placeholder='Type something here ...' onChangeText={(text) => this.setState({ note: text })} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, backgroundColor:colors.white }}>
                <View style={{flex:1}}>
                  <View style={{ flex: 1, flexDirection: "row", marginTop:0, backgroundColor:colors.white }}>
                      <View style={{width:'50%'}}>
                          <Text style={{fontSize:10}}>Residual Cemetron</Text>
                          <View style={{ flex: 1, flexDirection: "row", paddingLeft:'20%'}}>
                            <Button onPress={() => this.kurangResidualCemetron()} style={styles.btnQTYLeft}>
                                <Icon
                                  name="ios-arrow-back"
                                  style={styles.facebookButtonIconQTY}
                                />
                            </Button>
                            <View style={{width:40, height:30, marginTop: 10, backgroundColor: colors.white}}>
                              <Input
                                style={{
                                  height:30,
                                  marginTop:0,
                                  fontSize:9,
                                  textAlign:'center'}}
                                value={this.state.residualCemetron + ""}
                                keyboardType='numeric'
                                onChangeText={text => this.ubahResidualCemetron(text)}
                              />
                            </View>

                            <Button onPress={() => this.tambahResidualCemetron()} style={styles.btnQTYRight}>
                              <Icon
                                name="ios-arrow-forward"
                                style={styles.facebookButtonIconQTY}
                              />
                            </Button>

                          </View>
                      </View>
                      <View style={{width:'50%'}}>
                          <Text style={{fontSize:10}}>Residual Samator</Text>
                          <View style={{ flex: 1, flexDirection: "row", paddingLeft:'20%'}}>
                            <Button onPress={() => this.kurangResidualSamator()} style={styles.btnQTYLeft}>
                                <Icon
                                  name="ios-arrow-back"
                                  style={styles.facebookButtonIconQTY}
                                />
                            </Button>
                            <View style={{width:40, height:30, marginTop: 10, backgroundColor: colors.white}}>
                              <Input
                                style={{
                                  height:30,
                                  marginTop:0,
                                  fontSize:9,
                                  textAlign:'center'}}
                                value={this.state.residualSamator + ""}
                                keyboardType='numeric'
                                onChangeText={text => this.ubahResidualSamator(text)}
                              />
                            </View>

                            <Button onPress={() => this.tambahResidualSamator()} style={styles.btnQTYRight}>
                              <Icon
                                name="ios-arrow-forward"
                                style={styles.facebookButtonIconQTY}
                              />
                            </Button>
                          </View>
                      </View>
                  </View>
                </View>
              </CardItem>
          </View>
        </Content>
      </View>
      <View style={{ width: 270, position: "absolute" }}>
          <Dialog visible={this.state.visibleLoadingUnitKerja}>
            <DialogContent>
              {
                <ActivityIndicator size="large" color="#330066" animating />
              }
            </DialogContent>
          </Dialog>
      </View>
      <CustomFooter navigation={this.props.navigation} menu='FireSystem' />
    </Container>

    );
  }
}

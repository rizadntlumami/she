import React, { Component } from "react";
import { View, Text, Image } from "react-native";
import colors from "../../styles/colors";

export default class ListViewLotto extends Component {
    render() {
        return (
            <View style={{ flexDirection: "column", flex: 1 }}>
                <View style={{ flex: 1, flexDirection: "row" }}>
                    <View style={{  flex:1 }}>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>Equipment Number : {this.props.equipmentNumber}</Text>
                        <Text style={{ fontSize: 10}}>Area : {this.props.area}</Text>
                        <Text style={{ fontSize: 10}}>No Area : {this.props.noER}</Text>
                        <Text style={{ fontSize: 10}}>Kegiatan : {this.props.kegiatan}</Text>
                    </View>
                    <View style={{  flex:1 }}>
                        <Text style={{fontSize:8, textAlign:'right'}}>Tanggal : {this.props.date}</Text>
                        
                    </View>
                </View>
            </View>
                
            
        );
    }
}

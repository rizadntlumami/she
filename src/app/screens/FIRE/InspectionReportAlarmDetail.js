import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Switch,
  ListItem
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import SwitchToggle from "react-native-switch-toggle";
import Ripple from "react-native-material-ripple";

class ListItemDetail extends React.PureComponent {
  render() {
    return (
      <View style={{ flex: 1, width:25 }}>
        {this.props.data != null ? (
          <Text
            style={{ fontSize: 10, fontWeight: "bold", textAlign: "right" }}
          >
            {this.props.data.STATUS_COND}
          </Text>
        ) : (
          <Text
            style={{ fontSize: 10, fontWeight: "bold", textAlign: "right" }}
          >
            -
          </Text>
        )}
      </View>
    );
  }
}

class ListItemParameterWeek extends React.PureComponent {
  _renderItemDetail = ({ item }) => <ListItemDetail data={item} />;

  render() {
    return (
        <View style={{ marginTop: 0, paddingTop: 10}}>
          <View style={{ flex: 1, flexDirection: "row" }}>
            <View style={{ width: "60%" }}>
              <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                {this.props.data.PARAMETER_NAME}
              </Text>
            </View>
            <View style={{ width: "40%" }}>
              {/*<View style={{ flex: 1, flexDirection: "row" }}>*/}
                <FlatList
                  horizontal={true}
                  data={this.props.data.DETAIL}
                  renderItem={this._renderItemDetail}
                  keyExtractor={(item, index) => index.toString()}
                />
              {/*</View>*/}
            </View>
          </View>
        </View>
    );
  }
}

class ListItemParameterMonth extends React.PureComponent {
  render() {
    return (
        <View style={{ marginTop: 0, paddingTop: 10 }}>
          <View style={{flex: 1,
              flexDirection: "row",}}
          >
            <View style={{ width: "80%", justifyContent: "center" }}>
              <Text
                style={{
                  fontSize: 10,
                  fontWeight: "bold",
                  marginLeft: 0,
                  alignItems: "center"
                }}
              >
                {this.props.data.PARAMETER_NAME}
              </Text>
            </View>
            <View style={{ width: "20%", justifyContent: "center" }}>
              {this.props.data.DETAIL.BULAN != null ? (
                <Text
                  style={{
                    fontSize: 10,
                    fontWeight: "bold",
                    textAlign: "right",
                    marginRight: 10
                  }}
                >
                  {this.props.data.DETAIL.BULAN.STATUS_COND}
                </Text>
              ) : (
                <Text
                  style={{
                    fontSize: 10,
                    fontWeight: "bold",
                    textAlign: "right"
                  }}
                >
                  Belum di Cek
                </Text>
              )}
            </View>
          </View>
        </View>
    );
  }
}

class InspectionReportAlarmDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      dataDetail: [],
      isLoading: true,
      listReportAlarm: [],
      listParameterWeek: [],
      listParameterMonth: [],
      idLokasi: "",
      typePeriode: "",
      paramID: "",
      visibleDialogSubmit: false,
      periode: ""
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItemParameterWeek = ({ item }) => (
    <ListItemParameterWeek data={item} />
  );

  _renderItemParameterMonth = ({ item }) => (
    <ListItemParameterMonth data={item} />
  );

  componentDidMount() {
    AsyncStorage.getItem("idLokasi").then(idLokasi => {
      AsyncStorage.getItem("periode").then(periode => {
        this.setState({ idLokasi: idLokasi, periode: periode });
        this.setState({ isLoading: false });
        console.log(this.state.idLokasi);
        console.log(this.state.periode);
      });
    });
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
        // AsyncStorage.getItem("idLokasi").then(idLokasi => {
        //   AsyncStorage.getItem("periode").then(periode => {
        //     this.setState({ idLokasi: idLokasi, periode: periode });
        //     this.setState({ isLoading: false });
        //     console.log(this.state.idLokasi);
        //     console.log(this.state.periode);
        //   });
        // });
      }
    );
  }

  loadData() {
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/firesystem/alarm_n/get_all_loc_alarm";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID", this.state.idLokasi);
      formData.append("periode", this.state.periode);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listReportAlarm: responseJson.Data,
            typePeriode: responseJson.Data[0].TYPE_PERIODE,
            paramID: responseJson.Data[0].PARAM_ID,
            isLoading: false
          });
        });
    });
    this.loadParameterWeek();
    this.loadParameterMonth();
  }

  loadParameterWeek() {
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/firesystem/alarm_n/view_parameter_type/WEEK";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("periode", this.state.periode);
      formData.append("param_id", this.state.idLokasi);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listParameterWeek: responseJson.data,
            isLoading: false
          });
        });
    });
  }

  loadParameterMonth() {
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/firesystem/alarm_n/view_parameter_type/MONTH";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("periode", this.state.periode);
      formData.append("param_id", this.state.idLokasi);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            listParameterMonth: responseJson.data,
            isLoading: false
          });
        });
    });
  }

  konfirmasideleteAlarm(kondisi, id) {
    Alert.alert(
      "Apakah Anda Yakin Ingin Menghapus Inspection Report Alarm",
      kondisi,
      [
        { text: "Yes", onPress: () => this.deleteAlarm(id) },
        { text: "Cancel" }
      ],
      { cancelable: false }
    );
  }

  navigateToScreen(route, listReport) {
    AsyncStorage.setItem("listReport", JSON.stringify(listReport)).then(() => {
      this.props.navigation.navigate(route);
    });
    //alert(JSON.stringify(listReport))
  }

  navigateToScreenHistory(route, idLokasi, periode) {
    AsyncStorage.setItem("idLokasi", idLokasi).then(() => {
      AsyncStorage.setItem("periode", periode).then(() => {
        this.props.navigation.navigate(route);
      });
    });
  }

  deleteAlarm(id) {
    this.setState({
      visibleDialogSubmit: true
    });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST +
        "api/v_mobile/firesystem/alarm_n/m_log_cek_alarm/delete";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("ID", id);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.Status == 200) {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Success", "Delete Success", [
              {
                text: "Oke"
              }
            ]);
            this.props.navigation.navigate("InspectionReportAlarm");
          } else {
            this.setState({
              visibleDialogSubmit: false
            });
            Alert.alert("Error", "Delete Failed", [
              {
                text: "Oke"
              }
            ]);
          }
        });
    });
  }

  _renderItemParameterWeek = ({ item }) => (
    <ListItemParameterWeek data={item} />
  );

  _renderItemParameterMonth = ({ item }) => (
    <ListItemParameterMonth data={item} />
  );
  render() {
    var that = this;
    return (
      <Container style={styles.wrapper}>
        {this.state.listReportAlarm.map((listReport, index) => (
          <View key={index}>
            <Header style={styles.header}>
              <Left style={{ flex: 1 }}>
                <Button
                  transparent
                  onPress={() =>
                    that.props.navigation.navigate("InspectionReportAlarm")
                  }
                >
                  <Icon
                    name="ios-arrow-back"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button>
              </Left>
              <Body style={{ flex: 3, alignItems: "center" }}>
                <Title style={styles.textbody}>
                  {listReport.KONDISI_VALUE}
                </Title>
              </Body>
              <Right style={{ flex: 1 }}>
                {listReport.STATUS_COND == "V" && (
                  <View style={{ flex: 0, flexDirection: "row" }}>
                    <Button
                  transparent
                  onPress={() =>
                    this.navigateToScreenHistory('InspectionReportAlarmHistory', this.state.idLokasi, this.state.periode)}
                >
                  <Icon
                    name="ios-alarm"
                    size={20}
                    style={styles.facebookButtonIconOrder2}
                  />
                </Button>
                    <Button
                      transparent
                      onPress={() =>
                        this.konfirmasideleteAlarm(
                          listReport.KONDISI_VALUE,
                          listReport.ID
                        )
                      }
                    >
                      <Icon
                        name="ios-trash"
                        size={20}
                        style={styles.facebookButtonIconOrder2}
                      />
                    </Button>
                  </View>
                )}
              </Right>
            </Header>
            <StatusBar
              backgroundColor={colors.green03}
              barStyle="light-content"
            />
            <ScrollView>
              <Content style={{ marginTop: 5 }}>
                {listReport.STATUS_COND != "V" ? (
                  <View>
                    <CardItem style={{ borderRadius: 0, marginTop: 5 }}>
                      <View>
                        <Text style={{ fontSize: 10 }}>Area</Text>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          {listReport.KONDISI_VALUE}
                        </Text>
                        <Text style={{ fontSize: 10, paddingTop: 10 }}>
                          Lokasi
                        </Text>
                        <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                          {listReport.TYPE_KONDISI}
                        </Text>
                        <Text style={{ fontSize: 10, paddingTop: 10 }}>
                          Status
                        </Text>
                        <Text
                          style={{
                            fontSize: 10,
                            fontWeight: "bold",
                            color: "#FF0101"
                          }}
                        >
                          Belum Diperiksa
                        </Text>
                      </View>
                    </CardItem>
                    <Button
                      block
                      style={{
                        height: 45,
                        marginLeft: 20,
                        marginRight: 20,
                        marginBottom: 20,
                        borderWidth: 1,
                        backgroundColor: "#00b300",
                        borderColor: "#00b300",
                        borderRadius: 4
                      }}
                      onPress={() =>
                        this.navigateToScreen(
                          "InspectionReportAlarmCreate",
                          listReport
                        )
                      }
                    >
                      <Text
                        style={{
                          fontSize: 14,
                          fontWeight: "bold",
                          color: colors.white
                        }}
                      >
                        Lakukan Pemeriksaan
                      </Text>
                    </Button>
                  </View>
                ) : (
                  <View>
                    <View style={{}}>
                      <CardItem style={{ borderRadius: 0 }}>
                        <View>
                          <Text style={{ fontSize: 10 }}>Periode</Text>
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            {listReport.YEAR} - {listReport.MONTH}
                          </Text>
                          <Text style={{ fontSize: 10, paddingTop: 10 }}>
                            Status
                          </Text>
                          <Text
                            style={{
                              fontSize: 10,
                              fontWeight: "bold",
                              color: colors.green01
                            }}
                          >
                            Sudah Diperiksa
                          </Text>
                          <Text style={{ fontSize: 10, paddingTop: 10 }}>
                            Tanggal Periksa
                          </Text>
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            {listReport.TANGGAL_CEK_AKHIR}
                          </Text>
                          <Text style={{ fontSize: 10, paddingTop: 10 }}>
                            Petugas Periksa
                          </Text>
                          <Text style={{ fontSize: 10, fontWeight: "bold" }}>
                            {listReport.CREATE_BY}
                          </Text>
                        </View>
                      </CardItem>
                    </View>

                    <CardItem>
                      <View>
                        <Text style={{ fontSize: 10 }}>Data Pengecekan Mingguan</Text>
                        <View style={{ flex: 1, flexDirection: "column", paddingTop: 5}}>
                          <View style={{ flex: 1, flexDirection: "row" }}>
                            <View style={{ width: "60%" }}>
                              <Text style={{ fontSize: 10, textAlign: "center" }}>Parameter</Text>
                            </View>
                            <View style={{ width: "40%" }}>
                              <View style={{ flex: 1, flexDirection: "row" }}>
                                <View style={{ flex: 1 }}>
                                  <Text style={{fontSize: 10, textAlign: "center"}}>1</Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                  <Text style={{fontSize: 10, textAlign: "center"}}>2</Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                  <Text style={{fontSize: 10, textAlign: "center"}}>3</Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                  <Text style={{fontSize: 10, textAlign: "center"}}>4</Text>
                                </View>
                                <View style={{ flex: 1 }}>
                                  <Text style={{fontSize: 10, textAlign: "center"}}>5</Text>
                                </View>
                              </View>
                            </View>
                          </View>
                          <FlatList
                            horizontal={false}
                            data={this.state.listParameterWeek}
                            renderItem={this._renderItemParameterWeek}
                            keyExtractor={(item, index) => index.toString()}
                          />
                        </View>
                      </View>
                    </CardItem>
                    <CardItem>
                      <View>
                        <Text style={{ fontSize: 10 }}>
                          Data Pengecekan Bulanan
                        </Text>
                        <FlatList
                          horizontal={false}
                          data={this.state.listParameterMonth}
                          renderItem={this._renderItemParameterMonth}
                          keyExtractor={(item, index) => index.toString()}
                        />
                      </View>
                    </CardItem>
                  </View>
                )}
                <View style={{ width: 270, position: "absolute" }}>
                  <Dialog
                    visible={this.state.visibleDialogSubmit}
                    dialogTitle={
                      <DialogTitle title="Delete Inspection Report Alarm .." />
                    }
                  >
                    <DialogContent>
                      {
                        <ActivityIndicator
                          size="large"
                          color="#330066"
                          animating
                        />
                      }
                    </DialogContent>
                  </Dialog>
                </View>
              </Content>
            </ScrollView>
          </View>
        ))}
      </Container>
    );
  }
}

export default InspectionReportAlarmDetail;

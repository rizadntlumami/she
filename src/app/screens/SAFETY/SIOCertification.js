import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  AsyncStorage,
  FlatList,
  Alert,
  ActivityIndicator,
  RefreshControl,
  Dimensions
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Card,
  CardItem,
  Body,
  Fab,
  Icon,
  Item,
  Input,
  Thumbnail
} from "native-base";

import CustomRadioButton from "../../components/CustomRadioButton";
import Dialog, { DialogTitle, SlideAnimation, DialogContent, DialogButton } from 'react-native-popup-dialog';
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from "../../components/GlobalConfig";
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import ListViewSIO from "../../components/ListViewSIO";
import Ripple from "react-native-material-ripple";

class ListItem extends React.PureComponent {
  navigateToScreen(route, listSIOCertification) {
    AsyncStorage.setItem("listSIO", JSON.stringify(listSIOCertification)).then(
      () => {
        this.props.navigation.navigate(route);
      }
    );
  }

  render() {
    var arrNoPeg=this.props.data.BADGE.split('');
    var noPeg=''
    var zeroStop=false
    for (let i=0;i<arrNoPeg.length;i++){
      if (arrNoPeg[i]!='0'){
        zeroStop=true
      } 
      if (zeroStop){
        noPeg+=arrNoPeg[i]
      }
    }
    return (
      <View>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.navigateToScreen("SIOCertificationDetail", this.props.data)
          }
          rippleColor={colors.accent}
        >
          <View style={{ backgroundColor: "#FEFEFE" }}>
            <Card style={{ marginLeft: 20, marginRight: 20, borderRadius: 10 }}>
              <View style={{ marginTop: 5, marginLeft: 5, marginRight: 5 }}>
                <ListViewSIO
                  imageSIO={GlobalConfig.SERVERHOST + "api" + this.props.data.FOTO}
                  pemegang={noPeg+" - "+this.props.data.NAMA}
                  start={this.props.data.MASA_AWAL}
                  end={this.props.data.MASA_BERLAKU}
                  unitkerja={this.props.data.UNIT_KERJA_TXT}
                />
              </View>
            </Card>
          </View>
        </Ripple>
      </View>
    );
  }
}

class SIOCertification extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      isLoading: true,
      searchText:'',
      option_name:["Nama", "Tanggal Berlaku Awal", "Tanggal Berlaku Akhir"],
      order_name:["NAMA", "MASA_AWAL", "MASA_BERLAKU"],
      option_type:["A-Z", "Z-A"],
      order_type:["asc", "desc"],
      listClickedRad:[],
      listClickedRadType:[],
      selected_order_name:'',
      selected_order_type:''
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item }) => <ListItem data={item} navigation={this.props.navigation} />;

  onRefresh = () =>{
    console.log('refreshing')
    this.setState({ isLoading: true }, function() {
      this.setState({
        searchText:''
      })
      this.loadData();
    });
  }

  componentDidMount() {
    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.setState({
          searchText:''
        })
        this.loadData();
      }
    );
  }

  searchData(){
    this.setState({
      isLoading: true
    });
  AsyncStorage.getItem("token").then(value => {
    const url =
      GlobalConfig.SERVERHOST + "api/v_mobile/safety/sertification/view/sio";
    var formData = new FormData();
    formData.append("token", value);
    formData.append("search", this.state.searchText);

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.data!=undefined){
          this.setState({
            dataSource: responseJson.data,
            isLoading: false
          });
        }else{
          this.setState({
            dataSource:[],
            isLoading: false
          });
        }

      })
      .catch(error => {
          this.setState({
            dataSource:[],
              isLoading: false
            });
        console.log(error);
      });
  });
  }

  loadData() {
    this.setState({
        isLoading: true
      });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/safety/sertification/view/sio";
      var formData = new FormData();
      formData.append("token", value);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.data!=undefined){
            this.setState({
              dataSource: responseJson.data,
              isLoading: false
            });
          }else{
            this.setState({
              dataSource:[],
              isLoading: false
            });
          }

        })
        .catch(error => {
            this.setState({
              dataSource:[],
                isLoading: false
              });
          console.log(error);
        });
    });
  }

  filterData() {
    this.setState({
        isLoading: true,
        visibleSort:false
      });
    AsyncStorage.getItem("token").then(value => {
      const url =
        GlobalConfig.SERVERHOST + "api/v_mobile/safety/sertification/view/sio";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("order_by", this.state.selected_order_name);
      formData.append("order_type", this.state.selected_order_type);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          if (responseJson.data!=undefined){
            this.setState({
              dataSource: responseJson.data,
              isLoading: false
            });
          }else{
            this.setState({
              dataSource:[],
              isLoading: false
            });
          }

        })
        .catch(error => {
            this.setState({
              dataSource:[],
                isLoading: false
              });
          console.log(error);
        });
    });
  }

  selectedRadioButton(clickedIndex){
    this.setState({
      selected_order_name:this.state.order_name[clickedIndex]
    })
    console.log(this.state.order_name[clickedIndex])

      var tempclickRad = [];
      for (let i = 0; i < this.state.option_name.length; i++) {
        if (i == clickedIndex) {
          tempclickRad.push(true);
        } else {
          tempclickRad.push(false);
        }
      }
      this.setState({
        listClickedRad: tempclickRad
      });

  }

  selectedRadioButton2(clickedIndex){
    this.setState({
      selected_order_type:this.state.order_type[clickedIndex]
    })
    console.log(this.state.order_type[clickedIndex])

      var tempclickRad = [];
      for (let i = 0; i < this.state.option_type.length; i++) {
        if (i == clickedIndex) {
          tempclickRad.push(true);
        } else {
          tempclickRad.push(false);
        }
      }
      this.setState({
        listClickedRadType: tempclickRad
      });

  }

  render() {
    var listSIO;
    if (this.state.isLoading) {
      listSIO = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.dataSource.length == 0) {
        listSIO = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Data!</Text>
          </View>
        );
      } else {
        listSIO =
        <FlatList
        data={this.state.dataSource}
        renderItem={this._renderItem}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={
          <RefreshControl
            refreshing={this.state.isLoading}
            onRefresh={this.onRefresh.bind(this)}
          />}
      />
      }
    }
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("SafetyMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:3,alignItems:'center'}}>
            <Title style={styles.textbody}>Tools & SIO</Title>
          </Body>

            <Right style={{flex:1}}/>

        </Header>

        <Footer style={styles.tabHeight}>
          <FooterTab style={styles.tabfooter}>
            <Button
              onPress={() =>
                this.props.navigation.navigate("ToolsCertification")
              }
            >
              <Text style={{ color: "white", fontWeight: "bold" }}>Tools</Text>
            </Button>
            <Button active style={styles.tabfooter}>
              <View style={{ height: "40%" }} />
              <View style={{ height: "50%" }}>
                <Text style={styles.textbody}>SIO</Text>
              </View>
              <View style={{ height: "20%" }} />
              <View
                style={{
                  borderWidth: 2,
                  marginTop: 2,
                  height: 0.5,
                  width: "100%",
                  borderColor: colors.white
                }}
              />
            </Button>
          </FooterTab>
        </Footer>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={styles.searchTab}>
                    <View style={{ flex: 1, flexDirection: 'row' }}>
                        <View style={styles.viewLeftHeader}>
                            <Item style={styles.searchItem}>
                                <Input style={{ fontSize: 15 }} placeholder="Type something here" value={this.state.searchText} onChangeText={(text) => this.setState({ searchText: text })} />
                                <Icon name="ios-search" style={{ fontSize: 30, paddingLeft: 0 }} onPress={() => this.searchData()} />
                                <Icon name="ios-reorder" style={{ fontSize: 30, paddingLeft: 0 }} onPress={() => this.setState({visibleSort:true})} />
                            </Item>
                        </View>
                    </View>
                </View>
        <View style={{ flex: 1, flexDirection: "column" ,marginTop:60}}>{listSIO}</View>
        <Fab
          active={this.state.active}
          direction="up"
          containerStyle={{}}
          style={{ backgroundColor: colors.green01, marginBottom: ((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?80:50}}
          position="bottomRight"
          onPress={() =>
            this.props.navigation.navigate("CreateSIOCertification")
          }
        >
          <Icon name="ios-add" style={{ fontSize: 50, fontWeight: "bold" ,paddingTop: Platform.OS === 'ios' ? 25:null,  }} />
        </Fab>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleSort}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            onTouchOutside={() => {
              this.setState({ visibleSort: false });
            }}
            dialogStyle={{ position: 'absolute', width:"70%" }}
          >
            <DialogContent
              style={{
                backgroundColor: colors.white
              }}
            >
            <View>
              <View style={{marginBottom:10,marginTop:20,alignItems:'center',backgroundColor:colors.gray}}>
                <Text style={{color:colors.black}}>Order By</Text>
              </View>
              <View style={{flexDirection: "column",marginBottom:20 ,marginLeft:20}}>
                {this.state.option_name.map((option, index) => (
                  <CustomRadioButton 
                    name={option} 
                    selected={this.state.listClickedRad[index] ? true : false}
                    onPress={() => this.selectedRadioButton(index)}/>
                ))}
              </View>

              <View style={{marginBottom:10,marginTop:10,alignItems:'center',backgroundColor:colors.gray}}>
                <Text style={{color:colors.black}}>Order Type</Text>
              </View>
              <View style={{flexDirection: "column",marginBottom:20,marginLeft:20 }}>
                {this.state.option_type.map((option, index) => (
                  <CustomRadioButton 
                    name={option} 
                    selected={this.state.listClickedRadType[index] ? true : false}
                    onPress={() => this.selectedRadioButton2(index)}/>
                ))}
              </View> 
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Button
                    block
                    style={{
                      height: 45,
                      marginLeft: 20,
                      marginRight: 20,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: colors.green0,
                      borderColor: colors.green0,
                      borderRadius: 4
                    }}
                    onPress={() => this.filterData()}
                  >
                    <Text style={styles.buttonText}>Filter</Text>
                  </Button>
                </View>
              </View>
            </View>     
            </DialogContent>
          </Dialog>
        </View>
        <CustomFooter navigation={this.props.navigation} menu="Safety" />
      </Container>
    );
  }
}

export default SIOCertification;

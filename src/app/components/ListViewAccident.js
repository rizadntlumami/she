import React, { Component } from "react";
import { View, Text, Image, ImageBackground } from "react-native";
import { Thumbnail } from "native-base";
import colors from '../../styles/colors';

export default class ListViewAccident extends Component {

  render() {
    var arrNoPeg=this.props.nopeg.split('');
    var noPeg=''
    var zeroStop=false
    for (let i=0;i<arrNoPeg.length;i++){
      if (arrNoPeg[i]!='0'){
        zeroStop=true
      } 
      if (zeroStop){
        noPeg+=arrNoPeg[i]
      }
    }
    console.log(noPeg)

    return (
      <View
        style={{
          height: 85,
          width: "100%",
        }}
      >
        <View style={{ flex: 1, flexDirection: "row" }}>
          <View style={{ marginLeft: 20, marginTop: 10, flex: 2 }}>
            <Text style={{ fontSize: 10, fontWeight: "bold" }}>{noPeg} - {this.props.namapeg}</Text>
            <Text style={{ fontSize: 9, paddingTop: 2 }}>{this.props.position} ({this.props.statpos})</Text>
            <Text style={{ fontSize: 9, paddingTop: 2 }}>{this.props.post}</Text>
            {/* <Image style={{marginTop:10, height: 200, width: 300, borderColor: colors.gray, borderWidth: 3}} source={{uri : this.props.image_bef}}/> */}
          </View>
          <View style={{ marginTop: 10, marginRight: 20, flex: 1 }}>
            {this.props.status == 'OPEN' && (
              <Text
                note
                style={{
                  alignSelf: "flex-end",
                  fontSize: 10,
                  fontWeight: 'bold',
                  color: "white",
                  backgroundColor: colors.green01,
                  paddingRight: 4,
                  paddingLeft: 4,
                  paddingBottom: 2,
                  paddingTop: 2,
                  borderRadius: 6
                }}
              >OPEN
                </Text>
            )}
            {this.props.status == 'CLOSE' && (
                <Text
                  note
                  style={{
                    alignSelf: "flex-end",
                    fontSize: 10,
                    fontWeight: 'bold',
                    color: "white",
                    backgroundColor: colors.red,
                    paddingRight: 4,
                    paddingLeft: 4,
                    paddingBottom: 2,
                    paddingTop: 2,
                    borderRadius: 6
                  }}
                >CLOSE
                </Text>)}
            {this.props.status == 'PROGRESS' && (
                <Text
                  note
                  style={{
                    alignSelf: "flex-end",
                    fontSize: 10,
                    fontWeight: 'bold',
                    color: "white",
                    backgroundColor: colors.green01,
                    paddingRight: 4,
                    paddingLeft: 4,
                    paddingBottom: 2,
                    paddingTop: 2,
                    borderRadius: 6
                  }}
                >PROGRESS
                </Text>)}
                <Text style={{ fontSize: 8, fontStyle: "italic", textAlign: 'right', marginTop: 20 }}>{this.props.date}</Text>
          </View>
        </View>
      </View>
    );
  }
}

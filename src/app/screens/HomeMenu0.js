import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
  Alert,
  Platform
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
  Textarea
} from "native-base";
import LinearGradient from "react-native-linear-gradient";

import NewsContents from "../components/NewsContent";
import MenuItems from "../components/MenuItems";
import styles from "./styles/HomeMenu";
import colors from "../../styles/colors";
import GlobalConfig from "../components/GlobalConfig";
import CustomFooter from "../components/CustomFooter";

const helmet = require("../../assets/images/helmet.png");

export default class HomeMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      isLoading: true,
      token:'',
      name:'',
    };
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    this.setState({
      isLoading:false,
    })
    AsyncStorage.getItem("token").then(token => {
      const url = GlobalConfig.SERVERHOST + 'validasi';
      var formData = new FormData();
      formData.append("token", token)
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            name: responseJson.user.name,
            isloading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }


  logOut(){
    Alert.alert(
      'Confirmation',
      'Logout SHE Mobile?',
      [
        { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
        { text: 'Yes', onPress: () => this.methLogOut() },
      ],
      { cancelable: false }
    );
  }

  methLogOut(){
    this.props.navigation.navigate("LoggedOut")
  }

  Konfirmasi(){
    alert("Null")
  }

  render() {
    return this.state.isLoading ? (
      <View style={{ flex: 1, justifyContent: "center", alignItems: "center" }}>
        <ActivityIndicator size="large" color="#330066" animating />
      </View>
    ) : (
      <Container style={styles.wrapper}>
      <View style={styles.homeWrapper}>
        <View style={{ flex: 1, flexDirection: "column", backgroundColor: "#fff" }}>
          <View>
            <ScrollView>
              <StatusBar
                backgroundColor={colors.green03}
                barStyle="light-content"
              />
              <ImageBackground
                style={{
                  alignSelf: "stretch",
                  width: Dimensions.get("window").width,
                  height: (Dimensions.get("window").height) / 4,
                }}
                source={require("../../assets/images/headprof-img.jpg")}>
                <TouchableOpacity
                  transparent
                  style={{position:'absolute',top:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?40:20,right:20}}
                  onPress={()=>this.logOut()}
                >
                  <Icon
                    name='ios-exit'
                    size={20}
                    style={{color:colors.red}}
                  />
                </TouchableOpacity>
              </ImageBackground>
              <Card style={{ marginLeft: 25, marginRight: 25, borderRadius: 5, marginTop:-50, paddingLeft:10, paddingRight:10}}>


                <View style={{width:'33%', paddingTop:30}}>
                    <MenuItems
                      handleOnPress={() =>
                        this.Konfirmasi()
                      }
                      imageUri={require("../../assets/images/iconAPD.png")}
                      name="A.P.D"
                    />
                </View>
                <View style={{width:'33%', paddingTop:30}}>
                    <MenuItems
                      handleOnPress={() =>
                        this.Konfirmasi()
                      }
                      imageUri={require("../../assets/images/iconSafety.png")}
                      name="Safety"
                    />
                </View>
                <View style={{width:'33%', paddingTop:30}}>
                    <MenuItems
                      handleOnPress={() =>
                        this.props.navigation.navigate("FireMenu")
                      }
                      imageUri={require("../../assets/images/iconFireSystem.png")}
                      name="Fire System"
                    />
                </View>
              </Card>
            </ScrollView>
          </View>
        </View>
      {/*<CustomFooter navigation={this.props.navigation} menu="Home" />*/}
      </View>
      </Container>
      );
  }
}

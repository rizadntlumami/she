import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TouchableOpacity,
    ActivityIndicator

} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Label,
    Item
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import Ripple from "react-native-material-ripple";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/SafetyMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DatePicker from 'react-native-datepicker';
import CheckBox from 'react-native-check-box';

class ListItem extends React.PureComponent {

  render() {
    return (
      <View style={{ backgroundColor: "#FEFEFE" }}>
        <Ripple
          style={{
            flex: 2,
            justifyContent: "center",
            alignItems: "center"
          }}
          rippleSize={176}
          rippleDuration={600}
          rippleContainerBorderRadius={15}
          onPress={() =>
            this.props.setSelectedInspectorLabel(this.props.data.mk_nama,this.props.data.mk_nopeg)
          }
          rippleColor={colors.accent}
        >
          <CardItem
            style={{
              borderRadius: 0,
              marginTop: 4,
              backgroundColor: colors.gray
            }}
          >
            <View
              style={{
                flex: 1,
                flexDirection: "row",
                marginTop: 4,
                backgroundColor: colors.gray
              }}
            >
              <View >
                <Text style={{fontSize:12}}>{this.props.data.mk_nopeg} - {this.props.data.mk_nama}</Text>
              </View>
            </View>
          </CardItem>
        </Ripple>
      </View>
    );
  }
}


export default class CreateHeaderReportK3 extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      dataHeader: [],
      isLoading: true,
      nearmiss:'0',
      accident:'0',
      incident:'0',
      listPlant:[],
      listArea:[],
      listInspector:[],
      listInspectorMaster:[],
      isChecked:false,
      visibleLoadingInspector:false,
      visibleSearchListInspector:false,
      selectedInspectorLabel:'',
      searchWordInspector:'',
      jumlahNearmiss:'0',
      jumlahAccident:'0',
      jumlahIncident:'0',
      selectedShift:'',
      selectedPlant:'',
      selectedArea:'',
      visibleDialogSubmit:false
    };
  }

  static navigationOptions = {
      header: null
  };

  /*NEARMISS*/
  ubahJumlahNearmiss(jumlah) {
    let arr = this.state.jumlahNearmiss;
    arr = jumlah;
    this.setState({
      jumlahNearmiss: arr
    });
  }
  tambahJumlahNearmiss() {
    let arr = this.state.jumlahNearmiss;
    arr++;
    this.setState({
      jumlahNearmiss: arr
    });
    console.log(this.state.jumlahNearmiss);
  }
  kurangJumlahNearmiss() {
    let arr = this.state.jumlahNearmiss;
    if (arr == 0 || arr == null || arr == undefined) {
    } else {
      arr--;
      this.setState({
        jumlahNearmiss: arr
      });
    }
  }
  /*ACCIDENT*/
  ubahJumlahAccident(jumlah) {
    let arr = this.state.jumlahAccident;
    arr = jumlah;
    this.setState({
      jumlahAccident: arr
    });
  }
  tambahJumlahAccident() {
    let arr = this.state.jumlahAccident;
    arr++;
    this.setState({
      jumlahAccident: arr
    });
    console.log(this.state.jumlahAccident);
  }
  kurangJumlahAccident() {
    let arr = this.state.jumlahAccident;
    if (arr == 0 || arr == null || arr == undefined) {
    } else {
      arr--;
      this.setState({
        jumlahAccident: arr
      });
    }
  }
  /*INCIDENT*/
  ubahJumlahIncident(jumlah) {
    let arr = this.state.jumlahIncident;
    arr = jumlah;
    this.setState({
      jumlahIncident: arr
    });
  }
  tambahJumlahIncident() {
    let arr = this.state.jumlahIncident;
    arr++;
    this.setState({
      jumlahIncident: arr
    });
    console.log(this.state.jumlahIncident);
  }
  kurangJumlahIncident() {
    let arr = this.state.jumlahIncident;
    if (arr == 0 || arr == null || arr == undefined) {
    } else {
      arr--;
      this.setState({
        jumlahIncident: arr
      });
    }
  }


  componentDidMount() {
        this.loadData();
        this.loadPlant();
        this.loadArea();
        this.loadInspector();
  }

  loadData() {

  }

  loadPlant(){
    AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/pplant';
        var formData = new FormData();
        formData.append("token", value)
        formData.append("search", 0)
        formData.append("limit", 250)
        formData.append("plant", 0)
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listPlant: responseJson,
                    isloading: false
                });
            })
            .catch((error) => {
                console.log(error)
            })
    })
  }

  loadArea(){
    AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/inspection_area';
        var formData = new FormData();
        formData.append("token", value)
        formData.append("search", 0)
        formData.append("limit", 250)
        formData.append("plant", 0)
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listArea: responseJson,
                    isloading: false
                });
            })
            .catch((error) => {
                console.log(error)
            })
    })
  }

  loadInspector(){
    this.setState({
      visibleLoadingInspector:true
    })
    AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'api/v_mobile/list_master_data/search/employee';
        var formData = new FormData();
        formData.append("token", value)
        formData.append("search", this.state.searchWordInspector)
        formData.append("limit", 10)
        formData.append("plant", null)
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
              this.setState({
                visibleLoadingInspector:false
              })
                this.setState({
                    listInspector: responseJson,
                    listInspectorMaster: responseJson,
                    isloading: false
                });
            })
            .catch((error) => {
              this.setState({
                visibleLoadingInspector:false
              })
                console.log(error)
            })
    })
  }

  onChangeInspector(text){
    this.setState({
      searchWordInspector:text
    })
    this.loadInspector();
    // this.setState({
    //   listKaryawan:this.state.listKaryawanMaster.filter(x => x.mk_nama.includes(text)),
    // })
  }

  onClickSearchInspector(){
    console.log('masuk')
    if (this.state.visibleSearchListInspector){
      this.setState({
        visibleSearchListInspector:false
      })
    }else{
      this.setState({
        visibleSearchListInspector:true
      })
    }
  }

  setSelectedInspectorLabel(text,code){
    this.setState({
      selectedInspectorLabel:text,
      selectedInspectorCode:code,
      visibleSearchListInspector:false
    })
  }

  _renderItem = ({ item }) => <ListItem data={item} setSelectedInspectorLabel={(text,code) => this.setSelectedInspectorLabel(text,code)} />;

  createHeader()  {
    if (this.state.reportDate == null) {
      alert('Masukkan Tanggal Report');
    }
    else if (this.state.selectedShift == '') {
      alert('Masukkan Shift');
    }
    else if (this.state.selectedPlant == '') {
      alert('Masukkan Plant');
    }
    else if (this.state.selectedArea == '') {
      alert('Masukkan Area');
    }
    else if (this.state.subArea == null) {
      alert('Masukkan Sub Area');
    }
    else if (this.state.selectedInspectorLabel == '') {
      alert('Masukkan Nama Inspector');
    }
    else {
      this.setState({
        visibleDialogSubmit:true
      })
      AsyncStorage.getItem("token").then(value => {
        var url = GlobalConfig.SERVERHOST + "api/v_mobile/safety/unsafe_n/save_unsafe_report";
        var formData = new FormData();
        formData.append("token", value);
        formData.append("REPORT_DATE", this.state.reportDate);
        formData.append("SHIFT", this.state.selectedShift);
        formData.append("AREA_CODE", null);
        formData.append("AREA_TXT", this.state.selectedArea);
        formData.append("SUB_AREA_TXT", this.state.subArea);
        formData.append("PLANT_CODE", null);
        formData.append("PLANT_NAME", this.state.selectedPlant);
        formData.append("NEARMISS", this.state.jumlahNearmiss);
        formData.append("ACCIDENT", this.state.jumlahAccident);
        formData.append("INCIDENT", this.state.jumlahIncident);
        formData.append("INSPECTOR_CODE", this.state.isChecked==true?null:this.state.selectedInspectorCode);
        formData.append("INSPECTOR_NAME", this.state.selectedInspectorLabel);

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
        .then(response => response.json())
        .then(response => {
          if (response.Status == 200) {
            this.setState({
              visibleDialogSubmit:false
            })
            Alert.alert('Success', 'Create Header Success', [{
              text: 'Okay'
            }])
            this.props.navigation.navigate('DailyReportK3')
          } else {
            this.setState({
              visibleDialogSubmit:false
            })
            Alert.alert('Error', 'Create Header Failed', [{
              text: 'Okay'
            }])
          }
        })
        .catch((error)=>{
          this.setState({
            visibleDialogSubmit:false
          })
          Alert.alert('Error', 'Create Header Failed', [{
            text: 'Okay'
          }])
          console.log(error)
        })
    })
  }
};

  render() {
    let listPlant = this.state.listPlant.map( (s, i) => {
      return <Picker.Item key={i} value={'['+ s.PPLANT + ']' + ' ' + s.PPLANTDESC} label={s.PPLANTDESC} />
    });

    let listArea = this.state.listArea.map( (s, i) => {
      return <Picker.Item key={i} value={s.AREA_NAME} label={s.AREA_NAME} />
    });

    let listInspector = this.state.listInspector.map( (s, i) => {
      return <Picker.Item key={i} value={'['+ s.mk_unit + ']' + ' ' + s.mk_nama} label={s.mk_nama} />
    });

    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DailyReportK3")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:4,alignItems:'center'}}>
            <Title style={styles.textbody}>Create Daily Report K3</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={{ flex: 1 }}>
        <Content style={{ marginTop: 0 }}>
          <View style={{ backgroundColor: '#FEFEFE' }}>
              <CardItem style={{ borderRadius: 0, marginTop:4, backgroundColor:colors.gray }}>
                <View style={{ flex: 1}}>
                  <View>
                    <Text style={styles.titleInput}>Report Date *</Text>
                    <DatePicker
                      style={{ width: '100%', fontSize:10, borderRadius:20}}
                      date={this.state.reportDate}
                      mode="date"
                      placeholder="Choose Date ..."
                      format="YYYY-MM-DD"
                      minDate="2018-01-01"
                      maxDate="5000-12-31"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateInput: {
                          marginLeft: 5, marginRight:5, height: 35, borderRadius:5, fontSize:10, borderWidth:1, borderColor:"#E6E6E6"
                        },
                        dateIcon: {
                          position: 'absolute',
                          left: 0,
                          top: 5,
                        },
                      }}
                      onDateChange={(date) => { this.setState({ reportDate: date }) }} />
                  </View>
                </View>
              </CardItem>

              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Shift *</Text>
                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                            style={{ width: '100%', height:35}}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selectedShift}
                            onValueChange={(itemValue) => this.setState({selectedShift:itemValue})}>
                            <Picker.Item label="Choose Plant..."  value=""/>
                            <Picker.Item label="Shift I (Satu)"  value="1" />
                            <Picker.Item label="Shift II (Dua)"  value="2"/>
                            <Picker.Item label="Shift III (Tiga)"  value="3"/>
                        </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                  <View>
                    <Text style={styles.titleInput}>Plant *</Text>
                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                            style={{ width: '100%', height:35 }}
                            placeholder="Selectn a Plant ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selectedPlant}
                            onValueChange={(itemValue) => this.setState({selectedPlant:itemValue})}>
                            <Picker.Item label="Choose Plant..."  value=""/>
                            {listPlant}
                        </Picker>
                      </Form>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                  <View>
                    <Text style={styles.titleInput}>Area *</Text>
                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                            style={{ width: '100%', height:35 }}
                            placeholder="Select Area ..."
                            placeholderStyle={{ color: "#bfc6ea"}}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.selectedArea}
                            onValueChange={(itemValue) => this.setState({selectedArea:itemValue})}>
                            <Picker.Item label="Choose Area..."  value="" />
                            {listArea}
                        </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Sub Area *</Text>
                  </View>
                  <View>
                    <Textarea style={{borderRadius:5, marginLeft:5, marginRight:5, fontSize:10}} rowSpan={2} bordered value={this.state.subArea} placeholder='Type Subarea ...' onChangeText={(text) => this.setState({ subArea: text })} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1}}>
                  <View>
                    <Text style={styles.titleInput}>Inspector *</Text>
                    {this.state.isChecked == false
                      ? (
                        <View>
                        <View style={{ flex: 1,flexDirection:'column'}}>
                            <Button
                              block
                              style={{flex:1,borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, backgroundColor:colors.gray ,borderColor:"#E6E6E6", height:35}}
                              onPress={() => this.onClickSearchInspector()}>
                              <Text style={{fontSize:12}}>{this.state.selectedInspectorLabel}</Text>
                            </Button>
                        </View>
                        <View>
                          {this.state.visibleSearchListInspector &&
                          <View style={{height:300,flexDirection:'column',borderWidth:1,padding:10,backgroundColor:colors.gray,margin:5,borderColor:"#E6E6E6",}}>
                              <Form>
                                <Item stackedLabel style={{ marginLeft: 0 }}>
                                  <Input value={this.state.searchWordInspector} style={{borderRadius:5, marginLeft:5, marginRight:5, fontSize:10}} bordered onChangeText={(text) => this.onChangeInspector(text)} placeholder='Ketik nama karyawan' />
                                </Item>
                              </Form>
                              <FlatList
                              data={this.state.listInspector}
                              renderItem={this._renderItem}
                              keyExtractor={(item, index) => index.toString()}
                              />
                          </View>
                          }
                        </View>
                        </View>
                    ):(
                      <View>
                        <Textarea style={{borderRadius:5, marginLeft:5, marginRight:5, fontSize:10}} rowSpan={2} bordered value={this.state.selectedInspectorLabel} placeholder='Type Inspector ...' onChangeText={(text) => this.setState({ selectedInspectorLabel: text })} />
                      </View>
                    )}
                  </View>
                  <CheckBox
                    style={{flex:1, paddingTop:5}}
                    onClick={()=>{
                      this.setState({
                        isChecked:!this.state.isChecked,
                      })
                    }}
                    isChecked={this.state.isChecked}
                    rightText={"Non SMIG"}
                  />
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1, flexDirection: "row"}}>
                    <View style={{width:90}}>
                        <Text style={styles.titleInput}>Nearmiss * :</Text>
                    </View>
                    <Button onPress={() => this.kurangJumlahNearmiss()} style={styles.btnQTYLeft}>
                        <Icon
                          name="ios-arrow-back"
                          style={styles.facebookButtonIconQTY}
                        />
                    </Button>

                    <View style={{width:30,height:30, marginTop: 0, backgroundColor: colors.white}}>
                        <Input
                          style={{
                            height:30,
                            marginTop:-5,
                            fontSize:12,
                            textAlign:'center'}}
                          value={this.state.jumlahNearmiss + ""}
                          keyboardType='numeric'
                          onChangeText={text => this.ubahJumlahNearmiss(text)}
                        />
                    </View>
                    <Button onPress={() => this.tambahJumlahNearmiss()} style={styles.btnQTYRight}>
                        <Icon
                          name="ios-arrow-forward"
                          style={styles.facebookButtonIconQTY}
                        />
                    </Button>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1, flexDirection: "row"}}>
                    <View style={{width:90}}>
                        <Text style={styles.titleInput}>Accident * :</Text>
                    </View>
                    <Button onPress={() => this.kurangJumlahAccident()} style={styles.btnQTYLeft}>
                        <Icon
                          name="ios-arrow-back"
                          style={styles.facebookButtonIconQTY}
                        />
                    </Button>

                    <View style={{width:30,height:30, marginTop: 0, backgroundColor: colors.white}}>
                        <Input
                          style={{
                            height:30,
                            marginTop:-5,
                            fontSize:12,
                            textAlign:'center'}}
                          value={this.state.jumlahAccident + ""}
                          keyboardType='numeric'
                          onChangeText={text => this.ubahJumlahAccident(text)}
                        />
                    </View>
                    <Button onPress={() => this.tambahJumlahAccident()} style={styles.btnQTYRight}>
                        <Icon
                          name="ios-arrow-forward"
                          style={styles.facebookButtonIconQTY}
                        />
                    </Button>
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{width:90}}>
                    <Text style={styles.titleInput}>Incident * :</Text>
                </View>
                <View style={{ flex: 1, flexDirection: "row"}}>
                    <Button onPress={() => this.kurangJumlahIncident()} style={styles.btnQTYLeft}>
                        <Icon
                          name="ios-arrow-back"
                          style={styles.facebookButtonIconQTY}
                        />
                    </Button>

                    <View style={{width:30,height:30, marginTop: 0, backgroundColor: colors.white}}>
                        <Input
                          style={{
                            height:30,
                            marginTop:-5,
                            fontSize:12,
                            textAlign:'center'}}
                          value={this.state.jumlahIncident + ""}
                          keyboardType='numeric'
                          onChangeText={text => this.ubahJumlahIncident(text)}
                        />
                    </View>
                    <Button onPress={() => this.tambahJumlahIncident()} style={styles.btnQTYRight}>
                        <Icon
                          name="ios-arrow-forward"
                          style={styles.facebookButtonIconQTY}
                        />
                    </Button>
                  </View>
              </CardItem>
          </View>
          <View style={styles.Contentsave}>
          <Button
            block
            style={{
              height: 45,
              marginLeft: 20,
              marginRight: 20,
              marginBottom: 20,
              borderWidth: 1,
              backgroundColor: "#00b300",
              borderColor: "#00b300",
              borderRadius: 4
            }}
            onPress={() => this.createHeader()}
          >
            <Text style={{color:colors.white}}>SUBMIT</Text>
          </Button>
        </View>
        </Content>


      </View>
      <View style={{ width: 270, position: "absolute" }}>
          <Dialog visible={this.state.visibleLoadingInspector}>
            <DialogContent>
              {
                <ActivityIndicator size="large" color="#330066" animating />
              }
            </DialogContent>
          </Dialog>
      </View>
      <View style={{ width: 270, position: "absolute" }}>
        <Dialog
          visible={this.state.visibleDialogSubmit}
          dialogTitle={<DialogTitle title="Creating Header Report K3 .." />}
        >
          <DialogContent>
            {<ActivityIndicator size="large" color="#330066" animating />}
          </DialogContent>
        </Dialog>
      </View>
    </Container>

    );
  }
}

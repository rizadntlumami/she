import React, { Component } from "react";
import {
  StyleSheet,
  View,
  Image,
  StatusBar,
  ScrollView,
  FlatList,
  AsyncStorage,
  ActivityIndicator,
  Platform,
  Animated,
  AppRegistry,
  Alert,
  RefreshControl,
  Dimensions
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
  Icon
} from "native-base";

import GlobalConfig from '../../components/GlobalConfig';
import APDContent from "../../components/APDContent";
import MenuItems from "../../components/APDItems";
import styles from "../styles/OrderAPD";
import colors from "../../../styles/colors";
import SnackBar from "./SnackBarOrderUnitKerja";

class ListItem extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
    };
  }

  componentDidMount() {
    this.setState({
      listApdTroli: [],
    });

  }

  navigateToScreen(route, kodeAPD) {
    // alert(route);
    AsyncStorage.setItem('id', kodeAPD).then(() => {
      that.props.navigation.navigate(route);
    })
  }

  cekAPD(){
    var status=''
    if (this.props.data.uk!=1){
      status+='- Item tidak diperuntukkan order unit kerja \n'
    }
    if (this.props.data.a_stok==0){
      status+='- Tidak ada stok'
    }
    Alert.alert("APD Tidak Bisa di Order",status);
  }

  render() {
    return (
      <View style={{ backgroundColor: '#FEFEFE' ,width:'50%'}}>
        <Card style={{ marginLeft: 5, marginRight: 5, borderRadius: 0, flex:1 }}>
          <View style={styles.news}>
            {(this.props.data.foto_bef == null) ? (
              <APDContent
              imageUri={require("../../../assets/images/nopic.png")}
              name={this.props.data.nama}
              merk={this.props.data.merk}
              stok={this.props.data.a_stok}
            />
            ) : (
              <APDContent
                imageUri={{uri:GlobalConfig.IMAGEHOST + 'APD/' + this.props.data.foto_bef}}
                name={this.props.data.nama}
                merk={this.props.data.merk}
                stok={this.props.data.a_stok}
              />
            )}

              <View style={{ flex: 1, flexDirection: 'row' }}>

              {this.props.data.a_stok > 0 && this.props.data.uk == 1 ? (
              <Button style={{ backgroundColor: '#27ae60', width:'50%', height:25, marginTop: 2, borderRadius:0 }} onPress={() => this.props.showSnack(this.props.data)}>
                <Icon
                  name="ios-add"
                  style={styles.facebookButtonIconOrder}
                />
              </Button>
              ) : (
              <Button style={{ backgroundColor: '#27ae60', width:'50%', height:25, marginTop: 2, borderRadius:0 }} onPress={() => this.cekAPD()}>
                <Icon
                  name="ios-add"
                  style={styles.facebookButtonIconOrder}
                />
              </Button>
              )}

              {this.props.data.a_stok > 0 && this.props.data.uk == 1 ? (
              <Button style={{ backgroundColor: '#d8d8d8', width:'50%', height:25, marginTop: 2, borderRadius:0}} onPress={() => this.props.troli(this.props.data.kode)}>
                <Icon
                  name="ios-cart"
                  style={styles.facebookButtonIconOrderOrder}
                />
              </Button>
              ) : (
              <Button style={{ backgroundColor: '#d8d8d8', width:'50%', height:25, marginTop: 2, borderRadius:0}} onPress={() => this.cekAPD()}>
                <Icon
                  name="ios-cart"
                  style={styles.facebookButtonIconOrderOrder}
                />
              </Button>
              )}

              </View>
            </View>
        </Card>
      </View>
    )
  }
}

export default class OrderUnitKerjaAPD extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isloading: true,
      listApdNameChosen: [],
      listApdChosen: [],
      dataWishlist: [],
      listKodeWishlist: [],
      listClickedKatAPD: [],
      isEmpty: false,
      name:'',
      badge:'',
    };
  }

  static navigationOptions = {
    header: null
  };

  _renderItem = ({ item, index }) => (
    <ListItem data={item} index={index} showSnack={(nama) => this.DisplaySnackBar(nama)} troli={(kodeAPD) => this.troli(kodeAPD)}></ListItem>
  )

  // setClickedKategori(clickedIndex) {
  //   var tempclickAPD = [];
  //   for (let i = 0; i < 11; i++) {
  //     if (i == clickedIndex) {
  //       tempclickAPD.push(true);
  //     } else {
  //       tempclickAPD.push(false);
  //     }
  //   }
  //   this.setState({
  //     listClickedKatAPD: tempclickAPD
  //   });
  // }

  troli = kodeAPD => {
    var ketemu = false;
    for (let i = 0; i < this.state.listKodeWishlist.length; i++) {
      let tempApd = this.state.listKodeWishlist[i].kode_apd;
      if (kodeAPD == tempApd) {
        ketemu = true;
      }
    }
    if (ketemu == true) {
      alert("APD sudah ada di dalam keranjang!");
    } else {
      // var tempWishlistKode = this.state.listKodeWishlist;
      // tempWishlistKode.push(kodeAPD);
      // console.log(tempWishlistKode);
      // this.setState({
      //   listKodeWishlist: tempWishlistKode
      // });

      Alert.alert(
        'Confirmation',
        'Apakah anda ingin memasukkan dalam keranjang ?',
        [
          { text: 'No', onPress: () => console.log('Cancel Pressed'), style: 'cancel' },
          { text: 'Yes', onPress: () => this.inputWishlist(kodeAPD) },
        ],
        { cancelable: false }
      );
    }
  };

  inputWishlist(kodeAPD){
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + "orderapd/wishlist/create";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("no_badge", this.state.badge);
      formData.append("kode_apd", kodeAPD);
      formData.append("jumlah", 1);
      formData.append("status", 'UNITKERJA');

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
      this.componentDidMount()
    });
  }


  DisplaySnackBar = (nama) => {
    var ketemu = false;
    for (let index = 0; index < this.state.listApdChosen.length; index++) {
      let tempApd = this.state.listApdChosen[index]
      if (nama.id == tempApd.id) {
        ketemu = true
      }
    }
    if (ketemu == false) {
      let arrName = this.state.listApdNameChosen;
      let arrApd = this.state.listApdChosen;
      arrName.push(nama.nama);
      arrApd.push(nama)
      this.setState({
        listApdNameChosen: arrName,
        listApdChosen: arrApd
      });
    } else {
      let idx = this.state.listApdChosen.indexOf(nama);
      let arrName = this.state.listApdNameChosen;
      let arrApd = this.state.listApdChosen;
      arrName.splice(idx, 1);
      arrApd.splice(idx, 1);
      this.setState({
        listApdNameChosen: arrName,
        listApdChosen: arrApd
      });
    }if (this.state.listApdChosen.length==0){
      console.log('masuk undefined')
      this.refs.ReactNativeSnackBar.SnackBarCloseFunction();
    }else{
      this.refs.ReactNativeSnackBar.ShowSnackBarFunction(this.state.listApdNameChosen, this.state.listApdChosen);
    }
  };

  componentDidMount() {
    AsyncStorage.getItem("token").then(token => {
      const url = GlobalConfig.SERVERHOST + 'validasi';
      var formData = new FormData();
      formData.append("token", token)
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson))
          this.setState({
            listProfile: responseJson.user,
            name: responseJson.user.name,
            badge: responseJson.user.no_badge,
            isloading: false
          });
          this.loadWishlist();
          this.loadData();
        })
        .catch(error => {
          console.log(error);
        });
    });

    this._onFocusListener = this.props.navigation.addListener(
      "didFocus",
      payload => {
        this.loadData();
        this.loadWishlist();
        console.log("masuk didfokus");

        this.setState({
          listApdNameChosen: [],
          listApdChosen: []
        });
        this.refs.ReactNativeSnackBar.EmptySnackBarFunction();
        this.refs.ReactNativeSnackBar.SnackBarCloseFunction();

      }
    );
  }

  loadWishlist() {
    AsyncStorage.getItem("token").then(value => {
      const url = GlobalConfig.SERVERHOST + "orderapd/wishlist/get";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("badge", this.state.badge);
      formData.append("status", 'UNITKERJA');


      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
            this.setState({
              listKodeWishlist: responseJson.data,
            });
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  onRefresh() {
    console.log('refreshing')
    this.setState({ isloading: true }, function() {
      this.loadData();
      this.loadWishlist();
    });
  }


  loadData(groupAPD) {
    this.setState({
      isloading: true
    });
      const url = GlobalConfig.SERVERHOST + "masterapd";
      var formData = new FormData();
      formData.append("token", 'token');
      if(groupAPD!=undefined){
        formData.append("groupAPD", groupAPD);
      }
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          this.setState({
            dataSource: responseJson.data.filter(x => x.uk == 1),
            isloading: false
          });
        })
        .catch(error => {
          console.log(error);
        });
  }

  render() {
    var list;
    if (this.state.isloading) {
      list = (
        <View
          style={{ flex: 1, justifyContent: "center", alignItems: "center",marginTop:35  }}
        >
          <ActivityIndicator size="large" color="#330066" animating />
        </View>
      );
    } else {
      if (this.state.dataSource=='') {
        list = (
          <View
            style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
          >
            <Thumbnail
              square
              large
              source={require("../../../assets/images/empty.png")}
            />
            <Text>No Item!</Text>
          </View>
        );
      } else {
        list = (
          <FlatList
            data={this.state.dataSource}
            renderItem={this._renderItem}
            numColumns={2}
            keyExtractor={(item, index) => index.toString()}
            refreshControl={
            <RefreshControl
              refreshing={this.state.isloading}
              onRefresh={this.onRefresh.bind(this)}
            />}
          />
        );
      }
    }
    return (
        <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{ flex: 1 }}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("ApdMenu")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex: 3, alignItems: "center" }}>
            <Title style={styles.textbody}>Order APD</Title>
          </Body>
          <Right style={{ flex: 1 }}>
            <Button transparent onPress={() => this.props.navigation.navigate("OrderUnitKerjaTroli")}>
              <Icon
                name="ios-cart"
                style={{color:colors.white, fontSize:25}}
              />
            </Button>
          </Right>

        </Header>
          <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
          <Footer style={{height:((Dimensions.get("window").height===812||Dimensions.get("window").height===896) && Platform.OS==='ios')?22:null}}>
            <FooterTab style={styles.tabfooter}>
              <Button onPress={() =>
                  this.props.navigation.navigate("OrderPersonalAPD")
                }>
                <Text style={[styles.titleTabFont,{color:'white', fontWeight:'bold'}]}>PERSONAL</Text>
              </Button>
              <Button active style={styles.tabfooter}>
                <View style={{height:'40%'}}></View>
                <View style={{height:'50%'}}>
                <Text style={styles.titleTabFont}>UNIT KERJA</Text>
                </View>
                <View style={{height:'20%'}}></View>
                <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.white}}></View>
              </Button>
              <Button
                onPress={() =>
                  this.props.navigation.navigate("OrderPeminjamanAPD")
                }
              >
                <Text style={[styles.titleTabFont,{color:'white', fontWeight:'bold'}]}>PEMINJAMAN</Text>
              </Button>
            </FooterTab>
          </Footer>
          <View style={{ flex: 1 }}>
            <View style={styles.roundedBtn}>
              <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                <Left>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/all.png")}
                      name="All"
                      clicked={this.state.listClickedKatAPD[0] ? true : false} //coba j
                      handleOnPress={() => this.loadData()}
                    />
                  </View>
                </Left>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/body.jpg")}
                      name="Body"
                      clicked={this.state.listClickedKatAPD[1] ? true : false} //coba j
                      handleOnPress={() => this.loadData(600,1)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/head.jpg")}
                      name="Head"
                      clicked={this.state.listClickedKatAPD[2] ? true : false} //coba j
                      handleOnPress={() => this.loadData(100,2)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/eye.jpg")}
                      name="Eyes"
                      clicked={this.state.listClickedKatAPD[3] ? true : false} //coba j
                      handleOnPress={() => this.loadData(200,3)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/ear.jpg")}
                      name="Ear"
                      clicked={this.state.listClickedKatAPD[4] ? true : false} //coba j
                      handleOnPress={() => this.loadData(300,4)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/cloves.jpg")}
                      name="Cloves"
                      clicked={this.state.listClickedKatAPD[5] ? true : false} //coba j
                      handleOnPress={() => this.loadData(500,5)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/boots.jpg")}
                      name="Boots"
                      clicked={this.state.listClickedKatAPD[6] ? true : false} //coba j
                      handleOnPress={() => this.loadData(700,6)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/nose.png")}
                      name="Nose"
                      clicked={this.state.listClickedKatAPD[7] ? true : false} //coba j
                      handleOnPress={() => this.loadData(400,7)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/p3k.png")}
                      name="P3K"
                      clicked={this.state.listClickedKatAPD[8] ? true : false} //coba j
                      handleOnPress={() => this.loadData(900,8)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/full.jpg")}
                      name="Full Body"
                      clicked={this.state.listClickedKatAPD[9] ? true : false} //coba j
                      handleOnPress={() => this.loadData(800,9)}
                    />
                  </View>
                </Body>
                <Body>
                  <View style={styles.itemMenu}>
                    <MenuItems
                      imageUri={require("../../../assets/images/stock.png")}
                      name="Lain"
                      clicked={this.state.listClickedKatAPD[10] ? true : false} //coba j
                      handleOnPress={() => this.loadData(910,10)}
                    />
                  </View>
                </Body>
              </ScrollView>
            </View>

            <View style={{ marginBottom: this.state.listApdChosen.length!=0?55:5,flex:1,flexDirection:'column'  }}>{list}</View>
          </View>
          <SnackBar ref="ReactNativeSnackBar" data={this.props.navigation} />
        </Container>
    );
  }
}
AppRegistry.registerComponent('OrderUnitKerjaAPD', () => OrderUnitKerjaAPD);

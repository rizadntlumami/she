import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  AsyncStorage,
  ActivityIndicator,
  Platform,
  Alert
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
  Icon,
  Label,
  Textarea
} from "native-base";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import LinearGradient from "react-native-linear-gradient";
import RoundedButtons from "../../components/RoundedButton";
import styles from "../styles/OrderAPD";
import colors from "../../../styles/colors";
import SnackBar from "./SnackBarOrderPersonal";
import GlobalConfig from "../../components/GlobalConfig";
import DatePicker from 'react-native-datepicker';

var that;
export default class OrderPeminjamanTroli extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      listAPDPeminjaman: [],
      jumlahApd: [],
      dataProfile: [],
      isloading:true,
      visibleDialogSubmit:false,
      textDialogSubmit:'',
      visibleKonfirmasi:false,
      listOrder:[],
      listID:[],
      listKodeAPD:[],

    };
  }

  static navigationOptions = {
    header: null
  };

  valueJumlah(index) {
    let arr = this.state.jumlahApd;
    if (arr[index] == undefined) {
      return "1";
      console.log("masuk sini");
    } else {
      return this.state.jumlahApd[index];
    }
  }

  ubahJumlah(jumlah, index) {
    let arr = this.state.jumlahApd;
    arr[index] = jumlah;
    this.setState({
      jumlahApd: arr
    });
  }

  tambahJumlah(index) {
    let arr = this.state.jumlahApd;

    arr[index] = arr[index] + 1;
    this.setState({
      jumlahApd: arr
    });

    console.log(this.state.jumlahApd[index]);
  }

  kurangJumlah(index) {
    let arr = this.state.jumlahApd;
    if (arr[index] == 1 || arr[index] == null || arr[index] == undefined) {
    } else {
      arr[index] = arr[index] - 1;
      this.setState({
        jumlahApd: arr
      });
    }
  }

  sendApdKonfirmasi(){
    if (this.state.tgl_kembali == null) {
      alert('Masukkan Tanggal Pengembalian');
    }
    else if (this.state.keterangan == null) {
      alert('Masukkan Keterangan');
    } else {
      this.setState({
        visibleKonfirmasi:true
      })

    }
  }

  sendApd() {

      for (let i = 0; i < this.state.dataSource.length; i++) {
      let idAPD = this.state.dataSource[i].id;
      let kodeAPD = this.state.dataSource[i].kode_apd;
      let jumlahLet = this.state.jumlahApd[i];
      var temp = {'kode_apd': kodeAPD, 'jumlah': jumlahLet};
          var data = this.state.listOrder;
          var idWishlist = this.state.listID;
          var idKodeAPD = this.state.listKodeAPD
          data.push(temp);
          idWishlist.push(idAPD);
          idKodeAPD.push(kodeAPD);

          this.setState({
            listOrder:data,
            listID:idWishlist,
            listKodeAPD:idKodeAPD,
          })
    }
    this.loadFinalOrder()

  }

  deleteWishlistAll(){
    for (let i = 0; i < this.state.listID.length; i++) {
      let idAPD = this.state.listID[i];

      AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'orderapd/wishlist/delete';
        var formData = new FormData();
        formData.append("token", value);
        formData.append("id", idAPD);

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
      })
    }
    this.minusMaster();
  }

  minusMaster(){
    for (let i = 0; i < this.state.listKodeAPD.length; i++) {
      let kodeAPD = this.state.listKodeAPD[i];
      let jumlahAPD = this.state.jumlahApd[i];

      AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'masterapd/minusMaster';
        var formData = new FormData();
        formData.append("token", value);
        formData.append("kode_apd", kodeAPD);
        formData.append("minus", jumlahAPD-1);

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
      })
    }
    this.loadData()
  }

  loadFinalOrder(){

    this.setState({ visibleKonfirmasi: false })
    this.setState({
      textDialogSubmit:'Creating order...',
    })
    this.setState({
      visibleDialogSubmit:true,
    })

    AsyncStorage.getItem("token").then(value => {
      var url = GlobalConfig.SERVERHOST + "orderapd/create/pinjam";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("nopeg", this.state.badge);
      formData.append("status", 'ORDER');
      formData.append("detail", JSON.stringify(this.state.listOrder));
      formData.append("note", this.state.keterangan);
      formData.append("retur_date", this.state.tgl_kembali);

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit:false,
              listOrder:[],
            })
            Alert.alert('Success', 'Order Success, ID ORDER: '+response.data.kode_pinjam, [{
              text: 'Okay'
            }])
            this.deleteWishlistAll();
            this.props.navigation.navigate('OrderPeminjamanAPD')
          } else {
            this.setState({
              visibleDialogSubmit:false,
            })
            Alert.alert('Error', 'Order Failed', [{
              text: 'Okay'
            }])
          }
        })
        .catch((error) => {
          Alert.alert('Error', 'Check Your Internet Connection', [{
              text: 'Okay'
          }])
            console.log(error)
        })
    });
  }




  componentDidMount() {
    AsyncStorage.getItem("token").then(token => {
      const url = GlobalConfig.SERVERHOST + 'validasi';
      var formData = new FormData();
      formData.append("token", token)
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson))
          this.setState({
            listProfile: responseJson.user,
            name: responseJson.user.name,
            badge: responseJson.user.no_badge,
            isloading: false
          });
          this.loadData();
        })
        .catch(error => {
          console.log(error);
        });
    });
  }

  loadData() {
      AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'orderapd/wishlist/get';
        var formData = new FormData();
        formData.append("token", value);
        formData.append("badge", this.state.badge);
        formData.append("status", 'PEMINJAMAN');

        fetch(url, {
          headers: {
            'Content-Type': 'multipart/form-data'
          },
          method: 'POST',
          body: formData
        })
          .then((response) => response.json())
          .then((responseJson) => {
            // if (responseJson.data==undefined){
            //   this.setState({
            //     jumlahApd: [],
            //     dataSource: undefined,
            //     isloading: false
            //   });
            // }else{
              this.setState({
                dataSource: responseJson.data,
                isloading: false
              });
              var arr = new Array(this.state.dataSource.length);
              var tempData = this.state.dataSource;
              for (let index = 0; index < arr.length; index++) {
                arr[index] = parseInt(tempData[index].jumlah)
              }
              this.setState({
                jumlahApd: arr
              });
            // }
          })
          .catch((error) => {
            console.log(error)
          })
      })
    }

    deleteWishlist(id, kodeAPD) {
      this.setState({
        textDialogSubmit:'Deleting Item...',
      })
      this.setState({
        visibleDialogSubmit:true,
      })

      AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'orderapd/wishlist/cancel';
        var formData = new FormData();
        formData.append("token", value);
        formData.append("id", id);
        formData.append("kode_apd", kodeAPD);

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
          .then(response => response.json())
          .then(response => {
            if (response.status == 200) {
              this.setState({
                visibleDialogSubmit:false,
              })
              Alert.alert('Success', 'Delete Success', [{
                text: 'Okay',
              }])
              this.loadData();
            } else {
              this.setState({
                visibleDialogSubmit:false,
              })
              Alert.alert('Error', 'Troli Failed', [{
                text: 'Okay'
              }])
            }
          })
          .catch((error) => {
            Alert.alert('Error', 'Check Your Internet Connection', [{
                text: 'Okay'
            }])
              console.log(error)
          })
      })
    }

  render() {
    let arr = this.state.jumlahApd;
    var listAPDPeminjaman;

    if (this.state.dataSource) {
      listAPDPeminjaman = this.state.dataSource.map((apdListPeminjaman, index) => (
        <View key={index} style={{ backgroundColor: "#FEFEFE" }}>
            <CardItem style={{ borderRadius: 0, marginTop:4, backgroundColor:colors.gray }}>
                <View style={{ flex: 1, flexDirection: "row", marginTop:0, backgroundColor:colors.gray }}>
                    <View style={{width:'60%'}}>
                        <Text style={styles.viewName}>
                          {apdListPeminjaman.kode_apd}
                        </Text>
                        <Text style={styles.viewMerk}>
                          {apdListPeminjaman.name_apd}
                        </Text>
                    </View>

                    <View style={{width:'40%'}}>
                        <View style={{ flex: 1, flexDirection: "row"}}>
                          <Button onPress={() => this.kurangJumlah(index)} style={styles.btnQTYLeft}>
                              <Icon
                                name="ios-arrow-back"
                                style={styles.facebookButtonIconQTY}
                              />
                          </Button>

                          <View style={{width:30, height:30, marginTop: 0, backgroundColor: colors.white}}>
                            <Input
                              style={{
                                height:30,
                                marginTop:-5,
                                fontSize:12,
                                textAlign:'center'}}
                              value={this.state.jumlahApd[index] + ""}
                              keyboardType='numeric'
                              onChangeText={text => this.ubahJumlah(text, index)}
                            />
                          </View>

                          <Button onPress={() => this.tambahJumlah(index)} style={styles.btnQTYRight}>
                            <Icon
                              name="ios-arrow-forward"
                              style={styles.facebookButtonIconQTY}
                            />
                          </Button>

                          <View style={styles.btnWishlist}>
                            <Button transparent onPress={() => this.deleteWishlist(apdListPeminjaman.id, apdListPeminjaman.kode_apd)}>
                              <Icon
                                name="ios-trash"
                                style={styles.facebookButtonIconDangerous}
                              />
                            </Button>
                          </View>
                        </View>
                    </View>
                </View>
            </CardItem>
        </View>
      ))
    }

    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("OrderPeminjamanAPD")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:4,alignItems:'center'}}>
            <Title style={styles.textbody}>Konfirmasi Order</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <Footer style={styles.tabHeight}>
          <FooterTab style={styles.tabfooter}>
            <Button onPress={() => this.props.navigation.navigate("OrderPersonalTroli")}>
              <Text style={{color:'white', fontWeight:'bold'}}>Personal</Text>
            </Button>
            <Button onPress={() => this.props.navigation.navigate("OrderUnitKerjaTroli")}>
              <Text style={{color:'white', fontWeight:'bold'}}>Unit Kerja</Text>
            </Button>
            <Button active style={styles.tabfooter}>
              <View style={{height:'40%'}}></View>
              <View style={{height:'50%'}}>
              <Text>PEMINJAMAN</Text>
              </View>
              <View style={{height:'20%'}}></View>
              <View style={{borderWidth:2, marginTop:2, height:0.5, width:'100%', borderColor:colors.white}}></View>
            </Button>
          </FooterTab>
        </Footer>
        <View style={{ flex: 1 }}>
          <Content style={{ marginTop: 0 }}>
          {this.state.isloading ?
            (
              <View
              style={{
                flex: 1,
                justifyContent: "center",
                alignItems: "center"
              }}
            >
              <ActivityIndicator size="large" color="#330066" animating />
            </View>
            ) :
            listAPDPeminjaman
          }
            {listAPDPeminjaman != '' && (
            <View style={{ backgroundColor: '#FEFEFE' }}>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1, flexDirection: 'row' }}>
                  <View>
                    <Text style={styles.viewMerk}>Return Date</Text>
                    <DatePicker
                      style={{ width: 320, fontSize:11, borderRadius:20 }}
                      date={this.state.tgl_kembali}
                      mode="date"
                      placeholder="Return Date"
                      format="YYYY-MM-DD"
                      minDate="2018-01-01"
                      maxDate="2050-12-31"
                      confirmBtnText="Confirm"
                      cancelBtnText="Cancel"
                      customStyles={{
                        dateIcon: {
                          position: 'absolute',
                          left: 0,
                          top: 5,
                          marginLeft: 0
                        },
                        dateInput: {
                          marginLeft: 50,
                          borderRadius:20
                        }
                      }}
                      onDateChange={(date) => { this.setState({ tgl_kembali: date }) }} />
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.gray  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.viewMerk}>Keterangan</Text>
                  </View>
                  <View>
                  <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={3} bordered value={this.state.keterangan} placeholder='Description' onChangeText={(text) => this.setState({ keterangan: text })} />
                  </View>
                </View>
              </CardItem>
            </View>
          )}
          </Content>

          {listAPDPeminjaman == '' ? (
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center"  }}>
              <Thumbnail
                square
                large
                source={require("../../../assets/images/empty.png")}
              />
              <Text>No Item!</Text>
            </View>
          ) : (
          <View style={styles.Contentsave}>
            <Button
              block
              style={{
                height: 45,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 20,
                borderWidth: 1,
                backgroundColor: "#00b300",
                borderColor: "#00b300",
                borderRadius: 4
              }}
              onPress={() => this.sendApdKonfirmasi()}
            >
              <Text style={styles.buttonText}>Submit</Text>
            </Button>
          </View>
        )}
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialogSubmit}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogTitle={<DialogTitle title={this.state.textDialogSubmit} />}
          >
            <DialogContent>
              {

                  <ActivityIndicator size="large" color="#330066" animating />

              }
            </DialogContent>
          </Dialog>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleKonfirmasi}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            onTouchOutside={() => {
              this.setState({ visibleKonfirmasi: false });
            }}
            // dialogStyle={{ position: 'absolute', top: this.state.posDialog }}
            dialogTitle={
              <DialogTitle
                title="Apakah yang Anda pesan sudah sesuai?"
                style={{
                  backgroundColor: colors.white,
                  marginTop:20,
                  marginBottom:20
                }}
                textStyle={{fontSize:15}}
                hasTitleBar={false}
                align="center"
              />
            }
          >
            <DialogContent
              style={{
                backgroundColor: colors.white
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Button
                    block
                    style={{
                      height: 45,
                      marginLeft: 20,
                      marginRight: 20,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: colors.gray02,
                      borderColor: colors.gray02,
                      borderRadius: 4
                    }}
                    onPress={() => this.setState({ visibleKonfirmasi: false })}
                  >
                    <Text style={styles.buttonText}>Batal</Text>
                  </Button>
                </View>
                <View style={{ flex: 1 }}>
                  <Button
                    block
                    style={{
                      height: 45,
                      marginLeft: 20,
                      marginRight: 20,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: "#00b300",
                      borderColor: "#00b300",
                      borderRadius: 4
                    }}
                    onPress={() => this.sendApd()}
                  >
                    <Text style={styles.buttonText}>Iya</Text>
                  </Button>
                </View>
              </View>
            </DialogContent>
          </Dialog>
        </View>
      </Container>
    );
  }
}

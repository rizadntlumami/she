import React, { Component } from "react";
import {
    Platform,
    StyleSheet,
    View,
    Image,
    Text,
    StatusBar,
    ScrollView,
    AsyncStorage,
    FlatList,
    Alert,
    TouchableOpacity,
    ActivityIndicator,
    Slider,
} from "react-native";
import {
    Container,
    Header,
    Title,
    Content,
    Footer,
    FooterTab,
    Button,
    Left,
    Right,
    Card,
    CardItem,
    Body,
    Fab,
    Textarea,
    Icon,
    Picker,
    Form,
    Input,
    Label,
    Item,
    Thumbnail
} from "native-base";
import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import SubMenuSafety from "../../components/SubMenuSafety";
import GlobalConfig from '../../components/GlobalConfig';
import styles from "../styles/FireMenu";
import colors from "../../../styles/colors";
import CustomFooter from "../../components/CustomFooter";
import DateTimePicker from 'react-native-datepicker';
import DatePicker from 'react-native-datepicker';
import ImagePicker from "react-native-image-picker";
import Ripple from "react-native-material-ripple";
import CheckBox from 'react-native-check-box';
import Moment from 'moment'
import ListView from "../../components/ListView";

class ListItemPegawai extends React.PureComponent {
    render() {
        return (
            <View style={{ backgroundColor: "#FEFEFE" }}>
                <Ripple
                    style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() =>
                        this.props.setSelectedPegawaiLabel(this.props.data)
                    }
                    rippleColor={colors.accent}
                >
                    <CardItem
                        style={{
                            borderRadius: 0,
                            marginTop: 4,
                            backgroundColor: colors.white
                        }}
                    >
                        <View
                            style={{
                                flex: 1,
                                flexDirection: "row",
                                marginTop: 4,
                                backgroundColor: colors.white
                            }}
                        >
                            <View >
                                <Text style={{ fontSize: 12 }}>{this.props.data.mk_nopeg} - {this.props.data.mk_nama}</Text>
                            </View>
                        </View>
                    </CardItem>
                </Ripple>
            </View>
        );
    }
}

export default class DailyReportCO2Create extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: 'true',
      dataSource: [],
      isLoading: true,
      date:'',
      listPlant:[],
      visibleLoadingPegawai: false,
      visibleSearchListPegawai: false,
      selectedPegawaiLabel: '',
      searchWordPegawai: '',
      pegawaiName: '',
      pegawaiUnitKerja: '',
      shift:'1',
      listTabung:[],
      detailList:[],
      listTabungPlant:[],
      pplant:[],
      pplantdesc:[],
      pplantarea:[],
      locCode:[],
      gasName:[],
      totalMax:[],
      residual:[],
      area:'',
      plant:'',

      listLokasi:[],
      residualCemetron:0,
      residualSamator:0,
    };
  }


  static navigationOptions = {
      header: null
  };


  _renderItemTabung = ({ item }) => (
      <ListItemTabung data={item}/>
  )

  componentDidMount() {
      this._onFocusListener = this.props.navigation.addListener('didFocus', (payload) => {
      });
      var dateNow = Moment().format('YYYY-MM-DD');
      this.setState({
          date:dateNow,
      })
      this.loadPlant();
  }

  onChangePegawai(text) {
      this.setState({
          searchWordPegawai: text
      })
      this.setState({
          visibleLoadingPegawai: true
      })
          const url = GlobalConfig.SERVERHOST + 'getEmployee';
          var formData = new FormData();
          formData.append("searchEmp", this.state.searchWordPegawai)
          fetch(url, {
              headers: {
                  'Content-Type': 'multipart/form-data'
              },
              method: 'POST',
              body: formData
          })
              .then((response) => response.json())
              .then((responseJson) => {
                  this.setState({
                      visibleLoadingPegawai: false
                  })
                  this.setState({
                      listPegawai: responseJson.data,
                      listPegawaiMaster: responseJson.data,
                      isloading: false
                  });
              })
              .catch((error) => {
                  this.setState({
                      visibleLoadingPegawai: false
                  })
                  console.log(error)
              })
  }

  onClickSearchPegawai() {
      console.log('masuk')
      if (this.state.visibleSearchListPegawai) {
          this.setState({
              visibleSearchListPegawai: false
          })
      } else {
          this.setState({
              visibleSearchListPegawai: true
          })
      }
  }

  setSelectedPegawaiLabel(pegawai) {
      this.setState({
          pegawaiBadge: pegawai.mk_nopeg,
          pegawaiName: pegawai.mk_nama,
          pegawaiUnitKerja: pegawai.muk_nama,
          pegawaiKodeUnit: pegawai.muk_kode,
          visibleSearchListPegawai: false,
      })
  }

  _renderItemPegawai= ({ item }) => <ListItemPegawai data={item} setSelectedPegawaiLabel={text => this.setSelectedPegawaiLabel(text)} />;

  loadPlant(){
    this.setState({
      visibleLoadingUnitKerja:true
    })
        const url = GlobalConfig.SERVERHOST + 'getPlant';
        var formData = new FormData();
        formData.append("search", "")
        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listPlant: responseJson.data,
                    visibleLoadingUnitKerja:false
                });
            })
            .catch((error) => {
              this.setState({
                visibleLoadingUnitKerja:false
            });
                console.log(error)
            })
  }

  loadTabung(plant){
    this.setState({
      visibleLoadingUnitKerja:true
  });
        this.setState({
          listLokasi:[]
        })
        const url = GlobalConfig.SERVERHOST + 'getGasCO2';
        var formData = new FormData();
        formData.append("searchEmp", plant)

        fetch(url, {
            headers: {
                'Content-Type': 'multipart/form-data'
            },
            method: 'POST',
            body: formData
        })
            .then((response) => response.json())
            .then((responseJson) => {
                this.setState({
                    listLokasi: responseJson.data,
                    visibleLoadingUnitKerja:false
                });
            })
            .catch((error) => {
              this.setState({
                visibleLoadingUnitKerja:false
            });
                console.log(error)
            })

  }

  createReportCO2() {
  if (this.state.plant == '') {
    alert('Input Plant');
  }
  else if (this.state.pplantdesc == '') {
    alert('Input PPlant');
  }
  else if (this.state.pegawaiName =='') {
    alert('Input PIC');
  }
  else if (this.state.residualCemetron =='') {
    alert('Input Residual Cemetron');
  }
  else if (this.state.residualSamator =='') {
    alert('Input Residual Samator');
  }
  else if (this.state.shift == null) {
    alert('Input Shift');
  }
  else if (this.state.note == null) {
    alert('Input Note');
  }
  else {
      var url = GlobalConfig.SERVERHOST + "addInspectionGas";
      var formData = new FormData();
      formData.append("plant", this.state.plant);
      formData.append("pplant", '7301');
      formData.append("pplantdesc", this.state.pplantdesc);
      formData.append("total_max_cemetron", 6);
      formData.append("total_residual_cemetron", this.state.residualCemetron);
      formData.append("total_max_samator", 19);
      formData.append("total_residual_samator", this.state.residualSamator);
      formData.append("pic_name", this.state.pegawaiName);
      formData.append("pic_badge", this.state.pegawaiBadge);
      formData.append("shift", this.state.shift);
      formData.append("note", this.state.note);
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
      .then(response => response.json())
      .then(response => {
        if (response.status == 200) {
          Alert.alert('Success', 'Create Inspection CO2 Success', [{
            text: 'Oke'
          }])
          this.props.navigation.navigate('DailyReportCO2')
        } else {
          Alert.alert('Error', 'Create Inspection CO2 Failed', [{
            text: 'Oke'
          }])
        }
      });
    }
  }

  ubahResidualCemetron(jumlah) {
    let res = this.state.residualCemetron;
    res = jumlah;
    this.setState({
      residualCemetron: res
    });
  }

  tambahResidualCemetron() {
    let res = this.state.residualCemetron;
    if (res >= 6) {
    } else {
      res = res + 1;
      this.setState({
        residualCemetron: res
      });
    }
  }

  kurangResidualCemetron() {
    let res = this.state.residualCemetron;
    if (res == 0 || res == null || res == undefined) {
    } else {
      res = res - 1;
      this.setState({
        residualCemetron: res
      });
    }
  }

  ubahResidualSamator(jumlah) {
    let res = this.state.residualSamator;
    res = jumlah;
    this.setState({
      residualSamator: res
    });
  }

  tambahResidualSamator() {
    let res = this.state.residualSamator;
    if (res >= 19) {
    } else {
      res = res + 1;
      this.setState({
        residualSamator: res
      });
    }
  }

  kurangResidualSamator() {
    let res = this.state.residualSamator;
    if (res == 0 || res == null || res == undefined) {
    } else {
      res = res - 1;
      this.setState({
        residualSamator: res
      });
    }
  }

  onChangePlant(plant){
    this.loadTabung(plant)
    this.setState({
       plant:plant,
    })
  }

  render() {
    let listPlant = this.state.listPlant.map( (s, i) => {
      return <Picker.Item key={i} value={s.plant} label={s.plant_text}/>
    });

    let listLokasi = this.state.listLokasi.map( (s, i) => {
      return <Picker.Item key={i} value={s.pplantdesc} label={s.pplantdesc}/>
    });

    return (
      <Container style={styles.wrapper2}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("DailyReportCO2")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{ flex:3, alignItems:'center' }}>
            <Title style={styles.textbody}>Form Inspection CO2</Title>
          </Body>
          <Right style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.createReportCO2()}
            >
              <Icon
                name="ios-checkmark"
                style={{fontSize:40, color:colors.white}}
              />
            </Button>
          </Right>
        </Header>
      <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
      <View style={{ flex: 1 }}>
        <Content style={{ marginTop: 0 }}>
          <View style={{ backgroundColor:colors.white }}>
            <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }}>
              <View style={{ flex: 1,flexDirection:'row'}}>
                <View style={{width:'100%'}}>
                  <Text style={styles.titleInput}>Plant *</Text>
                  <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                      <Picker
                          mode="dropdown"
                          iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                          style={{ width: '100%', height:35}}
                          placeholder="Select Status ..."
                          placeholderStyle={{ color: "#bfc6ea" }}
                          placeholderIconColor="#007aff"
                          selectedValue={this.state.plant}
                          onValueChange={(itemValue) => this.onChangePlant(itemValue)}>
                          <Picker.Item label="Choose Plant..."  value=""/>
                          {listPlant}
                      </Picker>
                  </Form>
                </View>
              </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }}>
                <View style={{ flex: 1}}>
                  <View>
                    <Text style={styles.titleInput}>Area *</Text>
                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                            style={{ width: '100%', height: 35}}
                            placeholder="Select Lisensi ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.pplantdesc}
                            onValueChange={(itemValue) => this.setState({pplantdesc:itemValue})}>
                            <Picker.Item label="Choose Area..."  value=""/>
                            {listLokasi}
                        </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop: 0, backgroundColor: colors.white }}>
                  <View style={{ flex: 1 }}>
                      <View>
                          <Text style={styles.titleInput}>Pic *</Text>
                      </View>
                      <View style={{ flex: 1, flexDirection: 'column' }}>
                          <Button
                              block
                              style={{ justifyContent: "flex-start", flex: 1, borderWidth: Platform.OS==='ios'?1:0, borderColor: Platform.OS==='ios'?colors.lightwhite:null, borderRadius:5, paddingLeft: 10, marginRight: 5, marginLeft: 5, backgroundColor: colors.white, height: 35 }}
                              onPress={() => this.onClickSearchPegawai()}>
                              <Text style={{ fontSize: 12 }}>{this.state.pegawaiBadge} - {this.state.pegawaiName}</Text>
                          </Button>
                      </View>
                      {this.state.visibleSearchListPegawai &&
                          <View style={{ height: 300, flexDirection: 'column', borderWidth: 1, padding: 10, backgroundColor: colors.white, margin: 5, borderColor: "#E6E6E6", }}>
                              <View>
                                  <Textarea value={this.state.searchWordPegawai} style={{ marginLeft: 5, marginRight: 5, fontSize: 11 }} bordered rowSpan={1.5} onChangeText={(text) => this.onChangePegawai(text)} placeholder='Ketik nama karyawan' />
                              </View>
                              <FlatList
                                  data={this.state.listPegawai}
                                  renderItem={this._renderItemPegawai}
                                  keyExtractor={(item, index) => index.toString()}
                              />
                          </View>
                      }
                  </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }}>
                <View style={{ flex: 1 }}>
                  <View>
                    <Text style={styles.titleInput}>Shift *</Text>
                    <Form style={{borderWidth:1, borderRadius:5, marginRight:5, marginLeft:5, borderColor:"#E6E6E6", height:35}}>
                        <Picker
                            mode="dropdown"
                            iosIcon={<Icon name={Platform.OS? "ios-arrow-down": 'ios-arrow-down-outline'} />}
                            style={{ width: '100%', height:35}}
                            placeholder="Select Shift ..."
                            placeholderStyle={{ color: "#bfc6ea" }}
                            placeholderIconColor="#007aff"
                            selectedValue={this.state.shift}
                            onValueChange={(itemValue) => this.setState({shift:itemValue})}>
                            <Picker.Item label="Shift I (Satu)"  value="1" />
                            <Picker.Item label="Shift II (Dua)"  value="2"/>
                            <Picker.Item label="Shift III (Tiga)"  value="3"/>
                        </Picker>
                    </Form>
                  </View>
                </View>
              </CardItem>
              <CardItem style={{ borderRadius: 0, marginTop:0, backgroundColor:colors.white  }}>
                <View style={{ flex: 1 }}>
                  <View>
                  <Text style={styles.titleInput}>Note *</Text>
                  </View>
                  <View>
                  <Textarea style={ {borderRadius:5, marginLeft:5, marginRight:5, marginBottom:5, fontSize:11}} rowSpan={3} bordered value={this.state.note} placeholder='Type something here ...' onChangeText={(text) => this.setState({ note: text })} />
                  </View>
                </View>
              </CardItem>

              <CardItem style={{ borderRadius: 0, backgroundColor:colors.white }}>
                <View style={{flex:1}}>
                  <View style={{ flex: 1, flexDirection: "row", marginTop:0, backgroundColor:colors.white }}>
                      <View style={{width:'50%'}}>
                          <Text style={{fontSize:10}}>Residual Cemetron</Text>
                          <View style={{ flex: 1, flexDirection: "row", paddingLeft:'20%'}}>
                            <Button onPress={() => this.kurangResidualCemetron()} style={styles.btnQTYLeft}>
                                <Icon
                                  name="ios-arrow-back"
                                  style={styles.facebookButtonIconQTY}
                                />
                            </Button>
                            <View style={{width:40, height:30, marginTop: 10, backgroundColor: colors.white}}>
                              <Input
                                style={{
                                  height:30,
                                  marginTop:0,
                                  fontSize:9,
                                  textAlign:'center'}}
                                value={this.state.residualCemetron + ""}
                                keyboardType='numeric'
                                onChangeText={text => this.ubahResidualCemetron(text)}
                              />
                            </View>

                            <Button onPress={() => this.tambahResidualCemetron()} style={styles.btnQTYRight}>
                              <Icon
                                name="ios-arrow-forward"
                                style={styles.facebookButtonIconQTY}
                              />
                            </Button>

                          </View>
                      </View>
                      <View style={{width:'50%'}}>
                          <Text style={{fontSize:10}}>Residual Samator</Text>
                          <View style={{ flex: 1, flexDirection: "row", paddingLeft:'20%'}}>
                            <Button onPress={() => this.kurangResidualSamator()} style={styles.btnQTYLeft}>
                                <Icon
                                  name="ios-arrow-back"
                                  style={styles.facebookButtonIconQTY}
                                />
                            </Button>
                            <View style={{width:40, height:30, marginTop: 10, backgroundColor: colors.white}}>
                              <Input
                                style={{
                                  height:30,
                                  marginTop:0,
                                  fontSize:9,
                                  textAlign:'center'}}
                                value={this.state.residualSamator + ""}
                                keyboardType='numeric'
                                onChangeText={text => this.ubahResidualSamator(text)}
                              />
                            </View>

                            <Button onPress={() => this.tambahResidualSamator()} style={styles.btnQTYRight}>
                              <Icon
                                name="ios-arrow-forward"
                                style={styles.facebookButtonIconQTY}
                              />
                            </Button>
                          </View>
                      </View>
                  </View>
                </View>
              </CardItem>
          </View>
        </Content>
      </View>
      <View style={{ width: 270, position: "absolute" }}>
          <Dialog visible={this.state.visibleLoadingUnitKerja}>
            <DialogContent>
              {
                <ActivityIndicator size="large" color="#330066" animating />
              }
            </DialogContent>
          </Dialog>
      </View>
      <CustomFooter navigation={this.props.navigation} menu='FireSystem' />
    </Container>

    );
  }
}

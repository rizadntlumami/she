import React, { Component } from "react";
import {
  Platform,
  StyleSheet,
  View,
  Image,
  Text,
  StatusBar,
  ScrollView,
  ActivityIndicator,
  AsyncStorage,
  FlatList,
  Dimensions,
  ImageBackground,
  TouchableOpacity,
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Button,
  Left,
  Right,
  Body,
  Icon,
  Card,
  CardItem,
} from "native-base";

import LinearGradient from "react-native-linear-gradient";
import SubMenuFire from "../../components/FireSystem/SubMenuFire";
import styles from "../styles/FireMenu";
import GlobalConfig from "../../components/GlobalConfig";

// import NewsContentUnitCheck from "../../components/NewsContentUnitCheck";
// import NewsContentsDailyReportSection from "../../components/NewsContentDailyReportSection";
// import NewsContentsInspectionReport from "../../components/NewsContentInspectionReport";
// import NewsContentsFireReport from "../../components/NewsContentFireReport";
import Ripple from "react-native-material-ripple";
import colors from "../../../styles/colors";
import Moment from 'moment';
import CustomFooter from "../../components/CustomFooter";

var that
class ListItemNewsDailyReport extends React.PureComponent {
  navigateToScreen(route, listAktifitas) {
      AsyncStorage.setItem('list', JSON.stringify(listAktifitas)).then(() => {
          that.props.navigation.navigate(route);
      })
  }

  render(){
      return(
          <Ripple
            style={{
              flex: 2,
              justifyContent: "center",
              alignItems: "center"
            }}
            rippleSize={176}
            rippleDuration={600}
            rippleContainerBorderRadius={15}
            onPress={() => this.navigateToScreen('DailyReportAktifitasDetail', this.props.data)}
            rippleColor={colors.accent}
          >
          <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
            <NewsContentsDailyReportSection
              imageUri={require("../../../assets/images/hard.png")}
              aksi={this.props.data.TYPE_ACTION}
              date={this.props.data.DATE_ACTIVITY}
              status={this.props.data.STATUS}
            />
          </View>
          </Ripple>
      )
    }
}
var thatperiode;
class ListItemNewsInspectionReport extends React.PureComponent {
  navigateToScreen(route, idLokasi, periode) {
      AsyncStorage.setItem('idLokasi', idLokasi).then(() => {
        AsyncStorage.setItem('periode', periode).then(() => {
          that.props.navigation.navigate(route);
        })
      })
  }

  render(){
      return(
          <Ripple
            style={{
              flex: 2,
              justifyContent: "center",
              alignItems: "center"
            }}
            rippleSize={176}
            rippleDuration={600}
            rippleContainerBorderRadius={15}
            onPress={() => this.navigateToScreen('InspectionReportAparDetail', this.props.data.ID_LOKASI_APAR, thatperiode.state.periode)}
            rippleColor={colors.accent}
          >
          <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
            <NewsContentsInspectionReport
              imageUri={require("../../../assets/images/fire.png")}
              id={this.props.data.ID_LOKASI_APAR}
              nama={this.props.data.UPDATE_BY}
              date={this.props.data.TANGGAL_CEK_AKHIR}
            />
          </View>
          </Ripple>
      )
    }
}
class ListItemNewsFireReport extends React.PureComponent {
  navigateToScreen(route, idFireReport) {
      AsyncStorage.setItem('idFireReport', (idFireReport)).then(() => {
          that.props.navigation.navigate(route);
      })
  }
  render(){
      return(
          <Ripple
            style={{
              flex: 2,
              justifyContent: "center",
              alignItems: "center"
            }}
            rippleSize={176}
            rippleDuration={600}
            rippleContainerBorderRadius={15}
            onPress={() => this.navigateToScreen('FireReportDetail', this.props.data.ID)}
            rippleColor={colors.accent}
          >
          <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
            <NewsContentsFireReport
              imageUri={require("../../../assets/images/iconFire.png")}
              fireNumber={this.props.data.FIRE_NUMBER}
              status={this.props.data.STATUS}
              fireDate={this.props.data.FIRE_DATE}
              fireTime={this.props.data.FIRE_TIME}
            />
          </View>
          </Ripple>
      )
    }
}
class ListItemNewsUnitCheck extends React.PureComponent {
    navigateToScreen(route, listUnitcheck) {
        AsyncStorage.setItem('list', JSON.stringify(listUnitcheck)).then(() => {
            that.props.navigation.navigate(route);
        })
    }

    render() {
        return (
                <Ripple
                    style={{
                        flex: 2,
                        justifyContent: "center",
                        alignItems: "center"
                    }}
                    rippleSize={176}
                    rippleDuration={600}
                    rippleContainerBorderRadius={15}
                    onPress={() => this.navigateToScreen('DashboardUnitCheckDetail', this.props.data)}
                    // onPress={() => alert("Maintenance")}
                    rippleColor={colors.accent}
                >
                    <View style={{ marginTop: 0, marginLeft: 5, marginRight: 5 }}>
                    <NewsContentUnitCheck
                      imageUri={require("../../../assets/images/firetruck.png")}
                      kondisi={this.props.data.COUNT_NEGATIF_LIST}
                      tanggal={this.props.data.DATE_CHECKED}
                      inspector={this.props.data.VEHICLE_CODE}
                    />
                    </View>
                </Ripple>
        )
    }
}

class FireMenu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      token: "",
      active: "true",
      isLoading: true,
      isLoadingNews: false,
      dataSource: [],
      dataNewsUnitCheck: [],
      dataNewsUnitCheckPMK: [],
      dataNewsDailyReport: [],
      dataNewsInspectionReport: [],
      dataNewsFireReport: [],
      periode:'',
    };
    thatperiode=this;
  }

  static navigationOptions = {
    header: null
  };

  componentDidMount() {
    var dateNow = Moment().format('YYYY-MM');
    this.setState({
        periode:dateNow,
    })
    AsyncStorage.getItem("token")
      .then(value => {
        this.setState({ token: value });
      })
      .then(res => {
        //this.loadNewsDailyReport();
        //this.loadNewsUnitCheck();
        //this.loadNewsUnitCheckPMK();
        //this.loadNewsInsectionReport();
        //this.loadNewsFireReport();
      })
  }

  loadNewsUnitCheck() {
    this.setState({ isLoading: true })
    const url = GlobalConfig.SERVERHOST + "api/v_mobile/firesystem/unit_check/get_vehicle_list";
    var formData = new FormData();
    formData.append("token", this.state.token);
    formData.append("VEHICLE_TYPE", "Pick Up");
    formData.append("limit", 4);

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then((response) => response.json())
      .then((responseJson) => {
        // alert(JSON.stringify(responseJson))
        this.setState({
          dataNewsUnitCheck: responseJson.Data,
          isLoading: false
        });
      })
      .catch((error) => {
        console.log(error)
        this.setState({
          isLoading: false
        });
      })
  }
  loadNewsUnitCheckPMK() {
    this.setState({ isLoading: true })
    const url = GlobalConfig.SERVERHOST + "api/v_mobile/firesystem/unit_check/get_vehicle_list";
    var formData = new FormData();
    formData.append("token", this.state.token);
    formData.append("VEHICLE_TYPE", "PMK");
    formData.append("limit", 4);

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then((response) => response.json())
      .then((responseJson) => {
        // alert(JSON.stringify(responseJson))
        this.setState({
          dataNewsUnitCheckPMK: responseJson.Data,
          isLoading: false
        });
      })
      .catch((error) => {
        console.log(error)
        this.setState({
          isLoading: false
        });
      })
  }
  loadNewsInsectionReport() {
    this.setState({ isLoading: true })
    const url = GlobalConfig.SERVERHOST + "api/v_mobile/firesystem/apar_n/get_all_loc_apar";
    var formData = new FormData();
    formData.append("token", this.state.token);
    formData.append("limit", 4);

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then((response) => response.json())
      .then((responseJson) => {
        // alert(JSON.stringify(responseJson))
        this.setState({
          dataNewsInspectionReport: responseJson.Data,
          isLoading: false
        });
      })
      .catch((error) => {
        console.log(error)
        this.setState({
          isLoading: false
        });
      })
  }
  loadNewsDailyReport() {
    this.setState({ isLoading: true })
    const url = GlobalConfig.SERVERHOST + "api/v_mobile/firesystem/daily/view/activity";
    var formData = new FormData();
    formData.append("token", this.state.token);
    formData.append("DATE_ACTIVITY", '')

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then((response) => response.json())
      .then((responseJson) => {
        // alert(JSON.stringify(responseJson))
        this.setState({
          dataNewsDailyReport: responseJson.data,
          isLoading: false
        });
      })
      .catch((error) => {
        console.log(error)
        this.setState({
          isLoading: false
        });
      })
  }
  loadNewsFireReport() {
    this.setState({ isLoading: true })
    const url = GlobalConfig.SERVERHOST + 'api/v_mobile/firesystem/fire/list_view';
    var formData = new FormData();
    formData.append("token", this.state.token);

    fetch(url, {
      headers: {
        "Content-Type": "multipart/form-data"
      },
      method: "POST",
      body: formData
    })
      .then((response) => response.json())
      .then((responseJson) => {
        //alert(JSON.stringify(responseJson))
        this.setState({
          dataNewsFireReport: responseJson.data,
          isLoading: false
        });
      })
      .catch((error) => {
        console.log(error)
        this.setState({
          isLoading: false
        });
      })
  }

  _renderItemNewsDailyReport = ({ item }) => (
      <ListItemNewsDailyReport data={item}/>
  )

  _renderItemNewsInspectionReport = ({ item }) => (
      <ListItemNewsInspectionReport data={item}/>
  )

  _renderItemNewsFireReport = ({ item }) => (
      <ListItemNewsFireReport data={item}/>
  )

  _renderItemNewsUnitCheck = ({ item }) => (
      <ListItemNewsUnitCheck data={item}/>
  )

  _renderItemNewsUnitCheckPMK = ({ item }) => (
    <ListItemNewsUnitCheck data={item}/>
  )

  render() {
    that = this;
    return (
      <Container style={styles.wrapper}>
        <View style={{backgroundColor:colors.primer, height:130, paddingBottom:20}}>
          <View style={{flex:1, flexDirection:'row', marginLeft:20, marginRight:20, marginTop:30}}>
            <View style={{width:'10%'}}>
              <View style={{backgroundColor:colors.second, borderRadius:5, height:30, justifyContent:'center', alignItems:'center'}}>
              <TouchableOpacity
                transparent
                  onPress={() => this.props.navigation.navigate("HomeMenu")}>
                  <Icon
                      name='ios-home'
                      style={{fontSize:20, color:colors.white}}
                  />
                </TouchableOpacity>
              </View>
            </View>
            <View style={{width:'90%', paddingLeft:20}}>
              <Text style={{fontSize:18, fontWeight:'bold', color:colors.white}}>MENU FIRE SYSTEM</Text>
            </View>
          </View>
        </View>
        <View style={{marginLeft:20, marginRight:20, marginTop:-50, backgroundColor:colors.white, borderRadius:5, height:200,
          shadowColor:'#000',
          shadowOffset:{
            width:0,
            height:1,
          },
          shadowOpacity:1,
          shadowRadius:2,
          elevation:3,
        }}>
          <View style={{flex:1, flexDirection:'row'}}>
          <View style={{width:'33%', paddingTop:25}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("DailyReportAktifitas")}
                imageUri={require("../../../assets/images/iconDailyReportSection.png")}
                name="Daily Report"
              />
          </View>
          <View style={{width:'33%', paddingTop:25}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("InspectionReportApar")}
                imageUri={require("../../../assets/images/iconInspectionReport.png")}
                name="Inspection Report"
              />
          </View>
          <View style={{width:'33%', paddingTop:25}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("FireReport")}
                imageUri={require("../../../assets/images/iconFireReport.png")}
                name="Fire Report"
              />
          </View>
          </View>
          <View style={{flex:1, flexDirection:'row'}}>
          <View style={{width:'33%', paddingTop:10}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("UnitCheckPickup")}
                imageUri={require("../../../assets/images/iconUnitCheck.png")}
                name="Unit Check"
              />
          </View>
          <View style={{width:'33%', paddingTop:10}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("DashboardUnitCheckPickup")}
                imageUri={require("../../../assets/images/iconUnitCheckDashboard.png")}
                name="Dashboard"
              />
          </View>
          <View style={{width:'33%', paddingTop:10}}>
              <SubMenuFire
                handleOnPress={() => this.props.navigation.navigate("Lotto")}
                imageUri={require("../../../assets/images/iconLotto.png")}
                name="Lotto"
              />
          </View>
          </View>
        </View>
        <ScrollView>
        <View style={{flex:1, flexDirection:'row', paddingTop:25}}>
          <View style={{width:'50%', paddingLeft:20}}>
            <Text style={{fontSize:15, fontWeight:'bold'}}>Hot News</Text>
          </View>
          <View style={{width:'50%', paddingRight:20}}>
            <TouchableOpacity
              transparent
              onPress={() => this.props.navigation.navigate("AllNew")}
            >
              <Text style={{fontSize:12, textAlign:'right'}}>View All</Text>
            </TouchableOpacity>
          </View>
        </View>

        </ScrollView>
      </Container>
    );
  }
}

export default FireMenu;

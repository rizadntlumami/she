import React, { Component } from "react";
import {
  StyleSheet,
  View,
  StatusBar,
  AsyncStorage,
  ActivityIndicator,
  Platform,
  Alert
} from "react-native";
import {
  Container,
  Header,
  Title,
  Content,
  Footer,
  FooterTab,
  Text,
  Button,
  Left,
  Right,
  Body,
  Item,
  Card,
  CardItem,
  Input,
  Thumbnail,
  Fab,
  Icon
} from "native-base";
import LinearGradient from "react-native-linear-gradient";

import Dialog, {
  DialogTitle,
  SlideAnimation,
  DialogContent,
  DialogButton
} from "react-native-popup-dialog";
import RoundedButtons from "../../components/RoundedButton";
import styles from "../styles/OrderAPD";
import colors from "../../../styles/colors";
import SnackBar from "./SnackBarOrderPersonal";
import GlobalConfig from "../../components/GlobalConfig";

var that;
export default class OrderPersonalSubmit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      active: "true",
      dataSource: [],
      listApd: [],
      jumlahApd: [],
      visibleDialogSubmit:false,
      visibleKonfirmasi:false,
      listOrder:[],
      listKodeAPD:[],
    };
  }

  static navigationOptions = {
    header: null
  };

  valueJumlah(index) {
    let arr = this.state.jumlahApd;
    if (arr[index] == undefined) {
      return "1";
      console.log("masuk sini");
    } else {
      return this.state.jumlahApd[index];
    }
  }

  ubahJumlah(jumlah, index) {
    let arr = this.state.jumlahApd;
    arr[index] = jumlah;
    this.setState({
      jumlahApd: arr
    });
  }

  tambahJumlah() {
    alert("Anda Tidak Bisa menambahkan Jumlah");
  }

  kurangJumlah() {
    alert("Anda Tidak Bisa menambahkan Jumlah");
  }

  sendApd() {
    for (let i = 0; i < this.state.listApd.length; i++) {
      let idAPD = this.state.listApd[i].id;
      let kodeAPD = this.state.listApd[i].kode;
      var temp = {'kode_apd': kodeAPD, 'jumlah': 1};
          var data = this.state.listOrder;
          var idKodeAPD = this.state.listKodeAPD;
          data.push(temp);
          idKodeAPD.push(kodeAPD);

          this.setState({
            listOrder:data,
            listKodeAPD:idKodeAPD,
          })
    }
    this.loadFinalOrder()
  }

  minusMaster(){
    for (let i = 0; i < this.state.listKodeAPD.length; i++) {
      let kodeAPD = this.state.listKodeAPD[i];

      AsyncStorage.getItem('token').then((value) => {
        const url = GlobalConfig.SERVERHOST + 'masterapd/minusMaster';
        var formData = new FormData();
        formData.append("token", value);
        formData.append("kode_apd", kodeAPD);
        formData.append("minus", 1);

        fetch(url, {
          headers: {
            "Content-Type": "multipart/form-data"
          },
          method: "POST",
          body: formData
        })
      })
    }
  }


  loadFinalOrder(){

    this.setState({ visibleKonfirmasi: false })
    this.setState({
      textDialogSubmit:'Creating order...',
    })
    this.setState({
      visibleDialogSubmit:true,
    })

    AsyncStorage.getItem("token").then(value => {
      var url = GlobalConfig.SERVERHOST + "orderapd/create/personal";
      var formData = new FormData();
      formData.append("token", value);
      formData.append("no_badge", this.state.badge);
      formData.append("status", 'ORDER');
      formData.append("detail", JSON.stringify(this.state.listOrder));

      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(response => {
          if (response.status == 200) {
            this.setState({
              visibleDialogSubmit:false,
              listOrder:[],
            })
            Alert.alert('Success', 'Order Success, ID ORDER: '+response.data.kode_order, [{
              text: 'Okay'
            }])
            this.minusMaster();
            this.props.navigation.navigate('OrderPersonalAPD')
          } else {
            this.setState({
              visibleDialogSubmit:false,
            })
            Alert.alert('Error', 'Order Failed', [{
              text: 'Okay'
            }])
          }
        })
        .catch((error) => {
          Alert.alert('Error', 'Check Your Internet Connection', [{
              text: 'Okay'
          }])
            console.log(error)
        })
    });
  }

  componentDidMount() {
    AsyncStorage.getItem("token").then(token => {
      const url = GlobalConfig.SERVERHOST + 'validasi';
      var formData = new FormData();
      formData.append("token", token)
      fetch(url, {
        headers: {
          "Content-Type": "multipart/form-data"
        },
        method: "POST",
        body: formData
      })
        .then(response => response.json())
        .then(responseJson => {
          // alert(JSON.stringify(responseJson))
          this.setState({
            listProfile: responseJson.user,
            name: responseJson.user.name,
            badge: responseJson.user.no_badge,
            isloading: false
          });
          this.loadData();
        })
        .catch(error => {
          console.log(error);
        });
    });

    that = this;
    AsyncStorage.getItem("listApd")
      .then(req => JSON.parse(req))
      .then(json => {
        this.setState({
          listApd: json
        });
        let arr = new Array(this.state.listApd.length);
        for (let index = 0; index < arr.length; index++) {
          arr[index] = 1;
        }
        this.setState({
          jumlahApd: arr
        });
        console.log(this.state.listApd);
        console.log(this.state.jumlahApd);
      });
  }

  render() {
    let arr = this.state.jumlahApd;
    return (
      <Container style={styles.wrapper}>
        <Header style={styles.header}>
          <Left style={{flex:1}}>
            <Button
              transparent
              onPress={() => this.props.navigation.navigate("OrderPersonalAPD")}
            >
              <Icon
                name="ios-arrow-back"
                size={20}
                style={styles.facebookButtonIconOrder2}
              />
            </Button>
          </Left>
          <Body style={{flex:4,alignItems:'center'}}>
            <Title style={styles.textbody}>Konfirmasi Order</Title>
          </Body>
          <Right style={{flex:1}}/>
        </Header>
        <StatusBar backgroundColor={colors.green03} barStyle="light-content" />
        <View style={{ flex: 1 }}>
          <Content style={{ marginTop: 0 }}>
            {this.state.listApd.map((buttonInfo, index) => (
              <View key={index} style={{ backgroundColor: "#FEFEFE" }}>
                <CardItem
                  style={{
                    borderRadius: 0,
                    marginTop: 4,
                    backgroundColor: colors.gray
                  }}
                >
                  <View
                    style={{
                      flex: 1,
                      flexDirection: "row",
                      marginTop: 4,
                      backgroundColor: colors.gray
                    }}
                  >
                    <View style={{width:'70%'}}>
                      <Text style={styles.viewName}>
                        {buttonInfo.kode}
                      </Text>
                      <Text style={styles.viewMerk}>{buttonInfo.nama}</Text>
                    </View>
                    <View style={{width:'30%'}}>
                      <View style={{ flex: 1, flexDirection: "row" }}>
                        <Button
                          onPress={() => this.kurangJumlah()}
                          style={styles.btnQTYLeft}
                        >
                          <Icon
                            name="ios-arrow-back"
                            style={styles.facebookButtonIconQTY}
                          />
                        </Button>

                        <View
                          style={{
                            width: 30,
                            height: 30,
                            marginTop: 0,
                            backgroundColor: colors.white
                          }}
                        >
                          <Input
                            style={{
                              height: 30,
                              marginTop: -5,
                              fontSize: 12,
                              textAlign: "center"
                            }}
                            value="1"
                            keyboardType='numeric'
                          />
                        </View>

                        <Button
                          onPress={() => this.tambahJumlah()}
                          style={styles.btnQTYRight}
                        >
                          <Icon
                            name="ios-arrow-forward"
                            style={styles.facebookButtonIconQTY}
                          />
                        </Button>
                      </View>
                    </View>
                  </View>
                </CardItem>
              </View>
            ))}
          </Content>
          <View style={styles.Contentsave}>
            <Button
              block
              style={{
                height: 45,
                marginLeft: 20,
                marginRight: 20,
                marginBottom: 20,
                borderWidth: 1,
                backgroundColor: "#00b300",
                borderColor: "#00b300",
                borderRadius: 4
              }}
              onPress={() => this.setState({ visibleKonfirmasi: true })}
            >
              <Text style={styles.buttonText}>Submit</Text>
            </Button>
          </View>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleDialogSubmit}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            dialogTitle={<DialogTitle title="Creating Order.." />}
          >
            <DialogContent>
              {

                  <ActivityIndicator size="large" color="#330066" animating />

              }
            </DialogContent>
          </Dialog>
        </View>
        <View style={{ width: 270, position: "absolute" }}>
          <Dialog
            visible={this.state.visibleKonfirmasi}
            dialogAnimation={
              new SlideAnimation({
                slideFrom: "bottom"
              })
            }
            onTouchOutside={() => {
              this.setState({ visibleKonfirmasi: false });
            }}
            // dialogStyle={{ position: 'absolute', top: this.state.posDialog }}
            dialogTitle={
              <DialogTitle
                title="Apakah yang Anda pesan sudah sesuai?"
                style={{
                  backgroundColor: colors.white,
                  marginTop:20,
                  marginBottom:20
                }}
                textStyle={{fontSize:15}}
                hasTitleBar={false}
                align="center"
              />
            }
          >
            <DialogContent
              style={{
                backgroundColor: colors.white
              }}
            >
              <View style={{ flexDirection: "row" }}>
                <View style={{ flex: 1 }}>
                  <Button
                    block
                    style={{
                      height: 45,
                      marginLeft: 20,
                      marginRight: 20,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: colors.gray02,
                      borderColor: colors.gray02,
                      borderRadius: 4
                    }}
                    onPress={() => this.setState({ visibleKonfirmasi: false })}
                  >
                    <Text style={styles.buttonText}>Batal</Text>
                  </Button>
                </View>
                <View style={{ flex: 1 }}>
                  <Button
                    block
                    style={{
                      height: 45,
                      marginLeft: 20,
                      marginRight: 20,
                      marginBottom: 20,
                      borderWidth: 1,
                      backgroundColor: "#00b300",
                      borderColor: "#00b300",
                      borderRadius: 4
                    }}
                    onPress={() => this.sendApd()}
                  >
                    <Text style={styles.buttonText}>Iya</Text>
                  </Button>
                </View>
              </View>
            </DialogContent>
          </Dialog>
        </View>
      </Container>
    );
  }
}
